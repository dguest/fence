#!/usr/bin/env python
"""
the script provides statistic information for analysis approval procedure review
https://its.cern.ch/jira/browse/ATLASPO-200
"""

import sys
import json
import csv
from datetime import datetime

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

FAKE_DATA = '2001-01-01'
FAKE_GAP = '-1000'

def get_all_papers(dbm):
    """
    returns papers with no filter, so that we can get all of them
    """
    main_query = """
        SELECT 
         PUBLICATION.ID as pub_id,
         PUBLICATION.SHORT_TITLE as short_title,
         PUBLICATION.REF_CODE as reference_code,
         PUBLICATION.LEAD_GROUP as leading_group,
         to_char(PHASE_1.MAIL_TO_EDBOARD, 'yyyy-mm-dd') as editorial_board,
         to_char(PHASE_1.ATLAS_RELEASE, 'yyyy-mm-dd') as draft1,
         to_char(PHASE_3.LPG_APPROVAL, 'yyyy-mm-dd') as draft2,
         to_char(PUBLICATION.JOURNAL_SUB, 'yyyy-mm-dd') as published,
         to_char(PHASE_SUBMISSION.START_DATE, 'yyyy-mm-dd') as submission_started,
         PUBLICATION.ANALYSIS_ID as phase0id
        FROM
         PUBLICATION JOIN 
         PHASE ON PHASE.PUBLICATION_ID = PUBLICATION.ID JOIN
         PHASE_1 ON PHASE_1.PHASE_ID = PHASE.ID JOIN
         PHASE ph ON ph.PUBLICATION_ID = PUBLICATION.ID JOIN
         PHASE_3 ON PHASE_3.PHASE_ID = ph.ID  JOIN
         PHASE PHASE_SUBMISSION ON PHASE_SUBMISSION.PUBLICATION_ID = PUBLICATION.ID
        WHERE
         PHASE_SUBMISSION.PHASE = 4
         AND
         ph.PHASE = 3
         AND
         PHASE.PHASE = 1
         AND
         PUBLICATION.JOURNAL_SUB > DATE '2017-12-01'
         AND
         PHASE_1.MAIL_TO_EDBOARD IS NOT NULL
         AND
         PHASE_1.ATLAS_RELEASE IS NOT NULL
         AND
         PHASE_3.LPG_APPROVAL IS NOT NULL
          GROUP BY
          PUBLICATION.ID, PUBLICATION.SHORT_TITLE, PUBLICATION.REF_CODE, PUBLICATION.LEAD_GROUP, 
          PHASE_1.MAIL_TO_EDBOARD, PHASE_1.ATLAS_RELEASE, PHASE_3.LPG_APPROVAL, PUBLICATION.JOURNAL_SUB,
          PUBLICATION.ANALYSIS_ID, PHASE_SUBMISSION.START_DATE
          ORDER BY PUBLICATION.ID DESC    
          """

    results = dbm.execute(main_query)

    papers_dicts = []
    for result in results:
        paper_dict = {}
        (paper_dict["id"],
         paper_dict["short_title"],
         paper_dict["reference_code"],
         paper_dict["leading_group"],
         paper_dict["editorial_board"],
         paper_dict["draft1"],
         paper_dict["draft2"],
         paper_dict["published"],
         paper_dict["submission_started"],
         paper_dict["phase0id"]) = result

        paper_dict["type"] = "UNKNOWN"
        if paper_dict["leading_group"] in ["BPHY", "EXOT", "HDBS", "HIGG", "HION", "STDM", "SUSY",
                                           "TOPQ", "UPPH"]:
            paper_dict["type"] = "PA"
        elif paper_dict["leading_group"] in ["EGAM", "FTAG", "IDTR", "JETM", "MLFO", "MUON", "PMGR",
                                             "SIMU", "STAT", "TAUP"]:
            paper_dict["type"] = "CP"
        papers_dicts.append(paper_dict)

    return papers_dicts


def get_analysis_teams(dbm, papers):
    """
    gets all the analysis team for the list of papers
    """
    for paper in papers:

        count = []

        if paper["phase0_created"] != FAKE_DATA:
            analysis_team_date = paper["phase0_created"]
        else:
            analysis_team_date = paper["editorial_board"]

        for ptype in [1, 2, 3, 4]:
            memb_query = """SELECT
                COUNT(*)
                FROM 
                MEMB_PUBLICATION,
                EMPLOY
                WHERE 
                MEMB_PUBLICATION.PUBLICATION_ID = {}
                AND
                MEMB_PUBLICATION.GROUP_ID = 'EDITOR'
                AND
                EMPLOY.MEMB_ID = MEMB_PUBLICATION.MEMB_ID
                AND
                EMPLOY.START_DATE < DATE '{}'
                AND
                EMPLOY.END_DATE > DATE '{}'
                AND
                EMPLOY.PTYPE = {}
            """.format(paper["id"], analysis_team_date, analysis_team_date, ptype)

            result = dbm.execute(memb_query)
            count.append(result[0][0])

        (paper["physics"], paper["phd"], paper["student"], paper["summer_student"]) = count

        if sum(count):
            paper["analysis_team_size"] = sum(count)
            paper["physics_pc"] = round(paper["physics"] / (sum(count) * 1.0), 3)
            paper["phd_pc"] = round(paper["phd"] / (sum(count) * 1.0), 3)
            paper["student_pc"] = round(paper["student"] / (sum(count) * 1.0), 3)
            paper["summer_pc"] = round(paper["summer_student"] / (sum(count) * 1.0), 3)
        else:
            paper["analysis_team_size"] = 0
            paper["physics_pc"] = 0
            paper["phd_pc"] = 0
            paper["student_pc"] = 0
            paper["summer_pc"] = 0


def get_supp_docs(dbm, papers):
    """
    get all the supporting documents for all the papers
    counts them and save the result on the dictionary
    the links are stored to then get tue publication file from cds
    and count its page!!!
    """
    supporting_notes = []

    for paper in papers:

        supp_query = """SELECT
            URL
            FROM
            SUPP_NOTES
            WHERE
            SUPP_NOTES.PUBLICATION_ID = {}
        """.format(paper["id"])

        results = dbm.execute(supp_query)

        for result in results:
            supporting_notes.append({paper["id"] : result[0]})

        paper["supp_notes_count"] = len(results)

    return supporting_notes

def load_supp_docs_pages():
    """
    retrieves the count of pages from external file, calculated by another script
    """
    supp_docs_pages = {}
    with open("supp_pages_per_paper.json", "r") as in_file:
        supp_docs_pages = json.load(in_file)

    return supp_docs_pages


def make_diffs(papers):
    """
    calculate diff of days between 2 dates
    """

    for paper in papers:
        eoi = datetime.strptime(paper["eoi_meeting"], "%Y-%m-%d")
        phase0 = datetime.strptime(paper["phase0_created"], "%Y-%m-%d")
        edboard = datetime.strptime(paper["editorial_board"], "%Y-%m-%d")
        pre_approval = datetime.strptime(paper["pre_approval_meeting"], "%Y-%m-%d")
        approval = datetime.strptime(paper["approval_meeting"], "%Y-%m-%d")
        draft1 = datetime.strptime(paper["draft1"], "%Y-%m-%d")
        draft2 = datetime.strptime(paper["draft2"], "%Y-%m-%d")
        submission = datetime.strptime(paper["submission_started"], "%Y-%m-%d")
        published = datetime.strptime(paper["published"], "%Y-%m-%d")

        fake_time = datetime.strptime(FAKE_DATA, "%Y-%m-%d")

        if phase0 != fake_time and eoi != fake_time:
            paper["eoi_2_phase0"] = (phase0 - eoi).days
        else:
            paper["eoi_2_phase0"] = FAKE_GAP

        if edboard != fake_time and phase0 != fake_time:
            paper["phase0_2_edboard"] = (edboard - phase0).days
        else:
            paper["phase0_2_edboard"] = FAKE_GAP

        if pre_approval != fake_time and edboard != fake_time:
            paper["edboard_2_pre_approval"] = (pre_approval - edboard).days
        else:
            paper["edboard_2_pre_approval"] = FAKE_GAP

        if approval != fake_time and pre_approval != fake_time:
            paper["pre_approval_2_approval"] = (approval - pre_approval).days
        else:
            paper["pre_approval_2_approval"] = FAKE_GAP

        if draft1 != fake_time and approval != fake_time:
            paper["approval_2_draft1"] = (draft1 - approval).days
        else:
            paper["approval_2_draft1"] = FAKE_GAP

        if draft2 != fake_time and draft1 != fake_time:
            paper["draft1_2_draft2"] = (draft2 - draft1).days
        else:
            paper["draft1_2_draft2"] = FAKE_GAP

        if submission != fake_time and draft2 != fake_time:
            paper["draft2_2_submission"] = (submission - draft2).days
        else:
            paper["draft2_2_submission"] = FAKE_GAP

        if published != fake_time and submission != fake_time:
            paper["submission_2_published"] = (published - submission).days
        else:
            paper["submission_2_published"] = FAKE_GAP


def match_supp_docs_pages(papers, supp_notes_counts):
    """ adds the conut of supporting notes pages for the paper """
    for paper_id, value in supp_notes_counts.items():
        for paper in papers:
            if paper["id"] == int(paper_id):
                paper["supp_notes_pages_count"] = value

    return papers

def extract_analysis_ids(papers):
    """ all phase 0 ids are loaded in a list """
    return [str(paper["phase0id"]) for paper in papers if paper["phase0id"] is not None]

def match_all_phase0_dates(papers, analysis_ids, dbm):
    """ get all phase0 dates for every analysis """
    query = """
        SELECT
            ANALYSIS_SYS_MEETING.PUBLICATION_ID,
            to_char(ANALYSIS_SYS_MEETING.MEETING_DATE, 'yyyy-mm-dd'),
            ANALYSIS_SYS_MEETING.TYPE,
            to_char(ANALYSIS_SYS.CREATION_DATE, 'yyyy-mm-dd') as phase0_created
        FROM
            ANALYSIS_SYS_MEETING,
            ANALYSIS_SYS
        WHERE
            ANALYSIS_SYS_MEETING.PUBLICATION_ID IN ({})
            AND
            ANALYSIS_SYS_MEETING.PUBLICATION_ID = ANALYSIS_SYS.ID
        ORDER BY
            ANALYSIS_SYS_MEETING.PUBLICATION_ID, ANALYSIS_SYS_MEETING.MEETING_DATE;
    """.format(",".join(analysis_ids))

    results = dbm.execute(query)

    meetings = {}

    for result in results:
        if meetings.get(result[0], 0) == 0:
            meetings[result[0]] = {}
        meeting_type = result[2]
        if meetings[result[0]].get(meeting_type, 0) == 0:
            meetings[result[0]][meeting_type] = result[1]
        meetings[result[0]]['phase0_created'] = result[3]

    for meeting in meetings:
        if 'pre_approval_meeting' not in meetings[meeting]:
            meetings[meeting]['pre_approval_meeting'] = FAKE_DATA
        if 'edboard_meeting' not in meetings[meeting]:
            meetings[meeting]['edboard_meeting'] = FAKE_DATA
        if 'subgroup_meeting' not in meetings[meeting]:
            meetings[meeting]['subgroup_meeting'] = FAKE_DATA
        if 'eoi_meeting' not in meetings[meeting]:
            meetings[meeting]['eoi_meeting'] = FAKE_DATA
        if 'phase0_created' not in meetings[meeting]:
            meetings[meeting]['phase0_created'] = FAKE_DATA
        if 'edboard_request_meeting' not in meetings[meeting]:
            meetings[meeting]['edboard_request_meeting'] = FAKE_DATA
        if 'approval_meeting' not in meetings[meeting]:
            meetings[meeting]['approval_meeting'] = FAKE_DATA

    empty_meetings = {'pre_approval_meeting': FAKE_DATA,
                      'edboard_meeting': FAKE_DATA,
                      'subgroup_meeting': FAKE_DATA,
                      'eoi_meeting': FAKE_DATA,
                      'phase0_created': FAKE_DATA,
                      'edboard_request_meeting': FAKE_DATA,
                      'approval_meeting': FAKE_DATA}

    for paper in papers:
        if paper["phase0id"] is not None:
            if paper["phase0id"] in meetings:
                paper.update(meetings[paper["phase0id"]])
                continue
        paper.update(empty_meetings)
        del paper['edboard_meeting']
        del paper['subgroup_meeting']
        del paper['edboard_request_meeting']
        del paper['phase0id']

def save(papers):
    """ handles saving operations """
    with open("output.json", "w+") as out_file:
        json.dump(papers, out_file)

    with open("output.txt", "w+") as txt_file, open("output.csv", "w+") as csv_file:
        fieldnames = ["id", "short_title", "reference_code", "leading_group", "type",
                      "supp_notes_count", "supp_notes_pages_count",
                      "eoi_meeting", "phase0_created", "eoi_2_phase0",
                      "editorial_board", "phase0_2_edboard", "pre_approval_meeting",
                      "edboard_2_pre_approval", "approval_meeting", "pre_approval_2_approval",
                      "draft1", "approval_2_draft1", "draft2", "draft1_2_draft2", "submission_started",
                      "draft2_2_submission", "published", "submission_2_published",
                      "analysis_team_size", "physics", "phd", "student",
                      "summer_student", "physics_pc", "phd_pc", "student_pc", "summer_pc"]
        txt_writer = csv.DictWriter(txt_file, fieldnames=fieldnames, extrasaction='ignore', delimiter='\t')
        csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames, extrasaction='ignore')

        txt_writer.writeheader()
        csv_writer.writeheader()

        for paper in papers:
            txt_writer.writerow(paper)
            csv_writer.writerow(paper)

def run():
    """
    runs the operation to retrieve all the papers and add NULL to missing dates
    """

    dbm = DBManager()
    papers = get_all_papers(dbm)
    get_supp_docs(dbm, papers)
    supp_notes_counts = load_supp_docs_pages()
    match_supp_docs_pages(papers, supp_notes_counts)
    analysis_ids = extract_analysis_ids(papers)
    match_all_phase0_dates(papers, analysis_ids, dbm)
    make_diffs(papers)
    get_analysis_teams(dbm, papers)
    save(papers)


run()
