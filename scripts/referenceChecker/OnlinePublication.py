#!/usr/bin/env python2

"""
handles common operations for online publications
"""

import os
import requests
from bs4 import BeautifulSoup

class OnlinePublication(object):
    """
    common operations for online publications
    """

    BASE_CACHED_PATH = "/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/referenceChecker/cached_pages/"

    def __init__(self, url=None, original_url=None):

        self.url = url
        if not original_url:
            self.original_url = url
        else:
            self.original_url = original_url
        self.url_path = None
        self._url_to_path()

        self.title = None
        self.authors = None

        self.authors_list = []

        self.page_content = None

    def _url_to_path(self, url=None):
        """
        translates a url to a valid path
        """
        if not self.original_url:
            return

        path = self.original_url.replace("https://", "").replace("http://", "")
        path = path.replace("/", "_").replace(":", "_").replace("&", "_").replace("?", "_")
        self.url_path = OnlinePublication.BASE_CACHED_PATH + path

    def check_cached(self):
        """
        checks if cached version of webpage already saved
        """
        if not self.url:
            print("[ERROR] trying to check a webpage cached page but no url was found")
            return False

        if not self.url_path:
            self._url_to_path()

        if os.path.isfile(self.url_path):
            return True

        return False

    def _load_cached_version(self):
        """
        loads in page content the cached version of the page
        """
        if self.check_cached():
            with open(self.url_path, "r") as in_file:
                self.page_content = in_file.read()
                return True
        return False

    def load(self, cache=True):
        """
        downloads the webpage if not already downloaded
        and saves it (if cache equals True)
        """
        if cache:
            if self._load_cached_version():
                return

        if not self.url:
            print("[ERROR] trying to download a webpage but no url was found")
            return

        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0'}

        response = requests.get(self.url, headers=headers)
        self.page_content = response.text

        self._write_cache()

    def _write_cache(self):
        """
        must be called through load, which already verified url exists
        """
        if not self.url_path:
            self._url_to_path()

        with open(self.url_path, "w") as out_file:
            try:
                out_file.write(self.page_content.encode("UTF8"))
            except:
                print("[ERROR] can't write page content on file. Content = \n{}"
                      .format(self.page_content[0:1000]))

    def get_meta(self, meta_title_tag, meta_author_tag):
        """
        extracts title and authors from page_content through specified tags
        """
        soup = BeautifulSoup(self.page_content, "lxml")
        metas = soup.find_all('meta')

        for meta in metas:
            if 'name' in meta.attrs and meta.attrs['name'] == meta_title_tag:
                self.title = meta.attrs['content'].replace("&#xA;", "").replace("\n", "")
            if 'name' in meta.attrs and meta.attrs['name'] == meta_author_tag:
                self.authors_list.append(meta.attrs['content'].strip().encode('utf-8'))

    def get_title(self):
        """
        getter for title
        """
        return self.title

    def get_authors(self):
        """
        getter for authors
        """
        if not self.authors and self.authors_list:
            self.compose_authors()

        if self.authors == "The ATLAS Collaboration":
            self.authors = "ATLAS Collaboration"

        return self.authors

    def get_authors_list(self):
        """
        getter for authors list
        """
        return self.authors_list

    def compose_authors(self):
        """
        this method must be implemented in derived class
        """
        raise NotImplementedError()
