"""
classes to be used by the reference checker to handle bibliographies operations
"""

import os
from bib_parser import VerifiedEntries
import jinja2
#from BadResults import *

LOCAL_PATH = "/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/referenceChecker"

HTML_HEADERS = """<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <meta charset="utf-8" />

        <script type="text/x-mathjax-config">
          MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
        </script>
        <script type="text/javascript"
          src="http://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
        </script>


<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

    </head>
    <body>
        <script type="text/javascript">
    function set(id, outcome, title, authors, code, user){
        $.ajax({
            type: 'post',
            url: 'store_json.php',
            data: {
                id:id,
                outcome:outcome,
                title:title,
                authors:authors,
                code:code,
                user:user
            },
            success: function (response) {
                console.log("----");
                console.log(response)
            }
        });

        /* Graphic buttons changes */
        var span_container=document.getElementById(id);
        var buttonArray=span_container.getElementsByTagName('button');
        for(var s=0;s<buttonArray.length;s++){
            /* console.log('outcome: ' + outcome); */
            /* console.log(buttonArray[s].className); */
            /* Reset to default */
            if (buttonArray[s].className == "btn_clicked_correct"){
                buttonArray[s].className = 'correct_btn';
            }else if (buttonArray[s].className == "btn_clicked_error"){
                buttonArray[s].className = 'error_btn';
            }
            /* Set correct class for clicked button */
            if (outcome == 'correct' && buttonArray[s].className == "correct_btn"){
                buttonArray[s].className = "btn_clicked_correct"; 
            }else if (outcome == 'error' && buttonArray[s].className == 'error_btn'){
                buttonArray[s].className = "btn_clicked_error";   
            }
        }
    }
    </script>    
    """

HTML_FOOTERS = """</body></html>"""

VALIDATED = VerifiedEntries()
VALIDATED.load_entries()

#BAD_RESULTS = BadResults()



class Result(object):
    """
    handles and compares the two bibliographies
    """

    RESULT_MATCH = "exact match"
    RESULT_NO_MATCH = "no match"
    RESULT_CANT_COMPARE = "can't compare"
    RESULT_INCORRECT = "evaluated_incorrect"
    RESULT_CORRECT = "evaluated_correct"
    RESULT_MISSING_LINK = "missing link"

    def __init__(self):
        self.atlas = None
        self.original = None
        self.result = None
        self.note = None
        self.link_ok = False
        self.title_ok = False
        self.authors_ok = True

    def set_atlas(self, reference):
        """
        sets the reference as in ATLAS bibliography
        """
        self.atlas = reference

    def set_original(self, reference):
        """
        sets the reference as in external bibliography
        """
        self.original = reference

    def validate(self, debug=False):
        """
        checks if the bibliographies match
        """
        update_entry = False
        entry = {}

        #if not self.atlas.compare_title(self.original):
        #    BAD_RESULTS.append_title("title", self.atlas.get_bib_code(), self.atlas.get_title(), self.original.get_title())
        #    BAD_RESULTS.update()

        if self.original.get_source() in ['cds', 'arxiv', 'springer', 'aps', 'sciencedirect']:
            #print("[DEBUG] source = {}".format(self.original.get_source()))
            if self.atlas.get_shrink_title() == self.original.get_shrink_title():
                self.title_ok = True
                #print("[DEBUG] {} replacing white spaces and lowering, matches".format(self.original.get_source()))
                if self.atlas.get_cleaned_title() != self.original.get_cleaned_title():
                    self.note = "differences on capital letters or white spaces"
                    entry['note'] = self.note
            #if self.atlas.get_atlas_authors() == self.original.get_authors():
            #    self.authors_ok = True
            #if self.authors_ok and self.title_ok:
                self.result = self.RESULT_MATCH
                update_entry = True
                entry_name = self.atlas.get_bib_code()
                entry['title'] = self.atlas.get_title()
                entry['title_check'] = True
            else:
                self.result = self.RESULT_NO_MATCH
                print("[DEBUG-WARN] {} replacing won't match".format(self.original.get_source()))


                try:
                    if debug:
                        print("[INFO] - bibliography id = {}".format(self.atlas.get_bib_code()))
                        print("[DEBUG - atlas vs original] \n{}\n{}".
                              format(self.atlas.get_title(), self.original.get_title()))
                        print("[DEBUG - atlas vs original] \n{}\n{}".
                              format(self.atlas.get_shrink_title(), self.original.get_shrink_title()))

                        # for char in self.original.get_shrink_title():
                        #     print("[DEBUG] char({})".format(char))
                        #     print("[DEBUG] chr({})".format(ord(char)))

                except Exception as e:
                    print("[WARN-ENCODING] - check source titles not encoded {}".format(self.atlas.get_bib_code()))
                    print(e)

        else:
            self.result = self.RESULT_CANT_COMPARE

        if update_entry:
            VALIDATED.add_entry(entry_name, entry)

        return


    def get_result(self):
        """
        getter for result.result
        """
        return self.result


class Results(object):
    """
    manages the operations needed for a whole bibliography check
    """

    def __init__(self, ref_code=""):
        self.ref_code = ref_code

        self.results = []

    def set_ref_code(self, ref_code):
        self.ref_code = ref_code

    def ref_code_to_gitlab(self):
        """
        gets the gitlab link from the ref code
        """
        if not self.ref_code:
            return
        return ("https://gitlab.cern.ch/atlas-physics-office/" +
                self.ref_code[0:4] + "/" + "ANA-" + self.ref_code + "/" +
                "ANA-" + self.ref_code + "-PAPER")

    def validated_check(self, atlas, original, correct=[], error=[]):
        """
        checks if the entry has been already manually marked as correct/incorect
        """
        result = Result()
        result.set_atlas(atlas)
        result.set_original(original)

        code, label = Results.create_code(result)

        if code in correct:
            result.result = Result.RESULT_CORRECT
            self.results.append(result)
            return True, True
        elif code in error:
            result.result = Result.RESULT_INCORRECT
            # will not append the result here, but postpone in validate_bibs
            return True, False

        return False, None


    def validate(self, atlas, original, validated_error=None, debug=False):
        """
        sets elements to validate the bibliography
        """
        result = Result()

        result.set_atlas(atlas)
        result.set_original(original)

        if validated_error == False:
            result.result = Result.RESULT_INCORRECT
        else:
            result.validate(debug)

        return result

    def report_missing_url(self, atlas):
        """
        handles cases when the original bib can't be retrieved due to a unresolved link
        """
        result = Result()

        result.set_atlas(atlas)
        result.result = Result.RESULT_MISSING_LINK
        return result


    @staticmethod
    def create_buttons(count, code, original_url, title="", authors=""):
        """
        create buttons for html ajax interaction
        """
        html_buttons = """\n\t\t<button class="correct_btn" id="correct" onclick="set('{}','correct', '{}', '{}', '{}', '{}')">It's correct</button>
        <button class="error_btn" id="error" onclick="set('{}','error', '{}', '{}', '{}', '{}')">Error</button>"""

        return html_buttons.format(count, title, authors, code, original_url,
                                   count, title, authors, code, original_url)

    @staticmethod
    def create_code(result):
        """
        creates the code for the bib item
        """
        if result.result == Result.RESULT_MISSING_LINK:
            return False, False

        if result.original.source.lower() == 'arxiv':
            label = result.atlas.get_label(True)
        else:
            label = result.atlas.get_label()
        code = result.atlas.get_bib_code() + label

        return code, label

    def create_report(self, filepath):
        """
        creates the report and saves it
        """

        filepath = filepath.replace(".html", "_old.html")
        print("[DEBUG] filepath of report = {}".format(filepath))

        with open(filepath, "w") as out_file:
            previous_atlas_result = None
            bib_counter = 0
            out_file.write(HTML_HEADERS)

            inner_text = ""

            for count, result in enumerate(self.results):

                match_result = result.get_result()

                if previous_atlas_result != result.atlas.get_bib_code():
                    if bib_counter:
                        inner_text += "<tr><td><font size=1px>{}</font><br></tr></td></table>".format(previous_atlas_result)
                    bib_counter += 1
                    inner_text += "\n<table width=100%><tr bgcolor=#BCC6CC><td><span title='{}'><table width=100%><tr><td>".format(result.atlas.get_source_bibfile())
                    inner_text += "[<bold>{}</span></bold>] ".format(bib_counter)
                    inner_text += result.atlas.get_atlas_authors()

                    inner_text += "</td></tr><tr><td><i>{}</i></td></tr></table></span><br>".format(result.atlas.get_title())
                else:
                    inner_text += "<br>"

                if match_result == Result.RESULT_MISSING_LINK:
                    inner_text += "\n<tr><td><span class=missing_link>THE LINK TO THE JOURNAL CAN'T BE FOUND"
                    inner_text += "<br>{}</span>".format(str(result.atlas).replace("\n", "<br>"))
                else:

                    code, label = self.create_code(result)
                    original_url = result.original.get_url()

                    if match_result == Result.RESULT_NO_MATCH:
                        if not result.title_ok:
                            inner_text += "\n<tr bgcolor=#ffebeb><td><i><font class=link_red>{}".format(result.original.get_title())
                            inner_text += "</font></i><br>"
                        if not result.authors_ok:
                            try:
                                inner_text += "\n<tr bgcolor=#ffebeb><td><i><font class=link_red>{}".format(result.original.get_authors())
                                inner_text += "</font></i><br>"
                            except:
                                print("[DEBUG] bib code = {}".format(result.atlas.get_bib_code()))
                                print("[DEBUG] title = {}".format(result.original.get_title()))
                                print("[DEBUG] authors = {}".format(result.original.get_authors()))
                                raise
                        inner_text += "\n\t<span id='{}'>".format(count)
                        inner_text += self.create_buttons(count=count,
                                                          code=code,
                                                          original_url=original_url,
                                                          title=result.atlas.get_title(),
                                                          authors=result.atlas.get_authors())
                        inner_text += "\n\t</span>"
                        inner_text += "\n\t<a href='{}'' target=_blank class=link_red>{}</a>".format(original_url, label)
                        inner_text += "&nbsp;&nbsp; - <b><font color=\"red\">NO MATCH</font></b></td></tr><br>"
                    elif match_result == Result.RESULT_INCORRECT:
                        inner_text += "\n\t<tr><td><span id='{}'>".format(count)
                        inner_text += self.create_buttons(count=count,
                                                          code=code,
                                                          original_url=original_url,
                                                          title=result.atlas.get_title(),
                                                          authors=result.atlas.get_authors())
                        inner_text += "\n\t</span>"
                        inner_text += "\n\t<a href='{}'' target=_blank class=link_red>{}</a>".format(original_url, label)
                        inner_text += "&nbsp; - <b><font color=\"red\">PREVIOUSLY MARKED AS INCORRECT</font></b></td></tr><br>"
                    elif match_result == Result.RESULT_CANT_COMPARE:
                        inner_text += "\n\t<tr bgcolor=#FFFFE0><td><span id='{}'>".format(count)
                        inner_text += self.create_buttons(count=count,
                                                          code=code,
                                                          original_url=original_url,
                                                          title=result.atlas.get_title(),
                                                          authors=result.atlas.get_authors())
                        inner_text += "\n\t</span>&nbsp;"
                        inner_text += "\n\t<a href='{}'' target=_blank class=link_green>{}</a>".format(original_url, label)
                        inner_text += "&nbsp;&nbsp;&nbsp; - <b>couldn't retrieve webpage, action needed</b></td></tr><br>"
                    else:
                        inner_text += "\n<tr><td><a href='{}' target=_blank class=link_blu>{}</a>".format(original_url, label)
                        if match_result == Result.RESULT_CORRECT:
                            inner_text += "&nbsp; - <font class=link_blu>previously marked as correct</font>"
                        if result.note:
                            inner_text += "<font color=#999999 size=2px> - {}</font>".format(result.note)
                        inner_text += "<br></td></tr>"

                previous_atlas_result = result.atlas.get_bib_code()

            out_file.write(inner_text)

            out_file.write(HTML_FOOTERS)


    def create_jinja_report(self, filepath, until_element=None):
        """ creates the report through jinja engine """
        template_loader = jinja2.FileSystemLoader(searchpath=os.path.join(LOCAL_PATH, "./template"))
        template_env = jinja2.Environment(loader=template_loader)
        jinja_template = template_env.get_template("jinja_report.html")

        previous_atlas_result = ''
        bib_list = []
        bib_count = 0
        bib = {}

        if not until_element:
            until_element = len(self.results)

        for count, result in enumerate(self.results[0:until_element]):
            if previous_atlas_result != result.atlas.get_bib_code():
                if bib:
                    bib_list.append(bib)
                bib = {}
                bib_count += 1
                bib['seq'] = bib_count
                bib['code'] = result.atlas.get_bib_code()
                bib['title'] = result.atlas.get_title().decode("utf-8")
                bib['authors'] = result.atlas.get_atlas_authors().decode("utf-8")
                bib['file'] = result.atlas.get_source_bibfile()

                if result.get_result() != Result.RESULT_MISSING_LINK:
                    try:
                        if 'arxiv' not in result.original.get_url().lower():
                            bib['original_count'] = count
                            bib['original_result'] = result.get_result()
                            bib['original_code'], bib['original_label'] = self.create_code(result)
                            bib['original_code'] = bib['original_code'].decode("utf-8")
                            bib['original_label'] = bib['original_label'].decode("utf-8")
                            bib['original_title'] = result.original.get_title().decode("utf-8")
                            if result.get_result() != 'missing link':
                                bib['original_url'] = result.original.get_url()
                                bib['original_authors'] = result.original.get_authors()
                                bib['original_note'] = result.note

                    except:
                        print("[DEBUG] original count {}".format(count))
                        print("[DEBUG] original result {}".format(result.get_result()))
                        if 'original_code' in bib:
                            print("[DEBUG] original code {}".format(bib['original_code']))
                        if 'original_label' in bib:
                            print("[DEBUG] original label {}".format(bib['original_label']))
                        if 'original_title' in bib:
                            print("[DEBUG] original title {}".format(bib['original_title']))

            if result.get_result() != Result.RESULT_MISSING_LINK and result.original.get_url():
                if 'arxiv' in result.original.get_url().lower():
                    bib['arxiv_count'] = count
                    bib['arxiv_result'] = result.get_result()
                    if bib['arxiv_result'] != 'missing link':
                        bib['arxiv_code'], bib['arxiv_label'] = self.create_code(result)
                        bib['arxiv_title'] = result.original.get_title()
                        bib['arxiv_url'] = result.original.get_url()
                        bib['arxiv_authors'] = result.original.get_authors()
                        bib['arxiv_note'] = result.note


            previous_atlas_result = result.atlas.get_bib_code()
        bib_list.append(bib)

        report = {}
        report['ref_code'] = self.ref_code
        report['gitlab_link'] = self.ref_code_to_gitlab()

        # bib_list = [
        #     {'seq': '1', 'code': 'higgsino', 'title': 'this title'},
        #     {'seq': '2', 'code': 'replino', 'title': 'another title'},
        #     ]

        report['bibs'] = bib_list

        #print("[DEBUG] report = {}".format(report))

        jinja_output = jinja_template.render(data=report)

        try:
            with open(filepath, "w") as f_jinja:
                f_jinja.write(jinja_output.encode("utf-8"))
        except Exception, error:
            print("[ERROR] can't open jinja output for writing")
            print(error)

