#!/usr/bin/env python2

"""
handles retrieving metadatas from scipost website
"""

from OnlinePublication import OnlinePublication
from bs4 import BeautifulSoup

class Scipost(OnlinePublication):
    """
    Scipost publication class
    """

    def __init__(self, url=None, original_url=None):
        OnlinePublication.__init__(self, url, original_url)

    def compose_authors(self):
        """
        from a list of authors, compose the authors string
        """

        # authors come in the format
        # LastName(s), Name(s)
        self.authors = ""
        try:
            for author in self.authors_list:
                last_names, first_names = author.split(",")
                name_string = ""
                first_names = first_names.strip()
                for name in first_names.split(" "):
                    name_string += name[0] + ". "
                name_string += last_names
                self.authors += name_string + ", "

            self.authors = self.authors[0:-2]
        except:
            print("[ERROR] error while trying to parse scipost authors")
            print("        processin author {}".format(author))

    def fill_data(self):
        """
        loads all the needed information
        """
        self.load()
        self.get_meta('citation_title', 'citation_author')
        self.compose_authors()


"""
#scipost = Scipost("https://scipost.org/SciPostPhys.9.6.092")
#scipost = Scipost("https://scipost.org/10.21468/SciPostPhys.7.3.034")
#scipost = Scipost("https://scipost.org/SciPostPhys.10.2.026")
scipost = Scipost("https://scipost.org/SciPostPhys.10.1.023")
scipost.fill_data()
print("[DEBUG] title = {}".format(scipost.get_title().encode('utf-8')))
print("[DEBUG] authors list = {}".format(scipost.get_authors_list()))
print("[DEBUG] authors = {}".format(scipost.get_authors()))
"""
