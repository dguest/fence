# -*- coding: utf-8 -*-

import json
from parse_parens import *
from parse_titles import *

class Reference(object):

    def __init__(self):
        self.bib_code = None
        self.source = None
        self.title = ""
        self.url = None
        self.original_url = None
        self.authors = []
        self.atlas_authors = None
        self.journal = None
        self.volume = None
        self.year = None
        self.pages = None

        self.eprint = None

        self.primary_class = None

        self.label = None
        self.source_file = None

        self.debug_file = 'test-debug.json'

    def set_title(self, title):
        self.title = title

    def get_title(self):
        if not self.title:
            return ""
        return self.title

    def get_cleaned_title(self):
        cleaned_title = self.title

        if not cleaned_title:
            return ""

        clean_parens = ParseParens()
        cleaned_title = clean_parens.parser(cleaned_title)

        clean_titles = ParseDollars()
        cleaned_title = clean_titles.parser(cleaned_title)

        cleaned_title = cleaned_title.replace("</span>", "")
        cleaned_title = cleaned_title.replace("<span class=\"aps-inline-formula\">", "")
        cleaned_title = cleaned_title.replace("""<msqrt>""", "sqrt")
        cleaned_title = cleaned_title.replace("""<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline">""", "")
        cleaned_title = cleaned_title.replace("""</msqrt>""", "")
        cleaned_title = cleaned_title.replace("""<mi>""", "(")
        cleaned_title = cleaned_title.replace("""</mi>""", ")")
        cleaned_title = cleaned_title.replace("""<mo>""", "")
        cleaned_title = cleaned_title.replace("""</mo>""", "")
        cleaned_title = cleaned_title.replace("""<mn>""", "")
        cleaned_title = cleaned_title.replace("""</mn>""", "")
        cleaned_title = cleaned_title.replace("""<mtext>""", "")
        cleaned_title = cleaned_title.replace("""</mtext>""", "")
        cleaned_title = cleaned_title.replace("""</math>""", "")
        cleaned_title = cleaned_title.replace("""</span>""", "")
        cleaned_title = cleaned_title.replace("""fb^-1""", "fb-1")
        cleaned_title = cleaned_title.replace("−", "-")
        cleaned_title = cleaned_title.replace("–", "-")
        cleaned_title = cleaned_title.replace("--", "-")
        cleaned_title = cleaned_title.replace("~", " ")
        cleaned_title = cleaned_title.replace("sqrt(s)", "sqrts") # sometimes sqrt(s) is not inside a tag

        return cleaned_title

    def get_shrink_title(self):
        """
        runs on get_cleaned_title, but also removes whitespaces and makes the title lowercase
        """
        return self.get_cleaned_title().lower().replace(" ", "")


    def set_authors(self, authors):
        self.authors = authors

    def add_author(self, author):
        self.authors.append(author)

    def get_authors(self):
        return self.authors

    def set_source(self, source):
        self.source = source

    def get_source(self):
        return self.source

    def set_bib_source(self):
        self.source = "bibliography"

    def set_bib_code(self, bib_code):
        self.bib_code = bib_code

    def get_bib_code(self):
        return self.bib_code

    def set_url(self, url):
        self.url = url

    def get_url(self):
        return self.url

    def set_original_url(self, url):
        self.original_url = url

    def get_original_url(self):
        return self.original_url

    def set_journal(self, journal):
        self.journal = journal

    def get_journal(self):
        return self.journal or ""

    def set_volume(self, volume):
        self.volume = volume

    def get_volume(self):
        return self.volume or ""

    def set_year(self, year):
        self.year = year

    def get_year(self):
        return self.year or ""

    def set_pages(self, pages):
        self.pages = pages

    def get_pages(self):
        return self.pages or ""

    def set_eprint(self, eprint):
        self.eprint = eprint

    def get_eprint(self):
        return self.eprint or ""

    def set_primary_class(self, primary_class):
        self.primary_class = primary_class


    def get_label(self, arxiv=False):
        if arxiv:
            return self.eprint + " [" + self.primary_class + "]"

        if self.label:
            return self.label

        return self.get_journal() + " " \
               + self.get_volume() + " " \
               + self.get_pages() + " " \
               + "(" + self.get_year() + ") " \
               + self.get_pages()

    def get_atlas_authors(self):
        return self.atlas_authors

    def set_source_bibfile(self, filename):
        self.source_file = filename

    def get_source_bibfile(self):
        return self.source_file

    def set_atlas_authors(self, authors_list):
        """
        tries to follow convetion to convert list of authors in bib
        to expected result
        """
        if "ATLAS Collaboration" in authors_list:
            self.atlas_authors = "ATLAS Collaboration"
            return
        if "ATLAS" in authors_list:
            self.atlas_authors = authors_list
            return

        self.atlas_authors = ""

        authors = authors_list.split(" and ")

        max_authors = 3
        for index, author in enumerate(authors):
            author = author.strip()
            if (index == len(authors) -1 or index == max_authors) and len(authors) > 1:
                self.atlas_authors = self.atlas_authors[0:-2] + " and "
            if index > max_authors:
                break
            if "," in author:
                try:
                    last_name, names = author.split(",")
                    last_name = last_name.strip()
                    names = names.strip()
                    split_names = names.split(" ")
                    re_compose_name = ""
                    for name in split_names:
                        if len(name) > 2:
                            name = name[0] + ". "
                        if name.endswith("."):
                            name = name + " "
                        re_compose_name += name
                    self.atlas_authors += re_compose_name + last_name + ", "
                # TODO: must be fixed and not just bypassed, to check authors
                #       here we skip the problem of "bad" formatting in our bib files
                #       where we usually expect NAME, LAST_NAME and NAME, LAST NAME
                #       and instead we get NAME LAST_NAME, NAME LAST_NAME
                except:
                    print("[ERROR] author = {}".format(author))
                    print("\t bib code = {}".format(self.bib_code))
                    print("\t url = {}".format(self.url))
                    print("\t original url = {}".format(self.original_url))
                    pass
            elif "others" in author:
                if self.atlas_authors.endswith(", "):
                    self.atlas_authors = self.atlas_authors[0:-2]
                self.atlas_authors = self.atlas_authors.replace(" and ", "")
                self.atlas_authors += " et al."
                break
            else:
                if len(authors) > max_authors and not self.atlas_authors.endswith("et al."):
                    self.atlas_authors = author + " et al."
                    break
            if len(authors) > max_authors:
                self.atlas_authors = self.atlas_authors[0:-2] + " et al."
                break

        if self.atlas_authors.endswith(", "):
            self.atlas_authors = self.atlas_authors[0:-2]

        if not self.atlas_authors:
            self.atlas_authors = authors_list

    # NOTE:
    #       this method only to be used, by now, to collect bad titles
    #       not really used by the reference checker
    def compare_title(self, original):
        atlas_title = ""
        original_title = ""
        try:
            if self.get_title():
                atlas_title = self.get_title()
        except:
            return True
        try:
            if original.get_title():
                original_title = original.get_title()
        except:
            return True
        if not original_title:
            return True
        if atlas_title.lower().replace(" ", "") != original_title.lower().replace(" ", ""):
            print("[DEBUG - atlas vs original] \n{}\n{}".
                  format(atlas_title, original_title))
            return False

        return True


    def __str__(self):
        to_string = ""
        if self.bib_code:
            to_string += "Bibliography code: '{}'\n".format(self.bib_code)
        if self.source:
            to_string += "source: '{}'\n".format(self.source)
        if self.title:
            to_string += "title: '{}'\n".format(self.title)
        if self.url:
            to_string += "url: '{}'\n".format(self.url)
        if self.original_url:
            to_string += "original_url: '{}'\n".format(self.original_url)
        if self.authors:
            to_string += "authors: '{}'\n".format(self.authors)
        if self.atlas_authors:
            to_string += "atlas_authors: '{}'\n".format(self.atlas_authors)
        if self.journal:
            to_string += "journal: '{}'\n".format(self.journal)
        if self.volume:
            to_string += "volume: '{}'\n".format(self.volume)
        if self.year:
            to_string += "year: '{}'\n".format(self.year)
        if self.pages:
            to_string += "pages: '{}'\n".format(self.pages)
        if self.eprint:
            to_string += "eprint: '{}'\n".format(self.eprint)
        if self.primary_class:
            to_string += "primary_class: '{}'\n".format(self.primary_class)
        if self.label:
            to_string += "label: '{}'\n".format(self.label)

        return to_string
