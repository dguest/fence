#!/usr/bin/env python2
"""

author: Maurizio Colautti - maurizio.colautti@cern.ch

The purpose of the script is to retrive information on a document
stored in CDS
- 

"""

# import json
import requests
#import pylatexenc
#from pylatexenc.latex2text import LatexNodes2Text

class CDS(object):

    CDS_PREFIX = "https://cds.cern.ch/record/"

    def __init__(self, cds_id):
        # TODO: validate cds_id before assigning it
        self.cds_id = str(cds_id)
        self.content = None
        self.url = None

        self._compose_url(cds_id)

    def _compose_url(self, cds_id):
        if 'cern.ch' in cds_id:
            self.url = cds_id + "?of=recjson"
        else:
            self.url = CDS.CDS_PREFIX + self.cds_id + "?of=recjson"
        #print("[DEBUG] cds url = {}".format(self.url))

    def get_content(self):
        req = requests.get(self.url)
        #print("[INFO] automatic conversion? = \n{}".format(req.json()))
        #print("[INFO] encoding = {}".format(req.encoding))
        try:
            self.content = req.json()[0]
        except:
            print("[ERROR] can't retrieve information from cds for url {}".format(self.url))
        #self.content = json.loads(req.text)[0]
        #print("[INFO] cds content = {}".format(self.content))

    def get_title(self):
        #text = self.content['title']['title'].decode("utf-8")
        #print("[INFO] title = {}".format(LatexNodes2Text().latex_to_text(text)))
        #return LatexNodes2Text().latex_to_text(self.content['title']['title'])
        if self.content:
            if 'subtitle' in self.content['title']:
                if 'edition_statement' in self.content:
                    return self.content['title']['title'].encode("utf-8") + ": " + self.content['title']['subtitle'].encode("utf-8") + "; " + self.content['edition_statement'].encode("utf-8")
                else:
                    return self.content['title']['title'].encode("utf-8") + ": " + self.content['title']['subtitle'].encode("utf-8")

            return self.content['title']['title'].encode("utf-8")
        else:
            return ""
"""
cds = CDS("2280786")
cds.get_content()
print("[DEBUG] content = {}". format(cds.content))
print("[DEBUG] title = {}".format(cds.get_title()))
"""
