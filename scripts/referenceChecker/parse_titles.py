#!/bin/env python2


class ParseDollars(object):
    """
    class to parse content with dollars sign $ as delimiter of latex-type text
    """

    def __init__(self, debug=False):
        # in case we want to debug set to True
        self._debug = debug


    def debug(self, string, anyway=False):
        """
        will write debug output only if wanted
        """
        if self._debug or anyway:
            print(string)


    def parse(self, string, in_dollar=False, in_graphs=False, sub_level=0):
        """
        recursive function to evaluate a fraction of the string
        """
        in_string = ""
        counter = 0
        while True:

            code_continue = False

            if counter >= len(string):
                break

            if counter < len(string) -1:
                next_char = string[counter+1]
            else:
                next_char = ""

            if in_dollar:
                if string[counter] == '_' or string[counter] == '~':
                    counter += 1
                    continue

                for code in ["\\text{", "\\mathrm{", "\\ensuremath{", "\\mathcal{", "\\hbox{", "\\rm "]:
                    if string[counter:].startswith(code):
                        partial_string, increment = self.parse(string[counter+len(code):],
                                                               in_dollar=in_dollar,
                                                               in_graphs=True,
                                                               sub_level=sub_level+1)
                        in_string += partial_string
                        counter += increment + len(code)
                        counter += 1
                        code_continue = True
                        break

                # if we found one of the codes, we continue with next counter value
                if code_continue:
                    continue

                if string[counter] == '{':
                    partial_string, increment = self.parse(string[counter+len("{"):],
                                                           in_dollar=in_dollar,
                                                           in_graphs=True,
                                                           sub_level=sub_level+1)
                    in_string += partial_string
                    counter += increment + len("{")
                    counter += 1
                    continue

            if in_graphs:
                if string[counter] == '}':
                    return in_string, counter

            if string[counter] == '\\' and next_char in ['&', '%', '$', '#', '_', '{', '}', '^', '\\', '~']:
                in_string += next_char
                counter += 2
                continue

            if string[counter] == '\\':
                self.debug("[DEBUG] string starts with \\")
                partial_string, increment = self.parse(string[counter+1:],
                                                       in_dollar=in_dollar,
                                                       in_graphs=in_graphs,
                                                       sub_level=sub_level+1)
                in_string += partial_string
                counter += increment
            elif string[counter] == "$":
                self.debug("[DEBUG] string starts with $")
                partial_string, increment = self.parse(string[counter+1:],
                                                       in_dollar=True,
                                                       in_graphs=in_graphs,
                                                       sub_level=sub_level+1)
                in_string += partial_string
                counter += increment
            else:
                in_string += string[counter]

            counter += 1

        return in_string, counter


    def parser(self, string):
        """
        entry point for the class operations
        """
        string, _ = self.parse(string)

        return string


if __name__ == "__main__":
    STRING = "Test your string here"
    OBJ = ParseDollars()
    print("[DEBUG] string = {}".format(STRING))
    print("[DEBUG] parser = {}".format(OBJ.parser(STRING)))
