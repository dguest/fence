#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
handles retrieving metadatas from springer website
"""

from bs4 import BeautifulSoup
from OnlinePublication import OnlinePublication

class Springer(OnlinePublication):
    """
    Spginer online publication class
    """

    def __init__(self, url=None, original_url=None):
        OnlinePublication.__init__(self, url, original_url)

    def compose_authors(self):
        """
        from a list of authors, compose the authors string
        """
        self.authors = ""
        for author in self.authors_list:
            names = author.split(" ")
            name_string = ""
            try:
                for name in names[0:-1]:
                    name_string += name[0] + ". "
            except:
                print("[ERROR] error trying to extract name[0] for names = {}, author = {}".format(names, author))
            name_string += names[-1]
            self.authors += name_string + ", "

        self.authors = self.authors[0:-2]

        # TODO: HARDCODED-WORKAROUND, get rid off ABSOLUTELY
        if len(self.authors_list) > 10:
            if "Aad" in self.authors or "Abbott" in self.authors:
                self.authors = "ATLAS Collaboration"

    def get_meta(self, meta_title_tag, meta_author_tag):
        """
        overrides get_meta because in citation_title there are duplication of text in code
        """
        soup = BeautifulSoup(self.page_content, "lxml")
        metas = soup.find_all('meta')

        for meta in metas:
            if 'name' in meta.attrs and meta.attrs['name'] == meta_author_tag:
                self.authors_list.append(meta.attrs['content'].strip().encode('utf-8'))
            if 'name' in meta.attrs and meta.attrs['name'] == meta_title_tag:
                self.title = meta.attrs['content'].encode("utf-8")

        if not self.title:
            divs = soup.find_all('div')

            try:
                for div in divs:
                    if 'class' in div.attrs and 'c-context-bar__title' in div.attrs['class']:
                        self.title = div.text.encode("utf-8")
            except:
                raise

        if not self.title:
            metas = soup.find_all('meta')

            for meta in metas:
                if 'name' in meta.attrs and meta.attrs['name'] == 'dc.title':
                    self.title = meta.attrs['content'].encode("utf-8")

        self.title = (self.title.replace("&#xA;", "")
                      .replace("\n", "")
                      .replace(" ", " ")
                      .replace("\\text {", "\\text{")
                      .replace("\\mathrm {", "\\mathrm{")
                      .replace("\\hbox {", "\\hbox{")
                      .strip()) # not a whitespace!

    def fill_data(self):
        """
        loads all the needed information
        """
        self.load()
        self.get_meta('', 'dc.creator')
        self.compose_authors()


# springer = Springer("https://link.springer.com/article/10.1007/JHEP06(2018)108")
# springer.fill_data()
# print("[DEBUG] title = {}".format(springer.get_title()))
# print("[DEBUG] author list = {}".format(springer.get_authors_list()))
# print("[DEBUG] authors = {}".format(springer.get_authors()))
