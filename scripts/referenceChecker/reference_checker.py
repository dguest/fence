#!/usr/bin/env python2
"""

author: Maurizio Colautti - maurizio.colautti@cern.ch

The purpose of the script is to retrive the bibliographies for a paper
and check the references used in the article are correct:
- the link works
- the title is correct
- the authors are correct

sub processes:
a) identify the gitlab repository
b) retrieve all bib files to make one single bib container
c) retrieve all sections files
d) extract all the references used in the sections files
e) build the list of used references
f) load the verified references
g) substract f from e
h) test every entry from g
i) if entry is ok archive it
j) if entry is not ok, report

requires libraries:


DEVELOPER OPTIONS:
- running the script as user mcolautt will make the script to run as developer
- if len(arg.sys[]) last 2 args will be respictevely used as
  [-2] bib file to load
  [-1] bib code to check


"""

import sys
import re
import json
import time
import os
import requests
import gitlab
import traceback 
from pwd import getpwuid
from bib_parser import BibParser
#from pylatexenc.latex2text import LatexNodes2Text

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')

from POGitlab import POGitlab
from CDS import CDS
from arxiv import Arxiv
from springer import Springer
from aps import APS
from sciencedirect import ScienceDirect
from scipost import Scipost
from reference import Reference 
from result import *


SLOW = False
RESULTS = Results()
PATH_PREFIX = "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/references/"
WEB_PREFIX = "https://atlas.web.cern.ch/Atlas/GROUPS/PHYSOFFICE/references/"
VERIFIED_PATH = "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/references/reference_results.json"

PATH_PREFIX_EOS = "/eos/home-a/atlaspo/www/references/"
WEB_PREFIX_EOS = "https://atlaspo-eos.web.cern.ch/references/"
VERIFIED_PATH_EOS = "/eos/home-a/atlaspo/www/references/reference_results.json"

PATH_PREFIX_EOS_DEV = "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/references_dev/"
WEB_PREFIX_EOS_DEV = "https://atlas.web.cern.ch/Atlas/GROUPS/PHYSOFFICE/references_dev/"
VERIFIED_PATH_EOS_DEV = "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/references_dev/reference_results.json"

PATH_PREFIX_EOS_DEV = "/eos/home-a/atlaspo/www/references_dev/"
WEB_PREFIX_EOS_DEV = "https://atlaspo-eos.web.cern.ch/references_dev/"
VERIFIED_PATH_EOS_DEV = "/eos/home-a/atlaspo/www/references_dev/reference_results.json"

PATH_PREFIX_EOS = PATH_PREFIX
WEB_PREFIX_EOS = WEB_PREFIX
VERIFIED_PATH = VERIFIED_PATH

#VERIFIED_PATH = VERIFIED_PATH_EOS

def connect():
    """
    connects to the gitlab repo
    """
    #if len(sys.argv) > 1:
    #    return POGitlab(str(sys.argv[1]).upper())
    # TODO - just for fast testing, next line to be deleted
    return POGitlab(REF_CODE)
    # return None

def collect_bibs(repository):
    """
    # lists all files inside bib folder
    # for each file extract bibs and sum it up
    # returns a list of items through bib specific library
    """
    bib_files_list = repository.list_bib_files()
    bibs = BibParser()
    all_bibs = []
    file_content = ""
    for bib_file in bib_files_list:
        print("[INFO] Retrieving file {}".format(bib_file))
        file_content = repository.get_file(bib_file)
        bibliographies = bibs.parse(string=file_content, source_file=bib_file)
        all_bibs.extend(bibliographies)

    print("[INFO] number of bibs in bib files = {}".format(len(all_bibs)))
    return all_bibs

def extract_refs(file_content):
    """
    from the whole paper text extracts all the used references
    """
    raw_refs = re.findall(r'cite{([^}]*)}', file_content)
    all_refs = []
    for raw_ref in raw_refs:
        for ref in raw_ref.split(','):
            all_refs.append(ref.strip())

    return all_refs

def collect_refs(repository):
    """
    get all files into sections
    extracts all the refs
    """
    sections_files_list = repository.list_section_files()
    print("List of sections files = {}".format(sections_files_list))
    file_content = ""
    for section_file in sections_files_list:
        print("[INFO] Retrieving file {}".format(section_file))
        file_content += repository.get_file(section_file)

    refs = extract_refs(file_content)

    return refs

def detect_website(bib):
    """
    tries to detect the journal website for a publication 
    """
    #print("\n\n[INFO] bib = {}".format(bib))

    refs = []

    if 'url' in bib:
        if 'cds.cern.ch' in bib['url'] \
           or 'cdsweb.cern.ch' in bib['url']:
            refs.append(('cds', bib['url']))
        if 'books.google' in bib['url']:
            refs.append(('google', bib['url']))
        if 'doi.org' in bib['url']:
            refs.append(('doi', bib['url']))
    if 'doi' in bib:
        refs.append(('doi', "http://dx.doi.org/" + bib['doi']))
        #refs.append(('doi', 'ignored debugging DOI, but url can already be retrieved'))
        # TODO: some 'doi' might come with http://doi.org in front, should we report it?

    if 'archiveprefix' in bib:
        refs.append(('arxiv', "http://arxiv.org/abs/" + bib['eprint']))

    if not refs:
        print("""[WARN] Couldn't identify website to check for bibliography {}
      ... this will be shown in the report page""".format(bib))
        refs.append(('ERROR', 'ERROR'))

    return refs

def truncate_authors(authors):
    """
    tries to handle authors string for comparison and layout
    """
    try:
        and_tag = authors.find(" and ")
        return authors[0:and_tag] + " et al."
    except:
        try:
            first = authors.find(" ")
            second = authors[first+1:].find(" ")
            return authors[0:first+second+1] + " et al."
        except:
            return "ERROR undefined authors"

def resolve_doi(doi_url):
    """
    resolves doi urls
    """
    if "doi.org" in doi_url:
        req = requests.head(doi_url, allow_redirects=True)
    else:
        req = requests.head('http://www.doi.org/' + doi_url, allow_redirects=True)

    return req.url

def guess_journal(url):
    """
    tries to guess the journal from doi url
    """
    if "10.1140" in url.lower():
        return 'springer'
    if "10.1007" in url.lower():
        return 'springer'
    if "10.1103" in url.lower():
        return 'aps'
    if "10.1088" in url.lower():
        return 'iop'
    if "10.1016" in url.lower():
        return 'sciencedirect'
    if "10.21468" in url.lower():
        return 'scipost'


def validate_bibs(bibs_to_check, correct=[], error=[], debug=False):
    """
    the engine of validation for a list of bibliographies
    """

    MAX_AUTHORS_LENGTH = 170

    for bib in bibs_to_check:
        refs = detect_website(bib)

        for ref in refs:
            if ref:

                source_ref = ref[0]
                source_url = ref[1]


                atlas_bib = Reference()
                atlas_bib.set_bib_source()
                if 'id' in bib:
                    atlas_bib.set_bib_code(bib['id'])
                else:
                    continue
                atlas_bib.set_title(bib.get('title', ""))
                if len(bib.get('author', "")) > MAX_AUTHORS_LENGTH:
                    atlas_bib.set_authors(truncate_authors(bib['author']))
                else:
                    atlas_bib.set_authors(bib.get('author', ""))
                atlas_bib.set_journal(bib.get('journal', ""))
                atlas_bib.set_year(bib.get('year', ""))
                atlas_bib.set_volume(bib.get('volume', ""))
                atlas_bib.set_pages(bib.get('pages', ""))
                atlas_bib.set_eprint(bib.get('eprint', ""))
                atlas_bib.set_primary_class(bib.get('primaryclass', ""))
                atlas_bib.set_atlas_authors(bib.get('author', ""))
                atlas_bib.set_source_bibfile(bib.get('source_file', ""))

                if debug:
                    print("[DEBUG] validating bib = {}".format(bib))
                    print("[DEBUG] validating ref = {}".format(ref))
                    print("[DEBUG] atlas_bib title = {}".format(atlas_bib.title))

                if source_url == 'ERROR':
                    RESULTS.results.append(RESULTS.report_missing_url(atlas_bib))
                    continue


                original_bib = Reference()

                original_bib.set_source(source_ref)

                validated, validated_result = RESULTS.validated_check(atlas_bib, original_bib, correct, error)
                if validated and validated_result:
                    original_bib.set_url(source_url)
                    continue

                if source_ref == 'cds':
                    cds = CDS(source_url)
                    cds.get_content()

                    original_bib.set_source(source_ref)
                    original_bib.set_title(cds.get_title())
                    original_bib.set_url(source_url)
                    original_bib.set_original_url(source_url)

                    RESULTS.results.append(RESULTS.validate(atlas_bib, original_bib, validated_result, debug))
                elif source_ref == 'arxiv':
                    arxiv = Arxiv(source_url)
                    arxiv.fill_data()

                    original_bib.set_source(source_ref)
                    original_bib.set_title(arxiv.get_title())
                    original_bib.set_url(source_url)
                    original_bib.set_original_url(source_url)
                    original_bib.set_authors(arxiv.get_authors())

                    RESULTS.results.append(RESULTS.validate(atlas_bib, original_bib, validated_result, debug))
                elif source_ref == 'doi':
                    journal_guess = guess_journal(source_url)

                    if journal_guess == 'springer':
                        journal = Springer(source_url, source_url)
                    elif journal_guess == 'aps':
                        journal = APS(source_url, source_url)
                    elif journal_guess == 'sciencedirect':
                        journal = ScienceDirect(source_url, source_url)
                    elif journal_guess == 'scipost':
                        journal = Scipost(source_url, source_url)
                    else:
                        print("[WARN] this journal still tbd {}".format(source_url))
                        original_bib.set_source(source_ref)
                        original_bib.set_title("")
                        original_bib.set_url(source_url)
                        original_bib.set_original_url(source_url)

                        RESULTS.results.append(RESULTS.validate(atlas_bib, original_bib, validated_result, debug))
                        continue

                    if not journal.check_cached():
                        journal.url = resolve_doi(source_url)
                        print("[INFO] trying to resolve doi \n\t{}".format(source_url))
                        print("[INFO] doi resolved with url = {}".format(journal.url))

                    journal.fill_data()

                    original_bib.set_source(journal_guess)
                    original_bib.set_title(journal.get_title())
                    original_bib.set_url(source_url)
                    original_bib.set_original_url(source_url)
                    original_bib.set_authors(journal.get_authors())

                    RESULTS.results.append(RESULTS.validate(atlas_bib, original_bib, validated_result, debug))

                    """
                    if "epjc" in source_url or "JHEP" in source_url:
                        print("[INFO] resolving doi ")
                        journal, resolved_url = resolve_doi(source_url)
                        if journal == 'springer':
                            springer = Springer(resolved_url, source_original_url)
                            springer.fill_data()
                    """
                else:
                    print("[INFO] source not evaluated yet : {}".format(ref[0]))
                    original_bib.set_source(source_ref)
                    original_bib.set_title("")
                    original_bib.set_url(source_url)
                    original_bib.set_original_url(source_url)

                    RESULTS.results.append(RESULTS.validate(atlas_bib, original_bib, validated_result, debug))


                    """
                    #print("[INFO] cds = {}".format(cds.content))
                    #print("[INFO] bib = {}".format(LatexNodes2Text().latex_to_text(bib['title'])))
                    if cds.get_title() == bib['title']:
                        print("[WARN] bib and cds match")
                    else:
                        print("[WARN] ++++++ bib and cds don't match +++++")
                        print("[INFO] cds title = {}".format(cds.get_title()))
                        print("[INFO] bib title = {}".format(bib['title']))
                    """

def slow_me(seconds):
    if SLOW:
        time.sleep(seconds)





def collect_document(repository):
    """
    gets the tex document with subdocs
    # TODO: to be improved; including subdocuments handled by levels
            instead of recursively
    """
    if "ana" in REF_CODE.lower():
        file_content = repository.get_file(REF_CODE + "-PAPER.tex")
    else:
        file_content = repository.get_file("ANA-" + REF_CODE + "-PAPER.tex")

    paper_content = ""
    for line in file_content.splitlines():
        paper_content += line + "\n"
        if line.lower().startswith("\\input{"):
            if "metadata" in line:
                continue
            file_name = line[line.find("{")+1:line.find("}")]
            if not file_name.endswith(".tex"):
                file_name += ".tex"
            print("[INFO] Retrieving file {}".format(file_name))
            paper_content += repository.get_file(file_name)

    full_paper_content = ""
    for line in paper_content.splitlines():
        if line.lower().startswith("\\input{"):
            file_name = line[line.find("{")+1:line.find("}")]
            if ".tex" not in file_name.lower():
                file_name += ".tex"
            aux_file_content = repository.get_file(file_name)
            full_paper_content += aux_file_content + "\n"
            print("[INFO] Retrieving file {}".format(file_name))
        else:
            full_paper_content += line + "\n"

    third_paper_content = ""
    for line in full_paper_content.splitlines():
        if line.lower().startswith("\\input{"):
            file_name = line[line.find("{")+1:line.find("}")]
            if ".tex" not in file_name.lower():
                file_name += ".tex"
            aux_file_content = repository.get_file(file_name)
            third_paper_content += aux_file_content + "\n"
            print("[INFO] Retrieving file {}".format(file_name))
        else:
            third_paper_content += line + "\n"

    content = ""
    for line in third_paper_content.splitlines():
        if not line.startswith("%"):
            content += line + "\n"

    refs = extract_refs(content)

    return refs


def get_result_filename(ref_code, path_prefix=PATH_PREFIX, web_prefix=WEB_PREFIX):
    """
    check on the result path to have a correct namefile
    """
    prefix = ""
    number = 0
    while True:
        name_file = os.path.join(path_prefix,
                                 ref_code + prefix + ".html"
                                )
        if not os.path.isfile(name_file):
            return name_file, name_file.replace(path_prefix, web_prefix)
        number += 1
        prefix = "_" + str(number)

def create_summary_page(path_prefix=PATH_PREFIX):
    """
    create the summary page ordering (by now) by last execution time
    """
    items = []
    for file_name in os.listdir(path_prefix):
        if file_name.endswith(".html"):
            if file_name == 'index.html':
                continue
            if "jinja" in file_name:
                continue
            ref_code = file_name[0:12]
            items.append(
                (ref_code,
                 file_name,
                 os.path.getctime(os.path.join(path_prefix, file_name)),
                 getpwuid(os.stat(os.path.join(path_prefix, file_name)).st_uid).pw_name
                )
                )
    items = sorted(items, key=lambda x: x[2], reverse=True)
    html = """<html>
    <head>
        <title>Reference checker runs</title>
    </head>
    <body>
        <center><h3>Here you can find the last reports generated by the reference checker</h3></center>

        <ul>"""
    for item in items:
        html += "<li><a href="
        #html += WEB_PREFIX
        html += item[1]
        html += ">" + item[0] + "</a>"
        html += " run on " + time.ctime(item[2])
        #html += " by user " + item[3]
        html += "</li>\n"
    html += """        </ul>
    </body>
</html>"""

    with open(os.path.join(path_prefix, "index.html"), "w") as out_f:
        out_f.write(html)

def collect_all_bibs(repository):
    """
    provides all the bibliographies to check
    """
    print("[STATUS] -- Retrieving and parsing the bibliography content")
    all_bibs = collect_bibs(repository)

    # collect refs
    print("[STATUS] -- Collecting all the paper sections")
    slow_me(1)
    #used_bibs = collect_refs(repository)
    used_bibs = collect_document(repository)

    all_bib_titles = []
    for all_bib in all_bibs:
        if 'id' in all_bib:
            all_bib_titles.append(all_bib['id'])


    print("[STATUS] -- Building list of used references")
    slow_me(1)
    # build list to check (1)
    counter = 0
    used_list = []
    for used_bib in used_bibs:
        if used_bib in all_bib_titles:
            if used_bib not in used_list:
                used_list.append(used_bib)
                counter += 1

    bibs_to_check = []
    for used_bib in used_list:
        for all_bib in all_bibs:
            if all_bib['id'] == used_bib:
                if all_bib not in bibs_to_check:
                    bibs_to_check.append(all_bib)
                    break

    return bibs_to_check


def main(path_prefix=PATH_PREFIX, web_prefix=WEB_PREFIX):
    """
    where it all starts...
    """
    print("Execution starts")

    # load verified items
    verified_correct = []
    verified_error = []
    print("[DEBUG] opening file {}".format(VERIFIED_PATH))
    with open(VERIFIED_PATH) as json_file:
        data = json.load(json_file)
        for element in data['correct']:
            item = json.loads(element)
            verified_correct.append(item['code'])
        for element in data['error']:
            item = json.loads(element)
            verified_error.append(item['code'])

    # loads gitlab repo
    try:
        repository = connect()
    except gitlab.exceptions.GitlabGetError:
        print("[ERROR] Can't connect to the repository, please check provided reference code = {}"
              .format(REF_CODE))
        return

    bibs_to_check = collect_all_bibs(repository)
    print("[INFO] number of bibliographies to check = {}".format(len(bibs_to_check)))

    # validate them effectively
    print("[STATUS] -- Validating bibliographies")
    validate_bibs(bibs_to_check, verified_correct, verified_error)

    # create report
    print("[STATUS] -- Generating report")
    slow_me(1)

    # creates the result page
    name_file, web_file = get_result_filename(REF_CODE, path_prefix=path_prefix, web_prefix=web_prefix)
    RESULTS.set_ref_code(REF_CODE)
    #RESULTS.create_report(name_file)
    print("Saving report on page:\n{}".format(web_file))
    RESULTS.create_jinja_report(name_file)

    # creates the summary page
    create_summary_page(path_prefix)
    print("You can access the list of runs here:\n{}".format(web_prefix))

    return web_file


def db_collect_bibs(repository, debug_bib_file):
    bib_files_list = repository.list_bib_files()
    bibs = BibParser()
    all_bibs = []
    file_content = ""
    for bib_file in bib_files_list:
        if bib_file != debug_bib_file:
            continue
        print("[INFO] Retrieving file {}".format(bib_file))
        file_content = repository.get_file(bib_file)
        bibliographies = bibs.parse(string=file_content, source_file=bib_file)
        all_bibs.extend(bibliographies)

    print("[DEBUG] all_bibs count number = {}".format(len(all_bibs)))
    return all_bibs



def db_collect_specific_bib(repository, debug_bib_file, debug_bib_code):
    """
    will retrieve only one bibliography for fast checks
    """

    print("[STATUS] -- Retrieving {} bib file".format(debug_bib_file))
    file_bibs = db_collect_bibs(repository, debug_bib_file)

    for bib in file_bibs:
        if 'id' in bib:
            if bib['id'] == debug_bib_code:
                return [bib]

    #return file_bibs



def main_debug(debug_bib_file='', debug_bib_code='', path_prefix=PATH_PREFIX_EOS_DEV, web_prefix=WEB_PREFIX_EOS_DEV):
    """
    main, but for runs under development environment, which matches with user mcolautt
    """
    print("Execution starts")

    # load verified items
    verified_correct = []
    verified_error = []
    print("[DEBUG] opening file {}".format(VERIFIED_PATH))
    with open(VERIFIED_PATH) as json_file:
        data = json.load(json_file)
        for element in data['correct']:
            item = json.loads(element)
            verified_correct.append(item['code'])
        for element in data['error']:
            item = json.loads(element)
            verified_error.append(item['code'])

    # loads gitlab repo
    try:
        repository = connect()
    except gitlab.exceptions.GitlabGetError:
        print("[ERROR] Can't connect to the repository, please check provided reference code = {}"
              .format(REF_CODE))
        return

    if debug_bib_file and debug_bib_code:
        bib_to_check = db_collect_specific_bib(repository, debug_bib_file, debug_bib_code)
    else:
        bib_to_check = collect_all_bibs(repository)

    #print("[DEBUG] bib to check = {}".format(bib_to_check))

    # validate them effectively
    print("[STATUS] -- Validating bibliographies")
    validate_bibs(bib_to_check, verified_correct, debug=True)

    # create report
    print("[STATUS] -- Generating report")
    slow_me(1)

    # creates the result page
    name_file, web_file = get_result_filename(REF_CODE, path_prefix, web_prefix)
    RESULTS.set_ref_code(REF_CODE)
    #RESULTS.create_report(name_file)
    print("Saving report on page:\n{}".format(web_file))
    RESULTS.create_report(name_file)
    RESULTS.create_jinja_report(name_file)

    # creates the summary page
    create_summary_page()
    print("You can access the list of runs here:\n{}".format(WEB_PREFIX_EOS_DEV))


INTRO_MSG = """
************************************************************************
------                  Reference Checker                         ------

If you need assistance running the script, please check readme file
For further info, you can visit:
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ReferenceChecker


PLEASE NOTE:
If this is the FIRST TIME you run the script, you might need to ask 
read and write access in order to run the script without errors.
If so, and for any other issue, contact maurizio.colautti@cern.ch

******************************      ************************************
"""


if __name__ == '__main__':

    print(INTRO_MSG)

    # options to run as developer
    RUN_AS_DEV = False

    # if user is mcolautt => run as developer
    try:
        if os.getlogin() == 'mcolautt':
            RUN_AS_DEV = True
    except:
        pass

    # if no ref code is supplied, exit
    if len(sys.argv) < 2:
        print("[ERROR] You must provide a reference code")
        print("\tRead readme file or visit \n\thttps://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ReferenceChecker\n")
        create_summary_page(PATH_PREFIX)
    else:
        # ref code must be the first argument
        REF_CODE = str(sys.argv[1]).upper()
        EMAIL_USER = ""

        if "#" in REF_CODE:
            REF_CODE, EMAIL_USER = REF_CODE.split("#")

        print("[INFO] Reference code = {}".format(REF_CODE))
        if EMAIL_USER:
            print("[INFO] Email user = {}".format(EMAIL_USER))

        # if running as developer, switch to developer settings
        if RUN_AS_DEV:
            print("[INFO] running on DEVELOPMENT area\n")
            PATH_PREFIX = PATH_PREFIX_EOS_DEV
            WEB_PREFIX = WEB_PREFIX_EOS_DEV
            VERIFIED_PATH = VERIFIED_PATH_EOS_DEV
        #else:
        # if regular run, send email to maurizio to monitor runs
        TEXT = "From: atlaspo@cern.ch\n"
        TEXT += "To: maurizio.colautti@cern.ch\n"
        TEXT += "Subject: Reference checker run: \n\n"
        try:
            TEXT += "from user {} through command {}".format(os.getlogin(), sys.argv)
        except:
            TEXT += "from user {} through command {}".format(EMAIL_USER, sys.argv)


        _PIPE = os.popen("/usr/sbin/sendmail -t -i", "w")
        _PIPE.write(TEXT)
        STATUS = _PIPE.close()
        # execute script

        if len(sys.argv) > 3:
            print("[DEBUG] bib file to load = {}".format(sys.argv[-2]))
            print("[DEBUG] bib code to check = {}".format(sys.argv[-1]))
            main_debug(debug_bib_file=sys.argv[-2], debug_bib_code=sys.argv[-1])
        else:
            if RUN_AS_DEV:
                main_debug(path_prefix=PATH_PREFIX_EOS_DEV, web_prefix=WEB_PREFIX_EOS_DEV)
            else:
                try:
                    webpage_result = main()
                    TEXT = "From: mcolautt@cern.ch\n"
                    TEXT += "To: {}\n".format(EMAIL_USER)
                    TEXT += "Subject: Reference checker {} report: \n\n".format(REF_CODE)
                    TEXT += "The report you requested is here: {}\n".format(webpage_result)
                    _PIPE = os.popen("/usr/sbin/sendmail -t -i", "w")
                    _PIPE.write(TEXT)
                    STATUS = _PIPE.close()
                except Exception as err:
                    print("exception ERROR {}".format(str(err)))
                    TEXT = "From: mcolautt@cern.ch\n"
                    TEXT += "To: maurizio.colautti@cern.ch\n"
                    TEXT += "Subject: Reference checker ERROR: \n\n"
                    TEXT += "through command {}\n".format(sys.argv)
                    TEXT += "with error {}\n".format(str(err))
                    TEXT += "with error {}\n"
                    TEXT += traceback.format_exc()

                    _PIPE = os.popen("/usr/sbin/sendmail -t -i", "w")
                    _PIPE.write(TEXT)
                    STATUS = _PIPE.close()

                    TEXT = "From: mcolautt@cern.ch\n"
                    TEXT += "To: {}\n".format("EMAIL_USER")
                    TEXT += "Subject: ERROR running RC on {}: \n\n".format(REF_CODE)
                    TEXT += "An error has occurred while trying to run the reference checker on {}\n".format(REF_CODE)
                    TEXT += "I have been sent an email with the specific error \n"
                    TEXT += "I am going to investigate it and then I'll be back to you as soon as possible \n"

                    _PIPE = os.popen("/usr/sbin/sendmail -t -i", "w")
                    _PIPE.write(TEXT)
                    STATUS = _PIPE.close()






"""
gtl = POGitlab(ref_code)
gtl.get_file("ANA-SUSY-2018-23-PAPER.bib")
print(gtl.list_files(path='bib'))



gtl = POGitlab("ANA-SUSY-2018-23")
gtl.get_file("ANA-SUSY-2018-23-PAPER.bib")
gtl.list()
print(gtl.list_folders())
gtl.get_file("bib/ATLAS-errata.bib")
"""
