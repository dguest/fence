<!-- to be copied on output webservice path on ATLAS -->

<?php
    $id = $_POST['id'];
    $outcome = $_POST['outcome'];
    $title = $_POST['title'];
    $authors = $_POST['authors'];
    $code = $_POST['code'];
    $user = $_POST['user'];
    $return_string = "";
    // get JSON content
    $json = file_get_contents("reference_results.json");
    $verifiedItems = json_decode($json, true);
    // check if changes are needed into the JSON

    $correct_items = array();
    foreach ($verifiedItems['correct'] as $item => $item_code) {
        $json_item = json_decode($item_code); 
        array_push($correct_items, $json_item->{"code"});
    }
    $error_items = array();
    foreach ($verifiedItems['error'] as $item => $item_code) {
        $json_item = json_decode($item_code);
        array_push($error_items, $json_item->{'code'});
    }

    if ($outcome == 'correct'){
        if (in_array($code, $correct_items)){
            $return_string = "CORRECT record already present, nothing to do";
        }else{
            $return_string = "CORRECT record inserted";
            // array_push($verifiedItems['correct'],$code);
            $entry->code = $code;
            $entry->authors = $authors;
            $entry->title = $title;
            $json_entry = json_encode($entry);
            echo $json_entry;
            array_push($verifiedItems['correct'], $json_entry);
        }
    } else if ($outcome == 'error'){
        if (in_array($code, $error_items)){
            $return_string = "ERROR record already present, nothing to do";
        }else{
            $return_string = "ERROR record inserted";
            $entry->code = $code;
            $entry->authors = $authors;
            $entry->title = $title;
            $json_entry = json_encode($entry);
            echo $json_entry;
            array_push($verifiedItems['error'], $json_entry);
        }
    }
    // composing new JSON 
    $result = array(
        "correct" => $verifiedItems['correct'],
        "error" => $verifiedItems['error'],
    );
    // saving to file
    $newJsonString = json_encode($result);
    file_put_contents('reference_results.json', $newJsonString);
    echo $newJsonString;
    echo "\n\r";
    echo $return_string;
?>