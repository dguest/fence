#!/bin/env python2

"""
developer class to analyze differences in matches
"""

import json
import re

class BadResults(object):
    """
    need a way to check how to improve the titles match
    cleaning the code
    and authors match
    """

    def __init__(self):

        self.name_file = "test-debug.json"

        with open(self.name_file) as json_file:
            self.bad_results = json.load(json_file)

    def append_title(self, cat, bib_code, atlas, original):
        """
        appends a title (reduntant cat parameter? tbd)
        if not already in list
        """
        for item in self.bad_results:
            if bib_code in item:
                if item[bib_code]['cat'] == cat:
                    return
        self.bad_results.append({bib_code : {'cat': cat, 'atlas' : atlas.encode("utf-8"), 'original' : original.encode("utf-8")}})

    def update(self):
        """
        saves the bad_results on file
        could make it run when the object is destroyed?
        """
        with open(self.name_file, "w+") as json_file:
            json.dump(self.bad_results, json_file)


def clean_dollars_code(cleaned_title, dollars):
    """
    clean all generic code between two $
    """
    try:
        if len(dollars) % 2 == 0:
            it = iter(dollars)
            positions = zip(it, it)
            #print("[DEBUG] positions = {}".format(positions))
            print("[DEBUG] title = {}".format(cleaned_title))

            for position in positions:
                print("[DEBUG] between 2 dollars = {}".format(cleaned_title[position[0]:position[1]+1]))

        else:
            print("[ERROR] title has an odd number of dollars, can't decode (yet?)")
    except:
        pass

def clean_code(cleaned_title):
    # TODO: send a message to debug -> why not cleaned_title
    if not cleaned_title:
        return ""

    dollars = [m.start() for m in re.finditer(re.escape("$"), cleaned_title)]
    #print("[DEBUG] length = {} and dollars positions = {}".format(len(dollars), dollars))

    clean_dollars_code(cleaned_title, dollars)

    if len(dollars) >= 2:
        return cleaned_title[dollars[0]:dollars[1]+1]

    #cleaned_title = cleaned_title.replace(" ", "")
    cleaned_title = cleaned_title.replace("{s}", "(s)")
    cleaned_title = cleaned_title.replace("\\(\\", "")
    cleaned_title = cleaned_title.replace("\\(", "")
    cleaned_title = cleaned_title.replace("\\)", "")
    cleaned_title = cleaned_title.replace("\\,\\text{", "")
    cleaned_title = cleaned_title.replace("$\\", "")
    cleaned_title = cleaned_title.replace("{\\mathrm", "")
    cleaned_title = cleaned_title.replace("\\ \\mathrm{", "")
    cleaned_title = cleaned_title.replace("}$", "")
    cleaned_title = cleaned_title.replace("\\mbox{", "")
    cleaned_title = cleaned_title.replace("$", "")
    cleaned_title = cleaned_title.replace("\\text{", "")
    cleaned_title = cleaned_title.replace("}\\)", "")
    cleaned_title = cleaned_title.replace("}", "")
    cleaned_title = cleaned_title.replace("{", "")
    cleaned_title = cleaned_title.replace("--", "-")
    cleaned_title = cleaned_title.replace("\\textsc", "")
    cleaned_title = cleaned_title.replace("<span class=\"aps-inline-formula\">", "")
    cleaned_title = cleaned_title.replace("</span>", "")
    cleaned_title = cleaned_title.replace("""<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline">""", "")
    cleaned_title = cleaned_title.replace("""<msqrt>""", "sqrt")
    cleaned_title = cleaned_title.replace("""</msqrt>""", "")
    cleaned_title = cleaned_title.replace("""<mi>""", "(")
    cleaned_title = cleaned_title.replace("""</mi>""", ")")
    cleaned_title = cleaned_title.replace("""<mo>""", "")
    cleaned_title = cleaned_title.replace("""</mo>""", "")
    cleaned_title = cleaned_title.replace("""<mn>""", "")
    cleaned_title = cleaned_title.replace("""</mn>""", "")
    cleaned_title = cleaned_title.replace("""<mtext>""", "")
    cleaned_title = cleaned_title.replace("""</mtext>""", "")
    cleaned_title = cleaned_title.replace("""</math>""", "")
    cleaned_title = cleaned_title.replace("""</span>""", "")
    cleaned_title = cleaned_title.replace("\\ensuremath", "")
    cleaned_title = cleaned_title.replace("\\mathrm", "")
    cleaned_title = cleaned_title.replace("s=13sqrt(s)", "sqrt(s)")
    cleaned_title = cleaned_title.replace("s=7sqrt(s)", "sqrt(s)")
    #cleaned_title = cleaned_title.lower()

    return cleaned_title



if __name__ == "__main__":
    bad = BadResults()
    success = 0
    for index, item in enumerate(bad.bad_results):
        key = item.keys()[0]
        clean_code(item[key]['atlas'])
        clean_code(item[key]['original'])
        continue
        #if clean_code(item[key]['atlas']) != clean_code(item[key]['original']):
        if True:
            try:
                print()
                print("[DEBUG - a] {}".format(item[key]['atlas']))
            except:
                pass
            try:
                print("[DEBUG - a] {}".format(clean_code(item[key]['atlas'])))
            except:
                pass
            try:
                print("[DEBUG - o] {}".format(clean_code(item[key]['original'])))
            except:
                pass
            try:
                print("[DEBUG - o] {}".format(item[key]['original']))
            except:
                pass
        else:
            print("[INFO] title match")
            success +=1

    print("[INFO] success = {} out of {}".format(success, len(bad.bad_results)))


