#!/usr/bin/env python2

"""
handles retrieving metadatas from aps website
"""

from OnlinePublication import OnlinePublication
from bs4 import BeautifulSoup

class APS(OnlinePublication):
    """
    APS online publication class
    """

    def __init__(self, url=None, original_url=None):
        OnlinePublication.__init__(self, url, original_url)

    def compose_authors(self):
        """
        from a list of authors, compose the authors string
        """

        # authors come in the format
        # InitialName(s). LastName(s)
        # TO VERIFY, not sure when just few authors
        self.authors = ""
        for author in self.authors_list:
            names = author.split(" ")
            name_string = ""
            for name in names[0:-1]:
                name_string += name[0] + ". "
            name_string += names[-1]
            self.authors += name_string + ", "

        self.authors = self.authors[0:-2]

    def get_head_title(self):
        """
        title to be treated differently than usual
        """
        soup = BeautifulSoup(self.page_content, "lxml")
        titles = soup.find_all('title')

        for title in titles:
            title_tag_text = title.text.encode("utf-8")

        if "  -  " in title_tag_text:
            self.title = title_tag_text.split("  -  ")[1]


    def fill_data(self):
        """
        loads all the needed information
        """
        self.load()
        self.get_meta('citation_title', 'citation_author')
        self.get_head_title()
        self.compose_authors()

"""
aps = APS("https://journals.aps.org/prd/abstract/10.1103/PhysRevD.96.072002")
aps.fill_data()
print("[DEBUG] title = {}".format(aps.get_title().encode('utf-8')))
print("[DEBUG] authors list = {}".format(aps.get_authors_list()))
print("[DEBUG] authors = {}".format(aps.get_authors()))
"""
