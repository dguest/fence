#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
reads and loads in a list of diciontaries
all the relevant (not online/manual) bibliographies
from file
"""

import json

class BibParser(object):

    def __init__(self):
        pass

    def parse(self, namefile=None, string=None, source_file=None):
        """
        from a namefile, or string,
        extracts all the bibliographies and returns them as an array of dicts
        """
        all_bibs = []

        content = ""

        if namefile:
            with open(namefile, "r") as in_file:
                content = in_file.read()
        elif string:
            content = string
            #content = string.replace("\xe2\x80\x93", "-")

        if not content:
            return []

        in_bib = False
        counter = 0
        #print("[DEBUG] content = {}".format(content[0:1000]))
        tag = ""
        tag_open = False
        value_open = ""
        for line in content.splitlines():
            line = line.strip()
            # skip comment lines
            if line and line[0] == '%':
                continue
            # check new bib start
            if line and line[0] == '@':
                if "@online" in line.lower() \
                   or "@manual" in line.lower():
                    continue
                if "{" in line:
                    if "," in line:
                        # we try to see if we have a bib which was not close before
                        try:
                            if in_bib and bib_parse:
                                all_bibs.append(bib_parse)
                        except:
                            pass
                        bib_id = line[line.find("{")+1:line.rfind(",")]
                        bib_parse = {}
                        bib_parse['id'] = bib_id
                        in_bib = True
                        counter += 1
                        continue
            # check bib end
            #if in_bib and line and "=" in line and "in proton-proton collisions at $\\sqrt{s}$" not in line:
            if in_bib and line and "=" in line and not tag_open:
                tag, value = line.split("=", 1)
                tag = tag.strip().lower()
                tag = tag.lstrip("\"{")
                tag = tag.rstrip("}\"")
                value = value.strip()
                value = value.rstrip(",")
                value = value.rstrip()
                try:
                    value_open = value[0]
                except:
                    print("[ERROR] parsing line: {}".format(line))
                #print("[DEBUG] - the value opens with : {}".format(value_open))
                if value_open == "\"":
                    if value.endswith("\"") or value.endswith("\","):
                        #print("[DEBUG] - the value closes")
                        tag_open = False
                    else:
                        tag_open = True
                if value_open == "{":
                    if value.endswith("}") or value.endswith("},"):
                        #print("[DEBUG] - the value closes")
                        tag_open = False
                    else:
                        tag_open = True
                value = value.strip("\"{")
                value = value.rstrip("}\"")
                bib_parse[tag] = value
            # authors separated by newlines
            #elif in_bib and line and (tag == 'author' or tag == 'title'):
            elif in_bib and line and tag_open:
                value = line.strip()
                if value_open == "\"":
                    if value.endswith("\"") or value.endswith("\","):
                        #print("[DEBUG] - the value closes")
                        tag_open = False
                    else:
                        tag_open = True
                if value_open == "{":
                    if value.endswith("}") or value.endswith("},"):
                        #print("[DEBUG] - the value closes")
                        tag_open = False
                    else:
                        tag_open = True
                value = value.rstrip(",")
                value = value.rstrip()
                value = value.strip("\"{")
                value = value.rstrip("}\"")
                if tag in bib_parse:
                    bib_parse[tag] = bib_parse[tag] + " " + value
                else:
                    bib_parse[tag] = value
            if line and line[0] == '}':
                try:
                    if source_file:
                        bib_parse['source_file'] = source_file
                    if bib_parse:
                        all_bibs.append(bib_parse)
                except UnboundLocalError:
                    pass
                    #print("[ERROR] trying to parse content from file = \n{}".format(source_file))
                in_bib = False

        return all_bibs

class VerifiedEntries(object):

    def __init__(self):

        self.filename = "/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/referenceChecker/verified.json"

        self.entries = {}

    def load_entries(self):
        """
        loads all the verified entries
        """
        with open(self.filename, "r") as in_file:
            self.entries = json.load(in_file)

        #print("[DEBUG] entries = {}".format(self.entries))

    def add_entry(self, entry_name, entry_dict):
        """
        add one entry in the self.entries dictionary
        """

        """
        entry_name = "test03"
        entry_dict = {
            "url":"http://www.test.me",
            "title":"title test",
            "authors":"authors_test",
            "note":"here notes"
            }
        if entry_name:
            entry_dict = {
                "url":"http://www.testmeforever.me",
                "title":"title test",
                "authors":"authors_test",
                "note":"here notes"
                }
        """

        if entry_name in self.entries:
            del self.entries[entry_name]

        self.entries[entry_name] = entry_dict

        self.update_entries_file()

    def update_entries_file(self):
        """
        updates the entries files
        """
        with open(self.filename, "w") as out_file:
            json.dump(self.entries, out_file, indent=4)

    def verified(self, entry_id):
        """
        
        """


    def print_entries(self):
        """
        just for debug, prints the entries
        """
        print("[DEBUG] entries = {}".format(self.entries))

"""

test = VerifiedEntries()
test.load_entries()
test.print_entries()
test.add_entry("test03", {
    "url":"http://www.test.me",
    "title":"title test",
    "authors":"authors_test",
    "note":"here notes"
    })
test.print_entries()

"""
"""
bibP = BibParser()

bib_list = bibP.parse("output.txt")

print(len(bib_list))

"""
