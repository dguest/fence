#!/usr/bin/env python2

"""
handles retrieving metadatas from sciencedirect website
"""

from OnlinePublication import OnlinePublication
from bs4 import BeautifulSoup

class ScienceDirect(OnlinePublication):
    """
    ScienceDirect online publication class
    """

    def __init__(self, url=None, original_url=None):
        OnlinePublication.__init__(self, url, original_url)

    def get_meta(self):
        """
        overrides get_meta because of different location of authors in webpage
        """
        soup = BeautifulSoup(self.page_content, "lxml")
        metas = soup.find_all('meta')

        for meta in metas:
            if 'name' in meta.attrs and meta.attrs['name'] == 'citation_title':
                self.title = meta.attrs['content'].encode("utf-8")

        spans = soup.find_all('span')

        name = ""
        surname = ""
        for span in spans:
            if 'class' in span.attrs and 'text' in span.attrs['class'] and 'given-name' in span.attrs['class']:
                name = span.text
            if 'class' in span.attrs and 'text' in span.attrs['class'] and 'surname' in span.attrs['class']:
                surname = span.text
            if name and surname:
                self.authors_list.append(surname + ", " + name)
                name = ""
                surname = ""

    def compose_authors(self, max_authors=None):
        """
        from a list of authors, compose the authors string
        """
        self.authors = ""
        for counter, author in enumerate(self.authors_list):
            if max_authors and counter >= max_authors:
                self.authors = self.authors[0:-2] + " et al."
                return
            names = author.split(",")
            name_string = ""
            first_names = names[1].strip().split()
            for name in first_names:
                if not name.endswith("."):
                    name_string += name[0] + ". "
                else:
                    name_string += name + " "
            name_string += names[0]
            self.authors += name_string + ", "

        self.authors = self.authors[0:-2]

    def fill_data(self):
        """
        loads all the needed information
        """

        if "elsevier.com" in self.url:
            self.url = self.url.replace("https://linkinghub.elsevier.com/retrieve/pii/",
                                        "https://www.sciencedirect.com/science/article/pii/")

        self.load()

        """
        soup = BeautifulSoup(self.page_content, "lxml")
        metas = soup.find_all('meta')

        print("[DEBUG] page content = {}".format(self.page_content.encode("UTF8")))

        for meta in metas:
            print("[DEBUG] meta attrs = {}".format(meta))
            print("[DEBUG] meta attrs = {}".format(meta.attrs))
            if 'http-equiv' in meta.attrs and meta.attrs['http-equiv'] == 'REFRESH':
                print("[DEBUG] content = {}".format(meta.attrs['content']))
                start = meta.attrs['content'].find("Redirect=") + len("Redirect=")
                end = meta.attrs['content'].rfind("'")
                link = meta.attrs['content'][start:end]
                link = urllib.unquote(link)
                print("[DEBUG] extract link = {}".format(link))
        """

        self.get_meta()
        self.compose_authors()

"""
iop = ScienceDirect("https://linkinghub.elsevier.com/retrieve/pii/S0370269306010094")
#iop = ScienceDirect("https://www.sciencedirect.com/science/article/pii/S0370269306010094?via%3Dihub&key=e43251ee85773a46704b4c3d4c8489c0365b704e")
#iop = ScienceDirect("https://www.sciencedirect.com/science/article/abs/pii/S0550321312005500?via%3Dihub")
iop.fill_data()
print("[DEBUG] title = {}".format(iop.get_title()))
print("[DEBUG] author list = {}".format(iop.get_authors_list()))
print("[DEBUG] authors = {}".format(iop.get_authors()))
"""
