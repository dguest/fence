#!/usr/bin/env python2

"""
handles retrieving metadatas from arxiv website
"""

from OnlinePublication import OnlinePublication

class Arxiv(OnlinePublication):
    """
    arxiv online publication class
    """

    def __init__(self, code=None, original_url=None):
        self.code = code

        self.clean_code()
        url = "https://arxiv.org/abs/" + self.code
        OnlinePublication.__init__(self, url, original_url)

    def clean_code(self):
        """
        we want to let user pass both a valid code, or a link
        """
        if not self.code:
            return

        self.code = self.code.lower()

        if "arxiv.org" in self.code:
            self.code = self.code.replace("arxiv.org/abs/", "")
            self.code = self.code.replace("https://", "")
            self.code = self.code.replace("http://", "")
            self.code = self.code.replace("arxiv:", "")

    def compose_authors(self, max_authors=4):
        """
        from a list of authors, compose the authors string
        """
        if "ATLAS Collaboration" in self.authors_list:
            self.authors = "ATLAS Collaboration"
            return

        if "ATLAS" in self.authors_list:
            self.authors = " ".join(self.authors_list)
            return

        self.authors = ""
        if len(self.authors_list) == 1:
            self.authors = self.authors_list[0]
            return

        for counter, author in enumerate(self.authors_list):
            if "collaboration" in author.lower():
                self.authors = author
                return

            if max_authors and counter >= max_authors:
                self.authors = self.authors[0:-2] + " et al."
                return
            names = author.split(",")
            name_string = ""

            try:
                first_names = names[1].strip().split()

                for name in first_names:
                    if not name.endswith("."):
                        name_string += name[0] + ". "
                    else:
                        name_string += name + " "
                name_string += names[0]
                self.authors += name_string + ", "
            except:
                print("[ERROR] first_names debug {}".format(names))
                print("[ERROR] authors list = {}".format(self.authors_list))


        self.authors = self.authors[0:-2]

    def fill_data(self):
        """
        from the arxiv code, loads all the needed information
        """
        self.load()
        self.get_meta('citation_title', 'citation_author')
        self.compose_authors()

"""
arxiv = Arxiv("https://arxiv.org/abs/1510.03823")
arxiv.fill_data()
print("[DEBUG] title = {}".format(arxiv.get_title()))
print("[DEBUG] authors list = {}".format(arxiv.get_authors_list()))
print("[DEBUG] authors = {}".format(arxiv.get_authors()))
"""
"""
arxiv.compose_authors(1)
print("[DEBUG] authors = {}".format(arxiv.get_authors()))

arxiv = arXiv("https://arxiv.org/abs/1510.03823")
print(arxiv.code)
arxiv.get_title()
arxiv = arXiv("https://arxiv.org/abs/arXiv:1101.2185")
print(arxiv.code)
arxiv.get_title()
"""
