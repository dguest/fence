#!/usr/bin/env python2

"""
handles retrieving metadatas from iop website
"""

from OnlinePublication import OnlinePublication

class IOP(OnlinePublication):
    """
    IOP online publication class
    """

    def __init__(self, url=None):
        OnlinePublication.__init__(self, url)

    def compose_authors(self):
        """
        from a list of authors, compose the authors string
        """
        self.authors = ""
        for author in self.authors_list:
            names = author.split(" ")
            name_string = ""
            for name in names[0:-1]:
                name_string += name[0] + ". "
            name_string += names[-1]
            self.authors += name_string + ", "

        self.authors = self.authors[0:-2]

    def fill_data(self):
        """
        loads all the needed information
        """
        self.load()
        self.get_meta('citation_title', 'dc.creator')
        self.compose_authors()



"""
iop = IOP("https://iopscience.iop.org/article/10.1088/1126-6708/2007/11/070")
iop.fill_data()
print("[DEBUG] title = {}".format(iop.get_title()))
print("[DEBUG] author list = {}".format(iop.get_authors_list()))
print("[DEBUG] authors = {}".format(iop.get_authors()))
"""
