# Reference Checker

The reference checker is the tool meant to help ATLAS P.O. verifying the correctness of the references used in a publication. It follows the links provided in ATLAS bibliographies, check the link works, retrieves the official title and authors from the linked journal’s web page, and compare them with the ATLAS ones.

## This readme

In this readme file you will find essential informations to run the script.
To know more, and to understand how to read the report, you can access the [twiki page] (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ReferenceChecker)

## Usage

The script can be run through your terminal accessing the public path

```bash
/afs/cern.ch/user/a/atlaspo/public/scripts
```

as

```bash
./reference_checker.py REF_CODE
```

where REF_CODE must be a valid paper REF_CODE, such as JETM-2018-05, HIGG-2020-02


The script will then run and, at the end, provide two links:
the first one is different at every run and points to the report you created for the specifc REF_CODE.
the [second one] (https://atlaspo-eos.web.cern.ch/references/) points to a recap of the recents run of the script; it is static, and you can bookmark it in case you want to access any result page later.

## Developer extra usage

This section is meant for developing and debugging purspose only.
If you are not debugging the script, ignore this section.

Running the script with FOUR arguments, will skip all the process - which is otherwise going to take long - and allow fast debug for a specific entry in a specific file.

Example:

```bash
./reference_checker.py JETM-2018-05 bib/ATLAS.bib Baernreuther:2012ws
```
