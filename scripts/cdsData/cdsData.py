#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Reads from CDS values and store them in CDS_DATA database table
"""

import argparse
import time
import os
import sys
import datetime
from xml.dom.minidom import parseString

import requests

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

PAPERS_PATH = '/afs/cern.ch/atlas/www/GROUPS/PHYSICS/PAPERS/'
PUBNOTES_PATH = '/afs/cern.ch/atlas/www/GROUPS/PHYSICS/PUBNOTES/'
CONFNOTES_PATH = '/afs/cern.ch/atlas/www/GROUPS/PHYSICS/CONFNOTES/'

def is_valid_cds_id(cds_string):
    """
    checks if the id seems like a valid CDS id
    """
    cds_string = unicode(cds_string.strip(" \n"), 'utf-8')
    print("--{}--".format(cds_string))
    if len(cds_string) >= 6 and len(cds_string) <= 7:
        return cds_string.isnumeric()

    return False

def get_cds_id(file_name):
    """
    tries to retrieve the cds id
    """
    if os.path.isfile(file_name):
        with open(file_name, 'r') as input_file:
            file_content = input_file.read()

        if file_content:
            file_lines = file_content.splitlines()
            try:
                if file_lines[1]:
                    if is_valid_cds_id(file_lines[1]):
                        return file_lines[1]
            except:
                return False
    return False

def get_xml(cds_id):
    """
    retrieves the xml publication file from cds
    """
    cds_url = 'https://cds.cern.ch/record/' + cds_id + '?&of=xm&ot=520&ot=245&ot=269&ot=260'
    print("cdsUrl = {}".format(cds_url))
    cds_xml = requests.get(cds_url)
    return cds_xml

def save_cds_info(cds_xml, path):
    """
    saves the retrieved xml file on the proper folder
    """
    try:
        with open(path, 'w') as input_file:
            input_file.write(cds_xml.text.encode(cds_xml.encoding))
    except:
        print("error trying to save .cdsXml file on path {}".format(path))

def extract_xml_info(cds_xml):
    """
    gets the information from the xml downloaded file for the publication
    """
    cds_info = dict()
    dom_cds = parseString(cds_xml.text.encode(cds_xml.encoding))

    for datafield in dom_cds.getElementsByTagName('datafield'):
        if datafield.attributes["tag"].value == '520':
            key = ""
            value = ""
            for subfield in datafield.getElementsByTagName("subfield"):
                if (subfield.attributes["code"].value == 'a' or
                        subfield.attributes["code"].value == 'h'):
                    # since one of the conditions apply, we retrieve the value
                    value = subfield.firstChild.nodeValue
                if subfield.attributes["code"].value == '9':
                    key = subfield.firstChild.nodeValue
            if key == "":
                key = "abstract"

            cds_info[key] = value

        if datafield.attributes["tag"].value == '269':
            key = "publicationData"
            value = ""
            for subfield in datafield.getElementsByTagName("subfield"):
                if subfield.attributes["code"].value == 'c':
                    value = subfield.firstChild.nodeValue
            cds_info[key] = value

        if datafield.attributes["tag"].value == '245':
            key = "title"
            value = ""
            for subfield in datafield.getElementsByTagName("subfield"):
                if subfield.attributes["code"].value == 'a':
                    value = subfield.firstChild.nodeValue
            cds_info[key] = value

    return cds_info

def retrieve_cds_data(publication_type, ref_code, publication_id, dbm):
    """
    parses the xml file for the info we are seeking
    """
    # we check if cdsXml is there, if it is, we get info from that

    if publication_type == 'paper':
        base_path = PAPERS_PATH
    elif publication_type == 'confnote':
        base_path = CONFNOTES_PATH
    elif publication_type == 'pubnote':
        base_path = PUBNOTES_PATH

    if not ref_code:
        print("error - no ref_code for publication type = {}, publication_id = {}"
              .format(publication_type, publication_id))
        return False

    if not os.path.isdir(base_path + ref_code):
        #print("folder {} doesn't exists"
        #      .format(base_path + ref_code))
        return False

    if not os.path.isfile(base_path + ref_code + "/.cdsXml") or True:
        cds_id = get_cds_id(base_path + ref_code + "/.cdsinfo")

        if not isinstance(cds_id, basestring):
            print ("error - can't get cds_id for publication ref_code {}"
                   .format(ref_code))
            return False

        print("getting info from cds")
        cds_xml = get_xml(cds_id)
        save_cds_info(cds_xml, base_path + ref_code + "/.cdsXml")

        cds_info = dict()

        try:
            cds_info = extract_xml_info(cds_xml)
        except:
            print("error: can't retrieve CDS xml for publication {}"
                  .format(ref_code))
            return False

        abstract = ""
        if 'abstract' in cds_info:
            abstract = cds_info['abstract']
        elif 'arXiv' in cds_info:
            abstract = cds_info['arXiv']
        else:
            print("error - no abstract or arXiv abstract for publication {}"
                  .format(ref_code))
            return False

        try:
            dbm.insert("""INSERT INTO
                       CDS_DATA (PUBLICATION_ID, CDS_ID, ABSTRACT, TYPE) 
                       VALUES (:1,:2,:3,:4)""",
                       (publication_id, cds_id, abstract.encode('utf-8'), publication_type))
            print("""inserted data on db: \n
                publication_id = {} \n
                cds_id = {} \n
                abstract = {}, \n
                type = {}""".format(publication_id, cds_id, abstract, publication_type))
        except:
            print("error - can't insert values")
            print("{} - {} - {} - {}".format(publication_type, ref_code, publication_id, cds_id))
            print("abstract: {} ".format(cds_info['abstract'].encode("utf-8")))
            return False

        return True

def papers_operations(dbm, args):
    """
    operates on papers
    """
    query_data = dbm.select("SELECT ID, REF_CODE FROM PUBLICATION")

    papers_data = dict()
    for data in query_data:
        papers_data[data[1]] = data[0]

    query_data = dbm.select("SELECT PUBLICATION_ID FROM CDS_DATA WHERE TYPE='paper'")

    db_cds_publications = list()
    for data in query_data:
        db_cds_publications.append(data[0])

    if isinstance(args.papersonly, bool) or args.papersonly.upper() == 'ALL':
        # cycle through all the elements and call the function for each one

        for paper_ref_code, paper_id in papers_data.iteritems():
            if paper_id not in db_cds_publications:
                #print ("retrieving data for paper {} - {}".format(paper_ref_code,paper_id))
                if retrieve_cds_data('paper', paper_ref_code, paper_id, dbm):
                    time.sleep(3)

    else:
        # call the function only for the selected element
        ref_code = args.papersonly.upper()

        retrieve_cds_data('paper', ref_code, papers_data[ref_code], dbm)

        print ("we get the info from cdsXml file")

def confnotes_operations(dbm, args):
    """
    operates on confnotes
    """
    query_data = dbm.select("SELECT ID, REF_CODE FROM CONFNOTE_PUBLICATION")

    confnotes_data = dict()
    for data in query_data:
        confnotes_data[data[1]] = data[0]

    query_data = dbm.select("SELECT PUBLICATION_ID FROM CDS_DATA WHERE TYPE='confnote'")

    db_cds_publications = list()
    for data in query_data:
        db_cds_publications.append(data[0])

    if isinstance(args.confnotesonly, bool) or args.confnotesonly.upper() == 'ALL':
        # cycle through all the elements and call the function for each one

        for confnote_ref_code, confnote_id in confnotes_data.iteritems():
            if confnote_id not in db_cds_publications:
                if retrieve_cds_data('confnote', confnote_ref_code, confnote_id, dbm):
                    time.sleep(3)

    else:
        # call the function only for the selected element
        ref_code = args.confnotesonly.upper()

        retrieve_cds_data('confnote', ref_code, confnotes_data[ref_code], dbm)

        print ("we get the info from cdsXml file")

def pubnotes_operations(dbm, args):
    """
    operates on pubnotes
    """
    query_data = dbm.select("SELECT ID, FINAL_REF_CODE FROM PUBNOTE_PUBLICATION")

    pubnotes_data = dict()
    for data in query_data:
        pubnotes_data[data[1]] = data[0]

    query_data = dbm.select("SELECT PUBLICATION_ID FROM CDS_DATA WHERE TYPE='pubnote'")

    db_cds_publications = list()
    for data in query_data:
        db_cds_publications.append(data[0])

    if isinstance(args.pubnotesonly, bool) or args.pubnotesonly.upper() == 'ALL':
        # cycle through all the elements and call the function for each one

        for pubnote_ref_code, pubnote_id in pubnotes_data.iteritems():
            if pubnote_id not in db_cds_publications:
                print("retrieving data for pubnote {} - {}".format(pubnote_ref_code, pubnote_id))
                if retrieve_cds_data('pubnote', pubnote_ref_code, pubnote_id, dbm):
                    time.sleep(3)

    else:
        # call the function only for the selected element
        ref_code = args.pubnotesonly.upper()

        retrieve_cds_data('pubnote', ref_code, pubnotes_data[ref_code], dbm)

        print ("we get the info from cdsXml file")

def main():
    """
    executes main functionality
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--PAPERS', dest='papersonly', action="store_true")
    parser.set_defaults(papersonly=False)
    parser.add_argument('--CONFNOTES', dest='confnotesonly', action="store_true")
    parser.set_defaults(confnotesonly=False)
    parser.add_argument('--PUBNOTES', dest='pubnotesonly', action="store_true")
    parser.set_defaults(pubnotesonly=False)
    parser.add_argument("--CDS", dest="forceCDS", action='store_true')
    parser.set_defaults(forceCDS=False)
    args = parser.parse_args()

    print("Execution starts at %s" % datetime.datetime.now())

    if not args.papersonly and not args.confnotesonly and not args.pubnotesonly:
        args.papersonly = True
        args.confnotesonly = True
        args.pubnotesonly = True

    #print ("papersonlye = {}".format(args.papersonly))

    dbm = DBManager()

    if args.papersonly:
        papers_operations(dbm, args)

    if args.confnotesonly:
        confnotes_operations(dbm, args)

    if args.pubnotesonly:
        pubnotes_operations(dbm, args)

    dbm.close()


if __name__ == "__main__":
    main()
