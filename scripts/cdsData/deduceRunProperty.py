#!/usr/bin/python
"""
tries to deduce which run applies to a CMS paper from its abstract,
extracting collision energy and luminosity,
and going through the following algorithm:

The conditions could be according to the two variables CME (energy in TeV)
and LVAL (luminosity) LUNIT (unit of luminosity: fb-1, pb-1, nb-1)
as follows:

   type = Unknown
   if CME < 10 and (LVAL == fb or LVAL == pb):
type = Run1
   elif CME > 10 and LVAL == fb and LVAL < 100:
type = PartialRun2
   elif CME > 10 and LVAL == fb and LVAL > 100:
type = FullRun2
   else:
type = Other
"""

import sys
import operator
import re

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

def digits_in_string(string):
    for char in string:
        if char.isdigit():
            return True

def try_guess_cme(abstract, title=""):
    tevs = re.findall("[0-9\.]*\s?tev", abstract, re.IGNORECASE)
    values = [float(x.upper().replace("TEV", "").strip()) for x in tevs if digits_in_string(x)]
    value = ""
    if values:
        value = max(values)
    if not value:
        tevs = re.findall("[0-9\.]*\s?tev", title, re.IGNORECASE)
        values = [float(x.upper().replace("TEV", "").strip()) for x in tevs if digits_in_string(x)]
        value = ""
        if values:
            value = max(values)
    return value

def guess_best_unit_and_value(lvalues):
    comparable = list()
    for lvalue in lvalues:
        value = re.findall("[0-9\.]+", lvalue)
        if value:
            unit = ""
            if "microbarn" in lvalue or "mb" in lvalue:
                unit = 4
            elif "nanobarn" in lvalue or "nb" in lvalue:
                unit = 3
            elif "picobarn" in lvalue or "pb" in lvalue:
                unit = 2
            elif "femtobarn" in lvalue or "fb" in lvalue:
                unit = 1
            if unit:
                comparable.append((unit, float(value[0])))

    if comparable:
        comparable = sorted(comparable, key=operator.itemgetter(0, 1))
        if comparable[0][0] == 1:
            return "fb", comparable[0][1]
        elif comparable[0][0] == 2:
            return "pb", comparable[0][1]
        elif comparable[0][0] == 3:
            return "nb", comparable[0][1]
        elif comparable[0][0] == 4:
            return "mb", comparable[0][1]
    return "", ""

def try_guess_unit(abstract):
    microbarns = re.findall("[a-z0-9\.()]*\s?[a-z0-9\.()]*\s?-?microbarn", abstract, re.IGNORECASE)
    nanobarns = re.findall("[a-z0-9\.()]*\s?[a-z0-9\.()]*\s?-?nanobarn", abstract, re.IGNORECASE)
    picobarns = re.findall("[a-z0-9\.()]*\s?[a-z0-9\.()]*\s?-?picobarn", abstract, re.IGNORECASE)
    femtobarns = re.findall("[a-z0-9\.()]*\s?[a-z0-9\.()]*\s?-?femtobarn", abstract, re.IGNORECASE)

    mb = re.findall("[0-9\.]*\s?mb\$", abstract, re.IGNORECASE)
    nb = re.findall("[0-9\.]*\s?nb\$", abstract, re.IGNORECASE)
    fb = re.findall("[0-9\.]*\s?fb\$", abstract, re.IGNORECASE)
    pb = re.findall("[0-9\.]*\s?pb\$", abstract, re.IGNORECASE)

    lvalues = list()

    if microbarns:
        lvalues = lvalues + microbarns
    if picobarns:
        lvalues = lvalues + picobarns
    if femtobarns:
        lvalues = lvalues + femtobarns
    if nanobarns:
        lvalues = lvalues + nanobarns
    if mb:
        lvalues = lvalues + mb
    if fb:
        lvalues = lvalues + fb
    if pb:
        lvalues = lvalues + pb
    if nb:
        lvalues = lvalues + nb

    return guess_best_unit_and_value(lvalues)

def from_result_to_dictionary(papers):
    cms_papers = list()
    for paper in papers:
        cms_paper = dict()

        (cms_paper["recid"],
         cms_paper["ref_code"],
         cms_paper["arxiv"],
         cms_paper["abstract"],
         cms_paper["title"],
         cms_paper["imprint"],
         cms_paper["doi"],
         cms_paper["inspire"],
         cms_paper["cern_ep"],
         cms_paper["pre_publication"],
         cms_paper["ipotetic_run"],
         cms_paper["ignore"],
         cms_paper["tev"],
         cms_paper["lval"],
         cms_paper["lunit"]) = paper
        cms_papers.append(cms_paper)
    return cms_papers

def get_conditional_papers(condition):
    if not condition:
        condition = "TRUE"
    dbm = DBManager()
    # TODO: remove rownum <= 5, for optimized testing purpose only
    query = """SELECT
        RECID,
        REF_CODE,
        ARXIV,
        ABSTRACT,
        TITLE,
        IMPRINT,
        DOI,
        INSPIRE,
        CERN_EP,
        PRE_PUBLICATION,
        IPOTETIC_RUN,
        IGNORE,
        TEV,
        LVAL,
        LUNIT
        FROM CDS_CMS_DATA WHERE """ + condition + """ AND ROWNUM <= 1000"""
    results = dbm.execute(query)
    dbm.close()
    papers_fetched = from_result_to_dictionary(results)
    return papers_fetched

def get_not_ignored_papers():
    conditional_papers = get_conditional_papers("ignore = 0")
    return conditional_papers

def get_not_null_abstract_papers():
    conditional_papers = get_conditional_papers("abstract is not null AND ignore = 0")
    return conditional_papers

def get_empty_tevs_papers(abstract = True):
    if abstract:
        conditional_papers = get_conditional_papers("abstract is not null AND ignore = 0 AND TEV is null")
    else:
        conditional_papers = get_conditional_papers("ignore = 0 AND TEV is null")
    return conditional_papers

def get_empty_lval_papers(abstract = True):
    if abstract:
        conditional_papers = get_conditional_papers("abstract is not null AND ignore = 0 AND LVAL is null")
    else:
        conditional_papers = get_conditional_papers("ignore = 0 AND LVAL is null")
    return conditional_papers

def get_empty_lunit_papers(abstract = True):
    if abstract:
        conditional_papers = get_conditional_papers("abstract is not null AND ignore = 0 AND LUNIT is null")
    else:
        conditional_papers = get_conditional_papers("ignore = 0 AND LUNIT is null")
    return conditional_papers

def get_recid_paper(recid):
    conditional_papers = get_conditional_papers("recid = {}".format(recid))
    return conditional_papers

def get_ref_code_paper(ref_code):
    conditional_papers = get_conditional_papers("ref_code = {}".format(ref_code))
    return conditional_papers

def update_tev_paper(recid, tev_value, dbm):
    dbm.execute("UPDATE CDS_CMS_DATA SET TEV = :1 WHERE RECID = :2", (tev_value, recid))

def update_lunits_paper(recid, lvalue, lunit, dbm):
    dbm.execute("UPDATE CDS_CMS_DATA SET LVAL = :1, LUNIT = :2 WHERE RECID = :3", (lvalue, lunit, recid))

def update_empty_tevs_papers():
    dbm = DBManager()
    papers = get_empty_tevs_papers()
    for paper in papers:
        tev_value = try_guess_cme(paper["abstract"], paper["title"])
        if tev_value:
            print("updating paper {} with TeV = {}".format(paper["recid"], tev_value))
            update_tev_paper(paper["recid"], tev_value, dbm)
    dbm.close()

def update_empty_lunits_papers():
    dbm = DBManager()
    papers = get_empty_lunit_papers()
    for paper in papers:
        lunit, lvalue = try_guess_unit(paper["abstract"])
        if lunit:
            print("updating paper {} with lunit = {}, lvalue = {}".format(paper["recid"], lunit, lvalue))
            update_lunits_paper(paper["recid"], lvalue, lunit, dbm)
    dbm.close()

def return_header():
    return """<html><head><style>
        table {  width:100%; }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 15px;
            text-align: left;
        }
        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }
        table#t01 tr:nth-child(odd) {
            background-color: #fff;
        }
        table#t01 th {
            background-color: black;
            color: white;
        }
        </style></head><body><table id="t01">"""

def create_missing_report(missing="tev"):
    if missing == "tev":
        empty_papers = get_empty_tevs_papers()
        name_file = "missing_tev_report.html"
    else:
        empty_papers = get_empty_lunit_papers()
        name_file = "missing_lunit_report.html"

    header = return_header()
    body = "<tr><th>cds link</th><th>title</th><th>abstract</th>"
    for paper in empty_papers:
        body += """<tr><td><a href="https://cds.cern.ch/record/{}">{}</a></td>""".format(paper["recid"], paper["recid"])
        body += "<td>" + paper["title"] + "</td>"
        body += "<td>" + paper["abstract"] + "</td></tr>"
    footer = "</table></body></html>"

    with open(name_file, "w") as out_file:
        out_file.write(header)
        out_file.write(body)
        out_file.write(footer)

update_empty_tevs_papers()
update_empty_lunits_papers()

create_missing_report(missing="tev")
create_missing_report(missing="unit")
