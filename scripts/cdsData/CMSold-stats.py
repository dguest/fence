#!\usr\bin\env\python

from datetime import datetime
import requests
import re

def getCMSstats():

    paperlist = []
    arxivs = []
    _allDictCMS = {}
    _cmsMonthDict = {}

    ## get papers from CDS
    _lnk = "https://cds.cern.ch/search?jrec=%d&op1=a&op2=a&cc=CMS+Papers&ln=en&as=1&rg=200&m1=a&m3=a&m2=a&so=a"

    r = requests.get(_lnk % 1, verify=False)

    # print ( " requests.get = %s " % r)
    # print ( " _link text = %s" % r.text)

    ptn = [ (re.compile('arXiv:([0-9]{4}.[0-9]+).*(CMS-[A-Z,0-9]{3}-[0-9]+-[0-9]+(?:-[0-9]+)?)'), 1,2),
            (re.compile('(CMS-[A-Z,0-9]{3}-[0-9]+-[0-9]+(?:-[0-9]+)?).*arXiv:([0-9]{4}.[0-9]+)'), 2,1)
            ]

    for p,i1,i2 in ptn:
        for m in p.finditer(r.text):
            if m:
                arxiv = m.group(i1)
                docid = m.group(i2)
                if not arxiv in arxivs:
                    # print arxiv, docid
                    arxivs.append( arxiv )
                    paperlist.append( (arxiv, docid) )
                else:
                    pass
                    #print ("elment already in array %s " % arxiv)
            else:
                print "element non added"

    ptn_next = re.compile('records found &nbsp;.+?[0-9]+ - ([0-9]+)<a')

    while ptn_next.search(r.text):
        _jrec = int(ptn_next.findall(r.text)[0])+1
        print _jrec
        print _lnk % _jrec
        r = requests.get(_lnk % _jrec, verify=False)
        #_f=open("bla.html",'w'); _f.write(r.text.encode("UTF8")); _f.close()
        for p,i1,i2 in ptn:
            for m in p.finditer(r.text):
                if m:
                    arxiv = m.group(i1)
                    docid = m.group(i2)
                    if not arxiv in arxivs:
                        #print arxiv, docid
                        arxivs.append( arxiv )
                        paperlist.append( (arxiv, docid) )

    #print "CMS papers from CDS: %i" % len(paperlist)

    ## get papers from arXiv (last 2 months only)
    #_lnk2 = "http://arxiv.org/find/hep-ex/1/au:+Collaboration_CMS/0/1/0/all/0/1"
    _lnk2 = "http://arxiv.org/find/hep-ex/1/ANDNOT+au:+Collaboration_CMS+au:+for/0/1/0/all/0/1"  # leave out proceedings

    r = requests.get(_lnk2, verify=False)

    _y = datetime.today().year % 100
    _m = datetime.today().month
    ptn2 = re.compile('arXiv:((?:%02d%02d|%02d%02d).[0-9]+)' % (_y,_m-1,_y,_m))

    n_cms_recent_arxiv = 0

    for m in ptn2.finditer(r.text):
        if m:
            arxiv = m.group(1)
            docid = 'CMS-ARX'
            if not arxiv in arxivs:
                # print "Adding paper from arXiv: %s" % arxiv
                arxivs.append( arxiv )
                paperlist.append( (arxiv, docid) )
                n_cms_recent_arxiv += 1

    #print "CMS additional recent papers from the arxiv: %i" % n_cms_recent_arxiv

    paperlist_filtered = list()

    for i,j in paperlist:
        if 'CFT' in j or 'CMS-MUO-10-001' in j:
            print("exluding paper = ", j)
            #print 'Skipping paper: %s %s' % (i, j)
            continue

        paperlist_filtered.append( (i, j) )
        #print ("paperlist filtered added = %s" % j)

    paperlist_filtered.sort()

    #print "CMS papers selected: %i" % len(paperlist_filtered)

    for i,j in paperlist_filtered:
        #print "CMS: %s %s" % (i, j)

        grp = j.split('-')[1]

        # merges some of the groups
        if grp in ["FSQ", "QCD", "EWK", "SMP", "FWD", "GEN"]:
#                grp = "FSQ+QCD+EWK+SMP+FWD+GEN"
            grp = "STDM"
        if grp in ["B2G", "EXO"]:
            grp = "EXO+B2G"
        if grp in ["MUO", "JME", "TAU", "TRI", "BTV", "EGM", "TRK"]:
            grp = "PERF"

        if not _allDictCMS.has_key(grp):
            _allDictCMS[grp] = 1
        else:
            _allDictCMS[grp] += 1

        key = datetime.strptime(i.split(".")[0],"%y%m").strftime("%Y-%m")

        if not _cmsMonthDict.has_key(key):
            _cmsMonthDict[key] = {grp : 1, "Total" : 0}
        else:
            if not _cmsMonthDict[key].has_key(grp):
                _cmsMonthDict[key][grp] = 1
            else:
                _cmsMonthDict[key][grp] += 1

        _cmsMonthDict[key]["Total"] += 1

    print("cms papers list length =", len(paperlist))
    print("cms papers list length =", len(paperlist_filtered))

    return paperlist, paperlist_filtered


paperlist, paperlist_filtered = getCMSstats()

refcodes = [x[1] for x in paperlist]
print(refcodes)

#print("******* dictionary starts here")
#print(dictionary)
#print("******* monthly starts here")
#print(monthly)