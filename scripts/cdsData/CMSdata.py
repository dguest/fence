#!/usr/bin/python
"""
starting as a test file to understand how to retrieve information from cds on CMS papers
"""

import json
import sys
import time
import requests

# suppress insecure warning about requests connection for missing certificate
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
import dates
#pylint: enable=wrong-import-position

RUN2_THRESHOLD = 120

RECIDS_LINK = "https://cds.cern.ch/search?jrec=%d&op1=a&op2=a&ln=en&as=1&rg=200&m1=a&m3=a&m2=a&so=a&cc=CMS+Papers&rg=200&of=recjson&ot=recid"
PUBLICATION_LINK = "http://cdsweb.cern.ch/record/%d?&of=recjson&ot=recid&ot=abstract&ot=prepublication&ot=title&ot=doi&ot=primary_report_number&ot=imprint&ot=system_control_number&ot=report_number"
PRE_PUBLICATION_LINK = "http://cdsweb.cern.ch/record/%d?&of=recjson&ot=prepublication"

def retrieve_cms_recids():
    """
    retrives all the CDS recid for CMS publications
    returns a list with the recids
    """
    cms_recids = list()
    for index in range(0, 20):
        result = requests.get(RECIDS_LINK % (index*200 + 1), verify=False)

        result_json = json.loads(result.content)

        if not result_json:
            break

        print("[INFO] retrieving {} to {} recids returned status {}"
              .format(index*200, (index+1)*200, result.status_code))

        for key in result_json:
            if "recid" in key and key["recid"]:
                cms_recids.append(key["recid"])
    return cms_recids


def add_cms_recid_in_db(cms_recids):
    """
    when recids are gathered, we insert the new ones in the db
    """
    dbm = DBManager()
    query_data = dbm.execute("SELECT RECID FROM CDS_CMS_DATA")
    existing_recids = [x[0] for x in query_data]
    added_recids = list()
    for recid in cms_recids:
        if recid not in existing_recids:
            dbm.execute("INSERT INTO CDS_CMS_DATA (RECID, IGNORE) VALUES (:1, 0)", (recid,))
            added_recids.append(recid)

    dbm.close()
    return added_recids

def retrieve_cds_info(recid):
    """
    we gather information about a publication
    meant to be used for the new inserted one, with only recids info in db
    """
    print("[INFO] retrieving info for recid = {}".format(recid))
    recid = int(recid)
    result = requests.get(PUBLICATION_LINK % recid, verify=False)
    result_json = json.loads(result.content)
    return result_json

def retrieve_pre_publication_info(recid):
    """
    we gather information about a publication
    meant to be used for the new inserted one, with only recids info in db
    """
    print("[INFO] retrieving pre_publication info for recid = {}".format(recid))
    print("[DEBUG] cds link = {}".format(PRE_PUBLICATION_LINK % recid))
    recid = int(recid)
    result = requests.get(PRE_PUBLICATION_LINK % recid, verify=False)
    result_json = json.loads(result.content)
    return result_json

def extract_ref_code(primary_report_number, secondary_report_number):
    """
    extracts ref code from the primary report number, or in secondary if not yet found
    """
    if isinstance(primary_report_number, list):
        for report_number in primary_report_number:
            if "CMS-" in report_number:
                return report_number
    if "CMS-" in primary_report_number:
        return primary_report_number
    if isinstance(secondary_report_number, list):
        for report_number in secondary_report_number:
            if "report_number" in report_number:
                if "CMS-" in report_number["report_number"]:
                    return report_number["report_number"]
    if "CMS-" in secondary_report_number:
        return secondary_report_number
    if "report_number" in secondary_report_number:
        if "CMS-" in secondary_report_number["report_number"]:
            return secondary_report_number["report_number"]
    return ""

def extract_arxiv_code(primary_report_number):
    """
    tries to extract arXiv code from the primary report number
    """
    if isinstance(primary_report_number, list):
        for report_number in primary_report_number:
            if "arXiv" in report_number:
                return report_number
        return ""
    if "arXiv" in primary_report_number:
        return primary_report_number
    return ""

def extract_cernep_code(primary_report_number, secondary_report_number):
    """
    tries to extract cernep from primary or secondary report number
    """
    if isinstance(primary_report_number, list):
        for report_number in primary_report_number:
            if "CERN-" in report_number:
                return report_number
    if "CERN-" in primary_report_number:
        return primary_report_number
    if isinstance(secondary_report_number, list):
        for report_number in secondary_report_number:
            if "report_number" in report_number:
                if "CERN-" in report_number["report_number"]:
                    return report_number["report_number"]
    if "CERN-" in secondary_report_number:
        return secondary_report_number
    return ""

def extract_abstract(abstracts):
    """
    tries to extract the best abstract
    """
    arxiv_abstract = ""
    base_abstract = ""
    if isinstance(abstracts, list):
        for abstract in abstracts:
            if "number" in abstract:
                if abstract["number"] == "arXiv":
                    arxiv_abstract = abstract["summary"]
            else:
                base_abstract = abstract["summary"]
        if arxiv_abstract:
            return arxiv_abstract
        return base_abstract
    else:
        return abstracts["summary"]

def extract_digits(string):
    """
    extracts digits from a string
    """
    digits = ""
    for digit in [s for s in string.split() if s.isdigit()]:
        digits += digit
    return digits

def extract_doi(doi):
    """
    extracts the doi from the list of dois
    """
    if isinstance(doi, list):
        return doi[0]
    return doi

def extract_inspire(system_control_number):
    """
    tries to extract the inspire id from the system control number
    """
    if not system_control_number:
        return

    if isinstance(system_control_number, list):
        for system in system_control_number:
            if system["institute"] == "Inspire":
                return extract_digits(system["value"])
        return ""
    else:
        if "institute" in system_control_number and system_control_number["institute"] == "Inspire":
            if "value" in system_control_number:
                return extract_digits(system_control_number["value"])

def extract_title(title):
    """
    gets the title from the list, if it's in list format
    """
    if isinstance(title, list):
        return title[0]["title"]
    return title["title"]

def is_ignore_ref_code(ref_code):
    """
    we will ignore some ref_codes as not analysis publications
    """
    if "CFT" in ref_code:
        return 1
    if "CMS-MUO-10-001" in ref_code:
        return 1
    if "ATLAS" in ref_code:
        return 1
    return 0

def add_pre_publication_in_db(recid, info):
    """
    after retrieve_pre_publication_info we parse that info to insert data in db
    """

    pre_publication = extract_date(info)

    dbm = DBManager()
    dbm.insert("""
        UPDATE CDS_CMS_DATA SET
         PRE_PUBLICATION = :1
        WHERE RECID = :2;
        """, (pre_publication, int(recid)))
    print("added prepublication info for recid = {}".format(recid))
    dbm.close()


def add_cms_info_in_db(recid, info):
    """
    after retrieve_cds_info we parse that info to insert data in db
    """
    if not info["primary_report_number"]:
        return
    print("[INFO] adding cds data for recid = {}".format(recid))
    print("[INFO] = {}".format(info))
    ref_code = extract_ref_code(info["primary_report_number"], info["report_number"])
    if not ref_code:
        ref_code = 'ATLAS-CHEAT-001'
    arxiv = extract_arxiv_code(info["primary_report_number"])
    abstract = extract_abstract(info["abstract"])
    title = extract_title(info["title"])
    doi = extract_doi(info["doi"])
    inspire = extract_inspire(info["system_control_number"])
    cern_ep = extract_cernep_code(info["primary_report_number"], info["report_number"])
    pre_publication = extract_date(info)

    ignore = is_ignore_ref_code(ref_code)

    dbm = DBManager()
    dbm.insert("""
        UPDATE CDS_CMS_DATA SET
         REF_CODE = :1, 
         ARXIV = :2, 
         ABSTRACT = :3, 
         TITLE = :4, 
         DOI = :5, 
         INSPIRE = :6, 
         CERN_EP = :7, 
         IGNORE = :8,
         PRE_PUBLICATION = :9
        WHERE RECID = :10;
        """, (ref_code, arxiv, abstract.encode("utf-8"), title.encode("utf-8"), doi, inspire,
              cern_ep, ignore, pre_publication, int(recid)))
    print("[INFO] added cds info for recid = {}".format(recid))
    dbm.close()

def update_null_ref_codes(timer=2):
    """
    when new recids are inserted, we need to populate other information about those publications
    """
    dbm = DBManager()
    results = dbm.execute("SELECT RECID FROM CDS_CMS_DATA WHERE REF_CODE IS NULL AND ignore = 0")
    dbm.close()

    for result in results:
        recid = result[0]
        info = retrieve_cds_info(recid)
        add_cms_info_in_db(recid, info[0])
        print("[INFO] adding info in db for recid {}".format(recid))
        # we don't want to keep stressing servers, so we take a break
        time.sleep(timer)

def update_null_pre_publication(timer=1):
    """
    as at first we couldn't get this data, the meaning of this function is to update those values
     check to see if some publications still need this info
    """
    dbm = DBManager()
    results = dbm.execute("SELECT RECID FROM CDS_CMS_DATA WHERE PRE_PUBLICATION IS NULL AND ignore = 0")
    dbm.close()

    for result in results:
        recid = result[0]
        info = retrieve_pre_publication_info(recid)
        add_pre_publication_in_db(recid, info[0])
        print("[INFO] addes missing pre publication in db for recid {}".format(recid))
        time.sleep(timer)

def is_cds_id(recid):
    """
    tries to valide the recid code
    """
    recid = recid.strip()
    for char in recid.split():
        if not char.isdigit():
            return False
    # I guess in 2050 when recids will reach 8 digits, we won't use this script anymore...
    if len(recid) < 1 or len(recid) > 7:
        return False
    return True

def guess_original_algorithm(cms_publication):
    """
    applies original algorithm to guess the publication run
    """
    #recid, tev, lval, lunit, ipotetic_run = cms_publication
    _, tev, lval, lunit, _ = cms_publication

    guess_run = "unknown"
    if tev < 10 and (lunit == 'fb' or lunit == 'pb'):
        guess_run = "Run1"
    elif tev >= 10 and lunit == 'fb' and lval < RUN2_THRESHOLD:
        guess_run = "PartialRun2"
    elif tev >= 10 and lunit == 'fb' and lval >= RUN2_THRESHOLD:
        guess_run = "FullRun2"

    #if guess_run == "unknown":
    #    print(cms_publication)

    return guess_run

def guess_cms_run():
    """
    gets the data from the db to then guess their runs
    """
    dbm = DBManager()
    results = dbm.execute("SELECT recid, tev, lval, lunit, ipotetic_run FROM CDS_CMS_DATA WHERE ignore = 0")
    dbm.close()

    guess_result = dict()
    for result in results:
        guess = guess_original_algorithm(result)

        if guess not in guess_result:
            guess_result[guess] = 1
        else:
            guess_result[guess] = guess_result[guess] + 1

    return guess_result

def guess_simplified_algorithm(cms_publication):
    """
    applies the simplified algorithm to guess the publication run
    """
    #recid, tev, lval, lunit, ipotetic_run = cms_publication
    _, tev, lval, lunit, _ = cms_publication

    guess_run = "unknown"
    if tev < 10:
        guess_run = "Run1"
    elif tev >= 10 and lunit == 'fb' and lval < RUN2_THRESHOLD:
        guess_run = "PartialRun2"
    elif tev >= 10 and lunit == 'fb' and lval >= RUN2_THRESHOLD:
        guess_run = "FullRun2"

    #if guess_run == "unknown":
    #    print(cms_publication)

    return guess_run


def guess_cms_run_simplified(only_missing=True):
    """
    gets the data from the db to then guess their runs
    """
    dbm = DBManager()
    if only_missing:
        results = dbm.execute("SELECT recid, tev, lval, lunit, ipotetic_run FROM CDS_CMS_DATA WHERE ignore = 0 AND IPOTETIC_RUN IS NULL")
    else:
        results = dbm.execute("SELECT recid, tev, lval, lunit, ipotetic_run FROM CDS_CMS_DATA WHERE ignore = 0")

    guess_result = dict()
    for result in results:
        guess = guess_simplified_algorithm(result)

        print(guess, result[0])
        if guess == "Run1":
            ipotetic_run = 1
        elif guess == "PartialRun2":
            ipotetic_run = 2
        elif guess == "FullRun2":
            ipotetic_run = 3
        else:
            ipotetic_run = 0
        dbm.execute("UPDATE CDS_CMS_DATA SET IPOTETIC_RUN = :1 WHERE RECID = :2 ", (ipotetic_run, result[0]))

        if guess not in guess_result:
            guess_result[guess] = 1
        else:
            guess_result[guess] = guess_result[guess] + 1

    dbm.close()
    return guess_result


# TODO: move this function in utilities. It must be at use for every other script
def extract_date(info):
    """ try to extract date from input """
    pre_publication = None
    if not "prepublication" in info or not info["prepublication"]:
        print ("[WARN] missing prepublication info {}".format(info))
        return None

    if not "date" in info["prepublication"] or not info["prepublication"]["date"]:
        if isinstance(info["prepublication"], list):
            temp = info["prepublication"][0]
            pre_publication = temp
        else:
            print ("[WARN] missing date info {}".format(info))
            return None

    if not pre_publication:
        pre_publication = info["prepublication"]["date"]

    try:
        pre_publication = dates.extract_date(pre_publication, format_date="dd-MM-yyyy")
    except dates.NoStandardDate:
        print("[WARN] date is {} in info {}, please insert it manually".format(pre_publication, info))
        print("\tcouldn't add pre publication info for recid = {}".format(info))
        return ""

    return pre_publication


def run():
    """ executes all the operations to save data in the db """

    print("[STATUS] Updating null pre publications")
    update_null_pre_publication()
    # retrieves all CMS recids from CDS
    print("[STATUS] Retrieving cms recids")
    cms_recids = retrieve_cms_recids()
    print("[INFO] retrieved {} cds recids".format(len(cms_recids)))

    # add the one not already in the db
    added_recids = add_cms_recid_in_db(cms_recids)
    if added_recids:
        print("[INFO] new cms recids added to db: {}".format(added_recids))

    # if missing pre_publication info, we set them
    update_null_pre_publication()

    update_null_ref_codes() #  -> will call:
    #### retrieve_cds_info()
    #### add_cms_info_in_db() -> will cgitsall:
    #### #### extract_ref_code()
    #### #### extract_arxiv_code()
    #### #### extract_abstract()
    #### #### extract_title()
    #### #### extract_doi()
    #### #### extract_inspire()
    #### #### extract_cernep_code()

    #### #### is_ignore_ref_code()

    guess_cms_run_simplified()


run()
