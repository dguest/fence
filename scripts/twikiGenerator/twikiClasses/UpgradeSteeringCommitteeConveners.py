# Class to store anc complete conveners data
class UpgradeSteeringCommitteeConveners(object):
    appointment_name = ""
    appointment_id = ""
    conveners = ""
    position = 0


    # The class "constructor" - It's actually an initializer 
    def __init__(self, appointment_name, conveners, appointment_id):
        self.appointment_name = appointment_name
        self.appointment_id = appointment_id
        self.conveners = conveners
        self.position = 0
        if appointment_id == "31":
            self.position = 0
        elif appointment_id == "5":
            self.position = 1
        elif appointment_id == "2082":
            self.position = 2
        elif appointment_id == "b":
            self.position = 3
        elif appointment_id == "304":
            self.position = 4
        elif appointment_id == "13":
            self.position = 5
        elif appointment_id == "c":
            self.position = 6
        elif appointment_id == "31":
            self.position = 7
        elif appointment_id == "325":
            self.position = 8
        elif appointment_id == "8":
            self.position = 9
        elif appointment_id == "9":
            self.position = 10
        elif appointment_id == "326":
            self.position = 11
        elif appointment_id == "2140":
            self.position = 12
        elif appointment_id == "10":
            self.position = 13
        elif appointment_id == "16":
            self.position = 14
        elif appointment_id == "d":
            self.position = 15
        elif appointment_id == "e":
            self.position = 16
        elif appointment_id == "2160":
            self.position = 17
        elif appointment_id == "f":
            self.position = 18
        elif appointment_id == "68":
            self.position = 19
        elif appointment_id == "g":
            self.position = 20
        elif appointment_id == "28":
            self.position = 21
        elif appointment_id == "1023":
            self.position = 22
        elif appointment_id == "26":
            self.position = 23
        elif appointment_id == "24":
            self.position = 24
        elif appointment_id == "18":
            self.position = 25
        elif appointment_id == "3":
            self.position = 26
        elif appointment_id == "6":
            self.position = 27
        elif appointment_id == "4":
            self.position = 28
        else:
            self.position = 99