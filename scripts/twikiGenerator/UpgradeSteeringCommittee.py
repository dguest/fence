'''
    Thie class generate the table for

    Upgrade Steering Committee

    and store it into EOS output path

    developed by Gianluca Picco <gianluca.picco@cern.ch>
'''

import datetime
import json

import config

from UpgradeSteeringCommitteeConveners import UpgradeSteeringCommitteeConveners


now = datetime.datetime.now()

print ""
print "##########################################################################"
print "##                                                                      ##"
print "## Upgrade Steering Committee generation on: "+str(now)+" ##"
print "##                                                                      ##"
print "##########################################################################"
print ""

input_path = config.getInputPath()+"/json/"
input_file_name = "PhysicsAnalysisGroupsConveners.json"

output_path = config.getOutputPath()
output_file_name = "PhysicsAnalysisGroupsConveners.txt"

conveners_array = []
with open(input_path+input_file_name) as json_file:  
    data = json.load(json_file)
    for p in data['list']:
        appointment = p['APPOINTMENT']
        conveners = p['Name']
        appointment_id = p['APPOINTMENTID']
        conveners_array.append(UpgradeSteeringCommitteeConveners(appointment, conveners, appointment_id))

# Hardcoded appointments since there are no appointments for these
conveners_array.append(UpgradeSteeringCommitteeConveners('Upgrade Review Office Chair','Ariella Cattai','b'))
conveners_array.append(UpgradeSteeringCommitteeConveners('LAr Phase-1 Upgrade Project Leader','Luis Hervas,Hucheng Chen','c'))
conveners_array.append(UpgradeSteeringCommitteeConveners('FTK Project Coordinator','David Strom','d'))
conveners_array.append(UpgradeSteeringCommitteeConveners('TDAQ Phase-1 Upgrade Project Leader','Stefano Veneziano','e'))
conveners_array.append(UpgradeSteeringCommitteeConveners('TDAQ Architecture/Readout Subcommittee Chair','Gustaaf Brooijmans','f'))
conveners_array.append(UpgradeSteeringCommitteeConveners('Electronics','Philippe Farthouat','g'))

conveners_array.sort(key=lambda x: x.position)

# Generating the output

mainColor = '#98A0A8'
output = '<style type="text/css">'
output += '.twikiFirstCol{'
output += '    border-radius:10px 0 0 0;'
output += '}'
output += '.twikiLastCol{'
output += '    border-radius:0 10px 0 0;'
output += '}'
output += '.headerTable{'
output += '     border-bottom: 1px solid #98A0A8;'
output += '     color: #fff;'
output += '     font-weight: normal;'
output += '}'
output += '.oddRow{'
output += '     color: #000;'
output += '     background-color: #FFFFFF;'
output += '     border-bottom: 1px solid #98A0A8;'
output += '}'
output += '.evenRow{'
output += '     color: #000;'
output += '     background-color: #F5F5F5;'
output += '     border-bottom: 1px solid #98A0A8;'
output += '}'
output += '.tdSpaced{'
output += '    padding-top: .5em;'
output += '    padding-bottom: .5em;'
output += '}'
output += '</style>'
output_table = '<table id="conveners" style="border-collapse: collapse;width:80%;">'
output_table += '<tr style="height:2.15em;">'
output_table += '<th bgcolor="'+mainColor+'" valign="middle" width="30%" class="twikiTableCol0 twikiFirstCol twikiLast headerTable" align="left">&nbsp;&nbsp;Member</th>'
output_table += '<th bgcolor="'+mainColor+'" valign="middle" width="40%" class="twikiTableCol0 twikiLastCol twikiLast headerTable" align="left">Function</th>'
output_table += '</tr>'
output_table += '<tbody>'

oddRow = True
managementRowCol2 = "ATLAS Management";
managementRowCol1 = "";
for c in conveners_array:
    if (c.appointment_id == "3" or c.appointment_id == "6" or c.appointment_id == "4"):
        managementRowCol1 += c.conveners+','
    else:
        if oddRow:
            output_table += '<tr class="oddRow">'
            oddRow = False
        else:
            output_table += '<tr class="evenRow">'
            oddRow = True
        output_table += '<td class="tdSpaced" width="50%">&nbsp;&nbsp;'
        output_table += c.conveners.replace(',',', ')
        output_table += '</td>'
        output_table += '<td class="tdSpaced" width="50%" align="left">'
        output_table += c.appointment_name
        output_table += '</td>'
        output_table += '</tr>'

if oddRow:
    output_table += '<tr class="oddRow">'
    oddRow = False
else:
    output_table += '<tr class="evenRow">'
    oddRow = True
output_table += '<td class="tdSpaced" width="50%">&nbsp;&nbsp;'
output_table += managementRowCol1.replace(',',', ')[:-2]
output_table += '</td>'
output_table += '<td class="tdSpaced" width="50%" align="left">'
output_table += managementRowCol2
output_table += '</td>'
output_table += '</tr>'

output_table += '</tbody>'
output_table += '</table>'

if output[-2] == ',':
    output = output[:-2]
output += '</a></br></br>'
output += output_table

# Writing the output
output_file = output_path+output_file_name
f = open(output_file,"w")
print "Opening output file: ", output_file
f.write(output)
f.close()
