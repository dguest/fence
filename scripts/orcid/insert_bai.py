#!/usr/bin/env python
"""
the script updates the BAI values found by bai_from_inspire
saved on the csv file
"""

import sys
import csv

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager

dbm = DBManager()

with open("bai/bai_from_inspire_invalid.csv", "r") as in_file:
    csv_reader = csv.reader(in_file)
    header = True
    for row in csv_reader:
        if not header:
            sql_query = """
                        UPDATE
                        MEMB
                        SET 
                        INSPIRE_HEPNAME = '{}'
                        WHERE
                        ID = {}
                        """.format(row[6], row[0])
            print(sql_query)
            dbm.execute(sql_query)
            break

        else:
            header = False
            print("header = {}".format(row))



