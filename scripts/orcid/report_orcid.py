#!/usr/bin/env python

import sys
import datetime
import json

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager

inspire_orcid = """SELECT
                    ID,
                    FIRST_NAME,
                    LAST_NAME,
                    INSPIRE
                   FROM
                    MEMB
                   WHERE
                    INSPIRE IS NOT NULL
                   AND
                    ORCID IS NULL
                   ORDER BY LAST_NAME
                """

dbm = DBManager()

results = dbm.execute(inspire_orcid)

output = ""
for index, result in enumerate(results):
    output += "<a href=https://glance.cern.ch/atlas/membership/members/profile?id="
    output += str(result[0])
    output += ">" + result[2] + " " + result[1]
    output += "</a> - \n"

with open("missing_orcid.html", "w+") as out_file:
    out_file.write(output)


inspires = [result[3] for result in results]

dictionary = {}
with open("orcid_from_inspire.txt", "r") as in_file:
    dictionary = json.load(in_file)

output = ""
for inspire, orcid in dictionary.items():
    query = """UPDATE
                MEMB
                SET
                ORCID = '{}'
                WHERE
                INSPIRE = '{}'""".format(orcid, inspire)
    dbm.execute(query)
    try:
        index = inspires.index(inspire)
        output += "<a href=https://glance.cern.ch/atlas/membership/members/profile?id="
        output += str(results[index][0])
        output += ">" + results[index][2] + " " + results[index][1]
        output += "</a> - \n"
    except:
        pass

namefile = "inserted_orcid_{}.html".format(str(datetime.datetime.now())[0:10])
with open(namefile, "w+") as out_file:
    out_file.write(output)

