#!/usr/bin/env python
"""
this script runs through CERN-EP-2019-274 author list
get their inspire id
cehck if we have that inspire id
if not, suggests to add it

also, report the ones who don't have the inspire id
"""

import sys
import json
import requests

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager

query_inspire = "SELECT INSPIRE FROM MEMB WHERE INSPIRE IS NOT NULL"
dbm = DBManager()
inspires = dbm.execute(query_inspire)
inspire_list = []
for inspire in inspires:
    inspire_list.append(inspire[0])

print("inspire list = {}".format(inspire_list))




url = "http://old.inspirehep.net/search?p=CERN-EP-2019-274&of=recjson&ot=authors"

request = requests.get(url)

content = json.loads(request.content)

authors = content[0]["authors"]

print("authors = {}".format(authors))

for author in authors:
    if "INSPIRE_number" in author:
        inspire_number = author["INSPIRE_number"]

        if inspire_number not in inspire_list:
            print("inspire is missing but we can add it")

    else:
        print("author doesn't have inspire_number {}".format(author))