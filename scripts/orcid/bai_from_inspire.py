#!/usr/bin/env python
"""
the scripts goes through all the members in ATLASPO / GLANCE db
and for the one missing their orcid, but with a valid inspire record
gets to inspire to retrieve their orcid, if present
"""

import sys
import os
import json
import csv
import requests

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager

SEARCH_QUERY = "http://old.inspirehep.net/search?ln=it&cc=HepNames&p=%s&sf=exactfirstauthor&so=a&of=recjson&ot=system_control_number&ot=authors"
INSPIRE_CERN_PREFIX = "CERN-"
CSV_COLUMNS = ["glance_id", "cern_ccid", "first_name", "last_name", "inspire", "orcid", "bai", "cross-check"]
CSV_COLUMNS_CCID = ["glance_id", "cern_ccid", "first_name", "last_name", "inspire", "orcid", "bai", "cross_check", "inspire_last_name"]

def get_inspire_orcid_members(demo=False, rows=10):
    """
    gets ALL the entry in members with at least one between orcid and inspire
    """

    if demo:
        query = """SELECT
                    ID,
                    CERN_CCID,
                    FIRST_NAME,
                    LAST_NAME,
                    INSPIRE,
                    ORCID
                   FROM
                    MEMB
                   WHERE
                    (INSPIRE IS NOT NULL
                     OR
                    ORCID IS NOT NULL)
                   AND
                    ROWNUM <= {}
                   ORDER BY LAST_NAME, FIRST_NAME
                """.format(rows)
    else:
        query = """SELECT
                    ID,
                    CERN_CCID,
                    FIRST_NAME,
                    LAST_NAME,
                    INSPIRE,
                    ORCID
                   FROM
                    MEMB
                   WHERE
                    INSPIRE IS NOT NULL
                   OR
                    ORCID IS NOT NULL
                   ORDER BY LAST_NAME, FIRST_NAME
                """



    dbm = DBManager()
    members_db = dbm.execute(query)

    members_list = []
    for member in members_db:
        (glance_id, cern_ccid, first_name, last_name, inspire, orcid) = member
        members_list.append({
            "glance_id" : glance_id,
            "cern_ccid" : cern_ccid,
            "first_name" : first_name,
            "last_name" : last_name,
            "inspire" : inspire,
            "orcid" : orcid
            })

    return members_list


def evaluate_inspire_orcid_members(members):
    """
    for those member that we have an inspire or orcid record, we try to retrieve their name
    """

    members_count = len(members)
    print("number of authors we found with at least one between inspire_id and orc_id is {}"
          .format(members_count))

    bais = []

    for counter, member in enumerate(members):
        print("evaluating author {} out of {}".format(counter + 1, members_count))
        bai = None
        cern_id = None

        if member['inspire']:
            url = SEARCH_QUERY % member['inspire']
        elif member['orcid']:
            url = SEARCH_QUERY % member['orcid']

        request = requests.get(url)

        try:
            content = json.loads(request.content)
            institutes = content[0]["system_control_number"]

            for institute in institutes:
                if institute["institute"] == "BAI":
                    bai = institute["value"]
                    #bais[member[0]] = bai
                    member['bai'] = bai
                    print("value = {}".format(institute["value"]))
                    print("found missing one # {}".format(len(bais)))
                elif institute["institute"] == "CERN":
                    cern_id = institute["value"]
        except IndexError:
            print("[Error]: Can't retrieve info for user {} {}"
                  .format(member['first_name'], member['last_name']))
            print("         url = {}".format(url))
            print("         content = {}".format(content))
        except:
            continue

        if bai is not None:
            member['cross-check'] = False
            if cern_id is not None:
                print("cern_id = CERN-{} \t inspire_cern_id = {}"
                      .format(member['cern_ccid'], cern_id))
                # if NOT true, at this point, we should get a kind of ALERT
                if INSPIRE_CERN_PREFIX + str(member['cern_ccid']) == cern_id:
                    member['cross-check'] = True
                else:
                    member['cross-check'] = 'INVALID'

            bais.append(member)

    return bais


def save_bai(filename, dict_data, fieldnames=None, ffile='json'):
    """
    writes bais into a file
    """
    with open(filename, "w+") as out_file:
        if ffile == 'json':
            out_file.write(json.dumps(dict_data))
        elif ffile == 'csv':
            if fieldnames is None:
                print("[ERROR] Didn't specify columns headers for csv file")
                return
            writer = csv.DictWriter(out_file, fieldnames=fieldnames)
            writer.writeheader()
            for data in dict_data:
                try:
                    writer.writerow(data)
                except UnicodeEncodeError:
                    print("[ERROR] Can't write line to file: \n{}".format(data))
                    continue


def get_ccid_members(demo=False, rows=10):
    """
    retrieves all the atlas members with a cern ccid, but with no INSPIRE_ID, nor ORCID
    """
    if demo:
        query = """SELECT
                    ID,
                    CERN_CCID,
                    FIRST_NAME,
                    LAST_NAME
                   FROM
                    MEMB
                   WHERE
                    INSPIRE IS NULL
                    AND
                    ORCID IS NULL
                   AND
                    ROWNUM <= {}
                   ORDER BY LAST_NAME, FIRST_NAME
                """.format(rows)
    else:
        query = """SELECT
                    ID,
                    CERN_CCID,
                    FIRST_NAME,
                    LAST_NAME
                   FROM
                    MEMB
                   WHERE
                    INSPIRE IS NULL
                   AND
                    ORCID IS NULL
                   ORDER BY LAST_NAME, FIRST_NAME
                """

    dbm = DBManager()
    members_ccid = dbm.execute(query)

    members_list = []
    for member in members_ccid:
        (glance_id, cern_ccid, first_name, last_name) = member
        members_list.append({
            "glance_id" : glance_id,
            "cern_ccid" : cern_ccid,
            "first_name" : first_name,
            "last_name" : last_name
            })

    return members_list

def evaluate_cern_ccid_members(members):
    """
    walks through all the members with ccid, but no inspire or orcid
    """
    members_count = len(members)
    print("number of authors with no orcid, nor inspire id is {}".format(members_count))

    bais = []

    for counter, member in enumerate(members):
        print("evaluating author {} out ouf {}".format(counter + 1, members_count))
        bai = None
        inspire = None
        orcid = None

        search_term = "CERN-{}".format(member['cern_ccid'])

        url = SEARCH_QUERY % search_term

        request = requests.get(url)

        try:
            content = json.loads(request.content)
            institutes = content[0]["system_control_number"]

            for institute in institutes:
                if institute["institute"] == "CERN":
                    cern_check_id = institute["value"]
                    print("cern check id = {}".format(cern_check_id))
                elif institute["institute"] == "BAI":
                    bai = institute["value"]
                    print("found bai value id = {}".format(bai))
                elif institute["institute"] == 'INSPIRE':
                    inspire = institute["value"]
                    print("found inspire id = {}".format(inspire))
                elif institute["institute"] == 'ORCID':
                    orcid = institute["value"]
                    print("found orcid, value = {}".format(orcid))

            cross_check_name = content[0]["authors"][0]["last_name"]

        except IndexError:
            print("[ALERT]: Can't retrieve info for user {} {}"
                  .format(member['first_name'], member['last_name']))
            print("         url = {}".format(url))
            print("         content = {}".format(content))
        except:
            continue

        insert = False
        if bai is not None:
            member['bai'] = bai
            insert = True
        else:
            member['bai'] = ''

        if inspire is not None:
            member['inspire'] = inspire
            insert = True
        else:
            member['inspire'] = ''

        if orcid is not None:
            member['orcid'] = orcid
            insert = True
        else:
            member['orcid'] = ''

        if insert:
            if cross_check_name == member['last_name']:
                member['inspire_last_name'] = ''
                member['cross_check'] = True
            else:
                member['inspire_last_name'] = cross_check_name
                member['cross_check'] = 'INVALID'
            bais.append(member)

    return bais


def run():
    """
    makes all the needed operations to retrive the information
    """
    file_inspire = "bai_from_inspire_invalid"
    file_cern_ccid = "bai_from_ccid_invalid_TESTING"
    skip_inspire_message = """[INFO] File with previous inspire result found {}.json/.csv
       Please, remove it if you want to re-run the whole process for the inspire_id.""".format(file_inspire)
    skip_cern_ccid_message = """[INFO] File with previous inspire result found {}.json/.csv
       Please, remove it if you want to re-run the whole process for the inspire_id.""".format(file_cern_ccid)


    if os.path.isfile(file_inspire + ".json") or os.path.isfile(file_inspire + ".csv"):
        print(skip_inspire_message)
    else:
        # TODO: now evaluating all, but we should filter those who have already a inspireName 
        members_db = get_inspire_orcid_members(demo=False)

        bais = evaluate_inspire_orcid_members(members_db)
        print("dictionary = {}".format(bais))

        save_bai(file_inspire + ".csv", bais, CSV_COLUMNS, ffile='csv')
        save_bai(file_inspire + ".json", bais)

    if os.path.isfile(file_cern_ccid + ".json") or os.path.isfile(file_cern_ccid + ".csv"):
        print(skip_cern_ccid_message)
    else:
        # TODO: now evaluating all, but we should filter those who have already a inspireName 
        members_ccid = get_ccid_members(demo=False)

        bais = evaluate_cern_ccid_members(members_ccid)
        print("members from ccid = \n{}".format(members_ccid))

        save_bai(file_cern_ccid + ".json", bais)
        save_bai(file_cern_ccid + ".csv", bais, CSV_COLUMNS_CCID, ffile='csv')

run()

"""
members_ccid = get_ccid_members(demo=True, rows=20)

bais = evaluate_cern_ccid_members(members_ccid)

print(bais)
"""