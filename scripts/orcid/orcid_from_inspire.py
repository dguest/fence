#!/usr/bin/env python
"""
the scripts goes through all the members in ATLASPO / GLANCE db
and for the one missing their orcid, but with a valid inspire record
gets to inspire to retrieve their orcid, if present
"""

import sys
import json
import requests

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager


query_inspire = """SELECT
                    INSPIRE
                   FROM
                    MEMB
                   WHERE
                      INSPIRE IS NOT NULL
                    AND
                      ORCID IS NULL
                """

search_query = "http://old.inspirehep.net/search?ln=it&cc=HepNames&p=%s&sf=exactfirstauthor&so=a&of=recjson&ot=system_control_number"

dbm = DBManager()


inspires = dbm.execute(query_inspire)

print("result of query is {}".format(inspires))

missing_orcid = {}

for counter, inspire in enumerate(inspires):
    print("evaluating author {} of {}".format(counter + 1, len(inspires)))
    request = requests.get(search_query % inspire[0])

    try:
        content = json.loads(request.content)
        institutes = content[0]["system_control_number"]

        for institute in institutes:
            if institute["institute"] == "ORCID":
                missing_orcid[inspire[0]] = institute["value"]
                print("value = {}".format(institute["value"]))
                print("found missing one # {}".format(len(missing_orcid)))
    except:
        pass
    #print(request.content)


print("dictionary = {}".format(missing_orcid))

with open("orcid_from_inspire.txt", "w+") as out_file:
    out_file.write(json.dumps(missing_orcid))
