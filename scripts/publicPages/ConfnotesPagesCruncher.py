# -*- coding: utf-8 -*-
"""
Implementation of a Step 2 for a public page creation
specific for ATLAS Confnotes
"""
import os
import datetime
import sys
import json

from PublicationCruncher import PublicationCruncher

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

class ConfnotesPagesCruncher(PublicationCruncher):
    """
    The class needed to implement ATLAS confnotes creation over publicationCruncher
    """

    def __init__(self):
        PublicationCruncher.__init__(self)

        self.publication_name = "confnote"
        self.publication_saved_file_path = "./confnotes_data"
        self.publicationDir = ""
        self.superseded_by_paper = dict()
        self.superseded_by_confnote = dict()
        self._processedConfnotes = list()
        self._excludePaths = []        
        self._jinja_template_file = "jinja_confnotes.html"
        self.load_jinja()
        self.load_superseded_info()

    def _assign_date_tag(self):
        print("[INFO] from _assign_date_tag to extract_info_data")
        _, info_date, _ = self.extract_info_data()

        if len(info_date) > 1:
            info_date_manual = self.extended_date(self.pre_format_manual_date(info_date))
        elif "Creation Date" in self.publication_dict and self.publication_dict["Creation Date"]:
            info_date_manual = self.extended_date(self.publication_dict["Creation Date"])
        else:
            info_date_manual = ''
        self.publication_dict["datetag"] = info_date_manual

    def _assign_copyright_tag(self):
        if "datetag" in self.publication_dict and self.publication_dict["datetag"]:
            copyright_tag = self.extract_copyright_date(self.publication_dict["datetag"])
        else:
            copyright_tag = str(datetime.datetime.now().year)
        self.publication_dict["data_copyright"] = copyright_tag

    def _assign_superseded_tag(self):
        if "supersededLink" in self.publication_dict and self.publication_dict["supersededLink"]:
            publication_ref_code = self._fromLinkToRefCode(self.publication_dict["supersededLink"])
            if "CONFNOTES" in self.publication_dict["supersededLink"] \
                and os.path.isdir(self.base_path + "CONFNOTES/" + publication_ref_code):
                self.publication_dict["supersededtag"] = """
                    <P>
                        <TABLE BORDERCOLOR=RED CELLPADDING=5>
                            <TR>
                                <TD ALIGN=CENTER>These preliminary results are superseded by the following conference note: <br><br>
                                    <A HREF='""" + self.publication_dict["supersededLink"] + """'>
                                    """ + self.publication_dict["supersededLabel"] + """</a><br>
                                    <FONT SIZE=-1>These more recent results are based either on the same dataset, or a larger dataset containing these data. 
                                    They may also use improved analysis techniques or an improved treatment of the uncertainties. 
                                    ATLAS recommends to use the more recent results.</FONT>
                                    <br>
                                </TD>
                            </TR>
                        </TABLE>
                    </P>"""

                self.publication_dict["superseded_by_confnote_link"] = self.publication_dict["supersededLink"]
                self.publication_dict["superseded_by_confnote_label"] = self.publication_dict["supersededLabel"]

            if "PAPERS" in self.publication_dict["supersededLink"] \
                and os.path.isdir(self.base_path + "PAPERS/" + publication_ref_code):
                self.publication_dict["supersededtag"] = """
                    <P>
                        <TABLE BORDERCOLOR=RED CELLPADDING=5>
                            <TR>
                                <TD ALIGN=CENTER>These preliminary results are superseded by the following paper: <br><br>
                                    <A HREF='""" + self.publication_dict["supersededLink"] + """'
                                    >""" + self.publication_dict["supersededLabel"] + """
                                    </a><br>
                                    <FONT SIZE=-1>ATLAS recommends to use the results from the paper.</FONT>
                                    <br>
                                </TD>
                            </TR>
                        </TABLE>
                    </P>"""

                self.publication_dict["superseded_by_paper_link"] = self.publication_dict["supersededLink"]
                self.publication_dict["superseded_by_paper_label"] = self.publication_dict["supersededLabel"]

        if "supersededtag" not in self.publication_dict:
            self.publication_dict["supersededtag"] = ''

    def _extract_links(self):
        """
        extracts the links to identify inspire, doi, ...
        """
        if not "Links" in self.publication_dict:
            return
        if not self.publication_dict["Links"]:
            return

        self.publication_dict["links"] = list(self.publication_dict["Links"].split(','))

        for elem in self.publication_dict["links"]:
            if "final_cdsURL" in elem:
                self.publication_dict["link_final_cdsURL"] = elem[1:-1].split('@')
            if "final_arxivURL" in elem:
                self.publication_dict["link_final_arxivURL"] = elem[1:-1].split('@')
            if "phase1_supportDocumentsURL" in elem:
                self.publication_dict["link_phase1_supportDocumentsURL"] = elem[1:-1].split('@')
            if "phase3_cdsURL" in elem:
                self.publication_dict["link_phase3_cdsURL"] = elem[1:-1].split('@')
            if "phase1_indicoURL" in elem:
                self.publication_dict["link_phase1_indicoURL"] = elem[1:-1].split('@')
            if "phase1_cdsURL" in elem:
                self.publication_dict["link_phase1_cdsURL"] = elem[1:-1].split('@')
            if "final_journalPublicationURL" in elem:
                if "doi.org" in elem:
                    self.publication_dict["link_doi"] = elem[1:-1].split('@')
                elif "inspire" in elem:
                    self.publication_dict["link_inspire"] = elem[1:-1].split('@')
                elif "erratum" in elem.lower():
                    self.publication_dict["erratum"] = elem[1:-1].split('@')
                else:
                    self.publication_dict["final_journalPublicationURL"] = elem[1:-1].split('@')
            if "final_physics_briefing" in elem or "phase1_physics_briefing" in elem:
                self.publication_dict["final_physics_briefing"] = elem[1:-1].split('@')[1]




    def _assign_values(self):
        self._assign_data_now()
        self._collect_cds_info(self.publication_dict["Ref Code"])
        self._assign_date_tag()
        self._assign_copyright_tag()
        self._assign_superseded_tag()
        self._extract_links()

    def doFormatting(self):
        """
        collects all the data and manages to execute every needed operation for the publication
        """
        for confnote in self.reader:

            self.publication_dict = confnote
            # we keep track of confnotes that have already been processed
            # to be able to filter among the "extra confnotes"
            self._processedConfnotes.append(self.publication_dict["Ref Code"])

            # skip empty or weird ref codes
            if "Ref Code" not in self.publication_dict \
                or len(self.publication_dict["Ref Code"]) < 7:
                continue

            # corrects wrong entries in the db ending with /
            if len(self.publication_dict["Ref Code"]) > 1 \
                and self.publication_dict["Ref Code"][-1] == '/':
                self.publication_dict["Ref Code"] = self.publication_dict["Ref Code"][:-1]

            # if user wants a single paper to be updated
            # we check if it's the correct ref code. Otherwise we continue
            if isinstance(self.run_on, basestring):
                if self.run_on != "all":
                    if self.run_on != self.publication_dict["Ref Code"]:
                        continue

            self.publication_path = os.path.join(self.base_path,
                                                 self.publicationDir,
                                                 self.publication_dict['Ref Code'])
            # todo: probably we just want to skip the item if the directory doesn't exist
            if not os.path.isdir(self.publication_path):
                try:
                    os.mkdir(self.publication_path)
                except OSError:
                    print("[ERROR] can't create directory {}".format("self.publication_path"))
                    continue

            self._embargo_operations()
            self.dis_embargo_check()

            if not self.publication_needs_update():
                continue

            print("[INFO] processing confnote: %s " % self.publication_dict["Ref Code"])

            self.publication_dict["preliminary_data"] = list()

            self.publication_dict["supersededLabel"], self.publication_dict["supersededLink"] = self.get_superseded_info()
            self.publication_dict["supersededLink"] = self.fix_string(self.publication_dict["supersededLink"])

            self._assign_values()

            self.collect_images()
            self.convert_images()
            self._setup_preliminary_data()
            self._create_jinja_page()


    def get_superseded_info(self):
        """ from loaded dictionaries see if a confnote is indeed superseded and return info """
        if self.publication_dict["Ref Code"] in self.superseded_by_paper:
            return self.superseded_by_paper[self.publication_dict["Ref Code"]].split(";")
        if self.publication_dict["Ref Code"] in self.superseded_by_confnote:
            return self.superseded_by_confnote[self.publication_dict["Ref Code"]].split(";")

        return "", ""

    def _compose_preliminary_header(self, file_name):
        template = """# This file contains preliminary data from the ATLAS collaboration corresponding to Figure FIGURE_NAME in the auxiliary material of conference note REF_CODE.
# 
# Title: CONF_TITLE
# Reference: REF_CODE 
# Link: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/REF_CODE 
# 
# The ATLAS collaboration releases these data for the purpose of preliminary studies. 
# ATLAS recommends not to use these preliminary data for publications. 
#
"""
        template = template.replace("FIGURE_NAME", file_name)
        template = template.replace("REF_CODE", self.publication_dict["Ref Code"])
        template = template.replace("CONF_TITLE", self.publication_dict["Full Title"])
        return template

    def _setup_preliminary_data(self):
        """
        manages confnotes preliminary data files
        rules:
        if .data file is there -> check if the same namefile with _PRELIMINARY.data at the end is not
                                  or if .data file newer than _PRELIMINARY.data
                                  in case, build the _PRELIMINARY.data file
        """
        for file_name in os.listdir(self.publication_path):
            if file_name.startswith("."):
                continue
            if ".data" in file_name:
                if "PRELIMINARY" not in file_name:
                    preliminary_file_name = os.path.join(
                        self.publication_path,
                        file_name[:-5] + "_PRELIMINARY.data"
                        )
                    if not os.path.isfile(preliminary_file_name) or \
                       os.path.getmtime(preliminary_file_name) < os.path.getmtime(os.path.join(self.publication_path, file_name)):
                        header = self._compose_preliminary_header(file_name[:-5])
                        with open(preliminary_file_name, 'w') as outfile, \
                             open(os.path.join(self.publication_path, file_name), 'r') as inputfile:
                            # merging inputfile content with header
                            outfile.write(header)
                            outfile.write(inputfile.read())
                    os.rename(os.path.join(self.publication_path, file_name), os.path.join(self.publication_path, "." + file_name))
                #self.publication_dict["preliminary_data"].append(file_name[:-5].encode("ascii"))
                self.publication_dict["preliminary_data"].append(file_name[:-5].replace("_PRELIMINARY", "").encode("ascii"))

    def _load_superseded_from_db(self):
        """ from the database all the information needed for the confnotes which are superseded """
        mgr = DBManager()
        query_superseded_by_paper = """SELECT
                                    CP.REF_CODE, PA.REF_CODE || ';' || PA.FIGURES
                                    FROM 
                                    ANALYSIS_RELATIONSHIP AR1,
                                    ANALYSIS_RELATIONSHIP AR2,
                                    CONFNOTE_PUBLICATION CP,
                                    PUBLICATION PA
                                    WHERE 
                                    /*AR.ID > 1550 AND*/
                                    AR1.PUBLICATION_ID = CP.ID AND
                                    AR1.TYPE = 'CONF' AND
                                    AR1.CHILD_ID = AR2.ID AND
                                    PA.ID = AR2.PUBLICATION_ID AND
                                    AR2.TYPE = 'PAPER' AND
                                    PA.FIGURES IS NOT NULL
                                    ORDER BY AR1.ID DESC
                                    """
        self.superseded_by_paper = dict(mgr.select(query_superseded_by_paper))

        query_superseded_by_confnotes = """SELECT
                                        CP.REF_CODE, PA.REF_CODE || ';' || PA.CDS_URL
                                        FROM 
                                        ANALYSIS_RELATIONSHIP AR1,
                                        ANALYSIS_RELATIONSHIP AR2,
                                        CONFNOTE_PUBLICATION CP,
                                        CONFNOTE_PUBLICATION PA
                                        WHERE 
                                        /*AR.ID > 1550 AND*/
                                        AR1.PUBLICATION_ID = CP.ID AND
                                        AR1.TYPE = 'CONF' AND
                                        AR1.CHILD_ID = AR2.ID AND
                                        PA.ID = AR2.PUBLICATION_ID AND
                                        AR2.TYPE = 'CONF' AND
                                        PA.CDS_URL IS NOT NULL
                                        ORDER BY AR1.ID DESC
                                        """
        self.superseded_by_confnote = dict(mgr.select(query_superseded_by_confnotes))

        mgr.close()

    def _load_superseded_from_file(self):
        """
        gets superseded information from file which should have been updated on last cron run
        """
        try:
            with open(os.path.join(self.publication_saved_file_path,
                                   "superseded_by_paper.txt"), "r") as file_by_paper:
                self.superseded_by_paper = json.load(file_by_paper)
            with open(os.path.join(self.publication_saved_file_path,
                                   "superseded_by_confnote.txt"), "r") as file_by_confnote:
                self.superseded_by_confnote = json.load(file_by_confnote)
        except Exception, error:
            print("[ERROR] can't read confnote superseded info")
            print(error)

    def _save_superseded_info(self):
        """
        saves information on superseded publication so that the user
        who has no rights to access directly the database can still get some info
        """
        try:
            with open(os.path.join(self.publication_saved_file_path,
                                   "superseded_by_paper.txt"), "w+") as file_by_paper:
                json.dump(self.superseded_by_paper, file_by_paper)
            with open(os.path.join(self.publication_saved_file_path,
                                   "superseded_by_confnote.txt"), "w+") as file_by_confnote:
                json.dump(self.superseded_by_confnote, file_by_confnote)
        except Exception, error:
            print("[ERROR] can't save confnote superseded info")
            print(error)

    def load_superseded_info(self):
        """
        if the user can access the database, gets it and saves the info
        otherwise reads the info from file
        """
        try:
            self._load_superseded_from_db()
            self._save_superseded_info()
        except Exception, error:
            print("""[WARN] You're probably missing rights to access the database.
                The superseded info will be loaded from file.
                Information on superseded publication might not be reliable, 
                please check result page""")
            print(error)
            self._load_superseded_from_file()

    @staticmethod
    def _fromLinkToRefCode(link):
        """
        used to extract ref code of superseders publications
        from ATLAS glance DB we get the link of the superseder, but we need the ref code as well
        """
        if link.find('PAPERS/') != -1:
            start = link.find('PAPERS/') + len('PAPERS/')
        elif link.find('CONFNOTES/') != -1:
            start = link.find('CONFNOTES/') + len('CONFNOTES/')
        elif link.find('PUBNOTES/') != -1:
            start = link.find('PUBNOTES/') + len('PUBNOTES/')
        else:
            return ""

        ref_code = link[start:].rstrip()
        if ref_code[-1] == '/':
            return ref_code[:-1]
        return ref_code


    def processExtraConfnotes(self):

        extra_confnotes_path = os.path.join(self.base_path, self.publicationDir)

        for item in os.listdir(extra_confnotes_path):
            if os.path.isdir(extra_confnotes_path + item):
                if item in self._processedConfnotes:
                    continue

                if item in self._excludePaths:
                    continue

                if len(item) < 7:
                    continue

                self.publication_path = os.path.join(extra_confnotes_path, item)

                self.publication_dict = dict()
                self.publication_dict["Ref Code"] = item

                self._embargo_operations()

                if isinstance(self.run_on, basestring):
                    if self.run_on != "all":
                        if self.run_on != self.publication_dict["Ref Code"]:
                            continue

                if not self.publication_needs_update():
                    continue

                print ("processing extra confnote: %s " % self.publication_dict["Ref Code"])

                self.publication_dict["supersededLabel"], self.publication_dict["supersededLink"] = self.get_superseded_info()
                self.publication_dict["supersededLink"] = self.fix_string(self.publication_dict["supersededLink"])


                self.collect_images()
                self.convert_images()
                #self.createPage(extraConfnote=True)

                self.embargo_check()
                self.dis_embargo_check()

                self._assign_values()
                self._create_jinja_page()

    def processCSV(self):

        self.set_reader()
        self.doFormatting()
