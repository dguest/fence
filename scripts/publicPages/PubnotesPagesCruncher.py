# -*- coding: utf-8 -*-
import os
import sys
import io
import datetime

from PublicationCruncher import PublicationCruncher

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position


class PubnotesPagesCruncher(PublicationCruncher):

    def __init__(self):
        PublicationCruncher.__init__(self)

        self.publication_name = "pubnote"
        self.publicationDir = ""
        self.superseded_by_paper = dict()
        self.superseded_by_pubnote = dict()
        self._processedPubnotes = list()
        self._excludePaths = ["template", "ls", "devtest", "dev", "tt"]
        self._jinja_template_file = "jinja_pubnotes.html"
        self.load_superseded_info()
        self.load_jinja()


    def _assign_date_tag(self):
        print("[DEBUG] extracting info data from assign date tag")
        _, info_date, _ = self.extract_info_data()

        if len(info_date) > 1:
            info_date_manual = self.extended_date(self.pre_format_manual_date(info_date))
        elif "Creation Date" in self.publication_dict and self.publication_dict["Creation Date"]:
            info_date_manual = self.extended_date(self.publication_dict["Creation Date"])
        else:
            info_date_manual = ''
        self.publication_dict["datetag"] = info_date_manual

    def _assign_copyright_tag(self):
        if "datetag" in self.publication_dict and self.publication_dict["datetag"]:
            copyright_tag = self.extract_copyright_date(self.publication_dict["datetag"])
        else:
            copyright_tag = str(datetime.datetime.now().year)
        self.publication_dict["data_copyright"] = copyright_tag

    def _assign_values(self):
        self._assign_data_now()
        self._collect_cds_info(self.publication_dict["Ref Code"])
        self._assign_date_tag()
        self._assign_copyright_tag()
        self._assign_superseded_tag()

    def doFormatting(self):
        """
        collects all the data and manages to execute every needed operation for the publication
        """
        for pubnote in self.reader:

            self.publication_dict = pubnote

            # we keep track of pubnotes that have already been processed
            # to be able to filter among the "extra confnotes"
            self._processedPubnotes.append(pubnote["Ref Code"])

            # skip empty or weird ref codes
            if "Ref Code" not in self.publication_dict or len(self.publication_dict["Ref Code"]) < 7:
                continue

            # corrects wrong entries in the db ending with /
            if len(self.publication_dict["Ref Code"]) > 1 and self.publication_dict["Ref Code"][-1] == '/':
                self.publication_dict["Ref Code"] = self.publication_dict["Ref Code"][:-1]

            # if user wants a single paper to be updated
            # we check if it's the correct ref code. Otherwise we continue
            if isinstance(self.run_on, basestring):
                if self.run_on != "all":
                    if self.run_on != self.publication_dict["Ref Code"]:
                        continue

            self.publication_path = os.path.join(self.base_path, self.publicationDir, self.publication_dict['Ref Code'])
            # todo: probably we just want to skip the item if the directory doesn't exist
            if not os.path.isdir(self.publication_path):
                try:
                    os.mkdir(self.publication_path)
                except OSError:
                    print("[ERROR] can't create directory {}".format(self.publication_path))
                    continue # todo: send notification, otherwise a pubnote might never been created

            self._embargo_operations()
            if not self.publication_needs_update():
                continue

            print("[INFO] processing pubnote: %s " % self.publication_dict["Ref Code"])
            self.publication_dict["supersededLabel"], self.publication_dict["supersededLink"] = self.get_superseded_info()

            self.collect_images()
            self.convert_images()
            #self.embargo_check()
            #self.dis_embargo_check()

            self._assign_values()
            self._create_jinja_page()

    @staticmethod
    def _fromLinkToRefCode(link):
        """
        used to extract ref code of superseders publications
        from ATLAS glance DB we get the link of the superseder, but we need the ref code as well
        """
        if link.find('PAPERS/') != -1:
            start = link.find('PAPERS/') + len('PAPERS/')
        elif link.find('CONFNOTES/') != -1:
            start = link.find('CONFNOTES/') + len('CONFNOTES/')
        elif link.find('PUBNOTES/') != -1:
            start = link.find('PUBNOTES/') + len('PUBNOTES/')
        else:
            return ""

        ref_code = link[start:].rstrip()
        if ref_code[-1] == '/':
            return ref_code[:-1]
        return ref_code



    def _assign_superseded_tag(self):
        if "supersededLink" in self.publication_dict and self.publication_dict["supersededLink"]:
            publication_ref_code = self._fromLinkToRefCode(self.publication_dict["supersededLink"])
            if "PUBNOTES" in self.publication_dict["supersededLink"] \
                and os.path.isdir(self.base_path + "PUBNOTES/" + publication_ref_code):
                self.publication_dict["supersededtag"] = """
                    <P>
                        <TABLE BORDERCOLOR=RED CELLPADDING=5>
                            <TR>
                                <TD ALIGN=CENTER>These preliminary results are superseded by the following conference note: <br><br>
                                    <A HREF='""" + self.publication_dict["supersededLink"] + """'>
                                    """ + self.publication_dict["supersededLabel"] + """</a><br>
                                    <FONT SIZE=-1>These more recent results are based either on the same dataset, or a larger dataset containing these data. 
                                    They may also use improved analysis techniques or an improved treatment of the uncertainties. 
                                    ATLAS recommends to use the more recent results.</FONT>
                                    <br>
                                </TD>
                            </TR>
                        </TABLE>
                    </P>"""

                self.publication_dict["superseded_by_pubnote_link"] = self.publication_dict["supersededLink"]
                self.publication_dict["superseded_by_pubnote_label"] = self.publication_dict["supersededLabel"]

            if "PAPERS" in self.publication_dict["supersededLink"] \
                and os.path.isdir(self.base_path + "PAPERS/" + publication_ref_code):
                self.publication_dict["supersededtag"] = """
                    <P>
                        <TABLE BORDERCOLOR=RED CELLPADDING=5>
                            <TR>
                                <TD ALIGN=CENTER>These preliminary results are superseded by the following paper: <br><br>
                                    <A HREF='""" + self.publication_dict["supersededLink"] + """'
                                    >""" + self.publication_dict["supersededLabel"] + """
                                    </a><br>
                                    <FONT SIZE=-1>ATLAS recommends to use the results from the paper.</FONT>
                                    <br>
                                </TD>
                            </TR>
                        </TABLE>
                    </P>"""

                self.publication_dict["superseded_by_paper_link"] = self.publication_dict["supersededLink"]
                self.publication_dict["superseded_by_paper_label"] = self.publication_dict["supersededLabel"]

        if "supersededtag" not in self.publication_dict:
            self.publication_dict["supersededtag"] = ''


    def _load_superseded_from_db(self):
        """ from the database all the information needed for the confnotes which are superseded """
        mgr = DBManager()
        query_superseded_by_paper = """SELECT
                                    CP.FINAL_REF_CODE, PA.REF_CODE || ';' || PA.FIGURES
                                    FROM 
                                    ANALYSIS_RELATIONSHIP AR1,
                                    ANALYSIS_RELATIONSHIP AR2,
                                    PUBNOTE_PUBLICATION CP,
                                    PUBLICATION PA
                                    WHERE 
                                    /*AR.ID > 1550 AND*/
                                    AR1.PUBLICATION_ID = CP.ID AND
                                    AR1.TYPE = 'PUB' AND
                                    AR1.CHILD_ID = AR2.ID AND
                                    PA.ID = AR2.PUBLICATION_ID AND
                                    AR2.TYPE = 'PAPER' AND
                                    PA.FIGURES IS NOT NULL
                                    ORDER BY AR1.ID DESC
                                    """
        self.superseded_by_paper = dict(mgr.select(query_superseded_by_paper))

        query_superseded_by_pubnotes = """SELECT
                                    CP.FINAL_REF_CODE, PA.FINAL_REF_CODE
                                    FROM 
                                    ANALYSIS_RELATIONSHIP AR1,
                                    ANALYSIS_RELATIONSHIP AR2,
                                    PUBNOTE_PUBLICATION CP,
                                    PUBNOTE_PUBLICATION PA
                                    WHERE 
                                    /*AR.ID > 1550 AND*/
                                    AR1.PUBLICATION_ID = CP.ID AND
                                    AR1.TYPE = 'PUB' AND
                                    AR1.CHILD_ID = AR2.ID AND
                                    PA.ID = AR2.PUBLICATION_ID AND
                                    AR2.TYPE = 'PUB'
                                    ORDER BY AR1.ID DESC
                                    """
        self.superseded_by_pubnote = dict(mgr.select(query_superseded_by_pubnotes))

        mgr.close()



    def load_superseded_info(self):
        try:
            self._load_superseded_from_db()
        except Exception, error:
            print("""[WARN] You're probably missing rights to access the database.
                The superseded info will be loaded from file.
                Information on superseded publication might not be reliable, 
                please check result page""")
            print(error)

    def get_superseded_info(self):
        """ from loaded dictionaries see if a confnote is indeed superseded and return info """
        if self.publication_dict["Ref Code"] in self.superseded_by_paper:
            return self.superseded_by_paper[self.publication_dict["Ref Code"]].split(";")
        if self.publication_dict["Ref Code"] in self.superseded_by_pubnote:
            return self.superseded_by_pubnote[self.publication_dict["Ref Code"]], "https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/" + self.superseded_by_pubnote[self.publication_dict["Ref Code"]]

        return "", ""


    def processExtraPubnotes(self):

        extra_pubnote_path = os.path.join(self.base_path, self.publicationDir)

        for item in os.listdir(extra_pubnote_path):
            if os.path.isdir(extra_pubnote_path + item):
                if item in self._processedPubnotes:
                    continue

                if item in self._excludePaths:
                    continue

                if '-backup' in item:
                    continue
                if '-mcolautt' in item:
                    continue
                if len(item) < 7:
                    continue

                self.publication_path = os.path.join(extra_pubnote_path, item)

                self.publication_dict = dict()
                self.publication_dict["Ref Code"] = item

                self._embargo_operations()

                if isinstance(self.run_on, basestring):
                    if self.run_on != "all":
                        if self.run_on != self.publication_dict["Ref Code"]:
                            continue

                if not self.publication_needs_update():
                    continue

                print("[INFO] processing extra pubnote = %s" % self.publication_dict["Ref Code"])

                self.publication_dict["supersededLabel"], self.publication_dict["supersededLink"] = self.get_superseded_info()

                self.collect_images()
                self.convert_images()
                #self.createPage(extraPubnote=True)
                #self.embargo_check()
                #self.dis_embargo_check()

                self._assign_values()
                self._create_jinja_page()


    def processCSV(self):

        self.set_reader()
        self.doFormatting()
