"""
################################################################################################
# Csv reader:
# load csv file into a _Reader list of dictionaries
# adapted by Maurizio Colautti - maurizio.colautti@cern.ch
# from a script written by Marcello Barisonzi, Bergische Universitatet Wuppertal, May 30th, 2012
################################################################################################
"""

import csv

class CSVReader(object):
    """
    reads a csv file into _Reader as a list of dictionaries
    """

    def __init__(self):
        self.csv_file_name = ""
        self._in_file = None
        self.reader = []

    def __del__(self):
        if isinstance(self._in_file, file):
            self._in_file.close()

    def set_file_name(self, file_name):
        """
        sets file name for csv file
        """
        self.csv_file_name = file_name

    def _set_in_file(self):
        if isinstance(self._in_file, file):
            return
        self._in_file = open(self.csv_file_name)

    def set_reader(self):
        """
        set reader handles now new option from configuration file
        where we can indicate the file name of the csv source file
        and load indeed the file directly, not going through setInfile
        """
        self._set_in_file()
        self.reader = csv.DictReader(self._in_file)
