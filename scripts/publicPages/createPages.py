#!/usr/bin/env python
"""
This script gathers informations about
ATLAS papers, confnotes, pubnotes, plots and combined results
to create ATLAS public pages

# author: Colautti Maurizio - maurizio.colautti@cern.ch
"""
# -*- coding: utf-8 -*-

import json
import os
import traceback
import argparse
import time
import datetime
import sys

from PaperPagesCruncher import PaperPagesCruncher
from ConfnotesPagesCruncher import ConfnotesPagesCruncher
from PubnotesPagesCruncher import PubnotesPagesCruncher
from PlotsPagesCruncher import PlotsPagesCruncher
from UpgradesPagesCruncher import UpgradesPagesCruncher

print("Execution starts at %s" % datetime.datetime.now())
START = time.time()

# to make it correctly run when called from another path as symlink
os.chdir(os.path.dirname(os.path.realpath(__file__)))

PARSER = argparse.ArgumentParser()
PARSER.add_argument('--PAPERS', dest='paper_ref', default=False)
PARSER.add_argument('--CONFNOTES', dest='confnote_ref', default=False)
PARSER.add_argument('--PUBNOTES', dest='pubnote_ref', default=False)
PARSER.add_argument('--PLOTS', dest='plotsonly', default=False)
PARSER.add_argument('--UPGRADES', dest='upgrades', default=False)
PARSER.add_argument("--CDS", dest="forceCDS", action='store_true', default=False)
PARSER.add_argument("--SANDBOX", dest="sandbox", default=False)
PARSER.add_argument("--OUTPUT", dest="output", default=False)
ARGS = PARSER.parse_args()

SETTING_JSON_FILE = "settings.json"
if ARGS.sandbox:
    SETTING_JSON_FILE = ARGS.sandbox

with open(SETTING_JSON_FILE, "r") as setting_file:
    SETTING_TEXT = setting_file.read()
SETTINGS = json.loads(SETTING_TEXT)

if (not ARGS.paper_ref
        and not ARGS.confnote_ref
        and not ARGS.pubnote_ref
        and not ARGS.plotsonly
        and not ARGS.upgrades):
    ARGS.paper_ref = True
    ARGS.confnote_ref = True
    ARGS.pubnote_ref = True
    ARGS.plotsonly = True
    ARGS.upgrades = True

if ARGS.paper_ref:
    BUILD = PaperPagesCruncher()
    try:
        if BUILD.generic_settings_tag in SETTINGS:
            for key, value in SETTINGS[BUILD.generic_settings_tag][0].items():
                setattr(BUILD, key, value)
        if BUILD.publication_name in SETTINGS:
            for key, value in SETTINGS[BUILD.publication_name][0].items():
                setattr(BUILD, key, value)
        BUILD.set_run_on(ARGS.paper_ref)
        if ARGS.forceCDS:
            BUILD.set_force_cds(ARGS.forceCDS)
        if ARGS.output:
            BUILD.set_output(ARGS.output)
        if not ARGS.sandbox:
            BUILD.processCSV()
        BUILD.processExtraPapers()
    except:
        print("****** >> %s" % (traceback.format_exc()))
        os.system("echo 'Error in PaperPagesCruncher\n%s\nrunning command %s' | mail -s 'Error in createPages.py / public atlas pages' mcolautt " % (traceback.format_exc().replace("'", "\""), sys.argv))

if ARGS.confnote_ref:
    BUILD = ConfnotesPagesCruncher()
    try:
        if BUILD.generic_settings_tag in SETTINGS:
            for key, value in SETTINGS[BUILD.generic_settings_tag][0].items():
                setattr(BUILD, key, value)
        if BUILD.publication_name in SETTINGS:
            for key, value in SETTINGS[BUILD.publication_name][0].items():
                setattr(BUILD, key, value)
        BUILD.set_run_on(ARGS.confnote_ref)
        if ARGS.forceCDS:
            BUILD.set_force_cds(ARGS.forceCDS)
        if ARGS.output:
            BUILD.set_output(ARGS.output)
        BUILD.processCSV()
        BUILD.processExtraConfnotes()
    except:
        print("****** >> %s" % (traceback.format_exc()))
        os.system("echo 'Error in ConfnotesPagesCruncher\n%s\nrunning command %s' | mail -s 'Error in createPages.py / public atlas pages' mcolautt " % (traceback.format_exc().replace("'", "\""), sys.argv))

if ARGS.pubnote_ref:
    BUILD = PubnotesPagesCruncher()
    try:
        if BUILD.generic_settings_tag in SETTINGS:
            for key, value in SETTINGS[BUILD.generic_settings_tag][0].items():
                setattr(BUILD, key, value)
        if BUILD.publication_name in SETTINGS:
            for key, value in SETTINGS[BUILD.publication_name][0].items():
                setattr(BUILD, key, value)
        BUILD.set_run_on(ARGS.pubnote_ref)
        if ARGS.forceCDS:
            BUILD.set_force_cds(ARGS.forceCDS)
        if ARGS.output:
            BUILD.set_output(ARGS.output)
        BUILD.processCSV()
        BUILD.processExtraPubnotes()
    except:
        print("****** >> %s" % (traceback.format_exc()))
        os.system("echo 'Error in ConfnotesPagesCruncher\n%s\nrunning command %s' | mail -s 'Error in createPages.py / public atlas pages' mcolautt " % (traceback.format_exc().replace("'", "\""), sys.argv))

if ARGS.plotsonly:
    BUILD = PlotsPagesCruncher()
    try:
        if BUILD.generic_settings_tag in SETTINGS:
            for key, value in SETTINGS[BUILD.generic_settings_tag][0].items():
                setattr(BUILD, key, value)
        if BUILD.publication_name in SETTINGS:
            for key, value in SETTINGS[BUILD.publication_name][0].items():
                setattr(BUILD, key, value)
        BUILD.set_run_on(ARGS.plotsonly)
        if ARGS.forceCDS:
            BUILD.set_force_cds(ARGS.forceCDS)
        if ARGS.output:
            BUILD.set_output(ARGS.output)
        BUILD.process()
    except:
        print("****** >> %s" % (traceback.format_exc()))
        os.system("echo 'Error in PlotsPagesCruncher\n%s\nrunning command %s' | mail -s 'Error in createPages.py / public atlas pages' mcolautt " % (traceback.format_exc().replace("'", "\""), sys.argv))

if ARGS.upgrades:
    print("[DEBUG] running on upgrades")
    BUILD = UpgradesPagesCruncher()
    try:
        if BUILD.generic_settings_tag in SETTINGS:
            for key, value in SETTINGS[BUILD.generic_settings_tag][0].items():
                setattr(BUILD, key, value)
        if BUILD.publication_name in SETTINGS:
            for key, value in SETTINGS[BUILD.publication_name][0].items():
                setattr(BUILD, key, value)
        BUILD.set_run_on(ARGS.upgrades)
        if ARGS.forceCDS:
            BUILD.set_force_cds(ARGS.forceCDS)
        if ARGS.output:
            BUILD.set_output(ARGS.output)
        BUILD.process()
    except:
        print("****** >> %s" % (traceback.format_exc()))
        os.system("echo 'Error in UpgragesPagesCruncher\n%s\nrunning command %s' | mail -s 'Error in createPages.py / public atlas pages' mcolautt " % (traceback.format_exc().replace("'", "\""), sys.argv))


END = time.time()
print("Execution time: ")
print(END - START)
