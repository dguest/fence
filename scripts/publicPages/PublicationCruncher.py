# -*- coding: utf-8 -*-
"""
################################################################################################
# Publication cruncher:
# handles generic operations for the creation of a CERN / ATLAS publication webpage
# author Maurizio Colautti - maurizio.colautti@cern.ch
################################################################################################
"""
import os
import subprocess
import sys
import codecs
import requests

from shutil import copyfile, move
from datetime import datetime, date
from xml.dom.minidom import parseString
import jinja2

import requests

from CSVReader import CSVReader

class PublicationCruncher(CSVReader):
    """
    handles all operations needed for the creation of any generic ATLAS publication
    """

    def __init__(self):
        CSVReader.__init__(self)
        self.publication_name = ""
        self.publication_dict = {}

        self.generic_settings_tag = "generic"

        self.convert_tool_path = None
        self.eps_to_pdf_path = None

        self.publication_path = None
        self.htaccess_template = "template/.htaccess"
        self._needs_update_ignore_list = []
        self._jinja_search_path = "./template"
        self._jinja_template_file = ""
        self._jinja_template = None
        self.run_on = ""
        self.force_cds = False
        self._had_embargo = False
        self._force_convert = False
        self.fsencoding = sys.getfilesystemencoding()
        self.base_path = "/afs/cern.ch/atlas/www/GROUPS/PHYSICS/"

    @staticmethod
    def is_ok_file(file_path):
        """
        checks if the file is ok under conditions:
        - file exists
        - file is not 0 byte
        """
        return os.path.isfile(file_path) and os.stat(file_path).st_size != 0


    def set_run_on(self, value):
        """
        sets command line option for running the script on a specific ref code publication
        """
        self.run_on = value

    def set_force_cds(self, value):
        """
        sets command line option for forcing the script to update metadata from CDS
        """
        self.force_cds = value

    def set_output(self, value):
        """
        sets command line option for forcing the output to a specific folder
        therefore ignoring also the publicationDir subpath
        """
        self.base_path = value
        self.publicationDir = ""

    def _embargo_operations(self):
        # if the publication is in embargo, then
        # - make the path not public for www access
        # - keep track it has been embargoed
        #   (helps the script to run again when embargo is not there anymore)
        self._had_embargo = False
        self.publication_dict["embargo"] = False

        if os.path.isfile(os.path.join(self.publication_path, "embargo")):
            self.publication_dict["embargo"] = True
            if not os.path.isfile(os.path.join(self.publication_path, ".htaccess")):
                print("[INFO] Restricting access to embargoed publication {}"
                      .format(self.publication_path))
                copyfile(self.htaccess_template, os.path.join(self.publication_path, ".htaccess"))
            if not os.path.isfile(os.path.join(self.publication_path, ".hasembargo")):
                print("[INFO] Creation of hasEmbargo file for publication {}"
                      .format(self.publication_path))
                open(os.path.join(self.publication_path, ".hasembargo"), "a").close()

        # there was an embargo, but has been removed? then run on this item
        if os.path.isfile(os.path.join(self.publication_path, ".hasembargo")):
            if not os.path.isfile(os.path.join(self.publication_path, "embargo")):
                print("[INFO] status change: hasEmbargo -> hadEmbargo for publication {}"
                      .format(self.publication_path))
                self._had_embargo = True
                move(os.path.join(self.publication_path, ".hasembargo"),
                     os.path.join(self.publication_path, ".hadembargo"))

        self.embargo_check()
        self.dis_embargo_check()

    def embargo_check(self):
        """
        checks if the publication has the embargo file set
        # TODO: as embargo operations handles the operations should this just check and return?
        """
        self.publication_dict["embargo"] = False
        if os.path.isfile(os.path.join(self.publication_path, "embargo")):
            self.publication_dict["embargo"] = True
            if not os.path.isfile(os.path.join(self.publication_path, ".htaccess")):
                print("[INFO] Restricting access to embargoed publication {} through htaccess"
                      .format(self.publication_path))
                copyfile(self.htaccess_template, os.path.join(self.publication_path, ".htaccess"))
            return True
        return False


    def dis_embargo_check(self):
        """
        checks if the publication is not embargoed, if the .htaccess file has to be removed
        # TODO: as embargo operations handles the operations should this just check and return?
        """
        if not os.path.isfile(os.path.join(self.publication_path, "embargo")):
            if os.path.isfile(os.path.join(self.publication_path, ".htaccess")):
                print("[INFO] Removing embargo restrictions for publication {}"
                      .format(self.publication_path))
                move(os.path.join(self.publication_path, ".htaccess"),
                     os.path.join(self.publication_path, ".dead_htaccess"))

    def publication_needs_update(self):
        """
        rules to determine if the web page file needs to be produced, hence updated
        - 0) if the index file doesn't exists!
        - a) the script has been run on this specific publication -> run
        - b) the embargo file has been deleted -> run
        - c) there's a newer file than index.html -> run
        - d) a new day since last update -> run
        - otherwise, skip
        """

        # 0)
        if not os.path.isfile(os.path.join(self.publication_path, "index.html")):
            return True

        # a)
        if self.run_on == self.publication_dict["Ref Code"] or self.run_on == 'all':
            return True

        # b)
        if self._had_embargo:
            return True

        # c)
        try:
            index_st_mtime = os.stat(os.path.join(self.publication_path, "index.html")).st_mtime
            timestamp = date.fromtimestamp(index_st_mtime)
            if date.today() != timestamp:
                return True
        except IOError, err:
            print(err)

        # d)
        onlyfiles = [f for f in os.listdir(self.publication_path)
                     if os.path.isfile(os.path.join(self.publication_path, f))]

        max_st_mtime = 0
        index_st_mtime = 0
        for item in onlyfiles:
            if item in self._needs_update_ignore_list:
                continue
            if ".backup" in item:
                continue
            try:
                st_mtime = os.stat(os.path.join(self.publication_path, item)).st_mtime
            except IOError, err:
                print("[ERROR] OSError executing st_mtime = os.stat(os.path.join(self.publication_path, item)).st_mtime")
                print(err)
                st_mtime = 0
            if st_mtime > max_st_mtime:
                max_st_mtime = st_mtime
            if item == "index.html":
                index_st_mtime = st_mtime
        if max_st_mtime > index_st_mtime:
            return True

        return False


    @staticmethod
    def needs_to_be_updated(file_path):
        """
        checks if the file needs to be updated under conditions:
        - file doesn't exist
        - or its size is 0 byte
        """
        return not PublicationCruncher.is_ok_file(file_path)
        # return not os.path.isfile(file_path) or os.stat(file_path).st_size == 0


    def _newer_than_index(self, file_path):
        """
        checks if the file is newer than the index file.
        - used because in that case it will always try to convert through
          image formats / build image thumbnail in case some are missing
        """
        if os.path.isfile(os.path.join(self.publication_path, "index.html")):
            image_st_mtime = os.stat(os.path.join(self.publication_path, file_path)).st_mtime
            index_st_mtime = os.stat(os.path.join(self.publication_path, "index.html")).st_mtime
            return image_st_mtime > index_st_mtime
        return True


    def _will_convert(self, file_path):
        """
        in case we miss an image format or thumbnail we want to generate it
        but if we already tried, and failed for some error,
        we might want not to try again
        if force convert is set, we try anyway,
        no matter if the file is an old one (and so had mostly probably already failed)
        """
        if self._force_convert:
            return True

        return self._newer_than_index(file_path)

    @staticmethod
    def is_tool_set(tool, source_file=None, dest_file=None):
        """
        return true if the tool is set,
        false, plus warns out, if it's not set or if it's missing
        """
        if not tool or not os.path.isfile(tool):
            if source_file and dest_file:
                print("[ERROR] Can't convert {} in {} ".format(source_file, dest_file))
            if tool:
                print("[ERROR] Tool set but can't be found on system path: {}".format(tool))
            else:
                print("[ERROR] Convert tool not set for this conversion, please check guidelines")
            return False
        return True

    def convert_images(self):
        """
        if an image format, or a thumbnail needed to be shown on the page is missing
        we (try to) convert them
        - note: conversion sometime fails mostly among from starting eps or pdf file
        """
        for element in (self.publication_dict["images"]
                        + self.publication_dict["tables"]
                        + self.publication_dict["auxiliary"]):
            eps_file_path = os.path.join(self.publication_path, element + ".eps")
            png_file_path = os.path.join(self.publication_path, element + ".png")
            pdf_file_path = os.path.join(self.publication_path, element + ".pdf")
            jpg_file_path = os.path.join(self.publication_path, element + ".jpg")
            thumb_file_path = os.path.join(self.publication_path, ".thumb_" + element + ".png")
            if PublicationCruncher.is_ok_file(eps_file_path):
                if self.needs_to_be_updated(png_file_path) and self._will_convert(eps_file_path):
                    if self.is_tool_set(self.convert_tool_path, eps_file_path, png_file_path):
                        subprocess.call([self.convert_tool_path,
                                         '-density', '250',
                                         eps_file_path, png_file_path])
                if self.needs_to_be_updated(pdf_file_path) and self._will_convert(eps_file_path):
                    if self.is_tool_set(self.eps_to_pdf_path, eps_file_path, "pdf"):
                        subprocess.call([self.eps_to_pdf_path, eps_file_path])
                if self.needs_to_be_updated(thumb_file_path) and self._will_convert(eps_file_path):
                    if self.is_tool_set(self.convert_tool_path):
                        subprocess.call([self.convert_tool_path,
                                         '-trim', '-thumbnail', '480',
                                         png_file_path, thumb_file_path])
            elif PublicationCruncher.is_ok_file(pdf_file_path):
                if self.needs_to_be_updated(png_file_path) and self._will_convert(pdf_file_path):
                    if self.is_tool_set(self.convert_tool_path, pdf_file_path, png_file_path):
                        subprocess.call([self.convert_tool_path,
                                         '-density', '250', '-trim',
                                         '-bordercolor', 'white', '-border', '5',
                                         pdf_file_path, png_file_path])
                if self.needs_to_be_updated(thumb_file_path) and self._will_convert(pdf_file_path):
                    if self.is_tool_set(self.convert_tool_path, pdf_file_path, thumb_file_path):
                        subprocess.call([self.convert_tool_path,
                                         '-density', '250', '-trim',
                                         '-thumbnail', '480',
                                         png_file_path, thumb_file_path])
            elif PublicationCruncher.is_ok_file(jpg_file_path):
                if self.needs_to_be_updated(png_file_path) and self._will_convert(jpg_file_path):
                    if self.is_tool_set(self.convert_tool_path, jpg_file_path, png_file_path):
                        subprocess.call([self.convert_tool_path,
                                         '-density', '250', '-trim',
                                         '-bordercolor', 'white', '-border', '5',
                                         jpg_file_path, png_file_path])
                if self.needs_to_be_updated(thumb_file_path) and self._will_convert(jpg_file_path):
                    if self.is_tool_set(self.convert_tool_path, jpg_file_path, thumb_file_path):
                        subprocess.call([self.convert_tool_path,
                                         '-density', '250', '-thumbnail', '480',
                                         jpg_file_path, thumb_file_path])
            elif PublicationCruncher.is_ok_file(png_file_path):
                if self.needs_to_be_updated(thumb_file_path) and self._will_convert(png_file_path):
                    if self.is_tool_set(self.convert_tool_path, png_file_path, thumb_file_path):
                        subprocess.call([self.convert_tool_path,
                                         '-density', '250', '-thumbnail', '480',
                                         png_file_path, thumb_file_path])

    @staticmethod
    def file_size(size):
        """
        converts file size from byte to easily human readable format
        """
        power = 2**10
        index = 0
        dic_power_n = {0 : '', 1: 'k', 2: 'M', 3: 'g', 4: 't'}
        while size > power:
            size /= power
            index += 1
        return str(size) + dic_power_n[index] + 'B'


    def collect_images(self):
        """
        builds the list of images, tables,
        and a list for auxiliary images and auxiliary tables
        """
        self.publication_dict["images"] = list()
        self.publication_dict["tables"] = list()
        self.publication_dict["auxiliary"] = list()
        self.publication_dict["animated"] = list()
        for file_path in sorted(os.listdir(self.publication_path)):
            file_name, file_extension = os.path.splitext(file_path)
            if file_extension in [".png", ".pdf", ".eps", ".jpg", ".jpeg", ".gif"]:
                if file_name not in self.publication_dict["images"]:
                    if file_name.startswith("fig_"):
                        self.publication_dict["images"].append(file_name)
                if file_name not in self.publication_dict["tables"]:
                    if file_name.startswith("tab_"):
                        self.publication_dict["tables"].append(file_name)
                if file_name not in self.publication_dict["auxiliary"]:
                    if file_name.startswith("figaux_"):
                        self.publication_dict["auxiliary"].append(file_name)
                    if file_name.startswith("tabaux_"):
                        self.publication_dict["auxiliary"].append(file_name)
                if file_name not in self.publication_dict["animated"]:
                    if file_name.startswith("animated_"):
                        self.publication_dict["animated"].append(file_name)

    def _build_image_dict(self, image):
        """
        to feed jinja template, we create a dictionary for each image, table, figaux, tabaux
        with name, prefix, number, size and existence of every different possible format
        """
        dictionary = dict()
        dictionary["name"] = image
        dictionary["prefix"] = image.split('_')[0]
        dictionary["number"] = (image.replace("fig_", "").
                                replace("tab_", "").
                                replace("figaux_", "").
                                replace("tabaux_", "").
                                replace("animated_", ""))

        if self.is_ok_file(os.path.join(self.publication_path, image + ".png")):
            dictionary["png"] = True
            dictionary["png_size"] = self.file_size(os.stat(os.path.join(self.publication_path,
                                                                         image + ".png")).st_size)
        else:
            dictionary["png"] = False
            dictionary["png_size"] = 0

        if self.is_ok_file(os.path.join(self.publication_path, image + ".eps")):
            dictionary["eps"] = True
            dictionary["eps_size"] = self.file_size(os.stat(os.path.join(self.publication_path,
                                                                         image + ".eps")).st_size)
        else:
            dictionary["eps"] = False
            dictionary["eps_size"] = 0

        if self.is_ok_file(os.path.join(self.publication_path, image + ".pdf")):
            dictionary["pdf"] = True
            dictionary["pdf_size"] = self.file_size(os.stat(os.path.join(self.publication_path,
                                                                         image + ".pdf")).st_size)
        else:
            dictionary["pdf"] = False
            dictionary["pdf_size"] = 0

        dictionary["preliminary_image"] = "preliminary_data" in self.publication_dict \
                                          and image in self.publication_dict["preliminary_data"]

        dictionary["caption"] = self.get_description_text(image)

        return dictionary


    def get_description_text(self, item):
        """
        for each image (image+table) the caption is extracted from the matching text file
        """
        description_text_file_path = os.path.join(self.publication_path, item + '.txt')
        try:
            description_text_file = open(description_text_file_path, "r")
            description_text_file_text = description_text_file.read()
            description_text_file.close()
        except IOError:
            return ""

        try:
            return self.fix_string(description_text_file_text.decode(self.fsencoding))
        except:
            print("[ERROR] can't read description text for {}, file {}"
                  .format(self.publication_dict['Ref Code'], item))
            return ""


    def _assign_data_now(self):
        self.publication_dict["data_now"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    @staticmethod
    def _clear_starting_zero_in_date(strg):
        """
        removes starting 0 from dates such as 02-03-2020 -> 2-3-2020
        """
        if strg[0] == '0':
            return strg[1:]
        return strg


    @staticmethod
    def year_month_day(strg):
        """
        converts a standard date format 30-12-2020 to 2020-12-30
        support for extendedDate method
        """
        if strg[2] == '-' and strg[5] == '-':
            return strg[6:10] + "-" + strg[3:5] + "-" + strg[0:2]
        return strg


    def extended_date(self, strg):
        """
        converts the date from 31-12-2020 to 2020-12-30, with months names extended
        """
        strg = self.year_month_day(strg)

        months = {
            "01": "January",
            "02": "February",
            "03": "March",
            "04": "April",
            "05": "May",
            "06": "June",
            "07": "July",
            "08": "August",
            "09": "September",
            "10": "October",
            "11": "November",
            "12": "December",
            }

        if strg[5:7] in months:
            return self._clear_starting_zero_in_date(strg[-2:] + " " + months[strg[5:7]] + " " + strg[:4])

        return strg


    @staticmethod
    def pre_format_manual_date(info_date):
        """
        a date is converted from 31-12-2020 to 2020-12-30
        # actually can't recall why we need this which seems a clone of another method
        """
        info_split = info_date.split('-')
        return "%04d-%02d-%02d" % (int(info_split[2]), int(info_split[1]), int(info_split[0]))


    @staticmethod
    def extract_copyright_date(date_to_check):
        """
        extracing the YEAR of the publication, for copyright indications
        """
        date_to_check.strip()
        return date_to_check[-4:]


    def fix_string(self, string):
        """
        replaces html entities; could be improved with some modules/functions
        """
        string = string.replace(u"−", "-")
        string = string.replace('%3A', ':')
        string = string.replace('%20', ' ')
        string = string.replace('%28', '(')
        string = string.replace('%29', ')')
        string = string.replace('%2C', ',')
        string = string.replace("%u2013", "-")
        string = string.replace("%3F", "?")
        string = string.replace("~", " ")
        string = self.fix_complex_string(string)
        return string


    def fix_complex_string(self, string):
        """
        meant to replace complex syntax, actually just substitutes sqrt expressions
        """
        if "sqrt(" in string and ")" in string[string.find("sqrt(") + len("sqrt("):]:
            chars_to_replace = string[string.find("sqrt(") + len("sqrt(")
                                      :
                                      string.find(")", string.find("sqrt("))]
            string = string.replace('sqrt(' + chars_to_replace + ')', '&radic;' + chars_to_replace)
            string = self.fix_complex_string(string)
        return string


    def extract_info_data(self):
        """
        info file used to extract data such as title, date, cdsid
        these should be used only when cds information is not directly saved in the path
        through methods _collect_cds_info / get_abstract / write_cds_info

        info file is created by the author, with name file format
        ref code + ".info"
        """
        info_file_path = os.path.join(self.publication_path,
                                      self.publication_dict['Ref Code'] + ".info")
        print("[INFO]: reading info data in file: {}".format(info_file_path))
        info_title = ""
        info_date = ""
        info_cdsid = ""
        if self.is_ok_file(info_file_path):
            title_tag = False
            date_tag = False
            cds_tag = False
            cds_file = open(info_file_path, "r")
            cds_file_text = cds_file.read().decode(self.fsencoding).splitlines()
            for element in cds_file_text:
                if "<TITLE>" in element:
                    title_tag = True
                    date_tag = False
                    cds_tag = False
                    continue
                if "<DATE>" in element:
                    title_tag = False
                    date_tag = True
                    cds_tag = False
                    continue
                if "<CDSID>" in element:
                    title_tag = False
                    date_tag = False
                    cds_tag = True
                    continue
                if len(element) > 1 and element[0] == '<' and element[-1] == '>':
                    title_tag = False
                    date_tag = False
                    cds_tag = False
                    continue

                if title_tag:
                    info_title += element
                elif date_tag:
                    if info_date == "":
                        info_date = element
                elif cds_tag:
                    if info_cdsid == "":
                        info_cdsid = element

        info_title = self.fix_string(info_title)

        return info_title, info_date, info_cdsid
        return info_title.encode("utf-8"), info_date.encode("utf-8"), info_cdsid.encode("utf-8")


    def get_abstract(self, cern_code):
        """
        gets the abstract, but also the title and the recid
        of a publication from cds, searching for a cern_code.
        every publication can pass the most convenient cern_code:
        - recid
        - ref code
        a check for ref code consistency has been added at the end before saving results
        # TODO: should move this to a separate class, CDSDocument,
                where every operation is best handled
        """

        
        if (not cern_code.startswith("CERN-EP") and not cern_code.startswith("ATLAS-")
                and not cern_code.startswith("ATL-")):
            print("[WARN] CERN Preprint label missing on Glance, or does not seem to be correctly formatted")
            print("\tgiven = '{}', but expected starting with CERN-EP, ATLAS- or ATL-".format(cern_code))
            print("\tabstract won't be downloaded yet from CDS")
            return "", "", ""
        
        cds_abstract = u""
        cds_title = u""
        cds_id = u""
        cds_report_number = ""
        secondary_cds_report_number = ""

        cds_url = 'https://cds.cern.ch/search?p=' + cern_code + '&of=xm&ot=520&ot=245&ot=088&ot=037'

        try:
            cds_xml = requests.get(cds_url)

            dom_cds = parseString(cds_xml.text.encode(cds_xml.encoding))

            for datafield in dom_cds.getElementsByTagName("datafield"):
                if datafield.attributes["tag"].value == '520':
                    text = ""
                    website = ""
                    for subfield in datafield.getElementsByTagName("subfield"):
                        if subfield.attributes["code"].value == 'a':
                            text = subfield.firstChild.nodeValue
                        if subfield.attributes["code"].value == '9':
                            website = subfield.firstChild.nodeValue
                    if website == "" and cds_abstract == "":
                        cds_abstract = text
                    if website == 'arXiv':
                        cds_abstract = text
                if datafield.attributes["tag"].value == '245':
                    for subfield in datafield.getElementsByTagName("subfield"):
                        if subfield.attributes["code"].value == 'a':
                            if cds_title == "":
                                cds_title = subfield.childNodes[0].nodeValue
                if datafield.attributes["tag"].value == "088":
                    for subfield in datafield.getElementsByTagName("subfield"):
                        if subfield.attributes["code"].value == '9':
                            if cds_report_number == "":
                                cds_report_number = subfield.firstChild.nodeValue
                if datafield.attributes["tag"].value == "037":
                    for subfield in datafield.getElementsByTagName("subfield"):
                        if subfield.attributes["code"].value == 'a':
                            if secondary_cds_report_number == "":
                                secondary_cds_report_number = subfield.firstChild.nodeValue

            if (self.publication_dict["Ref Code"] not in cds_report_number
                    and self.publication_dict["Ref Code"] not in secondary_cds_report_number):
                print("[WARN]: Ref code {} is not in cds_report_number {} within CERN-Preprint label = {}"
                      .format(self.publication_dict["Ref Code"], cds_report_number, cern_code))
                return "", "", ""

        except Exception as err:
            print("[ERROR] Error trying to look for abstract of item = {}".format(cds_url))
            print("[ERROR] {}".format(err.message))
            return "", "", ""


        try:
            cds_id = dom_cds.getElementsByTagName("controlfield")[0].childNodes[0].nodeValue
        except IndexError as err:
            print("[ERROR] cds_url not working is = {}".format(cds_url))
            err.message = err.message + "\n %s" % (cds_xml.text)
            return "", "", ""

        self.write_cds_info(cds_id, cds_title, cds_abstract)

        return cds_id, cds_title, cds_abstract


    def get_cds_info(self):
        """
        reads the info we previosly gathered from cds and saved in .cdsinfo file
        """
        cds_info_file_path = os.path.join(self.publication_path, ".cdsinfo")
        cds_id = ""
        cds_title = ""
        cds_abstract = ""
        if self.is_ok_file(cds_info_file_path):
            cds_file = open(cds_info_file_path, "r")
            cds_file_text = cds_file.read().decode(self.fsencoding).splitlines()
            for idx, element in enumerate(cds_file_text):
                if idx == 1:
                    cds_id = element
                elif idx == 2:
                    cds_title = element
                elif idx == 3:
                    cds_abstract = element

        return cds_id, cds_title, cds_abstract


    def _collect_cds_info(self, search_key):
        """
        manages operations related to retriving CDS info for the paper
        """
        # first, we try to retrieve informations from .cdsinfo file
        # the file is created by the script itself, when we run through get_abstract() method

        (self.publication_dict["cdsId"],
         self.publication_dict["cdsTitle"],
         self.publication_dict["cdsAbstract"]) = self.get_cds_info()

        if self.force_cds:
            if self.run_on != 'all' and self.run_on != True:
                (self.publication_dict["cdsId"],
                 self.publication_dict["cdsTitle"],
                 self.publication_dict["cdsAbstract"]) = self.get_abstract(search_key)
        elif not self.publication_dict["cdsId"] \
           or not self.publication_dict["cdsTitle"] \
           or not self.publication_dict["cdsAbstract"]:
            (self.publication_dict["cdsId"],
             self.publication_dict["cdsTitle"],
             self.publication_dict["cdsAbstract"]) = self.get_abstract(search_key)

        self.publication_dict["cdsAbstract"] = self.fix_string(self.publication_dict["cdsAbstract"])
        self.publication_dict["cdsTitle"] = self.fix_string(self.publication_dict["cdsTitle"])
        self.publication_dict["Full Title"] = self.publication_dict["cdsTitle"]

        info_title, _, info_cdsid = self.extract_info_data()

        if "cdsTitle" not in self.publication_dict or not self.publication_dict["cdsTitle"]:
            self.publication_dict["cdsTitle"] = info_title

        if "cdsId" not in self.publication_dict or not self.publication_dict["cdsId"]:
            self.publication_dict["cdsId"] = info_cdsid

        self._local_pdf()

        if self.publication_dict["cdsId"] and self.publication_name != "paper":
            self._cds_pdf()

    def _cds_pdf(self):
        ## if file ._cds_pdf doesn't exists
        if not os.path.isfile(os.path.join(self.publication_path, ".cds_pdf")):
            ## check if the pdf is online, if so, create ._cds_pdf file
            print("[INFO] Checking once - if success - for publication {} if cds pdf exists".
                  format(self.publication_dict["Ref Code"]))
            request = requests.get("http://cdsweb.cern.ch/record/" \
                                   + self.publication_dict["cdsId"] \
                                   + "/files/" \
                                   + self.publication_dict["Ref Code"] + ".pdf")
            if request:
                with open(os.path.join(self.publication_path, ".cds_pdf"), "w"):
                    pass
            else:
                print("[WARN] cdsId exists, but the pdf doesn't seem to be there")
        ## again, if file ._cds_pdf exists: then create cds_pdf_link
        if os.path.isfile((os.path.join(self.publication_path, ".cds_pdf"))) or self.publication_name == "upgrade":
            self.publication_dict["cds_pdf_link"] = "http://cdsweb.cern.ch/record/" \
                                                    + self.publication_dict["cdsId"] \
                                                    + "/files/" \
                                                    + self.publication_dict["Ref Code"] + ".pdf"


    def _local_pdf(self):
        pdf_path = os.path.join(self.publication_path, self.publication_dict['Ref Code'] + ".pdf")
        if os.path.isfile(pdf_path):
            self.publication_dict["local_pdf"] = self.publication_dict['Ref Code'] + ".pdf"

    def write_cds_info(self, cds_id, cds_title, cds_abstract):
        """
        writes cds info on file to avoid polling cds every time the page is rebuilt
        """
        cds_info_file_path = os.path.join(self.publication_path, ".cdsinfo")
        cds_file = codecs.open(cds_info_file_path, "w")
        cds_file.write("#DO NOT EDIT THIS FILE (YOU MAY DELETE IT)\n")
        cds_file.write(cds_id + "\n")
        cds_file.write(cds_title.encode('utf-8') + "\n")
        cds_file.write(cds_abstract.encode('utf-8') + "\n")
        cds_file.close()

    def load_jinja(self):
        """
        handles operations needed to set up jinja
        """
        template_loader = jinja2.FileSystemLoader(searchpath=self._jinja_search_path)
        template_env = jinja2.Environment(loader=template_loader)
        self._jinja_template = template_env.get_template(self._jinja_template_file)

    def _create_jinja_page(self):
        """
        collects the last info (about images)
        and then writes the publication's page
        """
        self.publication_dict["jinja_images"] = list() # todo: move in init
        for image in self.publication_dict["images"]:
            image_dict = self._build_image_dict(image)
            self.publication_dict["jinja_images"].append(image_dict)
        self.publication_dict["jinja_tables"] = list() # todo: move in init
        for image in self.publication_dict["tables"]:
            image_dict = self._build_image_dict(image)
            self.publication_dict["jinja_tables"].append(image_dict)
        self.publication_dict["jinja_auxs"] = list() # todo: move in init
        for image in self.publication_dict["auxiliary"]:
            image_dict = self._build_image_dict(image)
            self.publication_dict["jinja_auxs"].append(image_dict)
        self.publication_dict["jinja_animated"] = list() # todo: move in init
        for image in self.publication_dict["animated"]:
            image_dict = self._build_image_dict(image)
            self.publication_dict["jinja_animated"].append(image_dict)
        try:
            jinja_output = self._jinja_template.render(data=self.publication_dict)
        except UnicodeDecodeError:
            for key in self.publication_dict:
                try:
                    jinja_output = self._jinja_template.render(data={key: self.publication_dict[key]})
                except:
                    print("[ERROR] ************************\non key = {}\nvalue = {}".format(key, self.publication_dict[key]))
            raise
        try:
            with open(os.path.join(self.publication_path, "index.html"), "w") as f_jinja:
                f_jinja.write(jinja_output.encode("utf-8"))
        except Exception, error:
            print("[ERROR] can't open index.html for writing")
            print(error)
