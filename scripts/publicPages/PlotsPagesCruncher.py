# -*- coding: utf-8 -*-
import os
import io
import datetime

from PublicationCruncher import PublicationCruncher

class PlotsPagesCruncher(PublicationCruncher):

    def __init__(self):
        PublicationCruncher.__init__(self)

        self.publicationDir = "PLOTS/"
        self.publication_name = "plot"
        self._excludePaths = ["tmpdir"]
        self._jinja_template_file = "jinja_plots.html"
        self.load_jinja()

    def extract_copyright_date(self, date):
        possibleYears = list()
        possibleYears = range(1990, 2100)

        for year in possibleYears:
            if str(year) in date:
                extractFrom = date.find(str(year))
                return date[extractFrom:]

    def extract_info_data(self):
        infoFilePath = os.path.join(self.publication_path, self.publication_dict['Ref Code'] + ".info")
        infoTitle = ""
        infoDate = ""
        infoIntro = ""
        if self.is_ok_file(infoFilePath):
            titleTag = False
            dateTag = False
            introTag = False
            cdsFile = open(infoFilePath, "r")
            cdsFileText = cdsFile.read().decode(self.fsencoding).splitlines()
            for idx, element in enumerate(cdsFileText):
                if "<TITLE>" in element:
                    titleTag = True
                    dateTag = False
                    introTag = False
                    continue
                if "<DATE>" in element:
                    titleTag = False
                    dateTag = True
                    introTag = False
                    continue
                if "<INTRO>" in element:
                    titleTag = False
                    dateTag = False
                    introTag = True
                    continue
                if len(element) > 1 and element[0] == '<' and element[-1] == '>':
                    titleTag = False
                    dateTag = False
                    introTag = False
                    continue

                if titleTag:
                    infoTitle += element
                elif dateTag:
                    if infoDate == "":
                        infoDate = element
                elif introTag:
                    if infoIntro == "":
                        infoIntro = element

        return infoTitle, infoDate, infoIntro

    def _assign_date(self, info_date):
        if len(info_date) > len("dd-mm-yyy"):
            return self.extended_date(info_date) + info_date[10:]
        elif info_date:
            return self.extended_date(info_date)
        return ""

    def _create_jinja_page(self):
        self.publication_dict["jinja_images"] = list() # todo: move in init
        for image in self.publication_dict["images"]:
            image_dict = self._build_image_dict(image)
            self.publication_dict["jinja_images"].append(image_dict)
        self.publication_dict["jinja_tables"] = list() # todo: move in init
        for image in self.publication_dict["tables"]:
            image_dict = self._build_image_dict(image)
            self.publication_dict["jinja_tables"].append(image_dict)
        self.publication_dict["jinja_auxs"] = list() # todo: move in init
        for image in self.publication_dict["auxiliary"]:
            image_dict = self._build_image_dict(image)
            self.publication_dict["jinja_auxs"].append(image_dict)
        jinja_output = self._jinja_template.render(data=self.publication_dict)
        with open(os.path.join(self.publication_path, "index.html"), "w") as f_jinja:
            f_jinja.write(jinja_output.encode("utf-8"))


    def process(self):

        plotsPath = os.path.join(self.base_path, self.publicationDir)

        for folder in os.listdir(plotsPath):
            if os.path.isdir(os.path.join(plotsPath, folder)):
                if folder not in self._excludePaths:
                    if "-" in folder:

                        if "-mcolautt" in folder:
                            continue
                        if "-backup" in folder:
                            continue

                        self.publication_path = os.path.join(plotsPath, folder)

                        self.publication_dict = dict()

                        self.publication_dict["Ref Code"] = folder

                        self._embargo_operations()

                        if not self.publication_needs_update():
                            continue

                        print("processing plot {}".format(self.publication_dict["Ref Code"]))

                        self.publication_dict["images"] = list()
                        self.publication_dict["tables"] = list()
                        self.publication_dict["auxiliary"] = list()

                        info_title, info_date, info_intro = self.extract_info_data()

                        self.publication_dict["title"] = self.fix_string(info_title)
                        self.publication_dict["date"] = self._assign_date(info_date)
                        self.publication_dict["data_copyright"] = self.extract_copyright_date(self.publication_dict["date"])
                        self.publication_dict["data_now"] = self._assign_data_now()
                        self.publication_dict["intro"] = self.fix_string(info_intro)

                        self.collect_images()
                        self.convert_images()
                        #self.createPage()
                        #self.embargo_check()
                        #self.dis_embargo_check()

                        self._create_jinja_page()
