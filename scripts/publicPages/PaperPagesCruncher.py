# -*- coding: utf-8 -*-
import os
import io
import sys
import datetime

from PublicationCruncher import PublicationCruncher

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager

class PaperPagesCruncher(PublicationCruncher):

    def __init__(self):
        PublicationCruncher.__init__(self)

        self.publication_name = "paper"
        self.publicationDir = ""
        self._processedPapers = list()
        self._excludePaths = ["template", "ls", "devtest"]
        self._excludeList = ["PERF-2007-01", "STDM-2015-03-backup", "STDM-2017-27"]
        self._jinja_template_file = "jinja_papers.html"
        self.load_jinja()

    def _assign_group(self):
        """
        detects publication main group from ref code
        """
        if 'BPHY' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'B Physics and Light States'
        elif 'CDMA' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Combined Dark matter'
        elif 'EGAM' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'e/gamma'
        elif 'EXOT' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Exotics'
        elif 'FTAG' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Flavour tag'
        elif 'HDBS' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Higgs and Diboson Searches'
        elif 'HIGG' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Higgs'
        elif 'HION' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Heavy Ion Physics'
        elif 'IDTR' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Inner Detector Tracking'
        elif 'JETM' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Jet/Etmiss'
        elif 'MUON' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Muon'
        elif 'PMGR' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Physics Modeling Group'
        elif 'SIMU' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Simulation'
        elif 'STAT' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Statistics Committee'
        elif 'STDM' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Standard Model'
        elif 'SUSY' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'SUSY'
        elif 'TAUP' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Tau'
        elif 'TOPQ' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Top'
        elif 'UPPH' in self.publication_dict["Ref Code"]:
            self.publication_dict["group"] = 'Upgrade Physics'
        else:
            self.publication_dict["group"] = self.publication_dict["Ref Code"][0:4]

    def _assign_short_group(self):
        """
        detects short group label from refcode
        """
        if 'TOPQ' in self.publication_dict["Ref Code"]:
            self.publication_dict["groupShort"] = 'top'
        elif 'STDM' in self.publication_dict["Ref Code"]:
            self.publication_dict["groupShort"] = 'sm'
        elif 'SUSY' in self.publication_dict["Ref Code"]:
            self.publication_dict["groupShort"] = 'susy'
        elif 'HION' in self.publication_dict["Ref Code"]:
            self.publication_dict["groupShort"] = 'hion'
        elif 'HIGG' in self.publication_dict["Ref Code"]:
            self.publication_dict["groupShort"] = 'higgs'
        elif 'EXOT' in self.publication_dict["Ref Code"]:
            self.publication_dict["groupShort"] = 'exotics'
        elif 'DAPR' in self.publication_dict["Ref Code"]:
            self.publication_dict["groupShort"] = 'dapr'
        elif 'TRIG' in self.publication_dict["Ref Code"]:
            self.publication_dict["groupShort"] = 'trigger'
        else:
            self.publication_dict["groupShort"] = self.publication_dict["Ref Code"][0:4]

    def _assign_email_label(self):
        """
        compose the label of the email from group and group short values
        """
        if self.publication_dict["groupShort"] == 'Trigger':
            self.publication_dict["contact_label_tag"] = " trigger coordinators"
        elif self.publication_dict["groupShort"] == 'dapr':
            self.publication_dict["contact_label_tag"] = self.publication_dict["group"] + " coordinators"
        else:
            self.publication_dict["contact_label_tag"] = self.publication_dict["group"] + " conveners"

    def _assign_email_prefix(self):
        """
        detects the prefix of the referring email
        """
        if 'BPHY' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-beauty-conveners'
        elif 'CDMA' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-gen-common-dark-matter-conveners'
        elif 'EGAM' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'perf-egamma-conveners'
        elif 'EXOT' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-exotics-conveners'
        elif 'FTAG' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'perf-flavtag-conveners'
        elif 'HDBS' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-hdbs-conveners'
        elif 'HIGG' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-higgs-conveners'
        elif 'HION' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-hi-conveners'
        elif 'IDTR' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'perf-idtracking-conveners'
        elif 'JETM' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'perf-jets-conveners'
        elif 'MUON' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'perf-muons-conveners'
        elif 'PERF' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'trigger-coordinators'
        elif 'PMGR' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-pmg-conveners'
        elif 'SIMU' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-simu-conveners'
        elif 'STAT' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-stat-conveners'
        elif 'STDM' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-sm-conveners'
        elif 'SUSY' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-susy-conveners'
        elif 'TAUP' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'perf-tau-conveners'
        elif 'TOPQ' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-top-conveners'
        elif 'TRIG' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'trigger-coordinators'
        elif 'UPPH' in self.publication_dict["Ref Code"]:
            self.publication_dict["contact_email_tag"] = 'phys-upgrade-conveners'
        else:
            self.publication_dict["contact_email_tag"] = "phys-" + self.publication_dict["Ref Code"][0:4] + "-conveners"

    def _extract_links(self):
        """
        extracts the links to identify inspire, doi, ...
        """
        if not "Raw Links" in self.publication_dict:
            return
        if not self.publication_dict["Raw Links"]:
            return

        self.publication_dict["links"] = list(self.publication_dict["Raw Links"].split(','))

        for elem in self.publication_dict["links"]:
            if "final_cdsURL" in elem:
                self.publication_dict["link_final_cdsURL"] = elem[1:-1].split('@')
            if "final_arxivURL" in elem:
                self.publication_dict["link_final_arxivURL"] = elem[1:-1].split('@')
            if "phase1_supportDocumentsURL" in elem:
                self.publication_dict["link_phase1_supportDocumentsURL"] = elem[1:-1].split('@')
            if "phase3_cdsURL" in elem:
                self.publication_dict["link_phase3_cdsURL"] = elem[1:-1].split('@')
            if "phase1_indicoURL" in elem:
                self.publication_dict["link_phase1_indicoURL"] = elem[1:-1].split('@')
            if "phase1_cdsURL" in elem:
                self.publication_dict["link_phase1_cdsURL"] = elem[1:-1].split('@')
            if "final_journalPublicationURL" in elem:
                if "doi.org" in elem:
                    self.publication_dict["link_doi"] = elem[1:-1].split('@')
                elif "inspire" in elem:
                    self.publication_dict["link_inspire"] = elem[1:-1].split('@')
                elif "erratum" in elem.lower():
                    self.publication_dict["erratum"] = elem[1:-1].split('@')
                else:
                    self.publication_dict["final_journalPublicationURL"] = elem[1:-1].split('@')
            if "final_physics_briefing" in elem or "phase1_physics_briefing" in elem:
                self.publication_dict["final_physics_briefing"] = elem[1:-1].split('@')[1]


    def _assign_full_title(self):
        """
        full title stores the title that will be placed in the page
        if a cds Title version exists, we use that; otherwise we keep the existing ["Full Title"]
        """
        if "cdsTitle" in self.publication_dict and self.publication_dict["cdsTitle"]:
            self.publication_dict["Full Title"] = self.fix_string(self.publication_dict["cdsTitle"])

    def _assign_publication(self):
        if "Final Publication Reference" in self.publication_dict and len(self.publication_dict["Final Publication Reference"]) > 1:
            if "final_journalPublicationURL" in self.publication_dict:
                self.publication_dict["publication_label"] = self.fix_string(self.publication_dict["final_journalPublicationURL"][0])
                self.publication_dict["publication_link"] = self.fix_string(self.publication_dict["final_journalPublicationURL"][1])
            else:
                self.publication_dict["publication_label"] = self.fix_string( self.publication_dict["Final Publication Reference"].split("@")[0])
                self.publication_dict["publication_link"] = self.fix_string(self.publication_dict["Final Publication Reference"].split("@")[1])
        elif "final_journalPublicationURL" in self.publication_dict:
            self.publication_dict["publication_label"] = self.fix_string( self.publication_dict["final_journalPublicationURL"].split("@")[0])
            self.publication_dict["publication_link"] = self.fix_string(self.publication_dict["final_journalPublicationURL"].split("@")[1])
        else:
            self.publication_dict["publication_label"] = ''
            self.publication_dict["publication_link"] = ''

    def _assign_datetag(self):
        if "Journal Sub" in self.publication_dict and len(self.publication_dict["Journal Sub"]) > 1:
            self.publication_dict["datetag"] = self.extended_date(self.publication_dict["Journal Sub"])
        else:
            self.publication_dict["datetag"] = ''

    def _assign_arxiv(self):
        if "ArXiv" in self.publication_dict and len(self.publication_dict["ArXiv"]) > 1:
            self.publication_dict["arxiv_label_tag"] = "arXiv:" + self.extractArxivCode(self.publication_dict["ArXiv"])
            self.publication_dict["arxiv_link_tag"] = self.fix_string(self.publication_dict["ArXiv"])
            self.publication_dict["pdf_link_tag"] = "https://arxiv.org/pdf/" + self.extractArxivCode(self.publication_dict["ArXiv"])
        else:
            self.publication_dict["arxiv_label_tag"] = ''
            self.publication_dict["arxiv_link_tag"] = ''
            self.publication_dict["pdf_link_tag"] = ''

    def _assign_inspire(self):
        if "link_inspire" in self.publication_dict and len(self.publication_dict["link_inspire"]) >= 1:
            self.publication_dict["inspire_link_tag"] = self.fix_string(self.publication_dict["link_inspire"][1])
        else:
            self.publication_dict["inspire_link_tag"] = ''

    def _assign_hepdata_file_string(self):
        if os.path.isfile(os.path.join(self.publication_path, 'hepdata_info.pdf')):
            self.publication_dict["hepdata_file"] = True

    def _assign_erratum_string(self):
        if "erratum" in self.publication_dict:
            print("[INFO] Erratum = {}".format(self.publication_dict["erratum"]))
            try:
                self.publication_dict["erratum_tag"] = self.publication_dict["erratum"].split("@")[1]
            except:
                pass
            if "erratum_tag" not in self.publication_dict:
                try:
                    self.publication_dict["erratum_tag"] = self.publication_dict["erratum"].split("@")[1]
                except:
                    pass     
            return
        """
        if "Final Publication Reference" in self.publication_dict and len(self.publication_dict["Final Publication Reference"]) >= 1:
            if "ERRATUM" in self.publication_dict["Final Publication Reference"].upper():
                self.publication_dict["erratum_tag"] = self.publication_dict["Final Publication Reference"].split("@")[1]
                return
        """
        self.publication_dict["erratum_tag"] = ''

    def _assign_copyright(self):
        if "Journal Sub" in self.publication_dict and len(self.publication_dict["Journal Sub"]) > 1:
            self.publication_dict["data_copyright"] = self.extract_copyright_date(self.extended_date(self.publication_dict["Journal Sub"]))
        else:
            self.publication_dict["data_copyright"] = str(datetime.datetime.now().year)


    def _retrieve_link_final_cdsURL(self):
        mgr = DBManager()
        query = """SELECT PHASE_LINKS.ALIAS
FROM
PHASE_LINKS,
PHASE,
PUBLICATION
WHERE
PUBLICATION.REF_CODE = '{}'
AND
PHASE.PUBLICATION_ID = PUBLICATION.ID
AND
PHASE.PHASE = 4
AND
PHASE_LINKS.PHASE_ID = PHASE.ID
AND
PHASE_LINKS.TYPE = 'final_cdsURL'""".format(self.publication_dict["Ref Code"])

        result = mgr.select(query)

        if result:
            TEXT = "From: atlaspo@cern.ch\n"
            TEXT += "To: maurizio.colautti@cern.ch\n"
            TEXT += "Subject: Create Pages looking for final_cds_url: \n\n"
            TEXT += "from user {} \nrunning query = {} \nwith result = {}".format(os.getlogin(), query, result)

            _PIPE = os.popen("/usr/sbin/sendmail -t -i", "w")
            _PIPE.write(TEXT)
            STATUS = _PIPE.close()

            if result[0]:
                return result[0][0]

        return ""

    def _assign_values(self):
        self._assign_group()
        self._assign_short_group()
        self._assign_email_label()
        self._assign_email_prefix()
        self._assign_data_now()
        self._assign_publication()
        self._assign_datetag()
        self._assign_arxiv()
        self._extract_links()

        if "link_final_cdsURL" in self.publication_dict:
            self._collect_cds_info(self.publication_dict["link_final_cdsURL"][0])
        else:
            self._collect_cds_info(self._retrieve_link_final_cdsURL())
        self._assign_full_title()
        self._assign_inspire()
        self._assign_hepdata_file_string()
        self._assign_erratum_string()
        self._assign_copyright()

    def doFormatting(self):
        """
        collects all the data and manages to start every needed operation for the publication
        """
        for paper in self.reader:

            self.publication_dict = paper
            if not self.publication_dict["Ref Code"]:
                continue
            # we keep track of papers that have been already processed
            # to be able to filter among the "extra papers"
            self._processedPapers.append(self.publication_dict["Ref Code"])

            # if user wants a single paper to be updated
            # we check if it's the correct ref code. Otherwise we continue
            if isinstance(self.run_on, basestring):
                if self.run_on != "all":
                    if self.run_on != self.publication_dict["Ref Code"]:
                        continue

            if self.publication_dict["Ref Code"].upper() in self._excludeList:
                continue

            self.publication_path = os.path.join(self.base_path, self.publicationDir, self.publication_dict["Ref Code"])
            # todo: probably we just want to skip the item if the directory doesn't exist
            if not os.path.isdir(self.publication_path):
                os.mkdir(self.publication_path)

            self._embargo_operations()

            if not self.publication_needs_update():
                continue

            print("[INFO] processing paper: %s " % self.publication_dict["Ref Code"])

            self.collect_images()
            self.convert_images()
            self.embargo_check()
            self.dis_embargo_check()

            self._assign_values()
            self._create_jinja_page()


    def extractArxivCode(self, code):
        if "/abs/arXiv:" in code:
            return code[code.rfind("/abs/arXiv:") + len("/abs/arXiv:"):].replace('%3A', '')
        return code[code.rfind("/abs/") + len("/abs/"):].replace('%3A', '')

    def processCSV(self):

        self.set_reader()
        self.doFormatting()


    def processExtraPapers(self):

        extra_papers_path = self.base_path + self.publicationDir

        for item in os.listdir(extra_papers_path):
            if os.path.isdir(extra_papers_path + item):
                if item not in self._processedPapers:
                    if item not in self._excludePaths and item not in self._excludeList:
                        self.publication_path = extra_papers_path + item + "/"

                        self.publication_dict = dict()
                        self.publication_dict["Ref Code"] = item

                        # if user wants a single paper to be updated
                        # we check if it's the correct ref code. Otherwise we continue
                        if isinstance(self.run_on, basestring):
                            if self.run_on != "all":
                                if self.run_on != self.publication_dict["Ref Code"]:
                                    continue

                        self._embargo_operations()

                        if not self.publication_needs_update():
                            continue

                        print("[INFO] processing extra paper: %s " % self.publication_dict["Ref Code"])

                        self._assign_values()

                        if ("cdsId" not in self.publication_dict
                                or not self.publication_dict["cdsId"]
                                or "cdsTitle" not in self.publication_dict
                                or not self.publication_dict["cdsTitle"]):
                            # only in one of these cases we go to info file
                            print("[INFO] multiple line condition is true -> ")
                            info_title, info_date, info_cdsid = self.extract_info_data()

                            if "cdsId" not in self.publication_dict or not self.publication_dict["cdsId"]:
                                self.publication_dict["cdsId"] = info_cdsid
                            if "cdsTitle" not in self.publication_dict or not self.publication_dict["cdsTitle"]:
                                self.publication_dict["cdsTitle"] = info_title

                        if "Journal Sub" not in self.publication_dict or not self.publication_dict["Journal Sub"]:
                            print("[INFO] journal sub condition is true -> ")
                            info_title, info_date, info_cdsid = self.extract_info_data()
                            self.publication_dict["Journal Sub"] = info_date


                        self.collect_images()
                        self.convert_images()
                        #self.embargo_check()
                        #self.dis_embargo_check()
                        self._assign_values()
                        self._create_jinja_page()
