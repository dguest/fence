# -*- coding: utf-8 -*-

import os

from PublicationCruncher import PublicationCruncher

class UpgradesPagesCruncher(PublicationCruncher):

    def __init__(self):
        PublicationCruncher.__init__(self)

        self.publicationDir = "UPGRADE/"
        self.publication_name = "upgrade"
        self._excludePaths = ["CERN-LHCC-2017-017"]
        self._jinja_template_file = "jinja_upgrades.html"
        self.load_jinja()


    def _assign_date(self, info_date):
        if len(info_date) > len("dd-mm-yyy"):
            return self.extended_date(info_date) + info_date[10:]
        elif info_date:
            return self.extended_date(info_date)
        return ""



    def process(self):

        upgrades_path = os.path.join(self.base_path, self.publicationDir)

        for folder in os.listdir(upgrades_path):
            if os.path.isdir(os.path.join(upgrades_path, folder)):
                if folder not in self._excludePaths:
                    if "-mcolautt" in folder:
                        continue
                    if "-backup" in folder:
                        continue

                    self.publication_path = os.path.join(upgrades_path, folder)

                    self.publication_dict = {}

                    self.publication_dict["Ref Code"] = folder

                    if isinstance(self.run_on, basestring):
                        if self.run_on != "all":
                            if self.run_on != self.publication_dict["Ref Code"]:
                                continue

                    self._collect_cds_info(self.publication_dict["Ref Code"])


                    print("processing upgrade {}".format(self.publication_dict["Ref Code"]))

                    self.publication_dict["images"] = []
                    self.publication_dict["tables"] = []
                    self.publication_dict["auxiliary"] = []

                    info_title, info_date, info_intro = self.extract_info_data()

                    self.publication_dict["title"] = self.fix_string(info_title)
                    self.publication_dict["date"] = self._assign_date(info_date)
                    self.publication_dict["data_copyright"] = self.extract_copyright_date(self.publication_dict["date"])
                    self._assign_data_now()
                    self.publication_dict["intro"] = self.fix_string(info_intro)

                    self.collect_images()
                    self.convert_images()
                    #self.createPage()
                    #self.embargo_check()
                    #self.dis_embargo_check()

                    self._create_jinja_page()
