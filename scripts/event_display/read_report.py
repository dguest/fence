#!/usr/bin/python2

"""
reads the report and builds the pages with the images grouped by darkness
"""

import os
import json
from collections import defaultdict
import jinja2

import pkg_resources
pkg_resources.require("Matplotlib==1.2.0")

# import matplotlib for headless terminal
# backend has to be set first
# http://stackoverflow.com/questions/5503601/python-headless-matplotlib-pyplot
import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
import pylab
import numpy as np

OUTPUT_DIR = "./output_files/"


def create_bar_report(bars, labels, title="", color='b', outfile="foo.png", max_500=True):
    """ creates the bar plot """

    print("[INFO] generating file: {}".format(outfile))

    if max_500:
        bars = [x if x < 500 else 500 for x in bars]

    ind = np.arange(len(bars))  # the x locations for the groups
    width = 1.0 #0.7       # the width of the bars

    _fig = pylab.figure()
    _fig.clf()

    pylab.title(title, fontsize=16, weight='bold')

    axes = _fig.add_subplot(111)
    rects = axes.bar(ind, bars, width, color=color)

    axes.set_xlim([0, len(bars)])

    new_labels = list()
    for i, label in enumerate(labels):
        if label.endswith('-01') or label.endswith('-07'):
            new_labels.append(label)
        else:
            new_labels.append('')

    axes.set_xticks(ind)
    axes.set_xticklabels(new_labels)

    for i in axes.get_xticklabels():
        i.set_rotation(70)
        i.set_size("x-small")

    avg = 0

    max_height = 0
    for rect in rects:
        height = rect.get_height()
        avg += height
        if height > max_height:
            max_height = height

    axes.set_ylim([0, max_height*1.3])

    avg = 1. * avg / len(rects)

    pylab.axhline(y=avg, color=color, ls='--')

    pylab.savefig(os.path.join(OUTPUT_DIR, outfile))
    pylab.savefig(os.path.join(OUTPUT_DIR, outfile[:-3] + "pdf"))

    pylab.close()

    return



REPORT = {}

with open("report.txt", "r") as in_file:
    REPORT = json.load(in_file)

print("[DEBUG] # of items {}".format(len(REPORT)))

DARKNESS_DICT = defaultdict(int)
DATA_SCALE = defaultdict(list)

TAGS_LABELS = []
TAGS_REPORT = {}

for key, element in REPORT.items():

    dark = element["darkness"]

    image = {}
    image['href'] = key.replace("/afs/cern.ch/atlas/www/", "https://atlas.web.cern.ch/Atlas/").replace("fig_", ".thumb_fig_").replace("figaux_", ".thumb_figaux_")
    image['href'] = image['href'] + ".png"
    image['page'] = image['href'][0:image['href'].rfind("/")]
    image['image_name'] = image['href'][image['href'].rfind("/")+1:].replace(".png", "").replace(".thumb_", "")
    image['tag'] = element['tag']
    image['darkness'] = element['darkness']
    image['refcode'] = element['refcode']
    if not dark:
        continue

    # if image['tag'] == "visualisation, event" and image['darkness'] > 130:
    #     print("[DEBUG] visualisation, event, href = {}".format(image['href']))

    this_darkness = 0
    for dark_range in range(15, 261, 5):
        if dark < dark_range:
            DARKNESS_DICT[str(dark_range)] += 1
            DATA_SCALE[str(dark_range)].append(image)
            this_darkness = dark_range
            break

    if image['tag'] not in TAGS_LABELS:
        TAGS_LABELS.append(image['tag'])

    if image['tag'] not in TAGS_REPORT:
        TAGS_REPORT[image['tag']] = {}

    if str(this_darkness) not in TAGS_REPORT[image['tag']]:
        TAGS_REPORT[image['tag']][str(this_darkness)] = 1
    else:
        TAGS_REPORT[image['tag']][str(this_darkness)] += 1

#print("[DEBUG] TAGS_REPORT = {}".format(TAGS_REPORT))

for tag in TAGS_LABELS:
    keys = sorted([int(X) for X in TAGS_REPORT[tag]])
    #print("keys for label \"{}\" are \n{}".format(tag, keys))
    dark_report = []
    dark_report_labels = []
    for dark_range in range(15, 261, 5):
        if str(dark_range) in TAGS_REPORT[tag]:
            dark_report.append(TAGS_REPORT[tag][str(dark_range)])
        else:
            dark_report.append(0)
        dark_report_labels.append(str(dark_range))

    out_file = tag.replace(", ", "_").replace(" ", "-") + ".png"
    create_bar_report(dark_report, dark_report_labels,
                      title="Tag:   {}   - range 15-255".format(tag),
                      outfile=out_file)

#print("[INFO] darkness list = {}".format(darkness))

# load jinja settings
TEMPLATE_LOADER = jinja2.FileSystemLoader(searchpath="./template")
TEMPLATE_ENV = jinja2.Environment(loader=TEMPLATE_LOADER)
JINJA_TEMPLATE = TEMPLATE_ENV.get_template("jinja_report.html")

# print("[DEBUG] keys = {}".format(DATA_SCALE.keys()))
# KEYS = sorted([int(X) for X in DATA_SCALE.keys()])
# print("[DEBUG] keys = {}".format(keys))
# element["keys"] = KEYS

for key, element in DATA_SCALE.items():
    jinja_output = JINJA_TEMPLATE.render(data=element)

    with open(os.path.join(OUTPUT_DIR, "data_{}.html".format(key)), "w") as f_jinja:
        print("[INFO] generating darkness level page: {}".format(key))
        f_jinja.write(jinja_output.encode("utf-8"))

DARK_REPORT = []
DARK_REPORT_LABELS = []
for dark_range in range(15, 261, 5):
    if str(dark_range) in DARKNESS_DICT:
        DARK_REPORT.append(DARKNESS_DICT[str(dark_range)])
    else:
        DARK_REPORT.append(0)
    DARK_REPORT_LABELS.append(str(dark_range))

#print("dark_report = {}".format(DARK_REPORT))

create_bar_report(DARK_REPORT, DARK_REPORT_LABELS,
                  title="Occurencies of luminonisity in range 15-255",
                  outfile="darkness_report.png")

#print("[DEBUG] dark report = {}".format(DARK_REPORT))
