#!/usr/bin/python2

"""
temporary script to create the json file needed for
the event display page starting from the url of the image
found in the txt file, building the full dict required
"""

import sys
import os
import json
from datetime import datetime
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager
from dates import *

PAPERS_REF_CODES = []
CONFNOTES_REF_CODES = []

def extract_ref_code(text):
    """ self explanatory """
    text = text.replace("https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/", "")
    text = text.replace("https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/", "")

    try:
        slash_pos = text.find("/")
        return text[0:slash_pos]
    except:
        pass


def extract_name(text):
    """ extract image name """
    try:
        thumb_pos = text.find(".thumb_")
        text = text[thumb_pos + len(".thumb_"):]
    except:
        pass

    return text.replace(".png", "")

def extract_group(text):
    """ self explanatory """
    if is_paper(text):
        ref_code = extract_ref_code(text)
        return ref_code[0:4]
    return ""


def build_figure_link(text):
    """ self explanatory """
    try:
        last_slash = text.rfind("/")
        return text[0:last_slash]
    except:
        pass

def build_link(text):
    """ self explanatory """
    return text.replace(".thumb_", "")

def is_paper(text):
    """ self explanatory """
    return "papers" in text.lower()

def is_confnote(text):
    """ self explanatory """
    return "confnotes" in text.lower()

def get_caption(text):
    """ get caption for specific image """
    text = text.replace("https://atlas.web.cern.ch/Atlas/", "/afs/cern.ch/atlas/www/")
    with open(text.replace(".png", ".txt").replace(".thumb_", "")) as in_file:
        return in_file.read()

def get_papers_from_db():
    """ retrieves info from db for papers """
    if PAPERS_REF_CODES:
        query = """SELECT
                REF_CODE,
                ID,
                FULL_TITLE, 
                JOURNAL_SUB,
                LISTAGG(SUBGROUP_ID, ',') WITHIN GROUP (ORDER BY SUBGROUP_ID) "SUBGROUPS"
                FROM
                PUBLICATION LEFT JOIN 
                PUBLICATION_ANALYSISSUBGROUP
                ON  PUBLICATION.ID = PUBLICATION_ANALYSISSUBGROUP.PUBLICATION_ID
                WHERE
                REF_CODE IN ({})
                GROUP BY (REF_CODE, ID, FULL_TITLE, JOURNAL_SUB)""".format(PAPERS_REF_CODES)

        query = query.replace("[", "").replace("]", "")
        dbm = DBManager()
        data = dbm.execute(query)
        print("[INFO] Running query \n{}".format(query))

        print("[DEBUG] papers data from db:\n{}".format(data))
        return data

    return ""

def get_confnotes_from_db():
    """ retrieves info from db for confnotes """
    if CONFNOTES_REF_CODES:
        query = """SELECT
                REF_CODE,
                ID,
                FULL_TITLE, 
                JOURNAL_SUB,
                LEAD_GROUP,
                LISTAGG(SUBGROUP_ID, ',') WITHIN GROUP (ORDER BY SUBGROUP_ID) "SUBGROUPS"
                FROM
                CONFNOTE_PUBLICATION LEFT JOIN
                CONFNOTE_PUBLICATION_SUBGROUP
                ON CONFNOTE_PUBLICATION.ID = CONFNOTE_PUBLICATION_SUBGROUP.PUBLICATION_ID
                WHERE
                REF_CODE IN ({})
                GROUP BY (REF_CODE, ID, FULL_TITLE, JOURNAL_SUB, LEAD_GROUP)""".format(CONFNOTES_REF_CODES)

        query = query.replace("[", "").replace("]", "")
        dbm = DBManager()
        data = dbm.execute(query)

        print("[INFO] Running query \n{}".format(query))

        print("[DEBUG] confnotes data from db:\n{}".format(data))
        return data

    return ""

def get_subgroups_from_db():
    query = """SELECT
                SUBGROUP_ID,
                DESCRIPTION
               FROM 
                SUBGROUPS
            """

    dbm = DBManager()
    data = dbm.execute(query)

    return data

def get_confnote_date(ref_code):
    """ confnote dates must be retrieved from info file in publication path """
    name_file = os.path.join("/afs/cern.ch/atlas/www/GROUPS/PHYSICS/CONFNOTES",
                             ref_code,
                             ref_code + ".info")
    print("[DEBUG] name_file: {}".format(name_file))

    with open(name_file) as in_file:
        file_content = in_file.read()

    next_line = False
    for line in file_content.splitlines():
        if next_line:
            return line
        if '<DATE>' in line:
            next_line = True

    return ""

def convert_datetime(date):
    """ from datetime to string date """
    return date.strftime("%d-%m-%Y")


RECORDS = []

last_ref_code = ""

with open("thumbnails_event_display.txt") as in_file:
    LINES = in_file.read()
    for path in LINES.splitlines():
        print("item: {}".format(path))

        ref_code = extract_ref_code(path)

        thumb_dict = {}
        if ref_code != last_ref_code:
            pub_dict = {}
            pub_dict["ref_code"] = ref_code
            pub_dict["thumbnail"] = []
            pub_dict["link_figure_page"] = build_figure_link(path)

            if is_paper(path):
                pub_dict["group"] = extract_group(path)
                PAPERS_REF_CODES.append(ref_code)
                pub_dict["type"] = "paper"
            elif is_confnote(path):
                CONFNOTES_REF_CODES.append(ref_code)
                pub_dict["type"] = "confnote"

        thumb_dict["name"] = extract_name(path)
        thumb_dict["link_thumbnail"] = path
        thumb_dict["link"] = build_link(path)
        thumb_dict["caption"] = get_caption(path)
        thumb_dict["keywords"] = ""

        pub_dict["thumbnail"].append(thumb_dict)

        if ref_code != last_ref_code:
            RECORDS.append(pub_dict)

        last_ref_code = ref_code

papers_info = get_papers_from_db()
confnotes_info = get_confnotes_from_db()
subgroups_ids = get_subgroups_from_db()

# adding info extracted from the DB to confnotes
for confnote in confnotes_info:
    print("REF in confnotes_info: {}".format(confnote[0]))
    for record in RECORDS:
        if record['ref_code'] == confnote[0]:
            record['title'] = confnote[2]
            record['link_glance'] = "https://atlas-glance.cern.ch/atlas/analysis/confnotes/details.php?id=" + str(confnote[1])
            record['date'] = extract_date(get_confnote_date(record['ref_code']), format_date='yyyy-mm-dd')
            record['group'] = confnote[4]
            record['subgroups'] = []
            if confnote[5]:
                for subgroup in confnote[5].split(","):
                    for subgroup_id in subgroups_ids:
                        if subgroup_id[0] == subgroup:
                            conf_subgroup = {'id': subgroup}
                            conf_subgroup['description'] = subgroup_id[1]
                    record['subgroups'].append(conf_subgroup)

# adding info extracted from the DB to papers
for paper in papers_info:
    print("REF in papers_info: {}".format(paper[0]))
    for record in RECORDS:
        if record['ref_code'] == paper[0]:
            record['title'] = paper[2]
            record['link_glance'] = "https://atlas-glance.cern.ch/atlas/analysis/papers/details.php?id=" + str(paper[1])
            record['date'] = extract_date(convert_datetime(paper[3]), format_date='yyyy-mm-dd')
            record['subgroups'] = []
            if paper[4]:
                for subgroup in paper[4].split(","):
                    for subgroup_id in subgroups_ids:
                        if subgroup_id[0] == subgroup:
                            paper_subgroup = {'id': subgroup}
                            paper_subgroup['description'] = subgroup_id[1]
                    record['subgroups'].append(paper_subgroup)

#print("RECORDS:\n{}".format(RECORDS))


with open("json_report.json", "w") as csv_file:
    json.dump(RECORDS, csv_file)
