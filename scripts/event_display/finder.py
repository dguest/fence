#!/usr/bin/python3.6

"""
finds all the images which might be, according to their captions,
event display images
"""

import os
import sys
import re
import json
import numpy as np
from PIL import Image

PAPERS_PATH = "/afs/cern.ch/atlas/www/GROUPS/PHYSICS/PAPERS"
CONFNOTE_PATH = "/afs/cern.ch/atlas/www/GROUPS/PHYSICS/CONFNOTES"
TAGS = ["event display", "sample event", "event displays", "candidate event", "visualisation",
        "event", "display"]
FOUND = []
ERROR_FILES = []
INSPECTED = {}

def valid_folders(base_path):
    """
    first, we find all the valid folders
    """
    folders_to_check = []

    for element in os.listdir(base_path):
        if os.path.isdir(os.path.join(base_path, element)):
            if re.match("^[A-Za-z]{4}-[0-9]{4}-[0-9]{2}$", element):
                #print("[DEBUG] base_path folders = {}".format(element))
                folders_to_check.append(element)
            elif re.match("^ATLAS-CONF-[0-9]{4}-[0-9]{3}$", element):
                folders_to_check.append(element)

    return sorted(folders_to_check)


def inspect_folders(base_path, folders):
    """
    for every folder, read all the (figures?) captions
    """
    for counter, folder in enumerate(folders):
        print("[INFO] running on folder # {} of {} = {}".format(counter, len(folders), folder))
        for element in os.listdir(os.path.join(base_path, folder)):
            if element.endswith(".txt") and "fig" in element and not element.startswith("."):
                #print("[DEBUG] file to check = {}".format(element))
                with open(os.path.join(base_path, folder, element)) as in_file:
                    try:
                        file_content = in_file.read().lower()
                    except Exception as err:
                        error = {}
                        error["file"] = os.path.join(base_path, folder, element)
                        error["error"] = err
                        ERROR_FILES.append(error)
                    else:
                        for tag in TAGS:
                            if tag in file_content:
                                #print("[DEBUG] matches. Tag = {}\n\tcontent = {}".format(tag, file_content))
                                found = {}
                                found["tag"] = tag
                                found["refcode"] = folder
                                found["path"] = os.path.join(base_path, folder, element).replace(".txt", "")
                                FOUND.append(found)

# def image_darkness(file_path):
#     """
#     given an image, it calculates how dark it is in average
#     """
#     try:
#         img = Image.open("{}.png".format(file_path).replace("fig_", ".thumb_fig_").replace("figaux_", ".thumb_figaux_"))
#     except:
#         print("[ERROR] png version of file {} not found".format(file_path))
#         return None
#     img_width, img_height = img.size
#     channels = len(img.getbands())
#     img_values = list(img.getdata())
#     img_values = np.array(img_values).reshape((img_height, img_width, channels))
#     img_values = img_values[:, :, 0:3]

#     row, column, _ = img_values.shape

#     darkness = 0
#     pixel_count = 0
#     for i in range(row):
#         for x in range(column):
#             pixel_count += 1
#             darkness += (img_values[i][x].sum() / 3)
#     return darkness/pixel_count

def image_darkness(file_path):
    """
    given an image, it calculates how dark it is in average
    """
    try:
        png = Image.open("{}.png".format(file_path).replace("fig_", ".thumb_fig_").replace("figaux_", ".thumb_figaux_"))
    except:
        print("[ERROR] png version of file {} not found".format(file_path))
        return None
    png_bands = len(png.getbands())
    
    darkness = 0
    if png_bands == 4:
        png.load()

        background = Image.new("RGB", png.size, (255, 255, 255))
        background.paste(png, mask=png.split()[3])

        img_width, img_height = background.size
        bands = background.getbands()
        img_values = list(background.getdata())
        img_values = np.array(img_values).reshape((img_height, img_width, 3))
        img_values = img_values[:, :, 0:3]

        row, column, _ = img_values.shape

        darkness = 0
        pixel_count = 0
        for i in range(row):
            for x in range(column):
                pixel_count += 1
                darkness += (img_values[i][x][0] + img_values[i][x][1] + img_values[i][x][2]) / 3

        darkness = darkness / pixel_count

    elif png_bands == 1:
        darkness = 255

    elif png_bands == 3:

        img_width, img_height = png.size
        img_values = list(png.getdata())
        img_values = np.array(img_values).reshape((img_height, img_width, 3))
        img_values = img_values[:, :, 0:3]

        row, column, _ = img_values.shape

        darkness = 0
        pixel_count = 0
        for i in range(row):
            for x in range(column):
                pixel_count += 1
                darkness += (img_values[i][x][0] + img_values[i][x][1] + img_values[i][x][2]) / 3

        darkness = darkness / pixel_count

    return darkness

def inspect_images():
    """
    goes through images found and inspects them
    """
    for counter, image in enumerate(FOUND):
        print("[INFO] inspecting image {} of {}".format(counter, len(FOUND)))
        if image["path"] in INSPECTED:
            INSPECTED[image["path"]]["tag"] = INSPECTED[image["path"]]["tag"] + ", " + image["tag"]
        else:
            inspected = {}
            inspected["tag"] = image["tag"]
            inspected["refcode"] = image["refcode"]

            darkness = image_darkness(image["path"])
            inspected["darkness"] = darkness

            INSPECTED[image["path"]] = inspected

        if (counter + 1) % 100 == 0:
            with open("report.txt", "w") as report_file:
                report_file.write(json.dumps(INSPECTED))

    if (counter + 1) % 100 == 0:
        with open("report.txt", "w") as report_file:
            report_file.write(json.dumps(INSPECTED))


def run():
    """
    run the task
    """
    paths = [PAPERS_PATH, CONFNOTE_PATH]

    for path in paths:
        folders = valid_folders(path)
        inspect_folders(path, folders)

    #print("[DEBUG] found elements = \n{}".format(FOUND))
    #print("[DEBUG] error on files = \n{}".format(ERROR_FILES))

    print("[INFO] matches count = {}".format(len(FOUND)))
    print("[INFO] errors count = {}".format(len(ERROR_FILES)))

    inspect_images()

    print("[INFO] inspected = {}".format(INSPECTED))

    with open("output.txt", "w") as out_file:
        out_file.write(json.dumps(FOUND))

    with open("errors.txt", "w") as out_file:
        try:
            out_file.write(json.dumps(ERROR_FILES))
        except:
            print("[ERROR] writing content = {}".format(ERROR_FILES))

run()
