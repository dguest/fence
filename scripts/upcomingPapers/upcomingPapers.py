#!/usr/bin/python2
"""
the script check through INSPIRE api
for publication that have been published
"""

import sys
import os
import json
from datetime import datetime
import requests

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager


def published_check(cern_preprint_reference):
    """
    retrieves info from INSPIRE and checks for
    imprint data
    and
    collection.primary
    """
    url = ("http://old.inspirehep.net/search?p=r+" +
           cern_preprint_reference +
           "&of=recjson&ot=collection,imprint,recid")
    response = requests.get(url)
    loaded_json = json.loads(response.text)

    print("checking paper %s" % cern_preprint_reference)
    local_published = False
    published_data = ""
    local_recid = ""
    for item_response in loaded_json:
        if item_response["imprint"] and "date" in item_response["imprint"]:
            #print(x["imprint"]["date"])
            published_data = item_response["imprint"]["date"]
            local_recid = item_response["recid"]
        for collection in item_response["collection"]:
            if collection["primary"].lower() == "published":
                local_published = True

    if local_published:
        print("the paper has been published")
        print("info from inspire returns: ")
        print(loaded_json)
        return True, published_data, local_recid

    return False, "", ""

def retrieve_new_publications():
    """
    gets from the database the list of publications
    that we must check with inspire
    """
    mgr = DBManager()
    local_to_be_checked = mgr.execute("""
        SELECT 
            PUBLICATION.ID, 
            PUBLICATION.REF_CODE,
            PHASE_3.CERN_REFERENCE_NUMBER
        FROM 
            PUBLICATION,
            PHASE,
            PHASE_3
        WHERE
            PHASE.PUBLICATION_ID = PUBLICATION.ID
            AND PHASE.PHASE = 3
            AND PHASE_3.PHASE_ID = PHASE.ID
            AND (instr('finished', PUBLICATION.STATUS) 
                 + instr('phase3_closed', PUBLICATION.STATUS) 
                 + instr('updated_Pubcomm', PHASE_3.STATE) 
                 + instr('updated_Spokesperson', PHASE_3.STATE) 
                 + instr('submission_active', PUBLICATION.STATUS)) > 0
            AND PHASE_3.CERN_REFERENCE_NUMBER IS NOT NULL
        GROUP BY
            PUBLICATION.ID,
            PUBLICATION.REF_CODE,
            PHASE_3.CERN_REFERENCE_NUMBER
        ORDER BY
            PUBLICATION.ID
        """)
    mgr.close()
    return local_to_be_checked

def notify_published(item_publication):
    """
    sends the email for the notification
    """
    text = "From: Upcoming Papers <atlaspo@cern.ch>\n"
    # text += "To: atlas-phys-office-pub@cern.ch\n"
    text += "Cc: mcolautt@cern.ch\n"
    text += "Subject: Paper %s published\n" % item_publication["ref_code"]
    text += "Dear PhysOfficer,\n\n"
    text += "Paper %s has been published. \n\n" % item_publication["ref_code"]
    text += "Please use this link:\n\n http://inspirehep.net/literature/%s\n\n" % item_publication["recid"]
    text += "and check that the DOI link is there and points to an existing page\n"
    text += "and update Glance if necessary.\n\n\n\n"
    text += "Note for the developer: the script is in testing mode: forward email to: "
    text += "atlas-phys-office-pub@cern.ch"

    process = os.popen("/usr/sbin/sendmail -t -i", "w")
    process.write(text)
    status = process.close()

    if not status:
        print "Mail sent."

print("Execution starts at {}".format(str(datetime.now())))

TO_BE_CHECKED = retrieve_new_publications()

try:
    with open("notified.json", "r") as infile:
        ALREADY_NOTIFIED = json.load(infile)
        print("publication already notified")
        print(ALREADY_NOTIFIED)
except:
    print("Problem opening notified publication file. Maybe it doesn't exist?")
    ALREADY_NOTIFIED = list()

UPDATE_NOTIFIED = False
for item in TO_BE_CHECKED:
    item_already_checked = False
    for notfified in ALREADY_NOTIFIED:
        if notfified["preprint"] == item[2].strip():
            item_already_checked = True
    if item_already_checked:
        continue
    #print(item)
    json_item = {"paper_id" : item[0], "ref_code" : item[1], "preprint" : item[2].strip()}
    published, data, recid = published_check(item[2].strip())
    if published:
        UPDATE_NOTIFIED = True
        json_item["date"] = data
        json_item["recid"] = recid
        notify_published(json_item)
        ALREADY_NOTIFIED.append(json_item)

if UPDATE_NOTIFIED:
    try:
        with open("notified.json", "w") as outfile:
            json.dump(ALREADY_NOTIFIED, outfile)
    except:
        print("Can't save the notified papers on output file")

print("Execution ends at {}".format(str(datetime.now())))
