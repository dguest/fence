#!/usr/bin/python3
"""
Getting rid of old log files in the log folder
"""

import os
import time

LOG_PATH = "/afs/cern.ch/user/a/atlaspo/po-scripts/log/"
DELETE_AFTER_DAYS = 45

NOW = time.time()
for subdirectory in os.listdir(LOG_PATH):
    for file_obj in os.listdir(os.path.join(LOG_PATH, subdirectory)):
        modificated_on = os.path.getmtime(os.path.join(LOG_PATH, subdirectory, file_obj))
        if (NOW - modificated_on) / 3600 > 24 * DELETE_AFTER_DAYS:
            print("file {} is old and is now deleted".format(
                os.path.join(LOG_PATH, subdirectory, file_obj)
                ))
            os.remove(os.path.join(LOG_PATH, subdirectory, file_obj))
