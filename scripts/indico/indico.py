#!/usr/bin/env python

###############################################################################
# indico.py
# about: Fetch daily events from Indico API to be
#        displayed at the ATLAS Collaboration page.
# author: Bruno Lange <bruno.lange@cern.ch>
###############################################################################

import sys
import hashlib
import hmac
import urllib
import urllib2
import time
from datetime import date, timedelta
from operator import itemgetter, attrgetter

try: import simplejson as json
except ImportError: import json

class Indico:
    def __init__(self, params):
        self.root = "https://indico.cern.ch"
        self.params = params

        # Default parameters for URL request
        self._api_key = None
        self._secret_key = None
        self._only_public = False
        self._persistent = False

    def import_keys( self, file ):
        self.api_key( None )
        self.secret_key( None )
        for line in open( file, "r" ):
            line = line.strip()
            if not line or line.startswith( "#" ):
                continue
            if not self._api_key:
                self.api_key( line )
                continue
            if not self._secret_key:
                self.secret_key( line )

    def api_key( self, key ):
        self._api_key = key

    def secret_key( self, key ):
        self._secret_key = key

    def only_public( _bool ):
        self._only_public = _bool

    def persistent( _bool ):
        self._persistent = _bool

    def _build_request(self):
        path = "/export/categ/1l2.json"
        items = self.params.items() if hasattr(self.params, 'items') else list(self.params)
        if self._api_key:
            items.append(('apikey', self._api_key))
        if self._only_public:
            items.append(('onlypublic', 'yes'))
        if self._secret_key:
            if not self._persistent:
                items.append(('timestamp', str(int(time.time()))))
            items = sorted(items, key=lambda x: x[0].lower())
            url = '%s?%s' % (path, urllib.urlencode(items))
            signature = hmac.new(self._secret_key, url, hashlib.sha1).hexdigest()
            items.append(('signature', signature))
            self.signature = signature
        if not items:
            return path
        return '%s?%s' % (path, urllib.urlencode(items))

    def response(self):
        _url = self.root + self._build_request()
        request = urllib2.Request( url=_url )
        return urllib2.urlopen( request ).read()

class Event:
    def __init__( self, result, categoryEvent ):
        self._master = result
        self._category = categoryEvent

        self._title = result[ "title" ]
        self._time  = result["startDate"]["time"]
        # actually ...
        # 'occurrences' takes priority. if it's not empty,
        # its value for time should be used.
        self._occurrences = result[ "occurrences" ]
        if self._occurrences:
            self._time = result["occurrences"][0]["startDT"]["time"]
        self._room  = result["room"]
        self._url   = result["url"]

        # Get event visibility and its category visibility as well
        self._visibility = {"event":    result["visibility"]["name"],
                            "category": categoryEvent["path"][-1]["visibility"]["name"]}
        # These are the allowed visibilities for both events and categories
        self._allowed = {"event":   ["Everywhere","ATLAS Meetings"],
                         "category":["Everywhere","ATLAS Meetings"]}

    @property
    def visible(self):
        """
        constrains visibility at the event level
        """
        for (type,name) in self._visibility.iteritems():
            if name not in self._allowed[type]:
                return False
        return True

    def _categories( self ):
        """returns (ac, ec) , tuple with
        atlas category and event category """

        # Build path from category excluding the
        # last element which concerns visibility

        path = self._category["path"][:-1]

        # Atlas Category should be the directory immediately
        # after "ATLAS Meetings" (id=1l2)
        index = map( itemgetter("id"),path ).index( 6733 )

        # Get ATLAS Category id and name
        idx = index+1
        ac_id   = path[idx]["id"]
        ac_name = path[idx]["name"]

        # Advance to fetch Event Category
        # which might contain several subcategories
        idx += 1
        if idx == len(path):
            ec_id = ac_id
            ec_name = ac_name
        else:
            _ec = { "id": [], "name": [] }
            for sub_category in path[idx:]:
                _ec["id"].append( str(sub_category["id"]) )
                _ec["name"].append( sub_category["name"] )
            ec_id   = "/".join( _ec["id"] )
            ec_name = "/".join( _ec["name"] )

        return ((ac_id,ac_name),(ec_id,ec_name))

    @property
    def atlas_category( self ):
        return self._categories()[0]
    @property
    def chronos(self):
        return int(self._time.replace(":",""))

    def __repr__(self):
        (ac, ec) = self._categories()
        return self._time + " | " + ec[1] + " | " + self._title

    def _render( self, items ):
        spans = []

        # aviod redundancies
        values = []

        for item, value in items.iteritems():
            if not value[0]: continue

            attrs = {   "class": [],
                        "style": { "margin-left":".5em" }  }

            if item == "time":
                attrs["style"]["margin-left"]="0"

            if item == "title":
                attrs["class"].append( "highlight" )

            if value[0] not in values:
                values.append(value[0])
                spans.append( [ "<span %s>%s</span>" % (self._render_attributes( attrs ), value[0]) , value[1] ] )

        # Now we gotta sort these spans!!
        spans = [s[0] for s in sorted( spans, key=lambda s:s[1] )]

        spans[0] = "    %s" % spans[0]

        payload = []
        payload.append( "<li><a href='%s'>" % self._url )
        payload.append( "\n    ".join( spans ) )
        payload.append( "</a></li>" )

        return u"\n".join( payload ).encode( 'utf-8' )


    def _render_attributes( self, attrs ):
        attr_list = []
        for key, value in attrs.iteritems():
            if not value: continue
            _v = ""
            if type(value)==str:
                value = [value]
            if type(value)==list:
                _v = " ".join( [str(v) for v in value] )
            elif type(value)==dict:
                aux = []
                for k,v in value.iteritems():
                    aux.append( "%s:%s" % (k,v) )
                _v = ";".join( aux )
            attr_list.append( "%s=\"%s\"" % (key,_v) )

        return " ".join(attr_list)

    def render( self, type="Hour" ):
        (ac, ec) = self._categories()

        time = self._time[:-3]
        title = self._title
        room = self._room

        if type=="Hour":
            items = { "time":           [time, "a"],
                      "atlas_category": [ac[1],"b"],
                      "event_category": [ec[1],"c"],
                      "title":          [title,"d"],
                      "room":           [room, "e"] }
        if type=="Category":
            items = { "time":           [time, "a"],
                      "event_category": [ec[1],"b"],
                      "title":          [title,"c"],
                      "room":           [room, "d"] }

        print self._render( items )

    def master(self):
        return self._master

    def cat(self):
        return self._category

class Main:
    def __init__(self):
        self.response = None
        self.events = []
        self.categories = []

        self._master = None

        self._hidden = {"ids":[],
                        "names":[]}
        today = time.strftime("%Y-%m-%d", time.localtime())

        self._from = today
        self._to = today

    def setFrom(self, _from):
        self._from = _from

    def setTo(self, _to):
        self._to = _to

    def hide(self, id=None, name=None):
        """ Prevents a particular category from being displayed.
            If id is given, name is neglected. """
        if id:
            self._hidden["ids"].append(id)
        elif name:
            self._hidden["names"].append(name)

    def is_hidden(self, event):
        """ Returns true if event belongs to a category that had been hidden """
        (id, name) = event.atlas_category
        return id in self._hidden["ids"] or name in self._hidden["names"]

    def _import(self):
        """ Gets response from Indico """
        #yesterday = (date.today() - timedelta(1)).strftime("%Y-%m-%d")
        #tomorrow = (date.today() + timedelta(1)).strftime("%Y-%m-%d")
        #custom = date(2013, 2, 1).strftime("%Y-%m-%d")
        indico = Indico(params={
            "from":        self._from,
            "to":          self._to,
            "pretty":      "yes",
            "occurrences": "yes"
        })
        indico.import_keys("keys")
        response = indico.response()

        if "-d" in sys.argv:
            fp = open("response", "w")
            fp.write(response)
            fp.close()

        _json = json.loads(response)

        results     = _json["results"]
        categories  = _json["additionalInfo"]["eventCategories"]

        self.events = [Event(evt,cat) for evt in results for cat in categories if int(evt["categoryId"]) == cat["categoryId"]]

    def run(self):
        self._import()
        self.render( split_by_category = ("-c" in sys.argv) )

    def render(self, split_by_category = False):
        """ prints HTML """

        # First of all, filter off events whose visibility or category's visibility
        # isn't in alllowed list.
        events = filter( lambda evt:evt.visible, self.events[:] )
        if not split_by_category:
            events.sort( key=lambda evt: (evt.chronos,evt.atlas_category[1]) )
            print "<ul>"
            for event in events:
                # Certain categories are not meant to be displayed,
                # regardless of their visibility or their events' visibilities
                if not self.is_hidden(event):
                    event.render()
            print "</ul>"
        else:
            events.sort( key=lambda evt: (evt.atlas_category[1], evt.chronos) )
            prev_category = None
            for event in events:
                curr_category = event.atlas_category[1]
                if curr_category != prev_category:
                    if prev_category:
                        print "</ul>"
                    if not self.is_hidden( event ):
                        print "<ul><li><span>%s</span></li>" % curr_category
                if not self.is_hidden(event):
                    event.render(type="Category")
                prev_category = curr_category
            print "</ul>"

if __name__ == '__main__':

    engine = Main()
    engine.hide(name = "National and institute meetings")
    engine.hide(name = "Publications Committee")
    #engine.run()

    if '--cache' in sys.argv:
        for i in range(1,4):
            fut = (date.today() + timedelta(i)).strftime("%Y-%m-%d")
            print "======================" + fut + "============================="
            engine.setFrom(fut)
            engine.setTo(fut)
            engine.run()
            print
    else:
        engine.run()

