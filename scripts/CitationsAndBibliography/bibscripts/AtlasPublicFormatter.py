# -*- coding: utf-8 -*-
"""
#########################################################################################
# AtlasPublicFormatter.py
# Derived class to format CSV to Twiki tables, Glance query 5457
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, May 30th, 2012
#########################################################################################
# $Rev: 238858 $
# $LastChangedDate: 2015-03-31 00:16:34 +0200 (Tue, 31 Mar 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import re

from BaseFormatter import BaseFormatter

class AtlasPublicFormatter(BaseFormatter):

    def __init__(self):
        super(AtlasPublicFormatter, self).__init__()
        self._fieldnames=('Full Title', 'Journal', 'Links', 'Status', 'Groups')
        self._afsDir = "/afs/cern.ch/atlas/www/GROUPS/PHYSICS/PAPERS/"
        self._papercount = 0
        self._published  = 0
        return

    def mergeGroups(self):
        lst  = [self._currDct["Lead Group"]]
        if self._currDct["Other Groups"] != "":
            lst += self._currDct["Other Groups"].split(",")

        self._currDct["Groups"] = " / ".join(lst)
        

    def makeLinks(self):
        """Create a column named 'Links' with the content of:
        -- Inspire
        -- Plots
        -- ArXiv
        -- ???
        """
        
        self._currDct["Links"] = ""

        linklist =[]

        renamingDict = { "ArXiv"   : "arXiv",
                         "Plots"   : "Figures",
                         "Inspire" : "Inspire"
                         }

        for i,j in renamingDict.iteritems():
            if self._currDct.has_key(i) and self._currDct[i] != '':
                linklist += ["[[%s][%s]]" % (self._currDct[i], j)]
                if len(linklist) > 0 and len(linklist) % 3 == 0:
                    linklist[-1] = "</br>"+linklist[-1]


        self._currDct["Links"] = ", ".join(linklist)

        return

    def makeLinksFromRaw(self):
        """Create a column named 'Links' with the content of:
        -- Inspire
        -- Plots (if not embargoed)
        -- ArXiv
        -- Video
        -- Erratum
        -- Synopsis
        -- Twiki
        """
        
        self._currDct["Links"] = ""

        linklist =[]

        keylist = ["arXiv","Inspire","Erratum","Figures","Video","Synopsis","twiki","Addendum"]

        # if Figures field is not empty, use it, skipping embargoed pages 
        if self._currDct["Figures"].strip() != "" or (self.noDirectory() or self.hasEmbargo()):
            keylist.remove("Figures")

        ptn = re.compile("\[(.*?)@(.*?)@(final.*?)\]")

        for i in self._currDct["Raw Links"].split(","):
            for _name, _link, _type in ptn.findall(i):
                    for j in keylist:
                        if j.lower() in _name.lower() and not "doi" in _name.lower():
                            linklist += ["[[%s][%s]]" % (self.cleanupLink(_link), j)]
                            if len(linklist) > 0 and len(linklist) % 3 == 0:
                                linklist[-1] = "</br>"+linklist[-1]

        # if Figures field is not empty, use it, skipping embargoed pages
        if self._currDct["Figures"].strip() != "" and not (self.noDirectory() or self.hasEmbargo()):
            linklist += ["[[%s][Figures]]" % self.cleanupLink(self._currDct["Figures"])]


        self._currDct["Links"] = ", ".join(linklist)

        return

    def makeStatus(self):
        """Make the status, checking if it is Accepted or Submitted"""

        statTag = ""

        if self._currDct["Status"] == "NEW":
            statTag = "%NEW% "
        #elif self._currDct["Published Online"] != "" and self.daysPassed(self._currDct["Published Online"]) < 31:
        #        statTag = "%PUBLISHED% "         
        #elif self._currDct["Journal Acceptance Date"] != "" and self.daysPassed(self._currDct["Journal Acceptance Date"]) < 31:
        #        statTag = "%ACCEPTED% "


        self._currDct["Full Title"] = statTag + self._currDct["Full Title"]

        if self._currDct['Final Publication Reference'] != '':
            l = self._currDct['Final Publication Reference'].split('@')
            title = self.cleanupLink(l[0])
            link  = l[1]

            self._currDct["Status"] = '[[%s][%s]]<br>(Submitted: %s)' % (link, title, self._currDct["Journal Sub"])

        elif self._currDct["Journal Acceptance Date"] != '':
            self._currDct["Status"] = 'Accepted<br>(Submitted: %s)' % self._currDct["Journal Sub"]
        else:
            self._currDct["Status"] = 'Submitted: %s' % self._currDct["Journal Sub"]

        return

    def makeStatusFromRaw(self):
        """Make the status, checking if it is Accepted or Submitted"""

        statTag = ""

        if self._currDct["Status"] == "NEW":
            statTag = "%NEW% "
        elif self._currDct["Published Online"] != "":
            statTag = "%PUBLISHED% "         
        #elif self._currDct["Journal Acceptance Date"] != "" and self.daysPassed(self._currDct["Journal Acceptance Date"]) < 31:
        #        statTag = "%ACCEPTED% "

        if self._currDct["Latex Title"] != '':
            self._currDct["Full Title"] = statTag + self._currDct["Latex Title"]
        else:
            self._currDct["Full Title"] = statTag + self._currDct["Full Title"]
            self.cleanupTitle("Full Title")

        ptn = re.compile("\[(.*?)@(.*?)@(.*?)\]")

        link  = ""

        if self._currDct['Final Publication Reference'] != '': # use a combination of new and old
            #print ">>>%s<<<" % self._currDct['Final Publication Reference']
            for i in self._currDct["Raw Links"].split(","):
                for _name, _link, _type in ptn.findall(i):
		    #print _name, _link, _type
                    if "final_journalPublicationURL".lower() in _type.lower():
                        if not "doi" in _name.lower() and \
                        not "figures" in _name.lower() and \
                        not "inspire" in _name.lower() and \
                        not "synopsis" in _name.lower() and \
                        not "twiki" in _name.lower() and \
                        not "erratum" in _name.lower():
                            link  = "[[%s][%s]]<br>" % ( self.cleanupLink(_link), self.cleanupLink(_name) )
                    #print link        
                        
            self._currDct["Status"] = '%s(Submitted: %s)' % (link, self._currDct["Journal Sub"])

        elif self._currDct["Journal Acceptance Date"] != '':
            self._currDct["Status"] = 'Accepted<br>(Submitted: %s)' % self._currDct["Journal Sub"]
        else:
            self._currDct["Status"] = 'Submitted: %s' % self._currDct["Journal Sub"]

        return

    def doFormatting(self):

        self._outFile.write("|")

        headers = dict( (n,'*%s*'%n) for n in self._fieldnames)

        self._Writer.writerow(headers)

        cnt = 0

        for r in self._Reader:
            self._currDct = r

            if self._currDct["Ref Code"] == "":
                continue

            if self._filter and not "NEW" in self._filter:
                found = False
                for i in self._filter:
                    if i in self._currDct["Status"] or \
                           i in self._currDct["Lead Group"] or \
                           i in self._currDct["Other Groups"]:
                        found = True
                        break
                if not found:
                    continue
            elif self._filter and "NEW" in self._filter:
                found = False
                for i in self._filter:
                    if i in self._currDct["Status"]:
                        found = True
                        break

                # stop after first five if nothing new happened
                if not found and cnt > 4:
                    break

            cnt += 1
            
            self.cleanupTitle('Full Title')

            #self.makeLinks()
            self.makeLinksFromRaw()

            #self.makeStatus()
            self.makeStatusFromRaw()

            if self._currDct['Lead Group'] == "HIGG":
                self._currDct['Lead Group'] = "HIGGS"
  
            #print r

            self.mergeGroups()


            # protect against empty items:
            for i in self._fieldnames:
                if self._currDct[i] == "":
                    self._currDct[i] = " "

            self._Writer.writerow(self._currDct)

            self._papercount += 1

            if self._currDct["Published Online"] != "":
                self._published += 1

         ### overwrite stray delimiter
        self._outFile.seek(-1,1)


        if not self._filter:
            self._outFile.write("<b>Number of public papers: %d</b>\n\n" % self._papercount)
            self._outFile.write("<b>Number of public papers with collision data: %d</b>\n\n" % (self._papercount - 8)) #HARDCODED! AAAH!!!
            self._outFile.write("<b>Number of published papers: %d</b>\n\n" % self._published)
