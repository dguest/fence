#! /bin/sh
# Script to run citcruncher.py in a virtualenv

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

lsetup python
# source /afs/cern.ch/user/b/brock/venv/bin/activate
cd /afs/cern.ch/user/a/atlaspo/gittools/CitationsAndBibliography/bibscripts

source venv/bin/activate

cd /afs/cern.ch/user/a/atlaspo/po-scripts/scripts/CitationsAndBibliography/bibscripts
./citcruncher.py
