#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
the script creates the bibliographies used by ATLAS publications
"""

from __future__ import print_function

import logging
import os
import glob
import shutil
import argparse

from PaperBibMaker import PaperBibMaker
from PubNoteBibMaker import PubNoteBibMaker
#from PubNoteFormatter import PubNoteFormatter

def send_success_mail():
    """ Send mail on success """
    text = "From: Bibliography Maker <atlaspo@cern.ch>\n"
    text += "To: brock@physik.uni-bonn.de\n"
    text += "Cc: nuno.castro@cern.ch\n"
    text += "Cc: maurizio.colautti@cern.ch\n"
    text += "Subject: Bibliography created\n"
    text += "Dear all,\n\n"
    text += "The automated bibliographies made by MakeAllBib.py.\n\n"

    if SENDMAIL:
        pipe = os.popen("/usr/sbin/sendmail -t -i", "w")
        pipe.write(text)
        status = pipe.close()
        if not status:
            print("Mail sent.")
    else:
        print("\n +++ Mail text:")
        print(text)


def send_error_mail(what):
    """ Send mail on error """
    print("Exception caught, sending mail.")

    text = "From: Bibliography Maker <atlaspo@cern.ch>\n"
    text += "To: brock@physik.uni-bonn.de\n"
    text += "Cc: nuno.castro@cern.ch\n"
    text += "Cc: maurizio.colautti@cern.ch\n"
    text += "Subject: " + what + " bibliography failed\n"
    text += "Dear all,\n\n"
    text += "The automated bibliography made by makeAllBibs.py for " + what + " has failed.\n\n"
    text += "Please run manually and check the output.\n\n"

    if SENDMAIL:
        pipe = os.popen("/usr/sbin/sendmail -t -i", "w")
        pipe.write(text)
        status = pipe.close()
        if not status:
            print("Mail sent.")
    else:
        print("\n +++ Mail text:")
        print(text)


# Create and configure logger
LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
logging.basicConfig(filename="makeAllBibs.log",
                    level=logging.DEBUG,
                    format=LOG_FORMAT,
                    filemode='w')
LOGGER = logging.getLogger()

#------------------------------------------------------------------------------
PARSER = argparse.ArgumentParser()
PARSER.add_argument('--ATLAS', dest='ATLAS', action='store_true', help='Run for ATLAS')
PARSER.add_argument('--no-ATLAS', dest='ATLAS', action='store_false', help='Do not run for ATLAS')
PARSER.add_argument('--CMS', dest='CMS', action='store_true', help='Run for CMS')
PARSER.add_argument('--no-CMS', dest='CMS', action='store_false', help='Do not run for CMS')
PARSER.add_argument('--CONF', dest='CONF', action='store_true', help='Run for ATLAS CONF notes')
PARSER.add_argument('--no-CONF', dest='CONF', action='store_false', help='Do not run for ATLAS CONF notes')
PARSER.add_argument('--PUB', dest='PUB', action='store_true', help='Run for ATLAS PUB notes')
PARSER.add_argument('--no-PUB', dest='PUB', action='store_false', help='Do not run for ATLAS PUB notes')
PARSER.add_argument('--c', '--copy', dest='copy', action='store_true', help='Copy bib files to output directory')
PARSER.add_argument('--no-copy', dest='copy', action='store_false', help='Do not copy bib files to output directory')
PARSER.add_argument('-m', '--mail', dest='mail', action='store_true', help='Send mail')
PARSER.add_argument('--no-mail', dest='mail', action='store_false', help='Do not send mail')
PARSER.set_defaults(ATLAS=True, CMS=True)
PARSER.set_defaults(CONF=True, PUB=True)
PARSER.set_defaults(copy=True, mail=True)
ARGS = PARSER.parse_args()

LOGGER.info("atlas set to %s", ARGS.ATLAS)
LOGGER.info("cms set to %s", ARGS.CMS)
LOGGER.info("conf set to %s", ARGS.CONF)
LOGGER.info("pub set to %s", ARGS.PUB)
LOGGER.info("copy set to %s", ARGS.copy)
LOGGER.info("mail set to %s", ARGS.mail)

# Scripts
PYTHONDIR = "/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/CitationsAndBibliography/bibscripts/"
# Directory with CSV files containing publications
TWIKIDIR = "/afs/cern.ch/user/a/atlaspo/twikirun/"
# Output data files that can be used by other scripts
DATADIR = "/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/CitationsAndBibliography/files/"
# Final destination that bib files are copied to
OUTDIR = "/afs/cern.ch/user/a/atlaspo/public/Bibliography/"

# print os.environ
if "pythonDir" in os.environ:
    PYTHONDIR = os.environ.get("pythonDir")
if "twikiDir" in os.environ:
    TWIKIDIR = os.environ.get("twikiDir")
if "dataDir" in os.environ:
    DATADIR = os.environ.get("dataDir")
if "outDir" in os.environ:
    OUTDIR = os.environ.get("outDir")
print("Python directory:", PYTHONDIR)
print("TWiki directory", TWIKIDIR)
print("Data directory", DATADIR)
print("Output directory", OUTDIR)
print("Copy files to", OUTDIR + "Bibliography", ARGS.copy)
print("Send mail when done", ARGS.mail)

SENDMAIL = ARGS.mail
os.chdir(PYTHONDIR)
BIBERROR = False

print("Process ATLAS papers:", ARGS.ATLAS)
print("Process CMS papers:", ARGS.CMS)
print("Process CONF notes:", ARGS.CONF)
print("Process PUB notes:", ARGS.PUB)

if ARGS.ATLAS:
    try:
        print("Processing ATLAS")
        BIBLIO = PaperBibMaker(PaperBibMaker.ATLAS)
        BIBLIO.setInfile(DATADIR + "atlas_citationsDict.pickle")
        BIBLIO.loadRefCodeMap() # creates a dictionary with {inspireID : 'REF_CODE', ...}
        for i in glob.glob(DATADIR + "ATLAScit_*.json"):
            BIBLIO.setInfile(i)
            BIBLIO.loadBibliography()
        BIBLIO.setOutfile(DATADIR + "ATLAS.bib")
        BIBLIO.makeBibFile()
        if ARGS.copy:
            if os.path.exists(OUTDIR + "ATLAS.bib"):
                shutil.move(OUTDIR + "ATLAS.bib", OUTDIR + "ATLAS_old.bib")
            shutil.copy(DATADIR + "ATLAS.bib", OUTDIR)
    except:
        send_error_mail("ATLAS")
        BIBERROR = True

if ARGS.CMS:
    try:
        print("Processing CMS")
        BIBLIO = PaperBibMaker(PaperBibMaker.CMS)
        BIBLIO.setInfile(DATADIR + "cms_citationsDict.pickle")
        BIBLIO.loadBibliography()
        BIBLIO.setOutfile(DATADIR + "CMS.bib")
        BIBLIO.makeBibFile()
        if ARGS.copy:
            if os.path.exists(OUTDIR + "CMS.bib"):
                shutil.move(OUTDIR + "CMS.bib", OUTDIR + "CMS_old.bib")
            shutil.copy(DATADIR + "CMS.bib", OUTDIR)
    except:
        send_error_mail("CMS")
        BIBERROR = True

if ARGS.CONF:
    try:
        print("Processing CONF")
        BIBLIO = PubNoteBibMaker()
        BIBLIO.setInfile(TWIKIDIR + "csv5505/table5505.csv")
        BIBLIO.setOutfile(DATADIR + "ConfNotes.bib")
        BIBLIO.processCSV()
        if ARGS.copy:
            if os.path.exists(OUTDIR + "ConfNotes.bib"):
                shutil.move(OUTDIR + "ConfNotes.bib", OUTDIR + "ConfNotes_old.bib")
            shutil.copy(DATADIR + "ConfNotes.bib", OUTDIR)
    except:
        send_error_mail("CONF")
        BIBERROR = True

if ARGS.PUB:
    try:
        print("Processing PUB")
        BIBLIO = PubNoteBibMaker()
        BIBLIO.setInfile(TWIKIDIR + "csv5967/table5967.csv")
        BIBLIO.setOutfile(DATADIR + "PubNotes.bib")
        BIBLIO.processCSV()
        if ARGS.copy:
            if os.path.exists(OUTDIR + "PubNotes.bib"):
                shutil.move(OUTDIR + "PubNotes.bib", OUTDIR + "PubNotes_old.bib")
            shutil.copy(DATADIR + "PubNotes.bib", OUTDIR)
    except:
        send_error_mail("PUB")
        BIBERROR = True

if not BIBERROR:
    print("Process complete with no errors")
    if ARGS.mail:
        print("Sending email")
        send_success_mail()
else:
    print("Process finished with error, not sending success mail")
