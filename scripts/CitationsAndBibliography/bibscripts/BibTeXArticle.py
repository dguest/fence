# -*- coding: utf-8 -*-
"""
#########################################################################################
# BibTeXArticle.py
# Utility class for Papers in BibTeX format
# Written by Marcello Barisonzi, ICTP, December 8th, 2014
#########################################################################################
# $Rev: 244869 $
# $LastChangedDate: 2017-01-03 14:22:05 +0100 (Tue, 03 Jan 2017) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

from __future__ import print_function

import sys

from BibUnicodeTex import unicode_to_tex

from JournalDict import *

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

class BibTeXArticle:    
    """ Interface / Abstract Class concept for readability. """
    
    # Static template
    _template = None

    # journal abbreviations
    _abbrev = get_abbreviations() 

    def __init__(self, dct, strategy=None):
        self._paperDict    = dct
        self._templateDict = {}
        self._strategy = strategy
        return


    def outputBibTeX(self):
        # explicitly set it up so this can't be called directly

        if self._strategy:
            concreteStrategy = self._strategy(self._paperDict)
            return concreteStrategy.outputBibTeX()
        else:
            raise NotImplementedError('Exception raised, BibTeXArticle is supposed to be an interface / abstract class!')    

    def getPublicationInfo(self):
        if not self._paperDict['publication_info']:
            return

        # there could be double records with erratum
        
        if self._paperDict['publication_info'].__class__() == [].__class__():
            _info = filter(lambda x:x.has_key('title') and 'erratum' not in x['title'].lower(), self._paperDict['publication_info'])[0]
        else:
            _info = self._paperDict['publication_info']

        if _info.has_key('title'):
            self._templateDict["Journal Ref"] = _info['title'].replace(".",". ").strip()
        
        if _info.has_key('cnum'):
            self._templateDict["Conf Ref"]    = _info['cnum']

        if _info.has_key('volume'):
            self._templateDict["Volume"]      = _info['volume']
            
        if _info.has_key('year'):
            self._templateDict["Year"]        = _info['year']
        elif self._templateDict.has_key("ArXiv"):
            try:
                _yr = int(self._templateDict["ArXiv"][:2])
                # no arXiv before 1991
                if _yr < 91:
                    self._templateDict["Year"] = str(2000+_yr)
                else:
                    self._templateDict["Year"] = str(1900+_yr)
                # print("SETTING YEAR FROM ARXIV", _yr)
                # sys.exit(0)
            except:
                pass

        if _info.has_key('pagination'):
            self._templateDict["Pages"]       = _info['pagination'].split("-")[0]  

        return

    def cleanup(self,_s):
        # fix CMS bullshit https://inspirehep.net/literature/1328962
        _s = _s.replace(u'\u1e6b','\\bar{t}')
        _s = _s.replace(u'\u2013', '-')
        _s = _s.replace(u'\u2014', '-')
        _s = _s.replace(u'\u204e', '*')
        _s = _s.replace(u'\u03b3', '\\gamma')
        _s = _s.replace(u'\u03bc', '\\mu')
        _s = _s.replace(u'\xa0', '~')

        try:
            _s.encode("ascii")
        except:
            # print('Error encoding:', _s)
            # print("BibTeXArticle unicode_to_tex")
            # print("Before:", _s)
            _s = unicode_to_tex(_s)
            # _s = _s.replace("\\space"," ")
            # print("After: ", _s)
        return _s
            
    def buildTemplate(self):
        # reference code and title are taken for granted
        self._template = """@%(Type)s{%(Ref Code)s,
    title          = "{%(Title)s}","""
        if self._templateDict.has_key("Collaboration"):
            self._template += """
    collaboration  = "{%(Collaboration)s}","""
        elif self._templateDict.has_key("Authors"):
            self._template += """
    author         = "%(Authors)s","""
        if self._templateDict.has_key("Journal Ref"):
            self._template += """
    journal        = "%(Journal Ref)s","""
        if self._templateDict.has_key("Volume"):
            self._template += """
    volume         = "%(Volume)s","""
        if self._templateDict.has_key("Year"):
            self._template += """
    year           = "%(Year)s","""
        if self._templateDict.has_key("Pages"):
            self._template += """
    pages          = "%(Pages)s","""
        if self._templateDict.has_key("Howpublished"):
            self._template += """
    howpublished   = "{%(Howpublished)s}","""
        if self._templateDict["Type"] == "booklet":
            self._template += """
    url = "%(Cds Url)s","""
        if self._templateDict.has_key("Doi") and self._templateDict["Doi"] and self._templateDict["Doi"] != "":
            self._template += """
    doi            = "%(Doi)s","""
        if self._templateDict.has_key("Cerncode"):
            self._template += """
    reportNumber   = "%(Cerncode)s","""
        if self._templateDict.has_key("ArXiv") and self._templateDict["ArXiv"]:
            self._template += """
    eprint         = "%(ArXiv)s",
    archivePrefix  = "arXiv","""
        if self._templateDict.has_key("HEPClass"):
            self._template += """
    primaryClass   = "%(HEPClass)s","""
        self._template += """
}

"""
        return

    def makeAuthors(self):
        _auth = ""

        if len(self._paperDict["authors"]) > 6:
            _auth = u"%s et al." % self._paperDict["authors"][0]["full_name"]
        else:
            for i in self._paperDict["authors"][:6]:
                _auth += u"%s and " % i["full_name"]

            _auth = _auth[:-5]

        return self.cleanup(_auth)

    def makeRefCodeFromJournal(self):

        if  self._templateDict.has_key('Journal Ref') and self._templateDict.has_key("Volume") and self._templateDict.has_key("Pages"):
            # first, check abbreviations, and if none, create one
            if not self._abbrev.has_key(self._templateDict["Journal Ref"]):
                self._abbrev = make_abbreviation(self._templateDict["Journal Ref"], self._abbrev)
                store_abbreviations(self._abbrev)
            self._templateDict['Ref Code'] = "%s%s_%s" % (self._abbrev[self._templateDict["Journal Ref"]], self._templateDict["Volume"], self._templateDict["Pages"])
        elif self._templateDict.has_key('Conf Ref'):
            self._templateDict['Ref Code'] = "CONF_%s" % self._templateDict["Conf Ref"]
        elif self._templateDict["Doi"]: 
            self._templateDict['Ref Code'] = self._templateDict["Doi"].split("/")[-1].replace("(","_").replace(")","_")
        elif self._templateDict["ArXiv"]:
            self._templateDict['Ref Code'] = "arxiv_%s" % self._templateDict["ArXiv"]
        else:
            self.makeAdHocRefCode()
        return

    def guess_doi(self):
        """
        try to guess which doi is correct, in case there are many
        """

        # if it's not a atlas collaboration, we can not investigate, and return default doi[0]
        if "Collaboration" in self._templateDict and not self._templateDict['Collaboration'] == 'ATLAS Collaboration':
            return self._paperDict["doi"][0]

        # if collaboration is not specified, we can not investigate, and return default doi[0]
        if "Collaboration" not in self._templateDict:
            return self._paperDict["doi"][0]

        # inspire sometimes returns many time the same record; we check for length of set from list
        doi_set = set(self._paperDict["doi"])
        if len(doi_set) > 1:
            # we retrieve the dois from the db
            dbm = DBManager()
            db_dois = dbm.execute("""
                SELECT 
                PHASE_LINKS.HREF, PHASE_LINKS.ALIAS
                FROM
                PHASE_LINKS, PHASE, PUBLICATION
                WHERE
                PHASE_LINKS.PHASE_ID = PHASE.ID
                AND PUBLICATION.ID = PHASE.PUBLICATION_ID
                AND PUBLICATION.REF_CODE = :1
                AND PHASE_LINKS.HREF LIKE '%doi%';
                """, (self._paperDict["Ref Code"],))
            dbm.close()

            # we return the first doi that doesn't have a label of erratum or addendum in it
            for doi in self._paperDict["doi"]:
                for db_doi in db_dois:
                    if doi.lower() in db_doi[0].lower():
                        if 'erratum' in db_doi[1].lower() or 'addendum' in db_doi[1].lower():
                            continue
                        else:
                            return doi

        # if no condition is satisfied, we keep returning the default value
        return self._paperDict["doi"][0]


    def makeDOI(self):
        if self._paperDict["doi"].__class__ == [].__class__:
            doi = self.guess_doi()
            return doi
        else:
            return self._paperDict["doi"]

    def makeArXiv(self):
        if not self._paperDict["primary_report_number"]:
            return None

        if self._paperDict["primary_report_number"].__class__ != [].__class__:
            self._paperDict["primary_report_number"] = [self._paperDict["primary_report_number"]]

        for i in self._paperDict['primary_report_number']:

            # MOTHERF**** DASH! In report number!!! WTF?
            if i and u'\u2013' in i:
                i = i.replace(u'\u2013', '-')

            # print("Report Number", i)
            if i and 'arXiv' in i:
                #self._templateDict['HEPClass'] = 'hep-ex'
                return i.replace('arXiv:','')
            elif i and 'hep' in i:
                self._templateDict['HEPClass'] = i.split("/")[0]
                return i.split("/")[1]
        
        return None
        

    def isCollaboration(self):
        if self._paperDict["corporate_name"]:
            return True

        return False

    def isBook(self):
        try:
            for i in self._paperDict["collection"]:
                if "Book" in i.values():
                    return True
        except:
            return False
        return False

    def makeAdHocRefCode(self):
        self._templateDict["Ref Code"] = "%s_%s" % (self._paperDict["authors"][0]["last_name"].lower(), self._paperDict['title']['title'].split()[0].lower())

class ATLASPaper(BibTeXArticle):

    def outputBibTeX(self):

        # no Ref Code? Skip
        if self._paperDict['Ref Code'] == "":
            return ""

        self._templateDict["Type"] = "article"

        self._templateDict['Ref Code'] = self._paperDict['Ref Code']

        self._templateDict["Collaboration"] = "ATLAS Collaboration"

        self._templateDict["Doi"] = self.makeDOI()

        try:
            self._templateDict["Cerncode"] = filter(lambda x:'CERN' in x, self._paperDict['primary_report_number'])[0]
        except:
            pass

        self._templateDict["ArXiv"]    = self.makeArXiv()

        if len(self._paperDict['title']) > 1:
            # this case is due to wrong entry on TOPQ-2017-16 with array of titles
            self._templateDict["Title"]    = self.cleanup(self._paperDict['title'][0]['title'])
        else:
            self._templateDict["Title"]    = self.cleanup(self._paperDict['title']['title'])

        if "ATLAS" in self._templateDict["Title"] and "CMS" in self._templateDict["Title"]:
            self._templateDict["Collaboration"] = "ATLAS and CMS Collaborations"


        self._templateDict['HEPClass'] = 'hep-ex'

        self.getPublicationInfo()

        #try:
        #    self._templateDict["Journal Ref"] = self._paperDict['publication_info']['title'].replace(".",". ").strip()
        #    self._templateDict["Volume"]   = self._paperDict['publication_info']['volume']
        #    self._templateDict["Year"]     = self._paperDict['publication_info']['year']
        #    self._templateDict["Pages"]    = self._paperDict['publication_info']['pagination'].split("-")[0]
        #except:
        #    pass

        self.buildTemplate()

        # print(self._templateDict)

        _template = self._template % self._templateDict
        # remove all sort of UNICODE shit
        return self.cleanup(_template)

class ATLASNote(BibTeXArticle):

    def outputBibTeX(self):

        # print(self._paperDict)

        # no Ref Code? Skip
        if self._paperDict['Ref Code'] == "":
            return ""

        self._templateDict["Type"] = "booklet"

        self._templateDict['Ref Code'] = self._paperDict['Ref Code'].strip("/")

        self._templateDict["Collaboration"] = "ATLAS Collaboration"

        if self._paperDict.has_key('Final Sign Off'):
            self._templateDict['Year'] = self._paperDict['Final Sign Off'].split("/")[0]
        elif self._paperDict.has_key('Phase1 Sign Off 2'):
            self._templateDict['Year'] = self._paperDict['Phase1 Sign Off 2'].split("/")[0]

        self._templateDict['Title'] = self._paperDict['Full Title']


        if "ATLAS" in self._templateDict["Title"] and "CMS" in self._templateDict["Title"]:
            self._templateDict["Collaboration"] = "ATLAS and CMS Collaborations"


        self._templateDict['Cds Url'] = self._paperDict['Cds Url']

        self._templateDict['Howpublished'] = self._paperDict['Ref Code']

        self.buildTemplate()

        # print(self._templateDict)

        _template = self._template % self._templateDict
        # remove all sort of UNICODE shit
        return self.cleanup(_template)


class CMSPaper(BibTeXArticle):

    def outputBibTeX(self):

        # print(self._paperDict)

        self._templateDict["Type"] = "article"

        if self._paperDict.has_key('Ref Code') and "B2G" in self._paperDict['Ref Code']:
            print(self._paperDict['Ref Code'])

        # No citations? Skip (empty string)
        if not self._paperDict.has_key('Ref Code'):
            return ""

        self._templateDict['Ref Code'] = self._paperDict['Ref Code']

        self._templateDict["Collaboration"] = "CMS Collaboration"

        self._templateDict["Doi"] = self.makeDOI()

        if self._templateDict["Doi"] == "10.1140/epjc/210052-014-2847-x":
            self._templateDict["Doi"] = "10.1140/epjc/s10052-014-2847-x"

        try:
            self._templateDict["Cerncode"] = filter(lambda x:x and 'CERN' in x, self._paperDict['primary_report_number'])[0]
        except:
            pass

        self._templateDict["ArXiv"]    = self.makeArXiv()

        if isinstance(self._paperDict["title"], dict):
            if "title" in self._paperDict["title"]:
                self._templateDict["Title"] = self.cleanup(self._paperDict["title"]["title"])
            elif "subtitle" in self._paperDict["title"]:
                self._templateDict["Title"] = self.cleanup(self._paperDict["title"]["subtitle"])
        if isinstance(self._paperDict["title"], list):
            self._templateDict["Title"] = self.cleanup(self._paperDict["title"][0]["title"])

        if "ATLAS" in self._templateDict["Title"] and "CMS" in self._templateDict["Title"]:
            self._templateDict["Collaboration"] = "ATLAS and CMS Collaborations"


        self._templateDict['HEPClass'] = 'hep-ex'

        self.getPublicationInfo()

        #try:
        #    self._templateDict["Journal Ref"] = self._paperDict['publication_info']['title'].replace(".",". ").strip()
        #    self._templateDict["Volume"]   = self._paperDict['publication_info']['volume']
        #    self._templateDict["Year"]     = self._paperDict['publication_info']['year']
        #    self._templateDict["Pages"]    = self._paperDict['publication_info']['pagination'].split("-")[0]
        #except:
        #    pass

        self.buildTemplate()

        # print("""TEMPLATE""", self._templateDict)

        _template = self._template % self._templateDict
        # remove all sort of UNICODE shit
        return self.cleanup(_template)


class OtherPaper(BibTeXArticle):

    def outputBibTeX(self):
        # print("Record ID", self._paperDict["recid"])

        self._templateDict["Type"] = "article"

        self._templateDict["Doi"]   = self.makeDOI()

        self._templateDict["ArXiv"] = self.makeArXiv()

        # print(self._paperDict['publication_info'])
        if (not self._paperDict.has_key('publication_info')) or self._paperDict['publication_info']==None:
            print("NO PUBLICATION INFO!!!")
            return ""

        self._templateDict["Title"]    = self.cleanup(self._paperDict['title']['title'])

        if self._paperDict['publication_info'].__class__ == [].__class__:
            self._paperDict['publication_info_additional'] = self._paperDict['publication_info'][1]
            self._paperDict['publication_info'] = self._paperDict['publication_info'][0]
            

        self.getPublicationInfo()

        self.makeRefCodeFromJournal()

        """
        try:
            self._templateDict["Journal Ref"] = self._paperDict['publication_info']['title'].replace(".",". ").strip()
            self._templateDict["Volume"]   = self._paperDict['publication_info']['volume']
            self._templateDict["Year"]     = self._paperDict['publication_info']['year']
            self._templateDict["Pages"]    = self._paperDict['publication_info']['pagination']

            if "-" in self._templateDict["Pages"]:
                self._templateDict["Pages"] = self._templateDict["Pages"].split("-")[0]

            self.makeRefCodeFromJournal()

        except:
            #self._templateDict["Journal Ref"] = ""
            #self._templateDict["Volume"]      = "" 
            #self._templateDict["Year"]        = ""
            #self._templateDict["Pages"]       = "" 
            pass
        """

        if self.isCollaboration():
            try:
                self._templateDict["Collaboration"] = self._paperDict["corporate_name"][0]["collaboration"]
            except:
                pass
 
            if "ATLAS" in self._templateDict["Title"] and "CMS" in self._templateDict["Title"]:
                self._templateDict["Collaboration"] = "ATLAS and CMS Collaborations"
        else:
            self._templateDict["Authors"] = self.makeAuthors()

        if self.isBook():
            if self._paperDict['publication_info'].has_key('reference'):
                self._templateDict["Howpublished"] =  self._paperDict['publication_info']['reference']
            elif self._paperDict.has_key('publication_info_additional'):
                self._templateDict["Howpublished"] =  self._paperDict['publication_info_additional']['reference']

        # if there is no DOI or no arXiv, fuck this shit?
        if self._templateDict["Doi"] == '' and self._templateDict["ArXiv"] == None and not (self._templateDict.has_key("Collaboration") or self._templateDict.has_key("Authors")):
            print("REJECTED:", self._templateDict)
            return ''


        self.buildTemplate()

        # print(self._templateDict)

        _template = self._template % self._templateDict
        # remove all sort of UNICODE shit
        return self.cleanup(_template)

