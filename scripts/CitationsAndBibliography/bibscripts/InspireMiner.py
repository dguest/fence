# -*- coding: utf-8 -*-
"""
#########################################################################################
# InspireMiner.py
# Data mine Inspire for citations data
# Written by Marcello Barisonzi, ICTP, January 16th, 2015
#########################################################################################
# $Rev: 241109 $
# $LastChangedDate: 2015-12-14 19:41:28 +0100 (Mon, 14 Dec 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import re
import time
import json

import certifi
import urllib3.contrib.pyopenssl

class InspireMiner:

    def __init__(self):

        self.citationsPtn = re.compile("Cited by:\s([0-9]+)\srecords")
        self.selfcitPtn   = re.compile("self-citations:\s([0-9]+)\srecords")

        # Set up a Pool Manager for verifying certificates
        urllib3.contrib.pyopenssl.inject_into_urllib3()
        self.http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())


    def getCitationsByRecid(self, recid):

        _lnk = "https://old.inspirehep.net/record/%s/citations" % recid

        try:
            r = self.http.request('GET', _lnk)
            _rtext = r.data
        except urllib3.exceptions.NewConnectionError:
            print "Connection error for %s" % _lnk
            _rtext = ""

        m = self.citationsPtn.search(_rtext)

        if m:
            self._outDct["Citations (Total)"] = int(m.group(1))
        else:
            self._outDct["Citations (Total)"] = None

        m = self.selfcitPtn.search(_rtext)

        if m:
            self._outDct["Citations (Self)"] = int(m.group(1))
        else:
            self._outDct["Citations (Self)"] = None

        try:
            self._outDct["Citations"] = self._outDct["Citations (Total)"] - self._outDct["Citations (Self)"]
        except:
            self._outDct["Citations"] = None

        print self._outDct["Citations (Total)"],  self._outDct["Citations (Self)"], self._outDct["Citations"]

        time.sleep(.3)

        # now get which papers cite it
        _ld = [0]*250
        _la = []
        _cnt = 0
        
        while(self._outDct["Citations (Total)"] > 0 and len(_ld)>=250):

            _lnk2 = "https://old.inspirehep.net/search?p=refersto+recid+%s+and+tc+p&of=recjson&ot=recid&rg=250&jrec=%d" % (recid, _cnt+1)

            print _lnk2

            try:
                r = self.http.request('GET', _lnk2)
                # _rtext = r.data.strip()
                _rtext = r.data
            except urllib3.exceptions.NewConnectionError:
                print "Connection error for %s" % _lnk2
                _rtext = ""

            # convert to list of dicts
            try:
                #exec("_ld = %s" % r.text.strip())
                _ld = json.loads(_rtext)
            except:
                print "Except"
                _ld = []

            print "--> Got %d lines" % len(_ld)

            _la += _ld

            _cnt += 250

            time.sleep(.3)

        self._outDct["Citation Records"] = _la


        return


    def getCitations(self):

        ptn = re.compile("([0-9]+)")

        m = ptn.search(self._outDct["Inspire"])

        if not m:
            return

        recid = int(m.groups(1)[0])

        # get papers from CDS
        _lnk = "https://old.inspirehep.net/record/%d/citations" % recid

        try:
            r = self.http.request('GET', _lnk)
            _rtext = r.data
        except urllib3.exceptions.NewConnectionError:
            print "Connection error for %s" % _lnk
            _rtext = ""

        m = self.citationsPtn.search(_rtext)

        if m:
            self._outDct["Citations (Total)"] = int(m.group(1))
        else:
            self._outDct["Citations (Total)"] = None

        m = self.selfcitPtn.search(_rtext)

        if m:
            self._outDct["Citations (Self)"] = int(m.group(1))
        else:
            self._outDct["Citations (Self)"] = None

        try:
            self._outDct["Citations"] = self._outDct["Citations (Total)"] - self._outDct["Citations (Self)"]
        except:
            self._outDct["Citations"] = None

        print self._outDct["Citations (Total)"],  self._outDct["Citations (Self)"], self._outDct["Citations"]

        time.sleep(.3)

        # now get which papers cite it
        _ld = [0]*250
        _la = []
        _cnt = 0
        
        while(self._outDct["Citations (Total)"] > 0 and len(_ld)>=250):

            _lnk2 = "https://old.inspirehep.net/search?p=refersto+recid+%d+and+tc+p&of=recjson&ot=recid&rg=250&jrec=%d" % (recid, _cnt+1)

            print _lnk2

            try:
                r = self.http.request('GET', _lnk2)
                # _rtext = r.data.strip()
                _rtext = r.data
            except urllib3.exceptions.NewConnectionError:
                print "Connection error for %s" % _lnk2
                _rtext = ""

            # convert to list of dicts
            try:
                _ld = json.loads(_rtext)
            except:
                print "Except"
                _ld = []

            print "--> Got %d lines" % len(_ld)

            _la += _ld

            _cnt += 250

            time.sleep(.3)

        self._outDct["Citation Records"] = _la

        return


    def mineInspire(self):
        return
