#! /bin/sh
# Setup for ATLAS bibliography scripts that get updated list
# of publications from various sources.
# This setup is for Ian Brock's local directory
export pythonDir="/Users/brock/atlas/LaTeX/atlaslatex-admin/bibscripts/"
# export twikiDir="/Users/brock/atlas/LaTeX/bibscripts/twikirun/"
export twikiDir="/afs/cern.ch/user/a/atlaspo/twikirun/"
export dataDir="/Users/brock/atlas/LaTeX/bibscripts/files/"
export outDir="/Users/brock/atlas/LaTeX/bibscripts/output/"
export pltDir="/Users/brock/atlas/LaTeX/bibscripts/plots/"
# export unicodeDir="/Users/brock/atlas/LaTeX/atlaslatex-admin/bibscripts/unicode_tex-0.1.1/build/lib"
