# -*- coding: utf-8 -*-
"""
#########################################################################################
# BaseFormatter.py
# Base class to format CSV to Twiki tables
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, May 30th, 2012
#########################################################################################
# $Rev: 238962 $
# $LastChangedDate: 2015-04-23 23:49:48 +0200 (Thu, 23 Apr 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import csv
import os
import re
from datetime import datetime

WGroups = ['BPHY', ['DAPR', 'LUMI'], 'EGAM', 'EXOT', 'FTAG', 'GENR', 'HIGG', 'HION', 'IDET', 'IDTR', 'JETM', 'LARG', 'MCGN', 'MUON', 'MDET', 'PERF', 'SOFT', 'STDM', 'SUSY', 'TAUP', 'TCAL', 'TECH', 'TOPQ', 'TRIG']

class BaseFormatter(object):

    def __init__(self):
        self._outFile = None
        self._inFile = None
        self._fieldnames = ()
        self._filter = None
        self._afsDir = None
        csv.register_dialect('twiki', delimiter='|', lineterminator='|\r\n|')

        # non-collision papers
        self._exclusionList = ["PERF-2007-01", "LARG-2009-01", "LARG-2009-02", "IDET-2010-01", "MUON-2010-01", "TCAL-2010-01", "PERF-2010-02", "SOFT-2010-01"]

        self._perfGroups = ["JETM","EGAM","DAPR","IDTR","PERF","TRIG","LARG","IDET","MUON","TAUP","FTAG", "TCAL"]
        
        return

    def setOutfile(self,fn):
        if self._outFile and not self._outFile.closed:
            print "Closing existing file", self._outFile.name
            self._outFile.close()
        f = open(fn,"w")
        print "Opening output file: ", fn
        self._outFile = f
        return

    def setFilter(self,fl):
        if fl.__class__() == []:
            self._filter = fl
        else:
            self._filter = [fl]
        return

    def setInfile(self,fn):
        f = open(fn)
        self._inFile = f
        return

    def setFieldnames(self,fn):
        self._fieldnames = tuple(fn)
        return

    def noDirectory(self):
        _path = os.path.join(self._afsDir, self._currDct["Ref Code"])
        #print _path, os.path.exists(_path)
        return not os.path.exists(_path)

    def hasEmbargo(self):
        _path = os.path.join(self._afsDir, self._currDct["Ref Code"])
        return os.path.exists(os.path.join(_path, "embargo"))

    def cleanupLink(self, col):

        col = col.replace("%3A",":")
        col = col.replace("%20"," ")
        col = col.replace("%28","(")
        col = col.replace("%29",")")
        col = col.replace("%2C",",")
        col = col.replace("%u2013","-")
        col = col.replace("%3F","?")
        col = col.replace("%3D","=")       
 
        return col

    def daysPassed(self, d, parseString = "%Y/%m/%d"):
        return (datetime.today() - datetime.strptime(d, parseString)).days

    def addGlanceLink(self):
        ptn = "<a target=\"_blank\" href=\"(.*?)\">([0-9]+)</a>"
        self._currDct["ID/Link"] = re.sub(ptn, lambda match: "[[%s][%s]]" % match.groups(), self._currDct["ID/Link"])
        return

    def cleanupTitle(self, key):

        col = self._currDct[key]

        col = col.replace('\n', '') #breaks wiki tables
        col = col.replace('|', '&#x007C;')#breaks wiki tables
        col = col.replace("K0S","!K0S")
        col = col.replace("ChiB","!ChiB")
        col = col.replace("LeptonJets","!LeptonJets")
        col = col.replace("MinBias","!MinBias")
        col = col.replace("LargeR","!LargeR")
        col = col.replace("WbWb","!WbWb")
        col = col.replace("TeV", "<noautolink>TeV</noautolink>")
        col = col.replace("GeV", "<noautolink>GeV</noautolink>")
        col = col.replace("MeV", "<noautolink>MeV</noautolink>")
        col = col.replace("fb-1", "fb<sup>-1</sup>")
        col = col.replace("fb^-1", "fb<sup>-1</sup>")
        col = col.replace("pb-1", "pb<sup>-1</sup>")
        col = col.replace("pb^-1", "pb<sup>-1</sup>")
        col = col.replace("\\sqrt{s}", "&#8730;s")
        col = col.replace("sqrt(s)", "&#8730;s")
        col = col.replace("sqrt{s}", "&#8730;s")
        col = col.replace("sqrt s", "&#8730;s")
        col = col.replace("sqrt(s_NN)", "&#8730;s<sub>NN</sub>")
        col = col.replace("sqrt(sNN)", "&#8730;s<sub>NN</sub>")
        col = col.replace("sqrts_{nn}", "&#8730;s<sub>NN</sub>")
        col = col.replace("->","&rarr;")
        col = col.replace("\\to","&rarr;")
        col = col.replace("\\rightarrow","&rarr;")
        col = col.replace("*", "&#42;")
        col = col.replace("_b", "<sub>b</sub>")
        col = col.replace("_s", "<sub>s</sub>")
        col = col.replace("^0", "<sup>0</sup>")
        col = col.replace("lambda", "&Lambda;")
        col = col.replace("Lambda", "&Lambda;")
        col = col.replace("alpha", "&alpha;")
        col = col.replace("Upsilon", "&Upsilon;")
        col = col.replace(" psi", " &psi;")
        col = col.replace("psi ", "&psi; ")
        col = col.replace("\\gamma", "&gamma;") 
        col = col.replace("\xcf\x84", "&#8467;")
        col = col.replace("s\xe2\x88\x9a", "&radic;s")
        col = col.replace("\xe2\x88\x9as", "&radic;s")
        col = col.replace("\xce\xb3", "&gamma;")
        col = col.replace("\xe2\x86\x92", "&rarr;")
        col = col.replace("(\xe2\x88\x97)", "<sup>(&#42;)</sup>")
        col = col.replace("\xce\xbd", "&nu;")
        col = col.replace("\xe2\x88\x921", "<sup>-1</sup>")
        col = col.replace("\\bar{q}", "<span style='text-decoration: overline'>q</span>")
        col = col.replace("\\bar{t}", "<span style='text-decoration: overline'>t</span>")
        col = col.replace("\xce\xbc", "&mu;")
        col = col.replace("\xe2\x88\x9a", "&radic;")

        
        words = col.split()

        for i in words:
            if i[0].isupper():
                for j,k in enumerate(i):
                    try:
                        if k.islower() and i[j+1].isupper():
                            idx = col.find(i)
                            ln  = len(i)
                            col = col[:idx] + ("!%s" % i) + col[idx+ln:]
                            break
                    except:
                        pass
                        


        self._currDct[key] = col

        return 

    def setReader(self):
        self._Reader = csv.DictReader(self._inFile)

    def setWriter(self):
        self._Writer = csv.DictWriter(self._outFile,self._fieldnames,dialect='twiki',extrasaction='ignore',restval=' ')

    def doFormatting(self):
        print "THIS IS A STUB CLASS, DOES NOTHING. CHECK YOUR COMMAND LINE PARAMETERS."

    def processCSV(self):

        self.setReader()

        self.setWriter()

        self.doFormatting()

        #####################
        #    Date Updated   #
        #####################
        now = datetime.now()
        last_updated = now.strftime("%Y-%m-%d, %H:%M")
        self._outFile.write('Last updated: ')
        self._outFile.write(last_updated)
        self._outFile.write('\n\n')

        self._outFile.close()
        self._inFile.close()
