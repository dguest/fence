# -*- coding: utf-8 -*-
"""
#########################################################################################
# CitationsCruncher.py
# Make citations tables and plots
# Written by Marcello Barisonzi, ICTP, January 16th, 2015
#########################################################################################
# $Rev: 237727 $
# $LastChangedDate: 2015-03-12 14:27:29 +0100 (Thu, 12 Mar 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

from __future__ import print_function

import csv
import os
import re
import sys
from copy import deepcopy
from datetime import datetime

# import matplotlib for headless terminal
# backend has to be set first
# http://stackoverflow.com/questions/5503601/python-headless-matplotlib-pyplot
# sys.path.insert(0,"/afs/cern.ch/user/a/atlaspo/workspace/matplotlib-1.2.0/build/lib.linux-x86_64-2.6/")
import matplotlib as mpl
mpl.use('Agg', warn=False)
# next import must be AFTER mpl.use('Agg', warn=False)
from matplotlib import pyplot
import numpy as np

from PyPlotCruncher import PyPlotCruncher

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position


pltDir = "/afs/cern.ch/user/a/atlaspo/gittools/CitationsAndBibliography/plots/"

class CitationsCruncher(PyPlotCruncher):

    def __init__(self):
        PyPlotCruncher.__init__(self)

        self.citationsPtn = re.compile("Cited by:\s([0-9]+)\srecords")
        self.selfcitPtn   = re.compile("self-citations:\s([0-9]+)\srecords")

        self._fieldnames = ["Ref Code","InspLink","Citations (Total)","Citations (Self)","Citations", "Published Citations (Total)","Published Citations (Self)","Published Citations"]

        self._queryInspire = False
        self._outDct = {}

        self._ATLASrecords = {}
        self._loadDict = {}
        self._loadDictDB = {}

        return

    def setPkfile(self, fn):
        self._pkfile = fn

        return

    def _loadDictDataBase(self, run=''):
        self._loadDictDB.clear()
        mgr = DBManager()

        query = """
        SELECT REF_CODE, INSPIRE_RECORD FROM CITATIONS_ATLAS_PUBLISHED WHERE SELF = 1 ORDER BY REF_CODE
        """

        result = mgr.select(query)

        tempAggregate = {}
        for element in result:
            if element[0] in tempAggregate:
                lista = tempAggregate[element[0]]
                lista.append(int(element[1]))
                tempAggregate[element[0]] = lista
            else:
                tempAggregate[element[0]] = list()
                tempAggregate[element[0]].append(int(element[1]))

        if run==2:
            query = """
            SELECT 
                PUBLICATION.REF_CODE,
                FULL_TITLE,
                PHYS_GROUP,
                INSPIRE_RECORD,
                CITATIONS_TOTAL,
                CITATIONS_SELF,
                PUBLISHED_CITATIONS_TOTAL,
                PUBLISHED_CITATIONS_SELF,
                PUBLICATION_COLLISION.RUN
            FROM
                CITATIONS_ATLAS,
                PUBLICATION,
                PUBLICATION_COLLISION
            WHERE
                PUBLICATION.REF_CODE = CITATIONS_ATLAS.REF_CODE
            AND
                PUBLICATION_COLLISION.RUN = 2
            AND
                PUBLICATION.ID = PUBLICATION_COLLISION.PUBLICATION_ID
            GROUP BY
                PUBLICATION.REF_CODE,
                FULL_TITLE,
                PHYS_GROUP,
                INSPIRE_RECORD,
                CITATIONS_TOTAL,
                CITATIONS_SELF,
                PUBLISHED_CITATIONS_TOTAL,
                PUBLISHED_CITATIONS_SELF,
                PUBLICATION_COLLISION.RUN
            """
        else:
            query = """
            SELECT 
                PUBLICATION.REF_CODE,
                FULL_TITLE,
                PHYS_GROUP,
                INSPIRE_RECORD,
                CITATIONS_TOTAL,
                CITATIONS_SELF,
                PUBLISHED_CITATIONS_TOTAL,
                PUBLISHED_CITATIONS_SELF
            FROM
                CITATIONS_ATLAS,
                PUBLICATION
            WHERE
                PUBLICATION.REF_CODE = CITATIONS_ATLAS.REF_CODE
            """

        result = mgr.select(query)
        mgr.close()

        for element in result:
            if element[0] in self._exclusionList:
                continue
            thisDict = {}
            thisDict['Ref Code'] = element[0]
            if element[1]:
                thisDict['Title'] = element[1].replace('\n',' ')
            else:
                thisDict['Title'] = "Missing full title in Atlas database"
                
            if element[2] in self._perfGroups:
                thisDict['Group'] = "PERF"
            else:
                thisDict['Group'] = element[2]
            thisDict['Inspire'] = 'https://inspirehep.net/literature/' + element[3]
            thisDict['Citations (Total)'] = element[4] if element[4] != None else 0 
            thisDict['Citations (Self)'] = element[5] if element[5] != None else 0
            thisDict['Citations'] = int(element[4] if element[4] != None else 0) - int(element[5] if element[5] != None else 0) 
            thisDict['Published Citations (Total)'] = element[6] if element[6] != None else 0
            thisDict['Published Citations (Self)'] = element[7] if element[7] != None else 0
            thisDict['Published Citations'] = int(element[6] if element[6] != None else 0) - int(element[7] if element[7] != None else 0)
            thisDict['InspLink'] = "[[%s][%s]]" % (thisDict['Inspire'], thisDict['Title'])
            thisDict['recid'] = int(element[3])
            if element[0] in tempAggregate:
                thisDict['Self Citations Records'] = deepcopy(tempAggregate[element[0]])
            else:
                thisDict['Self Citations Records'] = list()

            self._loadDictDB[element[0]] = thisDict

    def doFormatting(self):

        self._loadDictDataBase()

        self._outFile.write("|")

        headers = dict( (n,'*%s*' % n) for n in self._fieldnames)

        self._Writer.writerow(headers)

        for element in self._loadDictDB:
            self._currDct = self._loadDictDB[element]
            self._outDct = self._loadDictDB[self._currDct["Ref Code"]]
            self._Writer.writerow(self._outDct)

        now = datetime.now()
        last_updated = now.strftime("%Y-%m-%d, %H:%M")
        self._outFile.write('Last updated: ')
        self._outFile.write(last_updated)
        self._outFile.write('\n\n')
        self._outFile.close()

        return

    def doFormattingRun2(self):

        self.setOutfile(os.path.join(pltDir, "atlas_citations_run2.txt"))
        self.setWriter()
        self._loadDictDataBase(run=2)

        self._outFile.write("|")

        headers = dict( (n,'*%s*' % n) for n in self._fieldnames)

        self._Writer.writerow(headers)

        for element in self._loadDictDB:
            self._currDct = self._loadDictDB[element]
            self._outDct = self._loadDictDB[self._currDct["Ref Code"]]
            self._Writer.writerow(self._outDct)

        now = datetime.now()
        last_updated = now.strftime("%Y-%m-%d, %H:%M")
        self._outFile.write('Last updated: ')
        self._outFile.write(last_updated)
        self._outFile.write('\n\n')
        self._outFile.close()

        return

    def hIndex(self, _l, _k="Citations"):
        h=1
        for h in range(0,len(_l)):
            if len([i[_k] for i in _l if (i.has_key(_k) and i[_k]>=h)]) < h:
                break

        return h-1

    def doStuff(self,run=''):

        _groups = ["BPHY","EXOT","HDBS","HIGG","HION","PERF","STDM","SUSY","TOPQ"]

        # get outputdir from table file
        _outdir = os.path.dirname(self._outFile.name)
        _of = open(os.path.join(_outdir,"atlas_recap%s.txt" % run),"w")
        _of.write("<verbatim>\n")

        _strg = "Citations (Total)"

        _DictList = filter(lambda x:(x.has_key(_strg) and x.has_key('Group')),self._loadDictDB.values())

        print ("elements in dictionary list = %s" % len(_DictList))
        for i in _DictList:
            if not i.has_key("Group"):
                print("this item doesn't have group key: ")
                print(i)
            if not i.has_key("Published Citations"):
                print("this item doesn't have published citations key: ")
                print(i) 
        
        _cit = sum(filter(None, [i[_strg] for i in _DictList]))
        _of.write("==== Citations from All Sources ====\n")
        _of.write("Total number of %s: %d\n" % (_strg, _cit))
        h = self.hIndex(_DictList)
        _of.write("Overall h-Index: %d\n" % h)

        self.createHIndexPlot(_DictList, "ATLAS Overall %s: %d" % (_strg, _cit), \
            os.path.join(_outdir, "citations_overall%s.png" % run), h=h)

        for i in _groups:
            _l = filter(lambda x:(x.has_key("Group") and i==x["Group"]),_DictList)
            _cit = sum(filter(None, [k[_strg] for k in _l]))
            _of.write("\n==== %s ====\n" % i)
            _of.write("Number of %s: %d\n" % (_strg, _cit))
            h = self.hIndex(_l)
            _of.write("h-Index: %d\n" % h)

            self.createHIndexPlot(_l, "ATLAS %s %s: %d" % (i,_strg,_cit), \
                os.path.join(_outdir, "citations_%s%s.png" % (i,run)), h=h)

        _strg = "Citations"

        _DictList = filter(lambda x:(x.has_key(_strg) and x.has_key('Published Citations')),self._loadDictDB.values())

        _of.write("\n\n==== %s from Published Papers ====\n" % _strg)
        _cit = sum([i["Published %s" % _strg] for i in (filter(lambda x:x.has_key("Published Citations"),_DictList))])
        _of.write("Total number of Published %s: %d\n" % (_strg, _cit))
        h = self.hIndex(_DictList, "Published %s" % _strg)
        _of.write("Overall h-Index (Published): %d\n" % h)
        self.createHIndexPlot(_DictList, "ATLAS Overall Published %s: %d" % (_strg, _cit), \
            os.path.join(_outdir, "citations_overall_published%s.png" % run), h=h, c='blue', key="Published %s" % _strg)

        colz = dict(i for i in zip(_groups, pyplot.cm.Paired(np.linspace(0,1,len(_groups)))))

        self._populateAtlasRecordsRefCode()

        for i in _groups:
            _l = filter(lambda x:(i==x["Group"] and x.has_key("Published %s" % _strg)),_DictList)
            _cit = sum([k["Published %s" % _strg] for k in _l])
            _of.write("\n==== %s ====\n" % i)
            _of.write("Number of Published %s: %d\n" % (_strg, _cit))
            h = self.hIndex(_l,"Published %s" % _strg)
            _of.write("h-Index (Published): %d\n" % h)

            self.createHIndexPlot(_l, "%s Published %s: %d" % (i,_strg,_cit), \
                os.path.join(_outdir, "citations_%s_published%s.png" % (i,run)), h=h, c='blue', key="Published %s" % _strg)
            self.makeNeato(_l, colz, run)

        _of.write("</verbatim>\n")
        _of.close()

        return


    def _populateAtlasRecordsRefCode(self):
        mgr = DBManager()
        ATLASrecordsTuples = mgr.select("""
            SELECT INSPIRE_RECORD, REF_CODE FROM CITATIONS_ATLAS
            """)
        mgr.close() 
        for element in ATLASrecordsTuples:
            self._ATLASrecords[int(element[0])] = element[1]


    def makeNeato(self, D, colz, run=''):

        # use all records to make mape and write out legend
        Dsort = sorted(D, key=lambda x:(x["Published Citations (Total)"]), reverse=True)

        _grp = Dsort[0]["Group"]

        pmap = dict( (j,i) for i,j in enumerate(sorted(self._ATLASrecords.keys())))

        # get outputdir from table file
        # legfile = "neato_%s.txt" % _grp
        _outdir = os.path.dirname(self._outFile.name)
        legfile = os.path.join(_outdir, "neato_%s%s.txt" % (_grp,run) )

        l_f = open(legfile,"w")
        l_f.write("|*Id*|*Ref Code*|\n")
        
        for i in Dsort:
            l_f.write("|%d|%s|\n" % (pmap[i["recid"]], i["Ref Code"]))
        l_f.close()

        # now remove papers without citations
        Dsort = filter(lambda x:(x["Published Citations (Total)"])>0, Dsort)

        outfile = os.path.join(_outdir, "neato_%s%s.dot" % (_grp,run) )
        
        c = 255.* colz[_grp]

        o_f = open(outfile,"w")
        o_f.write(""" graph G {
overlap=false;
splines=true;
sep=.1;
fontsize=18;
fontname=Helvetica;
label="Citation Patterns %s";
node [shape=circle,color="#%2x%2x%2x%2x",fontsize=12,style=filled];
        edge [dir=forward, weight=0.85];\n""" % (_grp, c[0],c[1],c[2],c[3]))

        for i,j in enumerate(Dsort):
            recid = j["recid"]
            self_cits = j['Self Citations Records']
            for k in self_cits:
                try:
                    _g = self._ATLASrecords[k][:4]
                except:
                    continue
                if _g in (self._perfGroups + ["DAPR","LUMI","SOFT"]):
                    _g = "PERF"
                if _g != _grp: continue  # only same group (for the time being) 
                o_f.write('%d -- %d;\n' % (pmap[k],pmap[recid]))

        o_f.write("}\n")
        o_f.close()

        os.system("neato -Tpng -Gepsilon=.001 -o%s %s" % (outfile.replace("dot","png"), outfile)) # -Gstart=rand for random results

        return
    

    def createHIndexPlot(self, D, title="", outfile="foo.png", h=None, c='red', key="Citations"):
        _fig = pyplot.figure()
        _fig.clf()
        pyplot.title(title, fontsize="x-large", weight='bold')  
        ax1 = _fig.add_subplot(111)     
        Dsort = sorted(D, key=lambda x:x[key], reverse=True)
        Dsort = filter(lambda x:x[key]>0, Dsort)

        keys = [k["Ref Code"] for k in Dsort]
        
        x = np.linspace(0,len(keys),len(keys),endpoint=False)
        y1 = [k[key] for k in Dsort]
        if len(y1) < 2:
            return
        sum_y = sum(y1)

        labs = []
        log_y = np.log10(y1[0]/y1[-1])>2.75
                
        # plot the first h in red
        ax1.bar(x[:h], y1[:h], 1, color=c, linewidth=0, log=log_y)

        # then the rest in gray
        ax1.bar(x[h:], y1[h:], 1, color='gray', linewidth=0, log=log_y)

        ax1.set_xlim(0,len(keys)+1)

        if log_y:
            ppos = np.log10(h/ax1.get_ylim()[0])/np.log10(ax1.get_ylim()[1]/ax1.get_ylim()[0])
        else:
            ppos = h/ax1.get_ylim()[1]

        ax1.axhline(h,0,h/ax1.get_xlim()[1],color="black", ls='--')
        ax1.axvline(h,0,ppos,color="black", ls='--')

        if log_y:
            ypos = 8e-2
        else:
            ypos = -10
        pyplot.text(h+0.5,h+0.5,"h-Index: %d" % h, fontsize=9)  

        pyplot.savefig(outfile)
        pyplot.close()
        return 


    def makeShortTable(self,run=''):
        _hs = ["Papers","BPHY","EXOT","HDBS","HIGG","HION","PERF","STDM","SUSY","TOPQ"]

        # get outputdir from table file
        _outdir = os.path.dirname(self._outFile.name)
        if run==2:
            _of = open(os.path.join(_outdir,"atlas_summary_run2.txt"),"w")
        else:
            _of = open(os.path.join(_outdir,"atlas_summary.txt"),"w")

        _of.write("|")

        _Writer = csv.DictWriter(_of,_hs,dialect='twiki',extrasaction='ignore',restval=' ')

        headers = dict( (n,'*%s*' % n) for n in _hs)

        _Writer.writerow(headers)

        _dc = {}

        # Number of papers
        _dc["Papers"]="Number of papers"
        for i in _hs[1:]:
            _dc[i] = len(filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values()))

        _Writer.writerow(_dc)   

        # Number of citations
        _dc["Papers"]="Number of total citations"
        for i in _hs[1:]:
            _dc[i] = sum(filter(None, [k["Citations (Total)"] for k in filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values())]))

        _Writer.writerow(_dc)   

        # Average citations
        _dc["Papers"]="Average total citations"
        for i in _hs[1:]:
            _dc[i] = "%.3g" % (1. *  sum(filter(None, [k["Citations (Total)"] for k in filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values())])) / len(filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values())))

        _Writer.writerow(_dc) 

        # Number of citations
        _dc["Papers"]="Number of citations"
        for i in _hs[1:]:
            _dc[i] = sum(filter(None, [k["Citations"] for k in filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values())]))

        _Writer.writerow(_dc)   

        # Average citations
        _dc["Papers"]="Average citations"
        for i in _hs[1:]:
            _dc[i] = "%.3g" % (1. *  sum(filter(None, [k["Citations"] for k in filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values())])) / len(filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values())))

        _Writer.writerow(_dc) 

        # Number of citations
        _dc["Papers"]="Number of published citations"
        for i in _hs[1:]:
            _dc[i] = sum(filter(None, [k["Published Citations"] for k in filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values())]))

        _Writer.writerow(_dc)   

        # Average citations
        _dc["Papers"]="Average published citations"
        for i in _hs[1:]:
            _dc[i] =  "%.3g" % (1. *  sum(filter(None, [k["Published Citations"] for k in filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values())])) / len(filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values())))

        _Writer.writerow(_dc) 

        # h-Index
        _dc["Papers"]="h-Index (Published)"
        for i in _hs[1:]:
            _dc[i] = self.hIndex(filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values()),"Published Citations")

        _Writer.writerow(_dc) 

        # Most popular paper
        _dc["Papers"]="Most popular paper"
        for i in _hs[1:]:
            _dc[i] = sorted(filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values()),key=lambda x:x["Published Citations"],reverse=True)[0]["Published Citations"]
        _Writer.writerow(_dc) 

        # 2n most popular paper
        _dc["Papers"]="2nd most popular paper"
        if run == 2:
            _dc[1] = '-' # this is here because BPHY doesn't have more than 1 single publication in run 2
            startIndex = 2
        else:
            startIndex = 1
        for i in _hs[startIndex:]:
            _dc[i] = sorted(filter(lambda x:(x.has_key('Group') and i==x["Group"]),self._loadDictDB.values()),key=lambda x:x["Published Citations"],reverse=True)[1]["Published Citations"]
        _Writer.writerow(_dc) 

        _of.seek(-1,1)
        now = datetime.now()
        last_updated = now.strftime("%Y-%m-%d, %H:%M")
        _of.write('Last updated: %s\n\n' % last_updated)
        _of.close()
        return

            
    def processCSV(self):

        self.setReader()

        self.setWriter()

        self.doFormatting()

        self.doStuff()

        self.makeShortTable()

        self.doFormattingRun2()

        self.makeShortTable(run=2)

        self.doStuff(run='2')

        return
    
