# -*- coding: utf-8 -*-
"""
#########################################################################################
# ATLASInspireMiner.py
# Data mine Inspire for citations data
# Written by Marcello Barisonzi, ICTP, January 16th, 2015
#########################################################################################
# $Rev: 239129 $
# $LastChangedDate: 2015-05-22 19:10:04 +0200 (Fri, 22 May 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import csv
import time
import cPickle
import sys
from copy import deepcopy
from datetime import date

from InspireMiner import InspireMiner

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position


class ATLASInspireMiner(InspireMiner):

    def __init__(self, debug=0):
        InspireMiner.__init__(self)

        self._debug = debug

        self._ATLASrecords = {}
        self._ATLASrecids = list()
        self._ATLASpublished = list()
        self._loadDict = {}
        self._loadDictDB = {}
        self._outDct = {}
        self._outfile = ''

        self._chunkSize = 100

        self._exclusionList = ["LARG-2009-01", "LARG-2009-02", "IDET-2010-01", "MUON-2010-01", "TCAL-2010-01", "PERF-2010-02", "SOFT-2010-01"]

        return

    def cleanupLink(self, col):
        col = col.replace("%3A",":")
        col = col.replace("%20"," ")
        col = col.replace("%28","(")
        col = col.replace("%29",")")
        col = col.replace("%2C",",")
        col = col.replace("%u2013","-")
        col = col.replace("%3F","?")
        col = col.replace("%3D","=")       
 
        return col

    def setInfile(self, fn):
        f = open(fn)
        self._inFile = f
        return

    def setOutfile(self, fn):
        self._outfile = fn
        return

    def dumpPickle(self):
        pf = open(self._outfile, "w")
        cPickle.dump(self._loadDictDB, pf)
        pf.close()
        pass

    def setReader(self):
        self._Reader = csv.DictReader(self._inFile)


    def extractRecordId(self, string):
        recordStartPos = string.rfind('record/')
        code = ""
        for char in string[recordStartPos+len('record/'):]:
            if char.isdigit():
                code += char
            else:
                break

        if len(code) > 5: # probably valid
            return code
        return ""

    def _populateNewRecords(self):

        query = """
        SELECT 
            PUBLICATION.REF_CODE       as ref_code,
            HREF                       as ALL_LINKS
        FROM
            PUBLICATION
            LEFT OUTER JOIN 
                PHASE PHASE_SUBMISSION
                ON PHASE_SUBMISSION.PUBLICATION_ID = PUBLICATION.ID
                AND PHASE_SUBMISSION.PHASE = 4
            LEFT OUTER JOIN 
                PHASE_LINKS
                ON PHASE_LINKS.PHASE_ID IN (PHASE_SUBMISSION.ID)
        WHERE 
            length(JOURNAL_SUB)>1 AND 
            HREF LIKE '%inspire%' AND
            TYPE = 'final_journalPublicationURL' AND
            REF_CODE NOT IN (SELECT REF_CODE FROM CITATIONS_ATLAS)
        GROUP BY 
            PUBLICATION.REF_CODE,    
            HREF;
        """

        mgr = DBManager()
        paperList = mgr.select(query)
        itemsToInsert = list()

        for item in paperList:
            inspireRecord = self.extractRecordId(item[1]) 
            if inspireRecord:
                itemsToInsert.append((item[0], inspireRecord, item[0][0:4]))

        if itemsToInsert:
            mgr.insert_many("INSERT INTO CITATIONS_ATLAS(REF_CODE, INSPIRE_RECORD, PHYS_GROUP) VALUES (:1, :2, :3)", itemsToInsert)

        mgr.close()

    def _recordsToUpdate(self):
        query = """
        SELECT 
            REF_CODE,
            INSPIRE_RECORD
        FROM
            CITATIONS_ATLAS
        ORDER BY
            LAST_UPDATE ASC
            NULLS FIRST
        """
        mgr = DBManager()
        paperList = mgr.select(query)
        mgr.close()

        return paperList[0:self._chunkSize] # papers updated on chuncks of _chunkSize elements per run

    def _extractUpdateTuples(self):
        if not self._loadDict:
            return

        updateSet = list()

        for element in self._loadDict:
            record = self._loadDict[element]
            updateSet.append(
                (
                record["Citations (Total)"],
                record["Citations (Self)"],
                len(record["Citation Records"]),
                record["Published Citations (Self)"],
                str(date.today()),
                element
                )
            )

        return updateSet

    def _populateAtlasRecids(self):
        mgr = DBManager()
        ATLASrecidsTuples = mgr.select("""
            SELECT INSPIRE_RECORD FROM CITATIONS_ATLAS
            """)
        mgr.close() 
        for element in ATLASrecidsTuples:
            self._ATLASrecids.append(int(element[0]))

    def _countSelfRecids(self):
        for item in self._loadDict:
            record = self._loadDict[item]
            published_self = 0
            for published in record["Citation Records"]:
                recid = published[u'recid']
                if recid in self._ATLASrecids:
                    published_self += 1
                    self._ATLASpublished.append((item,recid,1))
                else:
                    self._ATLASpublished.append((item,recid,0))
            self._loadDict[item]["Published Citations (Self)"] = published_self

    def _updatePublished(self):
        if not self._ATLASpublished:
            return

        mgr = DBManager()
        for item in self._loadDict:
            mgr.insert("""
                DELETE FROM CITATIONS_ATLAS_PUBLISHED WHERE REF_CODE=:1
                """,(item,))

        query = """
        INSERT INTO 
            CITATIONS_ATLAS_PUBLISHED
        (
            REF_CODE,
            INSPIRE_RECORD,
            SELF
        )
        VALUES
        (
            :1,
            :2,
            :3
        )
        """
        mgr.insert_many(query,self._ATLASpublished)

        mgr.close() 

    def _updateRecords(self):
        if not self._loadDict:
            return

        if not self._ATLASrecids:
            self._populateAtlasRecids()

        self._countSelfRecids()

        self._updatePublished()

        query = """
        UPDATE 
            CITATIONS_ATLAS
        SET 
            CITATIONS_TOTAL=:1, 
            CITATIONS_SELF=:2,
            PUBLISHED_CITATIONS_TOTAL=:3,
            PUBLISHED_CITATIONS_SELF=:4,
            LAST_UPDATE=TO_DATE(:5,'yyyy-mm-dd'),
            AVAILABLE='y'
        WHERE REF_CODE=:6
        """

        mgr = DBManager()
        mgr.insert_many(query, 
            self._extractUpdateTuples()
            )
        mgr.close()

        self._loadDict.clear()
        del self._ATLASpublished[:]

    def _prepareForPickle(self):
        self._loadDictDB.clear()

        mgr = DBManager()

        query = """
        SELECT REF_CODE, INSPIRE_RECORD FROM CITATIONS_ATLAS_PUBLISHED ORDER BY REF_CODE
        """

        result = mgr.select(query)

        tempAggregate = {}
        for element in result:
            miniDict = {u'recid':int(element[1])}
            if element[0] in tempAggregate:
                lista = tempAggregate[element[0]]
                lista.append(miniDict)
                tempAggregate[element[0]] = lista
            else:
                tempAggregate[element[0]] = list()
                tempAggregate[element[0]].append(miniDict)

        query = """
        SELECT 
            PUBLICATION.REF_CODE,
            FULL_TITLE,
            PHYS_GROUP,
            INSPIRE_RECORD,
            CITATIONS_TOTAL,
            CITATIONS_SELF,
            PUBLISHED_CITATIONS_TOTAL,
            PUBLISHED_CITATIONS_SELF
        FROM
            CITATIONS_ATLAS,
            PUBLICATION
        WHERE
            PUBLICATION.REF_CODE = CITATIONS_ATLAS.REF_CODE
        """

        result = mgr.select(query)
        mgr.close()

        for element in result:
            #if element[0] in self._exclusionList:
            #    continue
            thisDict = {}
            thisDict['Ref Code'] = element[0]
            thisDict['Inspire'] = 'https://inspirehep.net/literature/' + element[3]
            thisDict['Citations (Total)'] = element[4] if element[4] != None else 0 
            thisDict['Citations (Self)'] = element[5] if element[5] != None else 0
            thisDict['Citations'] = int(element[4] if element[4] != None else 0) - int(element[5] if element[5] != None else 0) 
            if element[0] in tempAggregate:
                thisDict['Citation Records'] = deepcopy(tempAggregate[element[0]])
            else:
                thisDict['Citation Records'] = list()

            self._loadDictDB[element[0]] = thisDict

    def mineInspire(self):

        self._populateNewRecords()

        self._populateAtlasRecids()

        recordsToUpdate = self._recordsToUpdate()

        print ("These ATLAS items will be updated on this run: %s" % recordsToUpdate)

        _all = len(recordsToUpdate)
        _cnt = 0
        _t0  = time.time()
        _t   = _t0

        for r in recordsToUpdate:
            _t = time.time()
            if _cnt: print "[INFO] Done %d/%d\tTime elapsed: %d sec\tETA: %d sec" % (_cnt,_all, _t-_t0, 1.*(_all-_cnt)*(_t-_t0)/_cnt)
            _cnt += 1

            if not r[1]: # if for some reason there's not recid
                continue

            self.getCitationsByRecid(r[1])
            self._loadDict[r[0]] = deepcopy(self._outDct)
            if _cnt%10 == 0:
                self._updateRecords()

        self._updateRecords()

        self._prepareForPickle()

        self.dumpPickle()

        return
    