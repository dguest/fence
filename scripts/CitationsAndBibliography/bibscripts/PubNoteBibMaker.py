# -*- coding: utf-8 -*-
from __future__ import print_function

import re
import time
import sys

import certifi
import urllib3.contrib.pyopenssl

from PubNoteFormatter import PubNoteFormatter
from BibUnicodeTex import unicode_to_tex

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

# TODO: these 2 lines should be removed. Will need to test
# setting encoding on first line of code should sufficy
reload(sys)
sys.setdefaultencoding('utf8')

class PubNoteBibMaker(PubNoteFormatter):
    def __init__(self):
        super(PubNoteBibMaker, self).__init__()
        
        self._ptn1 = re.compile("(http://cdsweb.cern.ch/record/[0-9]+)")
        self._ptn2 = re.compile("(https://cdsweb.cern.ch/record/[0-9]+)")
        
        # Set up a Pool Manager for verifying certificates
        urllib3.contrib.pyopenssl.inject_into_urllib3()
        self.http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())

        return

    def _populate_exclusion_list(self):
        """
        pubnotes with public results listing flag set to 1 shouldn't be listed
        """
        dbm = DBManager()
        results = dbm.execute("""
            SELECT 
            FINAL_REF_CODE 
            FROM 
            PUBNOTE_PUBLICATION 
            WHERE 
            PUBLIC_RESULTS_LISTING_FLAG = '1'
            """)
        dbm.close()
        return [result[0] for result in results]

    def outputBibTeX(self, _d):
        return """
@Booklet{%(Ref Code)s,
    author       = "%(Author)s",
    title        = "{%(Full Title)s}",
    howpublished = "{%(Ref Code)s}",
    url          = "%(Cds Url)s",
    year         = "%(Year)s",
}
""" % _d

    def getCDSurl(self):
        """ Replace CONF note link wth CDS link
        """

        # req0 = requests.get(self._currDct['Cds Url'])
        req = self.http.request('GET', self._currDct['Cds Url'])
        # print('text:', req0.text)
        # print('data:', req.data)

        # m = self._ptn.search(req.text)
        m1 = self._ptn1.search(req.data)
        m2 = self._ptn2.search(req.data)
        # print('m1:', m1)
        # print('m2:', m2)

        if m1:
            l = m1.groups(1)[0]
            self._currDct['Cds Url'] = l
            # print('http link:', l)
        elif m2:
            l = m2.groups(1)[0]
            self._currDct['Cds Url'] = l
            # print('https link:', l)

        time.sleep(0.2)

        return

    def makeAuthors(self):
        _auth = ""

        _authors = self._currDct["Editorial Team"].split(",")

        _authors.sort(key=lambda x:x.split(" ")[-1])

        if len(_authors) > 6:
            _auth = u"%s et al." % _authors[0].strip()
        else:
            for i in _authors:
                _auth += u"%s and " % i.strip()

        return self.cleanup(_auth)

    def cleanup(self,_s):
        
        # fix CMS bullshit https://inspirehep.net/record/1328962
        _s = _s.replace(u'\u1e6b','\\bar{t}')

        try:
            _s.encode("ascii")
        except:
            _s = unicode_to_tex(_s)
            # _s = _s.replace("\\space"," ")
        return _s

    def doFormatting(self):
        exclusion_list = self._populate_exclusion_list()
        print(exclusion_list)
        for r in self._Reader:
            if r['Ref Code'] == '':
                continue
            self._currDct = r

            if self._currDct["Ref Code"] in exclusion_list:
                print("Skipping pubnote because not in public results listing %s" % self._currDct["Ref Code"])
                continue

            r['Ref Code'] = r['Ref Code'].strip("/")

            print(r['Ref Code'])
            #print(r)

            if not "cds" in self._currDct['Cds Url']:
                #print("trying to get cdsurl")
                self.getCDSurl()

            if "USE_ADHOC_LIST" in self._currDct["Additional Notes"]:
                self._currDct['Author'] = self.makeAuthors()
            elif "ATLAS" in self._currDct["Full Title"] and "CMS" in self._currDct["Full Title"]:
                self._currDct['Author'] = "{ATLAS and CMS Collaborations}"
            else:
                self._currDct['Author'] = "{ATLAS Collaboration}"

            if self._currDct.has_key('Final Sign Off'):
                self._currDct['Year'] = self._currDct['Final Sign Off'].split("/")[0]
            elif self._currDct.has_key('Phase1 Sign Off 2'):
                self._currDct['Year'] = self._currDct['Phase1 Sign Off 2'].split("/")[0]

            self._currDct['Full Title'] = self.cleanup(self._currDct['Full Title'])

            self._outFile.write(self.outputBibTeX(self._currDct))



        return

    def processCSV(self):

        self.setReader()

        self.doFormatting()

        self._outFile.close()
        self._inFile.close()

