# -*- coding: utf-8 -*-
"""
#########################################################################################
# PubNoteFormatter.py
# Derived class to format CSV to Twiki tables, Glance query 5967
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, October 4th, 2013
#########################################################################################
# $Rev: 241792 $
# $LastChangedDate: 2016-03-10 12:09:38 +0100 (Thu, 10 Mar 2016) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import re

from datetime import datetime

from BaseFormatter import BaseFormatter

class PubNoteFormatter(BaseFormatter):

    def __init__(self):
        super(PubNoteFormatter, self).__init__()
        self._fieldnames=('Reference', 'Full Title', 'Publication Date', 'Groups')
        self._afsDir = "/afs/cern.ch/atlas/www/GROUPS/PHYSICS/PUBNOTES/"
        #self._ptn = re.compile("(https://atglance.web.cern.ch/atglance/pubnote/detailAnalysis.php\?readonly=true&id=[0-9]+)")
        self._ptn = re.compile(r"(https://glance.cern.ch/atlas/analysis/pubnotes/details.php\?id=[0-9]+)")
        return

    def mergeGroups(self):
        lst  = [self._currDct["Lead Group"]]
        if self._currDct["Other Groups"] != "":
            lst += self._currDct["Other Groups"].split(",")

        self._currDct["Groups"] = " / ".join(lst)

    def makeLinks(self):
        _l = self._ptn.findall(self._currDct["ID/Link"])[0]

        _type = self._currDct["Type"]

        #if _type != "UPGRADE" and _type != "DAQ":
        #    self._currDct["Reference"] = "[[https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/%s][%s]]" % (self._currDct["Ref Code"], self._currDct["Ref Code"])
        #else:
        #    self._currDct["Reference"] = self._currDct["Ref Code"]

        if self.noDirectory():
            if self._currDct["Cds Url"] != "":
                self._currDct["Reference"] = "[[%s][%s]]" % (self._currDct["Cds Url"], self._currDct["Ref Code"])
            else:
                self._currDct["Reference"] = self._currDct["Ref Code"]
        else:
            self._currDct["Reference"] = "[[https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/%s][%s]]" % (self._currDct["Ref Code"], self._currDct["Ref Code"])

        #self._currDct["Links"] = "[[%s][link]]" % self._currDct["Cds Url"]

        return

    def getPubDateAndTitle(self):
        basedir = "/afs/cern.ch/atlas/www/GROUPS/PHYSICS/PUBNOTES"
        rc =  self._currDct["Ref Code"]
        infile = "%s/%s/%s.info" % (basedir, rc, rc)

        try:
            f = open(infile)

            date_ptn = re.compile("<DATE>.*?\n(.*)")
            titl_ptn = re.compile("<TITLE>\s*\n(.*?)\n<DATE>")

            a = f.read()

	    #print infile, date_ptn.findall(a)

            for i in date_ptn.findall(a):
	        #print datetime.strptime(i, "%d-%m-%Y")
                self._currDct["Publication Date"] = datetime.strptime(i.strip(), "%d-%m-%Y").strftime("%Y/%m/%d")

            for i in titl_ptn.findall(a):
                #print "Replacing title from CDS: %s" % i.strip().replace("\n"," ")
                self._currDct["Full Title"] = i.strip().replace("\n"," ")

            f.close()

        except:
            print "ERROR: File %s does not exist." % infile
            self._currDct["Publication Date"] = self._currDct["Phase1 Sign Off 2"]

        #print self._currDct["Publication Date"]

        return


    def doFormatting(self):

        self._outFile.write("|")

        headers = dict( (n,'*%s*'%n) for n in self._fieldnames)

        self._Writer.writerow(headers)

        rows = []

        for r in self._Reader:
            self._currDct = r

            if self._currDct["Ref Code"] == "":
                continue
            if self._currDct["Phase1 Sign Off 2"] == "":
                continue

            if not self.noDirectory() and self.hasEmbargo():
                continue

            self._currDct["Type"] = self._currDct["Ref Code"].split("-")[1]

            #if self._filter and self._currDct["Type"] not in self._filter:
            if self._filter and self._currDct["Lead Group"] not in self._filter:
                    continue

            self.getPubDateAndTitle()

            self.cleanupTitle('Full Title')

            self.mergeGroups()

            self.makeLinks()

            # protect against empty items:
            for i in self._fieldnames:
                if self._currDct[i] == "":
                    self._currDct[i] = " "

            rows.append(self._currDct.copy())

        # this can be used only after getPubDate()
        rows.sort(reverse=True, key=lambda x: x["Publication Date"])
	#rows.sort(reverse=True, key=lambda x: datetime.strptime(x["Phase1 Sign Off 2"],  "%Y/%m/%d"))



        for i,r in enumerate(rows):
            self._Writer.writerow(r)


        ### overwrite stray delimiter
        self._outFile.seek(-1,1)
