# -*- coding: utf-8 -*-
"""
#########################################################################################
# CMSInspireMiner.py
# Data mine Inspire for citations data
# Written by Marcello Barisonzi, ICTP, January 16th, 2015
#########################################################################################
# $Rev: 241109 $
# $LastChangedDate: 2015-12-14 19:41:28 +0100 (Mon, 14 Dec 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

from __future__ import print_function

import re
import time
import cPickle
import json
import glob
from copy import deepcopy
import certifi
import urllib3.contrib.pyopenssl

def getCMSCode(dct):
    # Get CMS code from publication info and sometimes comments
    _cmsRec = []

    # hard coded value for CMS TDR
    if dct["doi"] == u"10.1088/1748-0221/3/08/S08004":
        return ["CMS-TDR-08-001"]

    try:
        _cmsRec = filter(lambda x:x and re.search("CMS-[A-Z,0-9]{3}-[0-9]+-[0-9]+", x), dct["primary_report_number"])
        if not len(_cmsRec):
            _cmsRec += re.findall("(CMS-[A-Z,0-9]{3}+-[0-9]+-[0-9]+)", dct["comment"])
    except:
        return []
    return _cmsRec

class CMSInspireMiner:

    def __init__(self, debug=0, restart=False):

        self._debug = debug
        self._restart = restart

        self.citationsPtn = re.compile("Cited by:\s([0-9]+)\srecords")
        self.selfcitPtn   = re.compile("self-citations:\s([0-9]+)\srecords")

        self._outDct = {}
        self._CMSrecords = {}
        self._loadDict = []
        self._indir = ''
        self._outfile = ''

        # Set up a Pool Manager for verifying certificates
        urllib3.contrib.pyopenssl.inject_into_urllib3()
        self.http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())

        return

    def cleanupTitle(self, tit):
        return tit.replace(u'\u1e6b','\\bar{t}')

    def setIndir(self, dir):
        self._indir = dir
        return

    def setOutfile(self, fn):
        self._outfile = fn
        return

    def dumpPickle(self):
        pf = open(self._outfile, "w")
        cPickle.dump(self._loadDict, pf)
        pf.close()

    def loadPickle(self):
        pf = open(self._outfile)
        self._loadDict = cPickle.load(pf)
        pf.close()

    def fillDict(self):

        _cmsRec = getCMSCode(self._currDct)

        # eliminate worthless TDR
        if "CMS-TDR-008-1" in _cmsRec:
            return False

        # one record with string, to be expunged
        if _cmsRec.__class__ != [].__class__:
            return False

        print(self._currDct["primary_report_number"], _cmsRec)

        if len(_cmsRec) == 0:
            return False

        self._currDct['Ref Code'] = _cmsRec[0]

        return True

    def getCitations(self):

        recid = self._currDct["recid"]

        print("getCitations(%d)" % recid)

        # get papers from CDS
        _lnk = "https://old.inspirehep.net/record/%d/citations" % recid

        try:
            r = self.http.request('GET', _lnk)
            _rtext = r.data
        except urllib3.exceptions.NewConnectionError:
            print("Connection error for %s" % _lnk)
            _rtext = ""

        m = self.citationsPtn.search(_rtext)

        print(_lnk)

        if m:
            self._outDct["Citations (Total)"] = int(m.group(1))
        else:
            self._outDct["Citations (Total)"] = None

        m = self.selfcitPtn.search(_rtext)

        if m:
            self._outDct["Citations (Self)"] = int(m.group(1))
        else:
            self._outDct["Citations (Self)"] = None

        try:
            self._outDct["Citations"] = self._outDct["Citations (Total)"] - self._outDct["Citations (Self)"]
        except:
            self._outDct["Citations"] = None

        print(self._outDct["Citations (Total)"],  self._outDct["Citations (Self)"], self._outDct["Citations"])

        time.sleep(.3)

        # now get which papers cite it
        _ld = [0]*250
        _la = []
        _cnt = 0
        
        while(self._outDct["Citations (Total)"] > 0 and len(_ld)>=250):

            _lnk2 = "https://old.inspirehep.net/search?p=refersto+recid+%d+and+tc+p&of=recjson&ot=recid&rg=250&jrec=%d" % (recid, _cnt+1)

            print(_lnk2)

            try:
                r = self.http.request('GET', _lnk2)
                # _rtext = r.data.strip()
                _rtext = r.data
            except urllib3.exceptions.NewConnectionError:
                print("Connection error for %s" % _lnk2)
                _rtext = ""

            # convert to list of dicts
            try:
                #exec("_ld = %s" % r.text.strip())
                _ld = json.loads(_rtext.strip())
            except:
                print("Except")
                _ld = []

            print("--> Got %d lines" % len(_ld))

            _la += _ld

            _cnt += 250

            time.sleep(.3)

        self._outDct["Citation Records"] = _la

        return

    def mineInspire(self):
        for i in glob.glob(self._indir + "CMScit_*.json"):
            i_f = open(i)
            ### inspire returns a bad cms record that has been deleted,
            ### we must remove it from the input file otherwise the json parse will break
            #i_f.replace('{"comment": ["There is an error on cover due to a technical problem for some items", "Related figures: <a href=\"http://cdsweb.cern.ch/record/1326290\">CMS-PHO-GEN-2006-001</a>"], "doi": null, "title": {"subtitle": "Technical Design Report Volume 1: Detector Performance and Software", "title": "CMS Physics"}, "accelerator_experiment": [{"experiment": "CERN-LHC-CMS"}, {"accelerator": "CERN SPS"}, {"accelerator": "CERN SPS"}], "collection": [{"primary": "Citeable"}, {"primary": "CORE"}, {"primary": "HEP"}, {"primary": "Report"}], "primary_report_number": ["CERN-LHCC-2006-001", "CMS-TDR-8-1"], "publication_info": null, "recid": 1614070}',"")
            self._loadDict += json.load(i_f)
            i_f.close()

        print("Found %d records" % len(self._loadDict))

        # get list of all CMS papers
        for v in self._loadDict:
            print(v['recid'], v["primary_report_number"])
            try:
                self._CMSrecords[v['recid']] = getCMSCode(v)[0]
            except:
                continue

        _all = len(self._loadDict)
        _cnt = 0
        _t0  = time.time()
        _t   = _t0

        for r in self._loadDict:
            self._currDct = r

            _t = time.time()
            if _cnt: print("Done %d/%d\tTime elapsed: %d sec\tETA: %d sec" % (_cnt,_all, _t-_t0, 1.*(_all-_cnt)*(_t-_t0)/_cnt))
            _cnt += 1

            if not self.fillDict():
                continue

            # With high debug, only process first 20 publications
            if _cnt > 20 and self._debug >= 10:
                continue

            self.getCitations()
            self._currDct['citations'] = deepcopy(self._outDct)
            time.sleep(0.2)

            if not self._outDct.has_key("Citation Records"):
                continue

            self._outDct["Published Citations (Total)"] = len(self._outDct["Citation Records"])
            self._outDct["Published Citations (Self)"] = len([d for d in self._outDct["Citation Records"] if int(d['recid']) in self._CMSrecords.keys()])
            self._outDct["Published Citations"] = self._outDct["Published Citations (Total)"] -self._outDct["Published Citations (Self)"] 
            self._currDct['citations'] = deepcopy(self._outDct)

            # Periodically dump pickle file
            if _cnt%10 == 0:
                print("Dumping pickle file")
                self.dumpPickle()

        # Dump final pickle file
        self.dumpPickle()
