# -*- coding: utf-8 -*-
"""
#########################################################################################
# BibTexFormatter.py
# Output list of ATLAS Papers in BibTeX format
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, July 14th, 2014
#########################################################################################
# $Rev: 187302 $
# $LastChangedDate: 2014-07-15 16:46:52 +0200 (Tue, 15 Jul 2014) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import re
import os
from datetime import datetime

from BaseFormatter import BaseFormatter

class BibTeXArticle:
    
    def __init__(self, dct):
        self._paperDict = dct

        # take the year and month of submission/publication
        if dct["Published Online"] != "":
            _k = "Published Online"
        else:
            _k = "Journal Sub"  

        _d = datetime.strptime(dct[_k], "%Y/%m/%d")

        self._paperDict["Year"]  = _d.strftime("%Y")
        self._paperDict["Month"] = _d.strftime("%b")

	# get arXiv number from link
        self._paperDict["ArXiv"] = os.path.basename(dct["ArXiv"])


        self.extractLinksFromRaw()
        return

    def extractLinksFromRaw(self):
        """Create a column named 'Links' with the content of:
        -- Inspire
        -- Plots (if not embargoed)
        -- ArXiv
        -- Video
        -- Erratum
        -- Synopsis
        -- Twiki
        """
        
        keylist = ["arXiv","Inspire","Erratum","Figures","Video","Synopsis","twiki","doi"]

        ptn = re.compile("\[(.*?)@(.*?)@(final.*?)\]")

        self._paperDict["doi"] = ""
        self._paperDict["Journal Ref"] = "Submitted to %s" % self._paperDict["Journal"]
        self._paperDict["Cerncode"] = ""


        for i in self._paperDict["Raw Links"].split(","):
            for _name, _link, _type in ptn.findall(i):
                    _found = False
                    for j in keylist:
                        if j.lower() in _name.lower():
                            self._paperDict[j] = _link # self.cleanupLink(_link)
                            _found = True
                        elif "cern-ph-ep" in _name.lower():
                            self._paperDict["Cerncode"] = _name.strip()
                            _found = True
                     
                    if not _found: 
                        #print _name
                        self._paperDict["Journal Ref"] = _name.strip()
        
        # get only last part of DOI link
        self._paperDict["doi"] = self._paperDict["doi"].split("doi.org/")[-1]

        return

    def outputBibTeX(self):
      return """@article{atlas:%(Ref Code)s,
      title          = "{%(Full Title)s}",
      collaboration  = "ATLAS",
      journal        = "%(Journal Ref)s",
      year           = "%(Year)s",
      month          = "%(Month)s",
      doi            = "%(doi)s",
      eprint         = "%(ArXiv)s",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "%(Cerncode)s",
      SLACcitation   = "%%%%CITATION = ARXIV:%(ArXiv)s;%%%%",
}

""" % self._paperDict


class BibTeXFormatter(BaseFormatter):

    def __init__(self):
        super(BibTeXFormatter, self).__init__()

        self._articles = []
        return


    def doFormatting(self):
        for r in self._Reader:
            self._currDct = r

            self._articles.append(BibTeXArticle(r))

        for i in  self._articles:
            self._outFile.write(self.cleanupLink(i.outputBibTeX()))

        return

    def processCSV(self):

        self.setReader()

        self.doFormatting()

        #####################
        #    Date Updated   #
        #####################
        #now = datetime.now()
        #last_updated = now.strftime("%Y-%m-%d, %H:%M")
        #self._outFile.write('Last updated: ')
        #self._outFile.write(last_updated)
        #self._outFile.write('\n\n')

        self._outFile.close()
        self._inFile.close()
