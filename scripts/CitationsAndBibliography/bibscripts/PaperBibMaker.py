# -*- coding: utf-8 -*-

from __future__ import print_function

import json
import cPickle
import time
import re

import certifi
import urllib3.contrib.pyopenssl

from PubNoteBibMaker import PubNoteBibMaker
from BibTeXArticle import *

class PaperBibMaker(PubNoteBibMaker):
    ATLAS = 0
    CMS   = 1
    OTHER = 2

    def __init__(self, typ):
        super(PubNoteBibMaker, self).__init__()

        self._citationsDict = {}
        self._type = typ
        self._atlasRecords = []
        self._biblioSet    = set()
        self._biblioList   = []
        self._refCodeMap   = {}

        # Set up a Pool Manager for verifying certificates
        urllib3.contrib.pyopenssl.inject_into_urllib3()
        self.http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())

        return

    def ATLASorCMS(self,d):
        for i in d:
            if i["corporate_name"]:
                for j in i["corporate_name"][0].values():
                    if "CMS" in j or "ATLAS" in j:
                        return True
        return False


    def outputBibTeX(self, _d):
        return """
@Booklet{%(Ref Code)s,
    author       = "{ATLAS Collaboration}",
    title        = "{%(Full Title)s}",
    howpublished = "%(Ref Code)s",
    url          = "%(Cds Url)s",
    year         = "%(Year)s",
}
""" % _d

    def getBibliography(self):
        for k,v in self._citationsDict.iteritems():
            self._currDct = v
            
            for b in v["Bibliography Records"]:
                if b["recid"] in self._atlasRecords:
                    continue
                self._biblioSet.add(b["recid"])

        print("Unique bibliography items:", len(self._biblioSet))

        return

    def dumpBibliography(self):
        _all = len(self._biblioSet)
        _cnt = 0
        _t0  = time.time()
        _t   = _t0

        for _id in self._biblioSet:
            _t = time.time()
            if _cnt: print("Done %d/%d\tTime elapsed: %d sec\tETA: %d sec" % (_cnt,_all, _t-_t0, 1.*(_all-_cnt)*(_t-_t0)/_cnt))

            _ld = self.getInspireRecord(_id)

            time.sleep(.3)
            _cnt += 1

            if not _ld:
                continue

            if self.ATLASorCMS(_ld):
                continue
            print(_ld[0]["doi"])
            

            self._biblioList += _ld

        print("Final number of items:",len(self._biblioList))

        json.dump(self._biblioList, self._outFile)
        self._outFile.close()

    def getInspireRecord(self, rec):
        _lnk = "https://old.inspirehep.net/record/%d?of=recjson&ot=recid,title,authors,accelerator_experiment,collection,comment,doi,publication_info,agency_code,code_designation,corporate_name,edition_statement,internal_notes,journal_info,other_report_number,primary_report_number" % rec

        # r = requests.get(_lnk, verify=False)
        r = self.http.request('GET', _lnk)
        try:
            # _ld = json.loads(r.text.strip())
            _ld = json.loads(r.data.strip())
        except:
            print("Except https://old.inspirehep.net/record/%d", rec)
            _ld = None

        return _ld

    def loadBibliography(self):
        try:
            self._biblioList += json.load(self._inFile)
        except:
            self._inFile.seek(0)
            self._biblioList += cPickle.load(self._inFile)
        self._inFile.close()
        print("Bibliography has now %d items" % len(self._biblioList))
        return

    def makeBibFile(self):
        for d in self._biblioList:
            
            if self._type == self.ATLAS:
                if d["recid"] in self._refCodeMap.keys():
                    d["Ref Code"] = self._refCodeMap[d["recid"]]
                else:
                    print("%d not in ATLAS? Skipping..." % d["recid"])
                    continue
                

            b = BibTeXArticle(d, strategy=[ATLASPaper,CMSPaper,OtherPaper][self._type])
            self._outFile.write(b.outputBibTeX())
        self._outFile.close()
        return

    def processJSON(self):

        self._citationsDict.update(json.load(self._inFile))
        self._inFile.close()

        for k,v in self._citationsDict.iteritems():
            _id = int(re.findall("[0-9]+", v["Inspire"])[0])
            self._atlasRecords.append(_id)

        self.getBibliography()

    def loadRefCodeMap(self):

        # maurizio colautti note: we can extract this info from the database / search interfaces / db api

        """As Inspire records do not store ATLAS Ref Codes, we need to get it from an external map"""
        cD = cPickle.load(self._inFile)
        self._inFile.close()
        ptn1 = re.compile("https://inspire.+?.net/literature/([0-9]+)")
        ptn2 = re.compile("http://inspire.+?.net/literature/([0-9]+)")
        for k,v in cD.iteritems():
            # print(k, v)
            if not v.has_key('Inspire'):
                continue
            m1 = ptn1.search(v['Inspire'])
            m2 = ptn2.search(v['Inspire'])
            if m1:
                self._refCodeMap[int(m1.groups(1)[0])] = k
            elif m2:
                print("loadRefCodeMap: Inspire record with http:", k, v)
                self._refCodeMap[int(m2.groups(1)[0])] = k
        return

