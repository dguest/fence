# -*- coding: utf-8 -*-
"""
#########################################################################################
# InspireCitationsDumper.py
# Data mine Inspire for citations data
# Written by Marcello Barisonzi, ICTP, December 5th, 2014
#########################################################################################
# $Rev: 244869 $
# $LastChangedDate: 2017-01-03 14:22:05 +0100 (Tue, 03 Jan 2017) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import re
import time
# import cPickle
import json
from copy import deepcopy

import certifi
import urllib3.contrib.pyopenssl

from AtlasPublicFormatter import AtlasPublicFormatter

class InspireCitationsDumper(AtlasPublicFormatter):

    def __init__(self):
        AtlasPublicFormatter.__init__(self)

        self.citationsPtn = re.compile("Cited by:\s([0-9]+)\srecords")
        self.selfcitPtn   = re.compile("self-citations:\s([0-9]+)\srecords")

        self._fieldnames = ["Ref Code","InspLink","Citations (Total)","Citations (Self)","Citations", "Filtered Citations (Total)","Filtered Citations (Self)","Filtered Citations"]

        self._outDct = {}

        self._ATLASrecords = {}
        self._loadDict = {}

        # Set up a Pool Manager for verifying certificates
        urllib3.contrib.pyopenssl.inject_into_urllib3()
        self.http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())

        return


    def fillDict(self):

        ptn = re.compile("\[Inspire@(.*?)@final.*?\]")

        self._outDct["Ref Code"] = self._currDct["Ref Code"]

        if  self._currDct["Inspire"] != "":
            self._outDct["Inspire"] =  self._currDct["Inspire"]
        else:
            for i in self._currDct["Raw Links"].split(","):
                m = ptn.search(i)
                if m:
                    self._outDct["Inspire"] = self.cleanupLink(m.groups(1))
                    break

            if not m:
                print "Inspire not found??? %s >>>\t%s" %  (self._currDct["Ref Code"], self._currDct["Raw Links"])
                return False

        self._outDct["InspLink"] = "[[%s][%s]]" % (self._outDct["Inspire"], self._currDct["Full Title"])

        #print self._outDct["InspLink"]

        return True
        

    def getCitations(self):

        ptn = re.compile("([0-9]+)")

        m = ptn.search(self._outDct["Inspire"])

        if not m:
            return

        recid = int(m.groups(1)[0])

        # get papers from CDS
        _lnk = "https://old.inspirehep.net/record/%d/citations" % recid

        try:
            # r = requests.get(_lnk, verify=False, timeout=None)
            # _txt = r.text
            r = self.http.request('GET', _lnk)
            _txt = r.data

            m = self.citationsPtn.search(_txt)

        except:
            print "getCitations() could not open link:", _lnk
            m = None
            _txt = ''

        #print self.citationsPtn.findall(r.data)

        if m:
            self._outDct["Citations (Total)"] = int(m.group(1))
        else:
            self._outDct["Citations (Total)"] = None

        m = self.selfcitPtn.search(_txt)

        #print self.selfcitPtn.findall(r.data)

        if m:
            self._outDct["Citations (Self)"] = int(m.group(1))
        else:
            self._outDct["Citations (Self)"] = None

        try:
            self._outDct["Citations"] = self._outDct["Citations (Total)"] - self._outDct["Citations (Self)"]
        except:
            self._outDct["Citations"] = None

        print self._outDct["Citations (Total)"],  self._outDct["Citations (Self)"], self._outDct["Citations"]

        time.sleep(.3)

        # now get which papers cite it
        _la = []
        _ld = [0]*250
        _cnt = 0
        
        while(self._outDct["Citations (Total)"] > 0 and len(_ld)>=250):

            _lnk2 = "https://old.inspirehep.net/search?p=refersto+recid+%d+and+tc+p&of=recjson&ot=recid&rg=250&jrec=%d" % (recid, _cnt+1)

            print _lnk2

            # r = requests.get(_lnk2, verify=False)
            r = self.http.request('GET', _lnk2)

            # convert to list of dicts
            try:
                #exec("_ld = %s" % r.text.strip())
                # _ld = json.loads(r.text.strip())
                _ld = json.loads(r.data.strip())
            except:
                print "Except"
                _ld = []

            print "--> Got %d lines" % len(_ld)

            _la += _ld

            _cnt += 250

            time.sleep(.3)

        self._outDct["Citation Records"] = _la
  
        # now get the bibliography
        _la = []

        _lnk3 = "https://old.inspirehep.net/search?p=citedby+recid+%d&of=recjson&ot=recid&rg=250" % recid

        print _lnk3

        # r = requests.get(_lnk3, verify=False)
        r = self.http.request('GET', _lnk3)
        # r = requests.get(_lnk3, verify=False)

        # convert to list of dicts
        try:
            #exec("_ld = %s" % r.text.strip())
            # _ld = json.loads(r.text.strip())
            _ld = json.loads(r.data.strip())
        except:
            print "Except"
            _ld = []

        print "--> Got %d lines" % len(_ld)

        _la += _ld

        self._outDct["Bibliography Records"] = _la

        return


    def doFormatting(self):

        for r in self._Reader:
            self._currDct = r

            if not self.fillDict():
                continue

            if self._filter:
                found = False
                for i in self._filter:
                    if i in self._currDct["Lead Group"]:
                        found = True
                        break
                if not found:
                    continue

            if not self._outDct.has_key("Inspire"):
                continue

            self.getCitations()

            self._loadDict[self._outDct["Ref Code"]] = deepcopy(self._outDct)

            # incremental, by line update
            self._outFile.seek(0)
            json.dump(self._loadDict, self._outFile)

        return

    def processCSV(self):

        self.setReader()

        self.doFormatting()

        return
    
