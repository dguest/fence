# -*- coding: utf-8 -*-
"""
#########################################################################################
# CMSCitationsCruncher.py
# Make useful? paper statistics
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, February 7th, 2013
#########################################################################################
# $Rev: 238608 $
# $LastChangedDate: 2015-03-24 02:26:20 +0100 (Tue, 24 Mar 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import csv
import os
import re
import cPickle
from datetime import datetime

# import matplotlib for headless terminal
# backend has to be set first
# http://stackoverflow.com/questions/5503601/python-headless-matplotlib-pyplot
# sys.path.insert(0,"/afs/cern.ch/user/a/atlaspo/workspace/matplotlib-1.2.0/build/lib.linux-x86_64-2.6/")
import matplotlib as mpl
mpl.use('Agg', warn=False)
from matplotlib import pyplot
import numpy as np

from PyPlotCruncher import PyPlotCruncher


class CMSCitationsCruncher(PyPlotCruncher):

    def __init__(self):
        PyPlotCruncher.__init__(self)

        self._fieldnames = ["Ref Code","InspLink","Citations (Total)","Citations (Self)","Citations", "Published Citations (Total)","Published Citations (Self)","Published Citations"]

        self._CMSrecords = {}
        self._loadDict = []

        return

    def readPickle(self,fn):

        i_f = open(fn)
        self._loadDict = cPickle.load(i_f)
        i_f.close()

        return

    def setPkfile(self, fn):
        self._pkfile = fn

        return

    def cleanupTitle(self, _c):

        col = _c.encode('utf8')

        col = col.replace('\n', '') #breaks wiki tables
        col = col.replace('|', '&#x007C;')#breaks wiki tables
        col = col.replace("K0S","!K0S")
        col = col.replace("ChiB","!ChiB")
        col = col.replace("LeptonJets","!LeptonJets")
        col = col.replace("MinBias","!MinBias")
        col = col.replace("LargeR","!LargeR")
        col = col.replace("WbWb","!WbWb")
        col = col.replace("PbPb","!PbPb")
        col = col.replace("TeV", "<noautolink>TeV</noautolink>")
        col = col.replace("GeV", "<noautolink>GeV</noautolink>")
        col = col.replace("MeV", "<noautolink>MeV</noautolink>")
        col = col.replace("fb-1", "fb<sup>-1</sup>")
        col = col.replace("fb^-1", "fb<sup>-1</sup>")
        col = col.replace("pb-1", "pb<sup>-1</sup>")
        col = col.replace("pb^-1", "pb<sup>-1</sup>")
        col = col.replace("sqrt(s) =", "&#8730;s =")
        col = col.replace("sqrt{s} =", "&#8730;s =")
        col = col.replace("sqrt(s)=", "&#8730;s =")
        col = col.replace("sqrt{s}=", "&#8730;s =")
        col = col.replace("sqrt s =", "&#8730;s =")
        col = col.replace("sqrt s=", "&#8730;s =")
        col = col.replace("sqrt(s_NN)", "&#8730;s<sub>NN</sub>")
        col = col.replace("sqrt(sNN)", "&#8730;s<sub>NN</sub>")
        col = col.replace("sqrts_{nn}", "&#8730;s<sub>NN</sub>")
        col = col.replace("->","&rarr;")
        col = col.replace("\\to","&rarr;")
        col = col.replace("\\rightarrow","&rarr;")
        col = col.replace("*", "&#42;")
        col = col.replace("_b", "<sub>b</sub>")
        col = col.replace("_s", "<sub>s</sub>")
        col = col.replace("^0", "<sup>0</sup>")
        col = col.replace("lambda", "&Lambda;")
        col = col.replace("Lambda", "&Lambda;")
        col = col.replace("alpha", "&alpha;")
        col = col.replace("Upsilon", "&Upsilon;")
        col = col.replace(" psi", " &psi;")
        col = col.replace("psi ", "&psi; ")
        col = col.replace("\\gamma", "&gamma;") 
        col = col.replace("\xcf\x84", "&#8467;")
        col = col.replace("s\xe2\x88\x9a", "&radic;s")
        col = col.replace("\xe2\x88\x9as", "&radic;s")
        col = col.replace("\xce\xb3", "&gamma;")
        col = col.replace("\xe2\x86\x92", "&rarr;")
        col = col.replace("(\xe2\x88\x97)", "<sup>(&#42;)</sup>")
        col = col.replace("\xce\xbd", "&nu;")
        col = col.replace("\xe2\x88\x921", "<sup>-1</sup>")
        col = col.replace("\\bar{q}", "<span style='text-decoration: overline'>q</span>")
        col = col.replace("\\bar{t}", "<span style='text-decoration: overline'>t</span>")
        col = col.replace("\xce\xbc", "&mu;")
        col = col.replace("\xe2\x88\x9a", "&radic;")
        #col = col.replace(u"\u2013","&ndash;")

        return col

    def fillDict(self):

        if not self._currDct.has_key("Ref Code"):
            return False

        if not self._currDct.has_key("citations") or not self._currDct["citations"].has_key("Citation Records"):
            return False

        self._outDct = self._currDct["citations"]

        self._outDct["Ref Code"] = self._currDct["Ref Code"]

        _grp = self._outDct["Ref Code"][4:7]

        if _grp in ["FSQ","QCD","EWK","SMP","FWD"]:
            self._outDct["Group"] = "STD"
        elif _grp=='B2G':
            self._outDct["Group"] = "EXO"
        else:
            self._outDct["Group"] = _grp

        self._outDct["Inspire"] =  self._currDct["recid"]

        if "title" in self._currDct:
            if "title" in self._currDct["title"]:
                self._outDct["InspLink"] = "[[https://inspirehep.net/literature/%d][%s]]" % (self._outDct["Inspire"], self.cleanupTitle(self._currDct["title"]["title"]))

        return True

    def doFormatting(self):

        self._outFile.write("|")

        headers = dict( (n,'*%s*' % n) for n in self._fieldnames)

        self._Writer.writerow(headers)

        _ptn = re.compile("[0-9]+")
        
        i_f = open(self._pkfile)
        self._loadDict = cPickle.load(i_f)
        i_f.close()

        for r in self._loadDict:
            self._currDct = r

            if not self.fillDict():
                print ("entry doesn't have ref code or citations : %s" % r)
                continue

            self._Writer.writerow(self._outDct)

        ### overwrite stray delimiter
        self._outFile.seek(-1,1)

        return

    def hIndex(self, _l, _k="Citations"):
        h=1
        for h in range(0,len(_l)):
            if len([i[_k] for i in _l if i[_k]>=h]) < h:
                break
        return h-1

    def doStuff(self):

        # get outputdir from table file
        _outdir = os.path.dirname(self._outFile.name)
        _of = open(os.path.join(_outdir,"cms_recap.txt"),"w")
        _of.write("<verbatim>\n")

        _DictList = []
        for x in self._loadDict:
            if x.has_key("citations") and x["citations"].has_key("Published Citations"):
                _DictList.append(x["citations"])

        sumTest = 0
        for i in _DictList:
            if i["Citations"] == None:
                i["Citations"] = 0
            if i["Citations (Total)"] == None:
                i["Citations (Total)"] = 0
            sumTest += i["Citations"]

        _cit = sum([i["Citations"] for i in _DictList])
        _of.write("==== Citations from All Sources ====\n")
        _of.write("Total number of citations: %d\n" % _cit)
        h = self.hIndex(_DictList)
        _of.write("Overall h-Index: %d\n" % h)

        self.createHIndexPlot(_DictList, "CMS Overall Citations: %d" % _cit, \
            os.path.join(_outdir, "cms_citations_overall.png"), h=h)

        _groups = ["BPH","EXO","HIG","HIN","SUS","STD","TOP"]

        for i in _groups:
            _l = filter(lambda x:i==x["Group"],_DictList)
            _cit = sum([k["Citations"] for k in _l])
            _of.write("\n==== %s ====\n" % i)
            _of.write("Number of citations: %d\n" % _cit)
            h = self.hIndex(_l)
            _of.write("h-Index: %d\n" % h)

            self.createHIndexPlot(_l, "CMS %s Citations: %d" % (i,_cit), \
                os.path.join(_outdir, "cms_citations_%s.png" % i), h=h)

        _of.write("\n\n==== Citations from Published Papers ====\n")
        _cit = sum([i["Published Citations"] for i in _DictList])
        _of.write("Total number of published citations: %d\n" % _cit)
        h = self.hIndex(_DictList, "Published Citations")
        _of.write("Overall h-Index (published): %d\n" % h)
        self.createHIndexPlot(_DictList, "CMS Overall Published Citations: %d" % _cit, \
            os.path.join(_outdir, "cms_citations_overall_published.png"), h=h, c='blue', key="Published Citations")

        colz = dict(i for i in zip(_groups, pyplot.cm.Paired(np.linspace(0,1,len(_groups)))))

        for i in _groups:
            _l = filter(lambda x:i==x["Group"],_DictList)
            _cit = sum([k["Published Citations"] for k in _l])
            _of.write("\n==== %s ====\n" % i)
            _of.write("Number of published citations: %d\n" % _cit)
            h = self.hIndex(_l,"Published Citations")
            _of.write("h-Index (published): %d\n" % h)

            self.createHIndexPlot(_l, "CMS %s Published Citations: %d" % (i,_cit), \
                os.path.join(_outdir, "cms_citations_%s_published.png" % i), h=h, c='blue', key="Published Citations")

        _of.write("</verbatim>\n")
        _of.close()

        return

    def createHIndexPlot(self, D, title="", outfile="foo.png", h=None, c='red', key="Citations"):
        _fig = pyplot.figure()
        _fig.clf()
        pyplot.title(title, fontsize="x-large", weight='bold')  
        ax1 = _fig.add_subplot(111)     
        Dsort = sorted(D, key=lambda x:x[key], reverse=True)
        Dsort = filter(lambda x:x[key]>0, Dsort)

        keys = [k["Ref Code"] for k in Dsort]
        

        x = np.linspace(0,len(keys),len(keys),endpoint=False)
        y1 = [k[key] for k in Dsort]
        sum_y = sum(y1)
        labs = []
        
        log_y = len(y1) > 0 and (y1[0]/y1[-1]>1e3)
        
        # plot the first h in red
        ax1.bar(x[:h], y1[:h], 1, color=c, linewidth=0, log=log_y)

        # then the rest in gray
        ax1.bar(x[h:], y1[h:], 1, color='gray', linewidth=0, log=log_y)
        
        ax1.set_xlim(0,len(keys)+1)

        if log_y:
            ppos = np.log(h/ax1.get_ylim()[0])/np.log(ax1.get_ylim()[1]/ax1.get_ylim()[0])
        else:
            ppos = h/(ax1.get_ylim()[1]-ax1.get_ylim()[0])

        ax1.axhline(h,0,h/ax1.get_xlim()[1],color="black", ls='--')
        ax1.axvline(h,0,ppos,color="black", ls='--')

        if log_y:
            ypos = 8e-2
        else:
            ypos = -10
        pyplot.text(h+0.5,h+0.5,"h-Index: %d" % h, fontsize=9)  

        pyplot.savefig(outfile)
        pyplot.close()
        return 

    def makeShortTable(self):
        _hs = ["Papers","BPH","EXO","HIG","HIN","STD","SUS","TOP"]

        _l = filter(lambda x:x.has_key("Group"),[i["citations"] for i in self._loadDict if i.has_key("citations")])

        # get outputdir from table file
        _outdir = os.path.dirname(self._outFile.name)
        _of = open(os.path.join(_outdir,"cms_summary.txt"),"w")

        _of.write("|")

        _Writer = csv.DictWriter(_of,_hs,dialect='twiki',extrasaction='ignore',restval=' ')

        headers = dict( (n,'*%s*' % n) for n in _hs)

        _Writer.writerow(headers)

        _dc = {}

        # Number of papers
        _dc["Papers"]="Number of papers"
        for i in _hs[1:]:
            _dc[i] = len(filter(lambda x:i==x["Group"],_l))

        _Writer.writerow(_dc)   

        # Number of citations
        _dc["Papers"]="Number of total citations"
        for i in _hs[1:]:
            _dc[i] = sum([k["Citations (Total)"] for k in filter(lambda x:i==x["Group"],_l)])

        _Writer.writerow(_dc)   

        # Average citations
        _dc["Papers"]="Average total citations"
        for i in _hs[1:]:
            if len(filter(lambda x:i==x["Group"],_l)):
                _dc[i] = "%.3g" % (1. *  sum([k["Citations (Total)"] for k in filter(lambda x:i==x["Group"],_l)]) / len(filter(lambda x:i==x["Group"],_l)))

        _Writer.writerow(_dc) 

        # Number of citations
        _dc["Papers"]="Number of citations"
        for i in _hs[1:]:
            _dc[i] = sum([k["Citations"] for k in filter(lambda x:i==x["Group"],_l)])

        _Writer.writerow(_dc)   

        # Average citations
        _dc["Papers"]="Average citations"
        for i in _hs[1:]:
            if len(filter(lambda x:i==x["Group"],_l)):
                _dc[i] = "%.3g" % (1. *  sum([k["Citations"] for k in filter(lambda x:i==x["Group"],_l)]) / len(filter(lambda x:i==x["Group"],_l)))

        _Writer.writerow(_dc) 

        # Number of citations
        _dc["Papers"]="Number of published citations"
        for i in _hs[1:]:
            _dc[i] = sum([k["Published Citations"] for k in filter(lambda x:i==x["Group"],_l)])

        _Writer.writerow(_dc)   

        # Average citations
        _dc["Papers"]="Average published citations"
        for i in _hs[1:]:
            if len(filter(lambda x:i==x["Group"],_l)):
                _dc[i] =  "%.3g" % (1. *  sum([k["Published Citations"] for k in filter(lambda x:i==x["Group"],_l)]) / len(filter(lambda x:i==x["Group"],_l)))

        _Writer.writerow(_dc) 

        # h-Index
        _dc["Papers"]="h-Index (Published)"
        for i in _hs[1:]:
            _dc[i] = self.hIndex(filter(lambda x:i==x["Group"],_l),"Published Citations")

        _Writer.writerow(_dc) 

        # Most popular paper
        _dc["Papers"]="Most popular paper"
        """
        for i in _hs[1:]:
            print ("debug - i = {}".format(i))
            _dc[i] = sorted(
                        filter(
                            lambda x:i==x["Group"],_l
                              ),
                        key=lambda x:x["Published Citations"],
                        reverse=True
                    )#[0]["Published Citations"]
            print ("debug - _dc[i] = {}".format(_dc[i]))
        
        _Writer.writerow(_dc) 
        """

        _of.seek(-1,1)
        now = datetime.now()
        last_updated = now.strftime("%Y-%m-%d, %H:%M")
        _of.write('Last updated: %s\n\n' % last_updated)
        _of.close()

        
        return
     
    def processCSV(self):

        self.setReader()

        self.setWriter()

        self.doFormatting()

        now = datetime.now()
        last_updated = now.strftime("%Y-%m-%d, %H:%M")
        self._outFile.write('Last updated: ')
        self._outFile.write(last_updated)
        self._outFile.write('\n\n')

        self.doStuff()

        self.makeShortTable()

        return
    
