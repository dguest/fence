# -*- coding: utf-8 -*-
"""
#########################################################################################
# JournalDict.py
# Keys for abbreviating journal names
# Written by Marcello Barisonzi, Imagia Cybernetics inc., August 23rd, 2017
#########################################################################################
# $Rev: 241109 $
# $LastChangedDate: 2015-12-14 19:41:28 +0100 (Mon, 14 Dec 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import cPickle
import random
import string

# journal abbreviations
_abbrev = { 'Acta Phys. Polon.'         : 'APhyPol',
                'Acta Phys. Polon. Supp.'   : 'APhyPolSupp',
                'Adv. High Energy Phys.'    : 'AdvHEP',
                'AIP Conf. Proc.'           : 'AIPCP',
                'Adv. Ser. Direct. High Energy Phys.'           : 'ASDHEP',
                'Ann. Appl. Stat.'          : 'AAS',
                'Ann. Rev. Astron. Astrophys.' : 'AnnRevAstro2',
                'Ann. Rev. Nucl. Part. Sci.': 'ARNPS',
                'Annals Math. Statist.'     : "AnnMathStat",
                'Annalen Phys.'         :   'AdP',
                'Annals Phys.'              : 'AnnPhy',
                'Astron. Astrophys.'        : 'Astro2',
                'Astropart. Phys.'          : 'AstroPhys',
                'Astrophys. J. Suppl.'      : 'AstrophJS',
                'Astrophys. J.'             : 'AstrophJ',
                'Atom. Data Nucl. Data Tabl.' : 'ADNDT',
                'Biometrika'                  : 'Biometrika',
                'Bulg. J. Phys.'              : 'BJP',
                'Camb. Monogr. Part. Phys. Nucl. Phys. Cosmol.' : 'Cambridge',
                'Chin. J. Phys.'              : 'CJP',
                'Chin. Phys. Lett.'           : 'CPL',
                'Chin. Phys.'                 : 'ChPhy',
                'Commun. Math. Phys.'           : 'CMP',
                'Commun. Theor. Phys.'        : 'CTP',
                'Comptes Rendus Physique'     : 'CRP',
                'Comput. Phys. Commun.'       : 'CPC',
                'Comput. Stat. Data Anal.'    : 'CSDA',
                'Conf. Proc.'                 : 'ConfProc',
                'Czech. J. Phys.'             : 'CzJPh',
                'Dokl. Akad. Nauk Ser. Fiz.'  : 'DANSF',
                'EPJ Web Conf.'               : 'EPJWC',
                'Eur. Phys. J. Plus'          : 'EPJP',
                'Eur. Phys. J.'             : 'EPJ',
                'Europhys. Lett.'           : 'EPL',
                'Few Body Syst.'            : 'FBS',
                'Fiz. Elem. Chast. Atom. Yadra' : 'FEChAY',
                'Fortsch. Phys.'            : 'FPhy',
                'Found. Phys.'              : 'FoundPhy',  
                'Front. Phys.'              : 'FrontPhy',
                'Front. Phys. China'        : 'FrontPhyChina',
                'Helv. Phys. Acta'          : 'HPA',
                'Heavy Ion Phys.'           : 'HIONPHY',
                'IEEE Nucl. Sci. Symp. Conf. Rec.'              : 'IEEENSSCR',
                'IEEE Trans. Appl. Supercond.'          : 'IEEETAS',
                'IEEE Trans. Nucl. Sci.'    : 'IEEETNS',
                'Int. J. Mod. Phys.'        : 'IJMP',
                'Int. J. Mod. Phys. Conf. Ser.' : 'IJMPConf',
                'J. Comput. Syst. Sci.'     : 'JCSS',
                'J. Comput. Phys.'          : 'JCP',
                'J. Phys. Conf. Ser.'       : 'JPhyCS',
                'J. Phys.'                  : 'JPhy',
                'J. Phys. Soc. Jap.'        : 'JPSJ',
                'J. Statist. Phys.'         : 'JStatPhy',
                'JCAP'                      : 'JCAP',
                'J. Exp. Theor. Phys.'      : 'JETP',
                'JETP Lett.'                : 'JETPL',
                'JHEP'                      : 'JHEP',
                'JINST'                     : 'JINST',
                'Lect. Notes Phys.'         : 'LNP',
                'Measur. Sci. Tech.'        : 'MST',
                'Mod. Phys. Lett.'          : 'MPL',
                'Mon. Not. Roy. Astron. Soc.' : 'MNRAS',
                'Nature'                    : 'Nature',
                'New J. Phys.'              : 'NJP', 
                'Nucl. Instrum. Meth.'      : 'NIM',
                'Nucl. Phys. Proc. Suppl.'  : 'NucPhyProcS',
                'Nucl. Phys.'               : 'NucPhy',
                'Nuovo Cim.'                : 'NCim',
                'PMC Phys.'                 : 'PMCP',
                #'Phys.  Rev.  C 87,'        : 'PRC',
                'Phil. Mag.'                : 'PhilMag', 
                'Phys. Atom. Nucl.'         : 'PAN',
                'Phys. Dark Univ.'          : 'PhDarkU',
                'Phys. Lett.  B'            : 'PLB',
                'Phys. Lett.'               : 'PL',
                'Phys. Part. Nucl. Lett.'   : 'PPNL',
                'Phys. Part. Nucl.'         : 'PPN',
                'Phys. Rept.'               : 'PRep',
                'Phys. Rev. Lett.'          : 'PRL',
                'Phys. Rev. ST Accel. Beams'  : 'PRSTAB',
                'Phys. Rev.'                : 'PR',
                'Physical Review D'         : 'PRD',
                'Pisma Zh. Eksp. Teor. Fiz.': 'JETPL',
                'PoS'                       : 'PoS',
                'Pramana'                   : 'Pramana', 
                'Proc. Ire.'                : 'ProcIre',
                'Proc. Phys. Soc.'          : 'PPS',
                'Proc. Roy. Soc. Lond.'     : 'PRSL',
                'Prog. Part. Nucl. Phys.'   : 'PPNP',
                'Prog. Theor. Phys. Suppl.' : 'PTPS',
                'Prog. Theor. Phys.'        : 'PTP',
                'Rep. Prog. Phys.'          : 'RPP',
                'Rept. Prog. Phys.'         : 'RPP',
                'Rev. Mod. Phys.'           : 'RMP',
                'Rev. Sci. Instrum.'        : 'RevScIm',
                'Riv. Nuovo Cim.'           : 'RivNuCim',
                'Science'                   : 'Science',
                'Sitzungsber. Preuss. Akad. Wiss. Berlin (Math. Phys. )'                : 'SitzPrAWB',
                'Solid State Electron.'     : 'SSE',
                'Sov. J. Nucl. Phys.'       : 'SJNP',
                'Sov. Phys. JETP'           : 'JETP',
                'Sov. Phys. Usp.'           : 'SPU',
                'Statist. Sinica'           : 'StatSin',
                'Theor. Math. Phys.'        : 'TMP',
                'Universal J. Phys. Appl.'  : 'UJPA',
                'Vacuum'                    : 'Vacuum',
                'Yad. Fiz.'                 : 'YadFiz',
                'Z. Phys.'                  : 'ZP',
                'Zh. Eksp. Teor. Fiz.'      : 'JETP',
                'eConf'                     : 'eConf',
}


def store_abbreviations(dct):
    try:
        f = open('j_abbrev.pkl','wb')
        cPickle.dump(dct, f)
        f.close()
    except:
        print "Could not save abbreviations"


def get_abbreviations():
    try:
        f = open('j_abbrev.pkl','rb')
        abbrev = cPickle.load(f)
        f.close()
    except:
        print "Using default abbreviations"
        abbrev = _abbrev
        store_abbreviations(abbrev)

    return abbrev

def make_abbreviation(strg, dct):
    up = filter(lambda x:x.isalpha() and x.isupper(), strg)
    lo = filter(lambda x:x.isdigit() or (x.isalpha() and x.islower()), strg)

    abb = up
    i = 0

    # try to build a unique key adding lowercase and numbers 
    while abb in dct.values() and i<len(lo):
        abb = up+lo[i]
        i += 1

    # if still not, unique, this will do (but limit to 1000 iterations just to be safe)
    i = 0
    while abb in dct.values() and i<1000:
        abb = up + random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits)
        i += 1

    # if not unique at this point, I don't care
    print "New abbreviation:", strg, abb

    dct[strg] = abb

    return dct
