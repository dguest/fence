#!/usr/bin/env python2
"""
entry point to collect and output citations of ATLAS and CMS papers
author: Maurizio Colautti, maurizio.colautti@cern.ch
based on a script wrote by Marcello Barisonzi, mantained by Ian Brock
"""
from __future__ import print_function

import glob
import shutil
import os
import argparse
from datetime import datetime

# Scripts
PYTHON_DIR = "/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/CitationsAndBibliography/bibscripts/"
# Directory with CSV files containing publications
TWIKI_DIR = "/afs/cern.ch/user/a/atlaspo/twikirun/"
# Intermediate files
DATA_DIR = "/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/CitationsAndBibliography/files/"
# Plots
PLT_DIR = "/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/CitationsAndBibliography/plots/"
# Final destination that plots etc. are copied to
OUT_DIR = "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/Citations/"

if "pythonDir" in os.environ:
    PYTHON_DIR = os.environ.get("pythonDir")
if "twikiDir" in os.environ:
    TWIKI_DIR = os.environ.get("twikiDir")
if "dataDir" in os.environ:
    DATA_DIR = os.environ.get("dataDir")
if "pltDir" in os.environ:
    PLT_DIR = os.environ.get("pltDir")
if "outDir" in os.environ:
    OUT_DIR = os.environ.get("outDir")
print("Python directory:", PYTHON_DIR)
print("TWiki directory", TWIKI_DIR)
print("Data directory", DATA_DIR)
print("Plots directory", PLT_DIR)
print("Output directory", OUT_DIR)

os.chdir(PYTHON_DIR)

DAY = datetime.today().day

#------------------------------------------------------------------------------
PARSER = argparse.ArgumentParser()
PARSER.add_argument('--debug', default=0, type=int, help='Debug level')
PARSER.add_argument('--copy', dest='copy', action='store_true', help='Copy plots to TWiki')
PARSER.add_argument('--no-copy', dest='copy', action='store_false', help='Dont copy plots to TWiki')
PARSER.add_argument('--mine', dest='mine', action='store_true', help='Run Inspire miner')
PARSER.add_argument('--no-mine', dest='mine', action='store_false', help='Do not run Inspire miner')
PARSER.add_argument('--ATLAS', dest='ATLAS', action='store_true', help='Run for ATLAS')
PARSER.add_argument('--no-ATLAS', dest='ATLAS', action='store_false', help='Do not run for ATLAS')
PARSER.add_argument('--CMS', dest='CMS', action='store_true', help='Run for CMS')
PARSER.add_argument('--no-CMS', dest='CMS', action='store_false', help='Do not run for CMS')
PARSER.set_defaults(copy=True, mine=True)
PARSER.set_defaults(ATLAS=True, CMS=True)
ARGS = PARSER.parse_args()
print("Copy:", ARGS.copy)
print("Mine:", ARGS.mine)
print("ATLAS:", ARGS.ATLAS)
print("CMS:", ARGS.CMS)
print("Debug level:", ARGS.debug)

if ARGS.ATLAS:
    print("Running ATLASInspireMiner")
    from ATLASInspireMiner import ATLASInspireMiner
    BUILDER = ATLASInspireMiner(debug=ARGS.debug)
    BUILDER.setInfile(os.path.join(TWIKI_DIR, "csv5457/table5457.csv"))
    BUILDER.setOutfile(os.path.join(DATA_DIR, "atlas_citationsDict.pickle"))
    BUILDER.mineInspire()

if ARGS.mine or DAY == 1:
    if ARGS.CMS:
        print("Running CMSInspireMiner")
        from CMSInspireMiner import CMSInspireMiner
        BUILDER = CMSInspireMiner(debug=ARGS.debug)
        BUILDER.setIndir(DATA_DIR)
        BUILDER.setOutfile(os.path.join(DATA_DIR, "cms_citationsDict.pickle"))
        BUILDER.mineInspire()

if ARGS.ATLAS:
    print("Crunching ATLAS citations")
    from CitationsCruncher import CitationsCruncher
    BUILDER = CitationsCruncher()
    BUILDER.setInfile(os.path.join(TWIKI_DIR, "csv5457/table5457.csv"))
    BUILDER.setPkfile(os.path.join(DATA_DIR, "atlas_citationsDict.pickle"))
    BUILDER.setOutfile(os.path.join(PLT_DIR, "atlas_citations.txt"))
    BUILDER.processCSV()

if ARGS.CMS:
    print("Crunching CMS citations")
    from CMSCitationsCruncher import CMSCitationsCruncher
    BUILDER = CMSCitationsCruncher()
    BUILDER.setInfile(os.path.join(DATA_DIR, "cms_citationsDict.pickle"))
    BUILDER.setPkfile(os.path.join(DATA_DIR, "cms_citationsDict.pickle"))
    BUILDER.setOutfile(os.path.join(PLT_DIR, "cms_citations.txt"))
    BUILDER.processCSV()

if ARGS.copy:
    for i in glob.glob(os.path.join(PLT_DIR, "*citations*.png")) \
             + glob.glob(os.path.join(PLT_DIR, "neato*.[p,t]*")):
        print("Copying", i, "to", OUT_DIR)
        shutil.copy(i, OUT_DIR)
    for i in glob.glob(os.path.join(PLT_DIR, "atlas*.txt")) \
             + glob.glob(os.path.join(PLT_DIR, "cms*.txt")):
        print("Copying", i, "to", OUT_DIR)
        shutil.copy(i, OUT_DIR)
