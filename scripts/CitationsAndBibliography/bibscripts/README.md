# Scripts that run every month to update standard ATLAS bib files

This file contains a short description of what each of the scripts does.

The `cron` job run `makeAllBibs.py` once a month.

## Directories

The following subdirectories are needed (for default settings)
* `files` for json files from `getCitationJSON.sh` and for pickle files from Inspire mining
* `plots` for output of `citcruncher.py`

## Sequence

* First `getCitationJSON.sh` has to be run.
This creates `ATLAScit_*.json` and `CMScit_*.json`.
* Then `citationcruncher.py`
    - For CMS this uses `files/CMScit_*.json`.
    - For ATLAS the file `csv5457/table5457.csv` is used.
* Finally run `makeAllBibs2.py`.

## Python setup

I use `python2` from Homebrew. I already have `matplotlib`, `numpy`, `scipy` etc. installed.

It is probably worth investigating how to set up things so that Python from CVMFS is used.

Apart from the package(s) mentioned below,
all other packages tha are used should be available in a standard Python installation.

When using `https`, one also needs top verify certificates. I took the instructions from
https://urllib3.readthedocs.io/en/latest/user-guide.html#ssl

### BibUnicodeTex

This package converts some Unicode characters to TeX.
It is a modified version of unicode_tex and does not convert braces and backslash etc.

Further conversion seems to be done at various places in the scripts.

## Python virtualenv on lxplus

Starting from the hints in https://gitlab.cern.ch/snippets/183,
I used the following sequence of commands to set up Python 2.7 on lxplus:
```
setupATLAS
lsetup python
virtualenv -p `which python` venv
source venv/bin/activate
pip install --upgrade pip
pip install --upgrade setuptools
pip install certifi
pip install urllib3[secure]
pip install matplotlib
pip install Pillow
```

To us the environment I need:
```
setupATLAS
lsetup python
source venv/bin/activate
cd ~atlaspo/gittools/CitationsAndBibliography/bibscripts
```

If you run things interactively you can get out of the virtualenv with the command `deactivate`.
## `getCitationJSON.sh`

This runs a `wget` to get all ATLAS and CMS publications from Inspire.
Publications have to be from one of the collaborations and have more than 1000 authors.
They are fetched in blocks of 250.
Hence, if the number of ATLAS/CMS publications goes over 1000 the script needs to be updated.

## `citcruncher.py`

This has to be up to date in order for `makeAllBibs.py` to work.
In order to force it to mine Inspire run it with the `--mine` option.
Turn off copying files to TWiki with the `--no-copy` option.
It assumes that you have access to `csv5457/table5457.csv`.

Instead of `import Image` I changed to `from PIL import Image`.

The Inspire mining runs on the first day of each month.
It creates `citationsDict.pickle` for ATLAS and `cms_citationsDict.pickle` for CMS.

The cruncher makes plots and also creates `citations.txt` and `cms_citations.txt`
that contain citation numbers for each paper.
These files are put in the same directory as the plots.
By default this is `/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/Citations`.

* Running ATLASInspireMiner seems to take about 8000 seconds.
* Running CMSInspireMiner seems to take about 10000 seconds.
* ATLASInspireMiner uses `csv5457/table5457.csv` as the source of references.
* CMSInspireMiner uses `CMScit_*.json` as the source of references.

It would be good to have both "cold" and "warm" starts for `citcruncher.py` if it fails.
`citcruncher.py` should inform people if it fails, especially the Inspire mining.

## `makeAllBibs2.py`
Main file to run.

Need to set `pythonDir`, `twikiDir` and `publicDir`. Can also turn on or off sending of email.

I keep `twikiDir` at the default setting, as I have AFS on my laptop.
Without AFS, you need to copy the files over.

The directories needed are `csv5457`, `csv5505` and `csv5967`.

The definitions are in `brockLocalSetup.sh`, which I `source`.
If you want different definitions create your own file and `source` it.

### ATLAS bib file
* First step is to use csv file to get all citations from Glance.
* This creates `ATLAScitations.json`. It takes a few hours to run.
* Next this is used as input and creates or updates `CompleteBibliography.json`. If something went wrong with this step, delete the file and run `makeAllBibs.py`.
Creation takes over an hour.

### CMS bib file
* Needs `cms_citationsDict.pickle` which is created by `citcruncher.py` when it calls `CMSInspreMiner.py`

### CONF and PUB notes files
* The CDS links are take from the relevant webpages. The relevant function is `getCDSurl` in `PubNotesBibMaker.py`.

