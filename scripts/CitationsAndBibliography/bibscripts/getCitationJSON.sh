#!/bin/sh

# Select papers from ATLAS or CMS collaborations with at least 1000 authors
# According to Marcello, "tc p" for papers, somehow not working anymore

if [ -z "${dataDir}" ]; then
    outDir=/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/CitationsAndBibliography/files
else
    outDir=${dataDir}
fi
wget "old.inspirehep.net/search?p=cn+atlas+and+ac+1000%2B&rg=250&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ${outDir}/ATLAScit_1.json
wget "old.inspirehep.net/search?p=cn+cms+and+ac+1000%2B&rg=250&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ${outDir}/CMScit_1.json
wget "old.inspirehep.net/search?p=cn+atlas+and+ac+1000%2B&rg=250&jrec=251&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ${outDir}/ATLAScit_2.json
wget "old.inspirehep.net/search?p=cn+cms+and+ac+1000%2B&rg=250&jrec=251&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ${outDir}/CMScit_2.json
wget "old.inspirehep.net/search?p=cn+atlas+and+ac+1000%2B&rg=250&jrec=501&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ${outDir}/ATLAScit_3.json
wget "old.inspirehep.net/search?p=cn+cms+and+ac+1000%2B&rg=250&jrec=501&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ${outDir}/CMScit_3.json
wget "old.inspirehep.net/search?p=cn+atlas+and+ac+1000%2B&rg=250&jrec=751&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ${outDir}/ATLAScit_4.json
wget "old.inspirehep.net/search?p=cn+cms+and+ac+1000%2B&rg=250&jrec=751&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ${outDir}/CMScit_4.json
wget "old.inspirehep.net/search?p=cn+atlas+and+ac+1000%2B&rg=250&jrec=1001&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ${outDir}/ATLAScit_5.json
wget "old.inspirehep.net/search?p=cn+cms+and+ac+1000%2B&rg=250&jrec=1001&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ${outDir}/CMScit_5.json

# Todo:
# wget, saving CMScit_2.json includes a wrong entry that makes the parsing of the json file go broke
# {"comment": ["There is an error on cover due to a technical problem for some items", "Related figures: <a href=\"http://cdsweb.cern.ch/record/1326290\">CMS-PHO-GEN-2006-001</a>"], "doi": null, "title": {"subtitle": "Technical Design Report Volume 1: Detector Performance and Software", "title": "CMS Physics"}, "accelerator_experiment": [{"experiment": "CERN-LHC-CMS"}, {"accelerator": "CERN SPS"}, {"accelerator": "CERN SPS"}], "collection": [{"primary": "Citeable"}, {"primary": "CORE"}, {"primary": "HEP"}, {"primary": "Report"}], "primary_report_number": ["CERN-LHCC-2006-001", "CMS-TDR-8-1"], "publication_info": null, "recid": 1614070}
# This entry is now avoided through CMSInspireMiner version in use


# Previous version
#wget "inspirehep.net/search?p=cn+atlas+and+tc+p+and+a+Aad&rg=250&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ATLAScit_1.json
#wget "inspirehep.net/search?p=cn+atlas+and+tc+p+and+a+Aad&rg=250&jrec=251&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O ATLAScit_2.json
#wget "inspirehep.net/search?p=cn+cms+and+tc+p+and+a+Khachatryan+or+a+Chatrchyan&rg=250&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O CMScit_1.json
#wget "inspirehep.net/search?p=cn+cms+and+tc+p+and+a+Khachatryan+or+a+Chatrchyan&rg=250&jrec=251&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O CMScit_2.json

# Even older version
#wget "inspirehep.net/search?p=cn+atlas+and+tc+p+and+ac+1000%2B&rg=250&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O /afs/cern.ch/u    ser/a/atlaspo/svntools/CitationsAndBibliography/ATLAScit_1.json
#wget "inspirehep.net/search?p=cn+atlas+and+tc+p+and+ac+1000%2B&rg=250&jrec=251&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O /afs/    cern.ch/user/a/atlaspo/svntools/CitationsAndBibliography/ATLAScit_2.json
#wget "inspirehep.net/search?p=cn+cms+and+tc+p+and+ac+1000%2B&rg=250&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O /afs/cern.ch/user/a/atlaspo/svntools/CitationsAndBibliography/CMScit_1.json
#wget "inspirehep.net/search?p=cn+cms+and+tc+p+and+ac+1000%2B&rg=250&jrec=251&of=recjson&ot=recid,title,accelerator_experiment,collection,comment,doi,publication_info,primary_report_number" -O /afs/cern.ch/user/a/atlaspo/svntools/CitationsAndBibliography/CMScit_2.json
