# -*- coding: utf-8 -*-

import gitlab

import sys
sys.path.append("/afs/cern.ch/user/a/atlaspo/po-scripts/venv/project_atlaspo/lib/python3.6/site-packages")

from xml.dom.minidom import parse, parseString
from xml.dom.minidom import Node
from authorlists import Author
from authorlists import Institute

from pylatexenc.latex2text import LatexNodes2Text

class XmlObjReader():

    def __init__(self):
        # todo: verify this file exists
        self._source = ""
        self._path = ""
        self._dom = ""
        self.refCode = ""
        self.refDate = ""
        self.creationDate = ""
        self.last_update_date = ""

    def _retrieve_gitlab_authorlist(self):
        """
        retrieves the author list from gitlab
        todo: the private_token is Maurizio Colautti's one
              to be substituted with atlaspo's one (or a service account one)
              but now ATLASPO service account doesn't have access to gitlab repos
        """

        print("authorlist is retrieved from gitlab at path = \n{}".format(self._path))

        gl_connection = gitlab.Gitlab(
            "https://gitlab.cern.ch/",
            private_token="kDSzxbWj8UuXVy84LTa2"
            )
        # todo: check for errors
        project = gl_connection.projects.get(self._path)
        auth_file = project.files.get(file_path='atlas_authlist.xml', ref='master')
        self._dom = parseString(auth_file.decode())

    def set_gitlab_source(self, path):
        """
        sets the source of the authorlist to gitlab, to the specified path
        """
        self._source = 'gitlab'
        self._path = path

    def set_afs_source(self, path):
        """
        sets the source of the authorlist to afs, to the specified path
        """
        self._source = 'afs'
        self._path = path

    def parse_file(self):
        """
        loads the file from either gitlab or afs
        """
        if not self._dom:
            if self._source == 'gitlab':
                self._retrieve_gitlab_authorlist()
            elif self._source == 'afs':
                self._dom = parse(self._path)

    def populate_metadata(self):
        """ 
        extracts metadata from xml file
        """

        # todo: implements exceptions better
        #       not all the fields are necessarily found in the xml, depending on A.L. version

        # to be sure the file is loaded before parsing
        self.parse_file()

        try:
            self.refCode = self._dom.lastChild.getElementsByTagName("cal:publicationReference")[0].lastChild.data
            print("Ref Code = {}".format(self.refCode))
        except:
            pass
        try:
            self.refDate = self._dom.lastChild.getElementsByTagName("cal:authorlistReferenceDate")[0].lastChild.data
            print("refDate = {}".format(self.refDate))
        except:
            pass
        try:
            self.creationDate = self._dom.lastChild.getElementsByTagName("cal:creationDate")[0].lastChild.data
            print("Creation Date = {}".format(self.creationDate))
        except:
            pass

        try:
            self.last_update_date = self._dom.lastChild.getElementsByTagName("cal:authorlistUpdateDate")[0].lastChild.data
            print("Last Update Date = {}".format(self.last_update_date))
        except:
            pass


    def populateAuthors(self):
        """
        extracts authors from xml file
        """

        # to be sure the file is loaded before parsing
        self.parse_file()

        collauthlist = self._dom.lastChild.getElementsByTagName("cal:authors")[0]
        authors = collauthlist.getElementsByTagName("foaf:Person")

        authorsList = list()

        for author in authors:
            authorObj = Author()

            for element in author.getElementsByTagName("foaf:name"):
                if element.lastChild is not None:
                    if element.lastChild.nodeType == Node.TEXT_NODE:
                        authorObj.foafName = element.lastChild.data

            for element in author.getElementsByTagName("cal:authorNamePaper"):
                if element.lastChild is not None:
                    if element.lastChild.nodeType == Node.TEXT_NODE:
                        authorObj.namePaper = element.lastChild.data
                        authorObj.utf8Name = LatexNodes2Text().latex_to_text(element.lastChild.data)

            for element in author.getElementsByTagName("foaf:givenName"):
                if element.lastChild is not None:
                    if element.lastChild.nodeType == Node.TEXT_NODE:
                        authorObj.firstName = element.lastChild.data

            for element in author.getElementsByTagName("foaf:familyName"):
                if element.lastChild is not None:
                    if element.lastChild.nodeType == Node.TEXT_NODE:
                        authorObj.lastName = element.lastChild.data

            for element in author.getElementsByTagName("cal:authorStatus"):
                if element.lastChild is not None:
                    if element.lastChild.nodeType == Node.TEXT_NODE:
                        if element.lastChild.data == "DECEASED":
                            authorObj.deceased = True

            for element in author.getElementsByTagName("cal:authorAffiliations"):
                for affiliation in element.getElementsByTagName("cal:authorAffiliation"):
                    organization = affiliation.getAttribute("organizationid")
                    if len(organization) > 1 and organization[0] == "o":
                        organization = organization[1:]
                    # todo: evaluate if it makes sense to split organitazions vs also ats
                    """
                    if affiliation.getAttribute("connection") == "Also At":
                        authorObj.alsoAt.append(organization)
                    else:
                        authorObj.organization.append(organization)
                    """
                    authorObj.organization.append(organization)

            for element in author.getElementsByTagName("cal:authorids"):
                for authorid in element.getElementsByTagName("cal:authorid"):
                    if authorid.lastChild is not None:
                        if authorid.lastChild.nodeType is not None:
                            if authorid.lastChild.nodeType == Node.TEXT_NODE:
                                if authorid.getAttribute("source") == "INSPIRE":
                                    authorObj.inspire = authorid.lastChild.data
                                elif authorid.getAttribute("source") == "ORCID":
                                    authorObj.orcid = authorid.lastChild.data

            authorsList.append(authorObj)

        return authorsList

    def populateInstitutes(self):
        """
        extracts institutes from xml file
        """

        self.parse_file()

        collinstlist = self._dom.lastChild.getElementsByTagName("cal:organizations")[0]
        institutes = collinstlist.getElementsByTagName("foaf:Organization")

        institutesList = list()

        for institute in institutes:
            instituteObj = Institute()

            instituteId = institute.getAttribute("id")
            if len(instituteId) > 1 and instituteId[0] == "o":
                instituteId = instituteId[1:]

            instituteObj.id = instituteId
            if instituteId.isalpha():
                instituteObj.alsoAt = True

            for element in institute.getElementsByTagName("foaf:name"):
                if element.lastChild is not None:
                    if element.lastChild.nodeType == Node.TEXT_NODE:
                        instituteObj.name = element.lastChild.data
                        instituteObj.utf8Name = LatexNodes2Text().latex_to_text(element.lastChild.data)

            for element in institute.getElementsByTagName("cal:orgName"):
                if element.lastChild is not None:
                    if element.lastChild.nodeType == Node.TEXT_NODE:
                        sourceKind = element.getAttribute("source")
                        if sourceKind == "spiresICN":
                            instituteObj.orgSpiresICN = element.lastChild.data
                        elif sourceKind == "InstId":
                            instituteObj.orgInstId = element.lastChild.data
                        elif sourceKind == "shortName":
                            instituteObj.orgShortName = element.lastChild.data

            for element in institute.getElementsByTagName("cal:orgStatus"):
                if element.lastChild is not None:
                    if element.lastChild.nodeType == Node.TEXT_NODE:
                        instituteObj.status = element.lastChild.data


            institutesList.append(instituteObj)


        return institutesList
