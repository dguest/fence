#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
#####################################
# KnowledgeMaker.py
#
# Gather the fact bases
# for the pyKE engine
#
# Author: Marcello Barisonzi
#
# $Rev: 239255 $                                                                       
# $LastChangedDate: 2015-06-09 00:45:56 +0200 (Tue, 09 Jun 2015) $
# $LastChangedBy: atlaspo $
#
####################################
"""

import sys
from optparse    import OptionParser

from XmlReader   import XmlReader
from XpdfPaperReader     import XpdfPaperReader
from APSPaperReader      import APSPaperReader
from ElsevierPaperReader import ElsevierPaperReader
from EPJCPaperReader     import EPJCPaperReader
from JHEPPaperReader     import JHEPPaperReader
from NJPPaperReader      import NJPPaperReader
from formatProlog        import formatProlog
#from fundingReader       import fundingReader 
from fundingReaderGlance import fundingReaderGlance

#reload(sys)
sys.setdefaultencoding("utf-8")


def main():
    #parser for command-line variable
    parser = OptionParser(usage="usage: %prog [options]")
    parser.add_option("-x", dest="xml_source", help="Source XML document")
    #parser.add_option("-m", dest="xml_monthly", help="Monthly XML authorlist")
    parser.add_option("-p", dest="pdf_source", help="Source PDF paper")
    parser.add_option("-o", dest="output_dir", default=".", help="Output directory")
    parser.add_option("-f", dest="first_page", default="1", help="First page to convert")
    parser.add_option("-l", dest="last_page",  default="-1", help="Last page to convert") 

    (options, args) = parser.parse_args()

    xml = XmlReader(options.xml_source)

    #k_f = open("%s/reference.kfb" % options.output_dir, "w")
    #p_f = open("%s/reference.pl" % options.output_dir, "w")

    #print "getGeneralFacts"
    genFacts = xml.getGeneralFacts()
    #k_f.write(genFacts.encode("utf-8"))
    #print "getInstitutesFacts"
    instFacts = xml.getInstitutesFacts()
    #k_f.write(instFacts.encode("utf-8"))
    #print "getAuthorsFacts"
    authFacts = xml.getAuthorsFacts()
    #k_f.write(authFacts.encode("utf-8"))
    #print "fundingReader"
    #fundFacts = fundingReaderGlance()
    ##fundFacts = fundingReader()
    #k_f.write(fundFacts.encode("utf-8"))
    #print "formatProlog"

    #formatProlog(p_f,genFacts,instFacts,authFacts,fundFacts,ref=True)

    #k_f.close()
    #p_f.close()

    # use either monthly or pdf for target
    
    k_f = open("%s/target_pdf.kfb" % options.output_dir,"w")
    print ("%s/target_pdf.kfb" % options.output_dir)
    p_f = open("%s/target_pdf.pl"  % options.output_dir,"w")

    doFunding = False

    if options.pdf_source:
        
        publisher = XpdfPaperReader.getPublisher(options.pdf_source)
        if publisher == "Unknown":
            print("[WARN] the publisher couldn't be identified")
        else:
            print("Publication publisher: %s" % publisher)

        override_first_page = XpdfPaperReader.getFirstPage(options.pdf_source)
        if options.first_page == "1":
            options.first_page = override_first_page
            print ("analysis starts at page %s " % options.first_page)

        k_f.write("publisher('%s')\n" % publisher)
    
        if publisher == "Elsevier":
            pdf = ElsevierPaperReader(options.pdf_source, options.first_page, options.last_page)
        elif publisher == "APS":
            pdf = APSPaperReader(options.pdf_source, options.first_page, options.last_page)
            doFunding = True
        elif publisher == "EPJC":
            pdf = EPJCPaperReader(options.pdf_source, options.first_page, options.last_page)
        elif publisher == "JHEP":
            pdf = JHEPPaperReader(options.pdf_source, options.first_page, options.last_page)
        elif publisher == "NJP":
            pdf = NJPPaperReader(options.pdf_source, options.first_page, options.last_page)
        else:
            pdf = XpdfPaperReader(options.pdf_source, options.first_page, options.last_page)

        pdf.setFirstAuthor(xml.getFirstAuthor())
        pdf.setFirstInstitute(xml.getFirstInstitute())
        instFacts = pdf.getInstitutesFacts()
        k_f.write(instFacts.encode("utf-8"))
        authFacts = pdf.getAuthorsFacts()
        k_f.write(authFacts.encode("utf-8"))
        fundFacts=None
        if doFunding:
            fundFacts = pdf.getFundingFacts()
        
        formatProlog(p_f,'publisher("%s")\n' % publisher,instFacts,authFacts,fundFacts,ref=False)

    #elif options.xml_monthly:

    #    xml2 = XmlReader(options.xml_monthly)

    #    genFacts = xml2.getGeneralFacts()
    #    k_f.write(genFacts.encode("utf-8"))
    #    instFacts = xml2.getInstitutesFacts()
    #    k_f.write(instFacts.encode("utf-8"))
    #    authFacts = xml2.getAuthorsFacts()
    #    k_f.write(authFacts.encode("utf-8"))

    else:
        print "ERROR: Define either PDF input or Monthly list!"
        parser.print_help()
        sys.exit(666)

    k_f.close()
    p_f.close()



if __name__ == "__main__":
    main()
