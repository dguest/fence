from fundingReader  import fundingReader
from fundingReaderGlance import fundingReaderGlance

# main code
if __name__=='__main__':
    print "----------- Compare these values -----------"
    f = fundingReader()
    print f

    print "----------- With these values -----------"
    f = fundingReaderGlance()
    print f