# -*- coding: utf-8 -*-
"""
#####################################
# APSPaperReader.py
#
# Uses xpdf to extract text from PDF
# and convert into knowledge files
# Derived class for APS papers
#
# Author: Marcello Barisonzi
#
# $Rev: 239255 $
# $LastChangedDate: 2015-06-09 00:45:56 +0200 (Tue, 09 Jun 2015) $
# $LastChangedBy: atlaspo $
#
####################################
"""

import re

import sys
reload(sys)
sys.setdefaultencoding("utf-8")

from XpdfPaperReader     import XpdfPaperReader

class APSPaperReader(XpdfPaperReader):
    
    def __init__(self, filename, fp="1", lp="-1"):
        super(self.__class__, self).__init__(filename, fp, lp)

        ### pattern should be overwritten by subclasses
        self._authorPtn = re.compile(ur"((?:[A-Z]+[a-z]{0,1}[\.|\-]+)+[^0-9]+?),*?([0-9]+(?:[0-9]*[a-z]*\**†?,*)*)", re.UNICODE)

        self._instituteExc = [ re.compile("([a-z]{1,2}\s*\n*?Also(?:.*?(?:%s)){2}\.*)" % "|".join(self._excep_countries)),
                               re.compile("[^N,P]([0-9]{1,3}[a-z]{0,1}\n*?(?:[^0-9]*?\n*?[^0-9]*?(?:%s)){2})" % "|".join(self._excep_countries)) ]

        self._institutePtn = [ re.compile("([a-z]{1,2}\s*\n*?Also.*?(?:%s)\.*)" % "|".join(self._countries)),
                               re.compile("[^N,P]([0-9]{1,3}[a-z]{0,1}\n*?.*?(?:%s)\.*)" % "|".join(self._countries)),
                               re.compile("([a-z]{1,2}\s*\n*?Deceased\.*)")]
        
        self._institutePtn += [ re.compile("([a-z]{1,2}\s*\n*?Also.*?(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,7) ]
      
        self._institutePtn += [ re.compile("[^N,P]([0-9]{1,3}[a-z]{0,1}\n*.*(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,7) ]
        

        self._ptn_id    = re.compile("([0-9]{1,3}[a-z]{0,1})\s*:*\s*\n*")
        self._ptn_also  = re.compile("([a-z]{1,2})\s*\n*Also at(.*)\.*")
        self._ptn_dec_char = re.compile(ur"([a-z†]{1,2})\s*\n*Deceased\.*", re.UNICODE)
        self._ptn_dec   = re.compile(ur"[a-z†]{1,2}\s*\n*Deceased\.*", re.UNICODE)
        self._ptn_subid = re.compile("(?!.).") # match none, not used for APS
        self._ptn_state = re.compile("(%s)" % "|".join(self._countries))
        
        return


    def getFundingFacts(self):
        _txt = self.cleanUnicode(self._input)
        
        # add bullet point to replacements
        self._replaces[u"\u2022"] = ""
        
        for k,v in self._replaces.iteritems():
            _txt = _txt.replace(k,v)
        
        fund_ptn = re.compile("(.+),\s+FundRef ID\s+\n*(http://dx.doi.org/10.13039/[0-9]+)")
        
        ff = fund_ptn.findall(_txt)
        funds = []
        for f1,f2 in ff:
            funds.append('funding("%s","%s")\n' % (f1.strip(),f2.strip()) )
        
        return "".join(funds)