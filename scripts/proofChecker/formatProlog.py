

def formatProlog(outf,genFacts,instFacts,authFacts,fundFacts=None,ref=False):
    # write Prolog preamble
    preamble = """:- discontiguous(alsoat/2).
:- discontiguous(belongsto/2).
:- discontiguous(isdeceased/1).
:- discontiguous(isinstitute/2).
:- discontiguous(isinstitute_notmember/2).

""" 

    if ref:
        preamble = preamble.replace("(","(ref_")

    outf.write(preamble)

    def cleanup(strg, ref):
        _strg = strg.replace("\n",".\n").replace("\'","\\'").replace('"',"'").encode("utf-8")
        _strg = _strg.replace("\\'uth", "\'uth")
        if ref:
            _strg = _strg.replace("date",        "ref_date")
            _strg = _strg.replace("belongsto",   "ref_belongsto")
            _strg = _strg.replace("isdeceased",  "ref_isdeceased")
            _strg = _strg.replace("alsoat",      "ref_alsoat")
            _strg = _strg.replace("isinstitute", "ref_isinstitute")
        #_splt = sorted(_strg.split("\n"))
        #return "\n".join(_splt)
        return _strg
        
    outf.write(cleanup(genFacts, ref))

    outf.write(cleanup(instFacts, ref))
    outf.write(cleanup(authFacts, ref))

    if fundFacts:
        outf.write(cleanup(fundFacts, ref))
    
    return
