#!/bin/env python2
# -*- coding: utf-8 -*-
"""
# proofs_html.py - Create html file from proofs directory
# Marcello Barisonzi, 01/10/2012
"""

import os
import sys
import time
import glob
import re
from datetime import datetime
from datetime import date
from Comparison import Comparison
from XmlObjReader import XmlObjReader
from PdfObjReader import PdfObjReader

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

# TODO: this class should useless, TBD ?
class InvalidVersionError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class proofEntry:
    def __init__(self):
        self.proofs = []
        self.noredline = []
        self.xmls = []
        self.reports = []
        self.html_reports = []
        self.refcode = None
        self.short_title = None
        self.id = None
        self.tstamp = -1
        self.newproc = False

class entryTool:
    """Tool for processing entriess"""
    def __init__(self):
        """Initialisation from command line parameters"""
        # defaults for parameters
        self.changestate = False
        self.arcloc = "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/proofs"
        self.eosloc = "/eos/home-a/atlaspo/www/proofs"
        self.homedir = os.path.dirname(sys.argv[0])
        self.trap = False
        self.errors = []
        # other variables
        self.dirs = []
        self.proofs = []
        self.reports = []
        self.html_reports = []

        self.published_papers = []
        self.ref_codes = {}
        self.short_titles = {}

        self.mailtemplate = """Dear colleagues,

please download the %(ord)s proof of your paper at:
https://atlas.web.cern.ch/Atlas/GROUPS/PHYSOFFICE/proofs/

ID=%(doc)s, %(ref)s
The file to download is under the column "proofs":  %(file)s 

Please proofread the article very carefully.

*** Please note that journals typically give TWO days to respond.

 *** Please read carefully the guidelines on how to handle the proofs ***
(they might have changed since last time you read them) 

The guidelines can be found at:
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ProofsChecking

 * Comments and answer should be agreed between the editors and the editorial board. 
 * Please answer question by question (please include in your file the text of the questions)
 * If the proof doesn't contain a cover page with questions please check (and please do that anyway) for 
    -  layout and typesetting mistakes, 
    -  spelling mistakes in words or formulas, 
    -  wrong cross-references between formulas and text, 
    -  mistakes or updating in references, 
    -  language copy editing (not provided by the typesetter).
 * If you have additional comments/corrections please list them under "General comments and corrections"

 * Questions concerned the author list will be addressed by the physics office.
 * Please send your final response (not the intermediate email discussion between authors and edboard)  
   to atlas-phys-office-pub@cern.ch

     Best regards,

                       ATLAS Physics Office on behalf of the PubCom chairs"""

    def execute_all(self):
        """
        the script updates the page with all the recent published papers
        once a day, when the page modified date is different than 'today'
        """
        webfile = '%s/index_all.html' % self.arcloc

        modified = os.stat(webfile).st_mtime
        timestamp = date.fromtimestamp(modified)
        today = date.today()

        if today != timestamp:

            self.proofs = []

            for directory in self.get_dirs():
                if not self.read_dir(directory, filters="all"):
                    return False

            self.make_web("afs", filters="all")
            self.make_web(filters="all")


    def execute(self):
        """Main execution code"""

        self.populate_short_titles()
        self.populate_ref_codes()
        self.populate_published_papers()

        for d in self.get_dirs():
            if not self.read_dir(d):
                return False

        self.make_web("afs") # write both on afs (through parameter)
        self.make_web() # and eos (default behaviour)

        self.sendMail() # if you are testing, you may want to comment this line;
                        # DOING SO please put 0,1 spaces between # and self.sendMail()
                        # a script is checking to see if we forget to remove the comment tag

        self.run_checker()
        self.make_symlinks()
        return True

    def get_dirs(self):
        """Get only numbered directories"""
        a = glob.glob("%s/doc[0-9]*" % self.arcloc)
        a.sort()
        return a


    def getXMLs(self, d):
        return glob.glob("%s/*.xml" % d)

    def getProofs(self, d):
        return glob.glob("%s/*.pdf" % d)

    def getHTMLReports(self, d):
        return glob.glob("%s/*_authorlist_check.html" % d)

    def getLink(self, i):
        return i.replace("/afs/cern.ch/atlas/www", "https://atlas.web.cern.ch/Atlas")

    def populate_published_papers(self):
        mgr = DBManager()
        published = mgr.execute("""SELECT
            ADBPMAP.ID 
            FROM 
            PUBLICATION, PHASE, ADBPMAP
            WHERE 
            PUBLICATION.ID = PHASE.PUBLICATION_ID 
            AND PHASE.PHASE = 4 
            AND SIGN_OFF IS NOT NULL 
            AND ADBPMAP.PAPER_REF = PUBLICATION.REF_CODE""")
        mgr.close()

        self.published_papers = [str(x[0]) for x in published]

        #print("debug: published_papers \n{}".format(self.published_papers))

    def populate_ref_codes(self):
        mgr = DBManager()
        ref_codes = mgr.execute("SELECT ID, PAPER_REF FROM ADBPMAP")
        mgr.close()

        for ref_code in ref_codes:
            self.ref_codes[ref_code[0]] = ref_code[1]

    def populate_short_titles(self):
        mgr = DBManager()
        ref_codes = mgr.execute("SELECT ID, PAPER_NAME FROM ADBPMAP")
        mgr.close()

        for ref_code in ref_codes:
            self.short_titles[ref_code[0]] = ref_code[1]

    def run_checker(self):
        """ here the proof checker effectly runs """

        for p in self.proofs:
            if p.newproc:

                print("[INFO] publication RefCode = {}".format(p.refcode))

                for fullname, shortname in p.noredline:
                    savedir = os.path.realpath('.')
                    #os.chdir("/afs/cern.ch/user/a/atlaspo/svntools/AuthorChecker/")

                    _x = p.xmls[:]

                    #how to treat multiple XML entries? Assume atlas_authlist.xml must be removed
                    if len(_x) == 2:
                        _x = [(i, j) for i, j in p.xmls if j != "atlas_authlist.xml"]

                    xname = _x[0][0]
                    dname = os.path.dirname(fullname)

                    ckfile = fullname.replace(".pdf", "_authorlist_check.html")
                    if os.path.isfile(ckfile):
                        print("%s has already been checked, skipping..." % shortname)
                        continue

                    cmd1 = "python2 ./KnowledgeMaker.py -p %s -x %s -o %s" % \
                           (fullname, xname, dname)
                    print("[INFO] command executed: {}".format(cmd1))
                    os.system(cmd1)

                    # Check for existing gitlab version:
                    xml_destination = os.readlink(xname)
                    if xml_destination.find("/atlas_authlist.xml") != -1:
                        gitlab_destination = xml_destination.replace("/atlas_authlist.xml", "g/atlas_authlist.xml")

                    if os.path.isfile(gitlab_destination):
                        xml_destination = gitlab_destination

                    print("[INFO] new comparison method starts here")

                    kfb_target_file_name = dname + "/target_pdf.kfb"
                    xml_file_name = xml_destination
                    proof_file_name = fullname.split("/")[-1:][0].replace(".pdf", "")
                    document = dname.split("/")[-1:][0]

                    print("[INFO] kfb_target_file_name: %s\n" % kfb_target_file_name)
                    print("[INFO] xml_file_name: %s\n" % xml_file_name)
                    print("[INFO] proof_file_name: %s\n" % proof_file_name)
                    print("[INFO] document: %s\n" % document)

                    xml_object = self._make_xml_object(p.id, p.refcode, xml_file_name)

                    #xml_object.set_afs_source(xml_file_name)
                    pdf_object = PdfObjReader(kfb_target_file_name)

                    comparison = Comparison(xml_object, pdf_object)
                    comparison.setFilename(proof_file_name)
                    comparison.setDocument(document)
                    comparison.setReportPath(dname)
                    comparison.analyze()

                    self._make_redirect_page(proof_file_name, document)

                    print ("****       new comparison method ends here       ****")

                    os.chdir(savedir)

        return

    def _get_authorlist_location(self, authorlist_id):
        mgr = DBManager()
        location = mgr.execute("""SELECT
                                LOCATION 
                                FROM 
                                ADBPMAP
                                WHERE 
                                ID = :1""", (authorlist_id,)
                              )
        mgr.close()

        return location[0][0]

    def _gitlab_path_from_authorlist_id(self, authorlist_refcode):
        group = authorlist_refcode[0:4]
        return ("atlas-physics-office/"
                + group + "/"
                + "ANA-" + authorlist_refcode + "/"
                + "ANA-" + authorlist_refcode + "-PAPER")

    def _make_xml_object(self, authorlist_id, authorlist_refcode, afs_xml_filename):
        xml_object = XmlObjReader()
        authorlist_location = self._get_authorlist_location(authorlist_id)
        if authorlist_location == 'GITLAB-ANA':
            xml_object.set_gitlab_source(self._gitlab_path_from_authorlist_id(authorlist_refcode))
        else:
            xml_object.set_afs_source(afs_xml_filename)

        return xml_object


    def _make_redirect_page(self, proof_file_name, document):
        write_name_file = self.arcloc + "/"  + document + "/" + proof_file_name + "_authorlist_check.html"
        print("[DEBUG] output file name original procedure = {}".format(write_name_file))
        print("[DEBUG] output file name new procedure = {}".format(os.path.join(self.arcloc, document, proof_file_name + "_authorlist_check.html")))
        document = document.replace("doc", "")
        htfile = open(write_name_file, 'w')
        htfile.write("<html><head><meta http-equiv='refresh' content='0;URL=https://atlas.web.cern.ch/Atlas/GROUPS/PHYSOFFICE/proofs/proof_report.html?doc=%s&version=%s'</head></html>" % (document, proof_file_name))
        htfile.close()

    def make_symlinks(self):
        for p in self.proofs:
            src = "%s/doc%s" % (self.arcloc, p.id)
            dst = "%s/%s"    % (self.arcloc, p.refcode)
            if not os.path.islink(dst):
                os.symlink(src, dst)

    def read_dir(self, d, filters=None):
        """Read content of directory"""

        if d.find("/doc") != -1:
            al_number = d[d.find("/doc")+len("/doc"):]
            if al_number in self.published_papers and filters is None:
                return True

        debug_mode = False
        #if "1156" in d:
        #    debug_mode = True

        p = proofEntry()

        p.id = re.findall("doc([0-9]+)", d)[0]
        if debug_mode:
            print("[INFO] authorlist id = {}".format(p.id))

        # tstamp is needed to output the web page content in order
        p.tstamp = os.path.getatime(d)

        xmls = self.getXMLs(d)

        p.xmls = zip(xmls, [os.path.basename(i) for i in xmls])
        if debug_mode:
            print("[INFO] xmls file(s) {}".format(p.xmls))

        proofs = self.getProofs(d)

        p.proofs = zip(proofs, [os.path.basename(i) for i in proofs])
        if debug_mode:
            print("[INFO] proofs file(s) {}".format(p.proofs))

        html_reports = self.getHTMLReports(d)

        p.html_reports = zip(html_reports, [os.path.basename(i) for i in html_reports])
        if debug_mode:
            print("[INFO] html reports {}".format(p.html_reports))

        #print "Try to search for document", p.id

        # TODO: one single call for all the papers and build a dictionary to get ref codes and short title
        p.refcode = self.ref_codes[int(p.id)]
        #p.refcode = self.getRefcode(p.id)
        if debug_mode:
            print("[INFO] Publication refcode {}".format(p.refcode))

        p.short_title = self.short_titles[int(p.id)]
        #p.short_title = self.getShort_title(p.id)
        #print "Title: %s: %s - %s" % (p.id, p.refcode, p.short_title)

        p.noredline = [i for i in p.proofs if not 'redline' in i[1]]

        if debug_mode:
            print("[INFO] number of html reports {}".format(len(p.html_reports)))
            print("[INFO] number of red lines {}".format(len(p.noredline)))
        new_html_report_needed = len(p.html_reports) < len(p.noredline)
        # p.newproc = len(p.html_reports) < len(p.noredline) # modified; it was p.reports instead of p.html_reports for "NO TXT VERSION"

        if debug_mode:
            print("[INFO] new html report needed {}".format(new_html_report_needed))

        # p.newproc ?
        if new_html_report_needed:
            p.newproc = True
            print ("[INFO] Resume of proof folder needed to be updated: \n")
            print ("[INFO] New proc : %s \n" % p.newproc),
            print ("[INFO] Html reports %s \n" % p.html_reports),
            print ("[INFO] No redline %s \n" % p.noredline)

        if debug_mode:
            print("[INFO] new proc = {}".format(p.newproc))

        self.proofs.append(p)

        return True

    def _record_sent_email(self, file_name):
        with open("emails_sent.log", "a+") as email_log_file:
            email_log_file.write(file_name)
            email_log_file.write("\n")

    def _is_already_sent(self, file_name):
        try:
            with open("emails_sent.log") as email_log_file:
                emails_sent = email_log_file.read()
                if file_name in emails_sent:
                    return True
        except IOError, error:
            print("[ERROR] while trying to read from emails_sent.log")
            print(error)
        return False

    def _notify_developer(self, publication):
        text = "From: atlaspo@cern.ch\n"
        text += "To: maurizio.colautti@cern.ch\n"
        text += "Subject: proof cheker ERROR!\n\n"
        text += "The proof checker is trying to analyze multiple times the same file:"
        text += "doc {} - ord {} - ref {} - file {}".format(
            publication["doc"],
            publication["ord"],
            publication["ref"],
            publication["file"]
            )

        _pipe = os.popen("/usr/sbin/sendmail -t -i", "w")
        _pipe.write(text)


    def sendMail(self):
        """Send mail to editors when proof is uploaded"""
        ordinal = ['st', 'nd', 'rd', 'th']

        for p in self.proofs:
            if p.newproc:

                sorted_proofs = sorted(p.noredline, key=lambda x: os.path.getctime(x[0]), reverse=True)
                l_p = len(sorted_proofs)
                if l_p == 0:
                    continue

                if l_p > len(ordinal):
                    ordin = "%d%s" % (l_p, ordinal[-1])
                else:
                    ordin = "%d%s" % (l_p, ordinal[l_p-1])

                temp_dict = {"doc":p.id, "ord":ordin, "ref":p.refcode, "file":sorted_proofs[0][1]}

                if self._is_already_sent(temp_dict["file"].lower()):
                    self._notify_developer(temp_dict)
                    continue

                print "**********************"
                print "**** Sending mail for", p.id, p.refcode
                print "**********************"
                print str(datetime.now())
                time.sleep(20)

                text = "From: atlaspo@cern.ch\n"
                text += "Reply-to: atlas-phys-office-pub@cern.ch\n"
                text += "To: atlas-%s-editors@cern.ch\n" % p.refcode.lower()
                text += "Cc: atlas-phys-office-pub@cern.ch\n"
                text += "Cc: atlas-%s-editorial-board@cern.ch\n" % p.refcode.lower()
                text += "Cc: maurizio.colautti@cern.ch\n"
                text += "Subject: Please review: %s Proof %s %s\n" % (ordin, p.refcode, p.short_title)
                text += self.mailtemplate % temp_dict

                _pipe = os.popen("/usr/sbin/sendmail -t -i", "w")
                _pipe.write(text)
                status = _pipe.close()

                if not status:
                    print "Mail sent."
                    self._record_sent_email(temp_dict["file"].lower())
                else:
                    print status

        return

    def make_web(self, dest="eos", filters=None):
        """ creates the webpage with all the list of proofs """
        if dest == "afs":
            if filters is None:
                webfile = '%s/index.html' % self.arcloc
            else:
                webfile = '%s/index_all.html' % self.arcloc
        else:
            if filters is None:
                webfile = '%s/index.html' % self.eosloc
            else:
                webfile = '%s/index_all.html' % self.eosloc

        print "Write web listing on %s" % webfile

        try:
            htfile = open(webfile, 'w')
            header = open(os.path.join(self.homedir, "header.html"))
            htfile.write(header.read())
            header.close()

            htfile.write("\n\n<title>ATLAS collaboration proofs</title>\n</head>\n\n")

            body = open(os.path.join(self.homedir, "body.html"))
            htfile.write(body.read())
            body.close()

            htfile.write("\n\n<h1 align=center>ATLAS collaboration proofs</h1>\n\n")
            if filters is None:
                htfile.write("\n<h3 align=center><a href='index_all.html'>include proofs for recently published papers</a></h3>\n")
            else:
                htfile.write("\n<h3 align=center><a href='index.html'>proofs in submission</a></h3>\n")


            self.make_web_table(htfile, filters)

            htfile.write('<p style="margin-left:5px">Last update: %s</p>\n' % time.strftime("%d-%b-%Y %T"))
            htfile.write("""</div>
    <center><img src="ATLASXP_Transp_Small_01.png" width=50/></center><hr style="border-top:1px solid #ffffff; margin-top:13px; margin-bottom:5px"/>
    <font color="#999999"><font size="-1"><center>ATLAS Collaboration, %s</center></font></font><br/>
    </div><!-- /patternBottomBar-->
    </body>
    </html>""" % time.strftime("%Y"))

            htfile.close()
        except IOError, error:
            print("[ERROR] while trying to write on file {}".format(webfile))
            print("[INFO] timestamp {}\n".format(str(datetime.now())))
            print(error)
            print("\n")

    def make_web_table(self, htfile, filters=None):
        """
        Make web table of authorlists for status between given limits
        """
        htfile.write('<table border="1" frame="box" rules="cols" cellpadding="10" align=center style="margin-left:auto;margin-right:auto;">\n<tr bgcolor=$8080FF style="color:white"><th>ID</th><th>Reference</th><th>Authorlists</th><th>Proofs</th><th>Authorlist Checks</th><th>Last access</th></tr>\n')
        for proof in sorted(self.proofs, key=lambda x: x.tstamp, reverse=True):
            if proof.id not in self.published_papers or filters == "all":
                htfile.write("""<tr valign="top">
                                    <td>%s</td>
                                    <td>%s</td>
                                    <td>%s</BR>&nbsp;</td>
                                    <td>%s</BR>&nbsp;</td>
                                    <td>%s&nbsp;</td>
                                    <td>%s</td>
                                </tr>\n""" %
                             (proof.id,
                              proof.refcode,
                              "</BR>".join(['<a href="%s">%s</a>' % (self.getLink(i[0]), i[1]) for i in proof.xmls]),
                              "</BR>".join(['<a href="%s">%s</a>' % (self.getLink(i[0]), i[1]) for i in proof.proofs]),
                              "</BR>".join(['<a href="%s">%s</a>' % (self.getLink(i[0]), i[1]) for i in proof.html_reports]),
                              time.strftime("%d-%b-%Y", time.localtime(proof.tstamp))
                             )
                            )

        htfile.write("</table>\n\n")


# main code
if __name__ == '__main__':
    print("Execution starts at {}".format(str(datetime.now())))
    MYTOOL = entryTool()
    if MYTOOL.trap:
        try:
            MYTOOL.execute()
        except:
            print('Exception thrown during execution:', sys.exc_type, sys.exc_value)
            MYTOOL.errors += ['Exception during execution: %s %s' % (sys.exc_type, sys.exc_value)]
    else:
        MYTOOL.execute()
        MYTOOL.execute_all() # doesn't execute, just create report for all the 'recent' papers
    print("Run completed with {} errors".format(len(MYTOOL.errors)))
    print("Execution ends at {}".format(str(datetime.now())))
