# -*- coding: utf-8 -*-

from xml.dom.minidom import parse
from xml.dom.minidom import Node

class XmlReader():

    def __init__(self, filename):
        try:
            self._dom = parse(filename)
        except:
            pass
        self._firstAuthor = None
        self._secondAuthor = None # this only to prevent problem with new layout 1194
        self._firstInstitute = None
        self.refCode = None
        self.theDate = None
        self.last_update_date = None
        try:
            self.refCode = self._dom.lastChild.getElementsByTagName("cal:publicationReference")[0].lastChild.data
            #print "Parsed XML authorlist %s (%s)" % (filename, self.refCode)
        except:
            pass

        try:
            self.theDate = self._dom.lastChild.getElementsByTagName("cal:creationDate")[0].lastChild.data
            #print "Reference date: %s" % self.theDate
        except:    
            pass

        try:
            self.last_update_date = self._dom.lastChild.getElementsByTagName("cal:authorlistUpdateDate")[0].lastChild.data
            #print "Last update date: %s" % self.last_update_date
        except:
            pass

        return

    def get_last_update_date(self):
        return self.last_update_date

    def getArchiveDate(self):
        return self.theDate
    
    def getReferenceCode(self):
        return self.refCode 

    def cleanUnicode(self, txt):
        
        if '"' in txt and not '\\' in txt:
            txt = txt.replace('"','\\"')

        if '\\' in txt:
            #txt = txt.replace(u'\\"', u'\xf6')
            txt = txt.replace("{\\\"u}", u'\xfc') # 2018/01/24 mcolautt
            txt = txt.replace(u'\\v{c}', u'\u010d')
            txt = txt.replace("\\'l", u'\u013a')
            txt = txt.replace("\\c{L}", u'\u013b')
            txt = txt.replace("\\c{l}", u'\u013c')
            txt = txt.replace("\\v{L}", u'\u013d')
            txt = txt.replace("\\v{l}", u'\u013e')
            txt = txt.replace("\\'N", u'\u0143') 
            txt = txt.replace("\\'n", u'\u0144')
            txt = txt.replace("\\c{N}", u'\u0145')
            txt = txt.replace("\\c{n}", u'\u0146')
            txt = txt.replace("\\v{N}", u'\u0147')
            txt = txt.replace("\\v{n}", u'\u0148')
            txt = txt.replace("'n", u'\u0149')
            txt = txt.replace("\\={O}", u'\u014c')
            txt = txt.replace("\\={o}", u'\u014d')
            txt = txt.replace("\\u{O}", u'\u014e')
            txt = txt.replace("\\u{o}", u'\u014f')
            txt = txt.replace("\\H{O}", u'\u0150')
            txt = txt.replace("\\H{o}", u'\u0151')
            txt = txt.replace("\\OE{}", u'\u0152')
            txt = txt.replace("\\oe{}", u'\u0153')
            txt = txt.replace("\\'{R}", u'\u0154')
            txt = txt.replace("\\'{r}", u'\u0155')
            txt = txt.replace("\\c{R}", u'\u0156')
            txt = txt.replace("\\c{r}", u'\u0157')
            txt = txt.replace("\\v{R}", u'\u0158')
            txt = txt.replace("\\v{r}", u'\u0159')
            txt = txt.replace("\\'S", u'\u015a')
            txt = txt.replace("\\'s", u'\u015b')
            txt = txt.replace("\\^{S}", u'\u015c')
            txt = txt.replace("\\^{s}", u'\u015d')
            txt = txt.replace("\\c{S}", u'\u015e')
            txt = txt.replace("\\c{s}", u'\u015f')
            txt = txt.replace("\\v{S}", u'\u0160')
            txt = txt.replace("\\v{s}", u'\u0161')
            txt = txt.replace("\\vs", u'\u0161')  
            txt = txt.replace("\\v s", u'\u0161')
            txt = txt.replace("\\c{T}", u'\u0162')
            txt = txt.replace("\\c{t}", u'\u0163')
            txt = txt.replace("\\v{T}", u'\u0164')
            txt = txt.replace("\\v{t}", u'\u0165')
            txt = txt.replace("\\~{U}", u'\u0168')
            txt = txt.replace("\\~{u}", u'\u0169')
            txt = txt.replace("\\={U}", u'\u016a')
            txt = txt.replace("\\={u}", u'\u016b')
            txt = txt.replace("\\u{U}", u'\u016c')
            txt = txt.replace("\\u{u}", u'\u016d')
            txt = txt.replace("\\r{U}", u'\u016e')
            txt = txt.replace("\\r{u}", u'\u016f')
            txt = txt.replace("\\H{U}", u'\u0170')
            txt = txt.replace("\\H{u}", u'\u0171')
            txt = txt.replace("\\k{U}", u'\u0172')
            txt = txt.replace("\\k{u}", u'\u0173')
            txt = txt.replace("\\^{W}", u'\u0174')
            txt = txt.replace("\\^{w}", u'\u0175')
            txt = txt.replace("\\^{Y}", u'\u0176')
            txt = txt.replace("\\^{y}", u'\u0177')
            txt = txt.replace('\\"Y', u'\u0178')
            txt = txt.replace("\\'Z", u'\u0179')
            txt = txt.replace("\\'z", u'\u017a')
            txt = txt.replace("\\.{Z}", u'\u017b')
            txt = txt.replace("\\.{z}", u'\u017c')
            txt = txt.replace("\\v{Z}", u'\u017d')
            txt = txt.replace("\\vZ", u'\u017d')  
            txt = txt.replace("\\v Z", u'\u017d')
            txt = txt.replace("\\v{z}", u'\u017e')
            txt = txt.replace("\\vz", u'\u017e')
            txt = txt.replace("\\v z", u'\u017e')
            txt = txt.replace("\\v A", u'\u01CD')
            txt = txt.replace("\\'{A}", u'\u00C1')
            txt = txt.replace("\\v a", u'\u01CE')
            txt = txt.replace("\\v I", u'\u01CF')
            txt = txt.replace("\\v \i{}", u'\u01D0')
            txt = txt.replace("\\v O", u'\u01D1')
            txt = txt.replace("\\v o", u'\u01D2')
            txt = txt.replace("\\v U", u'\u01D3')
            txt = txt.replace("\\v u", u'\u01D4')
            txt = txt.replace("\\v G", u'\u01E6')
            txt = txt.replace("\\v g", u'\u01E7')
            txt = txt.replace("\\v K", u'\u01E8')
            txt = txt.replace("\\v k", u'\u01E9')
            txt = txt.replace("\\k O", u'\u01EA')
            txt = txt.replace("\\k o", u'\u01EB')
            #txt = txt.replace("DZ", u'\u01F1')
            #txt = txt.replace("Dz", u'\u01F2')
            #txt = txt.replace("dz", u'\u01F3')
            txt = txt.replace("\\'G", u'\u01F4')
            txt = txt.replace("\\`G", u'\u01F5')
            txt = txt.replace("\\`N", u'\u01F8')
            txt = txt.replace("\\`n", u'\u01F9')
            txt = txt.replace("\\textdoublegrave{A}", u'\u0200')
            txt = txt.replace("\\textdoublegrave{A}", u'\u0201')
            txt = txt.replace("\\textroundcap{A}", u'\u0202')
            txt = txt.replace("\\textroundcap{a}", u'\u0203')
            txt = txt.replace("\\textdoublegrave{E}", u'\u0204')
            txt = txt.replace("\\textdoublegrave{e}", u'\u0205')
            txt = txt.replace("\\textroundcap{A}", u'\u0206')
            txt = txt.replace("\\textroundcap{a}", u'\u0207')
            txt = txt.replace("\\textdoublegrave{I}", u'\u0208')
            txt = txt.replace("\\textdoublegrave{\i}", u'\u0209')
            txt = txt.replace("\\textroundcap{I}", u'\u020A')
            txt = txt.replace("\\textroundcap{\i}", u'\u020B')
            txt = txt.replace("\\textdoublegrave{O}", u'\u020C')
            txt = txt.replace("\\textdoublegrave{o}", u'\u020D')
            txt = txt.replace("\\textroundcap{O}", u'\u020E')
            txt = txt.replace("\\textroundcap{o}", u'\u020F')
            txt = txt.replace("\\textdoublegrave{R}", u'\u0210')
            txt = txt.replace("\\textdoublegrave{r}", u'\u0211')
            txt = txt.replace("\\textroundcap{R}", u'\u0212')
            txt = txt.replace("\\textroundcap{r}", u'\u0213')
            txt = txt.replace("\\textdoublegrave{U}", u'\u0214')
            txt = txt.replace("\\textdoublegrave{u}", u'\u0215')
            txt = txt.replace("\\textroundcap{U}", u'\u0216')
            txt = txt.replace("\\textroundcap{u}", u'\u0217')
            txt = txt.replace("\\textcommabelow{S}", u'\u0218')
            txt = txt.replace("\\textcommabelow{s}", u'\u0219')
            txt = txt.replace("\\textcommabelow{T}", u'\u021A')
            txt = txt.replace("\\textcommabelow{t}", u'\u021B')
            txt = txt.replace("\\v{H}", u'\u021E')
            txt = txt.replace("\\v{h}", u'\u021F')
            txt = txt.replace("\\.A", u'\u0226')
            txt = txt.replace("\\.a", u'\u0227')
            txt = txt.replace("\\c E", u'\u0228')
            txt = txt.replace("\\c e", u'\u0229')
            txt = txt.replace("\\.O", u'\u022E')
            txt = txt.replace("\\.o", u'\u022F')
            txt = txt.replace("\\=Y", u'\u0232')
            txt = txt.replace("\\=y", u'\u0233')
            txt = txt.replace("\\texteuro{}", u'\u20AC')
            txt = txt.replace('\\~{a}', u'\xe3')
            txt = txt.replace("\\'{c}", u'\u0107')
            txt = txt.replace('{\\"o}', u'\xf6')
            txt = txt.replace('\\"o', u'\xf6')
            txt = txt.replace('\\"{o}', u'\xf6')
            txt = txt.replace('\\"u', u'\xfc')
            txt = txt.replace('\\"a', u'\xe4')
            txt = txt.replace('\\`e', u'\xe8')
            txt = txt.replace('\\^e', u'\xea')
            txt = txt.replace('\\~n', u'\xf1')
            txt = txt.replace('\\cc', u'\xe7')
            txt = txt.replace('\\c c', u'\xe7')
            txt = txt.replace("\\'e", u'\xe9')
            txt = txt.replace('{\\AA}', u'\xc5')
            txt = txt.replace('\\AA ', u'\xc5')
            txt = txt.replace('\\AA', u'\xc5')
            txt = txt.replace('\\`o', u'\xf2')
            txt = txt.replace('\\ss{}', u'\xdf') # 2018/01/24 mcolautt
            txt = txt.replace('\\ss', u'\xdf')
            txt = txt.replace('\\^o', u'\xf4')
            txt = txt.replace("\\'i", u'\xed')
            txt = txt.replace("\\'o", u'\xf3')
            txt = txt.replace("\\'a", u'\xe1')
            txt = txt.replace("\\'{a}", u'\xe1')
            txt = txt.replace("{\\'a}", u'\xe1')
            txt = txt.replace('{\\o}', u'\xf8') 
            txt = txt.replace('\\\'u', u'\xfa') # 2018/01/24 mcolautt
            txt = txt.replace('\\\'{z}', 'z')
            txt = txt.replace('\u{a}', 'a')
            txt = txt.replace('t\,', 'tat')
            txt = txt.replace("\\'", "\'")
            txt = txt.replace("\~", "") # mcolautt - TODO: fast fix to make it run, but must check which is the correct substitution
            txt = txt.replace("\^", "") # mcolautt - TODO: fast fix to make it run, but must check which is the correct substitution

        return txt
    
    def getFirstAuthor(self):
        if not self._firstAuthor:
            self.getAuthors()
        #if self._secondAuthor:
        #    return self._secondAuthor
        return self._firstAuthor
       
    def getAuthors(self):
         
        collauthlist = self._dom.lastChild.getElementsByTagName("cal:authors")[0]

        authors = collauthlist.getElementsByTagName("foaf:Person")

        auth_tuple_list = []

        for i in authors:
            nm = ""
            af = []
    
            for j in i.getElementsByTagName("cal:authorNamePaper"):
                if j.lastChild.nodeType == Node.TEXT_NODE:
                    nm = j.lastChild.data #.replace(" ","")
                    nm = self.cleanUnicode(nm)     
                    #print "--> %s" % auth_tuple[0]
            
            for j in i.getElementsByTagName("cal:authorAffiliations"):
                for k in j.getElementsByTagName("cal:authorAffiliation"):
                    _af = k.getAttribute("organizationid").lstrip("o")
                    af.append(_af)

            if not self._secondAuthor and self._firstAuthor:
                self._secondAuthor = nm
    
            if not self._firstAuthor:
                self._firstAuthor = nm       
    
            auth_tuple_list.append((nm, ",".join(af)))
            
        return auth_tuple_list

    def getGeneralFacts(self):

        outstr = u""

        outstr += "date(\"%s\")\n" % self.getArchiveDate()
        outstr += "refcode(\"%s\")\n" % self.getReferenceCode()
        
        return outstr

    def getAuthorsFacts(self):
        
        collauthlist = self._dom.lastChild.getElementsByTagName("cal:authors")[0]

        authors = collauthlist.getElementsByTagName("foaf:Person")

        outstr = u""

        for i in authors:
            nm = ""
            af = []
    
            for j in i.getElementsByTagName("cal:authorNamePaper"):
                if j.lastChild.nodeType == Node.TEXT_NODE:
                    nm = self.cleanUnicode(j.lastChild.data).strip()     
            
            for j in i.getElementsByTagName("cal:authorAffiliations"):
                for k in j.getElementsByTagName("cal:authorAffiliation"):
                    if k.getAttribute("organizationid") == 'oao':
                        _af = 'ao'
                    elif k.getAttribute("organizationid") == 'oo':
                        _af = 'o'
                    else:
                        _af = k.getAttribute("organizationid").lstrip("o").strip() 
                    af.append(_af)
                    
            outstr = "%s%s" % (outstr, 'belongsto("%s", "%s")\n' % (nm, af[0]))
            
            for a in af[1:]:
                outstr = "%s%s" % (outstr, 'alsoat("%s", "%s")\n' % (nm, a))
            
            if i.getElementsByTagName("cal:authorStatus")[0].lastChild and \
                i.getElementsByTagName("cal:authorStatus")[0].lastChild.data == "DECEASED":
                outstr = "%s%s" % (outstr, 'isdeceased("%s")\n' % nm)  # :(
        
        
        return outstr

    def getFirstInstitute(self):
        if not self._firstInstitute:
            self.getInstitutes()
        return self._firstInstitute

    def getInstitutes(self):
         
        collinstlist = self._dom.lastChild.getElementsByTagName("cal:organizations")[0]

        institutes = collinstlist.getElementsByTagName("foaf:Organization")

        inst_tuple_list = []

        for i in institutes:
            
            nm = ""
            
            if i.getAttribute("id") == 'oao':
                _id = 'ao'
            elif i.getAttribute("id") == 'oo':
                _id = 'o'
            else:
                _id = i.getAttribute("id").replace("o","")

            for j in i.getElementsByTagName("foaf:name"):
                if j.lastChild.nodeType == Node.TEXT_NODE:
                    nm = j.lastChild.data #.replace(" ","")
                    nm = self.cleanUnicode(nm)     
                    #nm = nm.replace("UnitedStatesofAmerica","UnitedStates")
                    #print "--> %s" % auth_tuple[0]
    
            if not self._firstInstitute:
                    #self._firstInstitute = "%s %s" % (_id, nm)  
                    self._firstInstitute = "%s" % nm 
                    
    
            inst_tuple_list.append((_id, nm))
            
        return inst_tuple_list
    
    def getInstitutesFacts(self):
         
        collinstlist = self._dom.lastChild.getElementsByTagName("cal:organizations")[0]

        institutes = collinstlist.getElementsByTagName("foaf:Organization")

        outstr = u""

        for i in institutes:
            
            nm = ""
            
            if i.getAttribute("id") == 'oao':
                _id = 'ao'
            elif i.getAttribute("id") == 'oo':
                _id = 'o'
            else:
                _id = i.getAttribute("id").replace("o","")
            
            for j in i.getElementsByTagName("foaf:name"):
                if j.lastChild.nodeType == Node.TEXT_NODE:
                    nm = self.cleanUnicode(j.lastChild.data).strip().replace("\n","")     

            if len(i.getElementsByTagName("cal:orgStatus")) > 0 and \
                i.getElementsByTagName("cal:orgStatus")[0].lastChild.data == "member":
                outstr = "%s%s" % (outstr, 'isinstitute("%s", "%s")\n' % (_id, nm))
            else:
                outstr = "%s%s" % (outstr, 'isinstitute_notmember("%s", "%s")\n' % (_id, nm))
            
        return outstr
