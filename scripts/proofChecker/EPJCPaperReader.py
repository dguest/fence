# -*- coding: utf-8 -*-
"""
#####################################
# EPJCPaperReader.py
#
# Uses xpdf to extract text from PDF
# and convert into knowledge files
# Derived class for EPJC papers
#
# Author: Marcello Barisonzi
#
# $Rev: 233716 $
# $LastChangedDate: 2015-02-19 15:29:12 +0100 (Thu, 19 Feb 2015) $
# $LastChangedBy: atlaspo $
#
####################################
"""

import re
import sys

from XpdfPaperReader     import XpdfPaperReader

reload(sys)
sys.setdefaultencoding("utf-8")


class EPJCPaperReader(XpdfPaperReader):
    
    def __init__(self, filename, fp="1", lp="-1"):
        super(EPJCPaperReader, self).__init__(filename, fp, lp)
                               
	#self._countries = ["Romania","China"]
 
        ### pattern should be overwritten by subclasses

        self._instituteExc = [ re.compile("([a-z]{1,2}\s*\n*?Also(?:.*?(?:%s)){2}\.*)" % "|".join(self._excep_countries)),
			                         re.compile("([0-9]{1,3}(?:\s*\([a-z]{1,2}\)\s*.*\n*?(?:[^0-9]+?\n*?[^0-9]*?(?:%s)))+)" % "|".join(self._excep_countries)), 
                               re.compile("[^N,P](^[0-9]{1,3}\s*(?:[A-Z]{1}|\([a-z]{1,2}\))\s*\n*?(?:[^0-9]*?\n*?[^0-9]*?(?:%s)){2})" % "|".join(self._excep_countries)) ]

        
        self._institutePtn = [ re.compile("([a-z]{1,2}\s*\n*?Also.*?(?:%s)\.*)" % "|".join(self._countries)),
                               re.compile("([0-9]{1,3}(?:\s*\([a-z]{1,2}\)\s*.*\n?[^0-9]+(?:%s);?\.?)+)" % "|".join(self._countries)),
                               re.compile("[^N,P]([0-9]{1,3}\s*(?:[A-Z]{1}|\([a-z]{1,2}\))\s*\n*?.*?(?:%s)\.*)" % "|".join(self._countries)),
                               re.compile("(\*\s*\n*?Deceased\.*)")]

        self._institutePtn += [ re.compile("([a-z]{1,2}\s*\n*?Also.*?(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,7) ]
        self._institutePtn += [ re.compile("[^N,P]([0-9]{1,3}\s*(?:[A-Z]{1}|\([a-z]{1,2}\))\s*\n*.*(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,7) ]
        

        self._ptn_id    = re.compile("([0-9]{1,3})\s*\n*")
        self._ptn_also  = re.compile("([a-z]{1,2})\s*\n*Also at(.*)\.*")
        self._ptn_dec   = re.compile("(\*)\s*\n*Deceased\.*")
        self._ptn_subid = re.compile("\(([a-z]{1})\)")
        self._ptn_state = re.compile("(%s)" % "|".join(self._countries))

        self._remove_lines = [re.compile("_#+_")]
        
        return
