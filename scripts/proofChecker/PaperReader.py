# -*- coding: utf-8 -*-

import sys
import re

try:
    from pyPdf import PdfFileReader
except:
    sys.path.insert(0,'./pyPdf-1.13')
    from pyPdf import PdfFileReader

class PaperReader(object):
    
    def __init__(self, filename):
        self._input  = PdfFileReader(file(filename, "rb"))
        self._npages = self._input.getNumPages()

        print "Opened document %s of %s pages." % (filename, self._npages)
        
        self._firstAuthor    = 1
        self._lastAuthor     = self._npages
        self._firstInstitute = 1
        self._lastInstitute  = self._npages
        self._authorTxt      = None
        self._firstAuthorString = ""
        self._instituteTxt   = None
        self._firstInstituteString = ""
        
        self._countries = [ "Argentina",
                            "Armenia",
                            "Australia",
                            "Austria",
                            "Azerbaijan",
                            "Belarus",
                            "Brazil",
                            "Canada",
                            "Chile",
                            "China",
                            "Colombia",
                            "CzechRepublic",
                            "Denmark",
                            "France",
                            "Georgia",
                            "Germany",
                            "Greece",
                            "Hungary",
                            "Israel",
                            "Italy",
                            "Japan",
                            "Morocco",
                            "Netherlands",
                            "TheNetherlands",
                            "Norway",
                            "Poland",
                            "Portugal",
                            "RepublicofBelarus",
                            "Romania",
                            "Russia",
                            "Serbia",
                            "Slovakia",
                            "SlovakRepublic",
                            "Slovenia",
                            "SouthAfrica",
                            "Spain",
                            "Sweden",
                            "Switzerland",
                            "Taiwan",
                            "Turkey",
                            "UnitedKingdom",
                            "UnitedStates",
                            "USA",
                            "[D,d]eceased" ]
                        
        ### pattern should be overwritten by subclasses
        self._authorPtn = re.compile("((?:[A-Z]+[a-z]*?[\.|\-]+)+.+?),*?([0-9]+(?:[0-9]*[a-z]*,*)+)")
        #self._authorPtn = re.compile("([A-Z]+\..*?)((?:[0-9]{0,3}[a-z]{0,2},*)+)")
        #self._institutePtn = re.compile("((?:[0-9]{1,3}(?:[A-Z]|\()|[a-z]{1,2}Also).*?,(?:%s)\.*)" % "|".join(self._countries))
        self._institutePtn = re.compile("((?:[a-z]{1,2}Also|[0-9]{1,3}(?:[A-Z]|\([a-z]{1,2}\))).*?,(?:%s)\.*)" % "|".join(self._countries))
        #ptn = re.compile("((?:[0-9]|[a-z]){0,3}.*?\.)")
        
        return
    
    def setAuthorPages(self, first, last):
        self._firstAuthor    = first
        self._lastAuthor     = last
        return
    
    def setInstitutePages(self, first, last):
        self._firstInstitute = first
        self._lastInstitute  = last
        return
 
    def setFirstAuthor(self, stg):
        self._firstAuthorString = stg.replace(" ","")
        print "Setting first author --> %s" % self._firstAuthorString
        return

    def setLastAuthor(self, stg):
        self._lastAuthorString = stg.replace(" ","")
        print "Setting last author --> %s" % self._lastAuthorString
        return
    
    def setFirstInstitute(self, stg):
        # use first few charachters to avoid mismatching
        self._firstInstituteString = stg.replace(" ","")[:16] 
        print "Setting first institute --> %s" % self._firstInstituteString
        return
    
    def searchInstituteString(self, stg):
        if not self._instituteTxt: 
            self.getInstituteText()
    
        ptn = re.compile(stg)
    
        if not ptn.search(self._instituteTxt):   
            return  None
        else:
            return ptn.findall(self._instituteTxt)[0] 
    
#    def removePageLines(self, pg, txt):
#        _txt = txt
#        ptn = re.compile("%d166.*?65130" % pg)
#        if ptn.search(_txt):
#            _txt = _txt.replace(ptn.findall(_txt)[0], "")
#            
#        return _txt
    
    def getAuthorText(self):
        
        if not self._authorTxt:    
            
            self._authorTxt = u""
             
            for i in range(self._firstAuthor-1, self._lastAuthor):
                tPg = self._input.getPage(i)
            
                self._authorTxt += tPg.extractText()
        
            self._authorTxt = self.cleanUnicode(self._authorTxt)
            
            idx1 = self._authorTxt.rfind(self._firstAuthorString)
            idx2 = self._authorTxt.rfind(self._firstInstituteString)
            
            if idx1 == -1 or idx2 == -1:
                print self._authorTxt
        
            self._authorTxt = self._authorTxt[idx1:idx2]
            
        return self._authorTxt

    def getInstituteText(self):
        
        if not self._instituteTxt:    
            
            self._instituteTxt = u""
             
            for i in range(self._firstInstitute-1, self._lastInstitute):
                tPg = self._input.getPage(i)
            
                self._instituteTxt += tPg.extractText()
        
            self._instituteTxt = self.cleanUnicode(self._instituteTxt)
            
            idx = self._instituteTxt.find(self._firstInstituteString)
        
            if idx == -1:
                print self._instituteTxt
        
            self._instituteTxt = self._instituteTxt[idx:]
            
            #print "Found index: %d" % idx
            
        return self._instituteTxt
        
    def cleanUnicode(self, txt):

        if '"' in txt and not '\\' in txt:
            txt= txt.replace('"','\\"')

        ### use for reference: http://en.wikibooks.org/wiki/Unicode/Character_reference/0000-0FFF
        
        ### remove 'fi' and 'fl' ligatures
        # EPJC
        txt = txt.replace(u'\u02da','fi').replace(u'\u02dc','fl')
        
        # PLB
        txt = txt.replace(u'\ufb01','fi').replace(u'\ufb02','fl')

        # PRL
        txt = txt.replace(u'\xde','fi').replace(u'\xdf','fl')


        ### correct apostrophe
        txt = txt.replace(u'\u2122',"'") # EPJC
        
        txt = txt.replace(u'\u2019',"'") # PLB

        txt = txt.replace(u'\xd5',"'") # PRL
        
        ### c and diacritics
        # EPJC
        txt = txt.replace(u'\u02d8c',u'\u010d').replace(u'\xb4c',u'\u0107')
        
        # PLB 
        txt = txt.replace(u'\u02c7c',u'\u010d')

        # PRL
        txt = txt.replace(u'c\xff',u'\u010d')
        
        ### z and s and diacritics
        txt = txt.replace(u'\u201d', u'\u017d').replace(u'\u0131',u'\u0161')

        txt = txt.replace(u's\xff',u'\u0161').replace(u'Z\xff', u'\u017d').replace(u'z\xff', u'\u017e')
        
        # More PRL shit
        txt = txt.replace(u'c\xfc', u'\u00e7').replace(u'c\xab', u'\u0107')

        txt = txt.replace(u'u\xac', u'\u00fc').replace(u'a\xac',u'\u00e4').replace(u'o\xac',u'\u00f6')

        txt = txt.replace(u'a\xab', u'\u00e1').replace(u'e\xab', u'\u00e9').replace(u'\xf5\xab', u'\u00ed').replace(u'o\xab', u'\u00f3')

        txt = txt.replace(u'n\xf7',u'\u00f1').replace(u'a\xf7',u'\u00e3')

        txt = txt.replace(u'o\xf6',u'\u00f4').replace(u'e\xf6',u'\u00ea')

        txt = txt.replace(u'a`',u'\u00e0').replace(u'e`',u'\u00e8').replace(u'i`',u'\u00ec').replace(u'o`',u'\u00f2').replace(u'u`',u'\u00f9')
        
        txt = txt.replace(u'A\xfb',u'\u00c5')

        txt = txt.replace(u'\xa7',u'\u00df')

        txt = txt.replace(u'\xd0',u'-')
        
        txt = txt.replace(u'\xbf',u'\u00f8')
        
        txt = txt.replace(u'\xe2\x80\xa2', u'\u2022')
        
        return txt
    
    def getInstitutes(self):
        
        if not self._instituteTxt: 
                self.getInstituteText()
    
        _ilist = self._institutePtn.findall(self.cleanUnicode(self._instituteTxt))
        _nlist = []
        
        ptn_id    = re.compile("([0-9]{1,3})")
        ptn_a     = re.compile("([a-z]{1,2})Alsoat(.*)\.*")
        ptn_subid = re.compile(".*?\(([a-z]{1})\).*?")
        ptn_state = re.compile("(%s)" % "|".join(self._countries))
        
        # spurious page numbering PLB
        #ptn_pageno = re.compile("166.*?65130")
        
        _seq_id = 1  # expect ids to be sequential :)    
        
        for i in _ilist:
            
            # clean up some funny characters (PLB)     
            _idot = i.rfind(u'\u2022')
            if _idot != -1:
                i = i[_idot+1:]
            
            if not ptn_id.match(i) and not ptn_a.match(i):
                print "Skipping", i, ptn_id.match(i), ptn_a.match(i)
                continue
            
            #if ptn_pageno.search(i):
            #    strg = ptn_pageno.findall(i)[0]
            #    idx = i.find(strg) + len(strg)              
            #    _ilist.append(i[idx:])
            #    continue
               
            state = ptn_state.findall(i)[0]
                       
            if i[0].isdigit() and ptn_id.search(i):
                _id = ptn_id.findall(i)[0]
 
                _expected_id = "%s" % _seq_id
                if _expected_id in i and _id != _expected_id:
                    _id = _expected_id
                    
                _seq_id += 1
                
                if ptn_subid.search(i):
                    for j in i.split(";"):
                        try:
                            subid = ptn_subid.findall(j)[0]
                            k = j.replace(_id,"").replace("(%s)" % subid,"")
                            if not ",%s" % state in k:
                                k = "%s,%s" % (k, state)
                            _nlist.append(("%s%s" % (_id, subid), k))
                        except:
                            print "ERR: %s" % i
                else:
                    _nlist.append((_id, i[i.rfind(_id)+len(_id):]))      
            elif ptn_a.search(i):
                a,b = ptn_a.findall(i)[0]
                
                #a must be a letter, remove stray numbers:
                while not a[0].isalpha():
                    a = a[1:]
                
                _nlist.append((a, b))

    
        return _nlist

    def getInstitutesFacts(self):
        
        institutes = self.getInstitutes()
        
        outstr = u""

        for i,j in institutes:
            if i[0].isdigit():
                outstr = "%s%s" % (outstr, 'isinstitute("%s", "%s")\n' % (i, j.rstrip(".")))
            else:
                outstr = "%s%s" % (outstr, 'isinstitute_notmember("%s", "%s")\n' % (i, j.rstrip(".")))
            
        return outstr
    
    def getAuthors(self):
        
        if not self._authorTxt: 
                self.getAuthorText()
        
        _alist = self._authorPtn.findall(self._authorTxt)
        _nlist = []
        
        for i,j in _alist:
            #k = j.split(',')
            #while '' in k:
            #    k.remove('')
            
            # remove hyphenation
            i.strip('-')
            hyph = i.count('-')
            
            if hyph > 0:
                try:
                    m = [i[n] for n in range(0,len(i)) \
                         if i[n] != '-' or i[n+1].isupper()]
                    i = "".join(m)
                except:
                    print "Hyphenation error: %s" % i
            
            k = j.strip(",")
            _nlist.append((i,k))
            
        return _nlist
        
    def getAuthorsFacts(self):
        
        authors = self.getAuthors()

        outstr = u""

        for nm,j in authors:
            af = j.split(",")
                   
            outstr = "%s%s" % (outstr, 'belongsto("%s", "%s")\n' % (nm, af[0]))
            
            for a in af[1:]:
                outstr = "%s%s" % (outstr, 'alsoat("%s", "%s")\n' % (nm, a))
                    
        return outstr
