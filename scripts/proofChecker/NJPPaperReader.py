# -*- coding: utf-8 -*-
"""
#####################################
# NJPPaperReader.py
#
# Uses xpdf to extract text from PDF
# and convert into knowledge files
# Derived class for NJP papers
#
# Author: Marcello Barisonzi
#
# $Rev: 160923 $
# $LastChangedDate: 2014-04-28 22:14:16 +0200 (Mon, 28 Apr 2014) $
# $LastChangedBy: atlaspo $
#
####################################
"""

import re
import sys

from XpdfPaperReader import XpdfPaperReader

reload(sys)
sys.setdefaultencoding("utf-8")


class NJPPaperReader(XpdfPaperReader):
    
    def __init__(self, filename, fp="1", lp="-1"):
        super(self.__class__, self).__init__(filename, fp, lp)
                        
        ### pattern should be overwritten by subclasses

        self._authorPtn = re.compile("(?:,\s)*((?:[A-Z]{1}[a-z]{0,1}[\s|\-]+)+(?:(?:della|de|van(?: der)*|von(?: der)*|zur)\s)*[A-Z]{1}[^0-9]+?)([0-9]+(?:[0-9]*[a-z]*,*)*)")

        self._instituteExc = [ re.compile("([0-9]{1,3}\nAlso(?:.*?(?:%s)){2}\.*)" % "|".join(self._excep_countries)),
                               re.compile("[^N,P]([0-9]{1,3}(?:[a-z]{1}\n*)*?(?:[^0-9]*?\n*?[^0-9]*?(?:%s)){2})" % "|".join(self._excep_countries))
            ]

        
        self._institutePtn = [ re.compile("([0-9]{1,3}\nAlso.*?(?:%s)\.*)" % "|".join(self._countries)),
                               re.compile("[^N,P]([0-9]{1,3}[a-z]{0,1}\n*?.*?(?:%s)\.*)" % "|".join(self._countries)),
                               re.compile("([0-9]{1,3}\nDeceased\.*)")]

        self._institutePtn += [ re.compile("([0-9]{1,3}\nAlso.*?(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,3) ]
        self._institutePtn += [ re.compile("[^N,P]([0-9]{1,3}[a-z]{0,1}\n*?.*(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,3) ] 

        self._ptn_id    = re.compile("([0-9]{1,3}[a-z]{0,1})\s*\n*")
        self._ptn_also  = re.compile("([0-9]{1,3})\sAlso at(.*)\.*")
        self._ptn_dec   = re.compile("([0-9]{1,3})\sDeceased\.*")
        self._ptn_subid = re.compile("(?!.).") # match none, not used for NJP
        self._ptn_state = re.compile("(%s)" % "|".join(self._countries))
        
        return

    def setFirstAuthor(self, stg):
        self._firstAuthorString = stg.replace(".","")
        print "Setting first author --> %s" % self._firstAuthorString
        return

    def cleanUnicode(self, txt):

        ### use for reference: http://en.wikibooks.org/wiki/Unicode/Character_reference/0000-0FFF

        targets = {"T P A (.{1,3}\s*)kesson" : u"\u00c5",
                   "B (.{1,3}\s*)sman" : u"\u00c5",
                   "J Barreiro Guimar(.{1,3})es" : u"\u00e3",
                   "A Filip(.{1,3})i"                : u"\u010d",
                   "L ([^0-9]{1,3})ivkovi"             : u'\u017d',
                   "L .{1,3}ivkovi([^0-9]{1,3})"             : u'\u0107',
                   "V B(.{1,3})scher"            : u'\u00fc',
                   "P M(.{1,3})ttig" : u'\u00e4',
                   "Bergische Universit(.{1,3})t Wuppertal" : u'\u00e4',
                   "R Gon(.{1,3})alo" : u'\u00e7',
                   "C Garc(.*?)a" : u'\u00ed',
                   "J E Garc(.*?)a Navarro" : u'\u00ed',
                   "A Gori(.{1,3})ek" : u'\u0161',
                   "T Hen([^0-9]{1,3})"  : u'\u00df', 
                   #"P. Je(.{1,3})" :   u'\u017e',  # P. Jez and P. Jenni 
                   "B P Ker(.{1,3})evan" : u'\u0161',
                   "S Cr(.{1,3})p.{1,3}-Renaudin" : u'\u00e9',
                   "S Cr.{1,3}p(.{1,3})-Renaudin" : u'\u00e9',
                   "J Lev(.{1,3})que" : u'\u00ea',
                   "B Ma(.{1,3})ek" : u"\u010d",
                   "M Miku(.{1,3})" :   u'\u017e',
                   "P Conde Mui(.{1,3})o" :   u'\u00f1',
                   "M Mi(.{1,3})ano Moya" :   u'\u00f1',
                   "M T P(.{1,3})rez Garc.*?a-Esta.{1,3}" : u"\u00e9",
                   "M T P.{1,3}rez Garc(.*?)a-Esta.{1,3}" :   u'\u00ed',
                   "M T P.{1,3}rez Garc.*?a-Esta(.{1,3})" :   u'\u00f1',
                   #"T. (.{1,3})eni.{1,3}" : u'\u017d',
                   #"T. .{1,3}eni(.{1,3})" : u'\u0161',
                   "Palack(.{1,3}) University" : u'\u00fd',
                   "Universit(.{1,3}) di Pavia" : u'\u00e0',
                   "G Amor(.{1,3})s" : u"\u00f3",
                   "S Cabrera Urb(.{1,3})n" : u'\u00e1',
                   "T G(.{1,3})pfert" : u'\u00f6',
                   "C G(.{1,3})ssling" : u'\u00f6',
                   "T G(.{1,3})ttfert" : u'\u00f6',
                   "S Gonz(.{1,3})lez de la Hoz" : u'\u00e1',
                   "P Grafstr(.{1,3})m" : u'\u00f6',
                   "Y Hern(.{1,3})ndez Jim.{1,3}nez" : u'\u00e1',
                   "Y Hern.{1,3}ndez Jim(.{1,3})nez" : u'\u00e9',
                   "E Hig(.{1,3})n-Rodriguez" : u"\u00f3",
                   "K K(.{1,3})neke" : u'\u00f6',
                   "A C K(.{1,3})nig" : u'\u00f6',
                   "L K(.{1,3})pke" : u'\u00f6',
                   "J U Mj(.{1,3})rnmark" : u'\u00f6',
                   "K M(.{1,3})nig" : u'\u00f6',
                   "N M(.{1,3})ser" : u'\u00f6',
                   "S Mohrdieck-M(.{1,3})ck" : u'\u00f6',
                   "M Moreno Ll(.{1,3})cer" : u'\u00e1',
                   "G P(.{1,3})sztor" : u'\u00e1',
                   "K Pomm(.{1,3})s"   : u'\u00e8',
                   "J Sj(.{1,3})lin" : u'\u00f6',
                   "Universitat\s*\n*Aut(.{1,3})noma" : u'\u00f2',
                   "F Span([^0-9]{1,3})" : u'\u00f2',
                   "R Str(.{1,3})hmer" : u'\u00f6',
                   "J S(.{1,3})nchez" : u'\u00e1',
                   "S Tok(.{1,3})r" : u'\u00e1',
                   "E Torr(.{1,3}) Pastor" : u"\u00f3",
                   "F Anghinol([^0-9]{1,3})" : "fi",
                   "D Ban([^0-9]{1,3})" : "fi",
                   "P Cala(.{1,3})ura" : "fi",
                   "A Da(.{1,3})nca" : "fi",
                   "L Du(.{1,3})ot" : "fl",
                   "R Dux(.{1,3})eld" : "fi",
                   "G Gor(.{1,3})ne" : "fi",
                   "J Grif(.{1,3})ths" : "fi",
                   "J Gri(.{1,3})ths" : "ffi",
                   "J Inigo-Gol(.{1,3})n" : "fi",
                   "E Katsou(.{1,3})s" : "fi",
                   "I Mandi(.{1,3})"  : u'\u0107',
                   "S J Max(.{1,3})eld" : "fi",
                   "L Mijovi(.{1,3})"  : u'\u0107',
                   "K Proko(.{1,3})ev" :  "fi",
                   "R Ta(.{1,3})rout" : "fi",
                   "R W Cli(.{1,3})t" : "ff",
                   "I Hinchli(.{1,3})e" : "ff",
                   "J Ho(.{1,3})man" : "ff",
                   "D Ho(.{1,3})mann" : "ff",
                   "T B. Hu(.{1,3})man" : "ff",
                   "E Ko(.{1,3})eman" : "ff",
                   "A Ta(.{1,3})ard" : "ff",
                   }

        replaces = {}
        
        for k,v in targets.iteritems():
            #print k
            if re.search(k,txt):
                #print k, re.findall(k,txt)
                replaces[re.findall(k,txt)[-1]] = v


        #print replaces

        for k,v in replaces.iteritems():
            txt = txt.replace(k,v)
            
        #example: Cote
        #txt = txt.replace(u'o\u02c6',u'\u00f4').replace(u'\u02c6o',u'\u00f4')
        if re.search(u"D C(.{1,3})t\u00e9" ,txt, re.UNICODE):
            txt = txt.replace(re.findall(u"D C(.{1,3})t\u00e9" ,txt, re.UNICODE)[0], u'\u00f4')


        ### correct apostrophe
        txt = txt.replace(u'\u2122',"'") # EPJC
        
        txt = txt.replace(u'\u2019',"'") # PLB

        txt = txt.replace(u'\xd5',"'") # PRL

        ### asterisk
        txt = txt.replace('\xe2\x88\x97', "*") # JHEP

        return txt

    # NJP likes to put the summary at the end... *FACEPALM*
    def getAuthorText(self):
        
        if not self._authorTxt:    

            self._authorTxt = self._input
        
            self._authorTxt = self.cleanUnicode(self._authorTxt)
            
            idx1 = self._authorTxt.find(self._firstAuthorString)
            idx2 = self._authorTxt.find(self._firstInstituteString)-2

            if idx1 == -1 or idx2 == -1:
                print self._authorTxt
            else:
                print idx1, idx2
                self._authorTxt = self._authorTxt[idx1:idx2]
            
        return self._authorTxt
