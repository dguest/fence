#!/bin/env python2
# -*- coding: utf-8 -*-

"""
The script verifies that the author list last update date in the file is consistent
with the value in ATLAS Glance db
written by: Maurizio Colautti - maurizio.colautti@cern.ch
"""

import os
import sys
import datetime

from XmlReader import XmlReader

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

basePath = "/afs/cern.ch/atlas/www/GROUPS/DATABASE/authdb/authorlists/fenceAuthorlists/"

for root, dirs, files in os.walk(basePath):

    for file in files:

        if file.endswith("atlas_authlist.xml"):

            xml_file_name = os.path.join(root, file)
            #print ("xml_file_name = %s" % xml_file_name)

            #continue

            #xml_file_name = "/afs/cern.ch/user/a/atlaspo/authorlists/fenceAuthorlists/1086/atlas_authlist.xml"

            xml_object = XmlReader(xml_file_name)
            ref_code = xml_object.getReferenceCode()

            if not ref_code:
                continue

            last_update_date = xml_object.get_last_update_date()

            if not last_update_date:
                continue

            last_update_date_datetime = datetime.datetime.strptime(last_update_date, '%d-%b-%y')

            #print ("last update date datetime = %s" % last_update_date_datetime)

            db = DBManager()
            query = "SELECT LAST_UPDATE FROM ADBPMAP WHERE PAPER_REF = '%s'" % ref_code

            #print ("db query = %s" % query)

            last_update_date_db_result = db.execute(query)
            last_update_date_db = last_update_date_db_result[0][0]

            #print ("in the db the last update date is set to %s" % last_update_date_db)

            if last_update_date_datetime != last_update_date_db:
                print ("problem with date")
                print ("filename = %s" % xml_file_name)
                print ("last_update_date_datetime = %s" % last_update_date_datetime)
                print ("last_update_date_db = %s" % last_update_date_db)