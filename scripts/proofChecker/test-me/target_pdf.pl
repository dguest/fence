:- discontiguous(alsoat/2).
:- discontiguous(belongsto/2).
:- discontiguous(isdeceased/1).
:- discontiguous(isinstitute/2).
:- discontiguous(isinstitute_notmember/2).

publisher('JHEP').
isinstitute_notmember('af', 'Institute of Physics, Azerbaijan Academy of Sciences, Baku; Azerbaijan').
isinstitute_notmember('a', 'Borough of Manhattan Community College, City University of New York, New York NY; United States of America').
isinstitute_notmember('b', 'Centre for High Performance Computing, CSIR Campus, Rosebank, Cape Town; South Africa').
isinstitute_notmember('c', 'CERN, Geneva; Switzerland').
isinstitute_notmember('d', 'CPPM, Aix-Marseille Université, CNRS/IN2P3, Marseille; France').
isinstitute_notmember('e', 'Département de Physique Nucléaire et Corpusculaire, Université de Genève, Genève; Switzerland').
isinstitute_notmember('f', 'Departament de Fisica de la Universitat Autonoma de Barcelona, Barcelona; Spain').
isinstitute_notmember('g', 'Departamento de Física, Instituto Superior Técnico, Universidade de Lisboa, Lisboa; Portugal').
isinstitute_notmember('i', 'Department of Financial and Management Engineering, University of the Aegean, Chios; Greece').
isinstitute_notmember('j', 'Department of Physics and Astronomy, University of Louisville, Louisville, KY; United States of America').
isinstitute_notmember('k', 'Department of Physics and Astronomy, University of Sheffield, Sheffield; United Kingdom').
isinstitute_notmember('l', 'Department of Physics, California State University, East Bay; United States of America').
isinstitute_notmember('m', 'Department of Physics, California State University, Fresno; United States of America').
isinstitute_notmember('n', 'Department of Physics, California State University, Sacramento; United States of America').
isinstitute_notmember('o', 'Department of Physics, King\'s College London, London; United Kingdom').
isinstitute_notmember('p', 'Department of Physics, St. Petersburg State Polytechnical University, St. Petersburg; Russia').
isinstitute_notmember('q', 'Department of Physics, Stanford University, Stanford CA; United States of America').
isinstitute_notmember('r', 'Department of Physics, University of Fribourg, Fribourg; Switzerland').
isinstitute_notmember('s', 'Department of Physics, University of Michigan, Ann Arbor MI; United States of America').
isinstitute_notmember('t', 'Faculty of Physics, M.V. Lomonosov Moscow State University, Moscow; Russia').
isinstitute_notmember('u', 'Giresun University, Faculty of Engineering, Giresun; Turkey').
isinstitute_notmember('v', 'Graduate School of Science, Osaka University, Osaka; Japan').
isinstitute_notmember('w', 'Hellenic Open University, Patras; Greece').
isinstitute_notmember('x', 'Horia Hulubei National Institute of Physics and Nuclear Engineering, Bucharest; Romania').
isinstitute_notmember('y', 'Institucio Catalana de Recerca i Estudis Avancats, ICREA, Barcelona; Spain').
isinstitute_notmember('z', 'Institut fòr Experimentalphysik, Universität Hamburg, Hamburg; Germany').
isinstitute_notmember('ac', 'Institute for Particle and Nuclear Physics, Wigner Research Centre for Physics, Budapest; Hungary').
isinstitute_notmember('ad', 'Institute of Particle Physics (IPP); Canada').
isinstitute_notmember('ae', 'Institute of Physics, Academia Sinica, Taipei; Taiwan').
isinstitute_notmember('ag', 'Institute of Theoretical Physics, Ilia State University, Tbilisi; Georgia').
isinstitute_notmember('ah', 'Instituto de Fisica Teorica, IFT-UAM/CSIC, Madrid; Spain').
isinstitute_notmember('ai', 'Istanbul University, Dept. of Physics, Istanbul; Turkey').
isinstitute_notmember('aj', 'Joint Institute for Nuclear Research, Dubna; Russia').
isinstitute_notmember('ak', 'LAL, Université Paris-Sud, CNRS/IN2P3, Université Paris-Saclay, Orsay; France').
isinstitute_notmember('al', 'Louisiana Tech University, Ruston LA; United States of America').
isinstitute_notmember('am', 'LPNHE, Sorbonne Université, Paris Diderot Sorbonne Paris Cité, CNRS/IN2P3, Paris; France').
isinstitute_notmember('an', 'Manhattan College, New York NY; United States of America').
isinstitute_notmember('ao', 'Moscow Institute of Physics and Technology State University, Dolgoprudny; Russia').
isinstitute_notmember('ap', 'National Research Nuclear University MEPhI, Moscow; Russia').
isinstitute_notmember('aq', 'Physics Department, An-Najah National University, Nablus; Palestine').
isinstitute_notmember('ar', 'Physikalisches Institut, Albert-Ludwigs-Universität Freiburg, Freiburg; Germany').
isinstitute_notmember('as', 'School of Physics, Sun Yat-sen University, Guangzhou; China').
isinstitute_notmember('at', 'The City College of New York, New York NY; United States of America').
isinstitute_notmember('au', 'The Collaborative Innovation Center of Quantum Matter (CICQM), Beijing; China').
isinstitute_notmember('aw', 'TRIUMF, Vancouver BC; Canada').
isinstitute_notmember('ax', 'Universita di Napoli Parthenope, Napoli; Italy').
isinstitute('1', 'Department of Physics, University of Adelaide, Adelaide; Australia').
isinstitute('2', 'Physics Department, SUNY Albany, Albany NY; United States of America').
isinstitute('3', 'Department of Physics, University of Alberta, Edmonton AB; Canada').
isinstitute('5', 'LAPP, Université Grenoble Alpes, Université Savoie Mont Blanc, CNRS/IN2P3, Annecy; France').
isinstitute('6', 'High Energy Physics Division, Argonne National Laboratory, Argonne IL; United States of America').
isinstitute('7', 'Department of Physics, University of Arizona, Tucson AZ; United States of America').
isinstitute('8', 'Department of Physics, University of Texas at Arlington, Arlington TX; United States of America').
isinstitute('9', 'Physics Department, National and Kapodistrian University of Athens, Athens; Greece').
isinstitute('10', 'Physics Department, National Technical University of Athens, Zografou; Greece').
isinstitute('11', 'Department of Physics, University of Texas at Austin, Austin TX; United States of America').
isinstitute('13', 'Institute of Physics, Azerbaijan').
isinstitute('14', 'Institut de Física d\'Altes Energies (IFAE), Barcelona Institute of Science and Technology, Barcelona; Spain').
isinstitute('16', 'Institute of Physics, University of Belgrade, Belgrade; Serbia').
isinstitute('17', 'Department for Physics and Technology, University of Bergen, Bergen; Norway').
isinstitute('18', 'Physics Division, Lawrence Berkeley National Laboratory and University of California, Berkeley CA; United States of America').
isinstitute('19', 'Institut fòr Physik, Humboldt Universität zu Berlin, Berlin; Germany').
isinstitute('21', 'School of Physics and Astronomy, University of Birmingham, Birmingham; United Kingdom').
isinstitute('22', 'Facultad de Ciencias y Centro de Investigaciónes, Universidad Antonio Nariño, Bogota; Colombia').
isinstitute('23a', 'INFN Bologna and Universita\' di Bologna, Dipartimento di Fisica, Italy').
isinstitute('23b', 'INFN Sezione di Bologna, Italy').
isinstitute('24', 'Physikalisches Institut, Universität Bonn, Bonn; Germany').
isinstitute('25', 'Department of Physics, Boston University, Boston MA; United States of America').
isinstitute('26', 'Department of Physics, Brandeis University, Waltham MA; United States of America').
isinstitute('29', 'Physics Department, Brookhaven National Laboratory, Upton NY; United States of America').
isinstitute('30', 'Departamento de Física, Universidad de Buenos Aires, Buenos Aires; Argentina').
isinstitute('31', 'California State University, CA; United States of America').
isinstitute('32', 'Cavendish Laboratory, University of Cambridge, Cambridge; United Kingdom').
isinstitute('34', 'Department of Physics, Carleton University, Ottawa ON; Canada').
isinstitute('36', 'CERN, Geneva; Switzerland').
isinstitute('37', 'Enrico Fermi Institute, University of Chicago, Chicago IL; United States of America').
isinstitute('38', 'LPC, Université Clermont Auvergne, CNRS/IN2P3, Clermont-Ferrand; France').
isinstitute('39', 'Nevis Laboratory, Columbia University, Irvington NY; United States of America').
isinstitute('40', 'Niels Bohr Institute, University of Copenhagen, Copenhagen; Denmark').
isinstitute('42', 'Physics Department, Southern Methodist University, Dallas TX; United States of America').
isinstitute('43', 'Physics Department, University of Texas at Dallas, Richardson TX; United States of America').
isinstitute('44', 'National Centre for Scientific Research “Demokritos”, Agia Paraskevi; Greece').
isinstitute('45a', 'Department of Physics, Stockholm University, Sweden').
isinstitute('45b', 'Oskar Klein Centre, Stockholm, Sweden').
isinstitute('46', 'Deutsches Elektronen-Synchrotron DESY, Hamburg and Zeuthen; Germany').
isinstitute('47', 'Lehrstuhl fòr Experimentelle Physik IV, Technische Universität Dortmund, Dortmund; Germany').
isinstitute('48', 'Institut fòr Kern- und Teilchenphysik, Technische Universität Dresden, Dresden; Germany').
isinstitute('49', 'Department of Physics, Duke University, Durham NC; United States of America').
isinstitute('50', 'SUPA - School of Physics and Astronomy, University of Edinburgh, Edinburgh; United Kingdom').
isinstitute('51', 'INFN e Laboratori Nazionali di Frascati, Frascati; Italy').
isinstitute('52', 'Physikalisches Institut, Albert-Ludwigs-Universität Freiburg, Freiburg; Germany').
isinstitute('53', 'II. Physikalisches Institut, Georg-August-Universität Göttingen, Göttingen; Germany').
isinstitute('54', 'Département de Physique Nucléaire et Corpusculaire, Université de Genève, Genève; Switzerland').
isinstitute('55a', 'Dipartimento di Fisica, Università di Genova, Genova, Italy').
isinstitute('55b', 'INFN Sezione di Genova, Italy').
isinstitute('56', 'II. Physikalisches Institut, Justus-Liebig-Universität Giessen, Giessen; Germany').
isinstitute('57', 'SUPA - School of Physics and Astronomy, University of Glasgow, Glasgow; United Kingdom').
isinstitute('58', 'LPSC, Université Grenoble Alpes, CNRS/IN2P3, Grenoble INP, Grenoble; France').
isinstitute('59', 'Laboratory for Particle Physics and Cosmology, Harvard University, Cambridge MA; United States of America').
isinstitute('62', 'Faculty of Applied Information Science, Hiroshima Institute of Technology, Hiroshima; Japan').
isinstitute('64', 'Department of Physics, National Tsing Hua University, Hsinchu; Taiwan').
isinstitute('65', 'Department of Physics, Indiana University, Bloomington IN; United States of America').
isinstitute('67a', 'INFN Sezione di Lecce, Italy').
isinstitute('67b', 'Dipartimento di Matematica e Fisica, Università del Salento, Lecce, Italy').
isinstitute('68a', 'INFN Sezione di Milano, Italy').
isinstitute('68b', 'Dipartimento di Fisica, Università di Milano, Milano, Italy').
isinstitute('69a', 'INFN Sezione di Napoli, Italy').
isinstitute('69b', 'Dipartimento di Fisica, Università di Napoli, Napoli, Italy').
isinstitute('70a', 'INFN Sezione di Pavia, Italy').
isinstitute('70b', 'Dipartimento di Fisica, Università di Pavia, Pavia, Italy').
isinstitute('71a', 'INFN Sezione di Pisa, Italy').
isinstitute('71b', 'Dipartimento di Fisica E. Fermi, Università di Pisa, Pisa, Italy').
isinstitute('72a', 'INFN Sezione di Roma, Italy').
isinstitute('72b', 'Dipartimento di Fisica, Sapienza Università di Roma, Roma, Italy').
isinstitute('73a', 'INFN Sezione di Roma Tor Vergata, Italy').
isinstitute('73b', 'Dipartimento di Fisica, Università di Roma Tor Vergata, Roma, Italy').
isinstitute('74a', 'INFN Sezione di Roma Tre, Italy').
isinstitute('74b', 'Dipartimento di Matematica e Fisica, Università Roma Tre, Roma, Italy').
isinstitute('75a', 'INFN-TIFPA, Italy').
isinstitute('75b', 'Università degli Studi di Trento, Trento, Italy').
isinstitute('76', 'Institut fòr Astro- und Teilchenphysik, Leopold-Franzens-Universität, Innsbruck; Austria').
isinstitute('77', 'University of Iowa, Iowa City IA; United States of America').
isinstitute('78', 'Department of Physics and Astronomy, Iowa State University, Ames IA; United States of America').
isinstitute('79', 'Joint Institute for Nuclear Research, Dubna; Russia').
isinstitute('81', 'KEK, High Energy Accelerator Research Organization, Tsukuba; Japan').
isinstitute('82', 'Graduate School of Science, Kobe University, Kobe; Japan').
isinstitute('84', 'Institute of Nuclear Physics Polish Academy of Sciences, Krakow; Poland').
isinstitute('85', 'Faculty of Science, Kyoto University, Kyoto; Japan').
isinstitute('86', 'Kyoto University of Education, Kyoto; Japan').
isinstitute('87', 'Research Center for Advanced Particle Physics and Department of Physics, Kyushu University, Fukuoka; Japan').
isinstitute('88', 'Instituto de Física La Plata, Universidad Nacional de La Plata and CONICET, La Plata; Argentina').
isinstitute('89', 'Physics Department, Lancaster University, Lancaster; United Kingdom').
isinstitute('90', 'Oliver Lodge Laboratory, University of Liverpool, Liverpool; United Kingdom').
isinstitute('92', 'School of Physics and Astronomy, Queen Mary University of London, London; United Kingdom').
isinstitute('93', 'Department of Physics, Royal Holloway University of London, Egham; United Kingdom').
isinstitute('94', 'Department of Physics and Astronomy, University College London, London; United Kingdom').
isinstitute('95', 'Louisiana Tech University, Ruston LA; United States of America').
isinstitute('96', 'Fysiska institutionen, Lunds universitet, Lund; Sweden').
isinstitute('98', 'Departamento de Física Teorica C-15 and CIAFF, Universidad Autónoma de Madrid, Madrid; Spain').
isinstitute('99', 'Institut fòr Physik, Universität Mainz, Mainz; Germany').
isinstitute('100', 'School of Physics and Astronomy, University of Manchester, Manchester; United Kingdom').
isinstitute('101', 'CPPM, Aix-Marseille Université, CNRS/IN2P3, Marseille; France').
isinstitute('102', 'Department of Physics, University of Massachusetts, Amherst MA; United States of America').
isinstitute('103', 'Department of Physics, McGill University, Montreal QC; Canada').
isinstitute('104', 'School of Physics, University of Melbourne, Victoria; Australia').
isinstitute('105', 'Department of Physics, University of Michigan, Ann Arbor MI; United States of America').
isinstitute('106', 'Department of Physics and Astronomy, Michigan State University, East Lansing MI; United States of America').
isinstitute('107', 'B.I. Stepanov Institute of Physics, National Academy of Sciences of Belarus').
isinstitute('108', 'Research Institute for Nuclear Problems of Byelorussian State University, Minsk; Belarus').
isinstitute('109', 'Group of Particle Physics, University of Montreal, Montreal QC; Canada').
isinstitute('110', 'P.N. Lebedev Physical Institute of the Russian Academy of Sciences, Moscow; Russia').
isinstitute('112', 'National Research Nuclear University MEPhI, Moscow; Russia').
isinstitute('113', 'D.V. Skobeltsyn Institute of Nuclear Physics, M.V. Lomonosov Moscow State University, Moscow; Russia').
isinstitute('114', 'Fakultät fòr Physik, Ludwig-Maximilians-Universität Mònchen, Mònchen; Germany').
isinstitute('115', 'Max-Planck-Institut fòr Physik (Werner-Heisenberg-Institut), Mònchen; Germany').
isinstitute('116', 'Nagasaki Institute of Applied Science, Nagasaki; Japan').
isinstitute('117', 'Graduate School of Science and Kobayashi-Maskawa Institute, Nagoya University, Nagoya; Japan').
isinstitute('118', 'Department of Physics and Astronomy, University of New Mexico, Albuquerque NM; United States of America').
isinstitute('120', 'Nikhef National Institute for Subatomic Physics and University of Amsterdam, Amsterdam; Netherlands').
isinstitute('121', 'Department of Physics, Northern Illinois University, DeKalb IL; United States of America').
isinstitute('123', 'Institute for High Energy Physics of the National Research Centre Kurchatov Institute, Protvino; Russia').
isinstitute('124', 'Department of Physics, New York University, New York NY; United States of America').
isinstitute('125', 'Ochanomizu University, Otsuka, Bunkyo-ku, Tokyo; Japan').
isinstitute('126', 'Ohio State University, Columbus OH; United States of America').
isinstitute('127', 'Faculty of Science, Okayama University, Okayama; Japan').
isinstitute('128', 'Homer L. Dodge Department of Physics and Astronomy, University of Oklahoma, Norman OK; United States of America').
isinstitute('129', 'Department of Physics, Oklahoma State University, Stillwater OK; United States of America').
isinstitute('130', 'Palacký University, RCPTM, Joint Laboratory of Optics, Olomouc; Czech Republic').
isinstitute('131', 'Center for High Energy Physics, University of Oregon, Eugene OR; United States of America').
isinstitute('132', 'LAL, Université Paris-Sud, CNRS/IN2P3, Université Paris-Saclay, Orsay; France').
isinstitute('133', 'Graduate School of Science, Osaka University, Osaka; Japan').
isinstitute('134', 'Department of Physics, University of Oslo, Oslo; Norway').
isinstitute('135', 'Department of Physics, Oxford University, Oxford; United Kingdom').
isinstitute('136', 'LPNHE, Sorbonne Université, Paris Diderot Sorbonne Paris Cité, CNRS/IN2P3, Paris; France').
isinstitute('137', 'Department of Physics, University of Pennsylvania, Philadelphia PA; United States of America').
isinstitute('139', 'Department of Physics and Astronomy, University of Pittsburgh, Pittsburgh PA; United States of America').
isinstitute('141', 'Institute of Physics of the Czech Academy of Sciences, Prague; Czech Republic').
isinstitute('142', 'Czech Technical University in Prague, Prague; Czech Republic').
isinstitute('143', 'Charles University, Faculty of Mathematics and Physics, Prague; Czech Republic').
isinstitute('144', 'Particle Physics Department, Rutherford Appleton Laboratory, Didcot; United Kingdom').
isinstitute('145', 'IRFU, CEA, Université Paris-Saclay, Gif-sur-Yvette; France').
isinstitute('146', 'Santa Cruz Institute for Particle Physics, University of California Santa Cruz, Santa Cruz CA; United States of America').
isinstitute('147a', 'Departamento de Física, Pontificia Universidad Católica de Chile').
isinstitute('148', 'Department of Physics, University of Washington, Seattle WA; United States of America').
isinstitute('149', 'Department of Physics and Astronomy, University of Sheffield, Sheffield; United Kingdom').
isinstitute('150', 'Department of Physics, Shinshu University, Nagano; Japan').
isinstitute('151', 'Department Physik, Universität Siegen, Siegen; Germany').
isinstitute('152', 'Department of Physics, Simon Fraser University, Burnaby BC; Canada').
isinstitute('153', 'SLAC National Accelerator Laboratory, Stanford CA; United States of America').
isinstitute('154', 'Physics Department, Royal Institute of Technology, Stockholm; Sweden').
isinstitute('155', 'Departments of Physics and Astronomy, Stony Brook University, Stony Brook NY; United States of America').
isinstitute('156', 'Department of Physics and Astronomy, University of Sussex, Brighton; United Kingdom').
isinstitute('157', 'School of Physics, University of Sydney, Sydney; Australia').
isinstitute('158', 'Institute of Physics, Academia Sinica, Taipei; Taiwan').
isinstitute('160', 'Department of Physics, Technion, Israel').
isinstitute('161', 'Raymond and Beverly Sackler School of Physics and Astronomy, Tel Aviv University, Tel Aviv; Israel').
isinstitute('162', 'Department of Physics, Aristotle University of Thessaloniki, Thessaloniki; Greece').
isinstitute('164', 'Graduate School of Science and Technology, Tokyo Metropolitan University, Tokyo; Japan').
isinstitute('165', 'Department of Physics, Tokyo Institute of Technology, Tokyo; Japan').
isinstitute('166', 'Tomsk State University, Tomsk; Russia').
isinstitute('167', 'Department of Physics, University of Toronto, Toronto ON; Canada').
isinstitute('168a', 'TRIUMF, Vancouver BC, Canada').
isinstitute('168b', 'Department of Physics and Astronomy, York University, Toronto ON, Canada').
isinstitute('170', 'Department of Physics and Astronomy, Tufts University, Medford MA; United States of America').
isinstitute('171', 'Department of Physics and Astronomy, University of California Irvine, Irvine CA; United States of America').
isinstitute('172', 'Department of Physics and Astronomy, University of Uppsala, Uppsala; Sweden').
isinstitute('173', 'Department of Physics, University of Illinois, Urbana IL; United States of America').
isinstitute('174', 'Instituto de Física Corpuscular (IFIC), Centro Mixto Universidad de Valencia - CSIC, Valencia; Spain').
isinstitute('175', 'Department of Physics, University of British Columbia, Vancouver BC; Canada').
isinstitute('176', 'Department of Physics and Astronomy, University of Victoria, Victoria BC; Canada').
isinstitute('177', 'Fakultät fòr Physik und Astronomie, Julius-Maximilians-Universität Wòrzburg, Wòrzburg; Germany').
isinstitute('178', 'Department of Physics, University of Warwick, Coventry; United Kingdom').
isinstitute('179', 'Waseda University, Tokyo; Japan').
isinstitute('180', 'Department of Particle Physics, Weizmann Institute of Science, Rehovot; Israel').
isinstitute('181', 'Department of Physics, University of Wisconsin, Madison WI; United States of America').
isinstitute('183', 'Department of Physics, Yale University, New Haven CT; United States of America').
isinstitute('184', 'Yerevan Physics Institute, Yerevan; Armenia').
isinstitute('4a', 'Department of Physics, Ankara University, Ankara, Turkey').
isinstitute('4b', 'Istanbul Aydin University, Istanbul, Turkey').
isinstitute('4c', 'Division of Physics, TOBB University of Economics and Technology, Ankara, Turkey').
isinstitute('20', 'Albert Einstein Center for Fundamental Physics and Laboratory for High Energy Physics, University of Bern, Bern; Switzerland').
isinstitute('28a', 'Faculty of Mathematics, Physics and Informatics, Comenius University, Bratislava, Slovak Republic').
isinstitute('28b', 'Department of Subnuclear Physics, Institute of Experimental Physics of the Slovak Academy of Sciences, Kosice, Slovak Republic').
isinstitute('41a', 'Dipartimento di Fisica, Università della Calabria, Rende, Italy').
isinstitute('41b', 'INFN Gruppo Collegato di Cosenza, Laboratori Nazionali di Frascati, Italy').
isinstitute('60a', 'Department of Modern Physics and State Key Laboratory of Particle Detection and Electronics, University of Science and Technology of China').
isinstitute('61a', 'Kirchhoff-Institut fòr Physik, Ruprecht-Karls-Universität Heidelberg, Heidelberg, Germany').
isinstitute('61b', 'Physikalisches Institut, Ruprecht-Karls-Universität Heidelberg, Heidelberg, Germany').
isinstitute('66a', 'INFN Gruppo Collegato di Udine, Sezione di Trieste, Udine, Italy').
isinstitute('66b', 'ICTP, Trieste, Italy').
isinstitute('66c', 'Dipartimento Politecnico di Ingegneria e Architettura, Università di Udine, Udine, Italy').
isinstitute('83a', 'AGH University of Science and Technology, Faculty of Physics and Applied Computer Science, Krakow, Poland').
isinstitute('83b', 'Marian Smoluchowski Institute of Physics, Jagiellonian University, Krakow, Poland').
isinstitute('91', 'Department of Experimental Particle Physics, Jožef Stefan Institute and Department of Physics, University of Ljubljana, Ljubljana; Slovenia').
isinstitute('97', 'Centre de Calcul de l\'Institut National de Physique Nucléaire et de Physique des Particules (IN2P3), Villeurbanne; France').
isinstitute('111', 'Institute for Theoretical and Experimental Physics of the National Research Centre Kurchatov Institute, Moscow; Russia').
isinstitute('119', 'Institute for Mathematics, Astrophysics and Particle Physics, Radboud University Nijmegen/Nikhef, Nijmegen; Netherlands').
isinstitute('122a', 'Budker Institute of Nuclear Physics and NSU, SB RAS, Novosibirsk, Russia').
isinstitute('122b', 'Novosibirsk State University Novosibirsk, Russia').
isinstitute('138', 'Konstantinov Nuclear Physics Institute of National Research Centre “Kurchatov Institute”, PNPI, St. Petersburg; Russia').
isinstitute('159a', 'E. Andronikashvili Institute of Physics, Iv. Javakhishvili Tbilisi State University, Tbilisi, Georgia').
isinstitute('159b', 'High Energy Physics Institute, Tbilisi State University, Tbilisi, Georgia').
isinstitute('163', 'International Center for Elementary Particle Physics and Department of Physics, University of Tokyo, Tokyo; Japan').
isinstitute('169', 'Division of Physics and Tomonaga Center for the History of the Universe, Faculty of Pure and Applied Sciences, University of Tsukuba, Tsukuba; Japan').
isinstitute('182', 'Fakultät fòr Mathematik und Naturwissenschaften, Fachgruppe Physik, Bergische Universität Wuppertal, Wuppertal; Germany').
isinstitute('12a', 'Bahcesehir University, Faculty of Engineering and Natural Sciences, Istanbul, Turkey').
isinstitute('12b', 'Istanbul Bilgi University, Faculty of Engineering and Natural Sciences, Istanbul, Turkey').
isinstitute('12c', 'Department of Physics, Bogazici University, Istanbul, Turkey').
isinstitute('12d', 'Department of Physics Engineering, Gaziantep University, Gaziantep, Turkey').
isinstitute('15a', 'Institute of High Energy Physics, Chinese Academy of Sciences, Beijing, China').
isinstitute('15b', 'Physics Department, Tsinghua University, Beijing, China').
isinstitute('15c', 'Department of Physics, Nanjing University, Nanjing, China').
isinstitute('15d', 'University of Chinese Academy of Science (UCAS), Beijing, China').
isinstitute('33a', 'Department of Physics, University of Cape Town, Cape Town, South Africa').
isinstitute('33b', 'Department of Mechanical Engineering Science, University of Johannesburg, Johannesburg, South Africa').
isinstitute('33c', 'School of Physics, University of the Witwatersrand, Johannesburg, South Africa').
isinstitute('63a', 'Department of Physics, Chinese University of Hong Kong, Shatin, N.T., Hong Kong, China').
isinstitute('63b', 'Department of Physics, University of Hong Kong, Hong Kong, China').
isinstitute('63c', 'Department of Physics and Institute for Advanced Study, Hong Kong University of Science and Technology, Clear Water Bay, Kowloon, Hong Kong, China').
isinstitute('80a', 'Departamento de Engenharia Elétrica, Universidade Federal de Juiz de Fora (UFJF), Juiz de Fora, Brazil').
isinstitute('80b', 'Universidade Federal do Rio De Janeiro COPPE/EE/IF, Rio de Janeiro, Brazil').
isinstitute('80c', 'Universidade Federal de São João del Rei (UFSJ), São João del Rei, Brazil').
isinstitute('80d', 'Instituto de Física, Universidade de São Paulo, São Paulo, Brazil').
isinstitute('35a', 'Faculté des Sciences Ain Chock, Réseau Universitaire de Physique des Hautes Energies - Université Hassan II, Casablanca, Morocco').
isinstitute('35b', 'Faculté des Sciences, Université Ibn-Tofail, Kénitra, Morocco').
isinstitute('35c', 'Faculté des Sciences Semlalia, Université Cadi Ayyad, LPHEA-Marrakech, Morocco').
isinstitute('35d', 'Faculté des Sciences, Université Mohamed Premier and LPTPM, Oujda, Morocco').
isinstitute('35e', 'Faculté des sciences, Université Mohammed V, Rabat, Morocco').
isinstitute('140a', 'Laboratório de Instrumentação e Física Experimental de Partículas - LIP, Spain').
isinstitute('140b', 'Departamento de Física, Faculdade de Ciências, Universidade de Lisboa, Lisboa, Spain').
isinstitute('140c', 'Departamento de Física, Universidade de Coimbra, Coimbra, Spain').
isinstitute('140d', 'Centro de Física Nuclear da Universidade de Lisboa, Lisboa, Spain').
isinstitute('140e', 'Departamento de Física, Universidade do Minho, Braga, Spain').
isinstitute('27a', 'Transilvania University of Brasov, Brasov, Romania').
isinstitute('27b', 'Horia Hulubei National Institute of Physics and Nuclear Engineering, Bucharest, Romania').
isinstitute('27c', 'Department of Physics, Alexandru Ioan Cuza University of Iasi, Iasi, Romania').
isinstitute('27d', 'National Institute for Research and Development of Isotopic and Molecular Technologies, Physics Department, Cluj-Napoca, Romania').
isinstitute('27e', 'University Politehnica Bucharest, Bucharest, Romania').
isinstitute('020b', 'Institute of Frontier and Interdisciplinary Science and Key Laboratory of Particle Physics and Particle Irradiation (MOE), Shandong University, Qingdao, China').
isinstitute('020c', 'School of Physics and Astronomy, Shanghai Jiao Tong University, KLPPAC-MoE, SKLPPC, Shanghai, China').
isinstitute('020d', 'Tsung-Dao Lee Institute, Shanghai, China').
belongsto('M. Aaboud', '35d').
belongsto('G. Aad', '101').
belongsto('B. Abbott', '128').
belongsto('D.C. Abbott', '102').
belongsto('O. Abdinov', '13').
alsoat('O. Abdinov', '*').
belongsto('D.K. Abhayasinghe', '93').
belongsto('S.H. Abidi', '167').
belongsto('O.S. AbouZeid', '40').
belongsto('N.L. Abraham', '156').
belongsto('H. Abramowicz', '161').
belongsto('H. Abreu', '160').
belongsto('Y. Abulaiti', '6').
belongsto('B.S. Acharya', '66a').
alsoat('B.S. Acharya', '66b').
alsoat('B.S. Acharya', 'o').
belongsto('S. Adachi', '163').
belongsto('L. Adam', '99').
belongsto('C. Adam Bourdarios', '132').
belongsto('L. Adamczyk', '83a').
belongsto('L. Adamek', '167').
belongsto('J. Adelman', '121').
belongsto('M. Adersberger', '114').
belongsto('A. Adiguzel', '12c').
alsoat('A. Adiguzel', 'ai').
belongsto('T. Adye', '144').
belongsto('A.A. Affolder', '146').
belongsto('Y. Afik', '160').
belongsto('C. Agapopoulou', '132').
belongsto('M.N. Agaras', '38').
belongsto('A. Aggarwal', '119').
belongsto('C. Agheorghiesei', '27c').
belongsto('J.A. Aguilar-Saavedra', '140f').
alsoat('J.A. Aguilar-Saavedra', '140a').
alsoat('J.A. Aguilar-Saavedra', 'ah').
belongsto('F. Ahmadov', '79').
belongsto('G. Aielli', '73a').
alsoat('G. Aielli', '73b').
belongsto('S. Akatsuka', '85').
belongsto('T.P.A. Åkesson', '96').
belongsto('E. Akilli', '54').
belongsto('A.V. Akimov', '110').
belongsto('K. Al Khoury', '132').
belongsto('G.L. Alberghi', '23b').
alsoat('G.L. Alberghi', '23a').
belongsto('J. Albert', '176').
belongsto('M.J. Alconada Verzini', '88').
belongsto('S. Alderweireldt', '119').
belongsto('M. Aleksa', '36').
belongsto('I.N. Aleksandrov', '79').
belongsto('C. Alexa', '27b').
belongsto('D. Alexandre', '19').
belongsto('T. Alexopoulos', '10').
belongsto('M. Alhroob', '128').
belongsto('B. Ali', '142').
belongsto('G. Alimonti', '68a').
belongsto('J. Alison', '37').
belongsto('S.P. Alkire', '148').
belongsto('C. Allaire', '132').
belongsto('B.M.M. Allbrooke', '156').
belongsto('B.W. Allen', '131').
belongsto('P.P. Allport', '21').
belongsto('A. Aloisio', '69a').
alsoat('A. Aloisio', '69b').
belongsto('A. Alonso', '40').
belongsto('F. Alonso', '88').
belongsto('C. Alpigiani', '148').
belongsto('A.A. Alshehri', '57').
belongsto('M.I. Alstaty', '101').
belongsto('M. Alvarez Estevez', '98').
belongsto('B. Alvarez Gonzalez', '36').
belongsto('D. Álvarez Piqueras', '174').
belongsto('M.G. Alviggi', '69a').
alsoat('M.G. Alviggi', '69b').
belongsto('Y. Amaral Coutinho', '80b').
belongsto('A. Ambler', '103').
belongsto('L. Ambroz', '135').
belongsto('C. Amelung', '26').
belongsto('D. Amidei', '105').
belongsto('S.P. Amor Dos Santos', '140a').
alsoat('S.P. Amor Dos Santos', '140c').
belongsto('S. Amoroso', '46').
belongsto('C.S. Amrouche', '54').
belongsto('F. An', '78').
belongsto('C. Anastopoulos', '149').
belongsto('N. Andari', '145').
belongsto('T. Andeen', '11').
belongsto('C.F. Anders', '61b').
belongsto('J.K. Anders', '20').
belongsto('A. Andreazza', '68a').
alsoat('A. Andreazza', '68b').
belongsto('V. Andrei', '61a').
belongsto('C.R. Anelli', '176').
belongsto('S. Angelidakis', '38').
belongsto('I. Angelozzi', '120').
belongsto('A. Angerami', '39').
belongsto('A.V. Anisenkov', '122b').
alsoat('A.V. Anisenkov', '122a').
belongsto('A. Annovi', '71a').
belongsto('C. Antel', '61a').
belongsto('M.T. Anthony', '149').
belongsto('M. Antonelli', '51').
belongsto('D.J.A. Antrim', '171').
belongsto('F. Anulli', '72a').
belongsto('M. Aoki', '81').
belongsto('J.A. Aparisi Pozo', '174').
belongsto('L. Aperio Bella', '36').
belongsto('G. Arabidze', '106').
belongsto('J.P. Araque', '140a').
belongsto('V. Araujo Ferraz', '80b').
belongsto('R. Araujo Pereira', '80b').
belongsto('A.T.H. Arce', '49').
belongsto('F.A. Arduh', '88').
belongsto('J-F. Arguin', '109').
belongsto('S. Argyropoulos', '77').
belongsto('J.-H. Arling', '46').
belongsto('A.J. Armbruster', '36').
belongsto('L.J. Armitage', '92').
belongsto('A. Armstrong', '171').
belongsto('O. Arnaez', '167').
belongsto('H. Arnold', '120').
belongsto('A. Artamonov', '111').
alsoat('A. Artamonov', '*').
belongsto('G. Artoni', '135').
belongsto('S. Artz', '99').
belongsto('S. Asai', '163').
belongsto('N. Asbah', '59').
belongsto('E.M. Asimakopoulou', '172').
belongsto('L. Asquith', '156').
belongsto('K. Assamagan', '29').
belongsto('R. Astalos', '28a').
belongsto('R.J. Atkin', '33a').
belongsto('M. Atkinson', '173').
belongsto('N.B. Atlay', '151').
belongsto('K. Augsten', '142').
belongsto('G. Avolio', '36').
belongsto('R. Avramidou', '60a').
belongsto('M.K. Ayoub', '15a').
belongsto('A.M. Azoulay', '168b').
belongsto('G. Azuelos', '109').
alsoat('G. Azuelos', 'aw').
belongsto('A.E. Baas', '61a').
belongsto('M.J. Baca', '21').
belongsto('H. Bachacou', '145').
belongsto('K. Bachas', '67a').
alsoat('K. Bachas', '67b').
belongsto('M. Backes', '135').
belongsto('F. Backman', '45a').
alsoat('F. Backman', '45b').
belongsto('P. Bagnaia', '72a').
alsoat('P. Bagnaia', '72b').
belongsto('M. Bahmani', '84').
belongsto('H. Bahrasemani', '152').
belongsto('A.J. Bailey', '174').
belongsto('V.R. Bailey', '173').
belongsto('J.T. Baines', '144').
belongsto('M. Bajic', '40').
belongsto('C. Bakalis', '10').
belongsto('O.K. Baker', '183').
belongsto('P.J. Bakker', '120').
belongsto('D. Bakshi Gupta', '8').
belongsto('S. Balaji', '157').
belongsto('E.M. Baldin', '122b').
alsoat('E.M. Baldin', '122a').
belongsto('P. Balek', '180').
belongsto('F. Balli', '145').
belongsto('W.K. Balunas', '135').
belongsto('J. Balz', '99').
belongsto('E. Banas', '84').
belongsto('A. Bandyopadhyay', '24').
belongsto('Sw. Banerjee', '181').
alsoat('Sw. Banerjee', 'j').
belongsto('A.A.E. Bannoura', '182').
belongsto('L. Barak', '161').
belongsto('W.M. Barbe', '38').
belongsto('E.L. Barberio', '104').
belongsto('D. Barberis', '55b').
alsoat('D. Barberis', '55a').
belongsto('M. Barbero', '101').
belongsto('T. Barillari', '115').
belongsto('M-S. Barisits', '36').
belongsto('J. Barkeloo', '131').
belongsto('T. Barklow', '153').
belongsto('R. Barnea', '160').
belongsto('S.L. Barnes', '60c').
belongsto('B.M. Barnett', '144').
belongsto('R.M. Barnett', '18').
belongsto('Z. Barnovska-Blenessy', '60a').
belongsto('A. Baroncelli', '60a').
belongsto('G. Barone', '29').
belongsto('A.J. Barr', '135').
belongsto('L. Barranco Navarro', '174').
belongsto('F. Barreiro', '98').
belongsto('J. Barreiro Guimarães da Costa', '15a').
belongsto('R. Bartoldus', '153').
belongsto('G. Bartolini', '101').
belongsto('A.E. Barton', '89').
belongsto('P. Bartos', '28a').
belongsto('A. Basalaev', '46').
belongsto('A. Bassalat', '132').
alsoat('A. Bassalat', 'aq').
belongsto('R.L. Bates', '57').
belongsto('S.J. Batista', '167').
belongsto('S. Batlamous', '35e').
belongsto('J.R. Batley', '32').
belongsto('M. Battaglia', '146').
belongsto('M. Bauce', '72a').
alsoat('M. Bauce', '72b').
belongsto('F. Bauer', '145').
belongsto('K.T. Bauer', '171').
belongsto('H.S. Bawa', '31').
alsoat('H.S. Bawa', 'm').
belongsto('J.B. Beacham', '49').
belongsto('T. Beau', '136').
belongsto('P.H. Beauchemin', '170').
belongsto('P. Bechtle', '24').
belongsto('H.C. Beck', '53').
belongsto('H.P. Beck', '20').
alsoat('H.P. Beck', 'r').
belongsto('K. Becker', '52').
belongsto('M. Becker', '99').
belongsto('C. Becot', '46').
belongsto('A. Beddall', '12d').
belongsto('A.J. Beddall', '12a').
belongsto('V.A. Bednyakov', '79').
belongsto('M. Bedognetti', '120').
belongsto('C.P. Bee', '155').
belongsto('T.A. Beermann', '76').
belongsto('M. Begalli', '80b').
belongsto('M. Begel', '29').
belongsto('A. Behera', '155').
belongsto('J.K. Behr', '46').
belongsto('F. Beisiegel', '24').
belongsto('A.S. Bell', '94').
belongsto('G. Bella', '161').
belongsto('L. Bellagamba', '23b').
belongsto('A. Bellerive', '34').
belongsto('P. Bellos', '9').
belongsto('K. Beloborodov', '122b').
alsoat('K. Beloborodov', '122a').
belongsto('K. Belotskiy', '112').
belongsto('N.L. Belyaev', '112').
belongsto('O. Benary', '161').
alsoat('O. Benary', '*').
belongsto('D. Benchekroun', '35a').
belongsto('N. Benekos', '10').
belongsto('Y. Benhammou', '161').
belongsto('D.P. Benjamin', '6').
belongsto('M. Benoit', '54').
belongsto('J.R. Bensinger', '26').
belongsto('S. Bentvelsen', '120').
belongsto('L. Beresford', '135').
belongsto('M. Beretta', '51').
belongsto('D. Berge', '46').
belongsto('E. Bergeaas Kuutmann', '172').
belongsto('N. Berger', '5').
belongsto('B. Bergmann', '142').
belongsto('L.J. Bergsten', '26').
belongsto('J. Beringer', '18').
belongsto('S. Berlendis', '7').
belongsto('N.R. Bernard', '102').
belongsto('G. Bernardi', '136').
belongsto('C. Bernius', '153').
belongsto('F.U. Bernlochner', '24').
belongsto('T. Berry', '93').
belongsto('P. Berta', '99').
belongsto('C. Bertella', '15a').
belongsto('G. Bertoli', '45a').
alsoat('G. Bertoli', '45b').
belongsto('I.A. Bertram', '89').
belongsto('G.J. Besjes', '40').
belongsto('O. Bessidskaia Bylund', '182').
belongsto('N. Besson', '145').
belongsto('A. Bethani', '100').
belongsto('S. Bethke', '115').
belongsto('A. Betti', '24').
belongsto('A.J. Bevan', '92').
belongsto('J. Beyer', '115').
belongsto('R. Bi', '139').
belongsto('R.M. Bianchi', '139').
belongsto('O. Biebel', '114').
belongsto('D. Biedermann', '19').
belongsto('R. Bielski', '36').
belongsto('K. Bierwagen', '99').
belongsto('N.V. Biesuz', '71a').
alsoat('N.V. Biesuz', '71b').
belongsto('M. Biglietti', '74a').
belongsto('T.R.V. Billoud', '109').
belongsto('M. Bindi', '53').
belongsto('A. Bingul', '12d').
belongsto('C. Bini', '72a').
alsoat('C. Bini', '72b').
belongsto('S. Biondi', '23b').
alsoat('S. Biondi', '23a').
belongsto('M. Birman', '180').
belongsto('T. Bisanz', '53').
belongsto('J.P. Biswal', '161').
belongsto('A. Bitadze', '100').
belongsto('C. Bittrich', '48').
belongsto('D.M. Bjergaard', '49').
belongsto('J.E. Black', '153').
belongsto('K.M. Black', '25').
belongsto('T. Blazek', '28a').
belongsto('I. Bloch', '46').
belongsto('C. Blocker', '26').
belongsto('A. Blue', '57').
belongsto('U. Blumenschein', '92').
belongsto('S. Blunier', '147a').
belongsto('G.J. Bobbink', '120').
belongsto('V.S. Bobrovnikov', '122b').
alsoat('V.S. Bobrovnikov', '122a').
belongsto('S.S. Bocchetta', '96').
belongsto('A. Bocci', '49').
belongsto('D. Boerner', '46').
belongsto('D. Bogavac', '114').
belongsto('A.G. Bogdanchikov', '122b').
alsoat('A.G. Bogdanchikov', '122a').
belongsto('C. Bohm', '45a').
belongsto('V. Boisvert', '93').
belongsto('P. Bokan', '53').
alsoat('P. Bokan', '172').
belongsto('T. Bold', '83a').
belongsto('A.S. Boldyrev', '113').
belongsto('A.E. Bolz', '61b').
belongsto('M. Bomben', '136').
belongsto('M. Bona', '92').
belongsto('J.S. Bonilla', '131').
belongsto('M. Boonekamp', '145').
belongsto('H.M. Borecka-Bielska', '90').
belongsto('A. Borisov', '123').
belongsto('G. Borissov', '89').
belongsto('J. Bortfeldt', '36').
belongsto('D. Bortoletto', '135').
belongsto('V. Bortolotto', '73a').
alsoat('V. Bortolotto', '73b').
belongsto('D. Boscherini', '23b').
belongsto('M. Bosman', '14').
belongsto('J.D. Bossio Sola', '30').
belongsto('K. Bouaouda', '35a').
belongsto('J. Boudreau', '139').
belongsto('E.V. Bouhova-Thacker', '89').
belongsto('D. Boumediene', '38').
belongsto('S.K. Boutle', '57').
belongsto('A. Boveia', '126').
belongsto('J. Boyd', '36').
belongsto('D. Boye', '33b').
belongsto('I.R. Boyko', '79').
belongsto('A.J. Bozson', '93').
belongsto('J. Bracinik', '21').
belongsto('N. Brahimi', '101').
belongsto('G. Brandt', '182').
belongsto('O. Brandt', '61a').
belongsto('F. Braren', '46').
belongsto('U. Bratzler', '164').
belongsto('B. Brau', '102').
belongsto('J.E. Brau', '131').
belongsto('W.D. Breaden Madden', '57').
belongsto('K. Brendlinger', '46').
belongsto('L. Brenner', '46').
belongsto('R. Brenner', '172').
belongsto('S. Bressler', '180').
belongsto('B. Brickwedde', '99').
belongsto('D.L. Briglin', '21').
belongsto('D. Britton', '57').
belongsto('D. Britzger', '115').
belongsto('I. Brock', '24').
belongsto('R. Brock', '106').
belongsto('G. Brooijmans', '39').
belongsto('T. Brooks', '93').
belongsto('W.K. Brooks', '147b').
belongsto('E. Brost', '121').
belongsto('J.H Broughton', '21').
belongsto('P.A. Bruckman de Renstrom', '84').
belongsto('D. Bruncko', '28b').
belongsto('A. Bruni', '23b').
belongsto('G. Bruni', '23b').
belongsto('L.S. Bruni', '120').
belongsto('S. Bruno', '73a').
alsoat('S. Bruno', '73b').
belongsto('B.H. Brunt', '32').
belongsto('M. Bruschi', '23b').
belongsto('N. Bruscino', '139').
belongsto('P. Bryant', '37').
belongsto('L. Bryngemark', '96').
belongsto('T. Buanes', '17').
belongsto('Q. Buat', '36').
belongsto('P. Buchholz', '151').
belongsto('A.G. Buckley', '57').
belongsto('I.A. Budagov', '79').
belongsto('M.K. Bugge', '134').
belongsto('F. Bòhrer', '52').
belongsto('O. Bulekov', '112').
belongsto('T.J. Burch', '121').
belongsto('S. Burdin', '90').
belongsto('C.D. Burgard', '120').
belongsto('A.M. Burger', '129').
belongsto('B. Burghgrave', '8').
belongsto('K. Burka', '84').
belongsto('I. Burmeister', '47').
belongsto('J.T.P. Burr', '46').
belongsto('V. Bòscher', '99').
belongsto('E. Buschmann', '53').
belongsto('P.J. Bussey', '57').
belongsto('J.M. Butler', '25').
belongsto('C.M. Buttar', '57').
belongsto('J.M. Butterworth', '94').
belongsto('P. Butti', '36').
belongsto('W. Buttinger', '36').
belongsto('A. Buzatu', '158').
belongsto('A.R. Buzykaev', '122b').
alsoat('A.R. Buzykaev', '122a').
belongsto('G. Cabras', '23b').
alsoat('G. Cabras', '23a').
belongsto('S. Cabrera Urbán', '174').
belongsto('D. Caforio', '142').
belongsto('H. Cai', '173').
belongsto('V.M.M. Cairo', '2').
belongsto('O. Cakir', '4a').
belongsto('N. Calace', '36').
belongsto('P. Calafiura', '18').
belongsto('A. Calandri', '101').
belongsto('G. Calderini', '136').
belongsto('P. Calfayan', '65').
belongsto('G. Callea', '57').
belongsto('L.P. Caloba', '80b').
belongsto('S. Calvente Lopez', '98').
belongsto('D. Calvet', '38').
belongsto('S. Calvet', '38').
belongsto('T.P. Calvet', '155').
belongsto('M. Calvetti', '71a').
alsoat('M. Calvetti', '71b').
belongsto('R. Camacho Toro', '136').
belongsto('S. Camarda', '36').
belongsto('D. Camarero Munoz', '98').
belongsto('P. Camarri', '73a').
alsoat('P. Camarri', '73b').
belongsto('D. Cameron', '134').
belongsto('R. Caminal Armadans', '102').
belongsto('C. Camincher', '36').
belongsto('S. Campana', '36').
belongsto('M. Campanelli', '94').
belongsto('A. Camplani', '40').
belongsto('A. Campoverde', '151').
belongsto('V. Canale', '69a').
alsoat('V. Canale', '69b').
belongsto('A. Canesse', '103').
belongsto('M. Cano Bret', '60c').
belongsto('J. Cantero', '129').
belongsto('T. Cao', '161').
belongsto('Y. Cao', '173').
belongsto('M.D.M. Capeans Garrido', '36').
belongsto('M. Capua', '41b').
alsoat('M. Capua', '41a').
belongsto('R. Cardarelli', '73a').
belongsto('F.C. Cardillo', '149').
belongsto('I. Carli', '143').
belongsto('T. Carli', '36').
belongsto('G. Carlino', '69a').
belongsto('B.T. Carlson', '139').
belongsto('L. Carminati', '68a').
alsoat('L. Carminati', '68b').
belongsto('R.M.D. Carney', '45a').
alsoat('R.M.D. Carney', '45b').
belongsto('S. Caron', '119').
belongsto('E. Carquin', '147b').
belongsto('S. Carrá', '68a').
alsoat('S. Carrá', '68b').
belongsto('J.W.S. Carter', '167').
belongsto('M.P. Casado', '14').
alsoat('M.P. Casado', 'f').
belongsto('A.F. Casha', '167').
belongsto('D.W. Casper', '171').
belongsto('R. Castelijn', '120').
belongsto('F.L. Castillo', '174').
belongsto('V. Castillo Gimenez', '174').
belongsto('N.F. Castro', '140a').
alsoat('N.F. Castro', '140e').
belongsto('A. Catinaccio', '36').
belongsto('J.R. Catmore', '134').
belongsto('A. Cattai', '36').
belongsto('J. Caudron', '24').
belongsto('V. Cavaliere', '29').
belongsto('E. Cavallaro', '14').
belongsto('D. Cavalli', '68a').
belongsto('M. Cavalli-Sforza', '14').
belongsto('V. Cavasinni', '71a').
alsoat('V. Cavasinni', '71b').
belongsto('E. Celebi', '12b').
belongsto('F. Ceradini', '74a').
alsoat('F. Ceradini', '74b').
belongsto('L. Cerda Alberich', '174').
belongsto('A.S. Cerqueira', '80a').
belongsto('A. Cerri', '156').
belongsto('L. Cerrito', '73a').
alsoat('L. Cerrito', '73b').
belongsto('F. Cerutti', '18').
belongsto('A. Cervelli', '23b').
alsoat('A. Cervelli', '23a').
belongsto('S.A. Cetin', '12b').
belongsto('A. Chafaq', '35a').
belongsto('D. Chakraborty', '121').
belongsto('S.K. Chan', '59').
belongsto('W.S. Chan', '120').
belongsto('W.Y. Chan', '90').
belongsto('J.D. Chapman', '32').
belongsto('B. Chargeishvili', '159b').
belongsto('D.G. Charlton', '21').
belongsto('C.C. Chau', '34').
belongsto('C.A. Chavez Barajas', '156').
belongsto('S. Che', '126').
belongsto('A. Chegwidden', '106').
belongsto('S. Chekanov', '6').
belongsto('S.V. Chekulaev', '168a').
belongsto('G.A. Chelkov', '79').
alsoat('G.A. Chelkov', 'av').
belongsto('M.A. Chelstowska', '36').
belongsto('B. Chen', '78').
belongsto('C. Chen', '60a').
belongsto('C.H. Chen', '78').
belongsto('H. Chen', '29').
belongsto('J. Chen', '60a').
belongsto('J. Chen', '39').
belongsto('S. Chen', '137').
belongsto('S.J. Chen', '15c').
belongsto('X. Chen', '15b').
alsoat('X. Chen', 'au').
belongsto('Y. Chen', '82').
belongsto('Y-H. Chen', '46').
belongsto('H.C. Cheng', '63a').
belongsto('H.J. Cheng', '15a').
alsoat('H.J. Cheng', '15d').
belongsto('A. Cheplakov', '79').
belongsto('E. Cheremushkina', '123').
belongsto('R. Cherkaoui El Moursli', '35e').
belongsto('E. Cheu', '7').
belongsto('K. Cheung', '64').
belongsto('T.J.A. Chevalérias', '145').
belongsto('L. Chevalier', '145').
belongsto('V. Chiarella', '51').
belongsto('G. Chiarelli', '71a').
belongsto('G. Chiodini', '67a').
belongsto('A.S. Chisholm', '36').
alsoat('A.S. Chisholm', '21').
belongsto('A. Chitan', '27b').
belongsto('I. Chiu', '163').
belongsto('Y.H. Chiu', '176').
belongsto('M.V. Chizhov', '79').
belongsto('K. Choi', '65').
belongsto('A.R. Chomont', '132').
belongsto('S. Chouridou', '162').
belongsto('Y.S. Chow', '120').
belongsto('M.C. Chu', '63a').
belongsto('J. Chudoba', '141').
belongsto('A.J. Chuinard', '103').
belongsto('J.J. Chwastowski', '84').
belongsto('L. Chytka', '130').
belongsto('D. Cinca', '47').
belongsto('V. Cindro', '91').
belongsto('I.A. Cioară', '27b').
belongsto('A. Ciocio', '18').
belongsto('F. Cirotto', '69a').
alsoat('F. Cirotto', '69b').
belongsto('Z.H. Citron', '180').
belongsto('M. Citterio', '68a').
belongsto('B.M. Ciungu', '167').
belongsto('A. Clark', '54').
belongsto('M.R. Clark', '39').
belongsto('P.J. Clark', '50').
belongsto('C. Clement', '45a').
alsoat('C. Clement', '45b').
belongsto('Y. Coadou', '101').
belongsto('M. Cobal', '66a').
alsoat('M. Cobal', '66c').
belongsto('A. Coccaro', '55b').
belongsto('J. Cochran', '78').
belongsto('H. Cohen', '161').
belongsto('A.E.C. Coimbra', '180').
belongsto('L. Colasurdo', '119').
belongsto('B. Cole', '39').
belongsto('A.P. Colijn', '120').
belongsto('J. Collot', '58').
belongsto('P. Conde Muiño', '140a').
alsoat('P. Conde Muiño', 'g').
belongsto('E. Coniavitis', '52').
belongsto('S.H. Connell', '33b').
belongsto('I.A. Connelly', '57').
belongsto('S. Constantinescu', '27b').
belongsto('F. Conventi', '69a').
alsoat('F. Conventi', 'ax').
belongsto('A.M. Cooper-Sarkar', '135').
belongsto('F. Cormier', '175').
belongsto('K.J.R. Cormier', '167').
belongsto('L.D. Corpe', '94').
belongsto('M. Corradi', '72a').
alsoat('M. Corradi', '72b').
belongsto('E.E. Corrigan', '96').
belongsto('F. Corriveau', '103').
alsoat('F. Corriveau', 'ad').
belongsto('A. Cortes-Gonzalez', '36').
belongsto('M.J. Costa', '174').
belongsto('F. Costanza', '5').
belongsto('D. Costanzo', '149').
belongsto('G. Cowan', '93').
belongsto('J.W. Cowley', '32').
belongsto('J. Crane', '100').
belongsto('K. Cranmer', '124').
belongsto('S.J. Crawley', '57').
belongsto('R.A. Creager', '137').
belongsto('S. Crépé-Renaudin', '58').
belongsto('F. Crescioli', '136').
belongsto('M. Cristinziani', '24').
belongsto('V. Croft', '120').
belongsto('G. Crosetti', '41b').
alsoat('G. Crosetti', '41a').
belongsto('A. Cueto', '98').
belongsto('T. Cuhadar Donszelmann', '149').
belongsto('A.R. Cukierman', '153').
belongsto('S. Czekierda', '84').
belongsto('P. Czodrowski', '36').
belongsto('M.J. Da Cunha Sargedas De Sousa', '60b').
belongsto('J.V. Da Fonseca Pinto', '80b').
belongsto('C. Da Via', '100').
belongsto('W. Dabrowski', '83a').
belongsto('T. Dado', '28a').
belongsto('S. Dahbi', '35e').
belongsto('T. Dai', '105').
belongsto('C. Dallapiccola', '102').
belongsto('M. Dam', '40').
belongsto('G. D\'amen', '23b').
alsoat('G. D\'amen', '23a').
belongsto('J. Damp', '99').
belongsto('J.R. Dandoy', '137').
belongsto('M.F. Daneri', '30').
belongsto('N.P. Dang', '181').
belongsto('N.D Dann', '100').
belongsto('M. Danninger', '175').
belongsto('V. Dao', '36').
belongsto('G. Darbo', '55b').
belongsto('O. Dartsi', '5').
belongsto('A. Dattagupta', '131').
belongsto('T. Daubney', '46').
belongsto('S. D\'Auria', '68a').
alsoat('S. D\'Auria', '68b').
belongsto('W. Davey', '24').
belongsto('C. David', '46').
belongsto('T. Davidek', '143').
belongsto('D.R. Davis', '49').
belongsto('E. Dawe', '104').
belongsto('I. Dawson', '149').
belongsto('K. De', '8').
belongsto('R. De Asmundis', '69a').
belongsto('A. De Benedetti', '128').
belongsto('M. De Beurs', '120').
belongsto('S. De Castro', '23b').
alsoat('S. De Castro', '23a').
belongsto('S. De Cecco', '72a').
alsoat('S. De Cecco', '72b').
belongsto('N. De Groot', '119').
belongsto('P. de Jong', '120').
belongsto('H. De la Torre', '106').
belongsto('A. De Maria', '71a').
alsoat('A. De Maria', '71b').
belongsto('D. De Pedis', '72a').
belongsto('A. De Salvo', '72a').
belongsto('U. De Sanctis', '73a').
alsoat('U. De Sanctis', '73b').
belongsto('M. De Santis', '73a').
alsoat('M. De Santis', '73b').
belongsto('A. De Santo', '156').
belongsto('K. De Vasconcelos Corga', '101').
belongsto('J.B. De Vivie De Regie', '132').
belongsto('C. Debenedetti', '146').
belongsto('D.V. Dedovich', '79').
belongsto('A.M. Deiana', '42').
belongsto('M. Del Gaudio', '41b').
alsoat('M. Del Gaudio', '41a').
belongsto('J. Del Peso', '98').
belongsto('Y. Delabat Diaz', '46').
belongsto('D. Delgove', '132').
belongsto('F. Deliot', '145').
belongsto('C.M. Delitzsch', '7').
belongsto('M. Della Pietra', '69a').
alsoat('M. Della Pietra', '69b').
belongsto('D. Della Volpe', '54').
belongsto('A. Dell\'Acqua', '36').
belongsto('L. Dell\'Asta', '25').
belongsto('M. Delmastro', '5').
belongsto('C. Delporte', '132').
belongsto('P.A. Delsart', '58').
belongsto('D.A. DeMarco', '167').
belongsto('S. Demers', '183').
belongsto('M. Demichev', '79').
belongsto('S.P. Denisov', '123').
belongsto('D. Denysiuk', '120').
belongsto('L. D\'Eramo', '136').
belongsto('D. Derendarz', '84').
belongsto('J.E. Derkaoui', '35d').
belongsto('F. Derue', '136').
belongsto('P. Dervan', '90').
belongsto('K. Desch', '24').
belongsto('C. Deterre', '46').
belongsto('K. Dette', '167').
belongsto('M.R. Devesa', '30').
belongsto('P.O. Deviveiros', '36').
belongsto('A. Dewhurst', '144').
belongsto('S. Dhaliwal', '26').
belongsto('F.A. Di Bello', '54').
belongsto('A. Di Ciaccio', '73a').
alsoat('A. Di Ciaccio', '73b').
belongsto('L. Di Ciaccio', '5').
belongsto('W.K. Di Clemente', '137').
belongsto('C. Di Donato', '69a').
alsoat('C. Di Donato', '69b').
belongsto('A. Di Girolamo', '36').
belongsto('G. Di Gregorio', '71a').
alsoat('G. Di Gregorio', '71b').
belongsto('B. Di Micco', '74a').
alsoat('B. Di Micco', '74b').
belongsto('R. Di Nardo', '102').
belongsto('K.F. Di Petrillo', '59').
belongsto('R. Di Sipio', '167').
belongsto('D. Di Valentino', '34').
belongsto('C. Diaconu', '101').
belongsto('F.A. Dias', '40').
belongsto('T. Dias Do Vale', '140a').
alsoat('T. Dias Do Vale', '140e').
belongsto('M.A. Diaz', '147a').
belongsto('J. Dickinson', '18').
belongsto('E.B. Diehl', '105').
belongsto('J. Dietrich', '19').
belongsto('S. Díez Cornell', '46').
belongsto('A. Dimitrievska', '18').
belongsto('J. Dingfelder', '24').
belongsto('F. Dittus', '36').
belongsto('F. Djama', '101').
belongsto('T. Djobava', '159b').
belongsto('J.I. Djuvsland', '17').
belongsto('M.A.B. Do Vale', '80c').
belongsto('M. Dobre', '27b').
belongsto('D. Dodsworth', '26').
belongsto('C. Doglioni', '96').
belongsto('J. Dolejsi', '143').
belongsto('Z. Dolezal', '143').
belongsto('M. Donadelli', '80d').
belongsto('J. Donini', '38').
belongsto('A. D\'onofrio', '92').
belongsto('M. D\'Onofrio', '90').
belongsto('J. Dopke', '144').
belongsto('A. Doria', '69a').
belongsto('M.T. Dova', '88').
belongsto('A.T. Doyle', '57').
belongsto('E. Drechsler', '152').
belongsto('E. Dreyer', '152').
belongsto('T. Dreyer', '53').
belongsto('Y. Du', '60b').
belongsto('Y. Duan', '60b').
belongsto('F. Dubinin', '110').
belongsto('M. Dubovsky', '28a').
belongsto('A. Dubreuil', '54').
belongsto('E. Duchovni', '180').
belongsto('G. Duckeck', '114').
belongsto('A. Ducourthial', '136').
belongsto('O.A. Ducu', '109').
alsoat('O.A. Ducu', 'x').
belongsto('D. Duda', '115').
belongsto('A. Dudarev', '36').
belongsto('A.C. Dudder', '99').
belongsto('E.M. Duffield', '18').
belongsto('L. Duflot', '132').
belongsto('M. Dòhrssen', '36').
belongsto('C. Dòlsen', '182').
belongsto('M. Dumancic', '180').
belongsto('A.E. Dumitriu', '27b').
belongsto('A.K. Duncan', '57').
belongsto('M. Dunford', '61a').
belongsto('A. Duperrin', '101').
belongsto('H. Duran Yildiz', '4a').
belongsto('M. Dòren', '56').
belongsto('A. Durglishvili', '159b').
belongsto('D. Duschinger', '48').
belongsto('B. Dutta', '46').
belongsto('D. Duvnjak', '1').
belongsto('G.I. Dyckes', '137').
belongsto('M. Dyndal', '46').
belongsto('S. Dysch', '100').
belongsto('B.S. Dziedzic', '84').
belongsto('K.M. Ecker', '115').
belongsto('R.C. Edgar', '105').
belongsto('T. Eifert', '36').
belongsto('G. Eigen', '17').
belongsto('K. Einsweiler', '18').
belongsto('T. Ekelof', '172').
belongsto('M. El Kacimi', '35c').
belongsto('R. El Kosseifi', '101').
belongsto('V. Ellajosyula', '172').
belongsto('M. Ellert', '172').
belongsto('F. Ellinghaus', '182').
belongsto('A.A. Elliot', '92').
belongsto('N. Ellis', '36').
belongsto('J. Elmsheuser', '29').
belongsto('M. Elsing', '36').
belongsto('D. Emeliyanov', '144').
belongsto('A. Emerman', '39').
belongsto('Y. Enari', '163').
belongsto('J.S. Ennis', '178').
belongsto('M.B. Epland', '49').
belongsto('J. Erdmann', '47').
belongsto('A. Ereditato', '20').
belongsto('M. Escalier', '132').
belongsto('C. Escobar', '174').
belongsto('O. Estrada Pastor', '174').
belongsto('A.I. Etienvre', '145').
belongsto('E. Etzion', '161').
belongsto('H. Evans', '65').
belongsto('A. Ezhilov', '138').
belongsto('M. Ezzi', '35e').
belongsto('F. Fabbri', '57').
belongsto('L. Fabbri', '23b').
alsoat('L. Fabbri', '23a').
belongsto('V. Fabiani', '119').
belongsto('G. Facini', '94').
belongsto('R.M. Faisca Rodrigues Pereira', '140a').
belongsto('R.M. Fakhrutdinov', '123').
belongsto('S. Falciano', '72a').
belongsto('P.J. Falke', '5').
belongsto('S. Falke', '5').
belongsto('J. Faltova', '143').
belongsto('Y. Fang', '15a').
belongsto('Y. Fang', '15a').
belongsto('G. Fanourakis', '44').
belongsto('M. Fanti', '68a').
alsoat('M. Fanti', '68b').
belongsto('A. Farbin', '8').
belongsto('A. Farilla', '74a').
belongsto('E.M. Farina', '70a').
alsoat('E.M. Farina', '70b').
belongsto('T. Farooque', '106').
belongsto('S. Farrell', '18').
belongsto('S.M. Farrington', '178').
belongsto('P. Farthouat', '36').
belongsto('F. Fassi', '35e').
belongsto('P. Fassnacht', '36').
belongsto('D. Fassouliotis', '9').
belongsto('M. Faucci Giannelli', '50').
belongsto('W.J. Fawcett', '32').
belongsto('L. Fayard', '132').
belongsto('O.L. Fedin', '138').
alsoat('O.L. Fedin', 'p').
belongsto('W. Fedorko', '175').
belongsto('M. Feickert', '42').
belongsto('S. Feigl', '134').
belongsto('L. Feligioni', '101').
belongsto('C. Feng', '60b').
belongsto('E.J. Feng', '36').
belongsto('M. Feng', '49').
belongsto('M.J. Fenton', '57').
belongsto('A.B. Fenyuk', '123').
belongsto('J. Ferrando', '46').
belongsto('A. Ferrari', '172').
belongsto('P. Ferrari', '120').
belongsto('R. Ferrari', '70a').
belongsto('D.E. Ferreira de Lima', '61b').
belongsto('A. Ferrer', '174').
belongsto('D. Ferrere', '54').
belongsto('C. Ferretti', '105').
belongsto('F. Fiedler', '99').
belongsto('A. Filipčič', '91').
belongsto('F. Filthaut', '119').
belongsto('K.D. Finelli', '25').
belongsto('M.C.N. Fiolhais', '140a').
alsoat('M.C.N. Fiolhais', '140c').
alsoat('M.C.N. Fiolhais', 'a').
belongsto('L. Fiorini', '174').
belongsto('C. Fischer', '14').
belongsto('W.C. Fisher', '106').
belongsto('I. Fleck', '151').
belongsto('P. Fleischmann', '105').
belongsto('R.R.M. Fletcher', '137').
belongsto('T. Flick', '182').
belongsto('B.M. Flierl', '114').
belongsto('L.F. Flores', '137').
belongsto('L.R. Flores Castillo', '63a').
belongsto('F.M. Follega', '75a').
alsoat('F.M. Follega', '75b').
belongsto('N. Fomin', '17').
belongsto('G.T. Forcolin', '75a').
alsoat('G.T. Forcolin', '75b').
belongsto('A. Formica', '145').
belongsto('F.A. Förster', '14').
belongsto('A.C. Forti', '100').
belongsto('A.G. Foster', '21').
belongsto('D. Fournier', '132').
belongsto('H. Fox', '89').
belongsto('S. Fracchia', '149').
belongsto('P. Francavilla', '71a').
alsoat('P. Francavilla', '71b').
belongsto('M. Franchini', '23b').
alsoat('M. Franchini', '23a').
belongsto('S. Franchino', '61a').
belongsto('D. Francis', '36').
belongsto('L. Franconi', '146').
belongsto('M. Franklin', '59').
belongsto('M. Frate', '171').
belongsto('A.N. Fray', '92').
belongsto('B. Freund', '109').
belongsto('W.S. Freund', '80b').
belongsto('E.M. Freundlich', '47').
belongsto('D.C. Frizzell', '128').
belongsto('D. Froidevaux', '36').
belongsto('J.A. Frost', '135').
belongsto('C. Fukunaga', '164').
belongsto('E. Fullana Torregrosa', '174').
belongsto('E. Fumagalli', '55b').
alsoat('E. Fumagalli', '55a').
belongsto('T. Fusayasu', '116').
belongsto('J. Fuster', '174').
belongsto('A. Gabrielli', '23b').
alsoat('A. Gabrielli', '23a').
belongsto('A. Gabrielli', '18').
belongsto('G.P. Gach', '83a').
belongsto('S. Gadatsch', '54').
belongsto('P. Gadow', '115').
belongsto('G. Gagliardi', '55b').
alsoat('G. Gagliardi', '55a').
belongsto('L.G. Gagnon', '109').
belongsto('C. Galea', '27b').
belongsto('B. Galhardo', '140a').
alsoat('B. Galhardo', '140c').
belongsto('E.J. Gallas', '135').
belongsto('B.J. Gallop', '144').
belongsto('P. Gallus', '142').
belongsto('G. Galster', '40').
belongsto('R. Gamboa Goni', '92').
belongsto('K.K. Gan', '126').
belongsto('S. Ganguly', '180').
belongsto('J. Gao', '60a').
belongsto('Y. Gao', '90').
belongsto('Y.S. Gao', '31').
alsoat('Y.S. Gao', 'm').
belongsto('C. García', '174').
belongsto('J.E. García Navarro', '174').
belongsto('J.A. García Pascual', '15a').
belongsto('C. Garcia-Argos', '52').
belongsto('M. Garcia-Sciveres', '18').
belongsto('R.W. Gardner', '37').
belongsto('N. Garelli', '153').
belongsto('S. Gargiulo', '52').
belongsto('V. Garonne', '134').
belongsto('A. Gaudiello', '55b').
alsoat('A. Gaudiello', '55a').
belongsto('G. Gaudio', '70a').
belongsto('I.L. Gavrilenko', '110').
belongsto('A. Gavrilyuk', '111').
belongsto('C. Gay', '175').
belongsto('G. Gaycken', '24').
belongsto('E.N. Gazis', '10').
belongsto('C.N.P. Gee', '144').
belongsto('J. Geisen', '53').
belongsto('M. Geisen', '99').
belongsto('M.P. Geisler', '61a').
belongsto('C. Gemme', '55b').
belongsto('M.H. Genest', '58').
belongsto('C. Geng', '105').
belongsto('S. Gentile', '72a').
alsoat('S. Gentile', '72b').
belongsto('S. George', '93').
belongsto('T. Geralis', '44').
belongsto('D. Gerbaudo', '14').
belongsto('G. Gessner', '47').
belongsto('S. Ghasemi', '151').
belongsto('M. Ghasemi Bostanabad', '176').
belongsto('M. Ghneimat', '24').
belongsto('A. Ghosh', '77').
belongsto('B. Giacobbe', '23b').
belongsto('S. Giagu', '72a').
alsoat('S. Giagu', '72b').
belongsto('N. Giangiacomi', '23b').
alsoat('N. Giangiacomi', '23a').
belongsto('P. Giannetti', '71a').
belongsto('A. Giannini', '69a').
alsoat('A. Giannini', '69b').
belongsto('S.M. Gibson', '93').
belongsto('M. Gignac', '146').
belongsto('D. Gillberg', '34').
belongsto('G. Gilles', '182').
belongsto('D.M. Gingrich', '3').
alsoat('D.M. Gingrich', 'aw').
belongsto('M.P. Giordani', '66a').
alsoat('M.P. Giordani', '66c').
belongsto('F.M. Giorgi', '23b').
belongsto('P.F. Giraud', '145').
belongsto('G. Giugliarelli', '66a').
alsoat('G. Giugliarelli', '66c').
belongsto('D. Giugni', '68a').
belongsto('F. Giuli', '135').
belongsto('M. Giulini', '61b').
belongsto('S. Gkaitatzis', '162').
belongsto('I. Gkialas', '9').
alsoat('I. Gkialas', 'i').
belongsto('E.L. Gkougkousis', '14').
belongsto('P. Gkountoumis', '10').
belongsto('L.K. Gladilin', '113').
belongsto('C. Glasman', '98').
belongsto('J. Glatzer', '14').
belongsto('P.C.F. Glaysher', '46').
belongsto('A. Glazov', '46').
belongsto('M. Goblirsch-Kolb', '26').
belongsto('S. Goldfarb', '104').
belongsto('T. Golling', '54').
belongsto('D. Golubkov', '123').
belongsto('A. Gomes', '140a').
alsoat('A. Gomes', '140b').
belongsto('R. Goncalves Gama', '53').
belongsto('R. Gonçalo', '140a').
alsoat('R. Gonçalo', '140b').
belongsto('G. Gonella', '52').
belongsto('L. Gonella', '21').
belongsto('A. Gongadze', '79').
belongsto('F. Gonnella', '21').
belongsto('J.L. Gonski', '59').
belongsto('S. González de la Hoz', '174').
belongsto('S. Gonzalez-Sevilla', '54').
belongsto('G.R. Gonzalvo Rodriguez', '174').
belongsto('L. Goossens', '36').
belongsto('P.A. Gorbounov', '111').
belongsto('H.A. Gordon', '29').
belongsto('B. Gorini', '36').
belongsto('E. Gorini', '67a').
alsoat('E. Gorini', '67b').
belongsto('A. Gorišek', '91').
belongsto('A.T. Goshaw', '49').
belongsto('C. Gössling', '47').
belongsto('M.I. Gostkin', '79').
belongsto('C.A. Gottardo', '24').
belongsto('C.R. Goudet', '132').
belongsto('D. Goujdami', '35c').
belongsto('A.G. Goussiou', '148').
belongsto('N. Govender', '33b').
alsoat('N. Govender', 'b').
belongsto('C. Goy', '5').
belongsto('E. Gozani', '160').
belongsto('I. Grabowska-Bold', '83a').
belongsto('P.O.J. Gradin', '172').
belongsto('E.C. Graham', '90').
belongsto('J. Gramling', '171').
belongsto('E. Gramstad', '134').
belongsto('S. Grancagnolo', '19').
belongsto('M. Grandi', '156').
belongsto('V. Gratchev', '138').
belongsto('P.M. Gravila', '27f').
belongsto('F.G. Gravili', '67a').
alsoat('F.G. Gravili', '67b').
belongsto('C. Gray', '57').
belongsto('H.M. Gray', '18').
belongsto('C. Grefe', '24').
belongsto('K. Gregersen', '96').
belongsto('I.M. Gregor', '46').
belongsto('P. Grenier', '153').
belongsto('K. Grevtsov', '46').
belongsto('N.A. Grieser', '128').
belongsto('J. Griffiths', '8').
belongsto('A.A. Grillo', '146').
belongsto('K. Grimm', '31').
alsoat('K. Grimm', 'l').
belongsto('S. Grinstein', '14').
alsoat('S. Grinstein', 'y').
belongsto('J.-F. Grivaz', '132').
belongsto('S. Groh', '99').
belongsto('E. Gross', '180').
belongsto('J. Grosse-Knetter', '53').
belongsto('Z.J. Grout', '94').
belongsto('C. Grud', '105').
belongsto('A. Grummer', '118').
belongsto('L. Guan', '105').
belongsto('W. Guan', '181').
belongsto('J. Guenther', '36').
belongsto('A. Guerguichon', '132').
belongsto('F. Guescini', '168a').
belongsto('D. Guest', '171').
belongsto('R. Gugel', '52').
belongsto('B. Gui', '126').
belongsto('T. Guillemin', '5').
belongsto('S. Guindon', '36').
belongsto('U. Gul', '57').
belongsto('J. Guo', '60c').
belongsto('W. Guo', '105').
belongsto('Y. Guo', '60a').
alsoat('Y. Guo', 's').
belongsto('Z. Guo', '101').
belongsto('R. Gupta', '46').
belongsto('S. Gurbuz', '12c').
belongsto('G. Gustavino', '128').
belongsto('P. Gutierrez', '128').
belongsto('C. Gutschow', '94').
belongsto('C. Guyot', '145').
belongsto('M.P. Guzik', '83a').
belongsto('C. Gwenlan', '135').
belongsto('C.B. Gwilliam', '90').
belongsto('A. Haas', '124').
belongsto('C. Haber', '18').
belongsto('H.K. Hadavand', '8').
belongsto('N. Haddad', '35e').
belongsto('A. Hadef', '60a').
belongsto('S. Hageböck', '36').
belongsto('M. Hagihara', '169').
belongsto('M. Haleem', '177').
belongsto('J. Haley', '129').
belongsto('G. Halladjian', '106').
belongsto('G.D. Hallewell', '101').
belongsto('K. Hamacher', '182').
belongsto('P. Hamal', '130').
belongsto('K. Hamano', '176').
belongsto('H. Hamdaoui', '35e').
belongsto('G.N. Hamity', '149').
belongsto('K. Han', '60a').
alsoat('K. Han', 'ak').
belongsto('L. Han', '60a').
belongsto('S. Han', '15a').
alsoat('S. Han', '15d').
belongsto('K. Hanagaki', '81').
alsoat('K. Hanagaki', 'v').
belongsto('M. Hance', '146').
belongsto('D.M. Handl', '114').
belongsto('B. Haney', '137').
belongsto('R. Hankache', '136').
belongsto('P. Hanke', '61a').
belongsto('E. Hansen', '96').
belongsto('J.B. Hansen', '40').
belongsto('J.D. Hansen', '40').
belongsto('M.C. Hansen', '24').
belongsto('P.H. Hansen', '40').
belongsto('E.C. Hanson', '100').
belongsto('K. Hara', '169').
belongsto('A.S. Hard', '181').
belongsto('T. Harenberg', '182').
belongsto('S. Harkusha', '107').
belongsto('P.F. Harrison', '178').
belongsto('N.M. Hartmann', '114').
belongsto('Y. Hasegawa', '150').
belongsto('A. Hasib', '50').
belongsto('S. Hassani', '145').
belongsto('S. Haug', '20').
belongsto('R. Hauser', '106').
belongsto('L. Hauswald', '48').
belongsto('L.B. Havener', '39').
belongsto('M. Havranek', '142').
belongsto('C.M. Hawkes', '21').
belongsto('R.J. Hawkings', '36').
belongsto('D. Hayden', '106').
belongsto('C. Hayes', '155').
belongsto('R.L. Hayes', '175').
belongsto('C.P. Hays', '135').
belongsto('J.M. Hays', '92').
belongsto('H.S. Hayward', '90').
belongsto('S.J. Haywood', '144').
belongsto('F. He', '60a').
belongsto('M.P. Heath', '50').
belongsto('V. Hedberg', '96').
belongsto('L. Heelan', '8').
belongsto('S. Heer', '24').
belongsto('K.K. Heidegger', '52').
belongsto('J. Heilman', '34').
belongsto('S. Heim', '46').
belongsto('T. Heim', '18').
belongsto('B. Heinemann', '46').
alsoat('B. Heinemann', 'ar').
belongsto('J.J. Heinrich', '114').
belongsto('L. Heinrich', '124').
belongsto('C. Heinz', '56').
belongsto('J. Hejbal', '141').
belongsto('L. Helary', '61b').
belongsto('A. Held', '175').
belongsto('S. Hellesund', '134').
belongsto('C.M. Helling', '146').
belongsto('S. Hellman', '45a').
alsoat('S. Hellman', '45b').
belongsto('C. Helsens', '36').
belongsto('R.C.W. Henderson', '89').
belongsto('Y. Heng', '181').
belongsto('S. Henkelmann', '175').
belongsto('A.M. Henriques Correia', '36').
belongsto('G.H. Herbert', '19').
belongsto('H. Herde', '26').
belongsto('V. Herget', '177').
belongsto('Y. Hernández Jiménez', '33c').
belongsto('H. Herr', '99').
belongsto('M.G. Herrmann', '114').
belongsto('T. Herrmann', '48').
belongsto('G. Herten', '52').
belongsto('R. Hertenberger', '114').
belongsto('L. Hervas', '36').
belongsto('T.C. Herwig', '137').
belongsto('G.G. Hesketh', '94').
belongsto('N.P. Hessey', '168a').
belongsto('A. Higashida', '163').
belongsto('S. Higashino', '81').
belongsto('E. Higón-Rodriguez', '174').
belongsto('K. Hildebrand', '37').
belongsto('E. Hill', '176').
belongsto('J.C. Hill', '32').
belongsto('K.K. Hill', '29').
belongsto('K.H. Hiller', '46').
belongsto('S.J. Hillier', '21').
belongsto('M. Hils', '48').
belongsto('I. Hinchliffe', '18').
belongsto('F. Hinterkeuser', '24').
belongsto('M. Hirose', '133').
belongsto('D. Hirschbuehl', '182').
belongsto('B. Hiti', '91').
belongsto('O. Hladik', '141').
belongsto('D.R. Hlaluku', '33c').
belongsto('X. Hoad', '50').
belongsto('J. Hobbs', '155').
belongsto('N. Hod', '180').
belongsto('M.C. Hodgkinson', '149').
belongsto('A. Hoecker', '36').
belongsto('F. Hoenig', '114').
belongsto('D. Hohn', '52').
belongsto('D. Hohov', '132').
belongsto('T.R. Holmes', '37').
belongsto('M. Holzbock', '114').
belongsto('L.B.A.H Hommels', '32').
belongsto('S. Honda', '169').
belongsto('T. Honda', '81').
belongsto('T.M. Hong', '139').
belongsto('A. Hönle', '115').
belongsto('B.H. Hooberman', '173').
belongsto('W.H. Hopkins', '6').
belongsto('Y. Horii', '117').
belongsto('P. Horn', '48').
belongsto('A.J. Horton', '152').
belongsto('L.A. Horyn', '37').
belongsto('J-Y. Hostachy', '58').
belongsto('A. Hostiuc', '148').
belongsto('S. Hou', '158').
belongsto('A. Hoummada', '35a').
belongsto('J. Howarth', '100').
belongsto('J. Hoya', '88').
belongsto('M. Hrabovsky', '130').
belongsto('J. Hrdinka', '36').
belongsto('I. Hristova', '19').
belongsto('J. Hrivnac', '132').
belongsto('A. Hrynevich', '108').
belongsto('T. Hryn\'ova', '5').
belongsto('P.J. Hsu', '64').
belongsto('S.-C. Hsu', '148').
belongsto('Q. Hu', '29').
belongsto('S. Hu', '60c').
belongsto('Y. Huang', '15a').
belongsto('Z. Hubacek', '142').
belongsto('F. Hubaut', '101').
belongsto('M. Huebner', '24').
belongsto('F. Huegging', '24').
belongsto('T.B. Huffman', '135').
belongsto('M. Huhtinen', '36').
belongsto('R.F.H. Hunter', '34').
belongsto('P. Huo', '155').
belongsto('A.M. Hupe', '34').
belongsto('N. Huseynov', '79').
alsoat('N. Huseynov', 'af').
belongsto('J. Huston', '106').
belongsto('J. Huth', '59').
belongsto('R. Hyneman', '105').
belongsto('G. Iacobucci', '54').
belongsto('G. Iakovidis', '29').
belongsto('I. Ibragimov', '151').
belongsto('L. Iconomidou-Fayard', '132').
belongsto('Z. Idrissi', '35e').
belongsto('P.I. Iengo', '36').
belongsto('R. Ignazzi', '40').
belongsto('O. Igonkina', '120').
alsoat('O. Igonkina', 'aa').
belongsto('R. Iguchi', '163').
belongsto('T. Iizawa', '54').
belongsto('Y. Ikegami', '81').
belongsto('M. Ikeno', '81').
belongsto('D. Iliadis', '162').
belongsto('N. Ilic', '119').
belongsto('F. Iltzsche', '48').
belongsto('G. Introzzi', '70a').
alsoat('G. Introzzi', '70b').
belongsto('M. Iodice', '74a').
belongsto('K. Iordanidou', '39').
belongsto('V. Ippolito', '72a').
alsoat('V. Ippolito', '72b').
belongsto('M.F. Isacson', '172').
belongsto('N. Ishijima', '133').
belongsto('M. Ishino', '163').
belongsto('M. Ishitsuka', '165').
belongsto('W. Islam', '129').
belongsto('C. Issever', '135').
belongsto('S. Istin', '160').
belongsto('F. Ito', '169').
belongsto('J.M. Iturbe Ponce', '63a').
belongsto('R. Iuppa', '75a').
alsoat('R. Iuppa', '75b').
belongsto('A. Ivina', '180').
belongsto('H. Iwasaki', '81').
belongsto('J.M. Izen', '43').
belongsto('V. Izzo', '69a').
belongsto('P. Jacka', '141').
belongsto('P. Jackson', '1').
belongsto('R.M. Jacobs', '24').
belongsto('V. Jain', '2').
belongsto('G. Jäkel', '182').
belongsto('K.B. Jakobi', '99').
belongsto('K. Jakobs', '52').
belongsto('S. Jakobsen', '76').
belongsto('T. Jakoubek', '141').
belongsto('D.O. Jamin', '129').
belongsto('R. Jansky', '54').
belongsto('J. Janssen', '24').
belongsto('M. Janus', '53').
belongsto('P.A. Janus', '83a').
belongsto('G. Jarlskog', '96').
belongsto('N. Javadov', '79').
alsoat('N. Javadov', 'af').
belongsto('T. Javůrek', '36').
belongsto('M. Javurkova', '52').
belongsto('F. Jeanneau', '145').
belongsto('L. Jeanty', '131').
belongsto('J. Jejelava', '159a').
alsoat('J. Jejelava', 'ag').
belongsto('A. Jelinskas', '178').
belongsto('P. Jenni', '52').
alsoat('P. Jenni', 'c').
belongsto('J. Jeong', '46').
belongsto('N. Jeong', '46').
belongsto('S. Jézéquel', '5').
belongsto('H. Ji', '181').
belongsto('J. Jia', '155').
belongsto('H. Jiang', '78').
belongsto('Y. Jiang', '60a').
belongsto('Z. Jiang', '153').
alsoat('Z. Jiang', 'q').
belongsto('S. Jiggins', '52').
belongsto('F.A. Jimenez Morales', '38').
belongsto('J. Jimenez Pena', '174').
belongsto('S. Jin', '15c').
belongsto('A. Jinaru', '27b').
belongsto('O. Jinnouchi', '165').
belongsto('H. Jivan', '33c').
belongsto('P. Johansson', '149').
belongsto('K.A. Johns', '7').
belongsto('C.A. Johnson', '65').
belongsto('K. Jon-And', '45a').
alsoat('K. Jon-And', '45b').
belongsto('R.W.L. Jones', '89').
belongsto('S.D. Jones', '156').
belongsto('S. Jones', '7').
belongsto('T.J. Jones', '90').
belongsto('J. Jongmanns', '61a').
belongsto('P.M. Jorge', '140a').
alsoat('P.M. Jorge', '140b').
belongsto('J. Jovicevic', '168a').
belongsto('X. Ju', '18').
belongsto('J.J. Junggeburth', '115').
belongsto('A. Juste Rozas', '14').
alsoat('A. Juste Rozas', 'y').
belongsto('A. Kaczmarska', '84').
belongsto('M. Kado', '132').
belongsto('H. Kagan', '126').
belongsto('M. Kagan', '153').
belongsto('T. Kaji', '179').
belongsto('E. Kajomovitz', '160').
belongsto('C.W. Kalderon', '96').
belongsto('A. Kaluza', '99').
belongsto('A. Kamenshchikov', '123').
belongsto('L. Kanjir', '91').
belongsto('Y. Kano', '163').
belongsto('V.A. Kantserov', '112').
belongsto('J. Kanzaki', '81').
belongsto('L.S. Kaplan', '181').
belongsto('D. Kar', '33c').
belongsto('M.J. Kareem', '168b').
belongsto('E. Karentzos', '10').
belongsto('S.N. Karpov', '79').
belongsto('Z.M. Karpova', '79').
belongsto('V. Kartvelishvili', '89').
belongsto('A.N. Karyukhin', '123').
belongsto('L. Kashif', '181').
belongsto('R.D. Kass', '126').
belongsto('A. Kastanas', '45a').
alsoat('A. Kastanas', '45b').
belongsto('Y. Kataoka', '163').
belongsto('C. Kato', '60d').
alsoat('C. Kato', '60c').
belongsto('J. Katzy', '46').
belongsto('K. Kawade', '82').
belongsto('K. Kawagoe', '87').
belongsto('T. Kawaguchi', '117').
belongsto('T. Kawamoto', '163').
belongsto('G. Kawamura', '53').
belongsto('E.F. Kay', '176').
belongsto('V.F. Kazanin', '122b').
alsoat('V.F. Kazanin', '122a').
belongsto('R. Keeler', '176').
belongsto('R. Kehoe', '42').
belongsto('J.S. Keller', '34').
belongsto('E. Kellermann', '96').
belongsto('J.J. Kempster', '21').
belongsto('J. Kendrick', '21').
belongsto('O. Kepka', '141').
belongsto('S. Kersten', '182').
belongsto('B.P. Kerševan', '91').
belongsto('S. Ketabchi Haghighat', '167').
belongsto('R.A. Keyes', '103').
belongsto('M. Khader', '173').
belongsto('F. Khalil-Zada', '13').
belongsto('A. Khanov', '129').
belongsto('A.G. Kharlamov', '122b').
alsoat('A.G. Kharlamov', '122a').
belongsto('T. Kharlamova', '122b').
alsoat('T. Kharlamova', '122a').
belongsto('E.E. Khoda', '175').
belongsto('A. Khodinov', '166').
belongsto('T.J. Khoo', '54').
belongsto('E. Khramov', '79').
belongsto('J. Khubua', '159b').
belongsto('S. Kido', '82').
belongsto('M. Kiehn', '54').
belongsto('C.R. Kilby', '93').
belongsto('Y.K. Kim', '37').
belongsto('N. Kimura', '66a').
alsoat('N. Kimura', '66c').
belongsto('O.M. Kind', '19').
belongsto('B.T. King', '90').
alsoat('B.T. King', '*').
belongsto('D. Kirchmeier', '48').
belongsto('J. Kirk', '144').
belongsto('A.E. Kiryunin', '115').
belongsto('T. Kishimoto', '163').
belongsto('V. Kitali', '46').
belongsto('O. Kivernyk', '5').
belongsto('E. Kladiva', '28b').
alsoat('E. Kladiva', '*').
belongsto('T. Klapdor-Kleingrothaus', '52').
belongsto('M.H. Klein', '105').
belongsto('M. Klein', '90').
belongsto('U. Klein', '90').
belongsto('K. Kleinknecht', '99').
belongsto('P. Klimek', '121').
belongsto('A. Klimentov', '29').
belongsto('T. Klingl', '24').
belongsto('T. Klioutchnikova', '36').
belongsto('F.F. Klitzner', '114').
belongsto('P. Kluit', '120').
belongsto('S. Kluth', '115').
belongsto('E. Kneringer', '76').
belongsto('E.B.F.G. Knoops', '101').
belongsto('A. Knue', '52').
belongsto('D. Kobayashi', '87').
belongsto('T. Kobayashi', '163').
belongsto('M. Kobel', '48').
belongsto('M. Kocian', '153').
belongsto('P. Kodys', '143').
belongsto('P.T. Koenig', '24').
belongsto('T. Koffas', '34').
belongsto('N.M. Köhler', '115').
belongsto('T. Koi', '153').
belongsto('M. Kolb', '61b').
belongsto('I. Koletsou', '5').
belongsto('T. Kondo', '81').
belongsto('N. Kondrashova', '60c').
belongsto('K. Köneke', '52').
belongsto('A.C. König', '119').
belongsto('T. Kono', '125').
belongsto('R. Konoplich', '124').
alsoat('R. Konoplich', 'an').
belongsto('V. Konstantinides', '94').
belongsto('N. Konstantinidis', '94').
belongsto('B. Konya', '96').
belongsto('R. Kopeliansky', '65').
belongsto('S. Koperny', '83a').
belongsto('K. Korcyl', '84').
belongsto('K. Kordas', '162').
belongsto('G. Koren', '161').
belongsto('A. Korn', '94').
belongsto('I. Korolkov', '14').
belongsto('E.V. Korolkova', '149').
belongsto('N. Korotkova', '113').
belongsto('O. Kortner', '115').
belongsto('S. Kortner', '115').
belongsto('T. Kosek', '143').
belongsto('V.V. Kostyukhin', '24').
belongsto('A. Kotwal', '49').
belongsto('A. Koulouris', '10').
belongsto('A. Kourkoumeli-Charalampidi', '70a').
alsoat('A. Kourkoumeli-Charalampidi', '70b').
belongsto('C. Kourkoumelis', '9').
belongsto('E. Kourlitis', '149').
belongsto('V. Kouskoura', '29').
belongsto('A.B. Kowalewska', '84').
belongsto('R. Kowalewski', '176').
belongsto('C. Kozakai', '163').
belongsto('W. Kozanecki', '145').
belongsto('A.S. Kozhin', '123').
belongsto('V.A. Kramarenko', '113').
belongsto('G. Kramberger', '91').
belongsto('D. Krasnopevtsev', '60a').
belongsto('M.W. Krasny', '136').
belongsto('A. Krasznahorkay', '36').
belongsto('D. Krauss', '115').
belongsto('J.A. Kremer', '83a').
belongsto('J. Kretzschmar', '90').
belongsto('P. Krieger', '167').
belongsto('K. Krizka', '18').
belongsto('K. Kroeninger', '47').
belongsto('H. Kroha', '115').
belongsto('J. Kroll', '141').
belongsto('J. Kroll', '137').
belongsto('J. Krstic', '16').
belongsto('U. Kruchonak', '79').
belongsto('H. Kròger', '24').
belongsto('N. Krumnack', '78').
belongsto('M.C. Kruse', '49').
belongsto('T. Kubota', '104').
belongsto('S. Kuday', '4b').
belongsto('J.T. Kuechler', '46').
belongsto('S. Kuehn', '36').
belongsto('A. Kugel', '61a').
belongsto('T. Kuhl', '46').
belongsto('V. Kukhtin', '79').
belongsto('R. Kukla', '101').
belongsto('Y. Kulchitsky', '107').
alsoat('Y. Kulchitsky', 'aj').
belongsto('S. Kuleshov', '147b').
belongsto('Y.P. Kulinich', '173').
belongsto('M. Kuna', '58').
belongsto('T. Kunigo', '85').
belongsto('A. Kupco', '141').
belongsto('T. Kupfer', '47').
belongsto('O. Kuprash', '52').
belongsto('H. Kurashige', '82').
belongsto('L.L. Kurchaninov', '168a').
belongsto('Y.A. Kurochkin', '107').
belongsto('A. Kurova', '112').
belongsto('M.G. Kurth', '15a').
alsoat('M.G. Kurth', '15d').
belongsto('E.S. Kuwertz', '36').
belongsto('M. Kuze', '165').
belongsto('J. Kvita', '130').
belongsto('T. Kwan', '103').
belongsto('A. La Rosa', '115').
belongsto('J.L. La Rosa Navarro', '80d').
belongsto('L. La Rotonda', '41b').
alsoat('L. La Rotonda', '41a').
belongsto('F. La Ruffa', '41b').
alsoat('F. La Ruffa', '41a').
belongsto('C. Lacasta', '174').
belongsto('F. Lacava', '72a').
alsoat('F. Lacava', '72b').
belongsto('D.P.J. Lack', '100').
belongsto('H. Lacker', '19').
belongsto('D. Lacour', '136').
belongsto('E. Ladygin', '79').
belongsto('R. Lafaye', '5').
belongsto('B. Laforge', '136').
belongsto('T. Lagouri', '33c').
belongsto('S. Lai', '53').
belongsto('S. Lammers', '65').
belongsto('W. Lampl', '7').
belongsto('E. Lançon', '29').
belongsto('U. Landgraf', '52').
belongsto('M.P.J. Landon', '92').
belongsto('M.C. Lanfermann', '54').
belongsto('V.S. Lang', '46').
belongsto('J.C. Lange', '53').
belongsto('R.J. Langenberg', '36').
belongsto('A.J. Lankford', '171').
belongsto('F. Lanni', '29').
belongsto('K. Lantzsch', '24').
belongsto('A. Lanza', '70a').
belongsto('A. Lapertosa', '55b').
alsoat('A. Lapertosa', '55a').
belongsto('S. Laplace', '136').
belongsto('J.F. Laporte', '145').
belongsto('T. Lari', '68a').
belongsto('F. Lasagni Manghi', '23b').
alsoat('F. Lasagni Manghi', '23a').
belongsto('M. Lassnig', '36').
belongsto('T.S. Lau', '63a').
belongsto('A. Laudrain', '132').
belongsto('A. Laurier', '34').
belongsto('M. Lavorgna', '69a').
alsoat('M. Lavorgna', '69b').
belongsto('M. Lazzaroni', '68a').
alsoat('M. Lazzaroni', '68b').
belongsto('B. Le', '104').
belongsto('O. Le Dortz', '136').
belongsto('E. Le Guirriec', '101').
belongsto('M. LeBlanc', '7').
belongsto('T. LeCompte', '6').
belongsto('F. Ledroit-Guillon', '58').
belongsto('C.A. Lee', '29').
belongsto('G.R. Lee', '147a').
belongsto('L. Lee', '59').
belongsto('S.C. Lee', '158').
belongsto('S.J. Lee', '34').
belongsto('B. Lefebvre', '103').
belongsto('M. Lefebvre', '176').
belongsto('F. Legger', '114').
belongsto('C. Leggett', '18').
belongsto('K. Lehmann', '152').
belongsto('N. Lehmann', '182').
belongsto('G. Lehmann Miotto', '36').
belongsto('W.A. Leight', '46').
belongsto('A. Leisos', '162').
alsoat('A. Leisos', 'w').
belongsto('M.A.L. Leite', '80d').
belongsto('R. Leitner', '143').
belongsto('D. Lellouch', '180').
alsoat('D. Lellouch', '*').
belongsto('K.J.C. Leney', '42').
belongsto('T. Lenz', '24').
belongsto('B. Lenzi', '36').
belongsto('R. Leone', '7').
belongsto('S. Leone', '71a').
belongsto('C. Leonidopoulos', '50').
belongsto('A. Leopold', '136').
belongsto('G. Lerner', '156').
belongsto('C. Leroy', '109').
belongsto('R. Les', '167').
belongsto('C.G. Lester', '32').
belongsto('M. Levchenko', '138').
belongsto('J. Levêque', '5').
belongsto('D. Levin', '105').
belongsto('L.J. Levinson', '180').
belongsto('B. Li', '15b').
belongsto('B. Li', '105').
belongsto('C-Q. Li', '60a').
alsoat('C-Q. Li', 'am').
belongsto('H. Li', '60a').
belongsto('H. Li', '60b').
belongsto('K. Li', '153').
belongsto('L. Li', '60c').
belongsto('M. Li', '15a').
belongsto('Q. Li', '15a').
alsoat('Q. Li', '15d').
belongsto('Q.Y. Li', '60a').
belongsto('S. Li', '60d').
alsoat('S. Li', '60c').
belongsto('X. Li', '60c').
belongsto('Y. Li', '46').
belongsto('Z. Liang', '15a').
belongsto('B. Liberti', '73a').
belongsto('A. Liblong', '167').
belongsto('K. Lie', '63c').
belongsto('S. Liem', '120').
belongsto('C.Y. Lin', '32').
belongsto('K. Lin', '106').
belongsto('T.H. Lin', '99').
belongsto('R.A. Linck', '65').
belongsto('J.H. Lindon', '21').
belongsto('A.L. Lionti', '54').
belongsto('E. Lipeles', '137').
belongsto('A. Lipniacka', '17').
belongsto('M. Lisovyi', '61b').
belongsto('T.M. Liss', '173').
alsoat('T.M. Liss', 'at').
belongsto('A. Lister', '175').
belongsto('A.M. Litke', '146').
belongsto('J.D. Little', '8').
belongsto('B. Liu', '78').
belongsto('B.L Liu', '6').
belongsto('H.B. Liu', '29').
belongsto('H. Liu', '105').
belongsto('J.B. Liu', '60a').
belongsto('J.K.K. Liu', '135').
belongsto('K. Liu', '136').
belongsto('M. Liu', '60a').
belongsto('P. Liu', '18').
belongsto('Y. Liu', '15a').
alsoat('Y. Liu', '15d').
belongsto('Y.L. Liu', '60a').
belongsto('Y.W. Liu', '60a').
belongsto('M. Livan', '70a').
alsoat('M. Livan', '70b').
belongsto('A. Lleres', '58').
belongsto('J. Llorente Merino', '15a').
belongsto('S.L. Lloyd', '92').
belongsto('C.Y. Lo', '63b').
belongsto('F. Lo Sterzo', '42').
belongsto('E.M. Lobodzinska', '46').
belongsto('P. Loch', '7').
belongsto('T. Lohse', '19').
belongsto('K. Lohwasser', '149').
belongsto('M. Lokajicek', '141').
belongsto('J.D. Long', '173').
belongsto('R.E. Long', '89').
belongsto('L. Longo', '36').
belongsto('K.A. Looper', '126').
belongsto('J.A. Lopez', '147b').
belongsto('I. Lopez Paz', '100').
belongsto('A. Lopez Solis', '149').
belongsto('J. Lorenz', '114').
belongsto('N. Lorenzo Martinez', '5').
belongsto('M. Losada', '22').
belongsto('P.J. Lösel', '114').
belongsto('A. Lösle', '52').
belongsto('X. Lou', '46').
belongsto('X. Lou', '15a').
belongsto('A. Lounis', '132').
belongsto('J. Love', '6').
belongsto('P.A. Love', '89').
belongsto('J.J. Lozano Bahilo', '174').
belongsto('H. Lu', '63a').
belongsto('M. Lu', '60a').
belongsto('Y.J. Lu', '64').
belongsto('H.J. Lubatti', '148').
belongsto('C. Luci', '72a').
alsoat('C. Luci', '72b').
belongsto('A. Lucotte', '58').
belongsto('C. Luedtke', '52').
belongsto('F. Luehring', '65').
belongsto('I. Luise', '136').
belongsto('L. Luminari', '72a').
belongsto('B. Lund-Jensen', '154').
belongsto('M.S. Lutz', '102').
belongsto('D. Lynn', '29').
belongsto('R. Lysak', '141').
belongsto('E. Lytken', '96').
belongsto('F. Lyu', '15a').
belongsto('V. Lyubushkin', '79').
belongsto('T. Lyubushkina', '79').
belongsto('H. Ma', '29').
belongsto('L.L. Ma', '60b').
belongsto('Y. Ma', '60b').
belongsto('G. Maccarrone', '51').
belongsto('A. Macchiolo', '115').
belongsto('C.M. Macdonald', '149').
belongsto('J. Machado Miguens', '137').
alsoat('J. Machado Miguens', '140b').
belongsto('D. Madaffari', '174').
belongsto('R. Madar', '38').
belongsto('W.F. Mader', '48').
belongsto('N. Madysa', '48').
belongsto('J. Maeda', '82').
belongsto('K. Maekawa', '163').
belongsto('S. Maeland', '17').
belongsto('T. Maeno', '29').
belongsto('M. Maerker', '48').
belongsto('A.S. Maevskiy', '113').
belongsto('V. Magerl', '52').
belongsto('N. Magini', '78').
belongsto('D.J. Mahon', '39').
belongsto('C. Maidantchik', '80b').
belongsto('T. Maier', '114').
belongsto('A. Maio', '140a').
alsoat('A. Maio', '140b').
alsoat('A. Maio', '140d').
belongsto('O. Majersky', '28a').
belongsto('S. Majewski', '131').
belongsto('Y. Makida', '81').
belongsto('N. Makovec', '132').
belongsto('B. Malaescu', '136').
belongsto('Pa. Malecki', '84').
belongsto('V.P. Maleev', '138').
belongsto('F. Malek', '58').
belongsto('U. Mallik', '77').
belongsto('D. Malon', '6').
belongsto('C. Malone', '32').
belongsto('S. Maltezos', '10').
belongsto('S. Malyukov', '36').
belongsto('J. Mamuzic', '174').
belongsto('G. Mancini', '51').
belongsto('I. Mandić', '1').
belongsto('L. Manhaes de Andrade Filho', '80a').
belongsto('I.M. Maniatis', '162').
belongsto('J. Manjarres Ramos', '48').
belongsto('K.H. Mankinen', '96').
belongsto('A. Mann', '114').
belongsto('A. Manousos', '76').
belongsto('B. Mansoulie', '145').
belongsto('I. Manthos', '162').
belongsto('S. Manzoni', '120').
belongsto('A. Marantis', '162').
belongsto('G. Marceca', '30').
belongsto('L. Marchese', '135').
belongsto('G. Marchiori', '136').
belongsto('M. Marcisovsky', '141').
belongsto('C. Marcon', '96').
belongsto('C.A. Marin Tobon', '36').
belongsto('M. Marjanovic', '38').
belongsto('F. Marroquim', '80b').
belongsto('Z. Marshall', '18').
belongsto('M.U.F Martensson', '172').
belongsto('S. Marti-Garcia', '174').
belongsto('C.B. Martin', '126').
belongsto('T.A. Martin', '178').
belongsto('V.J. Martin', '50').
belongsto('B. Martin dit Latour', '17').
belongsto('M. Martinez', '14').
alsoat('M. Martinez', 'y').
belongsto('V.I. Martinez Outschoorn', '102').
belongsto('S. Martin-Haugh', '144').
belongsto('V.S. Martoiu', '27b').
belongsto('A.C. Martyniuk', '94').
belongsto('A. Marzin', '36').
belongsto('L. Masetti', '99').
belongsto('T. Mashimo', '163').
belongsto('R. Mashinistov', '110').
belongsto('J. Masik', '100').
belongsto('A.L. Maslennikov', '122b').
alsoat('A.L. Maslennikov', '122a').
belongsto('L.H. Mason', '104').
belongsto('L. Massa', '73a').
alsoat('L. Massa', '73b').
belongsto('P. Massarotti', '69a').
alsoat('P. Massarotti', '69b').
belongsto('P. Mastrandrea', '71a').
alsoat('P. Mastrandrea', '71b').
belongsto('A. Mastroberardino', '41b').
alsoat('A. Mastroberardino', '41a').
belongsto('T. Masubuchi', '163').
belongsto('A. Matic', '114').
belongsto('P. Mättig', '24').
belongsto('J. Maurer', '27b').
belongsto('B. Maček', '91').
belongsto('S.J. Maxfield', '90').
belongsto('D.A. Maximov', '122b').
alsoat('D.A. Maximov', '122a').
belongsto('R. Mazini', '158').
belongsto('I. Maznas', '162').
belongsto('S.M. Mazza', '146').
belongsto('S.P. Mc Kee', '105').
belongsto('T.G. McCarthy', '115').
belongsto('L.I. McClymont', '94').
belongsto('W.P. McCormack', '18').
belongsto('E.F. McDonald', '104').
belongsto('J.A. Mcfayden', '36').
belongsto('M.A. McKay', '42').
belongsto('K.D. McLean', '176').
belongsto('S.J. McMahon', '144').
belongsto('P.C. McNamara', '104').
belongsto('C.J. McNicol', '178').
belongsto('R.A. McPherson', '176').
alsoat('R.A. McPherson', 'ad').
belongsto('J.E. Mdhluli', '33c').
belongsto('Z.A. Meadows', '102').
belongsto('S. Meehan', '148').
belongsto('T. Megy', '52').
belongsto('S. Mehlhase', '114').
belongsto('A. Mehta', '90').
belongsto('T. Meideck', '58').
belongsto('B. Meirose', '43').
belongsto('D. Melini', '174').
belongsto('B.R. Mellado Garcia', '33c').
belongsto('J.D. Mellenthin', '53').
belongsto('M. Melo', '28a').
belongsto('F. Meloni', '46').
belongsto('A. Melzer', '24').
belongsto('S.B. Menary', '100').
belongsto('E.D. Mendes Gouveia', '140a').
alsoat('E.D. Mendes Gouveia', '140e').
belongsto('L. Meng', '36').
belongsto('X.T. Meng', '105').
belongsto('S. Menke', '115').
belongsto('E. Meoni', '41b').
alsoat('E. Meoni', '41a').
belongsto('S. Mergelmeyer', '19').
belongsto('S.A.M. Merkt', '139').
belongsto('C. Merlassino', '20').
belongsto('P. Mermod', '54').
belongsto('L. Merola', '69a').
alsoat('L. Merola', '69b').
belongsto('C. Meroni', '68a').
belongsto('J.K.R. Meshreki', '151').
belongsto('A. Messina', '72a').
alsoat('A. Messina', '72b').
belongsto('J. Metcalfe', '6').
belongsto('A.S. Mete', '171').
belongsto('C. Meyer', '65').
belongsto('J. Meyer', '160').
belongsto('J-P. Meyer', '145').
belongsto('H. Meyer Zu Theenhausen', '61a').
belongsto('F. Miano', '156').
belongsto('R.P. Middleton', '144').
belongsto('L. Mijović', '0').
belongsto('G. Mikenberg', '180').
belongsto('M. Mikestikova', '141').
belongsto('M. Mikuž', '1').
belongsto('M. Milesi', '104').
belongsto('A. Milic', '167').
belongsto('D.A. Millar', '92').
belongsto('D.W. Miller', '37').
belongsto('A. Milov', '180').
belongsto('D.A. Milstead', '45a').
alsoat('D.A. Milstead', '45b').
belongsto('R.A. Mina', '153').
alsoat('R.A. Mina', 'q').
belongsto('A.A. Minaenko', '123').
belongsto('M. Miñano Moya', '174').
belongsto('I.A. Minashvili', '159b').
belongsto('A.I. Mincer', '124').
belongsto('B. Mindur', '83a').
belongsto('M. Mineev', '79').
belongsto('Y. Minegishi', '163').
belongsto('Y. Ming', '181').
belongsto('L.M. Mir', '14').
belongsto('A. Mirto', '67a').
alsoat('A. Mirto', '67b').
belongsto('K.P. Mistry', '137').
belongsto('T. Mitani', '179').
belongsto('J. Mitrevski', '114').
belongsto('V.A. Mitsou', '174').
belongsto('M. Mittal', '60c').
belongsto('A. Miucci', '20').
belongsto('P.S. Miyagawa', '149').
belongsto('A. Mizukami', '81').
belongsto('J.U. Mjörnmark', '96').
belongsto('T. Mkrtchyan', '184').
belongsto('M. Mlynarikova', '143').
belongsto('T. Moa', '45a').
alsoat('T. Moa', '45b').
belongsto('K. Mochizuki', '109').
belongsto('P. Mogg', '52').
belongsto('S. Mohapatra', '39').
belongsto('R. Moles-Valls', '24').
belongsto('M.C. Mondragon', '106').
belongsto('K. Mönig', '46').
belongsto('J. Monk', '40').
belongsto('E. Monnier', '101').
belongsto('A. Montalbano', '152').
belongsto('J. Montejo Berlingen', '36').
belongsto('M. Montella', '94').
belongsto('F. Monticelli', '88').
belongsto('S. Monzani', '68a').
belongsto('N. Morange', '132').
belongsto('D. Moreno', '22').
belongsto('M. Moreno Llácer', '36').
belongsto('P. Morettini', '55b').
belongsto('M. Morgenstern', '120').
belongsto('S. Morgenstern', '48').
belongsto('D. Mori', '152').
belongsto('M. Morii', '59').
belongsto('M. Morinaga', '179').
belongsto('V. Morisbak', '134').
belongsto('A.K. Morley', '36').
belongsto('G. Mornacchi', '36').
belongsto('A.P. Morris', '94').
belongsto('L. Morvaj', '155').
belongsto('P. Moschovakos', '10').
belongsto('M. Mosidze', '159b').
belongsto('H.J. Moss', '149').
belongsto('J. Moss', '31').
alsoat('J. Moss', 'n').
belongsto('K. Motohashi', '165').
belongsto('E. Mountricha', '36').
belongsto('E.J.W. Moyse', '102').
belongsto('S. Muanza', '101').
belongsto('F. Mueller', '115').
belongsto('J. Mueller', '139').
belongsto('R.S.P. Mueller', '114').
belongsto('D. Muenstermann', '89').
belongsto('G.A. Mullier', '96').
belongsto('F.J. Munoz Sanchez', '100').
belongsto('P. Murin', '28b').
belongsto('W.J. Murray', '178').
alsoat('W.J. Murray', '144').
belongsto('A. Murrone', '68a').
alsoat('A. Murrone', '68b').
belongsto('M. Muškinja', '91').
belongsto('C. Mwewa', '33a').
belongsto('A.G. Myagkov', '123').
alsoat('A.G. Myagkov', 'ao').
belongsto('J. Myers', '131').
belongsto('M. Myska', '142').
belongsto('B.P. Nachman', '18').
belongsto('O. Nackenhorst', '47').
belongsto('K. Nagai', '135').
belongsto('K. Nagano', '81').
belongsto('Y. Nagasaka', '62').
belongsto('M. Nagel', '52').
belongsto('E. Nagy', '101').
belongsto('A.M. Nairz', '36').
belongsto('Y. Nakahama', '117').
belongsto('K. Nakamura', '81').
belongsto('T. Nakamura', '163').
belongsto('I. Nakano', '127').
belongsto('H. Nanjo', '133').
belongsto('F. Napolitano', '61a').
belongsto('R.F. Naranjo Garcia', '46').
belongsto('R. Narayan', '11').
belongsto('D.I. Narrias Villar', '61a').
belongsto('I. Naryshkin', '138').
belongsto('T. Naumann', '46').
belongsto('G. Navarro', '22').
belongsto('H.A. Neal', '105').
alsoat('H.A. Neal', '*').
belongsto('P.Y. Nechaeva', '110').
belongsto('F. Nechansky', '46').
belongsto('T.J. Neep', '145').
belongsto('A. Negri', '70a').
alsoat('A. Negri', '70b').
belongsto('M. Negrini', '23b').
belongsto('S. Nektarijevic', '119').
belongsto('C. Nellist', '53').
belongsto('M.E. Nelson', '135').
belongsto('S. Nemecek', '141').
belongsto('P. Nemethy', '124').
belongsto('M. Nessi', '36').
alsoat('M. Nessi', 'e').
belongsto('M.S. Neubauer', '173').
belongsto('M. Neumann', '182').
belongsto('P.R. Newman', '21').
belongsto('T.Y. Ng', '63c').
belongsto('Y.S. Ng', '19').
belongsto('Y.W.Y. Ng', '171').
belongsto('H.D.N. Nguyen', '101').
belongsto('T. Nguyen Manh', '109').
belongsto('E. Nibigira', '38').
belongsto('R.B. Nickerson', '135').
belongsto('R. Nicolaidou', '145').
belongsto('D.S. Nielsen', '40').
belongsto('J. Nielsen', '146').
belongsto('N. Nikiforou', '11').
belongsto('V. Nikolaenko', '123').
alsoat('V. Nikolaenko', 'ao').
belongsto('I. Nikolic-Audit', '136').
belongsto('K. Nikolopoulos', '21').
belongsto('P. Nilsson', '29').
belongsto('H.R. Nindhito', '54').
belongsto('Y. Ninomiya', '81').
belongsto('A. Nisati', '72a').
belongsto('N. Nishu', '60c').
belongsto('R. Nisius', '115').
belongsto('I. Nitsche', '47').
belongsto('T. Nitta', '179').
belongsto('T. Nobe', '163').
belongsto('Y. Noguchi', '85').
belongsto('M. Nomachi', '133').
belongsto('I. Nomidis', '136').
belongsto('M.A. Nomura', '29').
belongsto('M. Nordberg', '36').
belongsto('N. Norjoharuddeen', '135').
belongsto('T. Novak', '91').
belongsto('O. Novgorodova', '48').
belongsto('R. Novotny', '142').
belongsto('L. Nozka', '130').
belongsto('K. Ntekas', '171').
belongsto('E. Nurse', '94').
belongsto('F. Nuti', '104').
belongsto('F.G. Oakham', '34').
alsoat('F.G. Oakham', 'aw').
belongsto('H. Oberlack', '115').
belongsto('J. Ocariz', '136').
belongsto('A. Ochi', '82').
belongsto('I. Ochoa', '39').
belongsto('J.P. Ochoa-Ricoux', '147a').
belongsto('K. O\'Connor', '26').
belongsto('S. Oda', '87').
belongsto('S. Odaka', '81').
belongsto('S. Oerdek', '53').
belongsto('A. Ogrodnik', '83a').
belongsto('A. Oh', '100').
belongsto('S.H. Oh', '49').
belongsto('C.C. Ohm', '154').
belongsto('H. Oide', '55b').
alsoat('H. Oide', '55a').
belongsto('M.L. Ojeda', '167').
belongsto('H. Okawa', '169').
belongsto('Y. Okazaki', '85').
belongsto('Y. Okumura', '163').
belongsto('T. Okuyama', '81').
belongsto('A. Olariu', '27b').
belongsto('L.F. Oleiro Seabra', '140a').
belongsto('S.A. Olivares Pino', '147a').
belongsto('D. Oliveira Damazio', '29').
belongsto('J.L. Oliver', '1').
belongsto('M.J.R. Olsson', '171').
belongsto('A. Olszewski', '84').
belongsto('J. Olszowska', '84').
belongsto('D.C. O\'Neil', '152').
belongsto('A. Onofre', '140a').
alsoat('A. Onofre', '140e').
belongsto('K. Onogi', '117').
belongsto('P.U.E. Onyisi', '11').
belongsto('H. Oppen', '134').
belongsto('M.J. Oreglia', '37').
belongsto('G.E. Orellana', '88').
belongsto('Y. Oren', '161').
belongsto('D. Orestano', '74a').
alsoat('D. Orestano', '74b').
belongsto('N. Orlando', '14').
belongsto('R.S. Orr', '167').
belongsto('B. Osculati', '55b').
alsoat('B. Osculati', '55a').
alsoat('B. Osculati', '*').
belongsto('V. O\'Shea', '57').
belongsto('R. Ospanov', '60a').
belongsto('G. Otero y Garzon', '30').
belongsto('H. Otono', '87').
belongsto('M. Ouchrif', '35d').
belongsto('F. Ould-Saada', '134').
belongsto('A. Ouraou', '145').
belongsto('Q. Ouyang', '15a').
belongsto('M. Owen', '57').
belongsto('R.E. Owen', '21').
belongsto('V.E. Ozcan', '12c').
belongsto('N. Ozturk', '8').
belongsto('J. Pacalt', '130').
belongsto('H.A. Pacey', '32').
belongsto('K. Pachal', '49').
belongsto('A. Pacheco Pages', '14').
belongsto('C. Padilla Aranda', '14').
belongsto('S. Pagan Griso', '18').
belongsto('M. Paganini', '183').
belongsto('G. Palacino', '65').
belongsto('S. Palazzo', '50').
belongsto('S. Palestini', '36').
belongsto('M. Palka', '83b').
belongsto('D. Pallin', '38').
belongsto('I. Panagoulias', '10').
belongsto('C.E. Pandini', '36').
belongsto('J.G. Panduro Vazquez', '93').
belongsto('P. Pani', '46').
belongsto('G. Panizzo', '66a').
alsoat('G. Panizzo', '66c').
belongsto('L. Paolozzi', '54').
belongsto('K. Papageorgiou', '9').
alsoat('K. Papageorgiou', 'i').
belongsto('A. Paramonov', '6').
belongsto('D. Paredes Hernandez', '63b').
belongsto('S.R. Paredes Saenz', '135').
belongsto('B. Parida', '166').
belongsto('T.H. Park', '167').
belongsto('A.J. Parker', '89').
belongsto('M.A. Parker', '32').
belongsto('F. Parodi', '55b').
alsoat('F. Parodi', '55a').
belongsto('E.W.P. Parrish', '121').
belongsto('J.A. Parsons', '39').
belongsto('U. Parzefall', '52').
belongsto('L. Pascual Dominguez', '136').
belongsto('V.R. Pascuzzi', '167').
belongsto('J.M.P. Pasner', '146').
belongsto('E. Pasqualucci', '72a').
belongsto('S. Passaggio', '55b').
belongsto('F. Pastore', '93').
belongsto('P. Pasuwan', '45a').
alsoat('P. Pasuwan', '45b').
belongsto('S. Pataraia', '99').
belongsto('J.R. Pater', '100').
belongsto('A. Pathak', '181').
belongsto('T. Pauly', '36').
belongsto('B. Pearson', '115').
belongsto('M. Pedersen', '134').
belongsto('L. Pedraza Diaz', '119').
belongsto('R. Pedro', '140a').
alsoat('R. Pedro', '140b').
belongsto('S.V. Peleganchuk', '122b').
alsoat('S.V. Peleganchuk', '122a').
belongsto('O. Penc', '141').
belongsto('C. Peng', '15a').
belongsto('H. Peng', '60a').
belongsto('B.S. Peralva', '80a').
belongsto('M.M. Perego', '132').
belongsto('A.P. Pereira Peixoto', '140a').
alsoat('A.P. Pereira Peixoto', '140e').
belongsto('D.V. Perepelitsa', '29').
belongsto('F. Peri', '19').
belongsto('L. Perini', '68a').
alsoat('L. Perini', '68b').
belongsto('H. Pernegger', '36').
belongsto('S. Perrella', '69a').
alsoat('S. Perrella', '69b').
belongsto('V.D. Peshekhonov', '79').
alsoat('V.D. Peshekhonov', '*').
belongsto('K. Peters', '46').
belongsto('R.F.Y. Peters', '100').
belongsto('B.A. Petersen', '36').
belongsto('T.C. Petersen', '40').
belongsto('E. Petit', '58').
belongsto('A. Petridis', '1').
belongsto('C. Petridou', '162').
belongsto('P. Petroff', '132').
belongsto('M. Petrov', '135').
belongsto('F. Petrucci', '74a').
alsoat('F. Petrucci', '74b').
belongsto('M. Pettee', '183').
belongsto('N.E. Pettersson', '102').
belongsto('K. Petukhova', '143').
belongsto('A. Peyaud', '145').
belongsto('R. Pezoa', '147b').
belongsto('T. Pham', '104').
belongsto('F.H. Phillips', '106').
belongsto('P.W. Phillips', '144').
belongsto('M.W. Phipps', '173').
belongsto('G. Piacquadio', '155').
belongsto('E. Pianori', '18').
belongsto('A. Picazio', '102').
belongsto('R.H. Pickles', '100').
belongsto('R. Piegaia', '30').
belongsto('J.E. Pilcher', '37').
belongsto('A.D. Pilkington', '100').
belongsto('M. Pinamonti', '73a').
alsoat('M. Pinamonti', '73b').
belongsto('J.L. Pinfold', '3').
belongsto('M. Pitt', '180').
belongsto('L. Pizzimento', '73a').
alsoat('L. Pizzimento', '73b').
belongsto('M.-A. Pleier', '29').
belongsto('V. Pleskot', '143').
belongsto('E. Plotnikova', '79').
belongsto('D. Pluth', '78').
belongsto('P. Podberezko', '122b').
alsoat('P. Podberezko', '122a').
belongsto('R. Poettgen', '96').
belongsto('R. Poggi', '54').
belongsto('L. Poggioli', '132').
belongsto('I. Pogrebnyak', '106').
belongsto('D. Pohl', '24').
belongsto('I. Pokharel', '53').
belongsto('G. Polesello', '70a').
belongsto('A. Poley', '18').
belongsto('A. Policicchio', '72a').
alsoat('A. Policicchio', '72b').
belongsto('R. Polifka', '36').
belongsto('A. Polini', '23b').
belongsto('C.S. Pollard', '46').
belongsto('V. Polychronakos', '29').
belongsto('D. Ponomarenko', '112').
belongsto('L. Pontecorvo', '36').
belongsto('G.A. Popeneciu', '27d').
belongsto('D.M. Portillo Quintero', '136').
belongsto('S. Pospisil', '142').
belongsto('K. Potamianos', '46').
belongsto('I.N. Potrap', '79').
belongsto('C.J. Potter', '32').
belongsto('H. Potti', '11').
belongsto('T. Poulsen', '96').
belongsto('J. Poveda', '36').
belongsto('T.D. Powell', '149').
belongsto('M.E. Pozo Astigarraga', '36').
belongsto('P. Pralavorio', '101').
belongsto('S. Prell', '78').
belongsto('D. Price', '100').
belongsto('M. Primavera', '67a').
belongsto('S. Prince', '103').
belongsto('M.L. Proffitt', '148').
belongsto('N. Proklova', '112').
belongsto('K. Prokofiev', '63c').
belongsto('F. Prokoshin', '147b').
belongsto('S. Protopopescu', '29').
belongsto('J. Proudfoot', '6').
belongsto('M. Przybycien', '83a').
belongsto('A. Puri', '173').
belongsto('P. Puzo', '132').
belongsto('J. Qian', '105').
belongsto('Y. Qin', '100').
belongsto('A. Quadt', '53').
belongsto('M. Queitsch-Maitland', '46').
belongsto('A. Qureshi', '1').
belongsto('P. Rados', '104').
belongsto('F. Ragusa', '68a').
alsoat('F. Ragusa', '68b').
belongsto('G. Rahal', '97').
belongsto('J.A. Raine', '54').
belongsto('S. Rajagopalan', '29').
belongsto('A. Ramirez Morales', '92').
belongsto('K. Ran', '15a').
alsoat('K. Ran', '15d').
belongsto('T. Rashid', '132').
belongsto('S. Raspopov', '5').
belongsto('M.G. Ratti', '68a').
alsoat('M.G. Ratti', '68b').
belongsto('D.M. Rauch', '46').
belongsto('F. Rauscher', '114').
belongsto('S. Rave', '99').
belongsto('B. Ravina', '149').
belongsto('I. Ravinovich', '180').
belongsto('J.H. Rawling', '100').
belongsto('M. Raymond', '36').
belongsto('A.L. Read', '134').
belongsto('N.P. Readioff', '58').
belongsto('M. Reale', '67a').
alsoat('M. Reale', '67b').
belongsto('D.M. Rebuzzi', '70a').
alsoat('D.M. Rebuzzi', '70b').
belongsto('A. Redelbach', '177').
belongsto('G. Redlinger', '29').
belongsto('R.G. Reed', '33c').
belongsto('K. Reeves', '43').
belongsto('L. Rehnisch', '19').
belongsto('J. Reichert', '137').
belongsto('D. Reikher', '161').
belongsto('A. Reiss', '99').
belongsto('A. Rej', '151').
belongsto('C. Rembser', '36').
belongsto('H. Ren', '15a').
belongsto('M. Rescigno', '72a').
belongsto('S. Resconi', '68a').
belongsto('E.D. Resseguie', '137').
belongsto('S. Rettie', '175').
belongsto('E. Reynolds', '21').
belongsto('O.L. Rezanova', '122b').
alsoat('O.L. Rezanova', '122a').
belongsto('P. Reznicek', '143').
belongsto('E. Ricci', '75a').
alsoat('E. Ricci', '75b').
belongsto('R. Richter', '115').
belongsto('S. Richter', '46').
belongsto('E. Richter-Was', '83b').
belongsto('O. Ricken', '24').
belongsto('M. Ridel', '136').
belongsto('P. Rieck', '115').
belongsto('C.J. Riegel', '182').
belongsto('O. Rifki', '46').
belongsto('M. Rijssenbeek', '155').
belongsto('A. Rimoldi', '70a').
alsoat('A. Rimoldi', '70b').
belongsto('M. Rimoldi', '20').
belongsto('L. Rinaldi', '23b').
belongsto('G. Ripellino', '154').
belongsto('B. Ristić', '89').
belongsto('E. Ritsch', '36').
belongsto('I. Riu', '14').
belongsto('J.C. Rivera Vergara', '147a').
belongsto('F. Rizatdinova', '129').
belongsto('E. Rizvi', '92').
belongsto('C. Rizzi', '14').
belongsto('R.T. Roberts', '100').
belongsto('S.H. Robertson', '103').
alsoat('S.H. Robertson', 'ad').
belongsto('D. Robinson', '32').
belongsto('J.E.M. Robinson', '46').
belongsto('A. Robson', '57').
belongsto('E. Rocco', '99').
belongsto('C. Roda', '71a').
alsoat('C. Roda', '71b').
belongsto('Y. Rodina', '101').
belongsto('S. Rodriguez Bosca', '174').
belongsto('A. Rodriguez Perez', '14').
belongsto('D. Rodriguez Rodriguez', '174').
belongsto('A.M. Rodríguez Vera', '168b').
belongsto('S. Roe', '36').
belongsto('O. Røhne', '134').
belongsto('R. Röhrig', '115').
belongsto('C.P.A. Roland', '65').
belongsto('J. Roloff', '59').
belongsto('A. Romaniouk', '112').
belongsto('M. Romano', '23b').
alsoat('M. Romano', '23a').
belongsto('N. Rompotis', '90').
belongsto('M. Ronzani', '124').
belongsto('L. Roos', '136').
belongsto('S. Rosati', '72a').
belongsto('K. Rosbach', '52').
belongsto('N-A. Rosien', '53').
belongsto('B.J. Rosser', '137').
belongsto('E. Rossi', '46').
belongsto('E. Rossi', '74a').
alsoat('E. Rossi', '74b').
belongsto('E. Rossi', '69a').
alsoat('E. Rossi', '69b').
belongsto('L.P. Rossi', '55b').
belongsto('L. Rossini', '68a').
alsoat('L. Rossini', '68b').
belongsto('J.H.N. Rosten', '32').
belongsto('R. Rosten', '14').
belongsto('M. Rotaru', '27b').
belongsto('J. Rothberg', '148').
belongsto('D. Rousseau', '132').
belongsto('D. Roy', '33c').
belongsto('A. Rozanov', '101').
belongsto('Y. Rozen', '160').
belongsto('X. Ruan', '33c').
belongsto('F. Rubbo', '153').
belongsto('F. Ròhr', '52').
belongsto('A. Ruiz-Martinez', '174').
belongsto('Z. Rurikova', '52').
belongsto('N.A. Rusakovich', '79').
belongsto('H.L. Russell', '103').
belongsto('L. Rustige', '38').
alsoat('L. Rustige', '47').
belongsto('J.P. Rutherfoord', '7').
belongsto('E.M. Ròttinger', '46').
alsoat('E.M. Ròttinger', 'k').
belongsto('Y.F. Ryabov', '138').
belongsto('M. Rybar', '39').
belongsto('G. Rybkin', '132').
belongsto('S. Ryu', '6').
belongsto('A. Ryzhov', '123').
belongsto('G.F. Rzehorz', '53').
belongsto('P. Sabatini', '53').
belongsto('G. Sabato', '120').
belongsto('S. Sacerdoti', '132').
belongsto('H.F-W. Sadrozinski', '146').
belongsto('R. Sadykov', '79').
belongsto('F. Safai Tehrani', '72a').
belongsto('P. Saha', '121').
belongsto('S. Saha', '103').
belongsto('M. Sahinsoy', '61a').
belongsto('A. Sahu', '182').
belongsto('M. Saimpert', '46').
belongsto('M. Saito', '163').
belongsto('T. Saito', '163').
belongsto('H. Sakamoto', '163').
belongsto('A. Sakharov', '124').
alsoat('A. Sakharov', 'an').
belongsto('D. Salamani', '54').
belongsto('G. Salamanna', '74a').
alsoat('G. Salamanna', '74b').
belongsto('J.E. Salazar Loyola', '147b').
belongsto('P.H. Sales De Bruin', '172').
belongsto('D. Salihagic', '115').
alsoat('D. Salihagic', '*').
belongsto('A. Salnikov', '153').
belongsto('J. Salt', '174').
belongsto('D. Salvatore', '41b').
alsoat('D. Salvatore', '41a').
belongsto('F. Salvatore', '156').
belongsto('A. Salvucci', '63a').
alsoat('A. Salvucci', '63b').
alsoat('A. Salvucci', '63c').
belongsto('A. Salzburger', '36').
belongsto('J. Samarati', '36').
belongsto('D. Sammel', '52').
belongsto('D. Sampsonidis', '162').
belongsto('D. Sampsonidou', '162').
belongsto('J. Sánchez', '174').
belongsto('A. Sanchez Pineda', '66a').
alsoat('A. Sanchez Pineda', '66c').
belongsto('H. Sandaker', '134').
belongsto('C.O. Sander', '46').
belongsto('M. Sandhoff', '182').
belongsto('C. Sandoval', '22').
belongsto('D.P.C. Sankey', '144').
belongsto('M. Sannino', '55b').
alsoat('M. Sannino', '55a').
belongsto('Y. Sano', '117').
belongsto('A. Sansoni', '51').
belongsto('C. Santoni', '38').
belongsto('H. Santos', '140a').
alsoat('H. Santos', '140b').
belongsto('S.N. Santpur', '18').
belongsto('A. Santra', '174').
belongsto('A. Sapronov', '79').
belongsto('J.G. Saraiva', '140a').
alsoat('J.G. Saraiva', '140d').
belongsto('O. Sasaki', '81').
belongsto('K. Sato', '169').
belongsto('E. Sauvan', '5').
belongsto('P. Savard', '167').
alsoat('P. Savard', 'aw').
belongsto('N. Savic', '115').
belongsto('R. Sawada', '163').
belongsto('C. Sawyer', '144').
belongsto('L. Sawyer', '95').
alsoat('L. Sawyer', 'al').
belongsto('C. Sbarra', '23b').
belongsto('A. Sbrizzi', '23a').
belongsto('T. Scanlon', '94').
belongsto('J. Schaarschmidt', '148').
belongsto('P. Schacht', '115').
belongsto('B.M. Schachtner', '114').
belongsto('D. Schaefer', '37').
belongsto('L. Schaefer', '137').
belongsto('J. Schaeffer', '99').
belongsto('S. Schaepe', '36').
belongsto('U. Schäfer', '99').
belongsto('A.C. Schaffer', '132').
belongsto('D. Schaile', '114').
belongsto('R.D. Schamberger', '155').
belongsto('N. Scharmberg', '100').
belongsto('V.A. Schegelsky', '138').
belongsto('D. Scheirich', '143').
belongsto('F. Schenck', '19').
belongsto('M. Schernau', '171').
belongsto('C. Schiavi', '55b').
alsoat('C. Schiavi', '55a').
belongsto('S. Schier', '146').
belongsto('L.K. Schildgen', '24').
belongsto('Z.M. Schillaci', '26').
belongsto('E.J. Schioppa', '36').
belongsto('M. Schioppa', '41b').
alsoat('M. Schioppa', '41a').
belongsto('K.E. Schleicher', '52').
belongsto('S. Schlenker', '36').
belongsto('K.R. Schmidt-Sommerfeld', '115').
belongsto('K. Schmieden', '36').
belongsto('C. Schmitt', '99').
belongsto('S. Schmitt', '46').
belongsto('S. Schmitz', '99').
belongsto('J.C. Schmoeckel', '46').
belongsto('U. Schnoor', '52').
belongsto('L. Schoeffel', '145').
belongsto('A. Schoening', '61b').
belongsto('E. Schopf', '135').
belongsto('M. Schott', '99').
belongsto('J.F.P. Schouwenberg', '119').
belongsto('J. Schovancova', '36').
belongsto('S. Schramm', '54').
belongsto('A. Schulte', '99').
belongsto('H-C. Schultz-Coulon', '61a').
belongsto('M. Schumacher', '52').
belongsto('B.A. Schumm', '146').
belongsto('Ph. Schune', '145').
belongsto('A. Schwartzman', '153').
belongsto('T.A. Schwarz', '105').
belongsto('Ph. Schwemling', '145').
belongsto('R. Schwienhorst', '106').
belongsto('A. Sciandra', '24').
belongsto('G. Sciolla', '26').
belongsto('M. Scornajenghi', '41b').
alsoat('M. Scornajenghi', '41a').
belongsto('F. Scuri', '71a').
belongsto('F. Scutti', '104').
belongsto('L.M. Scyboz', '115').
belongsto('C.D. Sebastiani', '72a').
alsoat('C.D. Sebastiani', '72b').
belongsto('P. Seema', '19').
belongsto('S.C. Seidel', '118').
belongsto('A. Seiden', '146').
belongsto('T. Seiss', '37').
belongsto('J.M. Seixas', '80b').
belongsto('G. Sekhniaidze', '69a').
belongsto('K. Sekhon', '105').
belongsto('S.J. Sekula', '42').
belongsto('N. Semprini-Cesari', '23b').
alsoat('N. Semprini-Cesari', '23a').
belongsto('S. Sen', '49').
belongsto('S. Senkin', '38').
belongsto('C. Serfon', '76').
belongsto('L. Serin', '132').
belongsto('L. Serkin', '66a').
alsoat('L. Serkin', '66b').
belongsto('M. Sessa', '60a').
belongsto('H. Severini', '128').
belongsto('F. Sforza', '170').
belongsto('A. Sfyrla', '54').
belongsto('E. Shabalina', '53').
belongsto('J.D. Shahinian', '146').
belongsto('N.W. Shaikh', '45a').
alsoat('N.W. Shaikh', '45b').
belongsto('D. Shaked Renous', '180').
belongsto('L.Y. Shan', '15a').
belongsto('R. Shang', '173').
belongsto('J.T. Shank', '25').
belongsto('M. Shapiro', '18').
belongsto('A. Sharma', '135').
belongsto('A.S. Sharma', '1').
belongsto('P.B. Shatalov', '111').
belongsto('K. Shaw', '156').
belongsto('S.M. Shaw', '100').
belongsto('A. Shcherbakova', '138').
belongsto('Y. Shen', '128').
belongsto('N. Sherafati', '34').
belongsto('A.D. Sherman', '25').
belongsto('P. Sherwood', '94').
belongsto('L. Shi', '158').
alsoat('L. Shi', 'as').
belongsto('S. Shimizu', '81').
belongsto('C.O. Shimmin', '183').
belongsto('Y. Shimogama', '179').
belongsto('M. Shimojima', '116').
belongsto('I.P.J. Shipsey', '135').
belongsto('S. Shirabe', '87').
belongsto('M. Shiyakova', '79').
alsoat('M. Shiyakova', 'ab').
belongsto('J. Shlomi', '180').
belongsto('A. Shmeleva', '110').
belongsto('M.J. Shochet', '37').
belongsto('S. Shojaii', '104').
belongsto('D.R. Shope', '128').
belongsto('S. Shrestha', '126').
belongsto('E. Shulga', '180').
belongsto('P. Sicho', '141').
belongsto('A.M. Sickles', '173').
belongsto('P.E. Sidebo', '154').
belongsto('E. Sideras Haddad', '33c').
belongsto('O. Sidiropoulou', '36').
belongsto('A. Sidoti', '23b').
alsoat('A. Sidoti', '23a').
belongsto('F. Siegert', '48').
belongsto('Dj. Sijacki', '16').
belongsto('J. Silva', '140a').
belongsto('M. Silva Jr.', '181').
belongsto('M.V. Silva Oliveira', '80a').
belongsto('S.B. Silverstein', '45a').
belongsto('S. Simion', '132').
belongsto('E. Simioni', '99').
belongsto('M. Simon', '99').
belongsto('R. Simoniello', '99').
belongsto('P. Sinervo', '167').
belongsto('N.B. Sinev', '131').
belongsto('M. Sioli', '23b').
alsoat('M. Sioli', '23a').
belongsto('I. Siral', '105').
belongsto('S.Yu. Sivoklokov', '113').
belongsto('J. Sjölin', '45a').
alsoat('J. Sjölin', '45b').
belongsto('E. Skorda', '96').
belongsto('P. Skubic', '128').
belongsto('M. Slawinska', '84').
belongsto('K. Sliwa', '170').
belongsto('R. Slovak', '143').
belongsto('V. Smakhtin', '180').
belongsto('B.H. Smart', '5').
belongsto('J. Smiesko', '28a').
belongsto('N. Smirnov', '112').
belongsto('S.Yu. Smirnov', '112').
belongsto('Y. Smirnov', '112').
belongsto('L.N. Smirnova', '113').
alsoat('L.N. Smirnova', 't').
belongsto('O. Smirnova', '96').
belongsto('J.W. Smith', '53').
belongsto('M. Smizanska', '89').
belongsto('K. Smolek', '142').
belongsto('A. Smykiewicz', '84').
belongsto('A.A. Snesarev', '110').
belongsto('I.M. Snyder', '131').
belongsto('S. Snyder', '29').
belongsto('R. Sobie', '176').
alsoat('R. Sobie', 'ad').
belongsto('A.M. Soffa', '171').
belongsto('A. Soffer', '161').
belongsto('A. Søgaard', '50').
belongsto('F. Sohns', '53').
belongsto('G. Sokhrannyi', '91').
belongsto('C.A. Solans Sanchez', '36').
belongsto('E.Yu. Soldatov', '112').
belongsto('U. Soldevila', '174').
belongsto('A.A. Solodkov', '123').
belongsto('A. Soloshenko', '79').
belongsto('O.V. Solovyanov', '123').
belongsto('V. Solovyev', '138').
belongsto('P. Sommer', '149').
belongsto('H. Son', '170').
belongsto('W. Song', '144').
belongsto('W.Y. Song', '168b').
belongsto('A. Sopczak', '142').
belongsto('F. Sopkova', '28b').
belongsto('C.L. Sotiropoulou', '71a').
alsoat('C.L. Sotiropoulou', '71b').
belongsto('S. Sottocornola', '70a').
alsoat('S. Sottocornola', '70b').
belongsto('R. Soualah', '66a').
alsoat('R. Soualah', '66c').
alsoat('R. Soualah', 'h').
belongsto('A.M. Soukharev', '122b').
alsoat('A.M. Soukharev', '122a').
belongsto('D. South', '46').
belongsto('S. Spagnolo', '67a').
alsoat('S. Spagnolo', '67b').
belongsto('M. Spalla', '115').
belongsto('M. Spangenberg', '178').
belongsto('F. Spanò', '93').
belongsto('D. Sperlich', '19').
belongsto('T.M. Spieker', '61a').
belongsto('R. Spighi', '23b').
belongsto('G. Spigo', '36').
belongsto('L.A. Spiller', '104').
belongsto('M. Spina', '156').
belongsto('D.P. Spiteri', '57').
belongsto('M. Spousta', '143').
belongsto('A. Stabile', '68a').
alsoat('A. Stabile', '68b').
belongsto('B.L. Stamas', '121').
belongsto('R. Stamen', '61a').
belongsto('M. Stamenkovic', '120').
belongsto('S. Stamm', '19').
belongsto('E. Stanecka', '84').
belongsto('R.W. Stanek', '6').
belongsto('B. Stanislaus', '135').
belongsto('M.M. Stanitzki', '46').
belongsto('B. Stapf', '120').
belongsto('E.A. Starchenko', '123').
belongsto('G.H. Stark', '146').
belongsto('J. Stark', '58').
belongsto('S.H Stark', '40').
belongsto('P. Staroba', '141').
belongsto('P. Starovoitov', '61a').
belongsto('S. Stärz', '103').
belongsto('R. Staszewski', '84').
belongsto('G. Stavropoulos', '44').
belongsto('M. Stegler', '46').
belongsto('P. Steinberg', '29').
belongsto('B. Stelzer', '152').
belongsto('H.J. Stelzer', '36').
belongsto('O. Stelzer-Chilton', '168a').
belongsto('H. Stenzel', '56').
belongsto('T.J. Stevenson', '156').
belongsto('G.A. Stewart', '36').
belongsto('M.C. Stockton', '36').
belongsto('G. Stoicea', '27b').
belongsto('M. Stolarski', '140a').
belongsto('P. Stolte', '53').
belongsto('S. Stonjek', '115').
belongsto('A. Straessner', '48').
belongsto('J. Strandberg', '154').
belongsto('S. Strandberg', '45a').
alsoat('S. Strandberg', '45b').
belongsto('M. Strauss', '128').
belongsto('P. Strizenec', '28b').
belongsto('R. Ströhmer', '177').
belongsto('D.M. Strom', '131').
belongsto('R. Stroynowski', '42').
belongsto('A. Strubig', '50').
belongsto('S.A. Stucci', '29').
belongsto('B. Stugu', '17').
belongsto('J. Stupak', '128').
belongsto('N.A. Styles', '46').
belongsto('D. Su', '153').
belongsto('S. Suchek', '61a').
belongsto('Y. Sugaya', '133').
belongsto('V.V. Sulin', '110').
belongsto('M.J. Sullivan', '90').
belongsto('D.M.S. Sultan', '54').
belongsto('S. Sultansoy', '4c').
belongsto('T. Sumida', '85').
belongsto('S. Sun', '105').
belongsto('X. Sun', '3').
belongsto('K. Suruliz', '156').
belongsto('C.J.E. Suster', '157').
belongsto('M.R. Sutton', '156').
belongsto('S. Suzuki', '81').
belongsto('M. Svatos', '141').
belongsto('M. Swiatlowski', '37').
belongsto('S.P. Swift', '2').
belongsto('A. Sydorenko', '99').
belongsto('I. Sykora', '28a').
belongsto('M. Sykora', '143').
belongsto('T. Sykora', '143').
belongsto('D. Ta', '99').
belongsto('K. Tackmann', '46').
alsoat('K. Tackmann', 'z').
belongsto('J. Taenzer', '161').
belongsto('A. Taffard', '171').
belongsto('R. Tafirout', '168a').
belongsto('E. Tahirovic', '92').
belongsto('H. Takai', '29').
belongsto('R. Takashima', '86').
belongsto('K. Takeda', '82').
belongsto('T. Takeshita', '150').
belongsto('Y. Takubo', '81').
belongsto('M. Talby', '101').
belongsto('A.A. Talyshev', '122b').
alsoat('A.A. Talyshev', '122a').
belongsto('J. Tanaka', '163').
belongsto('M. Tanaka', '165').
belongsto('R. Tanaka', '132').
belongsto('B.B. Tannenwald', '126').
belongsto('S. Tapia Araya', '173').
belongsto('S. Tapprogge', '99').
belongsto('A. Tarek Abouelfadl Mohamed', '136').
belongsto('S. Tarem', '160').
belongsto('G. Tarna', '27b').
alsoat('G. Tarna', 'd').
belongsto('G.F. Tartarelli', '68a').
belongsto('P. Tas', '143').
belongsto('M. Tasevsky', '141').
belongsto('T. Tashiro', '85').
belongsto('E. Tassi', '41b').
alsoat('E. Tassi', '41a').
belongsto('A. Tavares Delgado', '140a').
alsoat('A. Tavares Delgado', '140b').
belongsto('Y. Tayalati', '35e').
belongsto('A.J. Taylor', '50').
belongsto('G.N. Taylor', '104').
belongsto('P.T.E. Taylor', '104').
belongsto('W. Taylor', '168b').
belongsto('A.S. Tee', '89').
belongsto('R. Teixeira De Lima', '153').
belongsto('P. Teixeira-Dias', '93').
belongsto('H. Ten Kate', '36').
belongsto('J.J. Teoh', '120').
belongsto('S. Terada', '81').
belongsto('K. Terashi', '163').
belongsto('J. Terron', '98').
belongsto('S. Terzo', '14').
belongsto('M. Testa', '51').
belongsto('R.J. Teuscher', '167').
alsoat('R.J. Teuscher', 'ad').
belongsto('S.J. Thais', '183').
belongsto('T. Theveneaux-Pelzer', '46').
belongsto('F. Thiele', '40').
belongsto('D.W. Thomas', '93').
belongsto('J.P. Thomas', '21').
belongsto('A.S. Thompson', '57').
belongsto('P.D. Thompson', '21').
belongsto('L.A. Thomsen', '183').
belongsto('E. Thomson', '137').
belongsto('Y. Tian', '39').
belongsto('R.E. Ticse Torres', '53').
belongsto('V.O. Tikhomirov', '110').
alsoat('V.O. Tikhomirov', 'ap').
belongsto('Yu.A. Tikhonov', '122b').
alsoat('Yu.A. Tikhonov', '122a').
belongsto('S. Timoshenko', '112').
belongsto('P. Tipton', '183').
belongsto('S. Tisserant', '101').
belongsto('K. Todome', '165').
belongsto('S. Todorova-Nova', '5').
belongsto('S. Todt', '48').
belongsto('J. Tojo', '87').
belongsto('S. Tokár', '28a').
belongsto('K. Tokushuku', '81').
belongsto('E. Tolley', '126').
belongsto('K.G. Tomiwa', '33c').
belongsto('M. Tomoto', '117').
belongsto('L. Tompkins', '153').
alsoat('L. Tompkins', 'q').
belongsto('K. Toms', '118').
belongsto('B. Tong', '59').
belongsto('P. Tornambe', '52').
belongsto('E. Torrence', '131').
belongsto('H. Torres', '48').
belongsto('E. Torró Pastor', '148').
belongsto('C. Tosciri', '135').
belongsto('J. Toth', '101').
alsoat('J. Toth', 'ac').
belongsto('D.R. Tovey', '149').
belongsto('C.J. Treado', '124').
belongsto('T. Trefzger', '177').
belongsto('F. Tresoldi', '156').
belongsto('A. Tricoli', '29').
belongsto('I.M. Trigger', '168a').
belongsto('S. Trincaz-Duvoid', '136').
belongsto('W. Trischuk', '167').
belongsto('B. Trocmé', '58').
belongsto('A. Trofymov', '132').
belongsto('C. Troncon', '68a').
belongsto('M. Trovatelli', '176').
belongsto('F. Trovato', '156').
belongsto('L. Truong', '33b').
belongsto('M. Trzebinski', '84').
belongsto('A. Trzupek', '84').
belongsto('F. Tsai', '46').
belongsto('J.C-L. Tseng', '135').
belongsto('P.V. Tsiareshka', '107').
alsoat('P.V. Tsiareshka', 'aj').
belongsto('A. Tsirigotis', '162').
belongsto('N. Tsirintanis', '9').
belongsto('V. Tsiskaridze', '155').
belongsto('E.G. Tskhadadze', '159a').
belongsto('M. Tsopoulou', '162').
belongsto('I.I. Tsukerman', '111').
belongsto('V. Tsulaia', '18').
belongsto('S. Tsuno', '81').
belongsto('D. Tsybychev', '155').
belongsto('Y. Tu', '63b').
belongsto('A. Tudorache', '27b').
belongsto('V. Tudorache', '27b').
belongsto('T.T. Tulbure', '27a').
belongsto('A.N. Tuna', '59').
belongsto('S. Turchikhin', '79').
belongsto('D. Turgeman', '180').
belongsto('I. Turk Cakir', '4b').
alsoat('I. Turk Cakir', 'u').
belongsto('R.J. Turner', '21').
belongsto('R.T. Turra', '68a').
belongsto('P.M. Tuts', '39').
belongsto('E. Tzovara', '99').
belongsto('G. Ucchielli', '47').
belongsto('I. Ueda', '81').
belongsto('M. Ughetto', '45a').
alsoat('M. Ughetto', '45b').
belongsto('F. Ukegawa', '169').
belongsto('G. Unal', '36').
belongsto('A. Undrus', '29').
belongsto('G. Unel', '171').
belongsto('F.C. Ungaro', '104').
belongsto('Y. Unno', '81').
belongsto('K. Uno', '163').
belongsto('J. Urban', '28b').
belongsto('P. Urquijo', '104').
belongsto('G. Usai', '8').
belongsto('J. Usui', '81').
belongsto('L. Vacavant', '101').
belongsto('V. Vacek', '142').
belongsto('B. Vachon', '103').
belongsto('K.O.H. Vadla', '134').
belongsto('A. Vaidya', '94').
belongsto('C. Valderanis', '114').
belongsto('E. Valdes Santurio', '45a').
alsoat('E. Valdes Santurio', '45b').
belongsto('M. Valente', '54').
belongsto('S. Valentinetti', '23b').
alsoat('S. Valentinetti', '23a').
belongsto('A. Valero', '174').
belongsto('L. Valéry', '46').
belongsto('R.A. Vallance', '21').
belongsto('A. Vallier', '5').
belongsto('J.A. Valls Ferrer', '174').
belongsto('T.R. Van Daalen', '14').
belongsto('P. Van Gemmeren', '6').
belongsto('I. Van Vulpen', '120').
belongsto('M. Vanadia', '73a').
alsoat('M. Vanadia', '73b').
belongsto('W. Vandelli', '36').
belongsto('A. Vaniachine', '166').
belongsto('R. Vari', '72a').
belongsto('E.W. Varnes', '7').
belongsto('C. Varni', '55b').
alsoat('C. Varni', '55a').
belongsto('T. Varol', '42').
belongsto('D. Varouchas', '132').
belongsto('K.E. Varvell', '157').
belongsto('G.A. Vasquez', '147b').
belongsto('J.G. Vasquez', '183').
belongsto('F. Vazeille', '38').
belongsto('D. Vazquez Furelos', '14').
belongsto('T. Vazquez Schroeder', '36').
belongsto('J. Veatch', '53').
belongsto('V. Vecchio', '74a').
alsoat('V. Vecchio', '74b').
belongsto('L.M. Veloce', '167').
belongsto('F. Veloso', '140a').
alsoat('F. Veloso', '140c').
belongsto('S. Veneziano', '72a').
belongsto('A. Ventura', '67a').
alsoat('A. Ventura', '67b').
belongsto('N. Venturi', '36').
belongsto('A. Verbytskyi', '115').
belongsto('V. Vercesi', '70a').
belongsto('M. Verducci', '74a').
alsoat('M. Verducci', '74b').
belongsto('C.M. Vergel Infante', '78').
belongsto('C. Vergis', '24').
belongsto('W. Verkerke', '120').
belongsto('A.T. Vermeulen', '120').
belongsto('J.C. Vermeulen', '120').
belongsto('M.C. Vetterli', '152').
alsoat('M.C. Vetterli', 'aw').
belongsto('N. Viaux Maira', '147b').
belongsto('M. Vicente Barreto Pinto', '54').
belongsto('I. Vichou', '173').
alsoat('I. Vichou', '*').
belongsto('T. Vickey', '149').
belongsto('O.E. Vickey Boeriu', '149').
belongsto('G.H.A. Viehhauser', '135').
belongsto('L. Vigani', '135').
belongsto('M. Villa', '23b').
alsoat('M. Villa', '23a').
belongsto('M. Villaplana Perez', '68a').
alsoat('M. Villaplana Perez', '68b').
belongsto('E. Vilucchi', '51').
belongsto('M.G. Vincter', '34').
belongsto('V.B. Vinogradov', '79').
belongsto('A. Vishwakarma', '46').
belongsto('C. Vittori', '23b').
alsoat('C. Vittori', '23a').
belongsto('I. Vivarelli', '156').
belongsto('M. Vogel', '182').
belongsto('P. Vokac', '142').
belongsto('G. Volpi', '14').
belongsto('S.E. von Buddenbrock', '33c').
belongsto('E. Von Toerne', '24').
belongsto('V. Vorobel', '143').
belongsto('K. Vorobev', '112').
belongsto('M. Vos', '174').
belongsto('J.H. Vossebeld', '90').
belongsto('N. Vranjes', '16').
belongsto('M. Vranjes Milosavljevic', '16').
belongsto('V. Vrba', '142').
belongsto('M. Vreeswijk', '120').
belongsto('T. Šfiligoj', '91').
belongsto('R. Vuillermet', '36').
belongsto('I. Vukotic', '37').
belongsto('T. Ženiš', '28a').
belongsto('L. Živković', '6').
belongsto('P. Wagner', '24').
belongsto('W. Wagner', '182').
belongsto('J. Wagner-Kuhr', '114').
belongsto('H. Wahlberg', '88').
belongsto('S. Wahrmund', '48').
belongsto('K. Wakamiya', '82').
belongsto('V.M. Walbrecht', '115').
belongsto('J. Walder', '89').
belongsto('R. Walker', '114').
belongsto('S.D. Walker', '93').
belongsto('W. Walkowiak', '151').
belongsto('V. Wallangen', '45a').
alsoat('V. Wallangen', '45b').
belongsto('A.M. Wang', '59').
belongsto('C. Wang', '60b').
belongsto('F. Wang', '181').
belongsto('H. Wang', '18').
belongsto('H. Wang', '3').
belongsto('J. Wang', '157').
belongsto('J. Wang', '61b').
belongsto('P. Wang', '42').
belongsto('Q. Wang', '128').
belongsto('R.-J. Wang', '136').
belongsto('R. Wang', '60a').
belongsto('R. Wang', '6').
belongsto('S.M. Wang', '158').
belongsto('W.T. Wang', '60a').
belongsto('W. Wang', '15c').
alsoat('W. Wang', 'ae').
belongsto('W.X. Wang', '60a').
alsoat('W.X. Wang', 'ae').
belongsto('Y. Wang', '60a').
alsoat('Y. Wang', 'am').
belongsto('Z. Wang', '60c').
belongsto('C. Wanotayaroj', '46').
belongsto('A. Warburton', '103').
belongsto('C.P. Ward', '32').
belongsto('D.R. Wardrope', '94').
belongsto('A. Washbrook', '50').
belongsto('A.T. Watson', '21').
belongsto('M.F. Watson', '21').
belongsto('G. Watts', '148').
belongsto('B.M. Waugh', '94').
belongsto('A.F. Webb', '11').
belongsto('S. Webb', '99').
belongsto('C. Weber', '183').
belongsto('M.S. Weber', '20').
belongsto('S.A. Weber', '34').
belongsto('S.M. Weber', '61a').
belongsto('A.R. Weidberg', '135').
belongsto('J. Weingarten', '47').
belongsto('M. Weirich', '99').
belongsto('C. Weiser', '52').
belongsto('P.S. Wells', '36').
belongsto('T. Wenaus', '29').
belongsto('T. Wengler', '36').
belongsto('S. Wenig', '36').
belongsto('N. Wermes', '24').
belongsto('M.D. Werner', '78').
belongsto('P. Werner', '36').
belongsto('M. Wessels', '61a').
belongsto('T.D. Weston', '20').
belongsto('K. Whalen', '131').
belongsto('N.L. Whallon', '148').
belongsto('A.M. Wharton', '89').
belongsto('A.S. White', '105').
belongsto('A. White', '8').
belongsto('M.J. White', '1').
belongsto('R. White', '147b').
belongsto('D. Whiteson', '171').
belongsto('B.W. Whitmore', '89').
belongsto('F.J. Wickens', '144').
belongsto('W. Wiedenmann', '181').
belongsto('M. Wielers', '144').
belongsto('C. Wiglesworth', '40').
belongsto('L.A.M. Wiik-Fuchs', '52').
belongsto('F. Wilk', '100').
belongsto('H.G. Wilkens', '36').
belongsto('L.J. Wilkins', '93').
belongsto('H.H. Williams', '137').
belongsto('S. Williams', '32').
belongsto('C. Willis', '106').
belongsto('S. Willocq', '102').
belongsto('J.A. Wilson', '21').
belongsto('I. Wingerter-Seez', '5').
belongsto('E. Winkels', '156').
belongsto('F. Winklmeier', '131').
belongsto('O.J. Winston', '156').
belongsto('B.T. Winter', '52').
belongsto('M. Wittgen', '153').
belongsto('M. Wobisch', '95').
belongsto('A. Wolf', '99').
belongsto('T.M.H. Wolf', '120').
belongsto('R. Wolff', '101').
belongsto('J. Wollrath', '52').
belongsto('M.W. Wolter', '84').
belongsto('H. Wolters', '140a').
alsoat('H. Wolters', '140c').
belongsto('V.W.S. Wong', '175').
belongsto('N.L. Woods', '146').
belongsto('S.D. Worm', '21').
belongsto('B.K. Wosiek', '84').
belongsto('K.W. Woźniak', '84').
belongsto('K. Wraight', '57').
belongsto('S.L. Wu', '181').
belongsto('X. Wu', '54').
belongsto('Y. Wu', '60a').
belongsto('T.R. Wyatt', '100').
belongsto('B.M. Wynne', '50').
belongsto('S. Xella', '40').
belongsto('Z. Xi', '105').
belongsto('L. Xia', '178').
belongsto('D. Xu', '15a').
belongsto('H. Xu', '60a').
alsoat('H. Xu', 'd').
belongsto('L. Xu', '29').
belongsto('T. Xu', '145').
belongsto('W. Xu', '105').
belongsto('Z. Xu', '153').
belongsto('B. Yabsley', '157').
belongsto('S. Yacoob', '33a').
belongsto('K. Yajima', '133').
belongsto('D.P. Yallup', '94').
belongsto('D. Yamaguchi', '165').
belongsto('Y. Yamaguchi', '165').
belongsto('A. Yamamoto', '81').
belongsto('T. Yamanaka', '163').
belongsto('F. Yamane', '82').
belongsto('M. Yamatani', '163').
belongsto('T. Yamazaki', '163').
belongsto('Y. Yamazaki', '82').
belongsto('Z. Yan', '25').
belongsto('H.J. Yang', '60c').
alsoat('H.J. Yang', '60d').
belongsto('H.T. Yang', '18').
belongsto('S. Yang', '77').
belongsto('X. Yang', '60b').
alsoat('X. Yang', '58').
belongsto('Y. Yang', '163').
belongsto('Z. Yang', '17').
belongsto('W-M. Yao', '18').
belongsto('Y.C. Yap', '46').
belongsto('Y. Yasu', '81').
belongsto('E. Yatsenko', '60c').
alsoat('E. Yatsenko', '60d').
belongsto('J. Ye', '42').
belongsto('S. Ye', '29').
belongsto('I. Yeletskikh', '79').
belongsto('E. Yigitbasi', '25').
belongsto('E. Yildirim', '99').
belongsto('K. Yorita', '179').
belongsto('K. Yoshihara', '137').
belongsto('C.J.S. Young', '36').
belongsto('C. Young', '153').
belongsto('J. Yu', '78').
belongsto('X. Yue', '61a').
belongsto('S.P.Y. Yuen', '24').
belongsto('B. Zabinski', '84').
belongsto('G. Zacharis', '10').
belongsto('E. Zaffaroni', '54').
belongsto('R. Zaidan', '14').
belongsto('A.M. Zaitsev', '123').
alsoat('A.M. Zaitsev', 'ao').
belongsto('T. Zakareishvili', '159b').
belongsto('N. Zakharchuk', '34').
belongsto('S. Zambito', '59').
belongsto('D. Zanzi', '36').
belongsto('D.R. Zaripovas', '57').
belongsto('S.V. Zeißner', '47').
belongsto('C. Zeitnitz', '182').
belongsto('G. Zemaityte', '135').
belongsto('J.C. Zeng', '173').
belongsto('O. Zenin', '123').
belongsto('D. Zerwas', '132').
belongsto('M. Zgubič', '135').
belongsto('D.F. Zhang', '15b').
belongsto('F. Zhang', '181').
belongsto('G. Zhang', '60a').
belongsto('G. Zhang', '15b').
belongsto('H. Zhang', '15c').
belongsto('J. Zhang', '6').
belongsto('L. Zhang', '15c').
belongsto('L. Zhang', '60a').
belongsto('M. Zhang', '173').
belongsto('R. Zhang', '60a').
belongsto('R. Zhang', '24').
belongsto('X. Zhang', '60b').
belongsto('Y. Zhang', '15a').
alsoat('Y. Zhang', '15d').
belongsto('Z. Zhang', '63a').
belongsto('Z. Zhang', '132').
belongsto('P. Zhao', '49').
belongsto('Y. Zhao', '60b').
belongsto('Z. Zhao', '60a').
belongsto('A. Zhemchugov', '79').
belongsto('Z. Zheng', '105').
belongsto('D. Zhong', '173').
belongsto('B. Zhou', '105').
belongsto('C. Zhou', '181').
belongsto('M.S. Zhou', '15a').
alsoat('M.S. Zhou', '15d').
belongsto('M. Zhou', '155').
belongsto('N. Zhou', '60c').
belongsto('Y. Zhou', '7').
belongsto('C.G. Zhu', '60b').
belongsto('H.L. Zhu', '60a').
belongsto('H. Zhu', '15a').
belongsto('J. Zhu', '105').
belongsto('Y. Zhu', '60a').
belongsto('X. Zhuang', '15a').
belongsto('K. Zhukov', '110').
belongsto('V. Zhulanov', '122b').
alsoat('V. Zhulanov', '122a').
belongsto('D. Zieminska', '65').
belongsto('N.I. Zimine', '79').
belongsto('S. Zimmermann', '52').
belongsto('Z. Zinonos', '115').
belongsto('M. Ziolkowski', '151').
belongsto('G. Zobernig', '181').
belongsto('A. Zoccoli', '23b').
alsoat('A. Zoccoli', '23a').
belongsto('K. Zoch', '53').
belongsto('T.G. Zorbas', '149').
belongsto('R. Zou', '37').
belongsto('L. Zwalinski', '36').
