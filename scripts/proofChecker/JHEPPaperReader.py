# -*- coding: utf-8 -*-
"""
#####################################
# JHEPPaperReader.py
#
# Uses xpdf to extract text from PDF
# and convert into knowledge files
# Derived class for JHEP papers
#
# Author: Marcello Barisonzi
#
# $Rev: 233716 $
# $LastChangedDate: 2015-02-19 15:29:12 +0100 (Thu, 19 Feb 2015) $
# $LastChangedBy: atlaspo $
#
####################################
"""

import re
import sys

from XpdfPaperReader import XpdfPaperReader

reload(sys)
sys.setdefaultencoding("utf-8")


class JHEPPaperReader(XpdfPaperReader):
    
    def __init__(self, filename, fp="1", lp="-1"):
        super(JHEPPaperReader, self).__init__(filename, fp, lp)
                        
        ### pattern should be overwritten by subclasses

        self._instituteExc = [re.compile("([a-z]{1,2}\s*(?:\n|:\s*)*Also(?:.*?(?:%s)){2}\.*)" % "|".join(self._excep_countries)),
                              re.compile("[^N,P](^[0-9]{1,3}(?:\n|:\s*)*(?:\([a-z]{1}\)\n*)*?(?:[^0-9]*?\n*?[^0-9]*?(?:%s)){2})" % "|".join(self._excep_countries))
            ]

        
        self._institutePtn = [re.compile("([a-z]{1,2}\s*(?:\n|:\s*)*Also.*?(?:%s)\.*)" % "|".join(self._countries)),
                              re.compile("[^N,P]([0-9]{1,3}(?:\n|:\s*)*(?:\([a-z]{1}\)\n*)*?(?:.*?(?:%s)\.*;*))+" % "|".join(self._countries)),
                              re.compile("(\*(?:\n|:\s*)Deceased\.*)")]

        self._institutePtn += [re.compile("([a-z]{1,2}(?:\n|:\s*)*Also.*?(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,7) ]
        self._institutePtn += [re.compile("[^N,P]([0-9]{1,3}(?:\n|:\s*)*(?:\([a-z]{1}\)\n*)*?.*(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,15) ]       

        self._ptn_id    = re.compile("([0-9]{1,3}):*")
        self._ptn_also  = re.compile("([a-z]{1,2})\s*:*\s*Also at(.*)\.*")
        self._ptn_dec   = re.compile("(\*)\s*:*\s*Deceased\.*")
        self._ptn_subid = re.compile("\(([a-z]{1})\)")
        self._ptn_state = re.compile("(%s)" % "|".join(self._countries))

        self._remove_lines =  [re.compile(ur'^[\u2013]')]
        self._remove_lines += [re.compile("(^proofsJHEP)")]

        return