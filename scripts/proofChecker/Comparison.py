#!/bin/env python2
# -*- coding: utf-8 -*-

import json
import sys
import os
import datetime
from XmlObjReader import XmlObjReader
from PdfObjReader import PdfObjReader
from fuzzywuzzy import fuzz
from SynonymsChecker import SynonymsChecker

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

class Comparison():

    def __init__(self, xml_object, pdf_object):
        self.XMLobj = xml_object
        self.PDFobj = pdf_object
        self.xmlInstitutes = list()
        self.pdfInstitutes = list()
        self.xmlAuthors = list()
        self.pdfAuthors = list()

        self.minInstitutesMaxRatio = 90
        self.minInstitutesMatchRatio = 95

        self.minAuthorsMaxRatio = 95
        self.minAuthorsMatchRatio = 99

        self.missingInstitutes = list()

        self.reportShouldBeMarkedAsDeceased = ""
        self.reportAreMarkedAsDeceasedButShouldNot = ""
        self.reportAuthorsMissing = ""
        self.reportInconsistentPuntuaction = ""
        self.reportInstitutesMissing = ""
        self.reportInstitutesCloseMatch = ""
        self.reportMismatchedAuthors = ""

        self.reportDictionary = dict()
        self.reportPath = ""

    def setDocument(self, document):
        self.reportDictionary["document"] = document
        
    def setFilename(self, filename):
        self.reportDictionary["filename"] = filename

    def setReportPath(self, path):
        self.reportPath = path

    def printXmlInstitutes(self):
        print ("\n" * 5)

        print ("ratio\tinstId\tid\tname")

        for institute in self.xmlInstitutes:
            print institute.match.fuzzRatio,
            print "\t",
            print institute.orgInstId,
            print "\t",
            print institute.id,
            print "\t",
            print institute.name

    def printXmlAuthors(self):
        print ("\n" * 5)
        for author in self.xmlAuthors:
            print "<<-->> ",
            print author.namePaper,
            print author.organization,
            print author.alsoAt,
            print author.firstName,
            print author.lastName,
            if author.deceased:
                print " *********** ",
            else:
                print "",
            print author.inspire,
            print author.orcid,

    def printXmlDeceased(self):
        print ("\n" * 5)
        print ("Deceased in xml file")
        for author in self.xmlAuthors:
            if author.deceased:
                print (author.namePaper)

    def printPdfDeceased(self):
        print ("\n" * 5)
        print ("Deceased in xml file")
        for author in self.pdfAuthors:
            if author.deceased:
                print (author.namePaper.encode("utf-8"))

    def printPdfAuthors(self):
        print ("\n" * 5)

        for author in self.pdfAuthors:
            print "<<-->> " + author.namePaper,
            print author.organization,

    def printPdfInstitutes(self):
        print ("\n" * 5)

        for institute in self.pdfInstitutes:
            print institute.id,
            print institute.name

    def printXmlAlsoAt(self):
        print ("\n" * 5)

        for institute in self.xmlInstitutes:
            if institute.alsoAt:
                print institute.id,
                print institute.name

    def printPdfAlsoAt(self):
        print ("\n" * 5)

        for institute in self.pdfInstitutes:
            if institute.alsoAt:
                print institute.id,
                print institute.name

    def _populate(self):
        self.XMLobj.populate_metadata()
        self.xmlInstitutes = self.XMLobj.populateInstitutes()
        self.pdfInstitutes = self.PDFobj.populateInstitutes()
        self.xmlAuthors = self.XMLobj.populateAuthors()
        self.pdfAuthors = self.PDFobj.populateAuthors()
        self.reportDictionary["ref_code"] = self.XMLobj.refCode
        self.reportDictionary["creation_date"] = self.XMLobj.creationDate
        self.reportDictionary["ref_date"] = self.XMLobj.refDate

        self.PDFobj.populatePublisher()
        self.reportDictionary["publisher"] = self.PDFobj.publisher

    def _loadInstitutesSynonyms(self):
        synonymsChecker = SynonymsChecker()
        for institute in self.xmlInstitutes:
            institute.aka = synonymsChecker.getInstSynonyms(institute.orgInstId)

    def _loadAuthorsSynonyms(self):
        synonymsChecker = SynonymsChecker()
        for author in self.xmlAuthors:
            author.aka = synonymsChecker.getAuthorSynonyms(author.inspire, author.foafName)

    def _loadSynonyms(self):
        self._loadInstitutesSynonyms()
        self._loadAuthorsSynonyms()

    def printInstitutesNotFound(self):
        print ("\n" * 5)
        notFoundCounter = 0
        for institute in self.xmlInstitutes:
            if not institute.match.found:
                notFoundCounter += 1
                print institute.match.fuzzRatio
                print institute.name
                print institute.match.foundAs.encode("utf-8")
        print ("Not found %d out of %d" % (notFoundCounter, len(self.xmlInstitutes)))

    def printInstitutesFound(self):
        print ("\n" * 5)
        for institute in self.xmlInstitutes:
            if institute.match.found:
                print institute.name 
                print institute.match.foundAs
                print ("-" * 10)

    def printAuthorsNotFound(self):
        print ("\n" * 5)
        notFoundCounter = 0
        for author in self.xmlAuthors:
            if not author.match.found:
                notFoundCounter += 1
                print author.match.fuzzRatio
                print author.namePaper.encode("utf-8") 
                print author.utf8Name.encode("utf-8") 
                print author.match.foundAs.encode("utf-8")
                print ("-" * 10)
        print ("Not found %d out of %d" % (notFoundCounter, len(self.xmlAuthors)))

    def printAuthorsFound(self):
        print ("\n" * 5)
        foundCounter = 0
        for author in self.xmlAuthors:
            if author.match.found:
                foundCounter += 1
                print author.namePaper 
                print author.match.foundAs
                print ("-" * 10)
        print ("Found %d out of %d" % (foundCounter, len(self.xmlAuthors)))

    def printInstitutesNotMatched(self):
        print ("\n" * 5)
        for institute in self.pdfInstitutes:
            if not institute.matched:
                print institute.name

    def printInstitutesFoundAs(self):
        print ("\n" * 5)
        for institute in self.xmlInstitutes:
            if institute.match.found:
                print ("Y: "),
            else:
                print ("N: "),
            print (institute.match.fuzzRatio),
            print (institute.id),
            print (" - "),
            print (institute.PDFid),
            print (" ("),
            print (institute.PDFindex),
            print (") "),
            print ("** "),
            print (institute.name),
            print ("** "),
            print (institute.match.foundAs)
            #print ("-" * 10)        

    def checkAuthorsPDFindex(self):
        print "displaying only found ones between first 100 authors"
        for author in self.xmlAuthors[0:100]:
            if author.match.found:
                print (author.namePaper + " - "),
                print (self.pdfAuthors[author.PDFindex].namePaper.encode("utf-8"))

    def checkAuthorsXMLindex(self):
        print "displaying only found ones between first 100 authors"
        for author in self.pdfAuthors[0:100]:
            if author.matched:
                print (author.namePaper.encode("utf-8") + " - "),
                print (self.xmlAuthors[author.XMLindex].namePaper)

    def checkInstitutesPDFindex(self):
        print "displaying only found ones between first 50 institutes"
        for institute in self.xmlInstitutes[0:100]:
            if institute.match.found:
                print (institute.name + " - "),
                print (self.pdfInstitutes[institute.PDFindex].name.encode("utf-8"))

    def _matchInstitutes(self):
        
        for xmlCounter, xmlInstitute in enumerate(self.xmlInstitutes):

            ratioTop = 0
            topCounter = None
            topName = ""
            ratio = 0

            for counter, pdfInstitute in enumerate(self.pdfInstitutes):
                ratio = 0

                if pdfInstitute.alsoAt != xmlInstitute.alsoAt:
                    continue
                if pdfInstitute.matched:
                    continue

                ratioName = fuzz.ratio(xmlInstitute.name, pdfInstitute.name)
                ratioUtf8 = fuzz.ratio(xmlInstitute.utf8Name, pdfInstitute.name)
                
                ratio = ratioName if ratioName > ratioUtf8 else ratioUtf8

                if ratio >= self.minInstitutesMatchRatio:
                    xmlInstitute.match.found = True
                    xmlInstitute.match.fuzzRatio = ratio
                    xmlInstitute.match.foundAs = pdfInstitute.name
                    xmlInstitute.PDFid = pdfInstitute.id
                    xmlInstitute.PDFindex = counter
                    pdfInstitute.matched = True
                    topCounter = counter
                    pdfInstitute.XMLindex = xmlCounter
                    break
                elif ratio > ratioTop:
                    ratioTop = ratio
                    topName = pdfInstitute.name
                    topCounter = counter

            if not ratioTop:
                continue

            if ratio >= self.minInstitutesMatchRatio:
                continue
            elif ratioTop >= self.minInstitutesMaxRatio:
                xmlInstitute.match.found = True
                if topCounter:
                    #xmlInstitute.match.foundAs = self.pdfInstitutes[topCounter].name
                    self.pdfInstitutes[topCounter].matched = True
                    self.pdfInstitutes[topCounter].XMLindex = xmlCounter
            else:
                xmlInstitute.match.found = False

            xmlInstitute.match.fuzzRatio = ratioTop
            xmlInstitute.match.foundAs = topName
            if topCounter:
                xmlInstitute.PDFid = self.pdfInstitutes[topCounter].id
            xmlInstitute.PDFindex = topCounter

    def _matchAuthors(self):

        # first run to get quite perfect matches
        for xmlCounter, xmlAuthor in enumerate(self.xmlAuthors):
            ratioTop = 0
            topCounter = None
            for counter, pdfAuthor in enumerate(self.pdfAuthors):
                if pdfAuthor.matched:
                    continue
                ratioName = fuzz.ratio(xmlAuthor.namePaper, pdfAuthor.namePaper)
                ratioUtf8 = fuzz.ratio(xmlAuthor.utf8Name, pdfAuthor.namePaper)
                ratio = ratioName if ratioName > ratioUtf8 else ratioUtf8

                if ratio >= self.minAuthorsMatchRatio:
                    xmlAuthor.match.found = True
                    xmlAuthor.match.fuzzRatio = ratio
                    xmlAuthor.match.foundAs = pdfAuthor.namePaper
                    xmlAuthor.PDFindex = counter
                    pdfAuthor.matched = True
                    pdfAuthor.XMLindex = xmlCounter
                    break

        # second run to get quite good matches
        for xmlCounter, xmlAuthor in enumerate(self.xmlAuthors):
            if xmlAuthor.match.found:
                continue
            ratioTop = 0
            topCounter = None
            for counter, pdfAuthor in enumerate(self.pdfAuthors):
                if pdfAuthor.matched:
                    continue
                ratioName = fuzz.ratio(xmlAuthor.namePaper, pdfAuthor.namePaper)
                ratioUtf8 = fuzz.ratio(xmlAuthor.utf8Name, pdfAuthor.namePaper)
                ratio = ratioName if ratioName > ratioUtf8 else ratioUtf8
                #print ratio, xmlAuthor.namePaper, pdfAuthor.namePaper.encode("utf-8")

                if ratio > ratioTop:
                    ratioTop = ratio
                    topName = pdfAuthor.namePaper
                    topCounter = counter

            if ratioTop >= self.minAuthorsMaxRatio:
                xmlAuthor.match.found = True 
                self.pdfAuthors[topCounter].matched = True
                xmlAuthor.match.fuzzRatio = ratioTop
                xmlAuthor.match.foundAs = topName
                xmlAuthor.PDFindex = topCounter
                self.pdfAuthors[topCounter].XMLindex = xmlCounter

        # third run to get the not worst matches
        for xmlCounter, xmlAuthor in enumerate(self.xmlAuthors):
            if xmlAuthor.match.found:
                continue
            ratioTop = 0
            topCounter = None
            topName = ""
            for counter, pdfAuthor in enumerate(self.pdfAuthors):
                if pdfAuthor.matched:
                    continue
                ratioName = fuzz.ratio(xmlAuthor.namePaper, pdfAuthor.namePaper)
                ratioUtf8 = fuzz.ratio(xmlAuthor.utf8Name, pdfAuthor.namePaper)
                ratio = ratioName if ratioName > ratioUtf8 else ratioUtf8

                if ratio > ratioTop:
                    ratioTop = ratio
                    topName = pdfAuthor.namePaper
                    topCounter = counter

            xmlAuthor.PDFindex = topCounter
            xmlAuthor.match.fuzzRatio = ratioTop
            xmlAuthor.match.foundAs = topName
            if topCounter:
                self.pdfAuthors[topCounter].XMLindex = xmlCounter


    # _verifyInconsitentPuntuaction MUST BE RUN before this function
    def _verifyMatchedAuthors(self):
        synonymsChecker = SynonymsChecker()
        for counter, author in enumerate(self.xmlAuthors):
            if author.match.fuzzRatio == 100:
                author.match.found = True
            elif author.match.foundAs in author.aka:
                author.match.found = True
                self.pdfAuthors[author.PDFindex].matched = True
                self.pdfAuthors[author.PDFindex].XMLindex = counter
                author.akaFound = True
            elif author.inconsistentSpaces:
                author.match.found = True
                self.pdfAuthors[author.PDFindex].matched = True
                self.pdfAuthors[author.PDFindex].XMLindex = counter
            else:
                author.match.found = False

    def _verifyMatchedInstitutes(self):
        for institute in self.xmlInstitutes:
            if institute.match.fuzzRatio == 100:
                institute.match.found = True
            elif institute.match.foundAs in institute.aka: 
                institute.match.found = True
                institute.akaFound = True
            elif institute.inconsistentPuntuaction:
                institute.match.found = True
            elif institute.match.fuzzRatio > 85: 
                institute.closeMatch = True
                institute.match.found = False
            else:
                institute.match.found = False

    def _verifyAuthorsInconsitentPuntuaction(self):
        for author in self.xmlAuthors:
            if not author.match.fuzzRatio == 100:
                try:
                    if author.namePaper.translate({ord(i): None for i in ' .-'}) == author.match.foundAs.translate({ord(i): None for i in ' .-'}) or \
                       author.utf8Name.translate({ord(i): None for i in ' .-'}) == author.match.foundAs.translate({ord(i): None for i in ' .-'}):
                        if author.namePaper.replace(" ", "") != author.match.foundAs.replace(" ","") or \
                           author.utf8Name.replace(" ", "") != author.match.foundAs.replace(" ",""):
                            if author.utf8Name.replace(" ", "") != author.match.foundAs.replace(" ",""):
                                author.inconsistentPuntuaction = True
                                #print("author inconsistentPuntuaction {} - {} - {} - {}".format(author.namePaper, author.aka, author.utf8Name.encode("utf-8"), author.match.foundAs.encode("utf-8")))
                        else:
                            author.inconsistentSpaces = True
                except Exception, error:
                    print("[ERROR] while working on {}".format(author.namePaper))
                    print("[ERROR] while working on {}".format(author.match.foundAs))
                    print(error)
                    continue

    def _verifyInstitutesInconsitentPuntuaction(self):
        for institute in self.xmlInstitutes:
            if not institute.match.fuzzRatio == 100:
                if institute.match.foundAs:
                    if institute.name.translate({ord(i): None for i in ' .-;,'}) == institute.match.foundAs.translate({ord(i): None for i in ' .-;,'}) or \
                       institute.utf8Name.translate({ord(i): None for i in ' .-;,'}) == institute.match.foundAs.translate({ord(i): None for i in ' .-;,'}):
                        institute.inconsistentPuntuaction = True

    def _reportShouldBeMarkedAsDeceased(self):
        self.reportDictionary["authors_not_deceased_list"] = list()
        for author in self.xmlAuthors:
            if author.deceased:
                if not self.pdfAuthors[author.PDFindex].deceased:
                    self.reportDictionary["authors_not_deceased_list"].append("%s" % author.namePaper)
                    self.reportShouldBeMarkedAsDeceased += ("Author %s not marked as deceased\n" % author.namePaper)

    def _reportAreMarkedAsDeceasedButShouldNot(self):
        self.reportDictionary["authors_deceased_list"] = list()
        for author in self.pdfAuthors:
            if author.deceased:
                if not self.xmlAuthors[author.XMLindex].deceased:
                    self.reportDictionary["authors_deceased_list"].append("%s" % author.namePaper)
                    self.reportAreMarkedAsDeceasedButShouldNot += ("Author %s is marked as deceased but should not\n" % author.namePaper)

    def _reportInconsistentPuntuaction(self):
        self.reportDictionary["authors_puntuation_list"] = list()
        for author in self.xmlAuthors:
            if author.inconsistentPuntuaction:
                self.reportDictionary["authors_puntuation_list"].append("Please change %s -> %s" % (author.match.foundAs, author.utf8Name))
                self.reportInconsistentPuntuaction += ("InconsistentPuntuaction for author %s - %s\n" % (author.namePaper, author.match.foundAs))

    def _reportAuthorsMissing(self):
        self.reportDictionary["authors_missing_list"] = list()
        self.reportDictionary["authors_missing_skip"] = list()
        for author in self.xmlAuthors:
            if not author.match.found and not author.inconsistentPuntuaction:
                entry = dict()
                entry["utf8Name"] = author.utf8Name
                entry["name"] = author.namePaper
                entry["inspire"] = author.inspire
                entry["foafName"] = author.foafName.replace(" ", "")
                if author.match.fuzzRatio > 80: # TODO: replace this "hardcoded" value with var 
                    entry["close_match"] = author.match.foundAs
                else:
                    entry["close_match"] = ""

                self.reportDictionary["authors_missing_list"].append(entry)
                self.reportAuthorsMissing += ("%s - %s : Author %s seems missing in the pdf\n" % (author.namePaper, author.match.foundAs, author.namePaper))
            if author.match.found and author.akaFound:
                entry = dict()
                entry["utf8Name"] = author.utf8Name
                entry["name"] = author.namePaper
                entry["inspire"] = author.inspire
                entry["foafName"] = author.foafName.replace(" ", "")
                entry["close_match"] = author.match.foundAs
                self.reportDictionary["authors_missing_skip"].append(entry)
                self.reportAuthorsMissing += ("%s - %s : Author %s seems missing in the pdf\n" % (author.namePaper, author.match.foundAs, author.namePaper))

    def _reportInstitutesMissing(self):
        self.reportDictionary["institutes_missing_pdf_list"] = list()
        self.reportDictionary["institutes_missing_pdf_skip"] = list()
        for institute in self.xmlInstitutes:
            if not institute.match.found and not institute.closeMatch:
                self.reportDictionary["institutes_missing_pdf_list"].append("%s - %s"  % (institute.id,institute.utf8Name))
                self.reportInstitutesMissing += ("%s - Institute seems missing \n %s \n" % (institute.id,institute.name))
            if institute.akaFound:
                self.reportDictionary["institutes_missing_pdf_skip"].append("%s - %s <br>%s" % (institute.id, institute.utf8Name, institute.match.foundAs))

    def _reportInstitutesCloseMatch(self):
        self.reportDictionary["institutes_close_matches_list"] = list()
        for institute in self.xmlInstitutes:
            if institute.closeMatch:
                entry = dict()
                entry["xmlRef"] = institute.id
                entry["utf8Name"] = institute.utf8Name
                entry["name"] = institute.name
                entry["close_match"] = institute.match.foundAs
                if institute.orgInstId == "":
                    entry["id"] = institute.orgSpiresICN.replace(" ", "")
                else:
                    entry["id"] = institute.orgInstId
                
                self.reportDictionary["institutes_close_matches_list"].append(entry)
                self.reportInstitutesCloseMatch += ("Institute with close match \n %s \n closer match: \n %s\n" % (institute.name, institute.match.foundAs))

    def _reportMismatchedAuthors(self):
        self.reportDictionary["authors_mismatched_list"] = list()

        institutesDictionary = dict()
        for index, inst in enumerate(self.xmlInstitutes):
            try:
                _id = inst.id
                _index = index
                _PDFindex = self.xmlInstitutes[_index].PDFindex
                _found = self.xmlInstitutes[_index].match.found
                if _PDFindex:
                    _pdfId = self.pdfInstitutes[_PDFindex].id
                else:
                    _pdfId = ""
                #print _id, _index, _PDFindex, _pdfId, _found
                institutesDictionary[_id] = {
                    "xmlIndex":_index,
                    "pdfIndex" : _PDFindex, 
                    "pdfId" : _pdfId, 
                    "found" : _found
                    }
            except:
                print ("id = %s" % _id)
                print ("index = %s" % _index)
                print ("xmlInstitutes = %s" % self.xmlInstitutes[_index])
                print ("_PDFindex = %s" % _PDFindex)
                print ("_found = %s" % _found)
                if _PDFindex:
                    print ("self.pdfInstitutes[_PDFindex] %s" % self.pdfInstitutes[_PDFindex].id)
                else:
                    print ("_pdfId %s " % _pdfId)

        for author in self.xmlAuthors:
            if author.match.found:
                pdfAuthorIndex = author.PDFindex
                pdfOrganizations = self.pdfAuthors[pdfAuthorIndex].organization
                #print ("author name %s , organizations list %s, author.organization %s" % (author.namePaper, pdfOrganizations, author.organization ) )
                for institute in author.organization:
                    if not institute in institutesDictionary:
                        continue
                    if institutesDictionary[institute]["pdfId"] not in pdfOrganizations:
                        if institutesDictionary[institute]["found"]:
                            self.reportDictionary["authors_mismatched_list"].append("Please check if author <b>%s</b> is listed at <br>%s" % (author.utf8Name, self.xmlInstitutes[institutesDictionary[institute]["xmlIndex"]].name))

        return

    def _report_notes(self):
        self.reportDictionary["note"] = list()

        self._check_last_update_date()

        self._check_msilvajr()

        self._check_zhang()

    def _check_zhang(self):
        """
        will check if "K. Zhang" is wrongly reported as "Kaili. Zhang" 
        """
        for index, author in enumerate(self.xmlAuthors):
            if author.namePaper == "Kaili. Zhang":
                print("[WARN] 'Kaili. Zhang' wrongly reported. Should be spelled 'K. Zhang'")
                self.reportDictionary["note"].append(
                """Please, report to the journal</br>Kaili. Zhang is meant to be spelled as:</br>K. Zhang""")

    def _check_last_update_date(self):
        db = DBManager()
        query = "SELECT LAST_UPDATE FROM ADBPMAP WHERE PAPER_REF = '%s'" % self.XMLobj.refCode

        last_update_date_db_result = db.execute(query)
        last_update_date_db = last_update_date_db_result[0][0]

        last_update_date_datetime = datetime.datetime.strptime(self.XMLobj.last_update_date, '%d-%b-%y')

        if last_update_date_datetime != last_update_date_db:
            print ("Last update dates differ:")
            print ("xml file last update date = %s" % last_update_date_datetime)
            print ("database last update date  = %s" % last_update_date_db)
            self.reportDictionary["note"].append(
            """The last update date in the xml file is equal to</br>
                %s</br>
                while on the database is set to
                %s<br>
                Please, contact maurizio.colautti@cern.ch, atlas-phys-office-mgr@cern.ch</br>
            """ % (last_update_date_datetime, last_update_date_db)
            )



    def _check_msilvajr(self):
        """ a specific check for this author is needed because it was wrongly saved in db """
        for author in self.xmlAuthors:
            if author.namePaper == "M. Silva Jr.":
                print ("M. Silva Jr. added to note case")
                self.reportDictionary["note"].append(
                """Please, report to the journal</br>M. Silva Jr. is meant to be author:</br>
                First name: Manuel Jr</br>Last Name: Silva</br>Inspire id = INSPIRE-00549189</br>
                orcid = 0000-0001-6940-8184</br>
                while M.V. Silva Oliveira is meant to be author:</br>
                First name: Marcos Vinicius</br>Last Name: Silva Oliveira</br>Inspire id = INSPIRE-00390428</br>
                orcid = 0000-0003-2285-478X</br>"""
                )

    def _createReports(self):
        self._reportAreMarkedAsDeceasedButShouldNot()
        self._reportShouldBeMarkedAsDeceased()
        self._reportAuthorsMissing()
        self._reportInconsistentPuntuaction()
        self._reportInstitutesMissing()
        self._reportInstitutesCloseMatch()
        self._reportMismatchedAuthors()
        self._report_notes()

    def _writeReports(self):
        print ("writing results on file: %s" % self.reportPath + "/" + self.reportDictionary["filename"] + '.json')
        with open(self.reportPath + "/" + self.reportDictionary["filename"] + '.json', 'w') as fp:
            json.dump(self.reportDictionary, fp)

    def addSynonyms(self):
        synonymsChecker = SynonymsChecker()
        print ("Confirm author synonyms (y/n)")
        for author in self.xmlAuthors:
            if not author.match.found and \
               not author.inconsistentPuntuaction:
                if author.match.foundAs in author.aka:
                    continue
                print ("\nxml %s" % author.utf8Name)
                print ("pdf %s" % author.match.foundAs)
                if sys.version_info[0] < 3:
                    input_var = raw_input("Enter something: ")
                else:
                    input_var = input("Enter something:")
                if input_var.upper() == "Y" or \
                   input_var.upper() == "YES":
                    synonymsChecker.insertAuthors(author.inspire, author.foafName, author.match.foundAs, author.namePaper)
                #synonymsChecker.insertAuthors()
        for institute in self.xmlInstitutes:
            if not institute.match.found:
                if institute.match.foundAs in institute.aka:
                    continue
                print ("\nxml %s" % institute.utf8Name)
                print ("pdf %s" % institute.match.foundAs)
                if sys.version_info[0] < 3:
                    input_var = raw_input("Enter something: ")
                else:
                    input_var = input("Enter something:")
                if input_var.upper() == "Y" or \
                   input_var.upper() == "YES":
                    synonymsChecker.insertInstitutes(institute.orgInstId, institute.match.foundAs, institute.name)                
        synonymsChecker.updateJson()


    def analyze(self):
        print ("populating xml and pdf elements ... ")
        self._populate()
        print ("loading synonyms ... ")
        self._loadSynonyms()
        print ("matching institutes ... ")
        self._matchInstitutes()
        print ("matching authors ... ")
        self._matchAuthors()
        print ("verifying authors inconsintent punctuation  ... ")
        self._verifyAuthorsInconsitentPuntuaction() # this must be first _verify function
        print ("verifying institutes inconsintent punctuation  ... ")
        self._verifyInstitutesInconsitentPuntuaction() # this must be first _verify function
        print ("verifying matched authors ... ")
        self._verifyMatchedAuthors()
        print ("verifying matched institutes ... ")
        self._verifyMatchedInstitutes()
        print ("creating reports ... ")
        self._createReports()
        self._writeReports()


#xmlFilename = "/home/mrz/Documenti/CERN/atlaspo/proofs/doc1080/atlas_authlist.xml"
#pdfFilename = "/home/mrz/Documenti/CERN/atlaspo/proofs/doc1080/target_pdf.kfb"

if __name__ == "__main__":
    
    try:
        ndoc = sys.argv[1]
    except:
        ndoc = "1156"

    try:
        if sys.argv[2] == "0" or sys.argv[2] == "False":
            synCheck = False
    except:
        synCheck = True

    synCheck = False

    xmlFilename = "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/proofs/doc%s/atlas_authlist.xml" % ndoc
    pdfFilename = "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/proofs/doc%s/target_pdf.kfb" % ndoc

    if not os.path.isfile(xmlFilename) or not os.path.isfile(pdfFilename):
        print ("Couldn't find both \nxmlFile: %s\npdfFile: %s" % (xmlFilename, pdfFilename))
        sys.exit()

    xml_object = XmlObjReader()
    #xml_object.set_afs_source(xmlFilename)
    #xml_object.set_gitlab_source("atlas-physics-office/HION/ANA-HION-2018-04/ANA-HION-2018-04-PAPER")
    xml_object.set_gitlab_source("atlas-physics-office/EGAM/ANA-EGAM-2018-01/ANA-EGAM-2018-01-PAPER")

    pdf_object = PdfObjReader(pdfFilename)

    comparison = Comparison(xml_object, pdf_object)
    comparison.setReportPath(".")
    comparison.setFilename("doc%s" % ndoc)
    comparison.setDocument("doc%s" % ndoc)
    comparison.analyze()
    if synCheck:
        comparison.addSynonyms()

    #comparison.printXmlInstitutes()
