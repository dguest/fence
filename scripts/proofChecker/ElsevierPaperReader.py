# -*- coding: utf-8 -*-
"""
#####################################
# ElsevierPaperReader.py
#
# Uses xpdf to extract text from PDF
# and convert into knowledge files
# Derived class for Elsevier papers
#
# Author: Marcello Barisonzi
#
# $Rev: 47951 $
# $LastChangedDate: 2012-11-14 20:25:28 +0100 (Wed, 14 Nov 2012) $
# $LastChangedBy: atlaspo $
#
####################################
"""

import re
import sys

from XpdfPaperReader     import XpdfPaperReader

reload(sys)
sys.setdefaultencoding("utf-8")


class ElsevierPaperReader(XpdfPaperReader):
    
    def __init__(self, filename, fp="1", lp="-1"):
        super(self.__class__, self).__init__(filename, fp, lp)

                        
        ### pattern should be overwritten by subclasses
        self._instituteExc = [ re.compile("([a-z]{1,2}\s*\n*?Also(?:.*?(?:%s)){2}\.*)" % "|".join(self._excep_countries)),
                               re.compile("[^N,P]([0-9]{1,3}\n*?(?:[^0-9]*?\n*?[^0-9]*?(?:%s)){2})" % "|".join(self._excep_countries))]

        self._institutePtn = [ re.compile("([a-z]{1,2}\s*\n*?Also.*?(?:%s)\.*)" % "|".join(self._countries)),
                               re.compile("[^N,P]([0-9]{1,3}\s*\n*?.*?(?:%s)\.*)" % "|".join(self._countries)),
                               re.compile("(.\s*Deceased\.*)")]

        self._institutePtn += [ re.compile("([a-z]{1,2}\s*\n*?Also.*?(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,7) ]
        
        self._institutePtn += [ re.compile("[^N,P]([0-9]{1,3}\s*\n*.*(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,7) ]



        self._ptn_id    = re.compile("([0-9]{1,3})\s*\n*")
        self._ptn_also  = re.compile("([a-z]{1,2})\s*\n*Also at(.*)\.*")
        self._ptn_dec   = re.compile("(.)\s*Deceased\.*")
        self._ptn_subid = re.compile("\(([a-z]{1})\)")
        self._ptn_state = re.compile("(%s)" % "|".join(self._countries))

        self._ptn_jhep_junk = re.compile("\s*:*\s*\n*") 
        
        return

