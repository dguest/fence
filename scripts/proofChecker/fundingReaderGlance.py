# -*- coding: utf-8 -*-

import os
import sys

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

os.environ["NLS_LANG"] = "RUSSIAN_RUSSIA.AL32UTF8"

class InvalidVersionError( Exception ):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def fundingReaderGlance():

    mgr = DBManager()
    # old query 
    #results = mgr.execute("""SELECT 
    #    fd.ACRONYMS AS ACRONYM, 
    #    fd.FUNDA AS NAME, 
    #    fd.DOMAIN AS DOMAIN,
    #    fd.COUNTRY as COUNTRY
    #    FROM 
    #    FUNDA_DOI fd
    #    WHERE end_date>=TO_DATE('""" + str(date.today()) + """', 'YYYY-MM-DD')
    #    AND (start_date<=TO_DATE('""" + str(date.today()) + """', 'YYYY-MM-DD') or start_date IS NULL)
    #    ORDER BY fd.ACRONYMS;"""
    #    )
    # new query: see ticket https://its.cern.ch/jira/browse/ATLASPO-50
    results = mgr.execute("""SELECT 
        fd.ACRONYMS AS ACRONYM, 
        fd.FUNDA AS NAME, 
        fd.DOMAIN AS DOMAIN,
        fd.COUNTRY as COUNTRY
        FROM 
        FUNDA_DOI fd
        ORDER BY fd.ACRONYMS;"""
        )
    funds = []
    for result in results:
        ref_funding = 'ref_funding("%s, %s, %s", "%s")\n' % (
                "" if result[0] == None else (str (result[0])).decode("utf-8"), 
                "" if result[1] == None else (str (result[1])).decode("utf-8"), 
                "" if result[3] == None else (str (result[3])).decode("utf-8"), 
                "" if result[2] == None else (str (result[2])).decode("utf-8")
            ) 
        ref_funding = ref_funding.replace('"None"', '""')
        ref_funding = ref_funding.replace(', ,', ',')
        ref_funding = ref_funding.replace('ref_funding(", ', 'ref_funding("')
        funds.append(ref_funding)
    return "".join(funds)
