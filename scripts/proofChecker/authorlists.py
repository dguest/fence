# -*- coding: utf-8 -*-
"""
#defining an hash table to directly access to the correct object would be fine
#such as "M. Aaboud" => points directly to object #n and not loop through all
"""

class Author():

    def __init__(self):
        self.foafName = ""
        self.firstName = ""
        self.lastName = ""
        self.namePaper = ""
        self.utf8Name = ""
        self.inspire = ""
        self.orcid = ""
        self.organization = list()
        self.alsoAt = list()
        self.deceased = False
        self.inconsistentPuntuaction = False
        self.inconsistentSpaces = False

        self.match = Match()

        self.PDFindex = None

        self.aka = list()
        self.akaFound = False
        """

        self.foundAuthor = False
        self.foundAs = ""
        self.consistent = False

        
        self.PDFnamePaper = ""
        self.PDForganization = list()
        self.PDFalsoAt = list()
        self.PDFdeceased = False
        """

class Institute():

    def __init__(self):
        self.name = ""
        self.utf8Name = ""
        self.id = ""
        self.orgSpiresICN = ""
        self.orgInstId = ""
        self.orgShortName = ""
        self.status = ""
        self.inconsistentPuntuaction = False
        self.closeMatch = False

        self.match = Match()

        self.PDFid = ""
        self.PDFindex = ""

        self.alsoAt = False

        self.aka = list()
        self.akaFound = False
        """

        self.foundInstitute = False
        self.foundAs = ""
        self.PDFid = ""
        """

class PDFAuthor:

    def __init__(self):
        self.namePaper = ""
        self.organization = list()
        self.alsoAt = list()
        self.deceased = False
        self.matched = False
        self.XMLindex = None

class PDFInstitute:

    def __init__(self):
        self.id = ""
        self.name = ""
        self.matched = False
        self.XMLIndex = None

        self.alsoAt = False

class Match:

    def __init__(self):
        # TODO:
        # this .found variable is used in 2 moments
        # for kind of 2 different purposes
        # not the best solution
        self.found = False
        self.foundAs = ""
        self.fuzzRatio = 0
        self.aka = list()
