# -*- coding: utf-8 -*-
"""
#####################################
# XpdfPaperReader.py
#
# Uses xpdf to extract text from PDF
# and convert into knowledge files
#
# Author: Marcello Barisonzi
#
# $Rev: 239255 $
# $LastChangedDate: 2015-06-09 00:45:56 +0200 (Tue, 09 Jun 2015) $
# $LastChangedBy: atlaspo $
#
####################################
"""

import re
import os
import sys

reload(sys)
sys.setdefaultencoding("utf-8")

class XpdfPaperReader(object):
    
    def __init__(self, filename, fp="1", lp="-1"):
        self._input  =  os.popen("pdftotext -raw -enc UTF-8 -nopgbrk -f %s -l %s %s -" % (fp, lp, filename)).read()
        print("[INFO] executing command pdftotext -raw -enc UTF-8 -nopgbrk -f %s -l %s %s -" % (fp, lp, filename))
        
        self._path = os.path.dirname(os.path.abspath(filename))
        self._firstAuthor    = 1
        self._lastAuthor     = -1 #self._npages
        self._firstInstitute = 1
        self._lastInstitute  = -1 #self._npages
        self._authorTxt      = None
        self._firstAuthorString = ""
        self._instituteTxt   = None
        self._firstInstituteString = ""
        self._replaces = {}
        #self._publisher      = self.getPublisher(filename)
        
        self._countries = [ "Argentina",
                            "Armenia",
                            "Australia",
                            "Austria",
                            "Azerbaijan",
                            "Belarus",
                            "Brazil",
                            "Bulgaria\\b",
                            "Canada",
                            "Chile",
                            "China",
                            "Colombia",
                            "Czech\s*\n*Republic",
                            "Denmark",
                            "France",
                            "Georgia",
                            "Germany",
                            "Greece",
                            "Hungary",
                            "Israel",
                            "Italy",
                            "Japan",
                            "Malaysia",
                            "Mexico(?!,)",   # not really correct, but it works, avoid Mexico / New Mexico
                            "Morocco",
                            "Netherlands",
                            "The\s*\n*Netherlands",
                            "Norway",
                            "Palestine",
                            "Poland",
                            "Portugal",
                            "Republic\s*\n*of\s*\n*Belarus",
                            "Romania",
                            "Russia\\b",
                            "Serbia",
                            "Slovakia",
                            "Slovak\s*\n*Republic",
                            "Slovenia",
                            "South\s*\n*Africa",
                            "Spain",
                            "Sweden",
                            "Switzerland",
                            "Taiwan",
                            "Turkey",
                            "United\s*\n*Kingdom",
                            "U\.K\.",
                            "UK",
                            "United\s*\n*States\s*\n*of\s*\n*America",
                            "United\s*\n*States",
                            "U\.S\.A\.",
                            "USA",
                            "India\\b"] # last, not to match "Indiana"

        self._excep_countries = [ "Azerbaijan" ,
                                  "Belarus",
                                  "Chile",
                                  "China",
                                  "Czech\s*\n*Republic",
                                  "Georgia",
                                  "Israel"
                                  ]
                        
        ### pattern should be overwritten by subclasses
        self._authorPtn = re.compile("((?:[A-Z]+[a-z]{0,1}[\.|\-]+)+[^0-9]+?),*?([0-9]+(?:[0-9]*[a-z]*\**,*)*)")
        #self._institutePtn = re.compile("((?:[a-z]{1,2}\n*Also|[0-9]{1,3}[a-z]{0,1}\n*(?:[A-Z]|\([a-z]{1,2}\))).*?(?:%s)\.*)" % "|".join(self._countries))

        self._instituteExc = [ re.compile("([a-z]{1,2}\s*:*\s*\n*?Also.*?(?:%s){2}\.*)" % "|".join(self._excep_countries)),
                               re.compile("[^N,P]([0-9]{1,3}(?:[a-z]{0,1}|\([a-z]{1,2}\))\n*?(?:[^0-9]*?\n*?[^0-9]*?(?:%s)){2})" % "|".join(self._excep_countries)) ]

        
        self._institutePtn = [ re.compile("([a-z]{1,2}\s*:*\s*\n*?Also.*?(?:%s)\.*)" % "|".join(self._countries)),
                               re.compile("[^N,P]([0-9]{1,3}\s*(?:[a-z]{0,1}|\([a-z]{1,2}\))\n*?.*?(?:%s)\.*)" % "|".join(self._countries)),
                               re.compile("((?:[a-z]{1,2}|\*)\s*:*\s*\n*?[D,d]eceased\.*)")]

        self._institutePtn +=  [ re.compile("([a-z]{1,2}\s*:*\s*\n*?Also.*?(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,7) ]
        self._institutePtn += [ re.compile("[^N,P]([0-9]{1,3}\s*(?:[a-z]{0,1}|\([a-z]{1,2}\))\n*.*(?:\n.*?){1,%d}.*?(?:%s)\.*)" % (i, "|".join(self._countries))) for i in range(1,7) ]


        self._ptn_id    = re.compile("([0-9]{1,3}[a-z]{0,1})\s*:*\s*\n*")
        self._ptn_also  = re.compile("([a-z]{1,2})\s*:*\s*\n*.*Also at(.*)\.*")
        self._ptn_dec   = re.compile("(.{1,2}?)\s*:*\s*\n*[d,D]eceased\.*")
        self._ptn_dec_char  = re.compile("(.{1,2}?)\s*:*\s*\n*[d,D]eceased\.*")
        self._ptn_subid = re.compile("\(([a-z]{1})\)")
        self._ptn_state = re.compile("(%s)" % "|".join(self._countries))

        self._ptn_jhep_junk = re.compile("\s*:*\s*\n*") 

        self._remove_lines = []

        self._deceased_char = ''
        
        return
    
    def setAuthorPages(self, first, last):
        self._firstAuthor    = first
        self._lastAuthor     = last
        return
    
    def setInstitutePages(self, first, last):
        self._firstInstitute = first
        self._lastInstitute  = last
        return

    def _clean_et_al(self):
        """
        we want to remove the header extracted from the publication
        where it's reported the name of the first author
        together with "et al."
        This doesn't apply to all the journal so we shouldn't make it generic
        see ticket https://its.cern.ch/jira/browse/ATLASPO-176
        """
        cleaned_input = ""
        for line in self._input.splitlines():
          if self._firstAuthorString in line and "et al." in line:
            #print("removing line {}".format(line))
            continue
          cleaned_input += line + "\n"
        self._input = cleaned_input
 
    def setFirstAuthor(self, stg):
        self._firstAuthorString = stg
        print "Setting first author --> %s" % self._firstAuthorString
        self._clean_et_al()
        return

    def setLastAuthor(self, stg):
        self._lastAuthorString = stg
        print "Setting last author --> %s" % self._lastAuthorString
        return

    
    def setFirstInstitute(self, stg):
        # use first few charachters to avoid mismatching
        self._firstInstituteString = stg[:22] 
        print "Setting first institute --> %s" % self._firstInstituteString
        return

    def getFirstPage(filename):
        _info = os.popen("pdfinfo %s" % filename)

        for i in _info:
            if "Pages:" in i:
                print i
                pages = i[len("Pages:")+1:].strip()
                if int(pages) > 400:
                    return int(pages) - 50
        return 1

    getFirstPage = staticmethod(getFirstPage)

    @staticmethod
    def notify_developer(filename):
        try:
          text = "From: atlaspo@cern.ch\n"
          text += "To: maurizio.colautti@cern.ch\n"
          text += "Subject: proof cheker can't recognize Publisher!\n\n"
          text += "The proof checker is not able to recognize the publisher"
          text += "for the file: {}".format(filename)

          _pipe = os.popen("/usr/sbin/sendmail -t -i", "w")
          _pipe.write(text)
        except Exception, error:
          print("[ERROR] while trying to notify_developer the publisher couldn't be recognized")
          print(error)

    def getPublisher(filename):
        _info = os.popen("pdfinfo %s" % filename)

        _pubDict = { "VTeX"        : "EPJC",
                     "Elsevier"    : "Elsevier",
                     "LaTeX"       : "EPJC", #"JHEP", (method changed, see below)
                     "Arbortext"   : "APS",
                     "Illustrator" : "APS",
                     "cairo"       : "EPJC",
                     "PDFium"      : "EPJC",
                     "Nitro"       : "EPJC",
                     "Springer"    : "EPJC",
                     "Preview"     : "APS"
                   } 

        publisher = "Unknown"

        for i in _info:
            if "Creator" in i:
                for k,v in _pubDict.iteritems():
                    if k in i:
                        if k == "LaTeX" and "njp" in filename.lower():
                            publisher = "NJP"
                        else:
                            publisher = v
                        break
            elif "Producer" in i:
                if "medialab" in i:
                    publisher = "JHEP" 

        if publisher == "Unknown":
            if XpdfPaperReader.getFirstPage(filename) > 500:
                publisher = "EPJC"

        if publisher == "Unknown":
            if 'jinst' in filename.lower():
                publisher = "JHEP"
            if 'jhep' in filename.lower():
                publisher = "JHEP"
            if filename.lower().startswith('du') or filename.lower().startswith('lv'):
                publisher = "APS"

        if publisher == "Unknown":
            XpdfPaperReader.notify_developer(filename)

        return publisher

    getPublisher = staticmethod(getPublisher)
    

    def searchInstituteString(self, stg):
        if not self._instituteTxt: 
            self.getInstituteText()
    
        ptn = re.compile(stg)
    
        if not ptn.search(self._instituteTxt):   
            return  None
        else:
            return ptn.findall(self._instituteTxt)[0] 
    
    def getAuthorText(self):
        """ todo problem with extraction data on document 1194
        the function should retrieve the long list of authors
        and their references
        """
        
        if not self._authorTxt:    

            self._authorTxt = self._input
        
            self._authorTxt = self.cleanUnicode(self._authorTxt)

            idx1 = self._authorTxt.rfind(self._firstAuthorString)
            idx2 = self._authorTxt.rfind(self._firstInstituteString)-2

            if idx1 == -1 or idx2 == -1:
                print self._authorTxt
            else:
                #print idx1, idx2
                self._authorTxt = self._authorTxt[idx1:idx2]
        return self._authorTxt

    def getInstituteText(self):
        
        if not self._instituteTxt:    

            self._instituteTxt = self._input
            
            self._instituteTxt = self.cleanUnicode(self._instituteTxt)
                       
            idx = self._instituteTxt.find(self._firstInstituteString)-3
        
            if idx == -1:
                print self._instituteTxt
            else:
                self._instituteTxt = self._instituteTxt[idx:]
            
            #print "Found index: %d" % idx
            
        return self._instituteTxt

    def removeLines(self, txt):
      if self._remove_lines:
        text = ""
        for line in txt.splitlines():
          if not any(expression.match(line) for expression in self._remove_lines):
            text += line + "\n"
        return text
      return txt

        
    def cleanUnicode(self, txt):

        ### use for reference: http://en.wikibooks.org/wiki/Unicode/Character_reference/0000-0FFF

        targets = {"T.\s*P.\s*A. (.{1,3}\s*)kesson" : u"\u00c5",
                   "B. (.{1,3}\s*)sman" : u"\u00c5",
                   "J. Barreiro Guimar(.{1,3})es" : u"\u00e3",
                   "A. Filip(.{1,3})i"                : u"\u010d",
                   "L. (.{1,3})ivkovi"             : u'\u017d',
                   #"L. .{1,3}ivkovi([^\w]+)"             : u'\u0107',
                   "L. .{1,3}ivkovi(.{1,3})\s?[0-9]?"             : u'\u0107',
                   "V. B(.{1,3})scher"            : u'\u00fc',
                   "P. M(.{1,3})ttig" : u'\u00e4',
                   "R. Gon(.{1,3})alo" : u'\u00e7',
                   "C. Garc(.*?)a" : u'\u00ed',
                   "J.\s*E. Garc(.*?)a Navarro" : u'\u00ed',
                   "A. Gori(.{1,3})ek" : u'\u0161',
                   "T. Hen(.{1,3})\s?[0-9]?"  : u'\u00df', 
                   #"P. Je(.{1,3})" :   u'\u017e',  # P. Jez and P. Jenni 
                   "B.\s*P. Ker(.{1,3})evan" : u'\u0161',
                   "S. Cr(.{1,3})p.{1,3}-Renaudin" : u'\u00e9',
                   "S. Cr.{1,3}p(.{1,3})-Renaudin" : u'\u00e9',
                   "T. Jav(.{1,3})rek" : u'\u016f',
                   "J. Lev(.{1,3})que" : u'\u00ea',
                   "B. Ma(.{1,3})ek" : u"\u010d",
                   "M. Miku(.{1,3})\s?[0-9]?" :   u'\u017e',
                   "P. Conde Mui(.{1,3})o" :   u'\u00f1',
                   "M. Mi(.{1,3})ano Moya" :   u'\u00f1',
                   "M.\s*T. P(.{1,3})rez Garc.*?a-Esta.{1,3}" : u"\u00e9",
                   "M.\s*T. P.{1,3}rez Garc(.*?)a-Esta.{1,3}" :   u'\u00ed',
                   "M.\s*T. P.{1,3}rez Garc.*?a-Esta(.{1,3})\s?[0-9]?" :   u'\u00f1',
                   #"T. (.{1,3})eni.{1,3}" : u'\u017d',
                   #"T. .{1,3}eni(.{1,3})" : u'\u0161',
                   "Palack(.{1,3}) University" : u'\u00fd',
                   "Universit(.{1,3}) di Pavia" : u'\u00e0',
                   "G. Amor(.{1,3})s" : u"\u00f3",
                   "S. Cabrera Urb(.{1,3})n" : u'\u00e1',
                   "T. G(.{1,3})pfert" : u'\u00f6',
                   "C. G(.{1,3})ssling" : u'\u00f6',
                   "T. G(.{1,3})ttfert" : u'\u00f6',
                   "S. Gonz(.{1,3})lez de la Hoz" : u'\u00e1',
                   "P. Grafstr(.{1,3})m" : u'\u00f6',
                   "Y. Hern(.{1,3})ndez Jim.{1,3}nez" : u'\u00e1',
                   "Y. Hern.{1,3}ndez Jim(.{1,3})nez" : u'\u00e9',
                   "E. Hig(.{1,3})n-Rodriguez" : u"\u00f3",
                   "K. K(.{1,3})neke" : u'\u00f6',
                   "A. C. K(.{1,3})nig" : u'\u00f6',
                   "L. K(.{1,3})pke" : u'\u00f6',
                   "J. U. Mj(.{1,3})rnmark" : u'\u00f6',
                   "K. M(.{1,3})nig" : u'\u00f6',
                   "N. M(.{1,3})ser" : u'\u00f6',
                   "S. Mohrdieck-M(.{1,3})ck" : u'\u00f6',
                   "M. Moreno Ll(.{1,3})cer" : u'\u00e1',
                   "G. P(.{1,3})sztor" : u'\u00e1',
                   "K. Pomm(.{1,3})s"   : u'\u00e8',
                   "J. Sj(.{1,3})lin" : u'\u00f6',
                   "Universitat\s*\n*Aut(.{1,3})noma" : u'\u00f2',
                   "R. Str(.{1,3})hmer" : u'\u00f6',
                   "J. S(.{1,3})nchez" : u'\u00e1',
                   "S. Tok(.{1,3})r" : u'\u00e1',
                   "E. Torr(.{1,3}) Pastor" : u"\u00f3",
                   "F. Anghinol([^\w]+)" : "fi",
                   "D. Ban([^\w]+)" : "fi",
                   "P. Cala(.{1,3})ura" : "fi",
                   "A. Da(.{1,3})nca" : "fi",
                   "L. Du(.{1,3})ot" : "fl",
                   "R. Dux(.{1,3})eld" : "fi",
                   "G. Gor(.{1,3})ne" : "fi",
                   "J. Grif(.{1,3})ths" : "fi",
                   "J. Gri(.{1,3})ths" : "ffi",
                   "J. Inigo-Gol(.{1,3})n" : "fi",
                   "E. Katsou(.{1,3})s" : "fi",
                   "I. Mandi(.{1,3})\s?[0-9]?"  : u'\u0107',
                   "S.\s*J. Max(.{1,3})eld" : "fi",
                   "L. Mijovi(.{1,3})\s?[0-9]?"  : u'\u0107',
                   "K. Proko(.{1,3})ev" :  "fi",
                   "R. Ta(.{1,3})rout" : "fi",
                   "R.\s*W. Cli(.{1,3})t" : "ff",
                   "I. Hinchli(.{1,3})e" : "ff",
                   "J. Ho(.{1,3})man" : "ff",
                   "D. Ho(.{1,3})mann" : "ff",
                   "T.\s*B. Hu(.{1,3})man" : "ff",
                   "E. Ko(.{1,3})eman" : "ff",
                   "A. Ta(.{1,3})ard" : "ff",
                   "SUPA (.{1,3}) School" : "-",
                   "G. Geß(.{1,3})ner" : "", # 2018/01/24 mcolautt
                   "D. (.{1,3})lvarez Piqueras" : u'\u00c1',
                   "F. Span(.{1,2})" : u'\u00f2', # 2018/01/24 mcolautt
                   "J. C(.{1,2}u)th" : u'\xfa', # 2018/01/24 mcolautt
                   "T. .{1,3}S(.{1,3})ligoj" : "fi", # 2018/01/24 mcolautt
                   "T. (.{1,3}S).{1,3}ligoj" : u'\u0160', # 2018/01/24 mcolautt
                   "Institut f(.{1,3})r Kern- und Teilchenphysik, Technische Universit.{1,3}t" : u'\u00f2'
                   #"Institut f.{1,3}r Kern- und Teilchenphysik, Technische Universit(.{1,3})t" : u'\u00fc'                   
                   }

        for k,v in targets.iteritems():
            if re.search(k,txt):
                _r = re.findall(k,txt)[-1]
                #print "****** for k,v in targets.iteritems ******"
                #print k, v
                #print _r
                #print re.findall(k,txt)
                # print "%s '%s'" % (k, _r)
                # avoid empty replacements and replacements with single existing letters
                if len(_r.strip())>0 and not (len(_r.strip())==1 and _r.strip().isalpha()):
                    self._replaces[_r] = v


        # print self._replaces

        for k,v in self._replaces.iteritems():
            #if v == u'\u010d':
            #    _ind = txt.rfind(k)
            #    print "BEFORE:\t", txt[_ind-10:_ind+10]
            # print k, v
            txt = txt.replace(k,v)
            #if v == u'\u010d':
            #    print "AFTER:\t", txt[_ind-10:_ind+10]
            

        #example: Cote
        #txt = txt.replace(u'o\u02c6',u'\u00f4').replace(u'\u02c6o',u'\u00f4')
        if re.search(u"D. C(.{1,3})t\u00e9" ,txt, re.UNICODE):
            txt = txt.replace(re.findall(u"D. C(.{1,3})t\u00e9" ,txt, re.UNICODE)[0], u'\u00f4')

        # Pagacova
        if re.search(u"D. Pag\u00e1(\b.{1,3})ov\u00e1", txt, re.UNICODE):
            txt = txt.replace(re.findall(u"D. Pag\u00e1(\b.{1,3})ov\u00e1" ,txt, re.UNICODE)[0], u'\u010d')

        ### correct apostrophe
        txt = txt.replace(u'\u2122',"'") # EPJC
        
        txt = txt.replace(u'\u2019',"'") # PLB

        txt = txt.replace(u'\xd5',"'") # PRL

        ### asterisk
        txt = txt.replace('\xe2\x88\x97', "*") # JHEP

        
        return txt

    def processInstitutesText(self):

        #print("***************************")
        #print("processing institutes text")
        #print("***************************")

        spacer = "\n\n\n\t\t\t\n\n\n"
        
        if not self._instituteTxt: 
                self.getInstituteText()

        #remove after finding        

        _tmpTxt = self.cleanUnicode(self._instituteTxt)

        _tmpTxt = re.sub(r"[0-9]{4}", "", _tmpTxt).replace("  ", " ").replace(" ,", ",")

        _cleanedTxt = ""
        for line in _tmpTxt.splitlines():
          if line.startswith("JHEP_"):
            continue
          if line.startswith("proofs"):
            continue
          if line.startswith("–"):
            continue
          #print (line)
          _cleanedTxt += line + '\n'

        #print(_cleanedTxt)
        _tmpTxt = _cleanedTxt

        #print(_tmpTxt)
        #_tmpTxt = self.removeLines(_tmpTxt)

        out_file = open(self._path + "/institutes_text.txt", "w")
        out_file.write(_tmpTxt)
        out_file.close()

        _ilist = []

        #print ("*" * 35)
        #print "processInstitutesText"

        for ptn in self._instituteExc + self._institutePtn + [self._ptn_dec]:

            _ilist += ptn.findall(_tmpTxt)

            _tmpTxt = ptn.sub(spacer, _tmpTxt)

        _tmpTxt = _tmpTxt.replace("\n", " ").strip()

        # print repr(_tmpTxt)
        #for ptn in [self._ptn_dec]:
          #print "ptn patter = "
          #print ptn.pattern

          #_ilist += ptn.findall(_tmpTxt)

        #print _ilist

        return _ilist 
    
    def getInstitutes(self):

        _ilist = self.processInstitutesText()
        
        _nlist = []
               
        #print ("*" * 35)
        #print "getInstitutes - print i"

        for i in _ilist:

            #print("**")
            #print("i in ilist = {}".format(i))
            #print("*********")

            # print i
            i = i.replace("\n"," ").strip()

            # print i
            
            # clean up some funny characters (PLB)     
            _idot = i.rfind(u'\u2022')
            if _idot != -1:
                i = i[_idot+1:]
            
            if not self._ptn_id.match(i) and not self._ptn_also.match(i) and not self._ptn_dec.match(i):
                print "Skipping", i, self._ptn_id.match(i), self._ptn_also.match(i)
                continue
            
            if self._ptn_dec.match(i):
              if self._ptn_dec_char:
                print "deceased or not?"
                _m = self._ptn_dec_char.findall(i)[0]
                _m = _m.strip()
                self._deceased_char = _m
                _nlist.append((_m, "Deceased"))
                print "Deceased mark is: '"
                print _m
                continue
               
            state = self._ptn_state.findall(i)[0]
                       
            if i[0].isdigit() and self._ptn_id.search(i):

                # case 1: member institute

                # remove page numbers if present

                if re.match("[0-9]+\s+[0-9]+", i):
                    _id = re.findall("[0-9]+\s+([0-9]+)", i)[0].strip()
                else:
                    _id = self._ptn_id.findall(i)[0].strip()
                
                if self._ptn_subid.search(i):
                    for j in i.split(";"):        
                        try:
                            #print "BEGINNING"
                            j = j.strip()                            
                            #print "Fullstring:\t", j
                            subid = self._ptn_subid.findall(j)[0]
                            #print "SubId:\t", subid
                            j = self._ptn_id.sub("",j).strip()
                            #print "Remove Id:\t", j
                            # JHEP junk
                            if self._ptn_jhep_junk.match(j):
                                j = self._ptn_jhep_junk.sub("",j,1)
                                #print "Remove Junk:\t", j
                            k = self._ptn_subid.sub("",j).strip()
                            #print "Remove SubId:\t",k
                            if not "%s" % state in k:
                                k = "%s, %s" % (k, state)
                            _nlist.append(("%s%s" % (_id, subid), k.strip()))
                            #print "Final:\t%s%s" % (_id, subid), k
                        except:
                            pass
                            #print "ERR: %s" % i
                else:
                    _nid = self._ptn_jhep_junk.sub("",_id) # keep one space
                    j = self._ptn_id.sub("", i, 1)
                    #print "Sub:\t", j
                    #_nlist.append((_id, i[i.rfind(_id)+len(_id):]))
                    _nlist.append((_nid, j))
            elif self._ptn_also.search(i):
                a,b = self._ptn_also.findall(i)[0]
                
                #a must be a letter, remove stray numbers:
                while not a[0].isalpha():
                    a = a[1:]
                    print ("a[] = %s " % a)
                
                _nlist.append((a, b))

    
        return _nlist

    def getInstitutesFacts(self):
        
        institutes = self.getInstitutes()
        
        outstr = u""

        #print ("*" * 35)
        #print "getInstitutesFacts: "

        for i,j in institutes:
            j = j.strip().rstrip(".").replace("\n"," ")
            #print i, j
            if j == "Deceased":
                outstr = "%s%s" % (outstr, 'has_deceased("%s")\n' % i)
            elif i[0].isdigit():
                outstr = "%s%s" % (outstr, 'isinstitute("%s", "%s")\n' % (i, j))
            else:
                outstr = "%s%s" % (outstr, 'isinstitute_notmember("%s", "%s")\n' % (i, j))
            
        #print ("*" * 35)
        #print "end of getInstitutesFacts: "

        return outstr
    
    def removeAuthorsBadNewLines(self, text):
        returnText = ""
        firstLine = True
        for line in text.splitlines():
            if line.startswith("proofs") or line.startswith("JHEP_"):
                continue
            if firstLine:
                returnText += line + "\n"
                firstLine = False
            else:
                if line.startswith(","):
                    returnText += line + "\n"
                else:
                    returnText = returnText[:-1] + " " + line + "\n"
        return returnText

    def getAuthors(self):
        
        if not self._authorTxt: 
                self.getAuthorText()

        self._authorTxt = self.removeLines(self._authorTxt)
        self._authorTxt = self.removeAuthorsBadNewLines(self._authorTxt)
        
        out_file = open(self._path + "/extracted_text.txt", "w")
        out_file.write(self._input)
        out_file.close()

        out_file = open(self._path + "/authors_text.txt", "w")
        out_file.write(self._authorTxt)
        out_file.close()

        _alist = self._authorPtn.findall(self._authorTxt)
        _nlist = []
        
        for i,j in _alist:
            #k = j.split(',')
            #while '' in k:
            #    k.remove('')
            
            # remove hyphenation
            i = i.strip('-').replace("\n","")
            hyph = i.count('-')
            
            if hyph > 0 and not "Khalil-zada" in i:
                #print i
                try:
                    m = [i[n] for n in range(0,len(i)) \
                         if i[n] != '-' or i[n+1].isupper()]
                    i = "".join(m)
                except:
                    print "Hyphenation error: %s" % i
            
            k = j.strip(",")
            
            if "Benary" in i: 
              print i
              print k
              print j
            _nlist.append((i,k))
            
        #print _nlist
        #sys.exit()
        return _nlist
        
    def getAuthorsFacts(self):
        authors = self.getAuthors()

        outstr = u""

        validChars = [".", "-", "'", " ", "˜", "˚", "¨", "˘", "¸", "Â", "´"]

        for nm,j in authors:
            if "uhrer" in nm:
              print ("x" * 30)
              print nm, 
              print j
            af = j.split(",")

            nm = nm.replace("\n","").strip()

            isValid = True
            # avoid strange cases
            for a in nm:
                
                if "uhrer" in nm:
                  print a
                if a.isdigit():
                    isValid = False
                    break
                if not a.isalpha() and a not in validChars:
                    isValid = False
                    break

            if not isValid:
                # print "Rejecting %s" % nm
                continue
                   
            if "uhrer" in nm:
              print ("x" * 30)
              print nm, 
              print j

            outstr = "%s%s" % (outstr, 'belongsto("%s", "%s")\n' % (nm, af[0]))
            
            #print nm, j
            for a in af[1:]:
              if a == self._deceased_char:
                outstr = "%s%s" % (outstr, 'isdeceased("%s")\n' % (nm))
              else:
                outstr = "%s%s" % (outstr, 'alsoat("%s", "%s")\n' % (nm, a))

        return outstr
