# -*- coding: utf-8 -*-
import json

class SynonymsChecker():

    filename = "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/proofs/synonyms/synonyms.json"
    jsonData = {}

    def __init__(self):
        #Read JSON data into the jsonData variable
        with open(self.filename, 'r') as f:
            self.jsonData = json.load(f)
        return

    def getInstSynonyms(self,inst_id):
        for inst in self.jsonData["institutes"]:
            if inst_id == inst["id"]:
                return inst["synonyms"]
        return []

    def getAuthorSynonyms(self,inspire, foafName):
        for auth in self.jsonData["authors"]:
            # if we have the inspire better check first for it
            if inspire != "":
                if inspire == auth["inspire"]:
                    return auth["synonyms"]
            # if not, must check for foafName
            elif foafName != "":
                if foafName == auth["foafName"]:
                    return auth["synonyms"]
            else:
                return []
        return []

    def insertInstitutes(self, inst_id, new_synonym, original):
        added = False
        for inst in self.jsonData["institutes"]:
            if inst_id == inst["id"]:
                if new_synonym in inst["synonyms"]:
                    # synonym already present
                    return
                else:
                    # must add new synonym
                    inst["synonyms"].append(new_synonym)
                    added = True

        if not added:
            # must add new record
            new_id = inst_id
            new_original = original
            new_syns = []
            new_syns.append(new_synonym)
            new_inst = {}
            new_inst["id"] = new_id
            new_inst["original"] = new_original
            new_inst["synonyms"] = new_syns
            self.jsonData["institutes"].append(new_inst)

        return

    def insertAuthors(self, inspire, foafName, new_synonym, original):
        added = False
        for auth in self.jsonData["authors"]:
            if ((inspire == auth["inspire"]) and (inspire != '')) or (foafName == auth["foafName"]):
                if new_synonym in auth["synonyms"]:
                    # synonym already present
                    return
                else:
                    # must add new synonym
                    auth["synonyms"].append(new_synonym)
                    added = True
                    
        if not added:
            # must add new record
            new_inspire = inspire
            new_foafName = foafName
            new_original = original
            new_syns = []
            new_syns.append(new_synonym)
            new_author = {}
            new_author["inspire"] = new_inspire
            new_author["foafName"] = new_foafName
            new_author["original"] = new_original
            new_author["synonyms"] = new_syns
            self.jsonData["authors"].append(new_author)

        return

    def updateJson(self):
        with open(self.filename, "w") as jsonFile:
            json.dump(self.jsonData, jsonFile)
        return