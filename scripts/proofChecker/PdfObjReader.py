# -*- coding: utf-8 -*-

from authorlists import PDFAuthor
from authorlists import PDFInstitute

class PdfObjReader():

    def __init__(self, filename):
        # verify the file exists
        self.filename = filename
        self.authorList = list()
        self.institutesList = list()
        self.publisher = ""
        self.content = ""

    def populate_content(self):
        """
        reads pdf kfb file file
        """
        if self.content == "":
            with open(self.filename, 'r') as content_file:
                self.content = content_file.read().decode("utf-8")

    def populateAuthors(self):

        self.populate_content()

        authorList = list()

        authorObj = PDFAuthor()
        firstLine = True
        for line in self.content.splitlines():
            if line.startswith("belongsto"):
                if not firstLine:
                    authorList.append(authorObj)
                else:
                    firstLine = False

                authorObj = PDFAuthor()
                authorName = self.extractBelongsToAuthor(line)
                organization = self.extractBelongsToInstitute(line)

                authorObj.namePaper = authorName

                #print ("organization = %s" % organization)

                if ',' in organization:
                    organization = organization.split(",")
                    authorObj.organization = organization
                else:                    
                    authorObj.organization.append(organization)
            elif line.startswith("alsoat"):

                alsoAtName = self.extractAlsoAtName(line)
                alsoAtOrganization = self.extractAlsoAtInstitute(line)

                if alsoAtName == authorObj.namePaper:
                    authorObj.organization.append(alsoAtOrganization)
            elif line.startswith("isdeceased"):

                isDeceasedName = self.extractDeceasedName(line)
                if isDeceasedName == authorObj.namePaper:
                    authorObj.deceased = True

        # append last author that couldn't be added by "fistLine" test
        if authorObj:
            authorList.append(authorObj)

        # TODO: one single list instead of copying (due to 2 steps development)
        self.authorList = authorList

        return authorList 

    def populateInstitutes(self):

        self.populate_content()

        institutesList = list()

        instituteObj = PDFInstitute()

        for line in self.content.splitlines():
            if line.startswith("isinstitute_notmember"):

               instituteObj = PDFInstitute()
               instituteObj.id = self.extractInstituteNMId(line)
               instituteObj.alsoAt = True
               instituteObj.name = self.extractInstituteNMName(line)

               institutesList.append(instituteObj)

            elif line.startswith("isinstitute"):

               instituteObj = PDFInstitute()
               instituteObj.id = self.extractInstituteId(line)
               instituteObj.name = self.extractInstituteName(line)

               institutesList.append(instituteObj)
            """
            or \
               line.startswith("isinstitute"):
               print line
            """
        # TODO: one single list instead of copying (due to 2 steps development)

        self.institutesList = institutesList

        return institutesList

    def populatePublisher(self):

        self.populate_content()

        for line in self.content.splitlines():
            if line.startswith("publisher"):
                self.publisher = self.extractPublisher(line)
                break

    def extract(self, line, start):
        line = line.replace(start, "")
        line = line.replace("\"", "")
        line = line.replace("(", "")
        line = line.replace(")", "")
        elements = line.split(",", 1)
        if "," in elements:
            print ("elements = %s" % elements)
        return elements        

    def extractBelongsToAuthor(self, line):
        return self.extract(line, "belongsto")[0].strip()

    def extractBelongsToInstitute(self, line):
        return self.extract(line, "belongsto")[1].strip()

    def extractAlsoAtName(self, line):
        return self.extract(line, "alsoat")[0].strip()

    def extractAlsoAtInstitute(self, line):
        return self.extract(line, "belongsto")[1].strip()

    def extractInstituteNMId(self, line):
        return self.extract(line, "isinstitute_notmember")[0].strip()

    def extractInstituteNMName(self, line):
        return self.extract(line, "isinstitute_notmember")[1].strip()

    def extractInstituteId(self, line):
        return self.extract(line, "isinstitute")[0].strip()

    def extractInstituteName(self, line):
        return self.extract(line, "isinstitute")[1].strip()

    def extractDeceasedName(self, line):
        return self.extract(line, "isdeceased")[0].strip()

    def extractPublisher(self, line):
        return self.extract(line, "publisher")[0].strip()

