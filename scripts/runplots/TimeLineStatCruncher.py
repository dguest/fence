# -*- coding: utf-8 -*-
"""
#########################################################################################
# TimeLineStatCruncher.py
# Make useful? timeline statistics
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, April 12th, 2013
#########################################################################################
# $Rev: 197911 $
# $LastChangedDate: 2014-09-08 15:45:43 +0200 (Mon, 08 Sep 2014) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

from datetime import datetime
from datetime import timedelta
from math import fabs
import pkg_resources
pkg_resources.require("Matplotlib==1.2.0")

import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
import pylab
import numpy as np

from TimelineFormatter import TimelineFormatter


class TimeLineStatCruncher(TimelineFormatter):

    def __init__(self):
        TimelineFormatter.__init__(self)

        self._sub2RefDict = {}
        self._plbDict = {}
        self._ref2AcceptDict = {}
        self._acc2ProofDict = {}
        self._pro2PubDict = {}
        self._totalDict = {}

        self._sub2RefDict2y = {}
        self._plbDict2y = {}
        self._ref2AcceptDict2y = {}
        self._acc2ProofDict2y = {}
        self._pro2PubDict2y = {}
        self._totalDict2y = {}

        return

    def fillDictionaries(self):
        self.fillDict(self._sub2RefDict, "Journal Sub", "Referee Report On")
        self.fillDict(self._ref2AcceptDict, "Referee Report On", "Journal Approval Date")
        self.fillDict(self._acc2ProofDict, "Journal Approval Date", "Proofs On")
        self.fillDict(self._pro2PubDict, "Proofs On", "Published Online")
        self.fillDict(self._totalDict, "Journal Sub", "Published Online")

        self.fillDict(self._sub2RefDict2y, "Journal Sub", "Referee Report On", twoYears=True)
        self.fillDict(self._ref2AcceptDict2y, "Referee Report On", "Journal Approval Date", twoYears=True)
        self.fillDict(self._acc2ProofDict2y, "Journal Approval Date", "Proofs On", twoYears=True)
        self.fillDict(self._pro2PubDict2y, "Proofs On", "Published Online", twoYears=True)
        self.fillDict(self._totalDict2y, "Journal Sub", "Published Online", twoYears=True)
        return

    def fillDict(self, dct, lab1, lab2, twoYears = False):

        if twoYears:
            if self._currDct["Published Online"]:
                #if self._currDct["Ref Code"] == "PERF-2017-03":
                #    print ("published online on : %s" % self._currDct["Published Online"])
                datetime_str = self._currDct["Published Online"]
                datetime_obj = datetime.strptime(datetime_str, "%Y/%m/%d")
                datetime_obj = datetime_obj + timedelta(days = 730)
                #if self._currDct["Ref Code"] == "PERF-2017-03":
                #    print ("datetime object = %s" % datetime_obj)
                if datetime_obj < datetime.now():
                    return

        if self._currDct["Ref Code"] in self._exclusionList:
           return

        _jrn = self._currDct["Journal"]
        
        # fill submission to 1st report
        if self._currDct[lab1] == "" or self._currDct[lab2] == "":
           return
           
        # get dates
        _d1 = datetime.strptime(self._currDct[lab1], "%Y/%m/%d")
        _d2 = datetime.strptime(self._currDct[lab2], "%Y/%m/%d")

        if not dct.has_key(_jrn):
            dct[_jrn] = []

        dct[_jrn].append((_d2-_d1).days)

        return

    def fillPLBDict(self):

        if self._currDct["Journal Sub"] == "" or self._currDct["Referee Report On"] == "" or self._currDct["Journal"] != "PLB":
           return

        # fill submission to 1st report
        _sub = datetime.strptime(self._currDct["Journal Sub"], "%Y/%m/%d")
        _ref = datetime.strptime(self._currDct["Referee Report On"], "%Y/%m/%d")

        if _sub.month < 3 or _sub.month > 11:
            _jrn = "Winter"
        elif _sub.month > 2 and _sub.month < 6:
            _jrn = "Spring"
        elif _sub.month > 5 and _sub.month < 9: 
            _jrn = "Summer"
        else:
            _jrn = "Autumn"

        if not self._plbDict.has_key(_jrn):
            self._plbDict[_jrn] = []

        self._plbDict[_jrn].append((_ref-_sub).days)
                                
        return

    def drawHist(self, key, col):

        _fig = pylab.figure()
        _fig.clf()

        inp = self._sub2RefDict[key]

        ax = _fig.add_subplot(111)

        hist, bins = np.histogram(inp, bins = 20)

        pylab.title(key, fontsize="x-large", weight='bold')

        width = 1*(bins[1]-bins[0])
        center = (bins[:-1]+bins[1:])/2
        ax.bar(center, hist, align = 'center', width = width, color = col)

        med = np.median(inp)
        mad = np.median([fabs(i-med) for i in inp])
        
        pylab.axvline(med, color="gray", ls='--')
        pylab.axvline(med-mad, color="gray", ls='-.')
        pylab.axvline(med+mad, color="gray", ls='-.')
        pylab.text(med, -1.,u"median: %g\u00B1%g" %  (med,mad), color="black", fontsize=8)

        pylab.text(-5,0,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9, rotation=90)
        
        pylab.savefig("TimeLine_%s.png" % key)
        pylab.savefig("TimeLine_%s.pdf" % key)

        pylab.close()
        
        return

    def drawScatterHist(self,dct,title,nm):

        #if nm == "Review":
        #    print ("dictionary = %s" % dct)

        _fig = pylab.figure()
        _fig.clf()

        ax = _fig.add_subplot(111)

        ax.set_title(title, fontsize="x-large", weight='bold')
        ax.set_xlabel("Days")
        #ax.get_xaxis().set_tick_params(top=False)

        x    = []
        y    = []
        xerr = []
        legs = []
        labs = []

        _tot = []

        texts = []

        i = 0 

        _gv = [(key,inp) for key,inp in dct.iteritems() if len(inp) > 4]

        colors = iter(pylab.cm.Paired(np.linspace(0,1,len(_gv))))

        for key,inp in _gv:
            _tot += inp

            hist, bins = np.histogram(inp, 20) #, (0,100))
            center = (bins[:-1]+bins[1:])/2

            i += 1

            #legs.append(key)

            med = np.median(inp)
            mad = np.median([fabs(j-med) for j in inp])

            #y.append(i+1)
            #x.append(med)
            #xerr.append(mad)

            c = colors.next()

            if key == "JHEP": c = [0.65098039,0.80784314,0.89019608,1.        ]
            elif key == "EPJC": c = [0.12156863,0.47058824,0.70588235,1.        ]
            elif key == "PRC": c = [0.2        ,0.62745098,0.17254902,1.        ]
            elif key == "PRD": c = [0.98431373,0.60392157,0.6        ,1.        ]
            elif key == "PRL": c = [0.99215686,0.74901961,0.43529412,1.        ]
            elif key == "JINST": c = [1.         ,0.49803922,0.         ,1.        ]
            elif key == "NPB": c = [0.41568627,0.23921569,0.60392157,1.        ]
            elif key == "NJP": c = [1.  ,1.  ,0.6 ,1. ]
            elif key == "PLB": c = [0.69411765,0.34901961,0.15686275,1.        ]

            errb = {'zorder'  : -100,
                    'alpha'   :  0.1,
                    'visible' :False,}

            #pylab.scatter(x=inp, y=[i+1]*len(inp), c=colors.next(), label=key, marker="o")

            ax.errorbar( med, -i-1, xerr=mad, c=c, mew=0, label=key, elinewidth=4, marker='+', **errb)
            ax.scatter(x=center, y=[-i-1]*len(center), s=[h*20. for h in hist], c=c, marker="o")

            # draw vert lines
            ymax = 1.-((i*1.)/(len(_gv)+1))
            ymin = 1.-((i+0.4)/(len(_gv)+1))
            
            ax.axvline(med, ymin, ymax, color=c, ls='--', lw=2)
            ax.axvline(med-mad, ymin, ymax,  color=c, ls='-.', lw=2)
            ax.axvline(med+mad, ymin, ymax,  color=c, ls='-.', lw=2)
            ax.axvspan(med-mad, med+mad, ymin, ymax,  color=c, alpha=0.5)
            ax.text(med, -i-1.6,u"%s median: %g\u00B1%g" %  (key,med,mad), color="black", fontsize=9)

            texts.append(ax.text(-5, -i-1, "%d" % hist.sum(), color="black", fontsize=9))

            legs.append(pylab.Rectangle((0, 0), 1, 1, fc=c))
            labs.append(key)
            
        ax.get_yaxis().set_ticks([])
        ax.set_xlim(0, ax.get_xlim()[1])

        pylab.legend(legs, labs, loc="upper right")

        # place text in a smart way
        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()
        w = (xmax - xmin) / 500.
        h = (ymax - ymin) / 300.

        for i in texts:
           i.set_x(xmin-(20*w))
        
        med = np.median(_tot)
        mad = np.median([fabs(j-med) for j in _tot])
        
        pylab.text(xmin-(40*w),-len(_gv)+1,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9, rotation=90)
        pylab.text(xmin+(5*w),ymax-0.4,u"FOR INTERNAL USE ONLY        Overall median: %g\u00B1%g" %  (med,mad), fontsize=12)

        pylab.savefig("TimeLine_%s.png" % nm)
        pylab.savefig("TimeLine_%s.pdf" % nm)

        pylab.close()

        return

    def doFormatting(self):

        skipFirst = True # the true headers

        for r in self._Reader:
            if skipFirst:
                skipFirst = False
                continue
            
            self._currDct = r
            
            self.fillDictionaries()
            self.fillPLBDict()

        #colors = pylab.cm.Paired(np.linspace(0,1,len(self._sub2RefDict.keys())))

        #for i,(key,inp) in enumerate(self._sub2RefDict.iteritems()):
        #    if len(inp) < 5:
        #        continue
        #    self.drawHist(key,colors[i])

        self.drawScatterHist(self._sub2RefDict,"Submission to 1st Review","Review")
        self.drawScatterHist(self._ref2AcceptDict,"1st Review to Acceptance","Accept")
        self.drawScatterHist(self._acc2ProofDict,"Acceptance to 1st Proof","Proof")
        self.drawScatterHist(self._pro2PubDict,"Proof to Publication","Published")
        self.drawScatterHist(self._totalDict,"Submission to Publication","Total")
        
        self.drawScatterHist(self._sub2RefDict2y,"Submission to 1st Review","Review2y")
        self.drawScatterHist(self._ref2AcceptDict2y,"1st Review to Acceptance","Accept2y")
        self.drawScatterHist(self._acc2ProofDict2y,"Acceptance to 1st Proof","Proof2y")
        self.drawScatterHist(self._pro2PubDict2y,"Proof to Publication","Published2y")
        self.drawScatterHist(self._totalDict2y,"Submission to Publication","Total2y")
        
        #self.drawScatterHist(self._plbDict,"Submission to Referee Report (PLB)","PLB")

        return
            
    def processCSV(self):

        self.setReader()

        self.doFormatting()


        return
    
