#!/usr/bin/env python2

import csv
import sys

import numpy as np
import pandas as pd
import matplotlib as mpl
if mpl.get_backend() != "Agg":
    mpl.use('Agg', warn=False)
import matplotlib.pyplot as plt
import pylab
from shutil import copyfile

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position


class MembershipQualificationPlot(object):

    def __init__(self):
        self.dbm = DBManager()
        self.labels = {}
        self.qualifications = {}
        self.qual_dict = {}
        self.totals = []
        self.dataframe = None
        self.zipponelength = None

    def load_labels(self):
        labels = self.dbm.execute("""
            SELECT * FROM PROJECT
            """)

        for label in labels:
            if label[1] == "Upgrade Project Without TDR":
                label = (label[0], "Upgrade Project\nWithout TDR")
            self.labels[label[0]] = label[1]

    def load_qualifications(self):
        if not self.labels:
            self.load_labels()

        qualifications = self.dbm.execute("""
            SELECT QUAL_RESP, EXTRACT(year from PROPOSED_QUAL_START) FROM MEMB WHERE PROPOSED_QUAL_START IS NOT NULL AND QUAL_RESP IS NOT NULL;
            """)

        qualification_dict = {}
        for qualification in qualifications:
            if str(qualification[0]) not in qualification_dict:
                qualification_dict[str(qualification[0])] = {}
            if str(qualification[1]) not in qualification_dict[str(qualification[0])]:
                qualification_dict[str(qualification[0])][str(qualification[1])] = 1
            else:
                qualification_dict[str(qualification[0])][str(qualification[1])] += 1
        if "10" not in qualification_dict:
            qualification_dict["10"] = {}

        self.qualifications = qualification_dict

        qual_dict = {}
        for qualification in qualifications:
            if str(qualification[1]) not in qual_dict:
                qual_dict[str(qualification[1])] = {}
                for i in range(0, len(self.labels)):
                    qual_dict[str(qualification[1])][str(i)] = 0
            qual_dict[str(qualification[1])][str(qualification[0])] += 1

        self.qual_dict = qual_dict

    def write_csv(self):
        with open('qualifications.csv', 'w') as outcsv:
            writer = csv.writer(outcsv)
            writer.writerow(["year"] + [self.labels[i] for i in range(0, len(self.labels))] + ["total per year"])

            for i in range(2009, 2021):
                writer.writerow(
                    [str(i)]
                    + [self.qual_dict[str(i)][str(x)] for x in range(0, len(self.qual_dict[str(i)]))]
                    + [sum(self.qual_dict[str(i)].values())]
                    )

            writer.writerow(
                ["Total per system"]
                + [sum(self.qualifications[str(x)].values()) for x in range(0, len(self.qualifications))]
                )

    @staticmethod
    def list_zip_sums(listing, size):
        sum_list = []
        for index in range(0, len(listing[0])):
            sum_list.append(0)
        for i in range(0, size+1):
            for index in range(0, len(listing[0])):
                sum_list[index] += listing[i][index]

        return sum_list

    def calculate_totals(self):
        one_level_dict = {}
        lista = []
        first_lista = [self.qualifications["0"].get(str(x), 0) for x in range(2009, 2021)]
        for index in range(1, len(self.qualifications)):
            one_level_dict[str(index)] = [self.qualifications[str(index)].get(str(x), 0) for x in range(2009, 2021)]
            lista.append([self.qualifications[str(index)].get(str(x), 0) for x in range(2009, 2021)])

        zippone = zip(first_lista, *lista)
        zipponelist = list(zippone)

        self.dataframe = pd.DataFrame(zipponelist)

        self.zipponelength = len(zipponelist[0])

        self.totals = []
        for b_index in range(0, len(zipponelist)):
            somma = sum(zipponelist[b_index])
            self.totals.append(somma)

    def arms_race(self, total=False):
        _fig = pylab.figure(figsize=(9, 4))
        _fig.clf()
        year_labels = [x_index for x_index in range(2009, 2020)]

        pylab.title("Qualification tasks started per system and per year", fontsize="x-large")
        ind = np.arange(len(year_labels))
        ax_subplot = _fig.add_subplot(111)
        box = ax_subplot.get_position()
        ax_subplot.set_position([0.045, box.y0, box.width * 0.90, box.height])

        plot = []
        colors = [(0, 0.55, 0.94), (0.97, 0.23, 0.22), (1, 0.72, 0.19), (0.05, 0.66, 0.35),
                  (1, 0.40, 0.11), (0, 0.75, 0.77), (0.42, 0.67, 0.95), (0.97, 0.47, 0.45),
                  (1, 0.80, 0.36), (1, 0.58, 0.33), (0.40, 0.76, 0.55), (0.67, 0.82, 0.98), 
                  (0.42, 0.82, 0.84), (1, 0.70, 0.68), (1, 0.88, 0.63), (1, 0, 0.88), (0, 0, 0)]

        for plot_index in range(0, len(self.qualifications)):
            serie1 = self.qualifications[str(plot_index)]
            plot_data = []
            for x_index in range(2009, 2020):
                if str(x_index) in serie1:
                    plot_data.append(serie1[str(x_index)])
                else:
                    plot_data.append(0)

            if plot_index % 4 == 0:
                linestyle = "-"
            elif plot_index % 4 == 1:
                linestyle = "--"
            elif plot_index % 4 == 2:
                linestyle = "-."
            elif plot_index % 4 == 3:
                linestyle = ":"

            plot.append(ax_subplot.plot(ind, plot_data, label=self.labels[plot_index],
                                        linewidth=2, color=colors[plot_index % len(colors)],
                                        linestyle=linestyle))

        if total:
            plot.append(ax_subplot.plot(ind, self.totals[0:11], label="totals", linewidth=1))
            pylab.legend([plot[i][0] for i in range(0, len(self.qualifications) + 1)],
                         [self.labels[x] for x in range(0, len(self.labels))] + ["TOTALS"],
                         fontsize='medium',
                         frameon=False,
                         loc='center left',
                         bbox_to_anchor=(1, 0.5))
        else:
            pylab.legend([plot[i][0] for i in range(0, len(self.qualifications))],
                         [self.labels[x] for x in range(0, len(self.labels))],
                         fontsize='medium',
                         frameon=False,
                         loc='center left',
                         bbox_to_anchor=(1, 0.5))
        pylab.xticks([0, 2, 4, 6, 8, 10], [str(x_index) for x_index in range(2009, 2020, 2)])
        pylab.grid(b=True)
        if total:
            pylab.savefig("membership-qualification-total.png")
            pylab.savefig("membership-qualification-total.pdf")
        else:
            pylab.savefig("membership-qualification.png")
            pylab.savefig("membership-qualification.pdf")


    def barh_plot(self):

        plt.figure()
        plt.clf()

        colors = [(0, 0.55, 0.94), (0.97, 0.23, 0.22), (1, 0.72, 0.19), (0.05, 0.66, 0.35),
                  (1, 0.40, 0.11), (0, 0.75, 0.77), (0.42, 0.67, 0.95), (0.97, 0.47, 0.45),
                  (1, 0.80, 0.36), (0.40, 0.76, 0.55), (1, 0.58, 0.33), (0.42, 0.82, 0.84),
                  (0.67, 0.82, 0.98), (1, 0.70, 0.68), (1, 0.88, 0.63), (1, 0, 0.88), (0, 0, 0)]

        barh = []
        for b_index in range(0, self.zipponelength):
            barh.append([(float(i) / float(j)) * 100 for i, j in zip(self.dataframe[b_index], self.totals)])

        r_labels = [str(x) for x in range(2009, 2021)]

        width = 0.75
        _fig = pylab.figure(figsize=(10, 6))
        _fig.clf()
        ax = _fig.add_subplot(111)
        box = ax.get_position()
        ax.set_position([0.05, box.y0, 0.70, box.height])
        ax.set_yticks(-0.38 + np.arange(len(r_labels)) + width / 2)
        ax.set_yticklabels(r_labels, minor=False)

        h_list = []

        h0 = plt.barh(r_labels, barh[0], height=0.75)
        h_list.append(h0)

        mpl.rcParams['hatch.linewidth'] = 0.15

        for index in range(1, len(barh)):
            left_on = MembershipQualificationPlot.list_zip_sums(barh, index-1)

            if index % 4 == 0:
                hatch = ''
            elif index % 4 == 1:
                hatch = '////'
            elif index % 4 == 2:
                hatch = '|||'
            elif index % 4 == 3:
                hatch = '\\\\\\'

            h0 = plt.barh(r_labels, barh[index], left=left_on, height=0.75, linewidth=2, color=colors[index-1 % len(colors)], hatch=hatch)
            h_list.append(h0)

        plt.legend(h_list, [self.labels[x] for x in range(0, len(self.labels))], loc='center left', bbox_to_anchor=(1, 0.5))

        plt.title("QTs started per year and system", fontsize=22)
        plt.savefig("qts_per_year.png")
        plt.savefig("qts_per_year.pdf")


    def run(self):
        self.load_labels()
        self.load_qualifications()
        self.calculate_totals()
        self.write_csv()
        self.arms_race()
        self.arms_race(total=True)
        self.barh_plot()
        copyfile("qts_per_year.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/qts_per_year.png")
        copyfile("qts_per_year.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/qts_per_year.pdf")
        copyfile("membership-qualification.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/membership-qualification.png")
        copyfile("membership-qualification.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/membership-qualification.pdf")
        copyfile("membership-qualification-total.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/membership-qualification-total.png")
        copyfile("membership-qualification-total.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/membership-qualification-total.pdf")
        copyfile("qualifications.csv", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/qualifications.csv")




MEMBERSHIP = MembershipQualificationPlot()
MEMBERSHIP.run()
