#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import csv
import sys
from datetime import datetime

import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
import pylab

# todo: remove HARDCODED constant
sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position


mgr = DBManager()
monthly_n = mgr.execute("""
SELECT 
to_char(REFDATE,'YY-MM-DD') as refDate, 
AUTH_COUNT authors
FROM ADBPMAP 
WHERE PAPER_NAME LIKE 'Monthly-%' 
AND PAPER_TYPE = 0 
AND REFDATE >= TO_DATE('2012/06/01', 'yyyy/mm/dd')
AND refDate IN (SELECT refDate FROM ADBPMAP WHERE PAPER_NAME LIKE 'Monthly-pdtc%')
ORDER BY REFDATE DESC
    """)

monthly_y = mgr.execute("""
SELECT 
to_char(REFDATE, 'YY-MM-DD') as refDate, 
AUTH_COUNT authors
FROM ADBPMAP 
WHERE PAPER_NAME LIKE 'Monthly-pdtc%'  
AND REFDATE >= TO_DATE('2012/06/01', 'yyyy/mm/dd')
AND refDate IN (SELECT refDate FROM ADBPMAP WHERE PAPER_NAME LIKE 'Monthly%' AND PAPER_TYPE = 0)
ORDER BY REFDATE DESC
    """)

mgr.close()

db_y_n = [int(i[1]) for i in monthly_n if i is not None and i[1] is not None]
db_x_n = [datetime.strptime(i[0],'%y-%m-%d').toordinal() for i in monthly_n  if i is not None and i[1] is not None]
db_y_y2 = [int(i[1]) for i in monthly_y  if i is not None and i[1] is not None]
db_x_y2 = [datetime.strptime(i[0],'%y-%m-%d').toordinal() for i in monthly_y  if i is not None and i[1] is not None]

_fig = pylab.figure()
_fig.clf()

ax = _fig.add_subplot(111)

ax.set_title("ATLAS authors", fontsize="x-large", weight='bold', ha='center')
ax.set_xlabel("Year")
ax.set_ylabel("Number of authors")

ax.plot(db_x_y2, db_y_y2, marker='', color='r')
ax.plot(db_x_n, db_y_n, marker='', color='b')
ax.grid(True)

newlabels = [y for y in range(2011,datetime.today().year+1)]
newticks = [datetime(y,1,1).toordinal() for y in newlabels]

ax.set_xticks(newticks)
ax.set_xticklabels(newlabels)

legs = [pylab.Rectangle((0, 0), 1, 1, fc='r'),  pylab.Rectangle((0, 0), 1, 1, fc='b')]
labs = ["Incl. pre-datataking credits","Not incl. pre-datataking credits"]

pylab.legend(legs, labs, loc="upper right")

xmin, xmax = ax.get_xlim()

ax.set_ylim(2600,3050)

ymin, ymax = ax.get_ylim()

w = (xmax - xmin) / 500.
h = (ymax - ymin) / 300.


pylab.text(xmax+(10*w),ymax-100,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9, rotation=90)
pylab.text(xmin+(7*w),ymax-35,"FOR INTERNAL USE ONLY", fontsize=10)

pylab.savefig("db_total_authors.png")
pylab.savefig("db_total_authors.pdf")

pylab.close()

all_data = list()

for num,el in enumerate(monthly_y):
    all_data.append( ('20' + str(el[0]), el[1], monthly_n[num][1]) )

with open('db_atlas_authors.csv', mode='w') as csv_file:
    csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    csv_writer.writerow(['date','not. incl. pre-datataking credits','incl. pre-datataking credits'])
    for row in all_data:
        csv_writer.writerow(row)
