#!/usr/bin/python

"""
creates plots for CSM publications
"""

import sys
import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)

import matplotlib.pyplot as plt


sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position

def runplots():
    """
    handles operations to create the CMS plots
    """
    dbm = DBManager()

    results = dbm.execute("""SELECT PRE_PUBLICATION, IPOTETIC_RUN, REF_CODE 
                          FROM CDS_CMS_DATA 
                          WHERE ignore=0 
                          AND REF_CODE IS NOT NULL
                          AND PRE_PUBlICATION IS NOT NULL""")

    #print(results)

    run1 = {}
    run2 = {}
    fullrun2 = {}

    dates = []

    for result in results:
        data = result[0]
        run = result[1]


        if 'CFT' in result[2]:
            continue

        if 'CMS-MUO-10-001' in result[2]:
            continue

        # are we missing a date? skip it; it should be filled in the future automatically
        if not data:
            continue


        if run == 0:
            run = 2

        data_string = data.strftime("%Y-%m")

        if data_string not in dates:
            dates.append(data_string)

        if run == 1:
            run1[data_string] = run1.get(data_string, 0) + 1
        elif run == 2 or run == 0:
            run2[data_string] = run2.get(data_string, 0) + 1
        elif run == 3:
            fullrun2[data_string] = fullrun2.get(data_string, 0) + 1
        else:
            print("[WARN] can't assign run to paper")

    dates_sorted = sorted(dates)

    plotrun1 = []
    count = 0
    for data in dates_sorted:
        count += run1.get(data, 0)
        plotrun1.append(count)

    plotrun2 = []
    count = 0
    for data in dates_sorted:
        count += run2.get(data, 0)
        plotrun2.append(count)

    plotfullrun2 = []
    count = 0
    for data in dates_sorted:
        count += fullrun2.get(data, 0)
        plotfullrun2.append(count)

    allpapers = []
    count = 0
    for data in dates_sorted:
        count += run1.get(data, 0)
        count += run2.get(data, 0)
        count += fullrun2.get(data, 0)
        allpapers.append(count)

    plt.figure(figsize=(8, 7))
    plt.title("CMS publications run - (internal)", size=24)

    plt.plot([n for n, _ in enumerate(dates_sorted[0:122])],
             allpapers[0:122], label='CMS Run 1+2', color='b', linewidth=2)

    plt.plot([n for n, _ in enumerate(dates_sorted)],
             plotrun1, label='CMS Run 1', color='k', linewidth=2)

    plt.plot([n for n, _ in enumerate(dates_sorted)],
             plotrun2, label='CMS Partial Run 2', color='r', linewidth=2, linestyle='dashed')

    plt.plot([n for n, _ in enumerate(dates_sorted)],
             plotfullrun2, label='CMS Full Run 2', color='r', linewidth=2)

    plt.xlim(right=len(allpapers) - 1)

    plt.xticks([i for i, val in enumerate(dates_sorted) if "-01" in val],
               [val for i, val in enumerate(dates_sorted) if "-01" in val], rotation=70, size=8)

    plt.text(len(dates_sorted), max(allpapers), max(allpapers), fontsize=8)
    plt.text(len(dates_sorted), max(plotrun1), max(plotrun1), fontsize=8)
    plt.text(len(dates_sorted), max(plotrun2), max(plotrun2), fontsize=8)
    plt.text(len(dates_sorted), max(plotfullrun2), max(plotfullrun2), fontsize=8)


    # display values on figure on last month of year
    for index, value in enumerate(dates_sorted):
        if value[-2:] == "12":
            plt.axvline(x=index, color="gray", ls='--')
            if allpapers[index] != 0:
                plt.text(index - 1.5, allpapers[index] + 20, "%d" % allpapers[index], fontsize=8, color="blue")
            if plotrun1[index] != allpapers[index]:
                plt.text(index, plotrun1[index] - 20, "%d" % plotrun1[index], fontsize=8, color="black")
            if plotrun2[index]:
                plt.text(index, plotrun2[index] + 20, "%d" % plotrun2[index], fontsize=8, color="r")
            if plotfullrun2[index]:
                plt.text(index, plotfullrun2[index] + 20, "%d" % plotfullrun2[index], fontsize=8, color="r")


    plt.legend(loc=2)
    plt.savefig("cms_runs.png")
    plt.savefig("cms_runs.pdf")

runplots()
