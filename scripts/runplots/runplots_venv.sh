source /afs/cern.ch/user/a/atlaspo/po-scripts/venv/runplots_membership/bin/activate
cd /afs/cern.ch/user/a/atlaspo/po-scripts/scripts/runplots/
/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/runplots/membership_qualification.py 2>&1
deactivate

/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/cdsData/CMSdata.py

source /afs/cern.ch/user/a/atlaspo/po-scripts/venv/project_runplots/bin/activate
cd /afs/cern.ch/user/a/atlaspo/po-scripts/scripts/runplots/
/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/runplots/plotcruncher_collision_venv.py # 2>&1 > paperlog.txt #/dev/null
/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/runplots/db_makeAuthorListPlot_venv.py 2>&1 /dev/null
/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/runplots/cms_plots.py
/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/runplots/editorial_board_paper_phase0.py
/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/runplots/p0_meetings.py

if [[ $(date +%d) == 02 ]]
    then 
    /afs/cern.ch/user/a/atlaspo/twikirun/testHRStats.py 2>&1 /dev/null
fi

#cp *.png /afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/
cp *.png /eos/home-a/atlaspo/www/twiki_include/plots/
#cp *.pdf /afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/
cp *.pdf /eos/home-a/atlaspo/www/twiki_include/plots/
#cp db_atlas_authors.csv /afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/
cp db_atlas_authors.csv /eos/home-a/atlaspo/www/twiki_include/plots/

/afs/cern.ch/user/a/atlaspo/po-scripts/scripts/runplots/parse_stats_venv.py 2>&1

rm *.png
rm *.pdf
rm db_atlas_authors.csv

deactivate


