# -*- coding: utf-8 -*-
"""
#########################################################################################
# CumulativePlotCruncher.py
# Make useful? cumulative statistics
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, September 3rd, 2013
#########################################################################################
# $Rev: 240444 $
# $LastChangedDate: 2015-09-23 01:23:44 +0200 (Wed, 23 Sep 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

from datetime import datetime
import colorsys
import pkg_resources
pkg_resources.require("Matplotlib==1.2.0")

import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
import pylab
import numpy as np

from CumulativeFormatter import CumulativeFormatter


class CumulativePlotCruncher(CumulativeFormatter):

    def __init__(self):
        CumulativeFormatter.__init__(self)

        self._allDict = {}

        self._colorMap = {}

        # 2017/JUN/14: added manually TCAL-2017-01 to the exclusionList to make it work. ASK P.O. if they are indeed to be excluded
        self._exclusionList = ["PERF-2007-01", "LARG-2009-01", "LARG-2009-02", "IDET-2010-01", "MUON-2010-01", "TCAL-2010-01", "PERF-2010-02", "SOFT-2010-01", "TCAL-2017-01", "SOFT-2017-01"] 

        self._groups = ["BPHY","EXOT","HIGG","HION","PERF","STDM","SUSY","TOPQ","GENR","TECH","SOFT"]
        colors = pylab.cm.Paired(np.linspace(0,1,len(self._groups)))
        
        self._statusDict = {}

        for i,j in enumerate(self._groups):
            self._colorMap[j]   = colors[i]
            self._allDict[j]    = {0 : {}, 1 : {}, 2 : {}}
            self._statusDict[j] = {}
            self._statusDict[j][0] = {"Edboard" : 0, "Draft 1" : 0, "Draft 2" : 0, "Submission" : 0}
            self._statusDict[j][1] = {"Edboard" : 0, "Draft 1" : 0, "Draft 2" : 0, "Submission" : 0}
            self._statusDict[j][2] = {"Edboard" : 0, "Draft 1" : 0, "Draft 2" : 0, "Submission" : 0}

        self._groups.append("Overall")
        self._allDict["Overall"]    = {0 : {}, 1 : {}, 2 : {}}
        self._statusDict["Overall"] = {}
        self._statusDict["Overall"][0] = {"Edboard" : 0, "Draft 1" : 0, "Draft 2" : 0, "Submission" : 0}
        self._statusDict["Overall"][1] = {"Edboard" : 0, "Draft 1" : 0, "Draft 2" : 0, "Submission" : 0}
        self._statusDict["Overall"][2] = {"Edboard" : 0, "Draft 1" : 0, "Draft 2" : 0, "Submission" : 0}

        self._colorMap["Overall"]   = (.2, .2, 1.)

        return

    def fillDictionaries(self):
        self.fillDict(self._allDict, "Creation Date", "Total")
        self.fillDict(self._allDict, "EdBoard Formed", "Edboard")
        self.fillDict(self._allDict, "Draft 1 Release", "Draft 1", "Edboard")
        self.fillDict(self._allDict, "Draft 2 Release", "Draft 2", "Draft 1")
        self.fillDict(self._allDict, "Journal Sub", "Submission",  "Draft 2") 
        self.fillDict(self._allDict, "Published Online", "Published", "Submission")
        #self.fillDict(self._ref2AcceptDict, "Referee Report On", "Journal Approval Date")
        #self.fillDict(self._acc2ProofDict, "Journal Approval Date", "Proofs On")
        #self.fillDict(self._pro2PubDict, "Proofs On", "Published Online")
        #self.fillDict(self._totalDict, "Journal Sub", "Published Online")
        self.fillStatus(self._statusDict)
        return

    def fillDict(self, dct, lab1, lab2, lab3=None):
        if self._currDct["Ref Code"] in self._exclusionList:
           return
        else:
           grp = self._currDct["Ref Code"].split("-")[0]
           if grp in self._perfGroups:
              grp = "PERF"
        # fill submission to 1st report
        if self._currDct[lab1] == "":
           return
      
        # get dates
        _d1 = datetime.strptime(self._currDct[lab1], "%Y/%m/%d").toordinal()

        if _d1 < datetime(2010,3,10).toordinal():
           return

        # TODO: correct SI 6449 which doesn't always load this value
        if self._currDct["Run"] == "":
          return
        run = int(self._currDct["Run"])

        _dct = dct[grp][run]

        if not _dct.has_key(_d1):
           _dct[_d1] = {}

        if not _dct[_d1].has_key(lab2):
           _dct[_d1][lab2] = 1
        else:
           _dct[_d1][lab2] += 1

        if lab3:
           if not _dct[_d1].has_key(lab3):
              _dct[_d1][lab3] = -1
           else:
              _dct[_d1][lab3] -= 1

        _dct = dct["Overall"][run]

        if not _dct.has_key(_d1):
           _dct[_d1] = {}

        if not _dct[_d1].has_key(lab2):
           _dct[_d1][lab2] = 1
        else:
           _dct[_d1][lab2] += 1

        if lab3:
           if not _dct[_d1].has_key(lab3):
              _dct[_d1][lab3] = -1
           else:
              _dct[_d1][lab3] -= 1
        
        return

    def fillStatus(self, dct):
        if self._currDct["Ref Code"] in self._exclusionList:
           return
        else:
           grp = self._currDct["Ref Code"].split("-")[0]
           if grp in self._perfGroups:
              grp = "PERF"
        
        # TODO: correct SI 6449 which doesn't always load this value
        if self._currDct["Run"] == "":
          return
        run = int(self._currDct["Run"])

        _dct = dct[grp][run]

        if self._currDct["Publication Status"] == "submission_active":
           _dct["Submission"] += 1
           dct["Overall"][run]["Submission"] += 1
        elif self._currDct["Publication Status"] == "phase3_active":
           _dct["Draft 2"] += 1
           dct["Overall"][run]["Draft 2"] += 1
        elif self._currDct["Publication Status"] == "phase1_active":
           if self._currDct["Draft 1 Release"] != "":
              _dct["Draft 1"] += 1
              dct["Overall"][run]["Draft 1"] += 1
           else:
              _dct["Edboard"] += 1
              dct["Overall"][run]["Edboard"] += 1

        return


    def accumulateHist(self, grp, lab, run=1):

        _dct = self._allDict[grp][run]

        x = sorted(set(_dct.keys()))

        y = []

        z = []

        for i in x:
           _y = 0
           for j in sorted(_dct.keys()):
              if j>i: break
              if _dct[j].has_key(lab):
                 _y += _dct[j][lab]
                 if _dct[j][lab]<0: z.append(j)
           y.append(_y)

        # last data as of today, fix for current status from DB (no transitions)
        x.append(datetime.today().toordinal())
        y.append(self._statusDict[grp][run][lab])
        #y.append(y[-1])

        return (np.array(x),np.array(y),np.array(z))


     
    def drawPipeHist(self, grp, key):

        _fig = pylab.figure()
        _fig.clf()

        toPlot = ["Edboard", "Draft 1", "Draft 2", "Submission"] # , "Published"]

        pos  = [0] * len(toPlot)
        ypos = [0] * len(toPlot)

        vfac = 1./len(toPlot)

        ax = _fig.add_subplot(111)
        ax.get_yaxis().set_tick_params(left=False, right=True)
        
        pylab.title("Pipeline %s" % grp, fontsize="x-large", weight='bold')

        #    * Accent
        #    * Dark2
        #    * Paired
        #    * Pastel1
        #    * Pastel2
        #    * Set1
        #    * Set2
        #    * Set3

        legs = []
        labs = []

        gray = '#cccccc'

        c = self._colorMap[grp] 

        for i,j in enumerate(toPlot):
        
            x1,y1,z1 = self.accumulateHist(grp, j, run=1)
            x2,y2,z2 = self.accumulateHist(grp, j, run=2)

            # need to merge the two arrays
            _tup1 = [(x1[k],y1[k]) for k in range(len(x1))]
            _tup2 = [(x2[k],y2[k]) for k in range(len(x2))]

            _tup = _tup1+_tup2
            _tup.sort(key=lambda x:x[0])
            
            # first dummy value will be skipped later on
            _y1 = [(0,0)]
            _y2 = [(0,0)]

            for t in _tup:

                if t in _tup1:
                    _y1.append(t)
                else:
                     _y1.append((t[0],_y1[-1][1]))
                if t in _tup2:
                    _y2.append(t)
                else:
                    _y2.append((t[0],_y2[-1][1]))            

            x =np.array([t[0] for t in _tup])
            y1=np.array([_t[1] for _t in _y1[1:]])
            y2=np.array([_t[1] for _t in _y2[1:]])          

            if i != 0:
               pos[i] += max(y1+y2)/2.

            ypos[i] = y1[-1]+y2[-1]

            #alpha = 1.-(0.1*i)

            r,g,b = mpl.colors.colorConverter.to_rgb(c)
            h,s,v = colorsys.rgb_to_hsv(r,g,b)
            v = min(v*vfac*(i+1),1.)
            _c = colorsys.hsv_to_rgb(h,s,v)
            _c2 = colorsys.hsv_to_rgb(h,s,min(1.,v*2))

            # first the run 2
            pylab.fill_between(x, pos[i]-(y2/2.), pos[i]+(y2/2.), facecolor=_c2, edgecolor=_c)
            # then the run 1 upper slice
            pylab.fill_between(x, pos[i]+(y2/2.), pos[i]+((y1+y2)/2.), facecolor=_c, edgecolor=_c)
            # then the run 1 lower slice
            pylab.fill_between(x, pos[i]-(y2/2.), pos[i]-((y1+y2)/2.), facecolor=_c, edgecolor=_c)

            legs.append(pylab.Rectangle((0, 0), 1, 1, fc=_c))
            labs.append(j)

            if i != len(toPlot)-1:
               pos[i+1] = pos[i] + max(y1+y2)/2. + 1
               
            #if i != 0:
            #   pylab.vlines(z, pos[i-1], pos[i], gray, zorder=0)


        ind = []
        lab = []

        for i in range(2011,datetime.today().year+1):
           ind.append(datetime(i,1,1).toordinal())
           lab.append(`i`)

        
        ax.set_xticks(ind)
        ax.set_xticklabels(lab)

        ax.yaxis.tick_right()
        ax.set_yticks(pos)
        ax.set_yticklabels(ypos)

        #pylab.axvline(med, color="gray", ls='--')
        #pylab.axvline(med-mad, color="gray", ls='-.')
        #pylab.axvline(med+mad, color="gray", ls='-.')
        #pylab.text(med, -1.,"median: %g+/-%g" %  (med,mad), color="black", fontsize=8)

        pylab.legend(reversed(legs), reversed(labs), loc="upper left", fontsize=8)

        ymin, ymax = ax.get_ylim()

        pylab.text(x[0],1.3*ymin,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9)
        
        pylab.savefig("Cumulative_%s_%s.png" % (key,grp))
        pylab.savefig("Cumulative_%s_%s.pdf" % (key,grp))

        pylab.close()
        
        return
     
    def doFormatting(self):

        skipFirst = True # the true headers

        for r in self._Reader:
            if skipFirst:
                skipFirst = False
                continue
            
            self._currDct = r
            
            self.fillDictionaries()
 
        for i in self._groups:
                self.drawPipeHist(i, "Pipe")

 

        return
            
    def processCSV(self):

        self.setReader()

        self.doFormatting()

        return
    
