# -*- coding: utf-8 -*-
"""
#########################################################################################
# ConfNotePaperPlotsCruncher.py
# Make useful? confnote statistics
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, February 7th, 2013
#########################################################################################
# $Rev: 238858 $
# $LastChangedDate: 2015-03-31 00:16:34 +0200 (Tue, 31 Mar 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import csv
from datetime import datetime
import pkg_resources
pkg_resources.require("Matplotlib==1.2.0")

import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
import pylab
import numpy as np

from PyPlotCruncher import PyPlotCruncher

class ConfNotePaperPlotsCruncher(PyPlotCruncher):

    def __init__(self):
        PyPlotCruncher.__init__(self)

        self._allCNDict = {}

        return

    def setCNReader(self):
        self._CNReader = csv.DictReader(self._CNinFile)
        return

    def setCNInfile(self, fn):
        f = open(fn)
        self._CNinFile = f
        return
    
    def fillDict(self):

        if self._currDct["Lead Group"] == '' or self._currDct["Ref Code"] in self._exclusionList :
            return

        grp = self._currDct["Lead Group"]

        # merge all perf groups into one:
        if grp in self._perfGroups:
            grp = "PERF"

        try:
            self._zeroGroups.remove(grp)
        except:
            pass

        if not self._allDict.has_key(grp):
            self._allDict[grp] = 1
        else:
            self._allDict[grp] += 1

        return

    def fillCNDict(self):

        if self._currDct["Final Sign Off"] == '' or self._currDct["Lead Group"] == '':
            return

        grp = self._currDct["Lead Group"]

        # merge all perf groups into one:
        if grp in self._perfGroups:
            grp = "PERF"

        try:
            self._zeroGroups.remove(grp)
        except:
            pass

        if not self._allCNDict.has_key(grp):
            self._allCNDict[grp] = 1
        else:
            self._allCNDict[grp] += 1

        return

    def createBubbleChart(self, labels, title="", outfile="test.png"):

        _fig = pylab.figure()
        _fig.clf()

        ax = _fig.add_subplot(111)

        ax.set_title(title, fontsize="x-large", weight='bold', ha='right')
        ax.set_xlabel("ConfNotes")
        ax.set_ylabel("Papers")

        legs = []

        i = 0 

        colors = iter(pylab.cm.Paired(np.linspace(0,1,len(labels))))

        for l in labels:

            try:
                x = [self._allCNDict[l]]
            except:
                x = [0]

            try:
                y = [self._allDict[l]]
            except:
                y = [0]            

            c = colors.next()

            ax.scatter(x, y, s=[10*(i+j) for i,j in zip(x,y)], c=c, marker="o", zorder=2)

            legs.append(pylab.Rectangle((0, 0), 1, 1, fc=c))

        ax.set_ylim(0,ax.get_ylim()[1])

        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()

        w = (xmax - xmin) / 500.
        h = (ymax - ymin) / 300.
        
        pylab.text(xmin-(70*w),ymax/2,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9, rotation=90)
        #pylab.text(xmin+(20*w),ymax-0.4,"FOR INTERNAL USE ONLY", fontsize=12)

        aspect = (1.*ymax)/xmax

        par1 = ax.twinx()
        par1.set_ylabel("Paper/ConfNote Ratio")        
        par1.set_ylim(ymin,aspect)


        newticks  = []
        newlabels = []

        for i in range(0,100,10):

            # alternate styles
            ls = ['--','-.'][(i/10)%2]

            yt = 1.*i/100.
            
            #par1.add_line(pylab.Line2D([0, xmax], [0, xmax*(i/100.)], color="gray", ls='--'))
            par1.add_line(pylab.Line2D([0, xmax], [ymin, yt], color="gray", ls=ls, zorder=1))

            if yt > aspect:
                newticks.append(ymax/yt)
                newlabels.append("%.1g" % yt)

        par1.add_line(pylab.Line2D([xmin, xmax], [ymin, 1], color="red", ls='--', zorder=1))
        newticks.append(ymax)
        newlabels.append("1.0")

        par2 = ax.twiny()
        par2.set_xlim(xmin, xmax)
        par2.set_xticks(newticks)
        par2.set_xticklabels(newlabels)
        #par2.get_xaxis().set_ticks_position('top')
        
        #for i in range(110,210,10):
        #    ax.add_line(pylab.Line2D([0, ymax*(100./i)], [0, ymax], color="gray", ls='--'))

        pylab.legend(legs, labels, loc="upper left")

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()

        return

    def doFormatting(self):


        for r1 in self._Reader:
            self._currDct = r1
            self.fillDict()

        for r2 in self._CNReader:
            self._currDct = r2 
            self.fillCNDict()

        # remove zero entry groups, sort from lowest to highest
        groups = [ i for i in self._allGroups if not i in self._zeroGroups ]

        labels = [i for i in groups if not i in self._zeroGroups]
        self.createBubbleChart(labels, title="ATLAS - ConfNotes/Papers", outfile="confnotes_papers.png")
            
        return


            
    def processCSV(self):

        self.setReader()

        self.setCNReader()

        self.doFormatting()

        return
    
