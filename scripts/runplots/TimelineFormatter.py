# -*- coding: utf-8 -*-
"""
#########################################################################################
# TimelineFormatter.py
# Derived class to format CSV to Twiki tables, Glance query 5449
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, May 30th, 2012
#########################################################################################
# $Rev: 240444 $
# $LastChangedDate: 2015-09-23 01:23:44 +0200 (Wed, 23 Sep 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import csv
import re

from BaseFormatter import BaseFormatter

class TimelineFormatter(BaseFormatter):

    def __init__(self):
        super(TimelineFormatter, self).__init__()
        # |*Short Title*|*Ref Code*|*Editorial Board Chair*|*Full Editorial Board*|
        self._fieldnames=("Journal","Journal Tracking Ref","Full Title","Ref Code","Short Title","Creation Date","P 1 State","P 1 PUBCOMM Sign Off","P 1 Reviewers Sign Off","P 2 State","Draft 2 Release","Referee Report On","Days_1","Journal Approval Date","Days_2","Proofs On","Days_3","Days_4","Total","Phase 1 Released On","DIW-here","DIW-journal","Paper Approval Meeting","Editorial Board Draft 2 Sign-off","Pubcomm Chair Sign Off")
        self._ptn = re.compile('"(http.*?)"')
        return

    ### we need explicit mapping
    def setReader(self):
        self._Reader = csv.DictReader(self._inFile, ("ID/Link","Journal","Journal Tracking Ref","Full Title","Ref Code","Short Title","Creation Date","EdBoard Formed","P 1 State","Draft 1 Release","P 1 PUBCOMM Sign Off","P 1 Reviewers Sign Off","P 2 State","Draft 2 Release","Draft 2 Public Reading Date","Revised Draft 2 EdBoard Sign Off","Revised Draft 2 PubComm Sign Off","Revised Draft 2 SP Sign Off","Publication Status","Journal Sub","Referee Report On","Days_1","Journal Approval Date","Days_2","Proofs On","Days_3","Published Online","Days_4","Total","Phase 1 Released On","DIW-here","DIW-journal","Run","Paper Approval Meeting","Editorial Board Draft 2 Sign-off","Pubcomm Chair Sign Off"))

    def doFormatting(self):

        self._outFile.write("|")

        if self._filter:
            self.setFieldnames(["ID/Link","Journal","Journal Tracking Ref","Full Title","Ref Code","Short Title","Creation Date","P 1 State","P 1 PUBCOMM Sign Off","P 1 Reviewers Sign Off","P 2 State","Draft 2 Release","Publication Status","Journal Sub","Referee Report On","Journal Approval Date","Proofs On","Published Online","Paper Approval Meeting","Editorial Board Draft 2 Sign-off","Pubcomm Chair Sign Off", "Phase 1 Released On"])
            self.setWriter()

        headers = dict( (n,'*%s*'%n) for n in self._fieldnames )

        self._Writer.writerow(headers)

        skipFirst = True # the true headers

        for r in self._Reader:
            if skipFirst:
                skipFirst = False
                continue
                
            self._currDct = r

            if self._filter:
                found = False
                for i in self._filter:
                    if i.lower() in self._currDct["Ref Code"].lower():
                        found = True
                        break
                if not found:
                    continue

            self.addGlanceLink()

            self.cleanupTitle("Full Title")
            self.cleanupTitle("Short Title")
  
            # protect against empty items:
            for i in self._fieldnames:
                if self._currDct[i] == "":
                    self._currDct[i] = " "

            self._Writer.writerow(self._currDct)

         ### overwrite stray delimiter
        self._outFile.seek(-1,1)
