#!/usr/bin/python

import sys
import datetime
from collections import defaultdict
from dateutil.relativedelta import relativedelta

import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
#pylint: disable=wrong-import-position
import pylab
import numpy as np
from numpy import mean, sqrt, square

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager
#pylint: enable=wrong-import-position

dbm = DBManager()

phase0 = dbm.execute("""SELECT 
 ANALYSIS_SYS_PHASE_0.EDITORIAL_BOARD_FORMED_DATE,
 NULL
FROM 
 ANALYSIS_SYS,
 ANALYSIS_SYS_PHASE,
 ANALYSIS_SYS_PHASE_0 
WHERE 
 ANALYSIS_SYS.ID = ANALYSIS_SYS_PHASE.PUBLICATION_ID AND
 ANALYSIS_SYS_PHASE.ID = ANALYSIS_SYS_PHASE_0.PHASE_ID AND
 ANALYSIS_SYS_PHASE_0.EDITORIAL_BOARD_FORMED_DATE IS NOT NULL AND
 ANALYSIS_SYS.STATUS != 'phase0_closed' AND
 ANALYSIS_SYS.DELETED = 0
""")

papers = dbm.execute("""
SELECT 
 PHASE_1.MAIL_TO_EDBOARD, 
 PUBLICATION.JOURNAL_SUB,
 (SELECT SIGN_OFF FROM PHASE WHERE PHASE.PHASE = 4 AND PHASE.PUBLICATION_ID = PUBLICATION.ID)
FROM 
 PHASE,
 PHASE_1,
 PUBLICATION 
WHERE 
 PHASE.PUBLICATION_ID = PUBLICATION.ID AND
 PHASE.PHASE = 1 AND
 PHASE.ID = PHASE_1.PHASE_ID AND
 PUBLICATION.DELETED = 0 AND
 PHASE_1.MAIL_TO_EDBOARD IS NOT NULL AND
 PUBLICATION.SYS_PAPER = 0
    """)

results = phase0 + papers

active_counter = 0
active_since = []
active_period = defaultdict(int)
active_now_3_months = defaultdict(int)
closed_days_open = []
open_days_open = []
for result in results:
    if result[1]:
        difference = result[1] - result[0]
        closed_days_open.append(difference.days)
    else:
        active_counter += 1
        active_since.append(result[0])

        difference = datetime.datetime.now() - result[0]
        open_days_open.append(difference.days)
        three_months_diff = difference.days / 90
        active_now_3_months[three_months_diff] += 1

    start_date = result[0].replace(day=1, hour=0, minute=0, second=0, microsecond=0)
    if result[1]:
        end_date = result[1]
    else:
        end_date = datetime.datetime.now()
    current_date = start_date

    while current_date <= end_date:
        year = str(current_date.year)
        month = '%02d' % current_date.month

        active_period[year + "-" + month] += 1
        current_date += relativedelta(months=1)

arr = np.array(closed_days_open)
cdo_mean = mean(arr)
partial_diff = 0
for element in arr:
    partial_diff += square(element - cdo_mean)
med_diff = partial_diff / len(closed_days_open)
rms_two = sqrt(med_diff)

rms = sqrt(mean(square(arr)))

arr_open = np.array(open_days_open)
cdo_mean_open = mean(arr_open)
partial_diff = 0
for element in arr_open:
    partial_diff += square(element - cdo_mean_open)
med_diff = partial_diff / len(open_days_open)
rms_two_open = sqrt(med_diff)

rms_open = sqrt(mean(square(arr_open)))

fig = pylab.figure()
fig.clf()
pylab.title("Active paper and phase 0 Editorial Boards over time")
ax = fig.add_subplot(111)

data = []
labels = sorted(set(active_period.keys()))
for i, j in enumerate(labels):
    data.append(active_period[j])

ind = np.arange(len(labels))

for ii, i in enumerate(sorted(active_period.keys())):
    if i[-2:] == "12":
        pylab.axvline(x=ind[ii], color="gray", ls='--')
        pylab.text(ind[ii], -7, "%s" % (int(i[:4])+1), color="black", fontsize=8, weight='bold')

plot1 = ax.plot(ind, data, 'b-', linewidth=2)
ax.set_xlim(right=ind[-1])
ax.set_xticks([])

pylab.savefig("editorial_p_p0.png")
pylab.savefig("editorial_p_p0.pdf")
pylab.close()

bar_half_size = 0.4
mean = cdo_mean_open/30
mean_bar = (mean / 3) - bar_half_size
rms_plus = (cdo_mean_open + rms_two_open) / 30
rms_plus_bar = (rms_plus / 3) - bar_half_size
rms_minus = (cdo_mean_open - rms_two_open) / 30
rms_minus_bar = (rms_minus / 3) - bar_half_size


fig2 = pylab.figure(figsize=(8, 7))
fig2.subplots_adjust(bottom=0.12)
fig2.clf()
pylab.title("All paper and phase 0 active Editorial Boards (on {}: {})".format(datetime.datetime.now().date(), active_counter))
ax = fig2.add_subplot(111)
labels = sorted(active_now_3_months.keys())
bars = [active_now_3_months[i] for i in labels]
ind = np.arange(len(bars))
rects = ax.bar(ind, bars, align='center')
ax.set_xlim([0, len(bars)])
ax.set_xticks(ind)
ax.tick_params(labelsize=14)
labels = ['{}-{}'.format(x*3, (x+1)*3-1) for x in labels]
ax.set_xticklabels(labels)
ax.set_xlabel("Number of months editorial boards have been active for", fontsize=14)
for i in ax.get_xticklabels():
    i.set_rotation(70)
    i.set_size("small")
pylab.vlines(mean_bar, 0, max(bars) + 2, colors='r')
pylab.text(mean_bar, max(bars), ' mean value {:.1f}'.format(mean), fontsize='x-small')
pylab.vlines(rms_minus_bar, 0, max(bars) + 2, colors='gray')
pylab.text(rms_minus_bar, max(bars), ' -rms {:.1f}'.format(rms_minus), fontsize='x-small', color='gray')
pylab.vlines(rms_plus_bar, 0, max(bars) + 2, colors='gray')
pylab.text(rms_plus_bar, max(bars), ' +rms {:.1f}'.format(rms_plus), fontsize='x-small', color='gray')
yint = range(0, max(bars) + 2, 2)
pylab.ylim(0, max(bars)+2)
pylab.xlim(xmin=-1)
pylab.yticks(yint)

pylab.savefig("editorial_months_p_p0.png")
pylab.savefig("editorial_months_p_p0.pdf")
pylab.close()
