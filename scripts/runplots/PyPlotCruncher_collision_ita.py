# -*- coding: utf-8 -*-
"""
#########################################################################################
# PyPlotCruncher.py
# Make useful? paper statistics
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, February 7th, 2013
#########################################################################################
# $Rev: 242050 $
# $LastChangedDate: 2016-04-18 13:35:34 +0200 (Mon, 18 Apr 2016) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import sys
import re
from datetime import datetime
import requests

# import matplotlib for headless terminal
# backend has to be set first
# http://stackoverflow.com/questions/5503601/python-headless-matplotlib-pyplot
import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
import pylab
import numpy as np

from BaseFormatter import BaseFormatter

class PyPlotCruncher(BaseFormatter):

    def __init__(self, run=None):
        BaseFormatter.__init__(self)
        self._monthDict = {}
        self._allDict = {}
        self._allDictCMS = {}

        # THIS MUST BE UPDATED: if not, self._order will make the script crash
        self._order = {"Total" : -99, "STDM" : 0, "FSQ+QCD+EWK+SMP+FWD+GEN" : 0, "EXOT" : 1, "EXO+B2G": 1, "SUSY" : 2, "SUS" : 2, "TOPQ" : 3, "TOP" : 3, "HIGG" : 4, "HIG" : 4, "PERF" : 5, "PRF" : 5, "HION" : 6, "HIN" : 6, "BPHY" : 7, "BPH" : 7, "ARX" : 8, "TRG" : 9, "TCAL" : 10, "SOFT" : 11, "HDBS": 12, "MDET": 13}

        self._exclusionList = ["PERF-2007-01", "LARG-2009-01", "LARG-2009-02", "IDET-2010-01", "MUON-2010-01", "TCAL-2010-01", "PERF-2010-02", "SOFT-2010-01", "SOFT-2017-01"]

        self._allGroups = ['BPHY', 'DAPR', 'LUMI', 'EGAM', 'EXOT', 'FTAG', 'HIGG', 'HION', 'IDET', 'IDTR', 'JETM', 'LARG', 'MCGN', 'MUON', 'MDET', 'PERF', 'SOFT', 'STDM', 'SUSY', 'TAUP', 'TCAL', 'TOPQ', 'TRIG']

        self._zeroGroups = self._allGroups[:]

        self._cmsMonthDict = {}

        self._journalDict = {}

        self._groupsJournalsDict = {}

        self._energyDict = {}

        self._categDict = { "Search" : {"Total" :0}, "Measure" : {"Total" :0}, "Performance" : {"Total" :0}, "Ambiguous" : {"Total" :0} }

        # hardcoded submission date for TOPQ-2012-16
        self._submissionDates = {"TOPQ-2012-16" : "2013/12/11"}

        self._lumiMultiplierDict = { "microbarn**-1" : 1e6,
                                     "nanobarn**-1"  : 1e9,
                                     "picobarn**-1"  : 1e12,
                                     "femtobarn**-1" : 1e15 }

        self._fullLumiDict = { '5.02 TeV' : 0, '2.76 TeV' : 0, '7 TeV (2010)' : 35e12, '7 TeV (2011)' : 4e15, '8 TeV' : 20e15, '13 TeV' : 3e15}

        self._run = run

        if self._run == 2:
            # empty months to be filled from Jan 2015:
            for i in range(2015, datetime.today().year+1):
                for j in range(1,13):
                    if ((i==2015 and j>= 1) or (i>2015)) and not (i == datetime.today().year and j > datetime.today().month):
                        self._monthDict["%d-%0.2d" % (i,j)] = {"Total" : 0}
                        self._cmsMonthDict["%d-%0.2d" % (i,j)] = {"Total" : 0}
                        self._energyDict["%d-%0.2d" % (i,j)] = {}
        else:
            # empty months to be filled from March 2010:
            for i in range(2010, datetime.today().year+1):
                for j in range(1,13):
                    if ((i==2010 and j>= 3) or (i>2010)) and not (i == datetime.today().year and j > datetime.today().month):
                        self._monthDict["%d-%0.2d" % (i,j)] = {"Total" : 0}
                        self._cmsMonthDict["%d-%0.2d" % (i,j)] = {"Total" : 0}
                        self._energyDict["%d-%0.2d" % (i,j)] = {}

        return

    def applyAtlasLogo(self, _fig):

        # add ATLAS logo
        im = Image.open('AtlasTwikiLogo.png')
        height = im.size[1] * 1.4

        # We need a float array between 0-1, rather than
        # a uint8 array between 0-255
        im = np.array(im).astype(np.float) / 255

        # With newer (1.0) versions of matplotlib, you can
        # use the "zorder" kwarg to make the image overlay
        # the plot, rather than hide behind it... (e.g. zorder=10)
        _fig.figimage(im, 0, _fig.bbox.ymax + height, zorder=10)


        return

    def fillDict(self):

        parseString = "%Y/%m/%d"

        # print self._currDct["Ref Code"]

        if self._currDct["Journal Sub"] == '' or \
               self._currDct["Lead Group"] == '' or \
               self._currDct["Ref Code"] in self._exclusionList :
            #print "******* exclusion list detected: *******"
            #print self._currDct["Ref Code"]
            return

        #print ("ref Code = %s" % self._currDct["Ref Code"])

        if self._currDct["Ref Code"] in self._submissionDates.keys():
            self._currDct["Journal Sub"] = self._submissionDates[self._currDct["Ref Code"]]

        d = datetime.strptime(self._currDct["Journal Sub"], parseString)

        key = d.strftime("%Y-%m")

        if not self._monthDict.has_key(key):
            self._monthDict[key] = {}

        grp = self._currDct["Lead Group"]

        # merge all perf groups into one:
        if grp in self._perfGroups:
            grp = "PERF"

        try:
            self._zeroGroups.remove(grp)
        except:
            pass

        if not self._monthDict[key].has_key(grp):
            self._monthDict[key][grp] = 1
        else:
            self._monthDict[key][grp] += 1

        if not self._monthDict[key].has_key("Total"):
            self._monthDict[key]["Total"] = 1
        else:
            self._monthDict[key]["Total"] += 1

        if not self._allDict.has_key(grp):
            self._allDict[grp] = 1
        else:
            self._allDict[grp] += 1


        j = self._currDct["Journal"]
        j = j.strip()
        if j == 'Nucl. Phys. B':
            j = 'NPB'


        #print ("journal = %s" % j)
        if j == "Computing and Software for Big Science":
            j = "CSBS"
        #print ("journal = %s" % j)


        if not self._journalDict.has_key(j):
            self._journalDict[j] = 1
        else:
            self._journalDict[j] += 1

        if not self._groupsJournalsDict.has_key(grp):
            self._groupsJournalsDict[grp] = {}

        if not self._groupsJournalsDict[grp].has_key(j):
            self._groupsJournalsDict[grp][j] = 1
        else:
            self._groupsJournalsDict[grp][j] += 1

        # fill ECM dict

        _ecms = self._currDct["ECM"].split(";")
        _luminosities = self._currDct["Luminosity"].split(";")
        _luminosities_units = self._currDct["Luminosity Unit"].split(";")
        for _ecm, _luminosity, _luminosity_unit in zip(_ecms, _luminosities, _luminosities_units):
            _ecm = _ecm.replace("/NN","").strip()

            if not self._lumiMultiplierDict.has_key(_luminosity_unit):
                continue

            _lumiMult = self._lumiMultiplierDict[_luminosity_unit]

            if _ecm == "7 TeV":
                _ecm = "7 TeV (2011)"
                if _luminosity_unit == "picobarn**-1":
                    _ecm = "7 TeV (2010)"

            if not self._fullLumiDict.has_key(_ecm):
                continue

            if ((float(_luminosity) * _lumiMult) < self._fullLumiDict[_ecm]):
#            if ((float(self.z_currDct["Luminosity"]) * _lumiMult) < self._fullLumiDict[_ecm]):
                continue

            if not self._energyDict[key].has_key(_ecm):
                self._energyDict[key][_ecm] = {"Total" : 0}

            if not self._energyDict[key][_ecm].has_key(grp):
                self._energyDict[key][_ecm][grp]  = 1
            else:
                self._energyDict[key][_ecm][grp] += 1
            self._energyDict[key][_ecm]["Total"] += 1


        _ft = self._currDct["Full Title"].lower()

        if grp in self._perfGroups:
            #print "Performance", self._currDct["Full Title"]
            _categ = "Performance"
        elif "determ" in _ft or "measur" in _ft or "evidence" in _ft or "observ" in _ft or "study" in _ft or "character" in _ft or "extraction" in _ft or "density" in _ft or "distribution" in _ft or "structure" in _ft or "dynamics" in _ft or "dependence" in _ft:
            #print "Measure", self._currDct["Full Title"]
            _categ = "Measure"
        elif "search" in _ft or "hunt" in _ft or "limit" in _ft or "constraint" in _ft:
            #print "Search", self._currDct["Full Title"]
            _categ = "Search"
        elif "performance" in _ft or "resolution" in _ft or "luminosity" in _ft:
            #print "Performance", self._currDct["Full Title"]
            _categ = "Performance"
        elif grp == "EXOT" or grp == "SUSY":
            _categ = "Search"
        elif grp == "GEN":
            _categ = "Measure"
        elif self._currDct["Journal"] == "JINST":
            _categ = "Performance"
        else:
            #print "Other", self._currDct["Full Title"]
            _categ = "Ambiguous"

        if not self._categDict[_categ].has_key(key):
            self._categDict[_categ][key] = 1
        else:
            self._categDict[_categ][key] += 1

        # set zero to all others
        for k,v in self._categDict.iteritems():
            if k != _categ and not v.has_key(key):
                self._categDict[k][key] = 0

        self._categDict[_categ]["Total"] += 1

        return

    def collateTrimesters(self, dct):

        _newDct = {}

        # structure of input dict is month,group
        for k1,v1 in dct.iteritems():
            _year = k1.split("-")[0]
            _month = int(k1.split("-")[1])
            _trim = "%s-%dT" % (_year, (1+((_month-1)/3)))

            for k2,v2 in v1.iteritems():
                if not _newDct.has_key(k2):
                    _newDct[k2]  = {_trim:v2}
                elif not _newDct[k2].has_key(_trim):
                    _newDct[k2].update({_trim:v2})
                else:
                   _newDct[k2][_trim] += v2

                # zero the other trimesters?
                for k3 in _newDct.keys():
                   if k2 != k3 and not _newDct[k3].has_key(_trim):
                        _newDct[k3][_trim] = 0


        # last zeroing check?
        for k1,v1 in _newDct.iteritems():
            for v2 in v1.keys():
                for k2 in _newDct.keys():
                    if k1!=k2 and not _newDct[k2].has_key(v2):
                        _newDct[k2][v2] = 0


        return _newDct

    def getCMSstats(self):

        paperlist = []
        arxivs = []

        ## get papers from CDS
        _lnk = "https://cds.cern.ch/search?jrec=%d&op1=a&op2=a&cc=CMS+Papers&ln=en&as=1&rg=200&m1=a&m3=a&m2=a&so=a"

        r = requests.get(_lnk % 1, verify=False)

        # print ( " requests.get = %s " % r)
        # print ( " _link text = %s" % r.text)

        ptn = [ (re.compile('arXiv:([0-9]{4}.[0-9]+).*(CMS-[A-Z,0-9]{3}-[0-9]+-[0-9]+(?:-[0-9]+)?)'), 1,2),
                (re.compile('(CMS-[A-Z,0-9]{3}-[0-9]+-[0-9]+(?:-[0-9]+)?).*arXiv:([0-9]{4}.[0-9]+)'), 2,1)
                ]

        for p,i1,i2 in ptn:
            for m in p.finditer(r.text):
                if m:
                    arxiv = m.group(i1)
                    docid = m.group(i2)
                    if not arxiv in arxivs:
                        # print arxiv, docid
                        arxivs.append( arxiv )
                        paperlist.append( (arxiv, docid) )
                    else:
                        pass
                        #print ("elment already in array %s " % arxiv)
                else:
                    print "element non added"

        ptn_next = re.compile('records found &nbsp;.+?[0-9]+ - ([0-9]+)<a')

        while ptn_next.search(r.text):
            _jrec = int(ptn_next.findall(r.text)[0])+1
            print _jrec
            print _lnk % _jrec
            r = requests.get(_lnk % _jrec, verify=False)
            #_f=open("bla.html",'w'); _f.write(r.text.encode("UTF8")); _f.close()
            for p,i1,i2 in ptn:
                for m in p.finditer(r.text):
                    if m:
                        arxiv = m.group(i1)
                        docid = m.group(i2)
                        if not arxiv in arxivs:
                            #print arxiv, docid
                            arxivs.append( arxiv )
                            paperlist.append( (arxiv, docid) )

        #print "CMS papers from CDS: %i" % len(paperlist)

        ## get papers from arXiv (last 2 months only)
        #_lnk2 = "http://arxiv.org/find/hep-ex/1/au:+Collaboration_CMS/0/1/0/all/0/1"
        _lnk2 = "http://arxiv.org/find/hep-ex/1/ANDNOT+au:+Collaboration_CMS+au:+for/0/1/0/all/0/1"  # leave out proceedings

        r = requests.get(_lnk2, verify=False)

        _y = datetime.today().year % 100
        _m = datetime.today().month
        ptn2 = re.compile('arXiv:((?:%02d%02d|%02d%02d).[0-9]+)' % (_y,_m-1,_y,_m))

        n_cms_recent_arxiv = 0

        for m in ptn2.finditer(r.text):
            if m:
                arxiv = m.group(1)
                docid = 'CMS-ARX'
                if not arxiv in arxivs:
                    # print "Adding paper from arXiv: %s" % arxiv
                    arxivs.append( arxiv )
                    paperlist.append( (arxiv, docid) )
                    n_cms_recent_arxiv += 1

        #print "CMS additional recent papers from the arxiv: %i" % n_cms_recent_arxiv

        paperlist_filtered = list()

        for i,j in paperlist:
            if 'CFT' in j or 'CMS-MUO-10-001' in j:
                #print 'Skipping paper: %s %s' % (i, j)
                continue

            paperlist_filtered.append( (i, j) )
            #print ("paperlist filtered added = %s" % j)

        paperlist_filtered.sort()

        #print "CMS papers selected: %i" % len(paperlist_filtered)

        for i,j in paperlist_filtered:
            #print "CMS: %s %s" % (i, j)

            grp = j.split('-')[1]

            # merges some of the groups
            if grp in ["FSQ", "QCD", "EWK", "SMP", "FWD", "GEN"]:
#                grp = "FSQ+QCD+EWK+SMP+FWD+GEN"
                grp = "STDM"
            if grp in ["B2G", "EXO"]:
                grp = "EXO+B2G"
            if grp in ["MUO", "JME", "TAU", "TRI", "BTV", "EGM", "TRK"]:
                grp = "PERF"

            if not self._allDictCMS.has_key(grp):
                self._allDictCMS[grp] = 1
            else:
                self._allDictCMS[grp] += 1

            key = datetime.strptime(i.split(".")[0],"%y%m").strftime("%Y-%m")

            if not self._cmsMonthDict.has_key(key):
                self._cmsMonthDict[key] = {grp : 1, "Total" : 0}
            else:
                if not self._cmsMonthDict[key].has_key(grp):
                    self._cmsMonthDict[key][grp] = 1
                else:
                    self._cmsMonthDict[key][grp] += 1

            self._cmsMonthDict[key]["Total"] += 1

        return


    def createMonthlyBar(self, bars, labels, title="", color='b', outfile="foo.png"):

        ind = np.arange(len(bars))  # the x locations for the groups
        width = 1.0 #0.7       # the width of the bars

        _fig = pylab.figure()
        _fig.clf()

        pylab.title(title, fontsize="x-large", weight='bold')

        ax = _fig.add_subplot(111)
        rects = ax.bar(ind, bars, width, color=color)

        ax.set_xlim([0, len(bars)])

#        assert False, '%s \n %s' % (bars, labels)
#        ax.set_xbound(lower='2010-03', upper='2016-03')

        # add some
        #ax.set_ylabel('Scores')
        #ax.set_title('Scores by group and gender')

        new_labels = list()
        for i, l in enumerate(labels):
            if (l.endswith('-01') or l.endswith('-07')):
                new_labels.append(l)
            else:
                new_labels.append('')

        ax.set_xticks(ind)
        ax.set_xticklabels(new_labels)

        for i in ax.get_xticklabels():
            i.set_rotation(70)
            i.set_size("x-small")

        avg = 0

        max_height = 0
        for rect in rects:
            height = rect.get_height()
            avg += height
            ax.text(rect.get_x()+rect.get_width()/2., .2+height, '%d'%int(height), ha='center', va='bottom', fontsize="x-small")
            if height > max_height:
                max_height = height

        ax.set_ylim([0, max_height*1.3])

        avg = 1. * avg / len(rects)

        pylab.axhline(y=avg, color=color, ls='--')
        pylab.text(width/2, .2+avg,"Average: %.2g" % avg, color=color, fontsize=9)

        #pylab.text(-5,ax.get_ylim()[1]/2,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9, rotation=90)
        pylab.text(ax.get_xlim()[1],ax.get_ylim()[1],datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='bottom', horizontalalignment='right', rotation=0)

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()

        return

    def createMonthlyStackedBar(self, bars, labels, legends, title="", outfile="foo.png", loc=2):

        ind = np.arange(len(bars[0]))  # the x locations for the groups
        width = 1.0 #0.7       # the width of the bars

        _fig = pylab.figure()
        _fig.clf()

        pylab.title(title, fontsize="x-large", weight='bold')

        ax = _fig.add_subplot(111)

        _colors = iter(pylab.cm.Paired(np.linspace(0,1,len(legends))))

        _colorMap = {"Search"      : 'r',
                     "Measure"     : 'b',
                     "Performance" : 'y',
                     "Other"       : 'k',
                     "Ambiguous"   : 'g'}

        allRects = []

        allSums = []

        #for i,b in enumerate(bars):
        #       print i, len(b), b

        for i,j in enumerate(bars[0]):
            allSums.append(sum([bars[k][i] for k in range(len(bars))]))

        max_len_bars = 0
        for i,j in enumerate(bars):
            if _colorMap.has_key(legends[i]):
                c = _colorMap[legends[i]]
            else:
                c = _colors.next()

            if i == 0:
                rects = ax.bar(ind, j, width, color=c)
            else:
                _sum = sum([ np.array(bars[k]) for k in range(i)])
                rects = ax.bar(ind, j, width, color=c, bottom=_sum)

            allRects.append(rects)

            if len(bars[k]) > max_len_bars:
                max_len_bars = len(bars[k])

        ax.set_xlim([0, max_len_bars])

        new_labels = list()
        for i, l in enumerate(labels[0]):
            if (l.endswith('-01') or l.endswith('-07')):
                new_labels.append(l)
            else:
                new_labels.append('')

        ax.set_xticks(ind)
        ax.set_xticklabels(new_labels)

        for i in ax.get_xticklabels():
            i.set_rotation(70)
            i.set_size("x-small")

        max_height = 0
        for r,rect in enumerate(rects):
            height = allSums[r]
            ax.text(rect.get_x()+rect.get_width()/2., .2+height, '%d'%int(height), ha='center', va='bottom', fontsize="x-small")
            if height > max_height:
                max_height = height

        ax.set_ylim([0, max_height*1.3])

        pylab.legend([r[0] for r in allRects], legends, loc=loc)
        #pylab.legend([r[0] for r in allRects], legends, fontsize='medium', frameon=False, loc=loc)

        #pylab.text(-5,ax.get_ylim()[1]/2,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9, rotation=90)
        pylab.text(ax.get_xlim()[1],ax.get_ylim()[1],datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='bottom', horizontalalignment='right', rotation=0)

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()

        return

    def createPieChart(self, _fracs, _labels, title="", outfile="foo.png"):

        _fig = pylab.figure(1, figsize=(6,6))
        _fig.clf()

        #ax = pylab.axes([0.1, 0.1, 0.9, 0.9])

        pylab.title(title, fontsize="x-large", weight='bold')

        #colors = get_colours(len(labels))
        #shuffle(colors)

        colors = pylab.cm.Paired(np.linspace(0,1,len(_labels)))

        # Available colormaps:
        #    * Accent
        #    * Dark2
        #    * Paired
        #    * Pastel1
        #    * Pastel2
        #    * Set1
        #    * Set2
        #    * Set3

        labels = [_labels[i] for i,j in enumerate(_fracs) if _fracs[i] > 0]
        colors = [colors[i] for i,j in enumerate(_fracs) if _fracs[i] > 0]
        fracs  = [i for i in _fracs if i>0]

        wedges = pylab.pie(fracs, startangle=90, autopct=lambda p: "%.0f" % (p * sum(fracs) / 100), shadow=True, colors=colors)

        # pylab.text(-1.6,-1.5,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9)
        pylab.text(1.5,1.5,datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='top', horizontalalignment='right', rotation=0)

        if len(fracs) < 5:
            ncol = 2
        elif len(fracs) < 10:
            ncol = 3
        else:
            ncol = 4

        # pylab.legend(wedges[0], labels, ncol=len(fracs)/3+1, fontsize='medium', frameon=False, mode='expand', bbox_to_anchor=(-0.1,0.,1.2,.1))
        pylab.legend(wedges[0], labels, ncol=ncol, fontsize='medium', frameon=False, mode='expand', bbox_to_anchor=(-0.1,0.,1.2,.1))

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()


        """
        _fig = pylab.figure(1, figsize=(6,6))
        _fig.clf()

        #ax = pylab.axes([0.1, 0.1, 0.9, 0.9])

        pylab.title(title, fontsize="x-large", weight='bold')

        #colors = get_colours(len(labels))
        #shuffle(colors)
        colors = pylab.cm.Paired(np.linspace(0,1,len(_labels)))

        # Available colormaps:
        #    * Accent
        #    * Dark2
        #    * Paired
        #    * Pastel1
        #    * Pastel2
        #    * Set1
        #    * Set2
        #    * Set3

        labels = [_labels[i] for i,j in enumerate(_fracs) if _fracs[i] > 0]
        colors = [colors[i] for i,j in enumerate(_fracs) if _fracs[i] > 0]
        fracs  = [i for i in _fracs if i>0]

        # print fracs

        ppps = ["%.0f" % (p * sum(fracs) / 100) for p in fracs]
        wedges = pylab.pie(fracs, autopct='%.0f', shadow=True, colors=colors)
        #wedges = pylab.pie(fracs, autopct=lambda p: "%.0f" % (p * sum(fracs) / 100), shadow=True, colors=colors, startangle=90)
        #wedges = pylab.pie(fracs, startangle=90, autopct=lambda p: "%.0f" % (p * sum(fracs) / 100), shadow=True, colors=colors)

        #pylab.text(-1.6,-1.5,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9)
        pylab.text(1.5,1.5,datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='top', horizontalalignment='right', rotation=0)

        if len(fracs) < 5:
            ncol = 2
        elif len(fracs) < 10:
            ncol = 3
        else:
            ncol = 4

        pylab.legend(wedges[0], labels, ncol=ncol, mode='expand', bbox_to_anchor=(-0.1,0.,1.2,.1))
        #pylab.legend(wedges[0], labels, ncol=len(fracs)/3+1, fontsize='medium', frameon=False, mode='expand', bbox_to_anchor=(-0.1,0.,1.2,.1))

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()

        return

        """

    def createECMPlots(self, grp="Total", title="", outfile="foo.png"):

        _fig = pylab.figure()
        _fig.clf()

        pylab.title(title, fontsize="x-large", weight='bold')

        labels = sorted(self._energyDict.keys())

        ecmSet = set()
        for v in self._energyDict.values():
            for k in v.keys():
                ecmSet.add(k)

        energies = sorted(list(ecmSet))

        colors = iter(pylab.cm.gist_rainbow(np.linspace(0,1,len(energies))))

        ind = np.arange(len(labels))

        # need to sum data for all ecms and all months

        labs = []
        legs = []

        for _e in energies:
            c=colors.next()
            # darken the green
            c[1] *= 0.7
            #print "COLOR", c
            dataE = []
            for i,j in enumerate(labels):
                _cum = 0
                if self._energyDict[j].has_key(_e):
                    if self._energyDict[j][_e].has_key(grp):
                        _cum = self._energyDict[j][_e][grp]

                if i == 0:
                    dataE.append(_cum)
                else:
                    dataE.append(_cum+dataE[i-1])

            if dataE[-1] == 0:
                continue

            ax = _fig.add_subplot(111)
            plot = ax.plot(ind, dataE, '-', color=c, linewidth=4)
            legs.append(pylab.Rectangle((0, 0), 1, 1, fc=c))
            labs.append(_e)

            pylab.text(ind[-1] * 1.01, dataE[-1], "%d" % dataE[-1], color=c, fontsize=8)

        ax.set_xlim(right=ind[-1])

        ax.set_xticks([])

        #ax.set_xticks(ind)
        #ax.set_xticklabels(labels)

        #for ii,i in enumerate(ax.get_xticklabels()):
        for ii,i in enumerate(labels):
            #i.set_rotation(70)
            #i.set_size("x-small")
            # draw year axis
            #if i.get_text()[-2:] == "12":
            if i[-2:] == "12":
                pylab.axvline(x=ind[ii], color="gray", ls='--')
                pylab.text(ind[ii], 0.1,"%s" % (int(i[:4])+1), color="black", fontsize=8)

                # plot points per experiment
                #if cms:
                #    if atlas[ii] > cms [ii]:
                #        # upper left
                #        pylab.text(ind[ii-1], atlas[ii]*1.02,"%d" % atlas[ii], color="blue", fontsize=8)
                #        # lower right
                #        pylab.text(ind[ii], cms[ii]*.95,"%d" % cms[ii], color="red", fontsize=8)
                #    else:
                #        # lower right
                #        pylab.text(ind[ii], atlas[ii]*.95,"%d" % atlas[ii], color="blue", fontsize=8)
                #        # upper left
                #        pylab.text(ind[ii-1], cms[ii]*1.02,"%d" % cms[ii], color="red", fontsize=8)
                #else:
                #    # upper left
                #    pylab.text(ind[ii-1], atlas[ii]*1.02,"%d" % atlas[ii], color="blue", fontsize=8)

        pylab.legend(legs, labs, loc=2)
        #pylab.legend(legs, labs, fontsize='medium', frameon=False, loc=2)

        #pylab.text(-5,ax.get_ylim()[1]/2,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9, rotation=90)
        pylab.text(ax.get_xlim()[1],ax.get_ylim()[1],datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='bottom', horizontalalignment='right', rotation=0)

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()

        return


    def createArmsRace(self, atlas, cms, labels, title="", outfile="foo_ita.png"):

        _fig = pylab.figure()
        _fig.clf()

        pylab.title(title, fontsize="x-large", weight='bold')

        ind = np.arange(len(labels))

        ax = _fig.add_subplot(111)
        plot1 = ax.plot(ind, atlas, 'b-', linewidth=2)
        if cms:
            plot2 = ax.plot(ind, cms,   'r-', linewidth=2)

        ax.set_xlim(right=ind[-1])

        ax.set_xticks([])

        #ax.set_xticks(ind)
        #ax.set_xticklabels(labels)

        #for ii,i in enumerate(ax.get_xticklabels()):
        for ii,i in enumerate(labels):
            #i.set_rotation(70)
            #i.set_size("x-small")
            # draw year axis
            #if i.get_text()[-2:] == "12":
            if i[-2:] == "12":
                pylab.axvline(x=ind[ii], color="gray", ls='--')
                pylab.text(ind[ii], -15.,"%s" % (int(i[:4])+1), color="gray", fontsize=8)

                # plot points per experiment
                if cms:
                    if atlas[ii] > cms [ii]:
                        # upper left
                        pylab.text(ind[ii-1], atlas[ii]*1.02,"%d" % atlas[ii], color="blue", fontsize=8)
                        # lower right
                        pylab.text(ind[ii], cms[ii]*.95,"%d" % cms[ii], color="red", fontsize=8)
                    else:
                        # lower right
                        pylab.text(ind[ii], atlas[ii]*.95,"%d" % atlas[ii], color="blue", fontsize=8)
                        # upper left
                        pylab.text(ind[ii-1], cms[ii]*1.02,"%d" % cms[ii], color="red", fontsize=8)
                else:
                    # upper left
                    pylab.text(ind[ii-1], atlas[ii]*1.02,"%d" % atlas[ii], color="blue", fontsize=8)

        if cms:
            pylab.legend([plot1[0], plot2[0]], ["ATLAS","CMS"], loc=2)
            #pylab.legend([plot1[0], plot2[0]], ["ATLAS","CMS"], fontsize='medium', frameon=False, loc=2)
        else:
            pylab.legend([plot1[0]], ["ATLAS"], loc=2)
            #pylab.legend([plot1[0]], ["ATLAS"], fontsize='medium', frameon=False, loc=2)

        pylab.text(ind[-1] * 1.01, max(atlas),"ATLAS: %d" % max(atlas), color="b", fontsize=8)
        if cms:
            pylab.text(ind[-1] * 1.01, max(cms),"CMS: %d" % max(cms), color="r", fontsize=8)

        # pylab.text(ax.get_xlim(),ax.get_ylim()[1]/2,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=7, rotation=0)
        #pylab.text(-5,ax.get_ylim()[1]/2,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9, rotation=0)
        pylab.text(ax.get_xlim()[1],ax.get_ylim()[1],datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='bottom', horizontalalignment='right', rotation=0)


        # pylab.figtext("2018-01-16")

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()

        return

    def doFormatting(self):


        for r in self._Reader:
            self._currDct = r
            self.fillDict()


        # Monthly chart
        labels = sorted(self._monthDict.keys())

        bars = [self._monthDict[i]["Total"] for i in labels]

        outfile = "months_ita.png"
        if self._run:
            outfile = "months_run%i_ita.png" % self._run
        self.createMonthlyBar(bars, labels, title="ATLAS Papers/Month", outfile=outfile)

        # remove zero entry groups, sort from lowest to highest
        groups = [ i for i in self._allGroups if not i in self._zeroGroups ]

        labels = [i for i in groups if not i in self._zeroGroups]

        labels.sort(key=lambda x: self._allDict[x], reverse=True)

        fracs = [self._allDict[i] for i in labels]

        outfile = "groups_ita.png"
        if self._run:
            outfile = "groups_run%i_ita.png" % self._run
        self.createPieChart(fracs, labels, title="ATLAS - Papers/Lead-group", outfile=outfile)

        """
        labels = [i for i in self._allDictCMS.keys()]

        labels.sort(key=lambda x: self._allDictCMS[x], reverse=True)

        fracs = [self._allDictCMS[i] for i in labels]

        outfile = "groups_CMS_ita.png"
        if self._run:
            outfile = "groups_CMS_run%i_ita.png" % self._run
        self.createPieChart(fracs, labels, title="CMS - Papers/Lead-group (Internal)", outfile=outfile)

        """
        labels1 = sorted(self._journalDict.keys(), key=lambda x: self._journalDict[x], reverse=True)
        #print labels1

        #labels2 = labels1[:]
        #labels2.reverse()

        #labels = []

        #for i in range(len(labels1)):
        #    labels += [ [labels2[i/2], labels1[i/2]][i%2] ]

        labels=labels1

        #print labels

        fracs = [self._journalDict[i] for i in labels]

        #print ("pyplotcruncher_collision: journal - labels = %s \n labels1 %s" % (labels, labels1))

        outfile = "journals_ita.png"
        if self._run:
            outfile = "journals_run%i_ita.png" % self._run
        self.createPieChart(fracs, labels, title="ATLAS - Journals (Internal)", outfile=outfile)

        labels = sorted([i for i in self._categDict.keys() if self._categDict[i]["Total"]], key=lambda x: self._categDict[x]["Total"], reverse=True)

        fracs = [self._categDict[i]["Total"] for i in labels]

        outfile = "type_ita.png"
        if self._run:
            outfile = "type_run%i_ita.png" % self._run
        self.createPieChart(fracs, labels, title="ATLAS - Type of Paper", outfile=outfile)

        # pie charts for individual groups
        for k,v in self._groupsJournalsDict.iteritems():
            labels = labels1 #sorted(v.keys(), key=lambda x: v[x], reverse=True)

            #fracs = [v[i] for i in labels]
            fracs = map(lambda x:v[x] if v.has_key(x) else 0, labels)

            outfile = "journals_%s_ita.png" % k
            if self._run:
                outfile = "journals_%s_run%i_ita.png" % (k, self._run)
            self.createPieChart(fracs, labels, title="%s - Journals (Internal)" % k, outfile=outfile)
        allFracs   = []
        allLabels  = []
        allLegends = []

        #  print self._categDict

        for i in self._categDict.keys(): #["Search","Measure","Performance","Other"]:
            if not self._categDict[i]["Total"]:
                continue

            allLegends.append(i)

            l = sorted([j for j in self._categDict[i].keys() if j != "Total"])

            allLabels.append(l)
            allFracs.append([self._categDict[i][j] for j in l])

        outfile = "months_stacked_ita.png"
        if self._run:
            outfile = "months_stacked_run%i_ita.png" % self._run
        self.createMonthlyStackedBar(allFracs, allLabels, allLegends, title="ATLAS - Type of Paper", outfile=outfile)

        # plot trimesters
        #print (self._monthDict)
        _ATLAStrimDict = self.collateTrimesters(self._monthDict)

        allFracs   = []
        allLabels  = []
        allLegends = []

        #print (_ATLAStrimDict.keys())

        for i in sorted(_ATLAStrimDict.keys(), key=lambda x: self._order[x]):
            if i == "Total":
                continue

            allLegends.append(i)

            l = sorted(_ATLAStrimDict[i].keys())

            allLabels.append(l)
            allFracs.append([_ATLAStrimDict[i][j] for j in l])

        outfile = "trimesters_stacked_ita.png"
        if self._run:
            outfile = "trimesters_stacked_run%i_ita.png" % self._run
        self.createMonthlyStackedBar(allFracs, allLabels, allLegends, title="ATLAS - Type of Paper", outfile=outfile)
        _CMStrimDict = self.collateTrimesters(self._cmsMonthDict)
        #_CMStrimDict = ast.literal_eval(json.dumps(_CMStrimDict))

        allFracs   = []
        allLabels  = []
        allLegends = []

        """
        /////// COMMENTED BECAUSE CAUSED THE SCRIPT TO CRASH; 14th march, 218
        #for i in sorted(_CMStrimDict, key=lambda x: self._order[x]):
        for i in sorted(_CMStrimDict.keys(), key=lambda x: self._order[x]):
            print "for i in sorted"
            if i == "Total":
                continue

            allLegends.append(i)

            l = sorted(_CMStrimDict[i].keys())

            allLabels.append(l)
            allFracs.append([_CMStrimDict[i][j] for j in l])

        print "pre trimesters_cms_stacked"
        outfile = "trimesters_cms_stacked.png"
        if self._run:
            outfile = "trimesters_cms_stacked_run%i.png" % self._run
        print "pre monthlyStackedBar trimesters_cms_stacked"
        self.createMonthlyStackedBar(allFracs, allLabels, allLegends, title="CMS - Type of Paper", outfile=outfile)
        """
        # unique months
        monthsSet = set(self._monthDict.keys() + self._cmsMonthDict.keys())

        labels = sorted(monthsSet)

        dataAtlas = []
        dataCMS   = []

        for i,j in enumerate(labels):

            if self._monthDict.has_key(j):
                _dA = self._monthDict[j]["Total"]
            else:
                _dA = 0

            if self._cmsMonthDict.has_key(j):
                _dC = self._cmsMonthDict[j]["Total"]
            else:
                _dC = 0

            if i == 0:
                dataAtlas.append(_dA)
                dataCMS.append(_dC)
            else:
                dataAtlas.append(_dA+dataAtlas[i-1])
                dataCMS.append(_dC+dataCMS[i-1])

        labels_cms = sorted(self._cmsMonthDict.keys())
        bars = [self._cmsMonthDict[i]["Total"] for i in labels_cms]

        """
        outfile = "months_CMS.png"
        if self._run:
            outfile = "months_CMS_run%i.png" % self._run
        self.createMonthlyBar(bars, labels_cms, title="CMS Papers/Month (Internal)", outfile=outfile, color='r')
        """

        outfile = "arms_race_ita.png"
        if self._run:
            outfile = "arms_race_run%i_ita.png" % self._run
        self.createArmsRace(dataAtlas, dataCMS, labels, title="ATLAS & CMS Submitted Papers (Internal)", outfile=outfile)

        outfile = "atlas_progress_ita.png"
        if self._run:
            outfile = "atlas_progress_run%i_ita.png" % self._run
        self.createArmsRace(dataAtlas, None, labels, title="ATLAS Submitted Papers", outfile=outfile)

        self.createECMPlots()
        for i in ["HION","SUSY","EXOT","HIGG","STDM","TOPQ","PERF","BPHY"]:
            outfile = "ecm-%s_ita.png" % i
            if self._run:
                outfile = "ecm-%s_run%i_ita.png" % (i,self._run)
            #self.createECMPlots(i,u"Full lumi papers \u2014 %s" % i, outfile)
            self.createECMPlots(i,u"Papers per center-of-mass energy \u2014 %s" % i, outfile)

        return



    def processCSV(self):

        self.setReader()

        #self.getCMSstats()

        self.doFormatting()

        return

