#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import traceback
import sys

from PyPlotCruncher_collision import PyPlotCruncher
from ConfNotePlotsCruncher import ConfNotePlotsCruncher
from AtlasRunPlotCruncher import AtlasRunPlotCruncher
from AtlasRun12PlotCruncher_collision import AtlasRun12PlotCruncher
from TimeLineStatCruncher import TimeLineStatCruncher
from TimeLineInternalStatCruncher import TimeLineInternalStatCruncher
from ConfNotePaperPlotsCruncher import ConfNotePaperPlotsCruncher
from CumulativePlotCruncher import CumulativePlotCruncher

##-----------------------------------------------------------------------------
## makes plots for:
## https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PubComDailyPlots

bf = PyPlotCruncher()
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv6649/table6649.csv")
    bf.processCSV()
except:
    print ("****** >> %s" % (traceback.format_exc()))
    print "Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6649/table6649.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6649/table6649.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")

# new one multi collision run/ecm values support

bf = AtlasRun12PlotCruncher()
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv6650/table6650.csv")
    bf.processCSV()
except:
    print "AtlasRun12PlotCruncher: Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6650/table6650.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6650/table6650.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")



bf = ConfNotePlotsCruncher()
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv5505/table5505.csv")
    bf.processCSV()
except:
    print "ConfNotePlotsCruncher: Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv5505/table5505.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv5505/table5505.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")


##-----------------------------------------------------------------------------
## make Run 1 only DailyPLots
## https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComDailyPlotsRun1




bf = AtlasRunPlotCruncher(run=1)
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv6528/table6528.csv")
    bf.processCSV()
except:
    print "AtlasRunPlotCruncher: Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6528/table6528.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6528/table6528.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")


bf = ConfNotePlotsCruncher(run=1)
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv6530/table6530.csv")
    bf.processCSV()
except:
    print "ConfNotePlotsCruncher: Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6530/table6530.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6530/table6530.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")

##-----------------------------------------------------------------------------
## make Run 2 only DailyPLots
## https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComDailyPlotsRun2

bf = AtlasRunPlotCruncher(run=2)
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv6529/table6529.csv")
    bf.processCSV()
except:
    print "AtlasRunPlotCruncher: Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6529/table6529.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6529/table6529.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")

bf = ConfNotePlotsCruncher(run=2)
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv6531/table6531.csv")
    bf.processCSV()
except:
    print "ConfNotePlotsCruncher: Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6531/table6531.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6531/table6531.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")

##-----------------------------------------------------------------------------
## makes plots for:
## https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComInternalPlots





bf = TimeLineStatCruncher()
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv5449/table5449.csv")
    bf.processCSV()
except:
    print "TimeLineStatCruncher: Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv5449/table5449.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv5449/table5449.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")





bf = TimeLineInternalStatCruncher()
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv6675/table6675.csv")
    bf.processCSV()
except:
    print "TimeLineInternalStatCruncher: Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6675/table6675.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6675/table6675.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")

bf = ConfNotePaperPlotsCruncher()
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv5457/table5457.csv")
    bf.setCNInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv5505/table5505.csv")
    bf.processCSV()
except:
    print "ConfNotePaperPlotsCruncher: Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv5457/table5457.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv5457/table5457.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")

bf = CumulativePlotCruncher()
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv5449/table5449.csv")
    bf.processCSV()
except:
    print "CumulativePlotCruncher: Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv5449/table5449.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv5449/table5449.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")
 
##----------------------------------------------------------------------------

