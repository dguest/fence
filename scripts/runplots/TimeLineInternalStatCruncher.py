# -*- coding: utf-8 -*-
"""
#########################################################################################
# TimeLineInternalStatCruncher.py
# Make useful? timeline statistics
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, April 12th, 2013
#########################################################################################
# $Rev: 238997 $
# $LastChangedDate: 2015-04-28 18:44:04 +0200 (Tue, 28 Apr 2015) $
# $LastChangedBy: atlaspo $
#########################################################################################
# used on pages as https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PubComInternalPlots
"""

from math import fabs
from datetime import datetime
import pkg_resources
pkg_resources.require("Matplotlib==1.2.0")

import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
import pylab
import numpy as np

from TimeLineStatCruncher import TimeLineStatCruncher

class TimeLineInternalStatCruncher(TimeLineStatCruncher):

    def __init__(self):
        TimeLineStatCruncher.__init__(self)

        self._lifetimeDict = {}#"Total" : []}
        self._cre2EdBoard = {}
        self._eb2D1 = {}
        self._d12D2 = {}
        self._d12pam = {}
        self._pam2ebd1so = {}
        self._ebd2so2d2 = {}
        self._d22EdBoard = {}
        self._d22PubComm = {}
        self._EB2PubComm = {}
        self._eb2SP = {}
        self._d22EbSo = {}
        self._d22SP = {}
        self._sp2Sub = {}
        self._d22PRtoSP = {}

        self._lifetimeDict_Monthly = {}#"Total" : []}
        self._cre2EdBoard_Monthly = {}
        self._eb2D1_Monthly = {}
        self._d12D2_Monthly = {}
        self._d12pam_Monthly = {}
        self._pam2ebd1so_Monthly = {}
        self._ebd2so2d2_Monthly = {}
        self._d22EdBoard_Monthly = {}
        self._d22PubComm_Monthly = {}
        self._EB2PubComm_Monthly = {}
        self._eb2SP_Monthly = {}
        self._d22EbSo_Monthly = {}
        self._d22SP_Monthly = {}
        self._sp2Sub_Monthly = {}
        self._d22PRtoSP_Monthly = {}

        self._cre2P1ReviewSODict = {}#"Total" : []}
        self._p1RSO2PubCommSODict = {}#"Total" : []}
        self._p1PCSO2Phase2SODict = {}#"Total" : []}

        

        

        self._totalDict = {}

        ## -  > 450 days for "EdBoard formation to Submission"
        ## -  > 21 days for "EdBoard formation"
        ## -  > 320 days for "EdBoard formation to draft-1"
        ## -  > 3 months for "draft-1 to draft-2"
        ## -  > 2 months for "draft-2 to Public Reading"
        ## -  > 21 days for "Public Reading to EdBoard sign-off"
        ## -  > 25 days for "EdBoard sign-off to PubCom sign-off"
        ## -  > 30 days for "Public Reading to PubCom sign-off"
        ## -  > 21 days for "PubCom sign-off to Spokesperson sign-off"
        ## -  > 10 days for "spokesperson sign-off to submission"
        self._outliers = { "EdBoard Formation to Submission"           : {"threshold" : 451, "list" : []},
                           "Paper Creation to Ed. Board formation"     : {"threshold" : 17,  "list" : []},
                           "EdBoard Formation to Draft 1 circ."        : {"threshold" : 332, "list" : []},
                           "Draft 1 circ. to Draft 2 circ."            : {"threshold" : 52,  "list" : []},
                           "Draft 1 circ. to Paper Approval"           : {"threshold" : 15,  "list" : []},
                           "Paper Approval to Ed. Board Draft2 sign-off"  : {"threshold" : 35,  "list" : []},
                           "Ed. Board Draft2 sign-off to Draft 2 circ" : {"threshold" : 5,  "list" : []},
                           "Closure to  Rev. Draft2 Ed. Board sign-off": {"threshold" : 17,  "list" : []},
                           "Paper Approval to PubComm Member Sign Off" : {"threshold" : 21,  "list" : []},
                           "Rev. Draft2 Ed. Board sign-off to PubCom Chair" : {"threshold" : 17,  "list" : []},
                           "Rev. Draft2 Ed.Board sign-off to Spokesperson Del." : {"threshold" : 29,  "list" : []},
                           "Draft2 circ. to rev. Draft2 Ed.Board sign-off" : {"threshold" : 28, "list" : []},
                           "PubComm Member Sign Off to Spokesperson Sign Off" : {"threshold" : 400, "list" : []},
                           "Spokesperson Del. to submission"           : {"threshold" : 4, "list" : []},
                           "Closure to Spokesperson Sign Off"          : {"threshold" : 46, "list" : []}
                           }
                           
                           
        return  

    def fillDict(self, dct, lab1, lab2, key=None, outlier=None):

        if lab1 == "EdBoard Formed" and lab2 == "Phase 1 Released On":
            debug = False
        else:
            debug = False

        if self._currDct["Ref Code"] in self._exclusionList:
                return

        if key:
           _grp = key
        else:
           _grp = self._currDct["Ref Code"][:4]
        
        # merge performance groups
        if _grp in self._perfGroups:
                _grp = "PERF"

        # fill submission to 1st report
        if self._currDct[lab1] == "" or self._currDct[lab2] == "":
            return

        # get dates
        _d1 = datetime.strptime(self._currDct[lab1], "%Y/%m/%d")
        _d2 = datetime.strptime(self._currDct[lab2], "%Y/%m/%d")

        _ykey = _d1.year
        if _ykey < 2011:
            return

        if not dct.has_key(_grp):
            dct[_grp] = []

        if (_d2-_d1).days < 0:
                return

        if outlier:
                if (_d2-_d1).days > self._outliers[outlier]["threshold"]:
                        self._outliers[outlier]["list"].append((self._currDct["Ref Code"], (_d2-_d1).days))

        dct[_grp].append((_d2-_d1).days)

        if dct.has_key("Total"):
                dct["Total"].append((_d2-_d1).days)

        if debug:
            print self._currDct[lab1] + " " + self._currDct[lab2],
            print (_d2-_d1).days,
            print " " + self._currDct["Ref Code"]

        return

    def fillMonthlyDict(self, dct, lab1, lab2, outlier=None):

        if lab1 == "EdBoard Formed" and lab2 == "Phase 1 Released On":
            debug = False
        else:
            debug = False

        if self._currDct["Ref Code"] in self._exclusionList:
            return

        # fill submission to 1st report
        if self._currDct[lab1] == "" or self._currDct[lab2] == "":
            return

        # get dates
        _d1 = datetime.strptime(self._currDct[lab1], "%Y/%m/%d")
        _d2 = datetime.strptime(self._currDct[lab2], "%Y/%m/%d")

        _ykey = _d1.year
        #_mkey = _d1.month

        # use trimesters
        _mkey = (_d1.month - 1) / 3

        # start from 2011

        if _ykey < 2011:
            return

        if not dct.has_key(_ykey):
            dct[_ykey] = {}
            for i in range(0,4): dct[_ykey][i]=[]

        if not dct.has_key(datetime.today().year):
            dct[datetime.today().year] = {}
            for i in range(0,4): dct[datetime.today().year][i]=[]

        if (_d2-_d1).days < 0:
                return

        if outlier:
                if (_d2-_d1).days > self._outliers[outlier]["threshold"]:
                        self._outliers[outlier]["list"].append((self._currDct["Ref Code"], (_d2-_d1).days))


        dct[_ykey][_mkey].append((_d2-_d1).days)

        #if outlier == "EdBoard formation":
        #    if self._currDct[lab2][0:4] == "2018" or self._currDct[lab1][0:4] == "2018":

        if dct.has_key("Total"):
                dct["Total"].append((_d2-_d1).days)

        if debug:
            print self._currDct[lab1] + " " + self._currDct[lab2],
            print (_d2-_d1).days,
            print " " + self._currDct["Ref Code"]

        return

    def fillDictionaries(self):

        #self.fillDict(self._cre2P1ReviewSODict, "Creation Date", "P 1 Reviewers Sign Off")
        #self.fillDict(self._p1RSO2PubCommSODict , "P 1 Reviewers Sign Off", "P 1 PUBCOMM Sign Off")
        #self.fillDict(self._p1PCSO2Phase2SODict  , "P 1 PUBCOMM Sign Off", "P 2 LPG APPROVAL")
        self.fillDict(self._lifetimeDict  , "EdBoard Formed", "Journal Sub", outlier="EdBoard Formation to Submission")
        self.fillDict(self._cre2EdBoard, "Creation Date", "EdBoard Formed", outlier="Paper Creation to Ed. Board formation")
        self.fillDict(self._eb2D1, "EdBoard Formed", "Phase 1 Released On", outlier="EdBoard Formation to Draft 1 circ.")
        self.fillDict(self._d12D2, "Phase 1 Released On", "Draft 2 Release", outlier="Draft 1 circ. to Draft 2 circ.")
        self.fillDict(self._d12pam, "Phase 1 Released On", "Paper Approval Meeting", outlier="Draft 1 circ. to Paper Approval")
        self.fillDict(self._pam2ebd1so, "Paper Approval Meeting", "Editorial Board Draft 2 Sign-off", outlier="Paper Approval to Ed. Board Draft2 sign-off")
        self.fillDict(self._ebd2so2d2, "Editorial Board Draft 2 Sign-off", "Draft 2 Release", outlier="Ed. Board Draft2 sign-off to Draft 2 circ")
        self.fillDict(self._d22EdBoard, "Draft 2 Public Reading Date", "Revised Draft 2 EdBoard Sign Off", outlier="Closure to  Rev. Draft2 Ed. Board sign-off")
        self.fillDict(self._d22PubComm, "Draft 2 Public Reading Date", "Revised Draft 2 PubComm Sign Off", outlier="Paper Approval to PubComm Member Sign Off")
        self.fillDict(self._EB2PubComm, "Revised Draft 2 EdBoard Sign Off", "Pubcomm Chair Sign Off", outlier="Rev. Draft2 Ed. Board sign-off to PubCom Chair")
        self.fillDict(self._eb2SP, "Revised Draft 2 EdBoard Sign Off", "Revised Draft 2 SP Sign Off", outlier="Rev. Draft2 Ed.Board sign-off to Spokesperson Del.")
        self.fillDict(self._d22EbSo, "Draft 2 Release", "Revised Draft 2 EdBoard Sign Off", outlier="Draft2 circ. to rev. Draft2 Ed.Board sign-off")
        self.fillDict(self._d22SP, "Revised Draft 2 PubComm Sign Off", "Revised Draft 2 SP Sign Off", outlier="PubComm Member Sign Off to Spokesperson Sign Off")
        self.fillDict(self._sp2Sub, "Revised Draft 2 SP Sign Off", "Journal Sub", outlier="Spokesperson Del. to submission") 
        self.fillDict(self._d22PRtoSP, "Draft 2 Public Reading Date", "Revised Draft 2 SP Sign Off", outlier="Closure to Spokesperson Sign Off")

        self.fillMonthlyDict(self._lifetimeDict_Monthly, "EdBoard Formed", "Journal Sub", outlier="EdBoard Formation to Submission")
        self.fillMonthlyDict(self._cre2EdBoard_Monthly, "Creation Date", "EdBoard Formed", outlier="Paper Creation to Ed. Board formation")
        self.fillMonthlyDict(self._eb2D1_Monthly, "EdBoard Formed", "Phase 1 Released On", outlier="EdBoard Formation to Draft 1 circ.")
        self.fillMonthlyDict(self._d12D2_Monthly, "Phase 1 Released On", "Draft 2 Release", outlier="Draft 1 circ. to Draft 2 circ.")
        self.fillMonthlyDict(self._d12pam_Monthly, "Phase 1 Released On", "Paper Approval Meeting", outlier="Draft 1 circ. to Paper Approval")
        self.fillMonthlyDict(self._pam2ebd1so_Monthly, "Paper Approval Meeting", "Editorial Board Draft 2 Sign-off", outlier="Paper Approval to Ed. Board Draft2 sign-off")
        self.fillMonthlyDict(self._ebd2so2d2_Monthly, "Editorial Board Draft 2 Sign-off", "Draft 2 Release", outlier="Ed. Board Draft2 sign-off to Draft 2 circ")
        self.fillMonthlyDict(self._d22EdBoard_Monthly, "Draft 2 Public Reading Date", "Revised Draft 2 EdBoard Sign Off", outlier="Closure to  Rev. Draft2 Ed. Board sign-off")
        self.fillMonthlyDict(self._d22PubComm_Monthly, "Draft 2 Public Reading Date", "Revised Draft 2 PubComm Sign Off", outlier="Paper Approval to PubComm Member Sign Off")
        self.fillMonthlyDict(self._EB2PubComm_Monthly, "Revised Draft 2 EdBoard Sign Off", "Pubcomm Chair Sign Off", outlier="Rev. Draft2 Ed. Board sign-off to PubCom Chair")
        self.fillMonthlyDict(self._eb2SP_Monthly, "Revised Draft 2 EdBoard Sign Off", "Revised Draft 2 SP Sign Off", outlier="Rev. Draft2 Ed.Board sign-off to Spokesperson Del.")
        self.fillMonthlyDict(self._d22EbSo_Monthly, "Draft 2 Release", "Revised Draft 2 EdBoard Sign Off", outlier="Draft2 circ. to rev. Draft2 Ed.Board sign-off")
        self.fillMonthlyDict(self._d22SP_Monthly, "Revised Draft 2 PubComm Sign Off", "Revised Draft 2 SP Sign Off", outlier="PubComm Member Sign Off to Spokesperson Sign Off")
        self.fillMonthlyDict(self._sp2Sub_Monthly, "Revised Draft 2 SP Sign Off", "Journal Sub", outlier="Spokesperson Del. to submission")
        self.fillMonthlyDict(self._d22PRtoSP_Monthly, "Draft 2 Public Reading Date", "Revised Draft 2 SP Sign Off", outlier="Closure to Spokesperson Sign Off")

        #self.fillDict(self._totalDict, "Creation Date", "P 1 Reviewers Sign Off", key="c2r")    
        #self.fillDict(self._totalDict, "P 1 Reviewers Sign Off", "P 1 PUBCOMM Sign Off", key="r2pc")
        #self.fillDict(self._totalDict, "P 1 PUBCOMM Sign Off", "P 2 LPG APPROVAL", key="p1-p2")
        #self.fillDict(self._totalDict, "Creation Date", "P 2 LPG APPROVAL", key="life")
        
        return

    def drawMediansHist(self,dct,title,nm):

        #if title == "EdBoard Formation":

        _fig = pylab.figure()
        _fig.clf()

        ax = _fig.add_subplot(111)
        ax.set_title(title, fontsize="x-large", weight='bold')
        #ax.get_xaxis().set_tick_params(top=False)  

        ax.set_xlabel("Trimester")

        ax.set_ylabel("Median")

        _years = datetime.today().year - 2011 + 1
        _w = 1./4 #1./ _years

        colors = iter(pylab.cm.Accent(np.linspace(0,1,_years)))

        legs = []
        labs = []
        #mlabels = ["January","February","March","April","May","June","July","August","September","October","November","December"]

        mlabels = ["1T","2T","3T","4T"]
        x = []
        y = []
        xerr = []
        yerr = []

        c=colors.next()

        # used to the total median for the horizontal line
        _tot = []

        for i in range(_years):
            for j in range(0,4):     
                if j == 0:               
                    labs.append("%d" % (2011+i))
                else:
                    labs.append("")

                index_i = 2011 + i
                if not index_i in dct:
                    continue
                if not j in dct[index_i]:
                    continue

                _med = np.median(dct[2011+i][j])
                _tot += dct[2011+i][j]

                _mad = np.median([fabs(k-_med) for k in dct[2011+i][j]])

                x.append(i+(j*_w))
                y.append(_med)

                xerr.append(_w/2)
                yerr.append(_mad)

            pylab.axvline(x=i-(_w/2), ls='--')
            #pylab.axvline(x=i-(_w/2), color="lightgray", ls='--')

        #ax.errorbar(x, y, yerr, xerr, color=c, fmt='o', mew=0, elinewidth=2)
        ax.errorbar(x, y, yerr, xerr, color=('#0c4412'), fmt='o', mew=0, elinewidth=2)
            #ax.scatter(x, y, color=c.next(), marker="+")
        ax.axhline(y=np.median(_tot), linestyle='dashed', color='red')

        #ax.set_xlim(1-(_w/2), 13-(_w/2))

        ax.set_xticks([i*_w for i in range(len(labs))])

        ax.set_xticklabels(labs)

        for i in ax.get_xticklabels():
            #i.set_rotation(30)
            i.set_size("small")



        pylab.savefig("Medians_%s.png" % nm)
        pylab.savefig("Medians_%s.pdf" % nm)

        pylab.close()

        return

    def drawScatterHist(self,dct,title,nm):

        _fig = pylab.figure()
        _fig.clf()

        ax = _fig.add_subplot(111)
        ax.set_title(title, fontsize="x-large", weight='bold')
        #ax.get_xaxis().set_tick_params(top=False)

        ax.set_xlabel("Days")

        x    = []
        y    = []
        xerr = []
        legs = []
        labs = []

        i = 0 

        _gv = sorted([(key,inp) for key,inp in dct.iteritems() if len(inp) > 0], key=lambda x:x[0])

        colors = iter(pylab.cm.Paired(np.linspace(0,1,len(_gv))))

        _tot = []

        texts = []

        for key,inp in _gv:
            _tot += inp

            hist, bins = np.histogram(inp, 20) #, (0,100))
            center = (bins[:-1]+bins[1:])/2

            i += 1

            #legs.append(key)

            med = np.median(inp)
            mad = np.median([fabs(j-med) for j in inp])

            #y.append(i+1)
            #x.append(med)
            #xerr.append(mad)

            c = colors.next()

            errb = {'zorder'  : -100,
                    'alpha'   :  0.1,
                    'visible' :False,}

            #pylab.scatter(x=inp, y=[i+1]*len(inp), c=colors.next(), label=key, marker="o")

            ax.errorbar( med, -i-1, xerr=mad, c=c, mew=0, label=key, elinewidth=4, marker='+', **errb)
            ax.scatter(x=center, y=[-i-1]*len(center), s=[h*20. for h in hist], c=c, marker="o")

            # draw vert lines
            ymax = 1.-((i*1.)/(len(_gv)+1))
            ymin = 1.-((i+0.4)/(len(_gv)+1))
            
            ax.axvline(med, ymin, ymax, color=c, ls='--', lw=2)
            ax.axvline(med-mad, ymin, ymax,  color=c, ls='-.', lw=2)
            ax.axvline(med+mad, ymin, ymax,  color=c, ls='-.', lw=2)
            ax.axvspan(med-mad, med+mad, ymin, ymax,  color=c, alpha=0.5)
            ax.text(med, -i-1.6,u"%s median: %g\u00B1%g" %  (key,med,mad), color="black", fontsize=9)

            texts.append(ax.text(-5, -i-1, "%d" % hist.sum(), color="black", fontsize=9))

            legs.append(pylab.Rectangle((0, 0), 1, 1, fc=c))
            labs.append(key)


        med = np.median(_tot)
        mad = np.median([fabs(j-med) for j in _tot])

        #ax.set_title(u"%s (%g\u00B1%g days)" % (title,med,mad), fontsize="x-large", weight='bold')     
        #ax.axvline(med, 0, 1.-(1/(len(_gv)+1.)), color='gray', ls='--', lw=2, zorder=-100)
        #ax.axvline(med-mad, 0, 1.-(1/(len(_gv)+1.)),  color='gray', ls='-.', lw=2, zorder=-100)
        #ax.axvline(med+mad, 0, 1.-(1/(len(_gv)+1.)),  color='gray', ls='-.', lw=2, zorder=-100)
        #ax.axvspan(med-mad, med+mad, 0, 1.-(1/(len(_gv)+1.)),  color='gray', alpha=0.1, zorder=-100)

            
        ax.get_yaxis().set_ticks([])
        ax.set_xlim(0, ax.get_xlim()[1])

        pylab.legend(legs, labs, loc="upper right")

        # place text in a smart way
        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()
        w = (xmax - xmin) / 500.
        h = (ymax - ymin) / 300.
        
        pylab.text(xmin-(40*w),-len(_gv)+2,datetime.today().strftime("generated on %d-%b-%Y, %H:%M"), color="#dddddd", fontsize=9, rotation=90)
        pylab.text(xmin+(5*w),ymax-0.4,u"FOR INTERNAL USE ONLY    Overall median: %g\u00B1%g" %  (med,mad), fontsize=12)

        for i in texts:
                i.set_x(xmin-(20*w))

        #pylab.text(med-mad, ymin-0.6, u"Overall median: %g\u00B1%g" % (med,mad), color="black", fontsize=9)

        pylab.savefig("TimeLine_%s.png" % nm)
        pylab.savefig("TimeLine_%s.pdf" % nm)

        pylab.close()

        return

    def doFormatting(self):

        skipFirst = True # the true headers

        for r in self._Reader:
            if skipFirst:
                skipFirst = False
                continue
            
            self._currDct = r
            self.fillDictionaries()

        #colors = pylab.cm.Paired(np.linspace(0,1,len(self._sub2RefDict.keys())))

        #for i,(key,inp) in enumerate(self._sub2RefDict.iteritems()):
        #    if len(inp) < 5:
        #        continue
        #    self.drawHist(key,colors[i])

        #self.drawScatterHist(self._cre2P1ReviewSODict,"Creation to Phase 1 Review","Internal_P1_Review")
        #self.drawScatterHist(self._p1RSO2PubCommSODict ,"Phase 1 Review to SignOff Phase 1","Internal_P1_SignOff")
        #self.drawScatterHist(self._p1PCSO2Phase2SODict ,"SignOff Phase 1 to SignOff Phase 2","Internal_P2_SignOff")
        self.drawScatterHist(self._lifetimeDict ,"EdBoard Formation to Submission","Internal_Lifetime")
        self.drawScatterHist(self._cre2EdBoard, "Paper Creation to Ed. Board formation", "EdBoard")
        self.drawScatterHist(self._eb2D1, "EdBoard Formation to Draft 1 circ.", "Draft_1")
        self.drawScatterHist(self._d12D2, "Draft 1 circ. to Draft 2 circ.", "Draft_2")
        self.drawScatterHist(self._d12pam, "Draft 1 circ. to Paper Approval", "D12PAM")
        self.drawScatterHist(self._pam2ebd1so, "Paper Approval to Ed. Board Draft2 sign-off", "PAM2EBD1SO")
        self.drawScatterHist(self._ebd2so2d2, "Ed. Board Draft2 sign-off to Draft 2 circ", "EBD2SO2D2")
        self.drawScatterHist(self._d22EdBoard, "Closure to  Rev. Draft2 Ed. Board sign-off ", "D2EBSO")
        self.drawScatterHist(self._d22PubComm, "Paper Approval to PubComm Member Sign Off", "PUB2PCSO")
        self.drawScatterHist(self._EB2PubComm, "Rev. Draft2 Ed. Board sign-off to PubCom Chair", "EB2PCSO")
        self.drawScatterHist(self._eb2SP, "Rev. Draft2 Ed.Board sign-off to Spokesperson Del.", "EB2SP")
        self.drawScatterHist(self._d22EbSo, "Draft2 circ. to rev. Draft2 Ed.Board sign-off", "D22EBSO") 
        self.drawScatterHist(self._d22SP, "PubComm Member Sign Off to Spokesperson Sign Off", "PC2SPSO")
        self.drawScatterHist(self._sp2Sub, "Spokesperson Del. to submission", "SPSO2SUB") 
        self.drawScatterHist(self._d22PRtoSP, "Closure to Spokesperson Sign Off", "PUB2PSO")

        self.drawMediansHist(self._lifetimeDict_Monthly ,"EdBoard Formation to Submission","EB2SUB")
        self.drawMediansHist(self._cre2EdBoard_Monthly, "Paper Creation to Ed. Board formation", "EdBoard")
        self.drawMediansHist(self._eb2D1_Monthly, "EdBoard Formation to Draft 1 circ.", "Draft_1")
        self.drawMediansHist(self._d12D2_Monthly, "Draft 1 circ. to Draft 2 circ.", "Draft_2")
        self.drawMediansHist(self._d12pam_Monthly, "Draft 1 circ. to Paper Approval", "D12PAM")
        self.drawMediansHist(self._pam2ebd1so_Monthly, "Paper Approval to Ed. Board Draft2 sign-off", "PAM2EBD1SO")
        self.drawMediansHist(self._ebd2so2d2_Monthly, "Ed. Board Draft2 sign-off to Draft 2 circ", "EBD2SO2D2")
        self.drawMediansHist(self._d22EdBoard_Monthly, "Closure to  Rev. Draft2 Ed. Board sign-off ", "D2EBSO")
        self.drawMediansHist(self._d22PubComm_Monthly, "Paper Approval to PubComm Member Sign Off", "PUB2PCSO")
        self.drawMediansHist(self._EB2PubComm_Monthly, "Rev. Draft2 Ed. Board sign-off to PubCom Chair", "EB2PCSO")
        self.drawMediansHist(self._eb2SP_Monthly, "Rev. Draft2 Ed.Board sign-off to Spokesperson Del.", "EB2SP")
        self.drawMediansHist(self._d22EbSo_Monthly, "Draft2 circ. to rev. Draft2 Ed.Board sign-off", "D22EBSO") 
        self.drawMediansHist(self._d22SP_Monthly, "PubComm Member Sign Off to Spokesperson Sign Off", "PC2SPSO")
        self.drawMediansHist(self._sp2Sub_Monthly, "Spokesperson Del. to submission", "SPSO2SUB") 
        self.drawMediansHist(self._d22PRtoSP_Monthly, "Closure to Spokesperson Sign Off", "PUB2PSO")


        #self.drawScatterHist(self._totalDict,"Creation to Phase 1 Review (Total)","Internal_P1_Review_Total")

        #self.drawScatterHist(self._plbDict,"Submission to Referee Report (PLB)","PLB")

        return
            
    def processCSV(self):

        self.setReader()

        self.doFormatting()

        o_f = open("timeline_outliers.txt","w")
        
        for k in ["EdBoard Formation to Submission"           ,
                  "Paper Creation to Ed. Board formation"     ,
                  "EdBoard Formation to Draft 1 circ."        ,
                  "Draft 1 circ. to Draft 2 circ."            ,
                  #"draft-2 to Public Reading"                 ,
                  "Ed. Board Draft2 sign-off to Draft 2 circ" ,
                  "Closure to  Rev. Draft2 Ed. Board sign-off",
                  "Paper Approval to PubComm Member Sign Off" ,
                  "Rev. Draft2 Ed. Board sign-off to PubCom Chair",
                  "Rev. Draft2 Ed.Board sign-off to Spokesperson Del.",
                  "Draft2 circ. to rev. Draft2 Ed.Board sign-off",
                  "Spokesperson Del. to submission",
                  "Closure to Spokesperson Sign Off"
                  ]:

                o_f.write("%s outliers (threshold=%d days)\n" % (k, self._outliers[k]["threshold"]))
                for i,j in sorted(self._outliers[k]["list"], key=lambda x:x[0]):
                        o_f.write("%d\t%s\n" % (j,i))

                o_f.write("\n\n")

        o_f.close()

        return
    
