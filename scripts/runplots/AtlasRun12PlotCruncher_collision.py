# -*- coding: utf-8 -*-
"""
#########################################################################################
# AtlasRun12PlotCruncher.py
# Make useful? paper statistics
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, February 7th, 2013
#########################################################################################
# $Rev: 242732 $
# $LastChangedDate: 2017-06-14 16:33:20 +0200 (Wed, 14 Jun 2017) $
# $LastChangedBy: atlaspo / Maurizio Colautti $
#########################################################################################
"""

from datetime import datetime
import pkg_resources
pkg_resources.require("Matplotlib==1.2.0")

# import matplotlib for headless terminal
# backend has to be set first
# http://stackoverflow.com/questions/5503601/python-headless-matplotlib-pyplot
import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
import pylab
import numpy as np

from BaseFormatter import BaseFormatter

class AtlasRun12PlotCruncher(BaseFormatter):

    def __init__(self):
        BaseFormatter.__init__(self)
        self._monthDict = {}
        self._allDict = {}

        self._order = {"Total" : -99, "STDM" : 0, "FSQ+QCD+EWK+SMP+FWD+GEN" : 0, "EXOT" : 1, "EXO+B2G": 1, "SUSY" : 2, "SUS" : 2, "TOPQ" : 3, "TOP" : 3, "HIGG" : 4, "HIG" : 4, "PERF" : 5, "HION" : 6, "HIN" : 6, "BPHY" : 7, "BPH" : 7, "ARX" : 8}

        self._exclusionList = ["PERF-2007-01", "LARG-2009-01", "LARG-2009-02", "IDET-2010-01", "MUON-2010-01", "TCAL-2010-01", "PERF-2010-02", "SOFT-2010-01", "SOFT-2017-01"]

        self._allGroups = ['BPHY', 'DAPR', 'LUMI', 'EGAM', 'EXOT', 'FTAG', 'HIGG', 'HION', 'IDET', 'IDTR', 'JETM', 'LARG', 'MCGN', 'MUON', 'MDET', 'PERF', 'SOFT', 'STDM', 'SUSY', 'TAUP', 'TCAL', 'TOPQ', 'TRIG']

        self._exclusionList.extend(["GENR-2018-01", "SOFT-2017-01", "LARG-2019-01", "PIXE-2019-02", "SCTD-2019-01"])

        self._journalDict = {}

        self._groupsJournalsDict = {}

        self._energyDict = {}

        self._categDict = { "Search" : {"Total" :0}, "Measure" : {"Total" :0}, "Performance" : {"Total" :0}, "Ambiguous" : {"Total" :0} }

        # hardcoded submission date for TOPQ-2012-16 
        self._submissionDates = {"TOPQ-2012-16" : "2013/12/11"}

        self._lumiMultiplierDict = { "microbarn**-1" : 1e6,
                                     "nanobarn**-1"  : 1e9,
                                     "picobarn**-1"  : 1e12,
                                     "femtobarn**-1" : 1e15 }

        self._fullLumiDict = { '5.02 TeV' : 0, '2.76 TeV' : 0, '7 TeV (2010)' : 35e12, '7 TeV (2011)' : 4e15, '8 TeV' : 20e15, '13 TeV' : 3.2e15}

        # empty months to be filled from March 2010:
        for i in range(2010, datetime.today().year+1):
            for j in range(1, 13):
                if ((i==2010 and j>= 3) or (i>2010)) and not (i == datetime.today().year and j > datetime.today().month):
                    self._monthDict["%d-%0.2d" % (i, j)] = {"Total" : 0}
                    self._energyDict["%d-%0.2d" % (i, j)] = {}

        return


    def applyAtlasLogo(self, _fig):

        # add ATLAS logo
        im = Image.open('AtlasTwikiLogo.png')
        height = im.size[1] * 1.4

        # We need a float array between 0-1, rather than
        # a uint8 array between 0-255
        im = np.array(im).astype(np.float) / 255

        # With newer (1.0) versions of matplotlib, you can
        # use the "zorder" kwarg to make the image overlay
        # the plot, rather than hide behind it... (e.g. zorder=10)
        _fig.figimage(im, 0, _fig.bbox.ymax + height, zorder=10)

        return


    def fillDict(self):

        parseString = "%Y/%m/%d"

        ## FIXME: Using 'Publication Date' instead of 'Journal Sub' because it's missing from table6353.csv.
        if not self._currDct.has_key('Journal Sub'):
            assert self._currDct.has_key('Publication Date')
            self._currDct['Journal Sub'] = self._currDct['Publication Date']

        if self._currDct["Journal Sub"] == '' or \
               self._currDct["Lead Group"] == '' or \
               self._currDct["Ref Code"] in self._exclusionList:
            return

        if self._currDct["Ref Code"] in self._submissionDates.keys():
            self._currDct["Journal Sub"] = self._submissionDates[self._currDct["Ref Code"]]

        d = datetime.strptime(self._currDct["Journal Sub"], parseString)

        key = d.strftime("%Y-%m")

        if not self._monthDict.has_key(key):
            self._monthDict[key] = {}

        grp = self._currDct["Lead Group"]

        ## merge all perf groups into one:
        if grp in self._perfGroups:
            grp = "PERF"

        if not self._monthDict[key].has_key(grp):
            self._monthDict[key][grp] = 1
        else:
            self._monthDict[key][grp] += 1

        if not self._monthDict[key].has_key("Total"):
            self._monthDict[key]["Total"] = 1
        else:
            self._monthDict[key]["Total"] += 1

        if not self._allDict.has_key(grp):
            self._allDict[grp] = 1
        else:
            self._allDict[grp] += 1

        Ecm = self._currDct['Ecm']
        data_used = self._currDct['Data Used']
        CreationDate = self._currDct['Creation Date']
        assert CreationDate.count('/') == 2
        CreationYear = int(CreationDate.split('/')[0])

        run = 'unknown'

        if self._currDct['Publication Run'] and self._currDct['Publication Run'] != 0:
            _publication_runs = self._currDct['Publication Run'].split(";")
            _publication_max_run = _publication_runs[0]
            for _publication_run in _publication_runs:
                if _publication_max_run < _publication_run:
                    _publication_max_run = _publication_run

            run = 'Run ' + _publication_max_run
        if run == 'Run 0' or run == 'unknown':
            if Ecm == '7 TeV' or Ecm == '7 TeV (2010)' or Ecm == '7 TeV (2011)':
                run = 'Run 1'
            elif Ecm == '8 TeV':
                run = 'Run 1'
            elif Ecm == '7 TeV,8 TeV':
                run = 'Run 1'
            elif Ecm == '0.9 TeV,7 TeV':
                run = 'Run 1'
            elif (Ecm == '2.76 TeV') or (Ecm == '2.76 TeV/NN'):
                run = 'Run 1' # pp February 2013
            elif Ecm == '0.9 TeV,2.36 TeV,7 TeV':
                run = 'Run 1'
            elif Ecm == '0.9 TeV,2.36 TeV':
                run = 'Run 1'
            elif Ecm == '0.9 TeV':
                run = 'Run 1'
            elif Ecm == '13 TeV':
                run = 'Run 2'
            elif Ecm == '13 TeV,5.02 TeV':
                run = 'Run 2'
            elif Ecm == '5 TeV' and CreationYear <= 2014:
                run = 'Run 1' # 'pilot pPb run data 2012'
            elif (Ecm == '5.02 TeV') or (Ecm == '5.02 TeV/NN') and CreationYear <= 2015:
                run = 'Run 1' # 2013 HION
            elif (Ecm == '5.02 TeV') or (Ecm == '5.02 TeV/NN') and CreationYear > 2015:
                run = 'Run 2' # 2015 HION
            elif Ecm == '13 TeV,2.76 TeV/NN,5.02 TeV/NN':
                run = 'Run 2'
            elif data_used == '2012':
                run = 'Run 1'
            elif data_used == '2010-2011':
                run = 'Run 1'

        if run == 'Run 0' and self._currDct["Ref Code"] in ["STDM-2017-15", "STDM-2018-01"]:
            run = 'Run 1'

        if run == 'Run 0' and self._currDct['Publication Run']:
            _publication_runs = self._currDct['Publication Run'].split(";")
            _publication_max_run = _publication_runs[0]
            for _publication_run in _publication_runs:
                if _publication_max_run < _publication_run:
                    _publication_max_run = _publication_run

            if str(_publication_max_run) != str(0):
                run = 'Run ' + _publication_max_run

        if self._currDct["Luminosity Unit"] == 'femtobarn**-1' and float(self._currDct["Luminosity"]) >= 100:
            run = "Full Run 2"

        ## treat run like grp
        if not self._monthDict[key].has_key(run):
            self._monthDict[key][run] = 1
        else:
            self._monthDict[key][run] += 1


        _ft = self._currDct["Full Title"].lower()

        return


    def createArmsRace(self, atlas, run1, run2, fullrun2, labels, title="", outfile="foo.png"):

        print("[INFO] generating file: {}".format(outfile))

        _fig = pylab.figure()
        _fig.clf()

        pylab.title(title, fontsize="x-large", weight='bold')

        ind = np.arange(len(labels))

        ax = _fig.add_subplot(111)
        plot2 = ax.plot(ind, run1, 'k-', linewidth=2)
        plot3 = ax.plot(ind, run2, 'r-', linewidth=2, linestyle='dashed')
        plot4 = ax.plot(ind, fullrun2, 'r-', linewidth=2)
        plot1 = ax.plot(ind, atlas, 'b-', linewidth=2)

        ax.set_xlim(right=ind[-1])

        ax.set_xticks([])

        for ii, i in enumerate(labels):
            if i[-2:] == "12":
                pylab.axvline(x=ind[ii], color="gray", ls='--')
                # if year label doesn't exceed plot size...
                if ind[ii] + 6 < len(ind):
                    pylab.text(ind[ii] + 6, -25.,"%s" % (int(i[:4])+1), color="black", fontsize=8, horizontalalignment="center")

                # upper left
                pylab.text(ind[ii-1] -1.5, atlas[ii] +20, "%d" % atlas[ii], color="blue", fontsize=8)
                if run1[ii] != atlas[ii]:
                    pylab.text(ind[ii-1], run1[ii]*0.95, "%d" % run1[ii], color="k", fontsize=8)
                if run2[ii] != 0:
                    pylab.text(ind[ii-1] -1, run2[ii] + 20, "%d" % run2[ii], color="r", fontsize=8)
                if fullrun2[ii] != 0:
                    pylab.text(ind[ii-1], fullrun2[ii] + 20,"%d" % fullrun2[ii], color="r", fontsize=8)

        pylab.legend([plot1[0], plot2[0], plot3[0], plot4[0]], ["ATLAS Run 1+2", 'ATLAS Run 1', 'ATLAS Partial Run 2', 'ATLAS Full Run 2'], loc=2)

        pylab.text(ind[-1] * 1.01, max(atlas), "ATLAS: %d" % max(atlas), color="b", fontsize=8)
        pylab.text(ind[-1] * 1.01, max(run1), "Run 1: %d" % max(run1), color="k", fontsize=8)
        pylab.text(ind[-1] * 1.01, max(run2), "Partial Run 2: %d" % max(run2), color="r", fontsize=8)
        pylab.text(ind[-1] * 1.01, max(fullrun2), "Full Run 2: %d" % max(fullrun2), color="r", fontsize=8)

        pylab.text(ax.get_xlim()[1],ax.get_ylim()[1],datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='bottom', horizontalalignment='right', rotation=0)

        pylab.tight_layout()
        pylab.subplots_adjust(right=0.86)

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()

        return

    def doFormatting(self):
        """
        Requires:
        -   self._Reader, self._currDct of course
        -   self._monthDict[][]
        """

        for r in self._Reader:
            self._currDct = r
            self.fillDict()

        # unique months
        monthsSet = set(self._monthDict.keys())

        labels = sorted(monthsSet)

        dataAtlas = []
        dataAtlasRun1 = []
        dataAtlasRun2 = []
        dataAtlasFullRun2 = []

        for i, j in enumerate(labels):

            if self._monthDict.has_key(j):
                _dA = self._monthDict[j]["Total"]
                _dA_Run1 = self._monthDict[j].get("Run 1", 0)
                _dA_Run2 = self._monthDict[j].get("Run 2", 0)
                _dA_Full_Run2 = self._monthDict[j].get("Full Run 2", 0)
            else:
                _dA = 0
                _dA_Run1 = 0
                _dA_Run2 = 0
                _dA_Full_Run2 = 0

            if i == 0:
                dataAtlas.append(_dA)
                dataAtlasRun1.append(_dA_Run1)
                dataAtlasRun2.append(_dA_Run2)
                dataAtlasFullRun2.append(_dA_Full_Run2)
            else:
                dataAtlas.append(_dA+dataAtlas[i-1])
                dataAtlasRun1.append(_dA_Run1+dataAtlasRun1[i-1])
                dataAtlasRun2.append(_dA_Run2+dataAtlasRun2[i-1])
                dataAtlasFullRun2.append(_dA_Full_Run2+dataAtlasFullRun2[i-1])

        outfile = "atlas_progress_run_1_and_2.png"
        self.createArmsRace(dataAtlas, dataAtlasRun1, dataAtlasRun2, dataAtlasFullRun2, labels, title="ATLAS Submitted Papers", outfile=outfile)

        return


    def processCSV(self):

        self.setReader()
        self.doFormatting()
        return
