# -*- coding: utf-8 -*-
"""
#########################################################################################
# PyPlotCruncher.py
# Make useful? paper statistics
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, February 7th, 2013
#########################################################################################
# $Rev: 242050 $
# $LastChangedDate: 2016-04-18 13:35:34 +0200 (Mon, 18 Apr 2016) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

import re
from datetime import datetime
import requests
import sys
import json

# import matplotlib for headless terminal
# backend has to be set first
# http://stackoverflow.com/questions/5503601/python-headless-matplotlib-pyplot
import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
import pylab
import numpy as np

from BaseFormatter import BaseFormatter

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
#pylint: disable=wrong-import-position
from DBManager import DBManager
#pylint: enable=wrong-import-position



class PyPlotCruncher(BaseFormatter):

    def __init__(self, run=None):
        BaseFormatter.__init__(self)
        self._monthDict = {}
        self._allDict = {}
        self._allDictCMS = {}

        # THIS MUST BE UPDATED: if not, self._order will make the script crash
        self._order = {"Total" : -99, "STDM" : 0, "FSQ+QCD+EWK+SMP+FWD+GEN" : 0, "EXOT" : 1, "EXO+B2G": 1, "SUSY" : 2, "SUS" : 2, "TOPQ" : 3, "TOP" : 3, "HIGG" : 4, "HIG" : 4, "PERF" : 5, "PRF" : 5, "HION" : 6, "HIN" : 6, "BPHY" : 7, "BPH" : 7, "ARX" : 8, "TRG" : 9, "TCAL" : 10, "SOFT" : 11, "HDBS": 12, "MDET": 13}

        self._exclusionList = ["PERF-2007-01", "LARG-2009-01", "LARG-2009-02", "IDET-2010-01", "MUON-2010-01", "TCAL-2010-01", "PERF-2010-02", "SOFT-2010-01", "SOFT-2017-01"]

        # TODO: verify and improve
        #       this papers are SYS papers. We can not filter for them in the query yet because the SI
        #       works on a view which doesn't include this information
        #       to be improved by running the query directly and filter out SYS papers
        self._exclusionList.extend(["GENR-2018-01", "SOFT-2017-01", "LARG-2019-01", "PIXE-2019-02", "SCTD-2019-01"])

        self._allGroups = ['BPHY', 'DAPR', 'LUMI', 'EGAM', 'EXOT', 'FTAG', 'HIGG', 'HION', 'IDET', 'IDTR', 'JETM', 'LARG', 'MCGN', 'MUON', 'MDET', 'PERF', 'SOFT', 'STDM', 'SUSY', 'TAUP', 'TCAL', 'TOPQ', 'TRIG', 'HDBS']

        self._zeroGroups = self._allGroups[:]

        self._cmsMonthDict = {}

        self._journalDict = {}

        self._groupsJournalsDict = {}

        self._energyDict = {}

        self._runDict = {}

        self._categDict = { "Search" : {"Total" :0}, "Measure" : {"Total" :0}, "Performance" : {"Total" :0}, "Ambiguous" : {"Total" :0} }

        # hardcoded submission date for TOPQ-2012-16
        self._submissionDates = {"TOPQ-2012-16" : "2013/12/11"}

        self._lumiMultiplierDict = { "microbarn**-1" : 1e6,
                                     "nanobarn**-1"  : 1e9,
                                     "picobarn**-1"  : 1e12,
                                     "femtobarn**-1" : 1e15}

        self._fullLumiDict = { '5.02 TeV' : 0, '2.76 TeV' : 0, '7 TeV (2010)' : 35e12, '7 TeV (2011)' : 4e15, '8 TeV' : 20e15, '13 TeV' : 3e15}

        self._run = run

        if self._run == 2:
            # empty months to be filled from Jan 2015:
            for i in range(2015, datetime.today().year+1):
                for j in range(1, 13):
                    if ((i==2015 and j>= 1) or (i>2015)) and not (i == datetime.today().year and j > datetime.today().month):
                        self._monthDict["%d-%0.2d" % (i, j)] = {"Total" : 0}
                        self._runDict["%d-%0.2d" % (i, j)] = {"Total" : 0}
                        self._cmsMonthDict["%d-%0.2d" % (i, j)] = {"Total" : 0}
                        self._energyDict["%d-%0.2d" % (i, j)] = {}
        else:
            # empty months to be filled from March 2010:
            for i in range(2010, datetime.today().year+1):
                for j in range(1, 13):
                    if ((i==2010 and j>= 3) or (i>2010)) and not (i == datetime.today().year and j > datetime.today().month):
                        self._monthDict["%d-%0.2d" % (i, j)] = {"Total" : 0}
                        self._runDict["%d-%0.2d" % (i, j)] = {"Total" : 0}
                        self._cmsMonthDict["%d-%0.2d" % (i, j)] = {"Total" : 0}
                        self._energyDict["%d-%0.2d" % (i, j)] = {}

        return

    def applyAtlasLogo(self, _fig):

        # add ATLAS logo
        im = Image.open('AtlasTwikiLogo.png')
        height = im.size[1] * 1.4

        # We need a float array between 0-1, rather than
        # a uint8 array between 0-255
        im = np.array(im).astype(np.float) / 255

        # With newer (1.0) versions of matplotlib, you can
        # use the "zorder" kwarg to make the image overlay
        # the plot, rather than hide behind it... (e.g. zorder=10)
        _fig.figimage(im, 0, _fig.bbox.ymax + height, zorder=10)


        return

    def fillDict(self):

        parseString = "%Y/%m/%d"

        if self._currDct["Journal Sub"] == '' or \
               self._currDct["Lead Group"] == '' or \
               self._currDct["Ref Code"] in self._exclusionList:
            return

        if self._currDct["Ref Code"] in self._submissionDates.keys():
            self._currDct["Journal Sub"] = self._submissionDates[self._currDct["Ref Code"]]

        d = datetime.strptime(self._currDct["Journal Sub"], parseString)

        key = d.strftime("%Y-%m")

        if not self._monthDict.has_key(key):
            self._monthDict[key] = {}

        grp = self._currDct["Lead Group"]

        # merge all perf groups into one:
        if grp in self._perfGroups:
            grp = "PERF"

        try:
            self._zeroGroups.remove(grp)
        except:
            pass

        if not self._monthDict[key].has_key(grp):
            self._monthDict[key][grp] = 1
        else:
            self._monthDict[key][grp] += 1

        if not self._monthDict[key].has_key("Total"):
            self._monthDict[key]["Total"] = 1
        else:
            self._monthDict[key]["Total"] += 1

        if not self._allDict.has_key(grp):
            self._allDict[grp] = 1
        else:
            self._allDict[grp] += 1


        j = self._currDct["Journal"]
        j = j.strip()
        if j == 'Nucl. Phys. B':
            j = 'NPB'

        if j == "Computing and Software for Big Science":
            j = "CSBS"

        if not self._journalDict.has_key(j):
            self._journalDict[j] = 1
        else:
            self._journalDict[j] += 1

        if not self._groupsJournalsDict.has_key(grp):
            self._groupsJournalsDict[grp] = {}

        if not self._groupsJournalsDict[grp].has_key(j):
            self._groupsJournalsDict[grp][j] = 1
        else:
            self._groupsJournalsDict[grp][j] += 1

        # fill ECM dict
        _ecms = self._currDct["ECM"].split(";")
        _luminosities = self._currDct["Luminosity"].split(";")
        _luminosities_units = self._currDct["Luminosity Unit"].split(";")
        for _ecm, _luminosity, _luminosity_unit in zip(_ecms, _luminosities, _luminosities_units):
            _ecm = _ecm.replace("/NN", "").strip()

            if not self._lumiMultiplierDict.has_key(_luminosity_unit):
                continue

            _lumiMult = self._lumiMultiplierDict[_luminosity_unit]

            if _ecm == "7 TeV":
                _ecm = "7 TeV (2011)"
                if _luminosity_unit == "picobarn**-1":
                    _ecm = "7 TeV (2010)"

            if not self._fullLumiDict.has_key(_ecm):
                continue

            if (float(_luminosity) * _lumiMult) < self._fullLumiDict[_ecm]:
#            if ((float(self.z_currDct["Luminosity"]) * _lumiMult) < self._fullLumiDict[_ecm]):
                continue

            if not self._energyDict[key].has_key(_ecm):
                self._energyDict[key][_ecm] = {"Total" : 0}

            if not self._energyDict[key][_ecm].has_key(grp):
                self._energyDict[key][_ecm][grp] = 1
            else:
                self._energyDict[key][_ecm][grp] += 1
            self._energyDict[key][_ecm]["Total"] += 1


        _ft = self._currDct["Full Title"].lower()

        if grp in self._perfGroups:
            _categ = "Performance"
        elif "determ" in _ft or "measur" in _ft or "evidence" in _ft or "observ" in _ft or "study" in _ft or "character" in _ft or "extraction" in _ft or "density" in _ft or "distribution" in _ft or "structure" in _ft or "dynamics" in _ft or "dependence" in _ft:
            _categ = "Measure"
        elif "search" in _ft or "hunt" in _ft or "limit" in _ft or "constraint" in _ft:
            _categ = "Search"
        elif "performance" in _ft or "resolution" in _ft or "luminosity" in _ft:
            _categ = "Performance"
        elif grp == "EXOT" or grp == "SUSY":
            _categ = "Search"
        elif grp == "GEN":
            _categ = "Measure"
        elif self._currDct["Journal"] == "JINST":
            _categ = "Performance"
        else:
            _categ = "Ambiguous"

        if not self._categDict[_categ].has_key(key):
            self._categDict[_categ][key] = 1
        else:
            self._categDict[_categ][key] += 1

        # set zero to all others
        for k, v in self._categDict.iteritems():
            if k != _categ and not v.has_key(key):
                self._categDict[k][key] = 0

        self._categDict[_categ]["Total"] += 1

        return


    def collateTrimesters(self, dct):

        _newDct = {}

        # structure of input dict is month,group
        for k1, v1 in dct.iteritems():
            _year = k1.split("-")[0]
            _month = int(k1.split("-")[1])
            _trim = "%s-%dT" % (_year, (1+((_month-1)/3)))

            for k2, v2 in v1.iteritems():
                if not _newDct.has_key(k2):
                    _newDct[k2] = {_trim:v2}
                elif not _newDct[k2].has_key(_trim):
                    _newDct[k2].update({_trim:v2})
                else:
                    _newDct[k2][_trim] += v2

                # zero the other trimesters?
                for k3 in _newDct.keys():
                   if k2 != k3 and not _newDct[k3].has_key(_trim):
                        _newDct[k3][_trim] = 0

        # last zeroing check?
        for k1, v1 in _newDct.iteritems():
            for v2 in v1.keys():
                for k2 in _newDct.keys():
                    if k1!=k2 and not _newDct[k2].has_key(v2):
                        _newDct[k2][v2] = 0


        return _newDct


    def getCMSstats(self):

        dbm = DBManager()

        results = dbm.execute("SELECT REF_CODE, PRE_PUBLICATION FROM CDS_CMS_DATA WHERE ignore=0 AND REF_CODE IS NOT NULL AND PRE_PUBLICATION IS NOT NULL")

        for j, i in results:

            if 'CFT' in str(j):
                continue

            if 'CMS-MUO-10-001' in str(j):
                continue

            grp = j.split('-')[1]

            # merges some of the groups
            if grp in ["FSQ", "QCD", "EWK", "SMP", "FWD", "GEN"]:
#                grp = "FSQ+QCD+EWK+SMP+FWD+GEN"
                grp = "STDM"
            if grp in ["B2G", "EXO"]:
                grp = "EXO+B2G"
            if grp in ["MUO", "JME", "TAU", "TRI", "BTV", "EGM", "TRK"]:
                grp = "PERF"

            if not self._allDictCMS.has_key(grp):
                self._allDictCMS[grp] = 1
            else:
                self._allDictCMS[grp] += 1

            key = i.strftime("%Y-%m")

            if not self._cmsMonthDict.has_key(key):
                self._cmsMonthDict[key] = {grp : 1, "Total" : 0}
            else:
                if not self._cmsMonthDict[key].has_key(grp):
                    self._cmsMonthDict[key][grp] = 1
                else:
                    self._cmsMonthDict[key][grp] += 1

            self._cmsMonthDict[key]["Total"] += 1

        return



    def createMonthlyBar(self, bars, labels, title="", color='b', outfile="foo.png"):

        print("[INFO] generating file: {}".format(outfile))

        ind = np.arange(len(bars))  # the x locations for the groups
        width = 1.0 #0.7       # the width of the bars

        _fig = pylab.figure()
        _fig.clf()

        pylab.title(title, fontsize="x-large", weight='bold')

        ax = _fig.add_subplot(111)
        rects = ax.bar(ind, bars, width, color=color)

        ax.set_xlim([0, len(bars)])

        new_labels = list()
        for i, l in enumerate(labels):
            if (l.endswith('-01') or l.endswith('-07')):
                new_labels.append(l)
            else:
                new_labels.append('')

        ax.set_xticks(ind)
        ax.set_xticklabels(new_labels)

        for i in ax.get_xticklabels():
            i.set_rotation(70)
            i.set_size("x-small")

        avg = 0

        max_height = 0
        for rect in rects:
            height = rect.get_height()
            avg += height
            ax.text(rect.get_x()+rect.get_width()/2., .2+height, '%d'%int(height), ha='center', va='bottom', fontsize="x-small")
            if height > max_height:
                max_height = height

        ax.set_ylim([0, max_height*1.3])

        avg = 1. * avg / len(rects)

        pylab.axhline(y=avg, color=color, ls='--')
        pylab.text(width/2, .2+avg, "Average: %.2g" % avg, color=color, fontsize=9)

        pylab.text(ax.get_xlim()[1],ax.get_ylim()[1],datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='bottom', horizontalalignment='right', rotation=0)

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()

        return


    def createMonthlyStackedBar(self, bars, labels, legends, title="", outfile="foo.png", loc=2):

        print("[INFO] generating file: {}".format(outfile))

        ind = np.arange(len(bars[0]))  # the x locations for the groups
        width = 1.0 #0.7       # the width of the bars

        _fig = pylab.figure()
        _fig.clf()

        pylab.title(title, fontsize="x-large", weight='bold')

        ax = _fig.add_subplot(111)

        _colors = iter(pylab.cm.Paired(np.linspace(0,1,len(legends))))

        _colorMap = {"Search"      : 'r',
                     "Measure"     : 'b',
                     "Performance" : 'y',
                     "Other"       : 'k',
                     "Ambiguous"   : 'g'}

        allRects = []

        allSums = []

        #for i,b in enumerate(bars):
        #       print i, len(b), b

        for i, j in enumerate(bars[0]):
            allSums.append(sum([bars[k][i] for k in range(len(bars))]))

        max_len_bars = 0
        for i, j in enumerate(bars):
            if _colorMap.has_key(legends[i]):
                c = _colorMap[legends[i]]
            else:
                c = _colors.next()

            if i == 0:
                rects = ax.bar(ind, j, width, color=c)
            else:
                _sum = sum([np.array(bars[k]) for k in range(i)])
                rects = ax.bar(ind, j, width, color=c, bottom=_sum)

            allRects.append(rects)

            if len(bars[k]) > max_len_bars:
                max_len_bars = len(bars[k])

        ax.set_xlim([0, max_len_bars])

        new_labels = list()
        for i, l in enumerate(labels[0]):
            if l.endswith('-01') or l.endswith('-07'):
                new_labels.append(l)
            else:
                new_labels.append('')

        ax.set_xticks(ind)
        ax.set_xticklabels(new_labels)

        for i in ax.get_xticklabels():
            i.set_rotation(70)
            i.set_size("x-small")

        max_height = 0
        for r, rect in enumerate(rects):
            height = allSums[r]
            ax.text(rect.get_x()+rect.get_width()/2., .2+height, '%d'%int(height), ha='center', va='bottom', fontsize="x-small")
            if height > max_height:
                max_height = height

        ax.set_ylim([0, max_height*1.3])

        pylab.legend([r[0] for r in allRects], legends, loc=loc)

        pylab.text(ax.get_xlim()[1],ax.get_ylim()[1],datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='bottom', horizontalalignment='right', rotation=0)

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()

        return


    def createPieChart(self, _fracs, _labels, title="", outfile="foo.png"):

        print("[INFO] generating file: {}".format(outfile))

        _fig = pylab.figure(1, figsize=(6, 6))
        _fig.clf()

        pylab.title(title, fontsize="x-large", weight='bold')

        colors = pylab.cm.Paired(np.linspace(0,1,len(_labels)))

        # Available colormaps:
        #    * Accent
        #    * Dark2
        #    * Paired
        #    * Pastel1
        #    * Pastel2
        #    * Set1
        #    * Set2
        #    * Set3

        labels = [_labels[i] + " " + str(_fracs[i]) for i, j in enumerate(_fracs) if _fracs[i] > 0]
        colors = [colors[i] for i, j in enumerate(_fracs) if _fracs[i] > 0]
        fracs  = [i for i in _fracs if i>0]

        wedges = pylab.pie(fracs, startangle=90, autopct=lambda p: "%.0f" % (p * sum(fracs) / 100), shadow=True, colors=colors)

        pylab.text(1.5,1.5,datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='top', horizontalalignment='right', rotation=0)

        if len(fracs) < 5:
            ncol = 2
        elif len(fracs) < 10:
            ncol = 3
        else:
            ncol = 4

        pylab.legend(wedges[0], labels, ncol=ncol, fontsize='medium', frameon=False, mode='expand', bbox_to_anchor=(-0.1,0.,1.2,.1))

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()


    def _create_ecm_plots_runs(self, grp, title, outfile):

        print("[INFO] generating file: {}".format(outfile))

        if grp == "HION":
            runs = ["1", "2"]
        else:
            runs = ["1", "2", "Full Run 2"]

        _fig = pylab.figure()
        _fig.clf()
        pylab.title(title, fontsize="x-large", weight="bold", y=1.02)

        labels = sorted(self._runDict.keys())
        ind = np.arange(len(labels))

        legs = []
        labs = []
        colors = iter(pylab.cm.gist_rainbow(np.linspace(0,1,len(runs))))

        len_of_runs = []
        for run in runs:
            color = colors.next()
            data_run = []
            for index, date in enumerate(labels):
                _count = 0
                if grp in self._runDict[date]:
                    if run in self._runDict[date][grp]:
                        _count = self._runDict[date][grp][run]

                if index == 0:
                    data_run.append(_count)
                else:
                    data_run.append(_count + data_run[index-1])

            len_of_runs.append(data_run[-1])
            axis = _fig.add_subplot(111)
            plot = axis.plot(np.array(ind), np.array(data_run), color=color, linewidth=4)
            legs.append(pylab.Rectangle((0, 0), 1, 1, fc=color))
            if run == "1":
                labs.append("Run 1")
            elif run == "2":
                if grp == "HION":
                    labs.append("Run 2")
                else:
                    labs.append("Partial Run 2")
            elif run == "Full Run 2":
                labs.append("Full Run 2")

            pylab.text(ind[-1] * 1.01, data_run[-1], "{}".format(data_run[-1]), color=color, fontsize=8)

        axis.set_xlim(right=ind[-1])
        axis.set_xticks([])

        for index, date in enumerate(labels):
            if date[-2:] == "12":
                if "2007" in date or "2009" in date:
                    continue
                pylab.axvline(x=ind[index], color="gray", ls='--')
                pylab.text(ind[index], -1.2 * max(len_of_runs)/50, "{}".format(int(date[:4])+1), color="black", fontsize=8)

        pylab.legend(legs, labs, loc=2)
        pylab.text(axis.get_xlim()[1],axis.get_ylim()[1],datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='bottom', horizontalalignment='right', rotation=0)

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()


    def createECMPlots(self, grp="Total", title="", outfile="foo.png"):

        print("[INFO] generating file: {}".format(outfile))

        _fig = pylab.figure()
        _fig.clf()

        pylab.title(title, fontsize="x-large", weight='bold')

        labels = sorted(self._energyDict.keys())

        ecmSet = set()
        for v in self._energyDict.values():
            for k in v.keys():
                ecmSet.add(k)

        energies = sorted(list(ecmSet))

        colors = iter(pylab.cm.gist_rainbow(np.linspace(0,1,len(energies))))

        ind = np.arange(len(labels))

        labs = []
        legs = []

        for _e in energies:
            c = colors.next()
            # darken the green
            c[1] *= 0.7
            #print "COLOR", c
            dataE = []
            for i, j in enumerate(labels):
                _cum = 0
                if self._energyDict[j].has_key(_e):
                    if self._energyDict[j][_e].has_key(grp):
                        _cum = self._energyDict[j][_e][grp]

                if i == 0:
                    dataE.append(_cum)
                else:
                    dataE.append(_cum+dataE[i-1])

            if dataE[-1] == 0:
                continue

            ax = _fig.add_subplot(111)
            plot = ax.plot(ind, dataE, '-', color=c, linewidth=4)
            legs.append(pylab.Rectangle((0, 0), 1, 1, fc=c))
            labs.append(_e)

            pylab.text(ind[-1] * 1.01, dataE[-1], "%d" % dataE[-1], color=c, fontsize=8)

        ax.set_xlim(right=ind[-1])

        ax.set_xticks([])

        for ii, i in enumerate(labels):
            if i[-2:] == "12":
                pylab.axvline(x=ind[ii], color="gray", ls='--')
                pylab.text(ind[ii], 0.1,"%s" % (int(i[:4])+1), color="black", fontsize=8)

        pylab.legend(legs, labs, loc=2)

        pylab.text(ax.get_xlim()[1],ax.get_ylim()[1],datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='bottom', horizontalalignment='right', rotation=0)

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()


    def createArmsRace(self, atlas, cms, labels, title="", outfile="foo.png"):

        print("[INFO] generating file: {}".format(outfile))

        _fig = pylab.figure()
        _fig.clf()

        pylab.title(title, fontsize="x-large", weight='bold')

        ind = np.arange(len(labels))

        ax = _fig.add_subplot(111)
        plot1 = ax.plot(ind, atlas, 'b-', linewidth=2)
        if cms:
            plot2 = ax.plot(ind, cms, 'r-', linewidth=2)

        ax.set_xlim(right=ind[-1])

        ax.set_xticks([])

        for ii, i in enumerate(labels):
            if i[-2:] == "12":
                pylab.axvline(x=ind[ii], color="gray", ls='--')
                # if year label doesn't exceed plot size...
                if ind[ii] + 6 < len(ind):
                    pylab.text(ind[ii] + 6, -25.,"%s" % (int(i[:4])+1), color="black", fontsize=8, horizontalalignment='center')

                # plot points per experiment
                if cms:
                    if atlas[ii] > cms[ii]:
                        # upper left
                        pylab.text(ind[ii-1] -2, atlas[ii] + 20, "%d" % atlas[ii], color="blue", fontsize=8)
                        # lower right
                        pylab.text(ind[ii] +1, cms[ii] -30, "%d" % cms[ii], color="red", fontsize=8)
                    else:
                        # lower right
                        pylab.text(ind[ii] +1, atlas[ii] -30, "%d" % atlas[ii], color="blue", fontsize=8)
                        # upper left
                        pylab.text(ind[ii-1] -2, cms[ii] + 20, "%d" % cms[ii], color="red", fontsize=8)
                else:
                    # upper left
                    pylab.text(ind[ii-1] - 1, atlas[ii] + 15, "%d" % atlas[ii], color="blue", fontsize=8)

        if cms:
            pylab.legend([plot1[0], plot2[0]], ["ATLAS", "CMS"], loc=2)
        else:
            pylab.legend([plot1[0]], ["ATLAS"], loc=2)

        pylab.text(ind[-1] * 1.01, max(atlas), "ATLAS: %d" % max(atlas), color="b", fontsize=8)
        if cms:
            pylab.text(ind[-1] * 1.01, max(cms), "CMS: %d" % max(cms), color="r", fontsize=8)

        pylab.text(ax.get_xlim()[1],ax.get_ylim()[1],datetime.today().strftime("Last update: %d-%b-%Y"), color="#000000", fontsize=6, verticalalignment='bottom', horizontalalignment='right', rotation=0)

        pylab.savefig(outfile)
        pylab.savefig(outfile[:-3] + "pdf")

        pylab.close()


    def _examine(self):
        journal_sub = datetime.strptime(self._currDct["Journal Sub"], "%Y/%m/%d")
        key = journal_sub.strftime("%Y-%m")

        if key not in self._runDict:
            self._runDict[key] = {}

        group = self._currDct["Lead Group"]
        if group in self._perfGroups:
            group = "PERF"

        if group not in self._runDict[key]:
            self._runDict[key][group] = {}

        _max_run = "0"
        if self._currDct["Run"]:
            _publication_runs = self._currDct["Run"].split(";")
            _max_run = str(max(_publication_runs))

        if self._currDct["Luminosity Unit"] == 'femtobarn**-1' and float(self._currDct["Luminosity"]) >= 100:
            if group != "HION":
                _max_run = "Full Run 2"

        if _max_run not in self._runDict[key][group]:
            self._runDict[key][group][_max_run] = 1
        else:
            self._runDict[key][group][_max_run] += 1


    def doFormatting(self):


        for r in self._Reader:
            self._currDct = r
            self.fillDict()
            self._examine()

        # Monthly chart
        labels = sorted(self._monthDict.keys())

        _ATLAStrimDict = self.collateTrimesters(self._monthDict)

        for key in _ATLAStrimDict:
            if key not in self._order:
                print("[ERROR] Key not in self._order {}".format(key))
                max_value = 0
                for _, value in self._order.iteritems():
                    if value > max_value:
                        max_value = value
                self._order[key] = max_value + 1
                print("[DEBUG] self._order[{}] = {}".format(key, self._order[key]))

        ##########################################################################
        ############## moved from below - then we exit, have to check ############
        self.createECMPlots()
        for i in ["HION", "SUSY", "EXOT", "HIGG", "STDM", "TOPQ", "PERF", "BPHY", "HDBS"]:
            outfile = "ecm-%s" % i
            if self._run:
                outfile = "ecm-%s_run%i" % (i, self._run)
            self._create_ecm_plots_runs(i, u"Papers per run / %s" % i, outfile + "_runs.png")

        ##########################################################################

        bars = [self._monthDict[i]["Total"] for i in labels]

        outfile = "months.png"
        if self._run:
            outfile = "months_run%i.png" % self._run
        self.createMonthlyBar(bars, labels, title="ATLAS Papers/Month", outfile=outfile)

        # remove zero entry groups, sort from lowest to highest
        groups = [i for i in self._allGroups if not i in self._zeroGroups]

        labels = [i for i in groups if not i in self._zeroGroups]

        labels.sort(key=lambda x: self._allDict[x], reverse=True)

        fracs = [self._allDict[i] for i in labels]

        outfile = "groups.png"
        if self._run:
            outfile = "groups_run%i.png" % self._run
        print("[DEBUG] atlas - papers lead group\nfracs = {}\nlabels = {}".format(fracs, labels))
        self.createPieChart(fracs, labels, title="ATLAS - Papers/Lead-group", outfile=outfile)

        labels = [i for i in self._allDictCMS.keys()]

        labels.sort(key=lambda x: self._allDictCMS[x], reverse=True)

        fracs = [self._allDictCMS[i] for i in labels]

        outfile = "groups_CMS.png"
        if self._run:
            outfile = "groups_CMS_run%i.png" % self._run
        self.createPieChart(fracs, labels, title="CMS - Papers/Lead-group (Internal)", outfile=outfile)

        labels1 = sorted(self._journalDict.keys(), key=lambda x: self._journalDict[x], reverse=True)

        labels=labels1

        fracs = [self._journalDict[i] for i in labels]

        outfile = "journals.png"
        if self._run:
            outfile = "journals_run%i.png" % self._run
        self.createPieChart(fracs, labels, title="ATLAS - Journals (Internal)", outfile=outfile)

        labels = sorted([i for i in self._categDict.keys() if self._categDict[i]["Total"]], key=lambda x: self._categDict[x]["Total"], reverse=True)

        fracs = [self._categDict[i]["Total"] for i in labels]

        outfile = "type.png"
        if self._run:
            outfile = "type_run%i.png" % self._run
        self.createPieChart(fracs, labels, title="ATLAS - Type of Paper", outfile=outfile)

        # pie charts for individual groups
        for k, v in self._groupsJournalsDict.iteritems():
            labels = labels1

            fracs = map(lambda x: v[x] if v.has_key(x) else 0, labels)

            outfile = "journals_%s.png" % k
            if self._run:
                outfile = "journals_%s_run%i.png" % (k, self._run)
            self.createPieChart(fracs, labels, title="%s - Journals (Internal)" % k, outfile=outfile)

        allFracs   = []
        allLabels  = []
        allLegends = []

        for i in self._categDict.keys(): #["Search","Measure","Performance","Other"]:
            if not self._categDict[i]["Total"]:
                continue

            allLegends.append(i)

            l = sorted([j for j in self._categDict[i].keys() if j != "Total"])

            allLabels.append(l)
            allFracs.append([self._categDict[i][j] for j in l])

        outfile = "months_stacked.png"
        if self._run:
            outfile = "months_stacked_run%i.png" % self._run
        self.createMonthlyStackedBar(allFracs, allLabels, allLegends, title="ATLAS - Type of Paper", outfile=outfile)

        allFracs   = []
        allLabels  = []
        allLegends = []

        for i in sorted(_ATLAStrimDict.keys(), key=lambda x: self._order[x]):
            if i == "Total":
                continue

            allLegends.append(i)

            l = sorted(_ATLAStrimDict[i].keys())

            allLabels.append(l)
            allFracs.append([_ATLAStrimDict[i][j] for j in l])

        outfile = "trimesters_stacked.png"
        if self._run:
            outfile = "trimesters_stacked_run%i.png" % self._run
        self.createMonthlyStackedBar(allFracs, allLabels, allLegends, title="ATLAS - Type of Paper", outfile=outfile)
        _CMStrimDict = self.collateTrimesters(self._cmsMonthDict)

        allFracs   = []
        allLabels  = []
        allLegends = []

        monthsSet = set(self._monthDict.keys() + self._cmsMonthDict.keys())

        labels = sorted(monthsSet)

        dataAtlas = []
        dataCMS   = []

        for i, j in enumerate(labels):

            if self._monthDict.has_key(j):
                _dA = self._monthDict[j]["Total"]
            else:
                _dA = 0

            if self._cmsMonthDict.has_key(j):
                _dC = self._cmsMonthDict[j]["Total"]
            else:
                _dC = 0

            if i == 0:
                dataAtlas.append(_dA)
                dataCMS.append(_dC)
            else:
                dataAtlas.append(_dA+dataAtlas[i-1])
                dataCMS.append(_dC+dataCMS[i-1])

        labels_cms = sorted(self._cmsMonthDict.keys())
        bars = [self._cmsMonthDict[i]["Total"] for i in labels_cms]

        outfile = "months_CMS.png"
        if self._run:
            outfile = "months_CMS_run%i.png" % self._run
        self.createMonthlyBar(bars, labels_cms, title="CMS Papers/Month (Internal)", outfile=outfile, color='r')

        outfile = "arms_race.png"

        if self._run:
            outfile = "arms_race_run%i.png" % self._run
        self.createArmsRace(dataAtlas, dataCMS, labels, title="ATLAS & CMS Submitted Papers (Internal)", outfile=outfile)

        outfile = "atlas_progress.png"
        if self._run:
            outfile = "atlas_progress_run%i.png" % self._run
        self.createArmsRace(dataAtlas, None, labels, title="ATLAS Submitted Papers", outfile=outfile)


    def processCSV(self):

        self.setReader()
        self.getCMSstats()
        self.doFormatting()
