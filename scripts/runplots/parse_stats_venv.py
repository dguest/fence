#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# author: mcolautt - maurizio.colautti@cern.ch - ctrl.mau@gmail.com

import os
import datetime
import operator
from shutil import copyfile
import argparse
import pkg_resources
pkg_resources.require("Matplotlib==1.2.0")

import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

# needed to have a correct months delta
from dateutil.relativedelta import relativedelta

# not running on /afs/ ?  then it must be a local test 
# which implies not copying files at the end in the www/GROUPS/PHYSOFFICE folder 
local = False
dir_path = os.path.dirname(os.path.realpath(__file__))
if (dir_path.find("/afs/") == -1):
    local = True


def check_captiondate(captiondate, last_date):
    if captiondate is not None:
        if captiondate == last_date:
            return True
    return False

def process_datas(data_num, starts_from, ends_at, captiondate = None, group_by = 1):
    xticklabel = list()
    xtickvalues = list()
    last_date = starts_from - datetime.timedelta(days = 1)
    total_proceedings = 0
    caption_index = 0
    index = 0
    for ent in data_num:
        if ent[0] < starts_from or ent[0] > ends_at:
            continue
        # to fill missing days between dates with no proceedings, and setting them to 0
        if ent[0] != last_date + datetime.timedelta(days=1):
            while last_date + datetime.timedelta(days=1) < ent[0]:
                if check_captiondate(captiondate, last_date):
                    caption_index = index
                xticklabel.append(last_date + datetime.timedelta(days=1))
                xtickvalues.append(0)
                index += 1
                last_date = last_date + datetime.timedelta(days=1)

        if check_captiondate(captiondate, last_date):
            caption_index = index
        total_proceedings = total_proceedings + ent[1]
        xticklabel.append(ent[0])
        index += 1
        xtickvalues.append(ent[1])

        last_date = ent[0]

    # to fill missing days until the ends_at date
    while last_date + datetime.timedelta(days=1) < ends_at:
                xticklabel.append(last_date + datetime.timedelta(days=1))
                if check_captiondate(captiondate, last_date):
                    caption_index = index
                xtickvalues.append(0)
                index += 1
                last_date = last_date + datetime.timedelta(days=1)
    
    # TODO: waiting to know if bin-size should be fixed; to 1/2/4; or variable
    # update: it has been chosen a 2 days-size bin. 
    #         if there will be need to have it variable, will start to refactor here.
    group_by = 2
    if group_by == 1:
        return xticklabel, xtickvalues, total_proceedings, caption_index
    else:
        grouped = 1
        partial_sum = 0
        xticklabel_grouped = list()
        xtickvalues_grouped = list()
        for xticklab, xtickval in zip(xticklabel, xtickvalues):
            partial_sum += xtickval
            if grouped == 1:
                xticklabel_grouped.append(xticklab)
            if grouped == group_by:
                xtickvalues_grouped.append(partial_sum)
                grouped = 0
                partial_sum = 0
            grouped += 1
        return xticklabel_grouped, xtickvalues_grouped, total_proceedings, caption_index



# can't apply all x-axis labels, so we only take one_every tot 
def filter_labels(labels, wantedticks):
    one_every = int((len(labels)+1) / (wantedticks-1))
    filtered_labels = list()
    for label in labels[0::one_every]:
        filtered_labels.append(label)
    return filtered_labels

def makeGraph(xlabel, ylabel, title, values, labels, wantedticks=10, namefile="foo.jpg", caption_index = "", caption = "", captiondate = ""):
    plt.figure(figsize=(14,8))
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)

    divisor = int((len(values)+1) / (wantedticks-1))


    ind = np.arange(len(values))

    if len(labels) > wantedticks:
        labels = filter_labels(labels, wantedticks)

    rects = plt.bar(ind, values, width=0.7, color="c", linewidth="0")
    plt.xticks([(lambda x: x)(x) for x in range(0,len(values)+1,divisor)], labels, rotation="vertical", fontsize="7")

    plt.gcf().subplots_adjust(bottom=0.15)

    # write values on top of bars
    # write caption on specific date
    index = 1
    for rect in rects:
        height = rect.get_height()
        if caption_index:
            if caption_index == index:
                plt.text(rect.get_x() + rect.get_width() -0.25, max(values) + 0.75, captiondate, ha='center', va='bottom', fontsize="8")
                plt.text(rect.get_x() + rect.get_width() -0.25, max(values) + 0.55, caption, ha='center', va='bottom', fontsize="8")
                plt.plot(rect.get_x() + rect.get_width() -0.25, max(values) + 0.4, marker='o')
        index += 1
        if height==0:
            continue
        plt.text(rect.get_x() + rect.get_width() -0.35, height + 0.05, '%d' % int(height), ha='center', va='bottom', fontsize="7")

    # set axis; has to be here for compatibility with older matplotlib versions
    plt.axis([0,len(values),0,max(values)+1])
    
    if namefile == "foo.jpg":
        plt.show()
    else:
        plt.savefig(namefile + ".png", dpi=96, bbox_inches='tight')
        plt.savefig(namefile + ".pdf",  bbox_inches='tight')

        plt.close()

# when in the source text files there's a line where there could be a date
# try to extract it and format it correctly 
def extract_date(line, file_name):
    global logError
    ent = line.rstrip("\r\n").split()
    if len(ent) != 2:
        return ""
    pdat = ent[1].split("/")
    if len(pdat) != 3:
        return ""
    try:
        if int(pdat[2]) < 2000:
            pdat[2] = int(pdat[2]) + 2000
    except ValueError:
        logError = logError + "file: " + file_name + " error in date: " + line
        # print "error in format: " + line
        return ""
    try:
        if int(pdat[1]) > 12:
            # print "error in format: " + line
            logError = logError + "file: " + file_name + " error in date: " + line
    except ValueError:
        # print "error in format: " + line
        logError = logError + "file: " + file_name + " error in date: " + line
        return ""
    try :
        date = datetime.date(int(pdat[2]), abs(int(pdat[1])), int(pdat[0]))
    except ValueError:
        # print "error in format: " + line
        logError = logError + "file: " + file_name + " error in date: " + line
        return ""
    return date

# transform date in proper datetime date object
def extract(date_in):
    pdat = date_in.split("/")
    date_out = datetime.date(int(pdat[2]), int(pdat[1]), int(pdat[0]))
    return date_out


parser = argparse.ArgumentParser()
parser.add_argument("-s", "--startdate", help="""Set a different start date in format DD-MM-YYYY (i.e. 21/10/2008)
    The start date is the least recent one, and will go up to today if --enddate is not set, otherwise it will consider
    the timelapse between --startdate and --enddate""")
parser.add_argument("-e", "--enddate", help="""Set a different end date than today in format DD-MM-YYYY (i.e. 21/10/2008)
    The end date is the most recent one, and the plots will display the 3 and 6 months previously to this date,
    if --startdate is not set otherwise it will consider the timelapse between --startdate and --enddate""")
parser.add_argument("-d", "--datecaption", help="Set a date where you want a caption (set through -c option) to be displayed")
parser.add_argument("-c", "--caption", help="Set a caption between quotations to be placed on a certain date (set through -d option)")
args = parser.parse_args()
captiondate = ""
caption = ""
if args.startdate:
    #print args.startdate
    try:
        datetime.datetime.strptime(args.startdate, '%d/%m/%Y')
    except ValueError:
        raise ValueError("Incorrect data format, should be DD/MM/YYYY")
if args.enddate:
    #print args.enddate
    try:
        datetime.datetime.strptime(args.enddate, '%d/%m/%Y')
    except ValueError:
        raise ValueError("Incorrect data format, should be DD/MM/YYYY")
if args.caption:
    caption = args.caption
    #print args.caption
if args.datecaption:
    #print args.datecaption
    try:
        datetime.datetime.strptime(args.datecaption, "%d/%m/%Y")
        captiondate = extract(args.datecaption)
    except ValueError:
        raise ValueError("Incorrect data format, should be DD/MM/YYYY")

logError = ""

proot="./PROC" # local test 1
proot="./PROC_ORIGINAL" # local test 2
if not local:
    proot="/afs/cern.ch/atlas/www/GROUPS/PHYSICS/Coord/proc/PROC" # atlaspo run
receiv=dict()
deadline=dict()
approv=dict()

from_year = 1800
to_year = 2177

for file in os.listdir(proot):
    if file.find(".")==-1:
        inf=os.path.join(proot,file)
        infile = open(inf,"r")
        receivdate=datetime.date(4000,1,1)
        for line in infile:
            if line.find("APPROVREQDATE") != -1:
                dent = extract_date(line, inf)
                if dent == "":
                    continue
                receivdate=dent
                rece=dent
                if rece not in receiv.keys():
                    receiv[rece]=1
                else:
                    receiv[rece] +=1

            if line.find("APPROVEDDATE") != -1:
                dent = extract_date(line, inf)
                if dent == "":
                    continue
                appr=dent
                if appr not in approv.keys():
                    approv[appr]=1
                else:
                    approv[appr] +=1
            if line.find("PROCDEADDATE") != -1:
                dentp = extract_date(line, inf)
                if dentp == "":
                    continue
                if receivdate.year == 4000:
                        continue
                if receivdate.year < from_year:
                        continue
                if receivdate.year > to_year:
                        continue
                dateoff=dentp-receivdate
                deadl=dateoff.days
                #if deadl < -30:
                #    print "sanity",dentp,receivdate
                #print "ETA=%s %s"%(dateoff,dateoff.days)
                if deadl not in deadline.keys():
                    deadline[deadl]=1
                else:
                    deadline[deadl] +=1

        infile.close()

xticklabel = list()
xtickvalues = list()
sorted_received=sorted(receiv.items(),key=operator.itemgetter(0))
sorted_approved=sorted(approv.items(),key=operator.itemgetter(0))
sorted_deadline=sorted(deadline.items(),key=operator.itemgetter(0))
today = datetime.date.today()
sixmonthsago = today + relativedelta(months=-6)
threemonthsago = today + relativedelta(months=-3)
date20180301 = datetime.date(2018,3,1)
date20190212 = datetime.date(2019,2,12)

if args.startdate:
    if args.enddate:
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_received, extract(args.startdate), extract(args.enddate), captiondate)
        makeGraph('Date of receipt', 'Number of received / 2 days', 'Total ' + str(total_proceedings) + ' proceedings received between ' + args.startdate + ' and ' + args.enddate, xtickvalues, xticklabel, 20, "received_from_" + args.startdate.replace("/", "_") + "_to_" + args.enddate.replace("/", "_"), caption_index, caption, captiondate)
        del xticklabel[:]
        del xtickvalues[:]
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_approved, extract(args.startdate), extract(args.enddate), captiondate)
        makeGraph('Date of approval', 'Number of approvals / 2 days', 'Total ' + str(total_proceedings) + ' proceedings approved between ' + args.startdate + ' and ' + args.enddate, xtickvalues, xticklabel, 20,  "approved_from_" + args.startdate.replace("/", "_") + "_to_" + args.enddate.replace("/", "_"), caption_index, caption, captiondate)
    else:
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_received, extract(args.startdate), today, captiondate)
        makeGraph('Date of receipt', 'Number of received / 2 days', 'Total ' + str(total_proceedings) + ' proceedings received since ' + args.startdate, xtickvalues, xticklabel, 20, "received_since_" + args.startdate.replace("/", "_"), caption_index, caption, captiondate)
        del xticklabel[:]
        del xtickvalues[:]
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_approved, extract(args.startdate), today, captiondate)
        makeGraph('Date of approval', 'Number of approvals / 2 days', 'Total ' + str(total_proceedings) + ' proceedings approved since ' + args.startdate, xtickvalues, xticklabel, 20,  "approved_since_" + args.startdate.replace("/", "_"), caption_index, caption, captiondate)
else:
    if args.enddate:
        sixmonthspreviously = extract(args.enddate) + relativedelta(months=-6)
        threemonthspreviously = extract(args.enddate) + relativedelta(months=-3)
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_received, sixmonthspreviously, extract(args.enddate), captiondate)
        makeGraph('Date of receipt', 'Number of received / 2 days', 'Total ' + str(total_proceedings) + ' proceedings received between ' + str(sixmonthspreviously) + ' and ' + args.enddate.replace("/", "-"), xtickvalues, xticklabel, 20, "received_six_months_since_" + str(sixmonthspreviously).replace("/", "_"), caption_index, caption, captiondate)

        del xticklabel[:]
        del xtickvalues[:]
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_received, threemonthspreviously, extract(args.enddate), captiondate)
        makeGraph('Date of receipt', 'Number of received / 2 days', 'Total ' + str(total_proceedings) + ' proceedings received between ' + str(threemonthspreviously) + ' and ' + args.enddate.replace("/", "-"), xtickvalues, xticklabel, 20, "received_three_months_since_" + str(threemonthspreviously).replace("/", "_"), caption_index, caption, captiondate)

        del xticklabel[:]
        del xtickvalues[:]
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_approved, sixmonthspreviously, extract(args.enddate), captiondate)
        makeGraph('Date of approval', 'Number of approvals / 2 days', 'Total ' + str(total_proceedings) + ' proceedings approved between ' + str(sixmonthspreviously) + ' and ' + args.enddate.replace("/", "-"), xtickvalues, xticklabel, 20, "approved_six_months_since_" + str(sixmonthspreviously).replace("/", "_"), caption_index, caption, captiondate)

        del xticklabel[:]
        del xtickvalues[:]
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_approved, threemonthspreviously, extract(args.enddate), captiondate)
        makeGraph('Date of approval', 'Number of approvals / 2 days', 'Total ' + str(total_proceedings) + ' proceedings approved between ' + str(threemonthspreviously) + ' and ' + args.enddate.replace("/", "-"), xtickvalues, xticklabel, 20, "approved_three_months_since_" + str(threemonthspreviously).replace("/", "_"), caption_index, caption, captiondate)
    else:
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_received, sixmonthsago, today, captiondate)
        makeGraph('Date of receipt', 'Number of received / 2 days', 'Total ' + str(total_proceedings) + ' proceedings received since ' + str(sixmonthsago), xtickvalues, xticklabel, 20, "received_six_months", caption_index, caption, captiondate)

        del xticklabel[:]
        del xtickvalues[:]
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_received, threemonthsago, today, captiondate)
        makeGraph('Date of receipt', 'Number of received / 2 days', 'Total ' + str(total_proceedings) + ' proceedings received since ' + str(threemonthsago), xtickvalues, xticklabel, 20, "received_three_months", caption_index, caption, captiondate)

        del xticklabel[:]
        del xtickvalues[:]
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_approved, sixmonthsago, today, captiondate)
        makeGraph('Date of approval', 'Number of approvals / 2 days', 'Total ' + str(total_proceedings) + ' proceedings approved since ' + str(sixmonthsago), xtickvalues, xticklabel, 20, "approved_six_months", caption_index, caption, captiondate)

        del xticklabel[:]
        del xtickvalues[:]
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_approved, threemonthsago, today, captiondate)
        makeGraph('Date of approval', 'Number of approvals / 2 days', 'Total ' + str(total_proceedings) + ' proceedings approved since ' + str(threemonthsago), xtickvalues, xticklabel, 20, "approved_three_months", caption_index, caption, captiondate)

        del xticklabel[:]
        del xtickvalues[:]
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_approved, date20180301, date20190212, captiondate)
        makeGraph('Date of approval', 'Number of approvals / 2 days', 'Total ' + str(total_proceedings) + ' proceedings approved from 1-3-2018 to 12-2-2019', xtickvalues, xticklabel, 20, "approved_20180301_20190212", caption_index, caption, captiondate)

        del xticklabel[:]
        del xtickvalues[:]
        xticklabel, xtickvalues, total_proceedings, caption_index = process_datas(sorted_received, date20180301, date20190212, captiondate)
        makeGraph('Date of receipt', 'Number of received / 2 days', 'Total ' + str(total_proceedings) + ' proceedings received since 1-3-2018 to 12-2-2019', xtickvalues, xticklabel, 20, "received_20180301_20190212", caption_index, caption, captiondate)

        # if I am trying on my pc, should not try to copy on atlas website folder
        if not local: 
            copyfile("received_six_months.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/received_six_months.png")
            copyfile("received_three_months.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/received_three_months.png")
            copyfile("approved_six_months.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/approved_six_months.png")
            copyfile("approved_three_months.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/approved_three_months.png")
            copyfile("approved_20180301_20190212.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/approved_20180301_20190212.png")
            copyfile("received_20180301_20190212.png", "/afs/cern.ch/atlas/www/GROUPS/PHYSOFFICE/received_20180301_20190212.png")

try :
    logFile = open("parse_stats_errors_log.txt", "w")
    logFile.write(logError)
    logFile.close()
except :
    print "couldn't write log file properly"

