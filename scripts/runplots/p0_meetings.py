#!/usr/bin/env python2

import sys
import numpy as np

from datetime import datetime
import matplotlib as mpl
if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)
import matplotlib.pyplot as plt

import pylab

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager

QUERY = """SELECT
 ANALYSIS_SYS_MEETING.PUBLICATION_ID, 
 ANALYSIS_SYS.REF_CODE,
 ANALYSIS_SYS_MEETING.ID
FROM
ANALYSIS_SYS_MEETING,
ANALYSIS_SYS,
PUBLICATION
WHERE
TYPE = :1
AND ANALYSIS_SYS.ID = ANALYSIS_SYS_MEETING.PUBLICATION_ID
AND ANALYSIS_SYS.LEAD_GROUP = :2
AND ANALYSIS_SYS.ID = PUBLICATION.ANALYSIS_ID
AND PUBLICATION.CREATION_DATE > sysdate - 365
UNION
/*SELECT
 ANALYSIS_SYS_MEETING.PUBLICATION_ID, 
 ANALYSIS_SYS.REF_CODE,
 ANALYSIS_SYS_MEETING.ID
 --PUBLICATION.CREATION_DATE
FROM
ANALYSIS_SYS_MEETING,
ANALYSIS_SYS
WHERE
TYPE = :1
AND ANALYSIS_SYS.ID = ANALYSIS_SYS_MEETING.PUBLICATION_ID
AND ANALYSIS_SYS.LEAD_GROUP = :2
AND ANALYSIS_SYS.STATUS != 'phase0_closed'
--ORDER BY ANALYSIS_SYS.ID
UNION
*/SELECT
 ANALYSIS_SYS_MEETING.PUBLICATION_ID, 
 ANALYSIS_SYS.REF_CODE,
 ANALYSIS_SYS_MEETING.ID
FROM
ANALYSIS_SYS_MEETING,
ANALYSIS_SYS,
CONFNOTE_PUBLICATION
WHERE
TYPE = :1
AND ANALYSIS_SYS.ID = ANALYSIS_SYS_MEETING.PUBLICATION_ID
AND ANALYSIS_SYS.LEAD_GROUP = :2
AND ANALYSIS_SYS.ID = CONFNOTE_PUBLICATION.ANALYSIS_ID
AND CONFNOTE_PUBLICATION.CREATION_DATE > sysdate - 365
"""

DBM = DBManager()

# TODO: should make so that leading grups are retrieved from DB
#       for future groups to come
LEADING_GROUPS = ["BPHY", "EGAM", "EXOT", "JETM", "HIGG", "HION", "LUMI", "MUON", "SUSY", "TAUP",
                  "TOPQ", "TRIG", "DAPR", "FTAG", "IDET", "IDTR", "LARG", "MCGN", "MDET", "PERF",
                  "SOFT", "STDM", "TCAL", "RUNX", "SCTD", "TECH", "GENR", "MLFO", "LHCC", "CONF",
                  "PUBN", "TDAQ", "PMGR", "UPPH", "OTRC", "FDET", "PIXE", "ITKD", "TRTD", "MAGN",
                  "HDBS", "UPGR", "STAT", "SIMU"]

TOTAL_PRE = []
TOTAL_APP = []
TOTAL_LABELS = []

for group in LEADING_GROUPS:

    approval = DBM.execute(QUERY, ('approval_meeting', group))
    pre_approval = DBM.execute(QUERY, ('pre_approval_meeting', group))

    data = {}
    x_labels = []

    for publication in approval:
        if publication[1] in data:
            data[publication[1]] = (data[publication[1]][0], data[publication[1]][1] + 1)
        else:
            data[publication[1]] = (0, 1)
            x_labels.append(publication[1])

    for publication in pre_approval:
        if publication[1] in data:
            data[publication[1]] = (data[publication[1]][0] + 1, data[publication[1]][1])
        else:
            data[publication[1]] = (1, 0)
            x_labels.append(publication[1])

    if not data:
        continue

    pre_data = []
    approval_data = []
    for publication in x_labels:
        pre_data.append(data[publication][0])
        approval_data.append(data[publication][1])

    avg_pre_numpy = round(np.mean(np.array(pre_data)), 1)
    rms_pre_numpy = np.around(np.sqrt(np.mean(np.square(np.array(pre_data)))), decimals=1)
    avg_app_numpy = round(np.mean(np.array(approval_data)), 1)
    rms_app_numpy = np.around(np.sqrt(np.mean(np.square(np.array(approval_data)))), decimals=1)

    TOTAL_PRE.append(avg_pre_numpy)
    TOTAL_APP.append(avg_app_numpy)
    TOTAL_LABELS.append(group)

    fig = pylab.figure(figsize=(12, 8))
    ax = fig.add_subplot(111)
    pylab.title("pre approval and approval meetings for {} phase 0 analysis".format(group))
    ax.bar(np.array(range(0, len(x_labels)))-0.2, pre_data,
           width=0.3, color='g', align='center')
    ax.bar(np.array(range(0, len(x_labels)))+0.2, approval_data,
           width=0.3, color='b', align='center')
    ax.set_xticks(np.arange(len(x_labels)))
    ax.set_xticklabels(x_labels, rotation=60, size=10)
    pylab.yticks(range(0, (max(max(pre_data), max(approval_data)))+1))
    pylab.tight_layout()
    ax.legend(["pre-approval meetings: avg {} - rms {}".format(avg_pre_numpy, rms_pre_numpy),
               "approval meetings avg {} - rms {}".format(avg_app_numpy, rms_app_numpy)],
              fontsize='small')
    pylab.text(len(x_labels), int(ax.get_ylim()[1]),
               datetime.today().strftime("Last update: %d-%b-%Y"),
               color="#000000", fontsize=6, verticalalignment='bottom',
               horizontalalignment='right', rotation=0)

    pylab.xlim([-1, len(x_labels)])
    pylab.savefig('p0_meetings_{}.png'.format(group))
    pylab.savefig('p0_meetings_{}.pdf'.format(group))


avg_pre_numpy = round(np.mean(np.array(TOTAL_PRE)), 1)
rms_pre_numpy = np.around(np.sqrt(np.mean(np.square(np.array(TOTAL_PRE)))), decimals=1)
avg_app_numpy = round(np.mean(np.array(TOTAL_APP)), 1)
rms_app_numpy = np.around(np.sqrt(np.mean(np.square(np.array(TOTAL_APP)))), decimals=1)

fig_t = pylab.figure(figsize=(12, 8))
ax_t = fig_t.add_subplot(111)
pylab.title("leading group average of pre approval and approval meetings for phase 0 analysis")
ax_t.bar(np.array(range(0, len(TOTAL_LABELS)))-0.2, TOTAL_PRE, width=0.3, color='g', align='center')
ax_t.bar(np.array(range(0, len(TOTAL_LABELS)))+0.2, TOTAL_APP, width=0.3, color='b', align='center')
ax_t.set_xticks(np.arange(len(TOTAL_LABELS)))
ax_t.set_xticklabels(TOTAL_LABELS, rotation=60, size=10)
ax_t.legend(["pre-approval meetings: avg {} - rms {}".format(avg_pre_numpy, rms_pre_numpy),
             "approval meetings avg {} - rms {}".format(avg_app_numpy, rms_app_numpy)],
            fontsize='small')
pylab.yticks(range(0, int((max(max(TOTAL_PRE), max(TOTAL_APP))))+1))
pylab.tight_layout()
pylab.text(len(TOTAL_LABELS),
           ax_t.get_ylim()[1],
           datetime.today().strftime("Last update: %d-%b-%Y"),
           color="#000000",
           fontsize=6,
           verticalalignment='bottom',
           horizontalalignment='right',
           rotation=0)

pylab.xlim([-1, len(TOTAL_LABELS)])
pylab.savefig('p0_meetings.png')
pylab.savefig('p0_meetings.pdf')
