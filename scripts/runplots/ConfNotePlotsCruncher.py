# -*- coding: utf-8 -*-
"""
#########################################################################################
# ConfNotePlotsCruncher.py
# Make useful? confnote statistics
# Written by Marcello Barisonzi, Bergische Universitatet Wuppertal, February 7th, 2013
#########################################################################################
# $Rev: 242031 $
# $LastChangedDate: 2016-04-15 16:43:02 +0200 (Fri, 15 Apr 2016) $
# $LastChangedBy: atlaspo $
#########################################################################################
"""

from datetime import datetime
import pkg_resources
pkg_resources.require("Matplotlib==1.2.0")

import matplotlib as mpl

from PyPlotCruncher import PyPlotCruncher

if mpl.get_backend() != "agg":
    mpl.use('Agg', warn=False)

class ConfNotePlotsCruncher(PyPlotCruncher):

    def __init__(self, run=None):
        PyPlotCruncher.__init__(self, run=run)

        self._exclusionList = []

        return

    def fillDict(self):

        parseString = "%Y/%m/%d"

        if self._currDct["Final Sign Off"] == '':
            return

        d = datetime.strptime(self._currDct["Final Sign Off"], parseString)

        key = d.strftime("%Y-%m")

        if not key:
            assert False, key

        if not self._monthDict.has_key(key):
            self._monthDict[key] = {}

        grp = self._currDct["Lead Group"]

        # merge all perf groups into one:
        if grp in ["JETM","EGAM","DAPR","IDTR","PERF","TRIG","MUON","FTAG","TAUP"]:
            grp = "PERF"

        try:
            self._zeroGroups.remove(grp)
        except:
            pass

        if not self._monthDict[key].has_key(grp):
            self._monthDict[key][grp] = 1
        else:
            self._monthDict[key][grp] += 1

        if not self._monthDict[key].has_key("Total"):
            self._monthDict[key]["Total"] = 1
        else:
            self._monthDict[key]["Total"] += 1

        if not self._allDict.has_key(grp):
            self._allDict[grp] = 1
        else:
            self._allDict[grp] += 1


        _ft = self._currDct["Full Title"].lower()

        if grp in ["PERF","JETM","EGAM","DAPR","IDTR","PERF","TRIG","MUON","FTAG","TAUP"]:
            # print "Performance", self._currDct["Full Title"]
            _categ = "Performance"
        elif "measur" in _ft or "observ" in _ft or "study" in _ft or "character" in _ft or "extraction" in _ft or "density" in _ft or "distribution" in _ft or "structure" in _ft or "determination" in _ft or "studies" in _ft:
            # print "Measure", self._currDct["Full Title"]
            _categ = "Measure" 
        elif "search" in _ft or "hunt" in _ft or "evidence" in _ft or "limit" in _ft or grp in ["HIGG","SUSY","EXOT"]:
            # print "Search", self._currDct["Full Title"]
            _categ = "Search"
        elif "performance" in _ft or "resolution" in _ft or "luminosity" in _ft or "calibrati" in _ft or "commission" in _ft or "reconstr" in _ft or "algorithm" in _ft:
            # print "Performance", self._currDct["Full Title"]
            _categ = "Performance"              
        else:
            # print "Other", self._currDct["Ref Code"], self._currDct["Full Title"]
            _categ = "Ambiguous"             

        if not self._categDict[_categ].has_key(key):
            self._categDict[_categ][key] = 1
        else:
            self._categDict[_categ][key] += 1

        # set zero to all others
        for k,v in self._categDict.iteritems():
            if k != _categ and not v.has_key(key):
                self._categDict[k][key] = 0
        
        self._categDict[_categ]["Total"] += 1

        return

    def doFormatting(self):


        for r in self._Reader:
            self._currDct = r
            self.fillDict()


        # Monthly chart

        labels = sorted(self._monthDict.keys())

        bars = [self._monthDict[i]["Total"] for i in labels]

        outfile = "CN_months.png"
        if self._run:
            outfile = "CN_months_run%i.png" % self._run
        self.createMonthlyBar(bars, labels, title="ATLAS ConfNotes/Month", outfile=outfile)

        # remove zero entry groups, sort from lowest to highest
        groups = [ i for i in self._allGroups if not i in self._zeroGroups ]

        labels = [i for i in groups if not i in self._zeroGroups]

        labels.sort(key=lambda x: self._allDict[x], reverse=True) 

        fracs = [self._allDict[i] for i in labels]

        outfile = "CN_groups.png"
        if self._run:
            outfile = "CN_groups_run%i.png" % self._run
        self.createPieChart(fracs, labels, title="ATLAS - ConfNotes/Lead-group", outfile=outfile)

        labels = sorted([i for i in self._categDict.keys() if self._categDict[i]["Total"]], key=lambda x: self._categDict[x]["Total"], reverse=True)

        fracs = [self._categDict[i]["Total"] for i in labels]

        outfile = "CN_type.png"
        if self._run:
            outfile = "CN_type_run%i.png" % self._run
        self.createPieChart(fracs, labels, title="ATLAS - Type of ConfNote", outfile=outfile)

        allFracs   = []
        allLabels  = []
        allLegends = []

        for i in self._categDict.keys(): #["Search","Measure","Performance","Other"]:
            if not self._categDict[i]["Total"]:
                continue

            allLegends.append(i)

            l = sorted([j for j in self._categDict[i].keys() if j != "Total"])

            allLabels.append(l)
            allFracs.append([self._categDict[i][j] for j in l])

        outfile = "CN_months_stacked.png"
        if self._run:
            outfile = "CN_months_stacked_run%i.png" % self._run
        self.createMonthlyStackedBar(allFracs, allLabels, allLegends, title="ATLAS - Type of ConfNote", outfile=outfile, loc=1)

        # unique months
        monthsSet = set(self._monthDict.keys())

        labels = sorted(monthsSet)

        dataAtlas = []

        for i,j in enumerate(labels):

            if self._monthDict.has_key(j):
                _dA = self._monthDict[j]["Total"]
            else:
                _dA = 0
                
            if i == 0:
                dataAtlas.append(_dA)
            else:
                dataAtlas.append(_dA+dataAtlas[i-1])

        outfile = "CN_atlas_progress.png"
        if self._run:
            outfile = "CN_atlas_progress_run%i.png" % self._run
        self.createArmsRace(dataAtlas, None, labels, title="ATLAS Public ConfNotes", outfile=outfile)
            
        return


            
    def processCSV(self):

        self.setReader()

        self.doFormatting()

        return
    
