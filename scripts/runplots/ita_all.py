#!/usr/bin/env python2.7

import os
import traceback

from PyPlotCruncher_collision_ita_all import PyPlotCruncher

bf = PyPlotCruncher()
try:
    bf.setInfile("/afs/cern.ch/user/a/atlaspo/twikirun/csv6649/table6649_ita_all.csv")
    bf.processCSV()
except:
    print ("****** >> %s" % (traceback.format_exc()))
    print "Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6649/table6649.csv"
    os.system("echo 'Error trying to load /afs/cern.ch/user/a/atlaspo/twikirun/csv6649/table6649.csv' | mail -s 'Error in plotcruncher_collision.py' mcolautt ")
