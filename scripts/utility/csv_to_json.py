'''
    This library convert an existing csv file
    passed as argument into a JSON file and store
    it into the path:
        /eos/home-a/atlaspo/input/json

    parameters:
        1 - the csv (entire path)to convert
        2 - the json output name (optional, if it is empty the csv name is used)

    example:
        csv_to_json.py /afs/cern.ch/user/a/atlaspo/twikirun/csv6688/table6688.csv PhysicsAnalysisGroupsConveners.json
    developed by Gianluca Picco <gianluca.picco@cern.ch>
'''

import sys
import os

import config

if len(sys.argv) > 1:
    if os.path.isfile(sys.argv[1]):
        output_dir = config.getInputPath()+'/json/'
        # check for output file_name
        if len(sys.argv) > 2:
            output_name = sys.argv[2]
            if ".json" not in output_name:
                output_name += ".json"
        else:
            output_name = sys.argv[1].split('/')[-1].replace(".csv",".json")

        output_dir += output_name

        output = '{"list":[\n'
        print "Converting " + sys.argv[1] + " into JSON: " + output_dir

        header = True
        headers = []

        file = open(sys.argv[1], "r")

        for line in file:
            line = line.replace('\r','').replace('\n','')
            if header:
                headers = line.replace('"','').split(',')
                header = False
            else:
                content = line.split('","')
                output += '{'
                for i in range(0,len(content)):
                    output += '"' + headers[i] + '": "' + content[i].replace('"','') + '", '
                output = output[:-2] + "}, \n"
        output = output[:-3]+']}'
        file.close()

        #Writing the converted JSON into the output_dir
        output_file = open(output_dir, "w")
        output_file.write(output)
        output_file.close()

    else:
        print "File " + sys.argv[1] + " does not exists"
else:
    print "Function require at least one argument"