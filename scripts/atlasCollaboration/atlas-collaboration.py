#!/usr/bin/env python
"""
The script gathers information for the webpage: https://atlas-collaboration.web.cern.ch/
author: Maurizio Colautti for ATLAS/CERN. maurizio.colautti@cern.ch
will need to be updated when ATLAS PO db will be remodeled
"""

from PaperCollaboration import PaperCollaboration
from PubnoteCollaboration import PubnoteCollaboration
from ConfnoteCollaboration import ConfnoteCollaboration


#RUN = PaperCollaboration()
#RUN.runpaper()
RUN = ConfnoteCollaboration()
RUN.runconfnote()
#RUN = PubnoteCollaboration()
#RUN.runpubnote()


"""
TODO:
pubnote filter by STATUS != 'not_started'
"""
