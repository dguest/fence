"""
classes to be used by atlas-collaboration
"""

import datetime
from datetime import timedelta
import sys
import json

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager

class PaperDict(object):
    """
    handles a paper entry
    """

    FIRST_CIRCULATION = '1^st^ circulation'
    SECOND_CIRCULATION = '2^nd^ circulation'
    PUBLIC = 'public'

    def __init__(self):
        self.id = None
        self.ref_code = None
        self.short_title = None
        self.lead_group = None
        self.status = None
        self.official_status = None
        self.type = 'paper'
        self.url = None
        self.journal_sub = None
        self.cds_link = None

    def elaborate(self):
        """
        assigns values from other properties
        """
        self.url = 'https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS' + self.ref_code.upper()

        if self.status == 'created' or self.status == 'phase1_active':
            self.official_status = PaperDict.FIRST_CIRCULATION
        elif self.status == 'phase3_active' or self.status == 'submission_active':
            self.official_status = PaperDict.SECOND_CIRCULATION
        elif self.status == 'submission_closed':
            self.official_status = PaperDict.PUBLIC

    def make_dict(self):
        """
        builds a dictionary from the object
        """
        dictionary = {}
        dictionary['glance_id'] = self.id
        dictionary['glance'] = "https://glance.cern.ch/atlas/analysis/papers/details.php?id={}".format(self.id)
        dictionary['figures'] = "https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/{}".format(self.ref_code)
        dictionary['id'] = self.ref_code
        dictionary['title'] = self.short_title
        dictionary['category'] = self.lead_group
        dictionary['type'] = self.type
        dictionary['status'] = self.official_status
        dictionary['cds'] = self.cds_link
        if self.journal_sub:
            if isinstance(self.journal_sub, datetime.datetime):
                self.journal_sub = self.journal_sub.strftime("%Y-%m-%d")
            dictionary['published'] = self.journal_sub
        else:
            dictionary['published'] = False
        return dictionary

    def __str__(self):
        return ("id = {}\n  ref code = {}\n  short title = {}\n" \
                + "  lead_group = {}\n  status = {}\n  published = {}").format(
                    self.id,
                    self.ref_code,
                    self.short_title,
                    self.lead_group,
                    self.status,
                    self.journal_sub
                    )

class PaperCollaboration(object):
    """
    Paper composed for atlas collaboration
    """
    N_MAX = 20

    def __init__(self):

        self.dbm = DBManager()
        self.papers = []
        self.arxiv_links = {}
        self.phase1_cds = {}
        self.phase2_cds = {}
        self.public_cds = {}
        self.first_circulation = {}
        self.second_cirulation = {}
        self.papers_export = {}

    def __del__(self):
        try:
            self.dbm.close()
        except:
            pass

    def gather_all_papers(self, demo=False):
        """
        retrieve all the papers
        """
        if demo:
            paper_query = """SELECT
                             ID,
                             REF_CODE,
                             SHORT_TITLE,
                             LEAD_GROUP,
                             STATUS,
                             TO_CHAR(JOURNAL_SUB, 'YYYY-MM-DD')
                             FROM
                             PUBLICATION
                             WHERE 
                             ROWNUM <= 10
                             AND
                             DELETED = 0
                             """
        else:
            paper_query = """SELECT
                             ID,
                             REF_CODE,
                             SHORT_TITLE,
                             LEAD_GROUP,
                             STATUS,
                             TO_CHAR(JOURNAL_SUB, 'YYYY-MM-DD')
                             FROM
                             PUBLICATION
                             WHERE 
                             DELETED = 0
                             """


        papers = self.dbm.execute(paper_query)

        for paper in papers:
            item = PaperDict()
            (item.id, item.ref_code, item.short_title,
             item.lead_group, item.status, item.journal_sub) = paper
            item.elaborate()

            self.papers.append(item.make_dict())

    def gather_arxiv(self):
        """
        retrieve all arxiv links in a dictionary
        """
        arxiv_query = """SELECT
                            PHASE.PUBLICATION_ID,
                            PHASE_LINKS.HREF
                         FROM
                            PHASE_LINKS,
                            PHASE
                         WHERE 
                            HREF LIKE '%arxiv%'
                         AND
                            PHASE_LINKS.PHASE_ID = PHASE.ID
                      """

        arxivs = self.dbm.execute(arxiv_query)

        for element in arxivs:
            self.arxiv_links[element[0]] = element[1]

    def gather_phase_cds(self, phase):
        """
        retrieve all cds phase1 links
        """
        dictionary = {}
        cds_query = """SELECT
                        PHASE.PUBLICATION_ID,
                        PHASE_LINKS.HREF
                     FROM
                        PHASE_LINKS,
                        PHASE
                     WHERE 
                        HREF LIKE '%cds%'
                     AND
                        PHASE_LINKS.PHASE_ID = PHASE.ID
                     AND
                        PHASE.PHASE = {}
                    """.format(phase)

        cds = self.dbm.execute(cds_query)

        for element in cds:
            dictionary[element[0]] = element[1]

        return dictionary

    def gather_public_cds(self):
        """
        retrive all cds for public papers
        """
        cds_query = """SELECT
                            PUBLICATION_ID,
                            CDS_ID
                        FROM
                            CDS_DATA
                        WHERE
                            TYPE = 'paper'
                    """

        cds = self.dbm.execute(cds_query)

        for element in cds:
            self.public_cds[element[0]] = element[1]

    def gather_reviewers_sign_off(self):
        """
        retrieve first circulation date
        """

        reviewers_query = """SELECT
                              PUBLICATION.ID,
                              PHASE_1.REVIEWERS_SIGN_OFF
                             FROM
                              PUBLICATION,
                              PHASE,
                              PHASE_1
                             WHERE 
                              PUBLICATION.ID = PHASE.PUBLICATION_ID
                             AND
                              PHASE.ID = PHASE_1.PHASE_ID 
                             AND
                              PHASE_1.REVIEWERS_SIGN_OFF IS NOT NULL
                          """

        first_circulation = self.dbm.execute(reviewers_query)

        for element in first_circulation:
            self.first_circulation[element[0]] = element[1]

    def gather_cern_sign_off(self):
        """
        retrieve 2nd circulation date
        """

        cern_query = """SELECT
                         PUBLICATION.ID,
                         PHASE_3.CERN_SIGN_OFF
                        FROM
                         PUBLICATION,
                         PHASE,
                         PHASE_3
                        WHERE 
                         PUBLICATION.ID = PHASE.PUBLICATION_ID
                        AND
                         PHASE.ID = PHASE_3.PHASE_ID 
                        AND
                         PHASE_3.CERN_SIGN_OFF IS NOT NULL
                        """

        second_cirulation = self.dbm.execute(cern_query)

        for element in second_cirulation:
            self.second_cirulation[element[0]] = element[1]

    def assign_cds(self, paper):
        """
        depending on phase, assigns the best cds entry
        """
        base_path = "https://cds.cern.ch/record/"
        glance_id = paper["glance_id"]
        if paper["status"] == PaperDict.FIRST_CIRCULATION:
            if glance_id in self.phase1_cds:
                paper["cds"] = base_path + str(self.phase1_cds[glance_id])
        elif paper["status"] == PaperDict.SECOND_CIRCULATION:
            if glance_id in self.phase2_cds:
                paper["cds"] = base_path + str(self.phase1_cds[glance_id])
        elif paper["status"] == PaperDict.PUBLIC:
            if glance_id in self.public_cds:
                paper["cds"] = base_path + str(self.public_cds[glance_id])

    def assign_sort_date(self, paper):
        """
        all entries to have an official date to sort through
        """
        glance_id = paper["glance_id"]
        if paper["status"] == PaperDict.FIRST_CIRCULATION:
            if glance_id in self.first_circulation:
                if isinstance(self.first_circulation[glance_id], datetime.datetime):
                    self.first_circulation[glance_id] = self.first_circulation[glance_id].strftime("%Y-%m-%d")
                paper["date"] = self.first_circulation[glance_id]
        elif paper["status"] == PaperDict.SECOND_CIRCULATION:
            if glance_id in self.second_cirulation:
                if isinstance(self.second_cirulation[glance_id], datetime.datetime):
                    self.second_cirulation[glance_id] = self.second_cirulation[glance_id].strftime("%Y-%m-%d")
                paper["date"] = self.second_cirulation[glance_id]
        elif paper["status"] == PaperDict.PUBLIC:
            paper["date"] = paper["published"]

    def assign_arxiv(self, paper):
        """
        assigns arxiv to the publications which have it
        """
        glance_id = paper["glance_id"]
        if glance_id in self.arxiv_links:
            paper["arxiv"] = self.arxiv_links[glance_id]
            paper["status"] = PaperDict.PUBLIC
        else:
            paper["arxiv"] = ""
            paper["figures"] = ""

    def merge(self):
        """
        matches values gathered from different queries
        """
        for paper in self.papers:
            self.assign_cds(paper)
            self.assign_arxiv(paper)
            self.assign_sort_date(paper)


    def remove_empty_date_papers(self):
        """
        remove all papers which don't really have a date set
        """
        for paper in reversed(self.papers):
            if "date" not in paper:
                self.papers.remove(paper)

    def sort_by_date(self):
        """
        sorts the list of items by date
        """
        try:
            self.papers.sort(key=lambda x: x["date"], reverse=True)
        except (TypeError, KeyError):
            print("Error in sorting papers")
            raise

    def split(self):
        """
        splits into categories:
        1^st circulation,
        2^nd circulation,
        published
        """
        first_circulation = []
        second_circulation = []
        published = []
        published_count = 0
        for paper in self.papers:
            if paper['status'] == PaperDict.FIRST_CIRCULATION:
                first_circulation.append(paper)
            elif paper['status'] == PaperDict.SECOND_CIRCULATION:
                second_circulation.append(paper)
            elif paper['status'] == PaperDict.PUBLIC:

                if published_count >= PaperCollaboration.N_MAX:
                    continue
                pap_date = datetime.datetime.strptime(paper['date'], '%Y-%m-%d')
                if pap_date < datetime.datetime.today() - timedelta(days=14):
                    continue
                published.append(paper)
                published_count += 1

        all_papers = {}
        all_papers['first_circulation'] = first_circulation
        all_papers['second_circulation'] = second_circulation
        all_papers['published'] = published

        self.papers_export = all_papers


    def save(self, namefile, pretty=False):
        """
        saves in json file the list of dictionary
        """
        with open(namefile, "w+") as out_file:
            if pretty:
                json.dump(self.papers_export, out_file, indent=4, default=str)
            else:
                json.dump(self.papers_export, out_file, default=str)


    def runpaper(self):
        """
        runs all the functions needed
        """
        self.gather_all_papers(demo=False)
        self.gather_arxiv()
        self.phase1_cds = self.gather_phase_cds(1)
        self.phase2_cds = self.gather_phase_cds(3)
        self.gather_public_cds()
        self.gather_reviewers_sign_off()
        self.gather_cern_sign_off()
        self.merge()

        self.remove_empty_date_papers()
        self.sort_by_date()
        self.split()
        self.save("papers.json", pretty=True)
