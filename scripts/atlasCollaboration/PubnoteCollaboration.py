"""
classes to be used by atlas-collaboration
"""

import datetime
import sys
import json

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager

class PubnoteDict(object):
    """
    handles a pubnote entry
    # TODO: make pubnote also inherit from a publication class
    """

    FIRST_CIRCULATION = '1^st^ circulation'
    PUBLIC = 'public'

    def __init__(self):
        self.id = None
        self.ref_code = None
        self.temp_ref_code = None
        self.short_title = None
        self.lead_group = None
        self.status = None
        self.official_status = None
        self.type = 'pubnote'
        self.cds_link = None

    def elaborate(self):
        """
        assigns values from other properties
        """
        if self.status == 'phase1_active':
            self.official_status = PubnoteDict.FIRST_CIRCULATION
        elif self.status == 'phase1_closed':
            self.official_status = PubnoteDict.PUBLIC

    def make_dict(self):
        """
        build a dictionary from the object
        """
        dictionary = {}
        dictionary['glance_id'] = self.id
        dictionary['glance'] = "https://glance.cern.ch/atlas/analysis/pubnotes/details.php?id={}".format(self.id)
        if self.ref_code:
            dictionary['id'] = self.ref_code
            dictionary['figures'] = "https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/{}".format(self.ref_code)
        else:
            dictionary['id'] = self.temp_ref_code
        dictionary['title'] = self.short_title
        dictionary['category'] = self.lead_group
        dictionary['type'] = self.type
        dictionary['status'] = self.official_status
        dictionary['cds'] = self.cds_link

        return dictionary



class PubnoteCollaboration(object):
    """
    handles pubnotes operations and query
    """

    def __init__(self):

        self.dbm = DBManager()
        self.pubnotes = []
        self.draft_cds_url = {}
        self.public_cds = {}
        # TODO: verify column name
        self.draft_sign_off = {}
        # TODO: verify column name
        self.atlas_approval = {}

    def gather_all_pubnotes(self, demo=False):
        """
        retrieve all pubnotes
        """
        if demo:
            pubnote_query = """SELECT
                                ID,
                                FINAL_REF_CODE,
                                SHORT_TITLE,
                                LEAD_GROUP,
                                STATUS,
                                TEMP_REF_CODE
                                FROM
                                PUBNOTE_PUBLICATION
                                WHERE
                                ROWNUM <= 10
                                AND
                                DELETED = 0
                                AND
                                STATUS != 'created'
                            """
        else:
            pubnote_query = """SELECT
                                ID,
                                FINAL_REF_CODE,
                                SHORT_TITLE,
                                LEAD_GROUP,
                                STATUS,
                                TEMP_REF_CODE
                                FROM
                                PUBNOTE_PUBLICATION
                                WHERE
                                DELETED = 0
                                AND
                                STATUS != 'created'
                            """

        pubnotes = self.dbm.execute(pubnote_query)

        for pubnote in pubnotes:
            item = PubnoteDict()
            (item.id, item.ref_code, item.short_title,
             item.lead_group, item.status, item.temp_ref_code) = pubnote
            item.elaborate()

            self.pubnotes.append(item.make_dict())

    def gather_cds_draft(self):
        """
        retrieves all the pubnote cds draft links
        """
        cds_url_draft_query = """
                            SELECT
                             PUBNOTE_PUBLICATION.ID,
                             PUBNOTE_PHASE_1.DRAFT_CDS_URL
                            FROM
                             PUBNOTE_PUBLICATION,
                             PUBNOTE_PHASE,
                             PUBNOTE_PHASE_1
                            WHERE
                             PUBNOTE_PUBLICATION.ID = PUBNOTE_PHASE.PUBLICATION_ID
                            AND
                             PUBNOTE_PHASE.ID = PUBNOTE_PHASE_1.PHASE_ID
                            AND
                             PUBNOTE_PHASE_1.DRAFT_CDS_URL IS NOT NULL
                             """

        cds_url_drafts = self.dbm.execute(cds_url_draft_query)

        for cds_url_draft in cds_url_drafts:
            self.draft_cds_url[cds_url_draft[0]] = cds_url_draft[1]

    def gather_public_cds(self):
        """
        retrive all cds for public confnotes
        """
        cds_query = """SELECT
                            PUBLICATION_ID,
                            CDS_ID
                        FROM
                            CDS_DATA
                        WHERE
                            TYPE = 'pubnote'
                    """

        cds = self.dbm.execute(cds_query)

        for element in cds:
            self.public_cds[element[0]] = element[1]

    def gather_draft_and_approval(self):
        """
        collects data of draft sign off and atlas approval
        """
        dates_query = """
                        SELECT
                          PUBNOTE_PUBLICATION.ID,
                          TO_CHAR(PUBNOTE_PHASE_1.SIGN_OFF_DRAFT, 'YYYY-MM-DD'),
                          TO_CHAR(PUBNOTE_PHASE_1.APPROVAL_DATE, 'YYYY-MM-DD')
                        FROM
                          PUBNOTE_PUBLICATION,
                          PUBNOTE_PHASE,
                          PUBNOTE_PHASE_1
                        WHERE
                          PUBNOTE_PUBLICATION.ID = PUBNOTE_PHASE.PUBLICATION_ID
                        AND
                          PUBNOTE_PHASE.ID = PUBNOTE_PHASE_1.PHASE_ID
                        AND
                          PUBNOTE_PUBLICATION.DELETED = 0
                        """

        dates = self.dbm.execute(dates_query)

        for date in dates:
            # TODO: check because first pubnotes don't have some dates 
            #       even if completed, maybe change in workflow
            if date[1]:
                self.draft_sign_off[date[0]] = date[1]
            if date[2]:
                self.atlas_approval[date[0]] = date[2]

    def assign_date(self, pubnote):
        """
        if approval exists, sets to approval
        if not, if sign off exists, set to sign off
        if not, skip, it will be deleted later
        """
        glance_id = pubnote['glance_id']
        if glance_id in self.atlas_approval:
            pubnote["date"] = self.atlas_approval[glance_id]
        elif glance_id in self.draft_sign_off:
            pubnote["date"] = self.draft_sign_off[glance_id]

    def assign_cds(self, pubnote):
        """
        if in 1^st circulation set draft,
        if public, set final cds entry
        """
        base_path = "https://cds.cern.ch/record/"
        glance_id = pubnote["glance_id"]
        if pubnote["status"] == PubnoteDict.FIRST_CIRCULATION:
            if glance_id in self.draft_cds_url:
                pubnote["cds"] = str(self.draft_cds_url[glance_id])
        elif pubnote["status"] == PubnoteDict.PUBLIC:
            if glance_id in self.public_cds:
                pubnote["cds"] = base_path + str(self.public_cds[glance_id])

    def remove_empty_date_pubnotes(self):
        """
        remove all confnotes which don't have a date set
        """
        for pubnote in reversed(self.pubnotes):
            if "date" not in pubnote:
                self.pubnotes.remove(pubnote)

    def sort_by_date(self):
        """
        sorts the list of items by date
        """
        try:
            self.pubnotes.sort(key=lambda x: x["date"], reverse=True)
        except (TypeError, KeyError):
            print("Error in sorting pubnotes")
            raise

    def save(self, namefile, pretty=False):
        """
        saves in json file the list of dictionary
        """
        with open(namefile, "w+") as out_file:
            if pretty:
                json.dump(self.pubnotes, out_file, indent=4, default=str)
            else:
                json.dump(self.pubnotes, out_file, default=str)

    def merge(self):
        """
        matches values gathered from different queries
        """
        for pubnote in self.pubnotes:
            self.assign_cds(pubnote)
            self.assign_date(pubnote)

    def report(self):
        """
        outputs pubnotes loaded
        """
        for pubnote in self.pubnotes:
            print(pubnote)

    def runpubnote(self):
        """
        runs all the functions needed
        """
        self.gather_all_pubnotes(demo=False)
        self.gather_cds_draft()
        self.gather_public_cds()
        self.gather_draft_and_approval()
        self.merge()

        self.remove_empty_date_pubnotes()
        self.sort_by_date()
        self.save("pubnotes.json", pretty=True)
        #self.report()
