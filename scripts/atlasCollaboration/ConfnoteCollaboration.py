"""
classes to be used by atlas-collaboration
"""

import datetime
from datetime import timedelta
import sys
import json

sys.path.insert(0, '/afs/cern.ch/user/a/atlaspo/po-scripts/utilities')
from DBManager import DBManager

class ConfnoteDict(object):
    """
    handles a confnote entry
    # TODO: make confnote and paper inherit from a publication class
    """

    FIRST_CIRCULATION = '1^st circulation'
    PUBLIC = 'public'

    def __init__(self):
        self.id = None
        self.ref_code = None
        self.temp_ref_code = None
        self.short_title = None
        self.lead_group = None
        self.status = None
        self.official_status = None
        self.type = 'confnote'
        self.cds_link = None
        self.draft_sign_off = None
        self.cds_sub = None

    def elaborate(self):
        """
        assigns values from other properties
        """
        # TODO: maybe it's not really like this
        #       maybe it's based on sign off / atlas approval?
        if self.status == 'created' or self.status == 'phase1_active':
            self.official_status = ConfnoteDict.FIRST_CIRCULATION
        else:
            self.official_status = ConfnoteDict.PUBLIC


    def make_dict(self):
        """
        builds a dictionary from the object
        """
        dictionary = {}
        dictionary['glance_id'] = self.id
        dictionary['glance'] = "https://glance.cern.ch/atlas/analysis/confnotes/details.php?id={}".format(self.id)
        if self.ref_code:
            dictionary['id'] = self.ref_code
            dictionary['figures'] = "https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/{}".format(self.ref_code)
        else:
            dictionary['id'] = self.temp_ref_code
        dictionary['title'] = self.short_title
        dictionary['category'] = self.lead_group
        dictionary['type'] = self.type
        dictionary['status'] = self.official_status
        dictionary['cds'] = self.cds_link

        return dictionary


class ConfnoteCollaboration(object):
    """
    Confnote composed for atlas collaboration
    """

    N_MAX = 20

    def __init__(self):

        self.dbm = DBManager()
        self.confnotes = []
        self.draft_cds_url = {}
        self.public_cds = {}
        self.draft_sign_off = {}
        self.atlas_approval = {}
        self.confnotes_export = {}

    def gather_all_confnotes(self, demo=False):
        """
        retrieve the confnotes
        """
        if demo:
            confnote_query = """
                                SELECT
                                ID,
                                REF_CODE,
                                SHORT_TITLE,
                                LEAD_GROUP,
                                STATUS,
                                TEMP_REF_CODE
                                FROM
                                CONFNOTE_PUBLICATION
                                WHERE
                                ROWNUM <= 10
                                AND
                                DELETED = 0
                                """
        else:
            confnote_query = """
                                SELECT
                                ID,
                                REF_CODE,
                                SHORT_TITLE,
                                LEAD_GROUP,
                                STATUS,
                                TEMP_REF_CODE
                                FROM
                                CONFNOTE_PUBLICATION
                                WHERE
                                DELETED = 0
                                """

        confnotes = self.dbm.execute(confnote_query)

        for confnote in confnotes:
            item = ConfnoteDict()
            (item.id, item.ref_code, item.short_title,
             item.lead_group, item.status, item.temp_ref_code) = confnote
            item.elaborate()

            self.confnotes.append(item.make_dict())

    def gather_cds_draft(self):
        """
        retrieves all the confnote cds draft links
        """
        cds_url_draft_query = """
                                SELECT
                                 CONFNOTE_PUBLICATION.ID,
                                 CONFNOTE_PHASE_1.DRAFT_CDS_URL
                                FROM
                                 CONFNOTE_PUBLICATION,
                                 CONFNOTE_PHASE,
                                 CONFNOTE_PHASE_1
                                WHERE
                                 CONFNOTE_PUBLICATION.ID = CONFNOTE_PHASE.PUBLICATION_ID
                                AND
                                 CONFNOTE_PHASE.ID = CONFNOTE_PHASE_1.PHASE_ID
                                AND
                                 CONFNOTE_PHASE_1.DRAFT_CDS_URL IS NOT NULL
                              """

        cds_url_drafts = self.dbm.execute(cds_url_draft_query)

        for cds_url_draft in cds_url_drafts:
            self.draft_cds_url[cds_url_draft[0]] = cds_url_draft[1]

    def gather_public_cds(self):
        """
        retrive all cds for public confnotes
        """
        cds_query = """SELECT
                            PUBLICATION_ID,
                            CDS_ID
                        FROM
                            CDS_DATA
                        WHERE
                            TYPE = 'confnote'
                    """

        cds = self.dbm.execute(cds_query)

        for element in cds:
            self.public_cds[element[0]] = element[1]

    def gather_draft_and_approval(self):
        """
        collects data of draft sign off and atlas approval
        """
        dates_query = """SELECT
                          CONFNOTE_PUBLICATION.ID,
                          TO_CHAR(CONFNOTE_PHASE_1.SIGN_OFF_DRAFT, 'YYYY-MM-DD'),
                          TO_CHAR(CONFNOTE_PHASE_1.ATLAS_APPROVAL, 'YYYY-MM-DD')
                        FROM
                          CONFNOTE_PUBLICATION,
                          CONFNOTE_PHASE,
                          CONFNOTE_PHASE_1
                        WHERE
                          CONFNOTE_PUBLICATION.ID = CONFNOTE_PHASE.PUBLICATION_ID
                        AND
                          CONFNOTE_PHASE.ID = CONFNOTE_PHASE_1.PHASE_ID
                        AND
                          CONFNOTE_PUBLICATION.DELETED = 0
                       """

        dates = self.dbm.execute(dates_query)

        for date in dates:
            self.draft_sign_off[date[0]] = date[1]
            self.atlas_approval[date[0]] = date[2]

    def assign_cds(self, confnote):
        """
        if in 1^st circulation set draft,
        if public, set final cds entry
        """
        base_path = "https://cds.cern.ch/record/"
        glance_id = confnote["glance_id"]
        if confnote["status"] == ConfnoteDict.FIRST_CIRCULATION:
            if glance_id in self.draft_cds_url:
                confnote["cds"] = str(self.draft_cds_url[glance_id])
        elif confnote["status"] == ConfnoteDict.PUBLIC:
            if glance_id in self.public_cds:
                confnote["cds"] = base_path + str(self.public_cds[glance_id])

    def assign_date(self, confnote):
        """
        if approval exists, sets to approval
        if doesn't exists, if exists, set to sign_off_draft
        if doesn't exists, skip. it will be deleted later in remove_empty_date
        """
        glance_id = confnote["glance_id"]
        if glance_id in self.atlas_approval:
            confnote["date"] = self.atlas_approval[glance_id]
        elif glance_id in self.draft_sign_off:
            confnote["date"] = self.draft_sign_off[glance_id]
        if "date" not in confnote or confnote["date"] is None:
            print("confnote with no date = {}".format(confnote))

    def merge(self):
        """
        maches values gathered from different queries
        """
        for confnote in self.confnotes:
            self.assign_cds(confnote)
            self.assign_date(confnote)

    def remove_empty_date_confnotes(self):
        """
        remove all confnotes which don't have a date set
        """
        for confnote in reversed(self.confnotes):
            if "date" not in confnote or confnote["date"] is None:
                self.confnotes.remove(confnote)

    def split(self):
        """
        splits into categories
        1^st circulation
        published
        """
        first_circulation = []
        published = []
        published_count = 0
        for confnote in self.confnotes:
            #print(confnote['status'])
            if confnote['status'] == ConfnoteDict.FIRST_CIRCULATION:
                first_circulation.append(confnote)
            elif confnote['status'] == ConfnoteDict.PUBLIC:

                if published_count >= ConfnoteCollaboration.N_MAX:
                    continue
                conf_date = datetime.datetime.strptime(confnote['date'], '%Y-%m-%d')
                if conf_date < datetime.datetime.today() - timedelta(days=14):
                    continue
                published.append(confnote)
                published_count += 1

        all_confnotes = {}
        all_confnotes['first_circulation'] = first_circulation
        all_confnotes['published'] = published

        self.confnotes_export = all_confnotes

    def sort_by_date(self):
        """
        sorts the list of items by date
        """
        try:
            self.confnotes.sort(key=lambda x: x["date"], reverse=True)
        except (TypeError, KeyError):
            print("Error in sorting confnotes")
            raise

    def save(self, namefile, pretty=False):
        """
        saves in json file the list of dictionary
        """
        with open(namefile, "w+") as out_file:
            if pretty:
                json.dump(self.confnotes_export, out_file, indent=4, default=str)
            else:
                json.dump(self.confnotes_export, out_file, default=str)

    def runconfnote(self):
        """
        runs all the functions needed
        """
        self.gather_all_confnotes(demo=False)
        print("1all confnotes = {}".format(len(self.confnotes)))
        self.gather_cds_draft()
        print("2all confnotes = {}".format(len(self.confnotes)))
        self.gather_public_cds()
        print("3all confnotes = {}".format(len(self.confnotes)))
        self.gather_draft_and_approval()
        print("4all confnotes = {}".format(len(self.confnotes)))
        self.merge()
        print("5all confnotes = {}".format(len(self.confnotes)))

        self.remove_empty_date_confnotes()
        print("6all confnotes = {}".format(len(self.confnotes)))
        self.sort_by_date()
        print("7all confnotes = {}".format(len(self.confnotes)))
        self.split()
        print("8all confnotes = {}".format(len(self.confnotes)))
        self.save("confnotes.json", pretty=True)
