# Convert CSV into JSON for Upgrade Steering Committee
python /srv/gpicco/po-scripts/scripts/utility/csv_to_json.py /afs/cern.ch/user/a/atlaspo/twikirun/csv6688/table6688.csv PhysicsAnalysisGroupsConveners.json

# Generate twiki table for Upgrade Steering Committee
python /srv/gpicco/po-scripts/scripts/twikiGenerator/UpgradeSteeringCommittee.py