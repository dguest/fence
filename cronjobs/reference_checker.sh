cd /afs/cern.ch/user/a/atlaspo/po-scripts/scripts/referenceChecker/

input="./run_on/runs.txt"
while IFS= read -r line
do
  ./reference_checker.py "$line"
done < "$input"
> "./run_on/runs.txt"
