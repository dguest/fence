# Scripts that run under ATLAS physics office supervision

This repository collects the scripts, the utilities and the cronjobs run by the ATLAS physics office.
Responsible for this repository are Maurizio Colautti and Gianluca Picco.

## General rules

Here are reported just the main guidelines for the repository.
Each script, project, utility, will have its readme file,
that you can access just surfing through the specific subfolders.

## Directories

The repository is composed by the following subdirectories
* `cronjobs`: the crons that must be added to the user acrontab
* `scripts`: the real tools running, including their dipendencies and libraries
* `utilities`: shared libraries should be here

## Please note

Work is in progress. Cleaning code, improvements, adapting code to linter requirements are on their way.

## TODO

A long list of todos is scheduled but what must be remembered is:
* `develop vs production`: 
`stage` branch is used as if it was a development environment, 
but please note that all the changes will run directly in production.
Building separate environments is a scheduled feature
* `environment variables must be exported`:
for the scripts that run as cronjobs to be able to connect to the database,
/utilities/export.sh
is run; this export settings to connect to the database and eventually
other configurations. The file is not tracked by git to avoid sharing sensitive information
If needed, ask the repository mantainer how to build this file.