#!/usr/bin/python2
"""
converts csv to json
"""

import sys
import os
import pandas as pd


try:
    INPUT_FILE = sys.argv[1]
except IndexError:
    print("Error. Must specify an input file")
    sys.exit()

if not os.path.isfile(INPUT_FILE):
    print("Error. File doesn't exist")
    sys.exit()

try:
    OUTPUT_FILE = sys.argv[2]
except IndexError:
    OUTPUT_FILE = os.path.splitext(INPUT_FILE)[0] + ".json"

DF = pd.read_csv(INPUT_FILE)
DF.to_json(path_or_buf=OUTPUT_FILE, orient="records")
