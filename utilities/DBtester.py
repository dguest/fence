#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback
import os
import cx_Oracle

print("Trying to connect to ATLAS_AUTHDB_WRITER...")

DSN = os.environ.get('PROD_W_DSN')
USERNAME = os.environ.get('PROD_W_USR')
PASSWORD = os.environ.get('PROD_W_PSW')

try:
    CONN = cx_Oracle.connect(dsn=DSN, user=USERNAME, password=PASSWORD)
    print("Connected\n")
    CONN.close()
except:
    print("-"*60)
    print("Exception trying to connect to the database")
    traceback.print_exc()
    print("-"*60)

print("Trying to connect to ATLAS_AUTHDB_READER...")

DSN = os.environ.get('PROD_R_DSN')
USERNAME = os.environ.get('PROD_R_USR')
PASSWORD = os.environ.get('PROD_R_PSW')

try:
    CONN = cx_Oracle.connect(dsn=DSN, user=USERNAME, password=PASSWORD)
    print("Connected\n")
    CONN.close()
except:
    print("-"*60)
    print("Exception trying to connect to the database")
    traceback.print_exc()
    print("-"*60)

print("Trying to connect to ATLAS_AUTHDB_DEV...")

DSN = os.environ.get('DEV_DSN')
USERNAME = os.environ.get('DEV_USR')
PASSWORD = os.environ.get('DEV_PSW')

try:
    CONN = cx_Oracle.connect(dsn=DSN, user=USERNAME, password=PASSWORD)
    print("Connected\n")
    CONN.close()
except:
    print("-"*60)
    print("Exception trying to connect to the database")
    traceback.print_exc()
    print("-"*60)


print("Trying to connect to ATLAS_AUTHDB_DEV_W...")

DSN = os.environ.get('DEV_W_DSN')
USERNAME = os.environ.get('DEV_W_USR')
PASSWORD = os.environ.get('DEV_W_PSW')

try:
    CONN = cx_Oracle.connect(dsn=DSN, user=USERNAME, password=PASSWORD)
    print("Connected\n")
    CONN.close()
except:
    print("-"*60)
    print("Exception trying to connect to the database")
    traceback.print_exc()
    print("-"*60)
