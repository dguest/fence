#!/usr/bin/python

import os

page1Content = "Testing mail"

text = "From: maurizio.colautti@cern.ch\n"
text += "To: ctrl.mau@gmail.com\n"
text += "Subject: Changed content on page\n\n"
text += page1Content

p = os.popen("sendmail -t -i", 'w')
p.write(text)
exitcode = p.close()