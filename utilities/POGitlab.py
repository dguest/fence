#!/usr/bin/env python2
"""
library used to manage operations on gitlab PO repositories

environment variable
GITLAB_TOKEN
must be set in order for this to work
"""

import gitlab

class POGitlab(object):
    """
    handles operations to standardized gitlab atlaspo repository
    """

    GITLAB_PREFIX = "atlas-physics-office/"

    def __init__(self, ref_code=None, publication_type='PAPER', project_name='', logger=None):
        self.logger = logger

        self.ref_code = ref_code
        self.type = publication_type
        self.project_name = project_name
        self.group = ''

        self.connection = None
        self.project = None
        self.file = None

        self.connect()

        print("[DEBUG] project url = {}".format("http://gitlab.cern.ch/" + self.project_name))


    def connect(self):
        """
        tries to create a connection to the gitlab project
        """
        if not self.project_name:
            self._project_from_refcode()
        if not self.project_name:
            return

        # try:
        self.connection = gitlab.Gitlab(
            "https://gitlab.cern.ch/",
            private_token="kDSzxbWj8UuXVy84LTa2"
            )
        # except:
        #     if self.logger:
        #         self.logger.error("Can not connect to Gitlab")
        #     else:
        #         print("[ERROR] Can not connect to Gitlab")

        self.project = self.connection.projects.get(self.project_name)

    def get_file(self, file_path):
        """
        gets the file from gitlab
        """
        if self.project:
            #self.file = self.project.files.get(file_path='ANA-SUSY-2018-23-PAPER.bib',ref='master')
            try:
                self.file = self.project.files.get(file_path=file_path, ref='master')
            except:
                print("[ERROR] can't retrieve file {}".format(file_path))
                return ""
            #print(self.file.decode())
            return self.file.decode()


    def _project_from_refcode(self):
        """
        builds the project name from the ref code
        """
        if not self.ref_code:
            if self.logger:
                self.logger.error("Ref code missing trying to connect to gitlab repository")
            return
        self._check_ref_code()
        if not self.group:
            self._extract_group()
        self.project_name = POGitlab.GITLAB_PREFIX + self.group + "/" + "ANA-" + self.ref_code \
                            + "/" + "ANA-" + self.ref_code
        if self.type == "PAPER":
            self.project_name += "-PAPER"
        elif self.type == 'CONFNOTE':
            self.project_name += "-CONFNOTE"
        elif self.type == 'PUBNOTE':
            self.project_name += "-PUBNOTE"


    def _check_ref_code(self):
        """
        evaluates the ref code if ok
        """
        if isinstance(self.ref_code, str):
            self.ref_code = self.ref_code.upper()
            # tries to handle error assigning ref code:
            if self.ref_code.startswith('ANA-'):
                self.ref_code = self.ref_code.replace("ANA-", '')
            return
        if self.logger:
            self.logger.error("Ref code is set to {}".format(self.ref_code))
        self.ref_code = None

    def _extract_group(self):
        """
        extracts group from ref code
        """
        if self.ref_code:
            self.group = self.ref_code[0:4]

    def list_folders(self, path='.'):
        """
        returns a list of folder in path
        """
        tree = self.project.repository_tree(path, all=True)
        return [x["name"] for x in tree if x["type"] == 'tree']

    def list_files(self, path='.'):
        """
        returns a list of files in path
        """
        tree = self.project.repository_tree(path, all=True)
        return [x["name"] for x in tree if x["type"] == 'blob']

    def list_bib_files(self):
        """
        returns a list with complete path of all the bib files in the repo
        """
        tree = self.project.repository_tree(".", all=True)
        all_bibs = [x["name"] \
                    for x in tree \
                    if x["type"] == 'blob' and x['name'].endswith(".bib")]
        for item in tree:
            if item['type'] == 'tree':
                sub_tree = self.project.repository_tree(item['name'])
                for check_file in sub_tree:
                    if check_file["type"] == 'blob' and check_file['name'].endswith(".bib"):
                        all_bibs.append(item['name'] + "/" + check_file["name"])

        return all_bibs

    def list_section_files(self):
        """
        returns a list with complete path of all the bib files in the repo
        """
        base_path = "sections/"
        tree = self.project.repository_tree(base_path)
        all_sections = [base_path + x["name"] \
                    for x in tree \
                    if x["type"] == 'blob' and x['name'].endswith(".tex")]
        for item in tree:
            if item['type'] == 'tree':
                sub_tree = self.project.repository_tree(base_path + item['name'])
                for check_file in sub_tree:
                    if check_file["type"] == 'blob' and check_file['name'].endswith(".tex"):
                        all_sections.append(base_path + item['name'] + "/" + check_file["name"])

        return all_sections


    def list(self, path='.'):
        """
        gets file on current (by now default) path
        """
        print(self.project.repository_tree(path))
        tree = self.project.repository_tree(path)
        for leaf in tree:
            print(leaf["name"])

"""

gl_connection = gitlab.Gitlab(
            "https://gitlab.cern.ch/",
            private_token="kDSzxbWj8UuXVy84LTa2"
            )

project = gl_connection.projects.get("atlas-physics-office/SUSY/ANA-SUSY-2018-23/ANA-SUSY-2018-23-PAPER")

auth_file = project.files.get(file_path='atlas_authlist.xml', ref='master')
print(auth_file.decode())

"""