"""
loads utilities to be used by different scripts

environment variables
ATPOU, ATPOP
must be set in order for this to work
"""

import os
import logging
import logging.handlers
from datetime import date
from environment import LOGS_PATH

ATPOU = os.getenv("ATPOU")
ATPOP = os.getenv("ATPOP")

def set_logger(file_name, toaddrs='', subject='',
               loglevel=logging.INFO, emaillevel=logging.ERROR):
    """
    sets logger
    """
    today = str(date.today())
    if not os.path.exists(os.path.join(LOGS_PATH, file_name)):
        os.mkdir(os.path.join(LOGS_PATH, file_name))


    logging.basicConfig(filename=os.path.join(LOGS_PATH, file_name, file_name + '_' + today + '.log'),
                        filemode='a',
                        format='%(asctime)s - [%(levelname)s] : %(message)s',
                        datefmt='%d-%m-%y %H:%M:%S')

    logger = logging.getLogger()
    logger.setLevel(loglevel)

    if not subject:
        subject = 'ATLASPO log ERROR'

    if not toaddrs:
        toaddrs = ['ctrl.mau@gmail.com']

    email_handler = logging.handlers.SMTPHandler(mailhost=('smtp.cern.ch', '587'),
                                                 fromaddr='atlaspo@cern.ch',
                                                 toaddrs=toaddrs,
                                                 subject=subject,
                                                 credentials=(ATPOU, ATPOP),
                                                 secure=())

    fmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    email_handler.setFormatter(fmt)
    email_handler.setLevel(emaillevel)
    logger.addHandler(email_handler)

    return logger
