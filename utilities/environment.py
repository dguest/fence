"""
sets environment config values
"""

import os

os.environ['ENVIRON'] = 'DEV'

# COMMON PATH
BASE_PATH = "/afs/cern.ch/user/a/atlaspo/po-scripts/"
UTILITY_PATH = os.path.join(BASE_PATH, "utility")
QUERIES_PATH = os.path.join(UTILITY_PATH, "queries")
LOGS_PATH = os.path.join(BASE_PATH, "log")
