"""
utility that from a date in format of string, tries to return a dd-mm-yyyy string
""" 

import re

DATES_MONTHES = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]

class NoStandardDate(Exception):
    """ can't parse this kind of date format """
    pass


def _extract_month_date(string_date):
    """
    serves extract date when the month is written in letters
    """
    index = 0
    for index, month in enumerate(DATES_MONTHES):
        if month in string_date.lower():
            break
    try:
        day, year = string_date.split(DATES_MONTHES[index])
    except:
        raise NoStandardDate

    day = day.replace("/", "").replace("-", "").replace(" ", "")
    year = year.replace("/", "").replace("-", "").replace(" ", "")

    if len(day) > len(year):
        day, year = year, day

    return "{}-{:02d}-{}".format(day, index+1, year)

def _extract_digits_date(string_date):
    """
    serves extract date when the date is all digits
    """
    string_date = string_date.strip()

    #if re.match("\\d{2}[-/\\]\\d{2}[-/\\]\\d{4}", string_date):
    if re.match("(\\d{2})([-\\/ ])(\\d{2})([-\\/ ])(\\d{4})", string_date):
        return string_date.replace("\\", "-").replace("/", "-").replace(" ", "-")

    if re.match("(\\d{4})([-\\/ ])(\\d{2})([-\\/ ])(\\d{2})", string_date):
        return string_date[8:10] + "-" + string_date[5:7] + "-" + string_date[0:4]

    separator = ""
    if string_date.count("-"):
        separator = "-"
    if string_date.count("/"):
        separator = "/"

    if separator:
        if string_date.count(separator) == 2:
            first, second, third = string_date.split(separator)

            if len(third) > len(first):
                return "{:02d}-".format(int(first)) + "{:02d}-".format(int(second)) + third
            return "{:02d}-".format(int(third)) + "{:02d}-".format(int(second)) + first

    return None

def _date_with_month(date, descending=False):
    if not descending:
        return date[0:3] + DATES_MONTHES[int(date[3:5])-1].upper() + date[5:]
    if descending:
        return date[6:] + "-" + DATES_MONTHES[int(date[3:5])-1].upper() + "-" + date[0:3]

def _descending_date(date):
    return date[6:] + "-" + date[3:5] + "-" + date[0:2]

def extract_date(string_date, format_date="dd-mm-yyyy"):
    """
    entry function for parsing of dates in various formats
    """
    for month in DATES_MONTHES:
        if month in string_date.lower():
            date = _extract_month_date(string_date)
            break
    else:
        date = _extract_digits_date(string_date)

    if not date:
        raise NoStandardDate
    else:
        if format_date == "dd-mm-yyyy":
            return date
        elif format_date == "dd-MM-yyyy":
            return _date_with_month(date)
        elif format_date == "yyyy-mm-dd":
            return _descending_date(date)
        elif format_date == "yyyy-MM-dd":
            return _date_with_month(date, descending=True)


print(extract_date("25-jan-2020", format_date="dd-MM-yyyy"))
# print(extract_date("2022-jan-14"))
# print(extract_date("20-01-2018"))
# print(extract_date("20-01-201"))
# print(extract_date("2022/01/14"))