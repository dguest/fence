#!/usr/bin/python

import requests
from DBManager import DBManager
import time
import urllib2

db = DBManager()

query = "SELECT id, href FROM PHASE_LINKS WHERE HREF LIKE '%inspirebeta%'"
results = db.execute(query)

for result in results:
    request = requests.get(result[1].replace("inspirebeta", "inspirehep").replace("%3A", ":"))
    print("link {} returns status {}".format(result[1].replace("inspirebeta", "inspirehep"),request.status_code))
    if request.status_code == 200:
        query = "UPDATE PHASE_LINKS SET HREF = '{}' WHERE id = {}".format(result[1].replace("inspirebeta", "inspirehep").replace("%3A", ":"), result[0])
        print(query)
        #db.execute(query)
    time.sleep(0.5)

querySpecialCharacters = "SELECT id, href, alias FROM PHASE_LINKS WHERE (instr(href, chr(37)) > 0 ) OR (instr(alias, chr(37)) >0)"
resultsSpecial = db.execute(querySpecialCharacters)

for res in resultsSpecial:
    print "UPDATE PHASE_LINKS SET HREF = '" + urllib2.unquote(res[1]) + "', ALIAS = '" + urllib2.unquote(res[2]) + "' WHERE ID = " + str(res[0])
