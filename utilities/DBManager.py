"""
library used to connect to ATLAS databases

environment variables
PROD_W_DSN, PROD_W_USR, PROD_W_PSW
PROD_R_DSN, PROD_R_USR, PROD_R_PSW
DEV_DSN, DEV_USR, DEV_PSW
must be set in order for this to work
"""

__version__ = '0.1'
__author__ = 'Maurizio Colautti - maurizio.colautti@cern.ch'

import os
import ast
import json
import csv
import cx_Oracle

class DBManager(object):
    """
    the class which interfaces with ATLAS db
    """

    def __init__(self, db="WRITER", logger=None):

        self._use_cache = False
        self._logger = logger

        if self._logger:
            self._logger.info("Initializing DBManager")


        if db == "WRITER":
            self.dsn = os.environ.get('PROD_W_DSN')
            self.username = os.environ.get('PROD_W_USR')
            self.password = os.environ.get('PROD_W_PSW')
        elif db == "READER":
            self.dsn = os.environ.get('PROD_R_DSN')
            self.username = os.environ.get('PROD_R_USR')
            self.password = os.environ.get('PROD_R_PSW')
        else:
            self.dsn = os.environ.get('DEV_DSN')
            self.username = os.environ.get('DEV_USR')
            self.password = os.environ.get('DEV_PSW')

        try:
            self.connect()
        except:
            self._use_cache = True
            if self._logger:
                self._logger.warning("Can't connect to the database, try to use cached result")
            else:
                print("[ERROR] Can't connect to the database, will use cached result, if possible")

    def connect(self):
        """
        establishes the connection to the database
        """
        self.conn = cx_Oracle.connect(dsn=self.dsn, user=self.username, password=self.password)

    def set_cache(self, cache):
        """
        manually sets the db manager to load / not load cache file
        instead of querying the db
        """
        self._use_cache = cache
        if self._logger:
            self._logger.info("Using DBManager cache turned to: {}".format(cache))

    def _get_query(self, sql, namefile):
        """
        detects and returns the query that has to run on the db.
        Rules:
        - if a query is passed, then it is executed
        - if no query is passed, we execute the one from the file
        """
        if not sql:
            try:
                with open(namefile, 'r') as sql_file:
                    sql = sql_file.read()
            except:
                # TODO: we don't want a print line when an error occurs
                #       to be replaced with log operations.
                if self._logger:
                    self._logger.error("DBManager: Error trying to load the query from file")
                else:
                    print("[ERROR] Error trying to load the query from file")
                raise
        sql = sql.strip()
        if sql.endswith(';'):
            return sql[:-1]
        return sql

    def _load_cache(self, output, namefile):
        try:
            with open(namefile + ".cache", 'r') as result_file:
                if output == 'json':
                    return json.load(result_file)
                return ast.literal_eval(result_file.read())
        except:
            if self._logger:
                self._logger.error("DBManager: Error trying to load result file {}".format(namefile))
            else:
                print("[ERROR] Trying to load result file {}".format(namefile))


    def _save_cache(self, output, namefile, result, columns='', outfile=''):
        """
        if the query is identified by a file, we keep track of the last efficient run
        """
        if outfile:
            namefile = outfile
        else:
            namefile = namefile + ".cache"
        try:
            with open(namefile, 'w') as result_file:
                if output == 'json':
                    json.dump(result, result_file, default=str)
                elif output == 'tuple':
                    result_file.write(str(result))
                elif output == 'csv':
                    csv_out = csv.writer(result_file, quoting=csv.QUOTE_ALL)
                    csv_out.writerow([columns[index][0] for index in range(0,len(columns))])
                    for row in result:
                        csv_out.writerow(row)
        except:
            if self._logger:
                self._logger.error("DBManager: Error trying to save results on file {}".format(namefile))
            else:
                print("[ERROR] Trying to save results on file {}".format(namefile))


    def execute(self, sql="", format_tuple=(), output='tuple', namefile='', outfile=''):
        """
        entry point for every query executed through the class
        output will default to tuple, unless output parameter is set to 'json'
        if output is 'json', an array of dictionaries, as a json file
        """

        if namefile and self._use_cache:
            return self._load_cache(output, namefile)
        elif self._use_cache:
            if self._logger:
                self._logger.error("DBManager: Can't load cached result because namefile is missing")
            else:
                print("[ERROR] Can't load cached result because namefile is missing")
            return

        sql = self._get_query(sql, namefile)

        cursor = self.conn.cursor()
        if not isinstance(format_tuple, tuple):
            format_tuple = (format_tuple, )
        try:
            cursor.execute(sql, format_tuple)
        except:
            if self._logger:
                self._logger.error("DBManager: Unable to run query {}\n with binds {}".format(sql, format_tuple))
            else:
                print("[ERROR] unable to run query {}\n with binds {}".format(sql, format_tuple))
        
        self.conn.commit()
        try:
            columns = cursor.description
            if output.lower() == 'json':
                result = [{columns[index][0]:column for index, column in enumerate(value)} \
                        for value in cursor.fetchall()]
            else:
                result = cursor.fetchall()

            if namefile:
                self._save_cache(output, namefile, result, columns, outfile)

            return result
        except cx_Oracle.InterfaceError:
            pass


    def execute_from_file(self, namefile, format_tuple=(), output='tuple'):
        """
        OBSOLETE - should use execute instead
        loads the query from a file and executes it
        TODO: insert a warning in the log
        """
        return self.execute(sql="", format_tuple=format_tuple, output=output, namefile=namefile)


    def select(self, sql, format_tuple=(), output='tuple'):
        """
        old entry point for select queries
        should be deprecated
        and will need to be changed in callers scripts to execute
        """
        return self.execute(sql, format_tuple, output)


    def insert(self, sql, binds=()):
        """
        old entry point for insert queries
        should be deprecated
        and will need to be changed in callers scripts to execute
        """
        self.execute(sql, binds)


    def insert_many(self, sql, item_list):
        """
        to be preferred to insert a list of items in a table
        """
        sql = sql.strip()
        if sql.endswith(';'):
            sql = sql[:-1]

        if self._logger:
            self._logger.info("items going to be inserted into database %s" % item_list)
        else:
            print("[INFO] items going to be inserted into database %s" % item_list)
        cursor = self.conn.cursor()
        cursor.prepare(sql)
        cursor.executemany(None, item_list)
        self.conn.commit()


    def close(self):
        """
        connection must be closed by the user through
        dbm.close()
        TODO: convert this in a context manager to avoid user missing
        """
        self.conn.close()
