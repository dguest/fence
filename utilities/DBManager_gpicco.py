import cx_Oracle
import os

class DBManager_gpicco(object):
    def __init__(self, db = "WRITER"):

        if db == "WRITER":
            self.dsn=os.environ.get('PROD_W_DSN')
            self.username=os.environ.get('PROD_W_USR')
            self.password=os.environ.get('PROD_W_PSW')
        elif db == "READER":
            self.dsn=os.environ.get('PROD_R_DSN')
            self.username=os.environ.get('PROD_R_USR')
            self.password=os.environ.get('PROD_R_PSW')
        else:
            self.dsn=os.environ.get('DEV_DSN')
            self.username=os.environ.get('DEV_USR')
            self.password=os.environ.get('DEV_PSW')

        self.connect()

    def connect(self):
        self.cursor = None

        self.conn = cx_Oracle.connect(dsn=self.dsn,user=self.username,password=self.password)

    def execute(self,sql,format_tuple=()):
        return self.select(sql,format_tuple)

    def select(self,sql,format_tuple=()):
        sql=sql.strip()
        if sql.endswith(';'): sql=sql[:-1]
        cursor=self.conn.cursor()
        if not isinstance(format_tuple,tuple):
            format_tuple=(format_tuple,)
        cursor.execute(sql,format_tuple)
        data = cursor.fetchall()
        return data

    def insert(self, sql, binds=()):
        sql=sql.strip()
        if sql.endswith(';'): sql=sql[:-1]
        cursor=self.conn.cursor()
        if not isinstance(binds,tuple):
            binds=(binds,)
        cursor.execute(sql,binds)
        self.conn.commit()

    def insertMany(self, sql, itemList):
        sql=sql.strip()
        if sql.endswith(';'): sql=sql[:-1]
        print ("items going to be inserted into database %s" % itemList)
        cursor = self.conn.cursor()
        cursor.prepare(sql)
        cursor.executemany(None, itemList)
        self.conn.commit()

    def close(self):
        self.conn.close()

    def tupleToJSON(self, keys, tuple):
        json = '{ "results":['
        if len(tuple)>0:
            if len(keys) == len(tuple[0]):
                for data in tuple:
                    counter = 0
                    json += '{'
                    for k in keys:
                        json += '"'+k+'":"'+str(data[counter])+'",'
                        counter += 1
                    json = json[:-1]
                    json += '},'
                json = json[:-1]
                json += ']}'
                return json
            else:
                return "Keys len different from results len"
                return False
        else:
            return "No results found"
            return False